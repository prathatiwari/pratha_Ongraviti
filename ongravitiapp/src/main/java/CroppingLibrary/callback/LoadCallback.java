package CroppingLibrary.callback;


public interface LoadCallback extends Callback{
    void onSuccess();
    void onError();
}
