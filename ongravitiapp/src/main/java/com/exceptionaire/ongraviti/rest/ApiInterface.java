package com.exceptionaire.ongraviti.rest;

import com.exceptionaire.ongraviti.model.EnrollEventResponse;
import com.exceptionaire.ongraviti.model.LikeUnlike;
import com.exceptionaire.ongraviti.model.LoginResponse;
import com.exceptionaire.ongraviti.model.ProfilePictureResponse;
import com.exceptionaire.ongraviti.model.ResponseAcceptRejectDatingRequest;
import com.exceptionaire.ongraviti.model.ResponseAcceptRejectOrbitRequest;
import com.exceptionaire.ongraviti.model.ResponseAskForDatingNFavouriteUser;
import com.exceptionaire.ongraviti.model.ResponseBlockUser;
import com.exceptionaire.ongraviti.model.ResponseDatingRequest;
import com.exceptionaire.ongraviti.model.ResponseDatingUsersList;
import com.exceptionaire.ongraviti.model.ResponseDeleteProfilePic;
import com.exceptionaire.ongraviti.model.ResponseFCMRegistrationID;
import com.exceptionaire.ongraviti.model.ResponseForSearchQuery;
import com.exceptionaire.ongraviti.model.ResponseGetCommentsByPostID;
import com.exceptionaire.ongraviti.model.ResponseGetInterestSubcategory;
import com.exceptionaire.ongraviti.model.ResponseGetOpponentsForDatingEvent;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseMultipleImage;
import com.exceptionaire.ongraviti.model.ResponseMyOrbitRequests;
import com.exceptionaire.ongraviti.model.ResponseNotificationsList;
import com.exceptionaire.ongraviti.model.ResponseOptionImageUpload;
import com.exceptionaire.ongraviti.model.ResponseOrbitContactList;
import com.exceptionaire.ongraviti.model.ResponsePurchasedEventList;
import com.exceptionaire.ongraviti.model.ResponseQuestionnaireList;
import com.exceptionaire.ongraviti.model.ResponseResendOTP;
import com.exceptionaire.ongraviti.model.ResponseSetDefaultProfileCategory;
import com.exceptionaire.ongraviti.model.ResponseSetUserInfo;
import com.exceptionaire.ongraviti.model.ResponseSpeedDatingUserList;
import com.exceptionaire.ongraviti.model.ResponseSuccess;
import com.exceptionaire.ongraviti.model.ResponseSwitchProfileImage;
import com.exceptionaire.ongraviti.model.ResponseUnBlockUser;
import com.exceptionaire.ongraviti.model.ResponseUpdateCover;
import com.exceptionaire.ongraviti.model.ResponseUpdateStatus;
import com.exceptionaire.ongraviti.model.ResponseUserPost;
import com.exceptionaire.ongraviti.model.ResponseUserPostList;
import com.exceptionaire.ongraviti.model.Set_Comment;
import com.exceptionaire.ongraviti.model.SpeedDatingRespose;
import com.exceptionaire.ongraviti.model.UserPostResponse;
import com.exceptionaire.ongraviti.model.deletePostResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Laxmikant on 23/3/17.
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(@Field("contact_number") String contact_number,
                              @Field("password") String password,
                              @Field("device_token_id") String device_token_id,
                              @Field("device_confirm_msg") String device_confirm_msg,
                              @Field("device_type") String device_type,
                              @Field("phonecode") String phonecode);

    @FormUrlEncoded
    @POST("login_fb")
    Call<LoginResponse> fbLogin(@Field("email") String email,
                                @Field("name") String name,
                                @Field("fbUrl") String fbUrl,
                                @Field("contact_number") String contact_number,
                                @Field("facebook_user_id") String facebook_user_id,
                                @Field("device_token_id") String device_token_id,
                                @Field("device_confirm_msg") String device_confirm_msg,
                                @Field("device_type") String device_type,
                                @Field("is_fb") String is_fb);
//    @Multipart
//    @POST("login_fb")
//    Call<LoginResponse> fbLogin(@Part("email") RequestBody email,
//                                @Part("name") RequestBody name,
//                                @Part MultipartBody.Part profile_pic_url,
//                                @Part("contact_number") RequestBody contact_number,
//                                @Part("facebook_user_id") RequestBody facebook_user_id,
//                                @Part("device_token_id") RequestBody device_token_id,
//                                @Part("device_confirm_msg") RequestBody device_confirm_msg,
//                                @Part("device_type") String device_type,
//                                @Part("is_fb") String is_fb);

    @FormUrlEncoded
    @POST("reset_OPT")
    Call<ResponseResendOTP> resendOTP(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("get_dating_event_details")
    Call<SpeedDatingRespose> getEventList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("get_enroll_event")
    Call<EnrollEventResponse> enrollEvent(@Field("user_id") String user_id,
                                          @Field("event_id") String event_id);

    @FormUrlEncoded
    @POST("get_event_credits")
    Call<EnrollEventResponse> getCredits(@Field("user_id") String user_id,
                                         @Field("event_id") String event_id);

    @FormUrlEncoded
    @POST("set_buy_event")
    Call<EnrollEventResponse> getBuyEvent(@Field("user_id") String user_id,
                                          @Field("event_id") String event_id,
                                          @Field("amount") String amount);

    @Multipart
    @POST("set_upload_event_bio_video")
    Call<EnrollEventResponse> uploadVideoBio(@Part("user_id") RequestBody user_id,
                                             @Part("event_id") RequestBody event_id,
                                             @Part MultipartBody.Part event_video_name,
                                             @Part MultipartBody.Part event_video_thumb);

    @FormUrlEncoded
    @POST("get_enroll_event_user_randam")
    Call<ResponseGetOpponentsForDatingEvent> getEventUserList(@Field("event_id") String event_id);

    @FormUrlEncoded
    @POST("userpostlist")
    Call<ResponseUserPostList> get_user_posts(@Field("user_id") String user_id,
                                              @Field("sub_cat_id") String sub_cat_id,
                                              @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("onlyUserPostList")
    Call<ResponseUserPost> get_only_user_posts(@Field("user_id") String user_id,
                                               @Field("profile_cat_id") String profile_cat_id);

    @FormUrlEncoded
    @POST("set_like_unlike_posts")
    Call<LikeUnlike> setLikeUnlike(@Field("user_id") String user_id,
                                   @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("set_comment_posts")
    Call<Set_Comment> setCommentPosts(@Field("user_id") String user_id,
                                      @Field("post_id") String post_id,
                                      @Field("comment_content") String comment_content,
                                      @Field("device_date") String device_date);

    @FormUrlEncoded
    @POST("get_comment_by_posts_id")
    Call<ResponseGetCommentsByPostID> getCommentsByPostID(@Field("post_id") String post_id,
                                                          @Field("comment_id") String comment_id);


    @Multipart
    @POST("set_posts")
    Call<UserPostResponse> set_postsImage(@Part("user_id") RequestBody user_id,
                                          @Part("post_type") RequestBody post_type,
                                          @Part("post_content") RequestBody post_content,
                                          @Part("emoji") RequestBody emoji,
                                          @Part("profile_ct_id") RequestBody profile_ct_id,
                                          @Part("visibility_status") RequestBody visibility_status,
                                          @Part MultipartBody.Part posts_pic,
                                          @Part("device_date") RequestBody device_date);

    @Multipart
    @POST("set_posts")
    Call<UserPostResponse> set_postsVideo(@Part("user_id") RequestBody user_id,
                                          @Part("post_type") RequestBody post_type,
                                          @Part("post_content") RequestBody post_content,
                                          @Part("emoji") RequestBody emoji,
                                          @Part("profile_ct_id") RequestBody profile_ct_id,
                                          @Part("visibility_status") RequestBody visibility_status,
                                          @Part MultipartBody.Part posts_video,
                                          @Part MultipartBody.Part post_video_thumb,
                                          @Part("device_date") RequestBody device_date);

    @FormUrlEncoded
    @POST("set_posts")
    Call<UserPostResponse> set_postContent(@Field("user_id") String user_id,
                                           @Field("post_type") String post_type,
                                           @Field("post_content") String post_content,
                                           @Field("emoji") String emoji,
                                           @Field("profile_ct_id") String profile_ct_id,
                                           @Field("visibility_status") String visibility_status,
                                           @Field("device_date") String device_date);

    @FormUrlEncoded
    @POST("delete_posts")
    Call<deletePostResponse> delete_user_post(@Field("user_id") String user_id,
                                              @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("set_report_abouse")
    Call<deletePostResponse> reportAbusePost(@Field("user_id") String user_id,
                                             @Field("post_id") String post_id,
                                             @Field("post_user_id") String post_user_id);

    @FormUrlEncoded
    @POST("get_user_orbit")
    Call<ResponseOrbitContactList> getUserOrbit(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("get_user_profiles_info")
    Call<ResponseGetUserProfileData> getUserData(@Field("user_id") String user_id,
                                                 @Field("orbitUser_id") String orbitUser_id);


    @FormUrlEncoded
    @POST("get_search_user")
    Call<ResponseForSearchQuery> getSearchReesults(@Field("keyword") String queryString,
                                                   @Field("user_id") String user_id);


    @Multipart
    @POST("set_profile_bio")
    Call<ProfilePictureResponse> uploadSocialVideoBio(@Part("user_id") RequestBody user_id,
                                                      @Part("profile_cat_id") RequestBody profile_cat_id,
                                                      @Part MultipartBody.Part profile_bio,
                                                      @Part MultipartBody.Part bio_Thumb);


    @FormUrlEncoded
    @POST("set_device_data")
    Call<ResponseFCMRegistrationID> setFCMDeviceRegistrationID(@Field("user_id") String user_id,
                                                               @Field("device_token_id") String device_token_id,
                                                               @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("set_request_to_orbit")
    Call<ResponseSuccess> sendOrbitRequest(@Field("user_id") String user_id,
                                           @Field("receiver_user_id") String receiver_user_id,
                                           @Field("device_date") String device_date);

    @FormUrlEncoded
    @POST("set_accept_or_reject_request_to_orbit")
    Call<ResponseAcceptRejectOrbitRequest> acceptORRejectOrbitRequest(@Field("user_id") String user_id,
                                                                      @Field("sender_user_id") String sender_user_id,
                                                                      @Field("profile_cat_id") String profile_cat_id,
                                                                      @Field("invite_status") String invite_status,
                                                                      @Field("device_date") String device_date);

    @FormUrlEncoded
    @POST("set_accept_or_reject_request_to_orbit")
    Call<ResponseSuccess> rejectOrbitRequest(@Field("user_id") String user_id,
                                             @Field("sender_user_id") String sender_user_id,
                                             @Field("profile_cat_id") String profile_cat_id,
                                             @Field("invite_status") String invite_status,
                                             @Field("device_date") String device_date);

    @FormUrlEncoded
    @POST("get_accept_reject_orbit_request_list")
    Call<ResponseMyOrbitRequests> getMyOrbitRequestList(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("get_notification_list")
    Call<ResponseNotificationsList> getNotificationsList(@Field("user_id") String user_id,
                                                         @Field("notification_id") String notification_id);


    @FormUrlEncoded
    @POST("set_user_default_profile")
    Call<ResponseSetDefaultProfileCategory> setUserDefaultProfile(@Field("user_id") String user_id,
                                                                  @Field("profile_cat_id") String profile_cat_id);

    @FormUrlEncoded
    @POST("get_user_profiles_info")
    Call<ResponseGetUserProfileData> getUserProfileData(@Field("user_id") String user_id,
                                                        @Field("orbitUser_id") String orbitUser_id);


    @FormUrlEncoded
    @POST("get_purchased_event_list")
    Call<ResponsePurchasedEventList> getPurchasedEventList(@Field("user_id") String user_id);

    //Block : 1
    @FormUrlEncoded
    @POST("set_user_profile_info")
    Call<ResponseSetUserInfo> setUserProfileData(@Field("user_id") String user_id,
                                                 @Field("name") String name,
                                                 @Field("city") String city,
                                                 @Field("traits") String traits,
                                                 @Field("date_of_birth") String date_of_birth,
                                                 @Field("gender") String gender,
                                                 @Field("interested_in") String interested_in,
                                                 @Field("relationship_status") String relationship_status,
                                                 @Field("beliefs_religious_beliefs") String beliefs_religious_beliefs,
                                                 @Field("political_beliefs") String political_beliefs,
                                                 @Field("height") String height,
                                                 @Field("weight") String weight,
                                                 @Field("body_type") String body_type,
                                                 @Field("eye_color") String eye_color,
                                                 @Field("hair_color") String hair_color,
                                                 @Field("smoking") String smoking,
                                                 @Field("drinking") String drinking,
                                                 @Field("gym") String gym,
                                                 @Field("favorite_book") String favorite_book,
                                                 @Field("favorite_shows") String favorite_shows,
                                                 @Field("favorite_movies") String favorite_movies,
                                                 @Field("countries_travelled") String countries_travelled,
                                                 @Field("events_attended") String events_attended,
                                                 @Field("hobbies") String hobbies,
                                                 @Field("highest_degree") String highest_degree,
                                                 @Field("degree_name") String degree_name,
                                                 @Field("college_university_name") String college_university_name,
                                                 @Field("employment_status") String employment_status,
                                                 @Field("future_goal") String future_goal,
                                                 @Field("area_of_interest") String area_of_interest,
                                                 @Field("interesting_ventures_to_look") String interesting_ventures_to_look,
                                                 @Field("enterpreneurs_i_like") String enterpreneurs_i_like,
                                                 @Field("events_attended_i_want") String events_attended_i_want,
                                                 @Field("events_m_looking") String events_m_looking,
                                                 @Field("industries_i_want_to_target") String industries_i_want_to_target,
                                                 @Field("books_i_like") String books_i_like,
                                                 @Field("what_i_read") String what_i_read,
                                                 @Field("my_interest_cat") String my_interest_cat);

    //Block : 1
    @FormUrlEncoded
    @POST("set_user_profile_info1")
    Call<ResponseSetUserInfo> setUserProfileData1(@Field("user_id") String user_id,
                                                  @Field("name") String name,
                                                  @Field("city") String city,
                                                  @Field("traits") String traits,
                                                  @Field("date_of_birth") String date_of_birth,
                                                  @Field("gender") String gender,
                                                  @Field("interested_in") String interested_in,
                                                  @Field("relationship_status") String relationship_status,
                                                  @Field("beliefs_religious_beliefs") String beliefs_religious_beliefs,
                                                  @Field("political_beliefs") String political_beliefs,
                                                  @Field("height") String height,
                                                  @Field("weight") String weight,
                                                  @Field("body_type") String body_type,
                                                  @Field("eye_color") String eye_color,
                                                  @Field("hair_color") String hair_color,
                                                  @Field("smoking") String smoking,
                                                  @Field("drinking") String drinking,
                                                  @Field("gym") String gym);

    //Block : 2
    @FormUrlEncoded
    @POST("set_user_profile_info2")
    Call<ResponseSetUserInfo> setUserProfileData2(@Field("user_id") String user_id,
                                                  @Field("favorite_book") String favorite_book,
                                                  @Field("favorite_shows") String favorite_shows,
                                                  @Field("favorite_movies") String favorite_movies,
                                                  @Field("countries_travelled") String countries_travelled,
                                                  @Field("events_attended") String events_attended,
                                                  @Field("hobbies") String hobbies);


    //Block : 3
    @FormUrlEncoded
    @POST("set_user_profile_info3")
    Call<ResponseSetUserInfo> setUserProfileData3(@Field("user_id") String user_id,
                                                  @Field("highest_degree") String highest_degree,
                                                  @Field("degree_name") String degree_name,
                                                  @Field("college_university_name") String college_university_name,
                                                  @Field("employment_status") String employment_status,
                                                  @Field("future_goal") String future_goal);


    //Block : 4
    @FormUrlEncoded
    @POST("set_user_profile_info4")
    Call<ResponseSetUserInfo> setUserProfileData4(@Field("user_id") String user_id,
                                                  @Field("area_of_interest") String area_of_interest,
                                                  @Field("interesting_ventures_to_look") String interesting_ventures_to_look,
                                                  @Field("enterpreneurs_i_like") String enterpreneurs_i_like,
                                                  @Field("events_attended_i_want") String events_attended_i_want,
                                                  @Field("events_m_looking") String events_m_looking,
                                                  @Field("industries_i_want_to_target") String industries_i_want_to_target,
                                                  @Field("books_i_like") String books_i_like,
                                                  @Field("what_i_read") String what_i_read);

    //Block : 5
    @FormUrlEncoded
    @POST("set_user_profile_info5")
    Call<ResponseSetUserInfo> setUserProfileData5(@Field("user_id") String user_id,
                                                  @Field("my_interest_cat") String my_interest_cat);

    @FormUrlEncoded
    @POST("get_questionnaire")
    Call<ResponseQuestionnaireList> getQuestionnaire(@Field("user_id") String user_id,
                                                     @Field("profile_cat_id") String profile_cat_id,
                                                     @Field("question_for") String question_for);


    @FormUrlEncoded
    @POST("get_interest_subcategory")
    Call<ResponseGetInterestSubcategory> getInterestSubcategory(@Field("id") String user_id,
                                                                @Field("sub_category") String sub_category);

    @FormUrlEncoded
    @POST("set_speak_out_feed_comment")
    Call<ResponseGetInterestSubcategory> setSpeakOutFeedComment(@Field("user_id") String user_id,
                                                                @Field("speak_ur_mind_id") String sub_category,
                                                                @Field("feed_comment_text") String feed_comment_text);

    @FormUrlEncoded
    @POST("get_dating_user_list")
    Call<ResponseDatingUsersList> getDatingUserList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("set_ask_for_dating_event")
    Call<ResponseAskForDatingNFavouriteUser> setAskForDatingEvent(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("set_favourites_user")
    Call<ResponseAskForDatingNFavouriteUser> setFavouriteUser(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("get_business_user_list")
    Call<ResponseDatingUsersList> getBusinessUserList(@FieldMap Map<String, String> params);


    @Multipart
    @POST("set_cover_pic")
    Call<ResponseUpdateCover> set_profileCover(@Part("user_id") RequestBody user_id,
                                               @Part("profile_cat_id") RequestBody profile_cat_id,
                                               @Part MultipartBody.Part cover_pic);

    @FormUrlEncoded
    @POST("get_event_user_video_list")
    Call<ResponseSpeedDatingUserList> getUserList(@Field("user_id") String user_id,
                                                  @Field("event_id") String event_id,
                                                  @Field("purchase_id") String purchase_id);


    @FormUrlEncoded
    @POST("set_event_request")
    Call<ResponseSuccess> setEventRequest(@Field("user_id") String user_id,
                                          @Field("receiver_user_id") String receiver_user_id,
                                          @Field("event_id") String event_id);


    @FormUrlEncoded
    @POST("set_accept_or_reject_event_request")
    Call<ResponseSuccess> respondToEventRequest(@Field("user_id") String user_id,
                                                @Field("sender_user_id") String receiver_user_id,
                                                @Field("event_id") String event_id,
                                                @Field("event_flag") String event_flag);

    @FormUrlEncoded
    @POST("get_advance_search_user")
    Call<ResponseForSearchQuery> getAdvanceSearchResult(@Field("keyword") String queryString,
                                                        @Field("profile_cat_id") String profileCatId,
                                                        @Field("gender") String gender,
                                                        @Field("smoking") String smoking,
                                                        @Field("drinking") String drinking,
                                                        @Field("relationship_status") String relationshipStatus,
                                                        @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("set_profile_status")
    Call<ResponseUpdateStatus> setProfileStatus(@Field("user_id") String user_id,
                                                @Field("profile_cat_id") String profile_cat_id,
                                                @Field("profile_status") String profile_status);


    @FormUrlEncoded
    @POST("set_accept_or_reject_ask_for_dating_request")
    Call<ResponseAcceptRejectDatingRequest> acceptRejectDatingRequest(@Field("user_id") String user_id,
                                                                      @Field("sender_user_id") String sender_user_id,
                                                                      @Field("invite_status") String invite_status,
                                                                      @Field("ask_for_date_id") String ask_for_date_id
            , @Field("device_date") String device_date);

    @FormUrlEncoded
    @POST("get_accept_reject_ask_for_dating_request_list")
    Call<ResponseDatingRequest> getMyDatingRequestList(@Field("user_id") String user_id,
                                                       @Field("ask_for_type") String ask_for_type);

    @Multipart
    @POST("set_profile_pic")
    Call<ProfilePictureResponse> set_profileImage(@Part("user_id") RequestBody user_id,
                                                  @Part("profile_cat_id") RequestBody profile_cat_id,
                                                  @Part MultipartBody.Part profile_pic);

    @Multipart
    @POST("set_profile_pic_add")
    Call<ResponseOptionImageUpload> setOptionProfileImage(@Part("user_id") RequestBody user_id,
                                                          @Part("profile_cat_id") RequestBody profile_cat_id,
                                                          @Part MultipartBody.Part profile_pic);

    @FormUrlEncoded
    @POST("delete_profile_pic")
    Call<ResponseDeleteProfilePic> deleteProfileImage(@Field("user_id") String user_id,
                                                      @Field("profile_cat_id") String profile_cat_id,
                                                      @Field("multi_pic_cat_id") String option_pic_id,
                                                      @Field("is_delete_profile") String is_delete_profile);

    @FormUrlEncoded
    @POST("delete_option_profile_pic")
    Call<ResponseSwitchProfileImage> deleteOptionProfileImage(@Field("user_id") String user_id,
                                                              @Field("option_pic_id") String option_pic_id);


    @FormUrlEncoded
    @POST("set_switch_profile_pic")
    Call<ResponseSwitchProfileImage> switchProfileImage(@Field("user_id") String user_id,
                                                        @Field("profile_cat_id") String profile_cat_id,
                                                        @Field("option_id") String option_id);

    @FormUrlEncoded
    @POST("set_edit_text_posts")
    Call<ResponseUpdateStatus> setEditPost(@Field("user_id") String user_id,
                                           @Field("post_id") String post_id,
                                           @Field("visibility_status") String visibility_status,
                                           @Field("post_content") String post_content,
                                           @Field("emoji") String emoji,
                                           @Field("device_date") String device_date);

    @FormUrlEncoded
    @POST("get_block_orbit_list")
    Call<ResponseBlockUser> getMyBlockList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("set_unblock_to_orbit")
    Call<ResponseUnBlockUser> unblockUser(@Field("user_id") String user_id,
                                          @Field("sender_user_id") String sender_user_id,
                                          @Field("flag_type") String flag_type);
}