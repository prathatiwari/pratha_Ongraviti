package com.exceptionaire.ongraviti.listener_interface;

/**
 * Created by root on 3/4/17.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}