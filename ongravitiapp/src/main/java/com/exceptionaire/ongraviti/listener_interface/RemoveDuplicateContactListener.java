package com.exceptionaire.ongraviti.listener_interface;

import com.exceptionaire.ongraviti.model.Contact;

import java.util.List;

/**
 * Created by Laxmikant on 17/3/17.
 */

public interface RemoveDuplicateContactListener {
    public void getResult(List<Contact> contactList);
}
