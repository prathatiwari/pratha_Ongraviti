package com.exceptionaire.ongraviti.listener_interface;

import com.exceptionaire.ongraviti.model.Country;

/**
 * Created by bhakti  on 4/18/2016.
 */
public interface ClickListener {

    void onClick(Country country);
}
