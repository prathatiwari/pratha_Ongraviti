package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_implementations;

import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_activities.VideoCallActivity;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback;

import org.webrtc.CameraVideoCapturer;

/**
 * Created by root on 14/4/17.
 */


public interface ConversationFragmentCallbackListenerVideo {

    void addTCClientConnectionCallback(QBRTCSessionStateCallback clientConnectionCallbacks);
    void removeRTCClientConnectionCallback(QBRTCSessionStateCallback clientConnectionCallbacks);

    void addRTCSessionEventsCallback(QBRTCSessionEventsCallback eventsCallback);
    void removeRTCSessionEventsCallback(QBRTCSessionEventsCallback eventsCallback);

    void addCurrentCallStateCallback(VideoCallActivity.CurrentCallStateCallback currentCallStateCallback);
    void removeCurrentCallStateCallback(VideoCallActivity.CurrentCallStateCallback currentCallStateCallback);

    void addOnChangeDynamicToggle(VideoCallActivity.OnChangeDynamicToggle onChangeDynamicCallback);
    void removeOnChangeDynamicToggle(VideoCallActivity.OnChangeDynamicToggle onChangeDynamicCallback);

    void onSetAudioEnabled(boolean isAudioEnabled);

    void onSetVideoEnabled(boolean isNeedEnableCam);

    void onSwitchAudio();

    void onHangUpCurrentSession();

    void onStartScreenSharing();

    void onSwitchCamera(CameraVideoCapturer.CameraSwitchHandler cameraSwitchHandler);
}
