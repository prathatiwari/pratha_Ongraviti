package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 14/4/17.
 */

public class QbUsersDbManagerVideo {
    private static String TAG = QbUsersDbManagerVideo.class.getSimpleName();

    private static QbUsersDbManagerVideo instance;
    private Context mContext;

    private QbUsersDbManagerVideo(Context context) {
        this.mContext = context;
    }

    public static QbUsersDbManagerVideo getInstance(Context context) {
        if (instance == null) {
            instance = new QbUsersDbManagerVideo(context);
        }

        return instance;
    }

    public ArrayList<QBUser> getAllUsers() {
        ArrayList<QBUser> allUsers = new ArrayList<>();
        DbHelperVideo dbHelper = new DbHelperVideo(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DbHelperVideo.DB_TABLE_NAME, null, null, null, null, null, null);

        if (c.moveToFirst()) {
            int userIdColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_ID);
            int userLoginColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_LOGIN);
            int userPassColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_PASSWORD);
            int userFullNameColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_FULL_NAME);
            int userTagColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_TAG);

            do {
                QBUser qbUser = new QBUser();

                qbUser.setFullName(c.getString(userFullNameColIndex));
                qbUser.setLogin(c.getString(userLoginColIndex));
                qbUser.setId(c.getInt(userIdColIndex));
                qbUser.setPassword(c.getString(userPassColIndex));

                StringifyArrayList<String> tags = new StringifyArrayList<>();
                tags.add(c.getString(userTagColIndex));
                qbUser.setTags(tags);

                allUsers.add(qbUser);
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return allUsers;
    }

    public QBUser getUserById(Integer userId) {
        QBUser qbUser = null;
        DbHelperVideo dbHelper = new DbHelperVideo(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DbHelperVideo.DB_TABLE_NAME, null, null, null, null, null, null);

        if (c.moveToFirst()) {
            int userIdColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_ID);
            int userLoginColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_LOGIN);
            int userPassColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_PASSWORD);
            int userFullNameColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_FULL_NAME);
            int userTagColIndex = c.getColumnIndex(DbHelperVideo.DB_COLUMN_USER_TAG);

            do {
                if (c.getInt(userIdColIndex) == userId) {
                    qbUser = new QBUser();
                    qbUser.setFullName(c.getString(userFullNameColIndex));
                    qbUser.setLogin(c.getString(userLoginColIndex));
                    qbUser.setId(c.getInt(userIdColIndex));
                    qbUser.setPassword(c.getString(userPassColIndex));

                    StringifyArrayList<String> tags = new StringifyArrayList<>();
                    tags.add(c.getString(userTagColIndex).split(","));
                    qbUser.setTags(tags);
                    break;
                }
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return qbUser;
    }

    public void saveAllUsers(ArrayList<QBUser> allUsers, boolean needRemoveOldData) {
        if (needRemoveOldData) {
            clearDB();
        }

        for (QBUser qbUser : allUsers) {
            saveUser(qbUser);
        }
        Log.d(TAG, "saveAllUsers");
    }

    public void saveUser(QBUser qbUser) {
        ContentValues cv = new ContentValues();
        DbHelperVideo dbHelper = new DbHelperVideo(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cv.put(DbHelperVideo.DB_COLUMN_USER_FULL_NAME, qbUser.getFullName());
        cv.put(DbHelperVideo.DB_COLUMN_USER_LOGIN, qbUser.getLogin());
        cv.put(DbHelperVideo.DB_COLUMN_USER_ID, qbUser.getId());
        cv.put(DbHelperVideo.DB_COLUMN_USER_PASSWORD, qbUser.getPassword());
        cv.put(DbHelperVideo.DB_COLUMN_USER_TAG, qbUser.getTags().getItemsAsString());

        db.insert(DbHelperVideo.DB_TABLE_NAME, null, cv);
        dbHelper.close();
    }

    public void clearDB() {
        DbHelperVideo dbHelper = new DbHelperVideo(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DbHelperVideo.DB_TABLE_NAME, null, null);
        dbHelper.close();
    }

    public ArrayList<QBUser> getUsersByIds(List<Integer> usersIds) {
        ArrayList<QBUser> qbUsers = new ArrayList<>();

        for (Integer userId : usersIds) {
            if (getUserById(userId) != null) {
                qbUsers.add(getUserById(userId));
            }
        }

        return qbUsers;
    }
}


