package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils;

import android.text.TextUtils;

import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_model.QbConfigsVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.quick_config.ConfigParserVideo;
import com.google.gson.Gson;
import com.quickblox.users.model.QBUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by root on 14/4/17.
 */

public class CoreConfigUtilsVideo {

    public static final String USER_LOGIN_FIELD_NAME = "user_login";
    public static final String USER_PASSWORD_FIELD_NAME = "user_password";

    public static QbConfigsVideo getCoreConfigs(String fileName) throws IOException {
        ConfigParserVideo configParser = new ConfigParserVideo();
        Gson gson = new Gson();
        return gson.fromJson(configParser.getConfigsAsJsonString(fileName), QbConfigsVideo.class);
    }

    public static QbConfigsVideo getCoreConfigsOrNull(String fileName){
        QbConfigsVideo qbConfigs = null;

        try {
            qbConfigs = getCoreConfigs(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return qbConfigs;
    }

    public static String getStringConfigFromFile(String fileName, String fieldName) throws IOException, JSONException {
        JSONObject appConfigs = new ConfigParserVideo().getConfigsAsJson(fileName);
        return appConfigs.getString(fieldName);
    }

    public static String getStringConfigFromFileOrNull(String fileName, String fieldName) {
        String fieldValue = null;

        try {
            fieldValue = getStringConfigFromFile(fileName, fieldName);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return fieldValue;
    }

    public static boolean isStringConfigFromFileNotEmpty(String fileName, String fieldName){
        return !TextUtils.isEmpty(getStringConfigFromFileOrNull(fileName, fieldName));
    }

    public static QBUser getUserFromConfig(String fileName){
        QBUser qbUser = null;

        String userLogin;
        String userPassword;

        try {
            JSONObject configs = new ConfigParserVideo().getConfigsAsJson(fileName);
            userLogin = configs.getString(USER_LOGIN_FIELD_NAME);
            userPassword = configs.getString(USER_PASSWORD_FIELD_NAME);
            qbUser = new QBUser(userLogin, userPassword);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return qbUser;
    }
}
