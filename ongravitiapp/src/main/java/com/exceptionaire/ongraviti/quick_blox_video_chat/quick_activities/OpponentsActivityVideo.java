package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_adapter.OpponentsAdapterVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_db.QbUsersDbManagerVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.PermissionsCheckerVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.PushNotificationSenderVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.SharedPrefsHelperVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.UsersUtilsVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.WebRtcSessionManagerVideo;
import com.exceptionaire.ongraviti.quickblox.services.CallService;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


/**
 * QuickBlox team
 */
public class OpponentsActivityVideo extends BaseActivity implements AppConstants, View.OnClickListener {
    private static final String TAG = OpponentsActivityVideo.class.getSimpleName();

    private static final long ON_ITEM_CLICK_DELAY = TimeUnit.SECONDS.toMillis(10);

    private OpponentsAdapterVideo opponentsAdapter;
    private ListView opponentsListView;
    private QBUser currentUser;
    private ArrayList<QBUser> currentOpponentsList;
    private QbUsersDbManagerVideo dbManager;
    private boolean isRunForCall;
    private WebRtcSessionManagerVideo webRtcSessionManager;

    private PermissionsCheckerVideo checker;

    private Button call, next;

    private int quickBlox_id = 0;
    private String quickBlox_name = null, user_id = null;


    private RoundedImageView user_profile;
    private ImageView user_bio_image;
    private Button call_to_user;
    private TextView user_name;

    public static void start(Context context, boolean isRunForCall) {
        Intent intent = new Intent(context, OpponentsActivityVideo.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(EXTRA_IS_STARTED_FOR_CALL, isRunForCall);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_opponents_video);

        initFields();
//        initDefaultActionBar();
        initUi();
        startLoadUsers();

        if (isRunForCall && webRtcSessionManager.getCurrentSession() != null) {
            VideoCallActivity.start(OpponentsActivityVideo.this, true);
        }

        checker = new PermissionsCheckerVideo(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUsersList();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null) {
            isRunForCall = intent.getExtras().getBoolean(EXTRA_IS_STARTED_FOR_CALL);
            if (isRunForCall && webRtcSessionManager.getCurrentSession() != null) {
                VideoCallActivity.start(OpponentsActivityVideo.this, true);
            }
        }
    }

/*    @Override
    protected View getSnackbarAnchorView() {
        return findViewById(R.id.list_opponents);
    }*/

    private void startPermissionsActivity(boolean checkOnlyAudio) {
        PermissionsActivityVideo.startActivity(this, checkOnlyAudio, PERMISSIONS);
    }

    private void initFields() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isRunForCall = extras.getBoolean(EXTRA_IS_STARTED_FOR_CALL);
        }

        currentUser = sharedPrefsHelper.getQbUser();
        dbManager = QbUsersDbManagerVideo.getInstance(getApplicationContext());
        webRtcSessionManager = WebRtcSessionManagerVideo.getInstance(getApplicationContext());
    }

    private void startLoadUsers() {
        String currentRoomName = SharedPrefsHelperVideo.getInstance().get(PREF_CURREN_ROOM_NAME);//tag name
        requestExecutor.loadUsersByTag(getResources().getString(R.string.ongraviti), new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> result, Bundle params) {
                dbManager.saveAllUsers(result, true);
                initUsersList();
            }

            @Override
            public void onError(QBResponseException responseException) {
                /*showErrorSnackbar(R.string.loading_users_error, responseException, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startLoadUsers();
                    }
                });*/
            }
        });
    }

    private void initUi() {

        user_profile = (RoundedImageView) findViewById(R.id.user_profile);
        call_to_user = (Button) findViewById(R.id.call_to_user);
        user_bio_image = (ImageView) findViewById(R.id.user_bio_image);
        user_name = (TextView) findViewById(R.id.user_name);


//        quickBlox_id = 28899300;//28895717; //27581370;
//        quickBlox_name = "8208395082"; //"8585858583";
        if (getIntent() != null) {

            Picasso.with(this).load(getIntent().getStringExtra("BioImage"))
                    .placeholder(R.drawable.alexandra_daddario_bg).fit().centerCrop().into(user_bio_image);

            Picasso.with(this).load(getIntent().getStringExtra("userImage"))
                    .placeholder(R.drawable.profile).fit().centerCrop().into(user_profile);

            user_name.setText(getIntent().getStringExtra("userName"));

            if (getIntent().getStringExtra("opponent_qb_id") != null && !getIntent().getStringExtra("opponent_qb_id").isEmpty())
                quickBlox_id = Integer.parseInt(getIntent().getStringExtra("opponent_qb_id"));
            quickBlox_name = getIntent().getStringExtra("opponent_qb_name");


          /*  user_id = getIntent().getStringExtra("user_id");

            if(user_id!=null) {

                if (user_id.equals("349")) {

                    quickBlox_id = 27581370;
                    quickBlox_name = "8585858583";

                } else if (user_id.equals("350")) {

                    quickBlox_id = 27581573;
                    quickBlox_name = "7878787875";

                } else if (user_id.equals("341")) {*/


               /* }else if(user_id.equals("352")){

                    quickBlox_id = 27588295;
                    quickBlox_name = "8686868685";

                }
            }*/
        }


        call_to_user.setOnClickListener(OpponentsActivityVideo.this);


        opponentsListView = (ListView) findViewById(R.id.list_opponents);
        call = (Button) findViewById(R.id.call);
        next = (Button) findViewById(R.id.next);

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startCall(true,id,name);
                startCall(true);
                if (isLoggedInChat()) {

                }
                if (checker.lacksPermissions(PERMISSIONS)) {
                    startPermissionsActivity(false);
                }
            }
        });
    }

    private boolean isCurrentOpponentsListActual(ArrayList<QBUser> actualCurrentOpponentsList) {
        boolean equalActual = actualCurrentOpponentsList.retainAll(currentOpponentsList);
        boolean equalCurrent = currentOpponentsList.retainAll(actualCurrentOpponentsList);
        return !equalActual && !equalCurrent;
    }

    private void initUsersList() {
//      checking whether currentOpponentsList is actual, if yes - return
        if (currentOpponentsList != null) {
            ArrayList<QBUser> actualCurrentOpponentsList = dbManager.getAllUsers();
            actualCurrentOpponentsList.remove(sharedPrefsHelper.getQbUser());
            if (isCurrentOpponentsListActual(actualCurrentOpponentsList)) {
                return;
            }
        }
        proceedInitUsersList();
    }

    private void proceedInitUsersList() {
        currentOpponentsList = dbManager.getAllUsers();
        Log.d(TAG, "proceedInitUsersList currentOpponentsList= " + currentOpponentsList);
        currentOpponentsList.remove(sharedPrefsHelper.getQbUser());
        opponentsAdapter = new OpponentsAdapterVideo(this, currentOpponentsList);
        opponentsAdapter.setSelectedItemsCountsChangedListener(new OpponentsAdapterVideo.SelectedItemsCountsChangedListener() {
            @Override
            public void onCountSelectedItemsChanged(int count) {
                updateActionBar(count);
            }
        });

        opponentsListView.setAdapter(opponentsAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (opponentsAdapter != null && !opponentsAdapter.getSelectedItems().isEmpty()) {
            getMenuInflater().inflate(R.menu.activity_selected_opponents, menu);
        } else {
            getMenuInflater().inflate(R.menu.activity_opponents, menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
          /*  case R.id.update_opponents_list:
                startLoadUsers();
                return true;

            case R.id.settings:
                showSettings();
                return true;

            case R.id.log_out:
                logOut();
                return true;*/

            case R.id.start_video_call:
                if (isLoggedInChat()) {
                   /* startCall(true,id,name);*/
                    startCall(true);
                }
                if (checker.lacksPermissions(PERMISSIONS)) {
                    startPermissionsActivity(false);
                }
                return true;

            case R.id.start_audio_call:
                if (isLoggedInChat()) {
                   /* startCall(false,id,name);*/
                    startCall(false);
                }
                if (checker.lacksPermissions(PERMISSIONS[1])) {
                    startPermissionsActivity(true);
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isLoggedInChat() {
        if (!QBChatService.getInstance().isLoggedIn()) {
//            Toaster.shortToast(R.string.dlg_signal_error);
            tryReLoginToChat();
            return false;
        }
        return true;
    }

    private void tryReLoginToChat() {
        if (sharedPrefsHelper.hasQbUser()) {
            QBUser qbUser = sharedPrefsHelper.getQbUser();
            CallService.start(this, qbUser);
        }
    }

/*    private void showSettings() {
        SettingsActivity.start(this);
    }*/

    private void startCall(boolean isVideoCall/*, int Id, String name*/) {
       /* if (opponentsAdapter.getSelectedItems().size() > MAX_OPPONENTS_COUNT) {
//            Toaster.longToast(String.format(getString(R.string.error_max_opponents_count),
//                    Consts.MAX_OPPONENTS_COUNT));
            return;
        }
*/
        Log.d(TAG, "startCall()");
//        ArrayList<Integer> opponentsList = CollectionsUtilsVideo.getIdsSelectedOpponents(opponentsAdapter.getSelectedItems());

        ArrayList<Integer> opponentsList = new ArrayList<>();
        opponentsList.add(quickBlox_id);

        QBRTCTypes.QBConferenceType conferenceType = isVideoCall
                ? QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO
                : QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_AUDIO;

        QBRTCClient qbrtcClient = QBRTCClient.getInstance(getApplicationContext());

        QBRTCSession newQbRtcSession = qbrtcClient.createNewSessionWithOpponents(opponentsList, conferenceType);

        WebRtcSessionManagerVideo.getInstance(this).setCurrentSession(newQbRtcSession);

        PushNotificationSenderVideo.sendPushMessage(opponentsList, /*currentUser.getFullName()*//*"8568965896"*/quickBlox_name);

        VideoCallActivity.start(this, false);
        Log.d(TAG, "conferenceType = " + conferenceType);
    }

    private void initActionBarWithSelectedUsers(int countSelectedUsers) {
      /*  setActionBarTitle(String.format(getString(
                countSelectedUsers > 1
                        ? R.string.tile_many_users_selected
                        : R.string.title_one_user_selected),
                countSelectedUsers));*/
    }

    private void updateActionBar(int countSelectedUsers) {
        if (countSelectedUsers < 1) {
//            initDefaultActionBar();
        } else {
//            removeActionbarSubTitle();
            initActionBarWithSelectedUsers(countSelectedUsers);
        }

        invalidateOptionsMenu();
    }

    private void logOut() {
        unsubscribeFromPushes();
        startLogoutCommand();
        removeAllUserData();
        startLoginActivity();
    }

    private void startLogoutCommand() {
        CallService.logout(this);
    }

    private void unsubscribeFromPushes() {
        SubscribeService.unSubscribeFromPushes(this);
    }

    private void removeAllUserData() {
        UsersUtilsVideo.removeUserData(getApplicationContext());
        requestExecutor.deleteCurrentUser(currentUser.getId(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                Log.d(TAG, "Current user was deleted from QB");
            }

            @Override
            public void onError(QBResponseException e) {
                Log.e(TAG, "Current user wasn't deleted from QB " + e);
            }
        });
    }

    private void startLoginActivity() {
//        LoginActivity.start(this);
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.call_to_user:

                if (isNetworkAvailable()) {

                    if (isLoggedInChat()) {
                   /* startCall(true,id,name);*/
                        startCall(true);
                    }
                    if (checker.lacksPermissions(PERMISSIONS)) {
                        startPermissionsActivity(false);
                    }
                } else {
                    CommonDialogs.showMessageDialog(OpponentsActivityVideo.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                }

                break;
        }
    }
}