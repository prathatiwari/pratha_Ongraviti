package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_implementations;

/**
 * Created by root on 14/4/17.
 */

public interface IncomeCallFragmentCallbackListenerVideo {

    void onAcceptCurrentSession();

    void onRejectCurrentSession();
}
