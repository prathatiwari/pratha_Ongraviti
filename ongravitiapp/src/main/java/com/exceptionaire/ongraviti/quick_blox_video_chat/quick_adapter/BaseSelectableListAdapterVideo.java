package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_adapter;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by root on 18/4/17.
 */

public abstract class BaseSelectableListAdapterVideo<T> extends BaseListAdapterVideo<T> {

    protected List<T> selectedItems;

    public BaseSelectableListAdapterVideo(Context context, List<T> objectsList) {
        super(context, objectsList);
        selectedItems = new ArrayList<>();
    }

    public void toggleSelection(int position) {
        T item = getItem(position);
        toggleSelection(item);
    }

    public void toggleSelection(T item) {
        if (selectedItems.contains(item)) {
            selectedItems.remove(item);
        } else {
            selectedItems.add(item);
        }
        notifyDataSetChanged();
    }

    public void selectItem(int position) {
        T item = getItem(position);
        selectItem(item);
    }

    public void selectItem(T item) {
        if (selectedItems.contains(item)) {
            return;
        }
        selectedItems.add(item);
        notifyDataSetChanged();
    }

    public Collection<T> getSelectedItems() {
        return selectedItems;
    }

    protected boolean isItemSelected(int position) {
        return !selectedItems.isEmpty() && selectedItems.contains(getItem(position));
    }

    public void clearSelection() {
        selectedItems.clear();
        notifyDataSetChanged();
    }
}