package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils;

import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by root on 14/4/17.
 */

public class CollectionsUtilsVideo {

    public static String makeStringFromUsersFullNames(ArrayList<QBUser> allUsers) {
        StringifyArrayList<String> usersNames = new StringifyArrayList<>();

        for (QBUser usr : allUsers) {
            if (usr.getFullName() != null) {
                usersNames.add(usr.getFullName());
            } else if (usr.getId() != null) {
                usersNames.add(String.valueOf(usr.getId()));
            }
        }
        return usersNames.getItemsAsString().replace(",",", ");
    }

    public static ArrayList<Integer> getIdsSelectedOpponents(Collection<QBUser> selectedUsers){
        ArrayList<Integer> opponentsIds = new ArrayList<>();
        if (!selectedUsers.isEmpty()){
            for (QBUser qbUser : selectedUsers){
                opponentsIds.add(qbUser.getId());
            }
        }

        return opponentsIds;
    }
}
