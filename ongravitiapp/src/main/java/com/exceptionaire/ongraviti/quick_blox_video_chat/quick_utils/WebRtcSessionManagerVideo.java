package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils;

import android.content.Context;
import android.util.Log;

import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_activities.OpponentsActivityVideo;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacksImpl;

/**
 * Created by root on 14/4/17.
 */


public class WebRtcSessionManagerVideo extends QBRTCClientSessionCallbacksImpl {
    private static final String TAG = WebRtcSessionManagerVideo.class.getSimpleName();

    private static WebRtcSessionManagerVideo instance;
    private Context context;

    private static QBRTCSession currentSession;

    private WebRtcSessionManagerVideo(Context context) {
        this.context = context;
    }

    public static WebRtcSessionManagerVideo getInstance(Context context){
        if (instance == null){
            instance = new WebRtcSessionManagerVideo(context);
        }

        return instance;
    }

    public QBRTCSession getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(QBRTCSession qbCurrentSession) {
        currentSession = qbCurrentSession;
    }

    @Override
    public void onReceiveNewSession(QBRTCSession session) {
        Log.d(TAG, "onReceiveNewSession to WebRtcSessionManagerVideo");

        if (currentSession == null){
            setCurrentSession(session);
            OpponentsActivityVideo.start(context, true);
        }
    }

    @Override
    public void onSessionClosed(QBRTCSession session) {
        Log.d(TAG, "onSessionClosed WebRtcSessionManagerVideo");

        if (session.equals(getCurrentSession())){
            setCurrentSession(null);
        }
    }
}
