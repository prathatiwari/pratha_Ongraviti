package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.core.OnGravitiApp;

/**
 * Created by root on 14/4/17.
 */


public class ResourceUtilsVideo {

    public static String getString(@StringRes int stringId) {
        return OnGravitiApp.getInstance().getString(stringId);
    }

    public static Drawable getDrawable(@DrawableRes int drawableId) {
        return OnGravitiApp.getInstance().getResources().getDrawable(drawableId);
    }

    public static int getColor(@ColorRes int colorId) {
        return OnGravitiApp.getInstance().getResources().getColor(R.color.grey);
    }

    public static int getDimen(@DimenRes int dimenId) {
        return (int) OnGravitiApp.getInstance().getResources().getDimension(dimenId);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

}
