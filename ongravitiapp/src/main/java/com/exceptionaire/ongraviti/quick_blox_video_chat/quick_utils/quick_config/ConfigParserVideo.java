package com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.quick_config;

import android.content.Context;

import com.exceptionaire.ongraviti.core.OnGravitiApp;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.AssetsUtilsVideo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by root on 14/4/17.
 */

public class ConfigParserVideo {

    private Context context;

    public ConfigParserVideo() {
        context = OnGravitiApp.getInstance().getApplicationContext();
    }

    public String getConfigsAsJsonString(String fileName) throws IOException {
        return AssetsUtilsVideo.getJsonAsString(fileName, context);
    }

    public JSONObject getConfigsAsJson(String fileName) throws IOException, JSONException {
        return new JSONObject(getConfigsAsJsonString(fileName));
    }

    public String getConfigByName(JSONObject jsonObject, String fieldName) throws JSONException {
        return jsonObject.getString(fieldName);
    }
}