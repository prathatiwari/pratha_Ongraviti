package com.exceptionaire.ongraviti.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.EnrollEventResponse;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.exceptionaire.ongraviti.utilities.TimerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EnrollActivity extends BaseDrawerActivity implements View.OnClickListener, AppConstants {

    private Button buy_session;
    private ProgressDialog progressDialog;
    private TimerView current_balance_progress, speed_dating_credit_progress;
    private TextView current_bal_value, speed_dating_credit_value;
    private String eventAmount, currentCredit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_enroll, frameLayout);
        initialiseComponents();
        arcMenu.setVisibility(View.GONE);
    }

    private void initialiseComponents() {

        current_balance_progress = (TimerView) findViewById(R.id.current_balance_progress);
        speed_dating_credit_progress = (TimerView) findViewById(R.id.speed_dating_credit_progress);
        current_bal_value = (TextView) findViewById(R.id.current_bal_value);
        speed_dating_credit_value = (TextView) findViewById(R.id.speed_dating_credit_value);

        buy_session = (Button) findViewById(R.id.buy_session);

        if (isNetworkAvailable()) {
            if (AppSettings.getLoginUserId() != null && AppSettings.getKeyEventId() != null) {
                getCreditDetails(AppSettings.getLoginUserId(), AppSettings.getKeyEventId());
            }
        } else {
            CommonDialogs.dialog_with_one_btn_without_title(EnrollActivity.this, getResources().getString(R.string.check_internet));
        }
        buy_session.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.buy_session:

                if (isNetworkAvailable()) {
                    if (AppSettings.getLoginUserId() != null && AppSettings.getKeyEventId() != null) {

                        if (Integer.parseInt(currentCredit) > Integer.parseInt(eventAmount)) {

                            buyEvent(AppSettings.getLoginUserId(), AppSettings.getKeyEventId(), eventAmount);
                        } else {
                            Toast.makeText(this, getApplicationContext().getResources().getString(R.string.credit_balance), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    CommonDialogs.dialog_with_one_btn_without_title(EnrollActivity.this, getResources().getString(R.string.check_internet));
                }

                break;
        }

    }

    /***********
     * Credit Details
     ***********/

    private void getCreditDetails(String user_id, String event_id) {

        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<EnrollEventResponse> getDetails = requestInterface.getCredits(user_id, event_id);
            getDetails.enqueue(new Callback<EnrollEventResponse>() {
                @Override
                public void onResponse(Call<EnrollEventResponse> call, Response<EnrollEventResponse> response) {

                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response.body() != null) {

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            showProgress(response.body());

                        } else if (response.body().getStatus().equals(RESPONSE_ERROR)) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<EnrollEventResponse> call, Throwable t) {

                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });

        } catch (Exception e) {

            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

    }

    private void showProgress(final EnrollEventResponse enrollEventResponse) {


        current_balance_progress.setProgress(Integer.parseInt(enrollEventResponse.getCurrent_credit()));

        speed_dating_credit_progress.setProgress(Integer.parseInt(enrollEventResponse.getEvent_amount()));


        eventAmount = enrollEventResponse.getEvent_amount();
        currentCredit = enrollEventResponse.getCurrent_credit();

        if (Integer.parseInt(enrollEventResponse.getCurrent_credit()) != 0) {

            current_balance_progress.animateProgressTo(0, Integer.parseInt(enrollEventResponse.getCurrent_credit()), new TimerView.ProgressAnimationListener() {


                @Override
                public void onAnimationStart() {
                }

                @Override
                public void onAnimationProgress(int progress) {
                    current_balance_progress.setTitleColor(getApplicationContext().getResources().getColor(R.color.white));
                    current_balance_progress.setTitle("" + Integer.parseInt(enrollEventResponse.getCurrent_credit()));
                }

                @Override
                public void onAnimationFinish() {
                    current_balance_progress.setSubTitle("");
                }
            });
        }

        if (Integer.parseInt(enrollEventResponse.getEvent_amount()) != 0) {

            speed_dating_credit_progress.animateProgressTo(0, Integer.parseInt(enrollEventResponse.getEvent_amount()), new TimerView.ProgressAnimationListener() {


                @Override
                public void onAnimationStart() {
                }

                @Override
                public void onAnimationProgress(int progress) {
                    speed_dating_credit_progress.setTitleColor(getApplicationContext().getResources().getColor(R.color.black));
                    speed_dating_credit_progress.setTitle("" + Integer.parseInt(enrollEventResponse.getEvent_amount()));

                }

                @Override
                public void onAnimationFinish() {
                    speed_dating_credit_progress.setSubTitle("");
                }
            });
        }

    }


    /******************
     * Buy Session
     ************/

    private void buyEvent(String user_id, String event_id, String amount) {

        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();


        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<EnrollEventResponse> getBuySession = apiInterface.getBuyEvent(user_id, event_id, amount);
            getBuySession.enqueue(new Callback<EnrollEventResponse>() {
                @Override
                public void onResponse(Call<EnrollEventResponse> call, Response<EnrollEventResponse> response) {

                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(new Intent(getApplicationContext(), AddVideoBioActivity.class));
                        } else if (response.body().getStatus().equals(RESPONSE_ERROR)) {
                            startActivity(new Intent(getApplicationContext(), AddVideoBioActivity.class));
                            finish();
                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EnrollEventResponse> call, Throwable t) {

                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });


        } catch (Exception e) {

            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }


    }

}
