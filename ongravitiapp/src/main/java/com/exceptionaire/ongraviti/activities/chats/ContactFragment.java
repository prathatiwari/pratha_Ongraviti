package com.exceptionaire.ongraviti.activities.chats;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.myorbit.AddPeopleToOrbitActivity;
import com.exceptionaire.ongraviti.activities.myorbit.MyOrbitActivity;
import com.exceptionaire.ongraviti.activities.widget.ContactListAdapter;
import com.exceptionaire.ongraviti.activities.widget.ContactListView;
import com.exceptionaire.ongraviti.core.AppGlobals;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.OrbitContact;
import com.exceptionaire.ongraviti.model.ResponseOrbitContactList;
import com.exceptionaire.ongraviti.model.UserData;
import com.exceptionaire.ongraviti.quickblox.core.utils.Toaster;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ContactFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactFragment extends Fragment {
    private ListView m_lstContact = null;
    private ContactListAdapter contactListAdapter = null;
    private UserData m_curUser = null;
    private boolean m_inSearchMode = false;

    private ProgressDialog progressDialog;
    private ArrayList<OrbitContact> orbitFriends = new ArrayList<>();
    private ArrayList<OrbitContact> orbitFriendsFilters = new ArrayList<>();

    public ContactFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ContactFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactFragment newInstance() {
        ContactFragment fragment = new ContactFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        m_lstContact = (ListView) view.findViewById(R.id.list_view_contact);
        contactListAdapter = new ContactListAdapter(getContext(), R.layout.item_contact, orbitFriends);
        m_lstContact.setFastScrollEnabled(true);
        m_lstContact.setAdapter(contactListAdapter);

        getOrbitFriendList();
        return view;
    }

    private void getOrbitFriendList() {
        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(getActivity(), "Loading...");
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<ResponseOrbitContactList> retrofitCall = requestInterface.getUserOrbit(AppSettings.getLoginUserId());
        retrofitCall.enqueue(new Callback<ResponseOrbitContactList>() {
            @Override
            public void onResponse(Call<ResponseOrbitContactList> call, retrofit2.Response<ResponseOrbitContactList> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                try {
                    Log.e("TAG", response + " ");
                    if (response.body().getStatus().equalsIgnoreCase("success")) {

                        orbitFriends = new ArrayList<>(Arrays.asList(response.body().getOrbit_list()));
                        AppGlobals.mAllUserData.clear();
                        for (OrbitContact orbitContact : orbitFriends) {
                            if (orbitContact.getQuickblox_id() != null && !orbitContact.getQuickblox_id().isEmpty())
                                try {
                                    int id = Integer.parseInt(orbitContact.getQuickblox_id());
                                    Log.i("QB USER PARSE SUCCESS", "" + orbitContact.getOrbit_name());
                                    AppGlobals.mAllUserData.add(orbitContact);
                                } catch (Exception e) {
                                    Log.i("QB USER PARSE FAILED", orbitContact.getQuickblox_id());
                                }
                        }


                        contactListAdapter = new ContactListAdapter(getContext(), R.layout.item_contact, orbitFriends);
                        m_lstContact.setFastScrollEnabled(true);
                        m_lstContact.setAdapter(contactListAdapter);
                        m_lstContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView parent, View v, int position, long id) {
                                progressDialog = new CustomProgressDialog(getActivity(), "Loading...");
                                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                progressDialog.show();

                                List<OrbitContact> searchList = m_inSearchMode ? orbitFriendsFilters : orbitFriends;
                                OrbitContact contact = searchList.get(position);
                                Log.i("hiiiiiii", "" + contact.getOrbit_name());
                                AppSettings.setKeyOrbitName(contact.getOrbit_name());
                                if (contact.getQuickblox_id() != null && !contact.getQuickblox_id().isEmpty()) {
                                    ((ChatsMainActivity) getActivity()).startChatActivity(contact.getQuickblox_id());

                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            // Do something after 5s = 5000ms
                                            progressDialog.dismiss();
                                        }
                                    }, 5000);

                                } else
                                    Toaster.longToast("This user is not registered to chat server");

                            }
                        });

                    } else if (response.body().getStatus().equalsIgnoreCase("error")) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        final Dialog dialog = CommonDialogs.showMessageDialog(getActivity(), getResources().getString(R.string.ongravity), "You don't have any people in your Orbit.");
                        dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                Intent i = new Intent(getActivity(), AddPeopleToOrbitActivity.class);
                                startActivity(i);

                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseOrbitContactList> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });

    }

}
