package com.exceptionaire.ongraviti.activities.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;

import java.util.ArrayList;


public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {
    ArrayList<String> android;
    private Context context;


    public GridAdapter(Context context, ArrayList<String> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public GridAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GridAdapter.ViewHolder viewHolder, int position) {

        Glide.with(context)
                .load(android.get(position))
                .into(viewHolder.img_android);
        viewHolder.img_android.setOnClickListener(view -> {
            new CommonDialogs().displayImageZoomDialog1((Activity) context,
                    android);
//                dialog.dismiss();
        });
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_android;

        public ViewHolder(View view) {
            super(view);

            img_android = view.findViewById(R.id.img_android);
        }
    }

}