package com.exceptionaire.ongraviti.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.CustomPagerAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.SearchableListDialog;
import com.exceptionaire.ongraviti.activities.base.ShowHidePasswordEditText;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppPreference;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.fcm.Config;
import com.exceptionaire.ongraviti.listener_interface.ClickListener;
import com.exceptionaire.ongraviti.model.Country;
import com.exceptionaire.ongraviti.model.LoginResponse;
import com.exceptionaire.ongraviti.model.ProfilePictureResponse;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_activities.OpponentsActivityVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_service.CallServiceVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.SharedPrefsHelperVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.UsersUtilsVideo;
import com.exceptionaire.ongraviti.quickblox.QBData;
import com.exceptionaire.ongraviti.quickblox.core.utils.SharedPrefsHelper;
import com.exceptionaire.ongraviti.quickblox.core.utils.Toaster;
import com.exceptionaire.ongraviti.quickblox.managers.QbDialogHolder;
import com.exceptionaire.ongraviti.quickblox.services.CallService;
import com.exceptionaire.ongraviti.quickblox.utils.chat.ChatHelper;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.exceptionaire.ongraviti.utilities.CustomVolleyRequest;
import com.exceptionaire.ongraviti.utilities.LogUtil;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.session.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.server.Performer;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by bhakti gade on 16/6/16.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener, Response.Listener, Response.ErrorListener, AppConstants {
    private QBUser QuickBloxUser;
    // declaring variables
    private AutoScrollViewPager autoScrollViewPager;
    private LinearLayout linearCountDots;
    private EditText edtContectNumber;
    private ShowHidePasswordEditText edt_password;
    private Button btnLogin;
    LoginButton btnLoginWithFB;
    private String flag, mobile, password, status, user_id, message, imei_number, profile_status, facebook_id, name, profile_picure, email, birthdate, gender, contact_number;
    private String device_confirm_msg = "Yes";
    private TextView txtForgotPassword, signupNow, txtCountryCode;
    private JSONObject json;
    private String FIELDS = "fields";
    private String ID = "id";
    private String NAME = "name";
    private String PICTURE = "picture";
    private String EMAIL = "email";
    private String BIRTHDAY = "birthday";
    private String GENDER = "gender";
    private String KEY_USERNAME = "email_address";
    private String KEY_PASSWORD = "password";
    private String REQUEST_FIELDS = TextUtils.join(",", new String[]{ID, NAME, PICTURE, EMAIL, BIRTHDAY, GENDER});
    private SharedPreferences.Editor edit;
    long TIME = 10 * 1000;
    private int WESERVICE_FLAG;
    private RequestQueue requestQueue;
    private CustomVolleyRequest customVolleyRequest;
    private ProgressDialog progressDialog;
    private ProgressDialog progressDialog1;
    private String emailID;
    Dialog dialog;
    private List<Country> countryList;
    private SearchableListDialog _searchableListDialog;
    private SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<>();
    private int dotsCount;
    private ImageView[] dots;
    private CustomPagerAdapter mAdapter;
    boolean doubleBackToExitPressedOnce = false;
    private QBUser userForSave;
    private CallbackManager callbackManager;
    Button basic_registration_btn_fb_login;
    EditText editTextSocialEmailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            FacebookSdk.sdkInitialize(getApplicationContext());
            setContentView(R.layout.activity_login);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            init();
            initListners();
            basic_registration_btn_fb_login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    basic_registration_btn_fb_login.setEnabled(false);
                    basic_registration_btn_fb_login.postDelayed(new Runnable() {
                        public void run() {
                            basic_registration_btn_fb_login.setEnabled(true);
                        }
                    }, 6000);
                    if (isNetworkAvailable()) {
                        if (progressDialog == null) {
                            progressDialog = new CustomProgressDialog(LoginActivity.this, getResources().getString(R.string.loading));
                            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            progressDialog.show();
                        }

                        btnLoginWithFB.performClick();
                    } else {

                        Toast.makeText(LoginActivity.this, R.string.error_no_internet_connection, Toast.LENGTH_LONG).show();
                    }
                }
            });
            btnLoginWithFB.setReadPermissions("public_profile email");
            if (AccessToken.getCurrentAccessToken() != null) {
                RequestData();
            }
            btnLoginWithFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LoginManager.getInstance().logOut();
                }
            });
            btnLoginWithFB.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    requestUserInfo(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }

                @Override
                public void onError(FacebookException e) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });

            //Login manager for login with facebook
//            LoginManager.getInstance().registerCallback(
//                    callbackManager,
//                    new FacebookCallback<LoginResult>() {
//                        @Override
//                        public void onSuccess(LoginResult loginResult) {
//                            requestUserInfo(loginResult.getAccessToken());
//                        }
//
//                        @Override
//                        public void onCancel() {
//                            Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
//                        }
//
//                        @Override
//                        public void onError(FacebookException error) {
//                            // TODO Auto-generated method stub
//                        }
//                    });
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        JSONObject json = response.getJSONObject();
                        System.out.println("Json data :" + json);
                        try {
                            if (json != null) {
//                                profile.setProfileId(json.getString("id"));
                                Log.d("data", json.getString("id"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onStart() {
        try {
            super.onStart();
            autoScrollViewPager.startAutoScroll();
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        try {
            super.onStop();
            autoScrollViewPager.stopAutoScroll();
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void init() {
        try {
            callbackManager = CallbackManager.Factory.create();
            // find view by id all views
            basic_registration_btn_fb_login = (Button) findViewById(R.id.basic_registration_btn_fb_login);
            autoScrollViewPager = (AutoScrollViewPager) findViewById(R.id.login_view_pager);
            linearCountDots = (LinearLayout) findViewById(R.id.linearCountDots);
            edt_password = (ShowHidePasswordEditText) findViewById(R.id.login_edt_password);
            edtContectNumber = (EditText) findViewById(R.id.login_edt_contact_number);
            btnLogin = (Button) findViewById(R.id.login_btn_login);
            btnLoginWithFB = (LoginButton) findViewById(R.id.login_btn_fb_login);
            signupNow = (TextView) findViewById(R.id.login_btn_signUpNow);
            txtForgotPassword = (TextView) findViewById(R.id.login_txt_forgot_PW);
            txtCountryCode = (TextView) findViewById(R.id.login_txt_country_code);
            //edt_password.setHintTextColor(getResources().getColor(R.color.OnGravitiGray));
            edt_password.setFilters(new InputFilter[]{AppUtils.ignoreFirstWhiteSpace()});
            appPreference = new AppPreference(this);

            edt_password.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (edt_password.getText().length() > 0) {
                        edt_password.showPasswordVisibilityIndicator(true);
                    } else {
                        edt_password.showPasswordVisibilityIndicator(false);
                    }
//                    edt_password.showPasswordVisibilityIndicator(true);

                    Log.d(LOG_TAG, "## onTouch : ");
                    return false;
                }
            });
            new AsyncPhoneInitTask(this).execute();
            // get GCM ID
            getRegistrationId();
            // get IMEI number of device
            getDeviceID();
            mAdapter = new CustomPagerAdapter(LoginActivity.this);
            autoScrollViewPager.setAdapter(mAdapter);
            autoScrollViewPager.setInterval(5000);
            autoScrollViewPager.setBorderAnimation(false);
            setUiPageViewController();
            autoScrollViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.bullet_d));
                    }
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.bullet_a));
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void initListners() {
        try {
            // set click listners to all clickable views
            btnLogin.setOnClickListener(this);
            btnLoginWithFB.setOnClickListener(this);
            signupNow.setOnClickListener(this);
            txtForgotPassword.setOnClickListener(this);


            txtCountryCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txtCountryCode.setEnabled(false);
                    Timer buttonTimer = new Timer();
                    buttonTimer.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    txtCountryCode.setEnabled(true);
                                }
                            });
                        }
                    }, 4000);

                    _searchableListDialog = SearchableListDialog.newInstance(countryList, new ClickListener() {
                        @Override
                        public void onClick(Country country) {
                            txtCountryCode.setText(String.valueOf(country.getCountryCode()));
                            //country_code = String.valueOf(country.getCountryCode());
                            //country_iso = country.getCountryISO();

                        }
                    });
                    _searchableListDialog.show((LoginActivity.this).getFragmentManager(), "LOG_TAG");
                }
            });
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    // all views on click
    @Override
    public void onClick(View view) {
        try {
            if (view == btnLogin) {
                mobile = edtContectNumber.getText().toString();
                password = edt_password.getText().toString();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        btnLogin.setEnabled(true);
                    }
                }, TIME);
                if (isNetworkAvailable()) {
                    if (validateForm(mobile, password)) {
                        Log.d("AppSettings.getQuic", AppSettings.getQuickBloxUserID() + "");

                        imei_number = getDeviceID();
                        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                        String regId = pref.getString("regId", null);
                        //TODO POT COUNTRY CODE WITH LOGIN
                        doLogin(mobile, password, imei_number, regId, "yes", txtCountryCode.getText().toString());
                    }
                } else {
                    CommonDialogs.showMessageDialog(LoginActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                }

            }
            if (view == signupNow) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        signupNow.setEnabled(true);
                    }
                }, TIME);
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                finish();
            }
            if (view == txtForgotPassword) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        txtForgotPassword.setEnabled(true);
                    }
                }, TIME);

                // set the custom dialog components - text, image and button
                final EditText edt_email_id;
                final Button btnSubmit;
                final Button close;

                dialog = new Dialog(LoginActivity.this, R.style.DialogStyle);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_forgot_password);

                edt_email_id = (EditText) dialog.findViewById(R.id.edtEmailForgotPassword);
                btnSubmit = (Button) dialog.findViewById(R.id.button_ok);
                close = (Button) dialog.findViewById(R.id.button_cancel);

                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                ((TextView) dialog.findViewById(R.id.text_view_dialog_header)).setText("Forgot Password");

                dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                // if button is clicked
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emailID = edt_email_id.getText().toString();
                        if (isNetworkAvailable()) {
                            edt_email_id.setError(null);

                            String EMAIL_PATTERN = AppConstants.REGEX_EMAIL;
                            if (emailID.length() == 0) {
                                setErrorMsg("Please Enter Email ID", edt_email_id, true);
                            } else if (!emailID.matches(EMAIL_PATTERN)) {
                                setErrorMsg(getResources().getString(R.string.valid_email), edt_email_id, true);
                            } else {

                                userForgotPassword();
                            }

                        } else {
                            CommonDialogs.showMessageDialog(LoginActivity.this, getResources().getString(R.string.ongraviti), getResources().getString(R.string.check_internet)).show();
                        }
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
//            if (view == btnLoginWithFB) {
//
//                btnLoginWithFB.setEnabled(false);
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        btnLoginWithFB.setEnabled(true);
//                    }
//                }, TIME);
////            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList());
//                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_friends", "email", "public_profile"));
//
//            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void UpdateQuickBloxUser() {
        QuickBloxUser = new QBUser();
        QuickBloxUser.setId(AppSettings.getQuickBloxUserID());

        QuickBloxUser.setPassword(password);

        QBUsers.updateUser(QuickBloxUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
            }

            @Override
            public void onError(QBResponseException errors) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    //validation for email id and password(edittexts)
    public boolean validateForm(String mobile, String password) {
        boolean valid = true;
        try {
            edtContectNumber.setError(null);
            edt_password.setError(null);
            String PHONE = AppConstants.REGEX_PHONE_NUMBER;
            if (mobile.length() == 0) {
                setErrorMsg("Enter Mobile Number", edtContectNumber, true);
                valid = false;
            } else if (!mobile.matches(PHONE)) {
                setErrorMsg("Enter Valid Mobile Number", edtContectNumber, true);
                valid = false;
            } else if (password.length() == 0) {
                setErrorMsg("Enter Password", edt_password, true);
                valid = false;
            } else if (password.contains("\\s")) {
                setErrorMsg("blank space", edt_password, true);
                valid = false;
            } else if (password.length() < 6) {
                ShowHidePasswordEditText.setErrorMsg("Password Length To Small", edt_password, true);
                valid = false;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return valid;
    }

    // get the data of user when pressed login with facebook
    private void requestUserInfo(AccessToken accessToken) {

        try {
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object,
                                        GraphResponse response) {
                    // TODO Auto-generated method stub
                    json = response.getJSONObject();
                    try {
//                        Log.e("h", json.toString());

                        name = json.getString("name");
                        gender = json.getString("gender");
                        facebook_id = json.getString("id");
                        if (facebook_id != null)
                            profile_picure = "https://graph.facebook.com/" + facebook_id + "/picture?width=2000&height=2200";//json.getJSONObject("picture").getJSONObject("data").getString("url");
                        Log.e("name", name);
                        Log.e("facebook_id", facebook_id);
                        Log.e("profile_picure", "https://graph.facebook.com/" + facebook_id + "/picture?width=1400&height=1500");
                        if (json.has("email") && json.getString("email") != null && !json.getString("email").equals("") && !json.getString("email").equals(" ")) {
                            try {
                                email = json.getString("email");
                                imei_number = getDeviceID();
                                SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                                String regId = pref.getString("regId", null);
//                                quickbloxSignUp(name, contact_number, email);

                                doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg, "1");
//
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        } else {

                            String fbId = json.getString("id");

                            if (AppSettings.getFBUserIds() != null) {

                                if (!AppSettings.getFBUserIds().equals(fbId)) {
                                    dialogSocialLogin(fbId);
                                } else {

                                    if (AppSettings.getFbUserEmailWithMobileLogin() != null) {
                                        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                                        String regId = pref.getString("regId", null);
                                        doLoginFB(AppSettings.getFbUserEmailWithMobileLogin(), name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg, "0");
                                    }
                                }

                            } else {
                                dialogSocialLogin(fbId);

                            }

                           /* if (!AppSettings.getFBUserIds().equals(facebook_id))*/

                        }

                        imei_number = getDeviceID();
                        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                        String regId = pref.getString("regId", null);

                        //  doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg);

                    } catch (JSONException e) {
                        /*if (AppSettings.isFBLogin()) {
                            email = AppSettings.getFBEmail();
                            contact_number = AppSettings.getKeyUserProfileContactNumber();
                            //if get all fields from facebook then user will login to directly application
                            imei_number = getDeviceID();
                            SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                            String regId = pref.getString("regId", null);
                            // doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg);
                        } else {
                            //if facebook will not provide users email for that time we will ask for enter email manually
                            Intent intent = new Intent(LoginActivity.this, GetUserEmail.class);
                            intent.putExtra("name", name);
                            intent.putExtra("profile_pic_url", profile_picure);
                            intent.putExtra("gender", gender);
                            intent.putExtra("facebook_user_id", facebook_id);
                            intent.putExtra("email", email);
                            startActivity(intent);
                        }*/
                    }
                }
            });
            Bundle parameters = new Bundle();
            parameters.putString(FIELDS, REQUEST_FIELDS);
            request.setParameters(parameters);
            request.executeAsync();
            ///get facebook friends
            new GraphRequest(
                    accessToken,
                    /// AccessToken.getCurrentAccessToken(),
                    "/me/invitable_friends",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
            /* handle the result */
                        }
                    }
            ).executeAsync();
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void dialogSocialLogin(String fbId) {
        final Dialog socialLogin = new CommonDialogs().socialLogin(LoginActivity.this,
                getResources().getString(R.string.ongraviti));
        editTextSocialEmailId = (EditText) socialLogin.findViewById(R.id.edtEmailForgotPassword);
        socialLogin.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!editTextSocialEmailId.getText().toString().trim().matches(REGEX_EMAIL)) {
                    setErrorMsg(getResources().getString(R.string.valid_email), editTextSocialEmailId, true);
                } else {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    String regId = pref.getString("regId", null);
                    AppSettings.setFBUserIds(fbId);
                    AppSettings.setFbUserEmailWithMobileLogin(editTextSocialEmailId.getText().toString());
                    doLoginFB(editTextSocialEmailId.getText().toString(), name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg, "0");

                    //Call api for checking social user id already existing or not
                    // socialUserExisting(AppSharedPreferences.getSocialId(), editTextSocialEmailId.getText().toString());
                    socialLogin.dismiss();
                }
            }
        });
        socialLogin.findViewById(R.id.button_cancel).

                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        socialLogin.dismiss();
                    }
                });

    }

    // method for forgot password with volley common request
    private void userForgotPassword() {
        try {
            WESERVICE_FLAG = 3;
            showProgressBar();
            Map<String, String> map = new HashMap<>();
            map.put("email_address", emailID);
            requestQueue = Volley.newRequestQueue(LoginActivity.this);
            customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.URL_FORGOT_PASSWORD, map, this, this);
            requestQueue.add(customVolleyRequest);
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    // blank all edittext after successful login
    private void blankEdittext() {
        try {
            edt_password.setText("");
            edtContectNumber.setText("");
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press once again to exit the app", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    //hide keyboard after touch any where on screen
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            View v = getCurrentFocus();

            if (v != null &&
                    (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                    v instanceof EditText &&
                    !v.getClass().getName().startsWith("android.webkit.")) {
                int scrcoords[] = new int[2];
                v.getLocationOnScreen(scrcoords);
                float x = ev.getRawX() + v.getLeft() - scrcoords[0];
                float y = ev.getRawY() + v.getTop() - scrcoords[1];

                if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                    AppUtils.hideKeyboard(this);
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return super.dispatchTouchEvent(ev);
    }


    private void getRegistrationId() {

        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = "";
            if (instanceID != null)
                token = instanceID.getToken("ongraviti-151006",
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            Log.i(LOG_TAG, "GCM Registration Token: " + token);

        } catch (Exception e) {
            Log.d(LOG_TAG, "Failed to complete token refresh", e);
        }
    }

    // get IMEI number of device
    private String getDeviceID() {
        String deviceId = null;
        try {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();
            Log.e(LOG_TAG, countryCodeValue + " ");
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = telephonyManager.getDeviceId();
            Log.e(LOG_TAG, deviceId + " ");
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return deviceId;
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d(LOG_TAG, "## onErrorResponse : " + error.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            error.printStackTrace();
            String errorMessage = getString(R.string.error_connection_generic);
            if (error instanceof NoConnectionError)
                errorMessage = getString(R.string.error_no_connection);
            else if (error instanceof AuthFailureError)
                errorMessage = getString(R.string.error_authentication);
            else if (error instanceof NetworkError)
                errorMessage = getString(R.string.error_network_error);
            else if (error instanceof ParseError)
                errorMessage = getString(R.string.error_parse_error);
            else if (error instanceof ServerError)
                errorMessage = getString(R.string.error_server_error);
            else if (error instanceof TimeoutError)
                errorMessage = getString(R.string.error_connection_timeout);
            Toast.makeText(LoginActivity.this, errorMessage, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        JSONObject jObject;

        try {
            jObject = new JSONObject(String.valueOf(response));
            if (WESERVICE_FLAG == 3) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                status = jObject.getString("status");
                message = jObject.getString("msg");
                System.out.println(message);

                if (status.equals("success")) {
//                    UpdateQuickBloxUser();
//                    setQuickbloxIDToGravitiServer(String.valueOf(QuickBloxUser.getId()), String.valueOf(QuickBloxUser.getFullName()), String.valueOf(QuickBloxUser.getPassword()));
                    dialog.dismiss();
                    CommonDialogs.showMessageDialog(LoginActivity.this, getResources().getString(R.string.ongravity), message).show();

                } else if (status.equalsIgnoreCase("error")) {
                    CommonDialogs.showMessageDialog(LoginActivity.this, getResources().getString(R.string.ongravity), message).show();

                }
            } else if (WESERVICE_FLAG == 4) {
                if (jObject.has("result")) {
                    String result = jObject.getString("result");
                    if (result.equals("1")) {
                        progressDialog1 = new CustomProgressDialog(this, getResources().getString(R.string.loading));
                        progressDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        progressDialog1.show();
//                        QuickBloxUser.setPassword(AppSettings.getQuickPassword());
                        loginToChat(QuickBloxUser);/*
                        if(QuickBloxUser.getPassword()!=null) {
                            loginToChat(QuickBloxUser);
                        }else {
                            regWithNoPassword();
                        }*/

//                    loginToChatVideo(QuickBloxUser);

                        Toast.makeText(this, "Quick Blox Registration.", Toast.LENGTH_SHORT).show();
                        //doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, "124567899", device_confirm_msg);
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                progressDialog1.dismiss();
                            }
                        }, 4000);

                    } else {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setUiPageViewController() {
        try {
            dotsCount = mAdapter.getCount();
            dots = new ImageView[dotsCount];

            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.bullet_d));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(4, 0, 4, 0);

                linearCountDots.addView(dots[i], params);
            }

            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.bullet_a));
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {

        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<Country>(233);
            BufferedReader reader = null;
            TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    final Country c = new Country(mContext, line, i);
                    data.add(c);
                    if (c.getCountryISO().toLowerCase().trim().equals(countryCodeValue.toLowerCase().trim())) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                txtCountryCode.setText(String.valueOf(c.getCountryCode()));
                            }
                        });
                    }
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            countryList = data;
        }
    }

    //TODO PUT COUNTRY CODE WITH LOGIN
    private void doLogin(String contact_number, final String password, String imeiNumber, String device_token_id, String deviceConfirm_msg, String phonecode) {
        if (progressDialog == null || !progressDialog.isShowing()) {
            progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<LoginResponse> loginCall = loginRequest.login(contact_number, password, device_token_id, deviceConfirm_msg, "android", phonecode);
        loginCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {

                if (response != null && response.body() != null) {


                    if (response.body().getMsg().equals("You have successfully login.")) {
                        AppSettings.setLoginTypePatchForQuickEnter(AppConstants.LOGIN_TYPE_FOR_QUICK_NORMAL);
                        blankEdittext();
                        Log.e(LOG_TAG, "--- User Id : " + response.body().getUser());
                        AppSettings.setProfileDefaultCategory(response.body().getUser().getProfile_cat_id());
                        AppSettings.setLoginUserId(response.body().getUser().getId());
                        AppSettings.setUserProfileEmail(response.body().getUser().getEmail_address());
                        AppSettings.setKeyUserProfileContactNumber(response.body().getUser().getFull_contact_number());
                        AppSettings.setUserProfileName(response.body().getUser().getC_name());
                        AppSettings.setLoginUserPassword(password);
                        AppSettings.setProfileDefaultCategory(response.body().getUser().getProfile_cat_id());
                        if (response.body().getUser().getProfile_cat_id() != null && !response.body().getUser().getProfile_cat_id().isEmpty() && !response.body().getUser().getProfile_cat_id().equals("")) {
                            AppSettings.setProfileDefaultCategory(response.body().getUser().getProfile_cat_id());
                            if (response.body().getUser().getProfile_cat_id().equals(AppConstants.PROFILE_CATEGORY_ONE_SOCIAL)) {
                                if (response.body().getUser().getProfile_picture() != null) {
                                    AppSettings.setProfileCatPicPathOne(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getUser().getProfile_picture());
                                } else if (AppSettings.getProfileCatPicPathFB() != null) {
                                    AppSettings.setProfileCatPicPathOne(AppSettings.getProfileCatPicPathFB());
                                }
                            } else if (response.body().getUser().getProfile_cat_id().equals(AppConstants.PROFILE_CATEGORY_TWO_DATING)) {
                                AppSettings.setProfileCatPicPathTwo(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getUser().getProfile_picture());
                            } else if (response.body().getUser().getProfile_cat_id().equals(AppConstants.PROFILE_CATEGORY_THREE_BUSINESS)) {
                                AppSettings.setProfileCatPicPathThree(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getUser().getProfile_picture());
                            }
                        }

                        final Boolean result = response.body().getProfile_status().equalsIgnoreCase("Yes");
                        try {
                            if (response.body().getUser().getQuickblox_id() != null && !response.body().getUser().getQuickblox_id().isEmpty()) {
                                Performer<QBUser> qbUserPerformer = QBUsers.getUser(Integer.parseInt(response.body().getUser().getQuickblox_id()));
                                qbUserPerformer.performAsync(new QBEntityCallback<QBUser>() {
                                    @Override
                                    public void onSuccess(QBUser qbUser, Bundle bundle) {
                                        qbUser.setPassword(response.body().getUser().getQuickblox_password());
                                        saveUserData(qbUser);
                                        QBData.curQBUser = qbUser;
                                        //TODO Uncomment if there is some problem in video chating
//                                        saveUserDataVideo(qbUser);
                                        createSession(result);
                                    }

                                    @Override
                                    public void onError(QBResponseException e) {
                                        if (progressDialog != null && progressDialog.isShowing())
                                            progressDialog.dismiss();
                                        AppSettings.setLogin(false);
                                        new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongraviti), "you are not registered to chat server.");
                                    }
                                });
                            } else {
                                if (progressDialog != null && progressDialog.isShowing())
                                    progressDialog.dismiss();
                                AppSettings.setLogin(false);
                                new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongravity), "User is not registered to QuickBlox server properly while verifying the OTP");
                            }
                        } catch (Exception e) {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            AppSettings.setLogin(false);
                            new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongraviti), "you are not registered to chat server.");
                        }
                    } else if (response.body().getMsg().equals("Different mobile device exists.")) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        device_confirm_msg = "Yes";
                        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                        final String regId = pref.getString("regId", null);
                        //TODO PUT COUNTRY CODE WITH LOGIN
                        doLogin(mobile, LoginActivity.this.password, imei_number, regId, device_confirm_msg, txtCountryCode.getText().toString());

                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    private void doLoginFB(String emailId, String user_name, String profile_pic_url, String contactNumber, String facebook_user_id, String imeiNumber, String device_token_id, String deviceConfirm_msg, String is_fb) {
        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        try {

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<LoginResponse> fbLoginCall = requestInterface.fbLogin(emailId, user_name, profile_pic_url, contactNumber, facebook_user_id, device_token_id, deviceConfirm_msg, "android", is_fb);
            fbLoginCall.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {

                    try {
                        //response.body().getUser().getQuickblox_id()
                        //Convert Fb url to file
                        URL url = new URL(profile_pic_url);
                        File f = new File(url.getFile());
                        device_confirm_msg = "yes";
                        Log.e(LOG_TAG, response + " ");
                        status = response.body().getStatus();
                        message = response.body().getMsg();
                        if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {

                            AppSettings.setLoginTypePatchForQuickEnter(AppConstants.LOGIN_TYPE_FOR_QUICK_FB);
                            if (response.body().getUser().getQuickblox_id() == null || response.body().getUser().getQuickblox_id() == "") {
                                AppSettings.setLoginUserId(response.body().getUser().getId());
                                if (email == null) {
                                    email = editTextSocialEmailId.getText().toString();
                                }
                                quickbloxSignUp(name, contact_number, email);

                            } else {
//                                setQuickbloxIDToGravitiServer(String.valueOf(QuickBloxUser.getId()), String.valueOf(QuickBloxUser.getFullName()), password);
                            }

                            if (response.body().getMsg().equalsIgnoreCase("You have successfully login.")) {
                                blankEdittext();
                                Log.e(LOG_TAG, "--- User Id : " + response.body());
                                AppSettings.setRegistrationStatus(response.body().getregistration_status());
                                AppSettings.setProfileDefaultCategory(response.body().getUser().getProfile_cat_id());
                                if (AppSettings.getRegistrationStatus().equals("yes")) {
                                    Log.e(LOG_TAG, ":::::registration needed");
                                    AppSettings.setUserProfileName(user_name);
                                    if (response.body().getUser().getProfile_picture() != null) {
                                        AppSettings.setProfileCatPicPathOne(response.body().getUser().getProfile_picture());
                                    } else {
                                        AppSettings.setProfileCatPicPathOne(profile_pic_url);
                                    }
                                    AppSettings.setLoginUserId(response.body().getUser().getId());
                                    AppSettings.setUserProfileEmail(response.body().getUser().getEmail_address());
                                    AppSettings.setKeyUserProfileContactNumber(response.body().getUser().getFull_contact_number());
                                    AppSettings.setLogin(true);
                                    AppSettings.setFBUserId(response.body().getUser().getEmail_address());
                                    AppSettings.setFBLogin(true);
                                    AppSettings.setProfileCatPicPathFB(profile_pic_url);


//                                    startActivity(new Intent(LoginActivity.this, EditProfileDetailsActivity.class));
//                                    finish();
                                    //TODO need status for call this

                                } else {
                                    Log.e(LOG_TAG, ":::::login needed");
                                    AppSettings.setUserProfileName(response.body().getUser().getC_name());
                                    if (response.body().getUser().getProfile_picture() != null) {
                                        AppSettings.setProfileCatPicPathOne(response.body().getUser().getProfile_picture());
                                    } else {
                                        AppSettings.setProfileCatPicPathOne(profile_pic_url);
                                    }
                                    AppSettings.setLoginUserId(response.body().getUser().getId());
                                    AppSettings.setUserProfileEmail(response.body().getUser().getEmail_address());
                                    AppSettings.setKeyUserProfileContactNumber(response.body().getUser().getFull_contact_number());
                                    AppSettings.setLogin(true);
                                    AppSettings.setFBUserId(response.body().getUser().getEmail_address());
                                    AppSettings.setFBLogin(true);
//                                    startActivity(new Intent(LoginActivity.this, HomeUserPostsActivity.class));
//                                    finish();
//                                    setQuickbloxIDToGravitiServer(String.valueOf(QuickBloxUser.getId()), String.valueOf(QuickBloxUser.getFullName()), password);

//                                    QBUserSingUp(QuickBloxUser);
                                    final Boolean result = response.body().getProfile_status().equalsIgnoreCase("Yes");

                                    Log.e(LOG_TAG, ":::::login befor try");
                                    try {
                                        if (response.body().getUser().getQuickblox_id() != null
                                                && !response.body().getUser().getQuickblox_id().isEmpty()) {
                                            Log.e(LOG_TAG, ":::::login inside quickblox login");
                                            Performer<QBUser> qbUserPerformer = QBUsers.getUser(Integer.parseInt(response.body().getUser().getQuickblox_id()));
                                            qbUserPerformer.performAsync(new QBEntityCallback<QBUser>() {
                                                @Override
                                                public void onSuccess(QBUser qbUser, Bundle bundle) {

                                                    Log.e(LOG_TAG, ":::::response.body" + response.body());
                                                    Log.e(LOG_TAG, ":::::login inside quickblox onsuccess");
                                                    if (qbUser.getPassword() == null) {
                                                        Log.e(LOG_TAG, ":::::login inside quickblox onsuccess pwd null");
                                                        //TODO
                                                      /*  if (AppSettings.getQuickPassword() != null) {
                                                            qbUser.setPassword(AppSettings.getQuickPassword());
                                                        } else {*/
                                                        qbUser.setPassword(response.body().getUser().getQuickblox_password());// Take password pfrom server
                                                        Log.e("password_quickblox", response.body().getUser().getQuickblox_password());
                                                        AppSettings.setQuickPassword(response.body().getUser().getQuickblox_password());
//                                                        }
                                                    }
                                                    //  qbUser.setPassword(password);
                                                    saveUserData(qbUser);
                                                    QBData.curQBUser = qbUser;
                                                    QuickBloxUser = qbUser;


                                                    Gson gson = new Gson();
                                                    String json = gson.toJson(QuickBloxUser);
                                                    AppSettings.setFbQuickUserPatch(json);
                                                    Log.i("My Qb user", AppSettings.getFbQuickUserPatch());

                                                    String s = AppSettings.getFbQuickUserPatch();
                                                    //TODO Uncomment if there is some problem in video chating
//                                        saveUserDataVideo(qbUser);
                                                    if (QuickBloxUser.getPassword() != null) {
                                                        Log.e(LOG_TAG, ":::::pwd not null");
                                                        loginToChat(QuickBloxUser);
                                                    } else {
                                                        Log.e(LOG_TAG, ":::::pwd null");
                                                        Toast.makeText(LoginActivity.this, "Password Not Found", Toast.LENGTH_SHORT).show();
                                                    }


//                                                    createSession(result);
                                                }

                                                @Override
                                                public void onError(QBResponseException e) {

                                                    Log.e(LOG_TAG, ":::::quickblox login error");
                                                    if (progressDialog != null && progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                    AppSettings.setLogin(false);
                                                    new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongraviti), "you are not registered to chat server.");
                                                }
                                            });
                                        } else {
                                            if (progressDialog != null && progressDialog.isShowing())
                                                progressDialog.dismiss();
                                            AppSettings.setLogin(false);
                                            new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongravity), "You are not registered to QuickBlox server properly while Login with facebook.");
                                        }
                                    } catch (Exception e) {
                                        if (progressDialog != null && progressDialog.isShowing())
                                            progressDialog.dismiss();
                                        AppSettings.setLogin(false);
                                        new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongraviti), "you are not registered to chat server.");
                                    }

                                }

//                                setQuickbloxIDToGravitiServer(String.valueOf(QuickBloxUser.getId()), String.valueOf(QuickBloxUser.getFullName()), password);

                            } else if (response.body().getMsg().equalsIgnoreCase("Different mobile device exists.")) {
                                if (progressDialog != null && progressDialog.isShowing())
                                    progressDialog.dismiss();
                                device_confirm_msg = "Yes";
                                SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                                final String regId = pref.getString("regId", null);
                                if (email != null) {
                                    doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg, "1");
                                } else {
                                    doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg, "0");
                                }

                            }


                        } else if (response.body().getStatus().equalsIgnoreCase("error")) {

                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
                            AppSettings.setFBUserIds(null);
                            //if account is inactive flag will 0 ==== opening pin view
                            flag = response.body().getFlag();
                            if (Integer.parseInt(flag) == 0) {
                                Intent pinIntent = new Intent(LoginActivity.this, OTPEntryView.class);
                                pinIntent.putExtra("contact_number", edtContectNumber.getText().toString());
                                blankEdittext();
                                startActivity(pinIntent);
                            } else {
                                final Dialog dialog = CommonDialogs.showMessageDialog(LoginActivity.this, getResources().getString(R.string.ongravity), response.body().getMsg());
                                dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    /*===========================QB Code - CHAT LOGIN ==========================*/

    private QBUser quickbloxUser = null;
    private AppPreference appPreference;

    private void createSession(Boolean goToProfile) {

        QBChatService chatService = QBChatService.getInstance();
        chatService.setDebugEnabled(true);

        QBChatService.ConfigurationBuilder chatserviceConfigurationBuilder = new QBChatService.ConfigurationBuilder();
        chatserviceConfigurationBuilder.setSocketTimeout(100000);
        QBChatService.setConfigurationBuilder(chatserviceConfigurationBuilder);

        QBAuth.createSession().performAsync(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession result, Bundle params) {

               /* if (QuickBloxUser != null) {
                    quickbloxUser = QuickBloxUser;
                } else {

                }*/
                if (getQBUserData() != null) {

                    quickbloxUser = getQBUserData();
                } else {
                    quickbloxUser = QuickBloxUser;
                }
                if (quickbloxUser != null) {
                    AppSettings.setQuickBloxUserID(quickbloxUser.getId());
//                    Toast.makeText(LoginActivity.this, "Session Created by P", Toast.LENGTH_SHORT).show();
                } else if (quickbloxUser == null) {
                    Toaster.longToast("QuickBlox user is not created while sign up...");
                    return;
                }
                startLoginService(quickbloxUser);
            }

            @Override
            public void onError(QBResponseException e) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                AppSettings.setLogin(false);
                Toaster.longToast(e.getErrors().get(0));
            }
        });
    }

    protected void startLoginService(QBUser qbUser) {
        Intent tempIntent = new Intent(this, CallService.class);
        PendingIntent pendingIntent = createPendingResult(AppConstants.EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
        CallService.start(this, qbUser, pendingIntent);
    }

    //getting login with facebook result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.EXTRA_LOGIN_RESULT_CODE) {
            boolean isLoginSuccess = data.getBooleanExtra(AppConstants.EXTRA_LOGIN_RESULT, false);
            String errorMessage = data.getStringExtra(AppConstants.EXTRA_LOGIN_ERROR_MESSAGE);
            if (isLoginSuccess) {

                Log.d("Login Service ----> ", "In login");
                startLoginServiceVideo(quickbloxUser);
//                saveUserDataVideo(quickbloxUser);
//                signInCreatedUser(quickbloxUser, false);

            } else {
                Toaster.longToast(getString(R.string.login_chat_login_error) + errorMessage);
            }
        } else if (resultCode == EXTRA_LOGIN_RESULT_CODE_VIDEO) {
            /*QBUserSignIn(quickbloxUser);*/
            if (quickbloxUser != null) {
                QBUserSignIn(quickbloxUser);
            } else {
                QBUserSignIn(QuickBloxUser);
            }
        } else {
            try {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            } catch (Exception e) {
                Log.e(LOG_TAG, "## error : " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private void startLoginServiceVideo(QBUser qbUser) {
        Intent tempIntent = new Intent(this, CallServiceVideo.class);
        PendingIntent pendingIntent = createPendingResult(EXTRA_LOGIN_RESULT_CODE_VIDEO, tempIntent, 0);
        CallServiceVideo.start(this, qbUser, pendingIntent);
    }

    private void saveUserDataVideo(QBUser qbUser) {
        SharedPrefsHelperVideo sharedPrefsHelperVideo = SharedPrefsHelperVideo.getInstance();
        sharedPrefsHelperVideo.save(PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
        sharedPrefsHelperVideo.saveQbUser(qbUser);
    }


    private void QBUserSignIn(QBUser user) {
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                if (AppSettings.getLoginTypePatchForQuickEnter().equals(LOGIN_TYPE_FOR_QUICK_FB)) {

                    if (AppSettings.getRegistrationStatus().equals("yes")) {
                        AppSettings.setLogin(true);
                        Intent intent = new Intent(LoginActivity.this, EditProfileDetailsActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        AppSettings.setLogin(true);
                        Intent intent = new Intent(LoginActivity.this, HomeUserPostsActivity.class);
                        startActivity(intent);
                        finish();

                    }

                } else {
                    AppSettings.setLogin(true);
                    Intent intent = new Intent(LoginActivity.this, HomeUserPostsActivity.class);
                    startActivity(intent);
                    finish();

                }


            }

            @Override
            public void onError(QBResponseException e) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                Toaster.longToast(e.getMessage());
            }
        });
    }

    /****************Video Call data**********************/

    /*****************************
     * Video Calling
     **********************************************/


    /*public QBUser createUserWithEnteredData() {
        return createQBUserWithCurrentData(String.valueOf("TestingTwo"),//user name
                String.valueOf("testingtwo@gmail.com"),// user email id
                String.valueOf("ongravity")); // group name
    }

    private QBUser createQBUserWithCurrentData(String userName, String email, String chatRoomName) {
        QBUser qbUser = null;
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(chatRoomName)) {
            StringifyArrayList<String> userTags = new StringifyArrayList<>();
            userTags.add(chatRoomName);

            qbUser = new QBUser();
            qbUser.setFullName(userName);
            qbUser.setEmail(email);
//            qbUser.setLogin(getCurrentDeviceId());
            qbUser.setPassword(DEFAULT_USER_PASSWORD);
            qbUser.setTags(userTags);
        }

        return qbUser;
    }
*/
    public void startSignUpNewUser(final QBUser newUser) {
        requestExecutor.signUpNewUser(newUser, new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser result, Bundle params) {
                        loginToChatVideo(result);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (e.getHttpStatusCode() == ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                            signInCreatedUser(newUser, true);
                        } else {
                            Toast.makeText(LoginActivity.this, R.string.sign_up_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );
    }

    private void loginToChatVideo(final QBUser qbUser) {
        qbUser.setPassword(DEFAULT_USER_PASSWORD);

        userForSave = qbUser;


        //startLoginServiceVideo(qbUser);
    }


    private void signInCreatedUser(final QBUser user, final boolean deleteCurrentUser) {
        requestExecutor.signInUser(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser result, Bundle params) {
                if (deleteCurrentUser) {
                    removeAllUserData(result);
//                    startOpponentsActivity();

                } else {


                    Log.i("Registered User info", user.toString());
                    // Toast.makeText(LoginActivity.this, user.toString(), Toast.LENGTH_SHORT).show();

                    // startOpponentsActivity();
                }
            }

            @Override
            public void onError(QBResponseException responseException) {
                Toast.makeText(LoginActivity.this, R.string.sign_up_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void removeAllUserData(final QBUser user) {
        requestExecutor.deleteCurrentUser(user.getId(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                UsersUtilsVideo.removeUserData(getApplicationContext());
                //  startSignUpNewUser(createUserWithEnteredData());
            }

            @Override
            public void onError(QBResponseException e) {
                Toast.makeText(LoginActivity.this, R.string.sign_up_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void startOpponentsActivity() {
        OpponentsActivityVideo.start(LoginActivity.this, false);


        //  finish();
    }

    /*************************************************/

    public void quickbloxSignUp(String name, String phone, String email) {
        char[] chars1 = "1234567890".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 8; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        String randomPassword = sb1.toString();
        AppSettings.setQuickPassword(randomPassword);

        QuickBloxUser = new QBUser();
//        QuickBloxUser.setLogin(name);
        QuickBloxUser.setPassword(randomPassword);
        password = randomPassword;
        //TODO set user name not contact no
        QuickBloxUser.setFullName(name);
//        QuickBloxUser.setPhone(name);
        QuickBloxUser.setEmail(email);
//      QuickBloxUser.setWebsite(country);
        StringifyArrayList arrayList = new StringifyArrayList();
        arrayList.add(AppConstants.QB_USERS_TAG);
        QuickBloxUser.setTags(arrayList);
        QBUserSingUp(QuickBloxUser);

//            regWithNoPassword();

    }


    private void QBUserSingUp(final QBUser user) {
        LogUtil.writeDebugLog("hi", "QBUserSingUp", "start");
        QBUsers.signUpSignInTask(user).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                Log.e(LOG_TAG, "--- User Id123 : " + qbUser);
                LogUtil.writeDebugLog("hi", "QBUserSingUp", "onSuccess");
                QuickBloxUser = qbUser;
                AppSettings.setQuickBloxUserID(qbUser.getId());
            /*    if (QuickBloxUser.getPassword() == null) {

                }*/


                user.setPassword(AppSettings.getQuickPassword());
//                user.setPassword(password);

//                doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, "124567899", device_confirm_msg);

                QuickBloxUser = user;
                saveUserData(QuickBloxUser);
                QBData.curQBUser = QuickBloxUser;
                Gson gson = new Gson();
                String json = gson.toJson(QuickBloxUser);
                AppSettings.setFbQuickUserPatch(json);
                Log.i("My Qb user", AppSettings.getFbQuickUserPatch());

                String s = AppSettings.getFbQuickUserPatch();


                setQuickbloxIDToGravitiServer(String.valueOf(qbUser.getId()), String.valueOf(qbUser.getFullName()), AppSettings.getQuickPassword());
            }

            @Override
            public void onError(QBResponseException error) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                LogUtil.writeDebugLog("hi", "QBUserSingUp", "onError");
                if (error != null && error.getErrors() != null && error.getErrors().get(0) != null)
                    new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongravity), error.getErrors().get(0));
                else if (error.getMessage() != null)
                    new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongravity), error.getMessage());
                else
                    new CommonDialogs().showMessageDialogNonStatic(LoginActivity.this, getResources().getString(R.string.ongravity), "QBResponseException");
            }
        });
    }

    private void setQuickbloxIDToGravitiServer(String quickbloxID, String quickFullName, String quickbloxPassword) {
        try {
            WESERVICE_FLAG = 4;
            Map<String, String> map = new HashMap<>();
            map.put("user_id", AppSettings.getLoginUserId());
            map.put("quickblox_id", quickbloxID);
            map.put("full_name", quickFullName);
            map.put("quickblox_password", quickbloxPassword);
            SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
            String regId = pref.getString("regId", null);
            if (regId != null)
                map.put("device_token_id", regId);
            else
                map.put("device_token_id", "");


//            String imei_number = getDeviceID();
//            if (imei_number != null)
//                map.put("imei_number", imei_number);
//            else
//                map.put("imei_number", "");

            map.put("device_type", "android");

            requestQueue = Volley.newRequestQueue(LoginActivity.this);
            customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.SET_QUICKBLOX_ID, map, this, this);
            requestQueue.add(customVolleyRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loginToChat(final QBUser qbUser) {
        LogUtil.writeDebugLog("hi", "loginToChat", "start");
        qbUser.setPassword(AppSettings.getQuickPassword());
        userForSave = qbUser;
        startLoginService(qbUser);
        // startLoginServiceVideo(qbUser);

    }


}

