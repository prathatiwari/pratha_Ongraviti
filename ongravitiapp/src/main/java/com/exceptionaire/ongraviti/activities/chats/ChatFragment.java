package com.exceptionaire.ongraviti.activities.chats;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.quickblox.chat.model.QBChatDialog;

import java.util.ArrayList;

import com.exceptionaire.ongraviti.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragment extends Fragment {
    private ListView m_lstChatView;
    private ArrayList<QBChatDialog> mDialogArray;
    private ChatListAdapter m_Adapter = null;
    public ChatsMainActivity m_Context = null;

    public ChatFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatFragment newInstance(ChatsMainActivity context) {
        ChatFragment fragment = new ChatFragment();
        fragment.m_Context = context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        m_lstChatView = (ListView) view.findViewById(R.id.list_chats);
        m_Adapter = m_Context.chatListAdapter;
        if (m_Adapter != null)
            m_lstChatView.setAdapter(m_Adapter);
        else {
            Toast.makeText(m_Context, getActivity().getString(R.string.no_recent_chats), Toast.LENGTH_SHORT).show();
        }

        return view;
    }
}
