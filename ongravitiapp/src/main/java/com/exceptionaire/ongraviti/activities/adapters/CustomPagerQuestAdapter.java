package com.exceptionaire.ongraviti.activities.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.model.Question;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomPagerQuestAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<Question> list_questionaire;
    private String TAG = "CustomPagerQuestAdapter";
    public static Boolean isCurAnsSelected = false; // this var check if current selected page, questionaire is selected or not

    private int countRightAns = 0;
    private AnswerAdptrListener listener;
    private Boolean isViewScoreVisible = false;

    public CustomPagerQuestAdapter(Context context, ArrayList<Question> list_questionaire, AnswerAdptrListener listener) {
        mContext = context;
        this.list_questionaire = list_questionaire;
        this.listener = listener;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public interface AnswerAdptrListener {
        void btnViewScoreClick(View v, int RightAnsCount);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View itemView = inflater.inflate(R.layout.layout_answer_questionaire, container, false);
        try {
            Log.d(TAG, "## position : " + position);
// Declare Variables
            TextView txt_question;
            final EditText txtans1, txtans2, txtans3, txtans4;
            final ImageView img_option1, img_option2, img_option3, img_option4;
            final Button btnViewScore;

            // Locate the TextViews in viewpager_item.xml
            btnViewScore = itemView.findViewById(R.id.btnViewScore);
            txt_question = itemView.findViewById(R.id.txt_que);
            txtans1 = itemView.findViewById(R.id.edt_option1);
            txtans2 = itemView.findViewById(R.id.edt_option2);
            txtans3 = itemView.findViewById(R.id.edt_option3);
            txtans4 = itemView.findViewById(R.id.edt_option4);

            img_option1 = itemView.findViewById(R.id.img_option1);
            img_option2 = itemView.findViewById(R.id.img_option2);
            img_option3 = itemView.findViewById(R.id.img_option3);
            img_option4 = itemView.findViewById(R.id.img_option4);

            // Capture position and set to the TextViews
            txt_question.setText(list_questionaire.get(position).getQuestion());

            isCurAnsSelected = false;
            img_option1.setTag("uncheck");
            img_option2.setTag("uncheck");
            img_option3.setTag("uncheck");
            img_option4.setTag("uncheck");

            if (isViewScoreVisible) {
                btnViewScore.setVisibility(View.VISIBLE);
                btnViewScore.setEnabled(true);
            }

            if (FavoriteUserInfoActivity.list_ans != null) {
                if (FavoriteUserInfoActivity.list_ans.size() > 2)
                    isViewScoreVisible = true;
                btnViewScore.setVisibility(View.VISIBLE);
                btnViewScore.setEnabled(true);
            }
            Log.d(TAG, "## equals FavoriteUserInfoActivity.list_ans.size() : " + FavoriteUserInfoActivity.list_ans.size());
            Log.d(TAG, "## equals FavoriteUserInfoActivity.currentPagerIndex : " + FavoriteUserInfoActivity.currentPagerIndex);

            //check if answer is selected in current page
            isCurAnsSelected = img_option1.getTag().equals("check") && img_option1.getTag().equals("check") && img_option1.getTag().equals("check") && img_option1.getTag().equals("check");

            btnViewScore.setOnClickListener(v -> {
                Log.d(TAG, "## FavoriteUserInfoActivity.list_ans : " + FavoriteUserInfoActivity.list_ans.size());
                Log.d(TAG, "## list_questionaire : " + list_questionaire.size());
                for (int i = 0; i < FavoriteUserInfoActivity.list_ans.size(); i++) {
                    Log.d(TAG, "## FavoriteUserInfoActivity.list_ans : " + FavoriteUserInfoActivity.list_ans.get(i));
                    Log.d(TAG, "## list_questionaire : " + list_questionaire.get(i).getAnswer());
                    if (FavoriteUserInfoActivity.list_ans.get(i).equals(list_questionaire.get(i).getAnswer()))
                        countRightAns++;
                }
                Log.d(TAG, "## countRightAns : " + countRightAns);
                listener.btnViewScoreClick(v, countRightAns);

                if (FavoriteUserInfoActivity.list_ans != null) {
                    if (FavoriteUserInfoActivity.list_ans.size() > 2) {
                        isViewScoreVisible = true;
                        btnViewScore.setVisibility(View.VISIBLE);
                        btnViewScore.setEnabled(true);
                    }
                }
            });

            img_option1.setOnClickListener(v -> {
                if (img_option1.getTag().equals("uncheck")) {
                    img_option1.setImageResource(R.drawable.d_chk);
                    img_option1.setTag("check");

                    List<String> list_temp = Arrays.asList(list_questionaire.get(FavoriteUserInfoActivity.currentPagerIndex).getQuestion_option().split(","));
                    if (FavoriteUserInfoActivity.list_ans.size() > FavoriteUserInfoActivity.currentPagerIndex) {
                        FavoriteUserInfoActivity.list_ans.set(FavoriteUserInfoActivity.currentPagerIndex, list_temp.get(0));
                    } else {
                        FavoriteUserInfoActivity.list_ans.add(list_temp.get(0));
                    }

                    isCurAnsSelected = true;

                    if (img_option2.getTag().equals("check")) {
                        img_option2.setImageResource(R.drawable.chekmark_de);
                        img_option2.setTag("uncheck");
                    }
                    if (img_option3.getTag().equals("check")) {
                        img_option3.setImageResource(R.drawable.chekmark_de);
                        img_option3.setTag("uncheck");
                    }
                    if (img_option4.getTag().equals("check")) {
                        img_option4.setImageResource(R.drawable.chekmark_de);
                        img_option4.setTag("uncheck");
                    }

                    if (FavoriteUserInfoActivity.list_ans != null) {
                        if (FavoriteUserInfoActivity.list_ans.size() > 2) {
                            isViewScoreVisible = true;
                            btnViewScore.setVisibility(View.VISIBLE);
                            btnViewScore.setEnabled(true);
                        }
                    }
                }
            });

            img_option2.setOnClickListener(v -> {
                if (img_option2.getTag().equals("uncheck")) {
                    img_option2.setImageResource(R.drawable.d_chk);
                    img_option2.setTag("check");

                /*if (list_options.size() > 0) {
                    FavoriteUserInfoActivity.list_ans.add(list_options.get(1));
                }*/
                    List<String> list_temp = Arrays.asList(list_questionaire.get(FavoriteUserInfoActivity.currentPagerIndex).getQuestion_option().split(","));
                    if (FavoriteUserInfoActivity.list_ans.size() > FavoriteUserInfoActivity.currentPagerIndex) {
                        FavoriteUserInfoActivity.list_ans.set(FavoriteUserInfoActivity.currentPagerIndex, list_temp.get(1));
                    } else {
                        FavoriteUserInfoActivity.list_ans.add(list_temp.get(1));
                    }

                    isCurAnsSelected = true;

                    if (img_option1.getTag().equals("check")) {
                        img_option1.setImageResource(R.drawable.chekmark_de);
                        img_option1.setTag("uncheck");
                    }
                    if (img_option3.getTag().equals("check")) {
                        img_option3.setImageResource(R.drawable.chekmark_de);
                        img_option3.setTag("uncheck");
                    }
                    if (img_option4.getTag().equals("check")) {
                        img_option4.setImageResource(R.drawable.chekmark_de);
                        img_option4.setTag("uncheck");
                    }

                    if (FavoriteUserInfoActivity.list_ans != null) {
                        if (FavoriteUserInfoActivity.list_ans.size() > 2) {
                            isViewScoreVisible = true;
                            btnViewScore.setVisibility(View.VISIBLE);
                            btnViewScore.setEnabled(true);
                        }
                    }
                }
            });

            List<String> list_options = Arrays.asList(list_questionaire.get(position).getQuestion_option().split(","));

            if (list_options.size() == 4) {


                txtans1.setText(list_options.get(0));
                txtans2.setText(list_options.get(1));
                txtans3.setText(list_options.get(2));
                txtans4.setText(list_options.get(3));

                img_option3.setOnClickListener(v -> {
                    if (img_option3.getTag().equals("uncheck")) {
                        img_option3.setImageResource(R.drawable.d_chk);
                        img_option3.setTag("check");
                    /*if (list_options.size() > 0) {
                        FavoriteUserInfoActivity.list_ans.add(list_options.get(2));
                    }*/
                        List<String> list_temp = Arrays.asList(list_questionaire.get(FavoriteUserInfoActivity.currentPagerIndex).getQuestion_option().split(","));
                        if (FavoriteUserInfoActivity.list_ans.size() > FavoriteUserInfoActivity.currentPagerIndex) {
                            FavoriteUserInfoActivity.list_ans.set(FavoriteUserInfoActivity.currentPagerIndex, list_temp.get(2));
                        } else {
                            FavoriteUserInfoActivity.list_ans.add(list_temp.get(2));
                        }

                        isCurAnsSelected = true;

                        if (img_option2.getTag().equals("check")) {
                            img_option2.setImageResource(R.drawable.chekmark_de);
                            img_option2.setTag("uncheck");
                        }
                        if (img_option1.getTag().equals("check")) {
                            img_option1.setImageResource(R.drawable.chekmark_de);
                            img_option1.setTag("uncheck");
                        }
                        if (img_option4.getTag().equals("check")) {
                            img_option4.setImageResource(R.drawable.chekmark_de);
                            img_option4.setTag("uncheck");
                        }
                    }

                    if (FavoriteUserInfoActivity.list_ans != null) {
                        if (FavoriteUserInfoActivity.list_ans.size() > 2) {
                            isViewScoreVisible = true;
                            btnViewScore.setVisibility(View.VISIBLE);
                            btnViewScore.setEnabled(true);
                        }
                    }
                });

                img_option4.setOnClickListener(v -> {
                    if (img_option4.getTag().equals("uncheck")) {
                        img_option4.setImageResource(R.drawable.d_chk);
                        img_option4.setTag("check");

                        List<String> list_temp = Arrays.asList(list_questionaire.get(FavoriteUserInfoActivity.currentPagerIndex).getQuestion_option().split(","));
                        if (FavoriteUserInfoActivity.list_ans.size() > FavoriteUserInfoActivity.currentPagerIndex) {
                            FavoriteUserInfoActivity.list_ans.set(FavoriteUserInfoActivity.currentPagerIndex, list_temp.get(3));
                        } else {
                            FavoriteUserInfoActivity.list_ans.add(list_temp.get(3));
                        }

                        isCurAnsSelected = true;

                        if (img_option2.getTag().equals("check")) {
                            img_option2.setImageResource(R.drawable.chekmark_de);
                            img_option2.setTag("uncheck");
                        }
                        if (img_option3.getTag().equals("check")) {
                            img_option3.setImageResource(R.drawable.chekmark_de);
                            img_option3.setTag("uncheck");
                        }
                        if (img_option1.getTag().equals("check")) {
                            img_option1.setImageResource(R.drawable.chekmark_de);
                            img_option1.setTag("uncheck");
                        }
                    }

                    if (FavoriteUserInfoActivity.list_ans != null) {
                        if (FavoriteUserInfoActivity.list_ans.size() > 2) {
                            isViewScoreVisible = true;
                            btnViewScore.setVisibility(View.VISIBLE);
                            btnViewScore.setEnabled(true);
                        }
                    }
                });
            } else {
                txtans1.setText(list_options.get(0));
                txtans2.setText(list_options.get(1));
                txtans3.setVisibility(View.GONE);
                txtans4.setVisibility(View.GONE);

                img_option3.setVisibility(View.GONE);
                img_option4.setVisibility(View.GONE);


            }

            container.addView(itemView);

        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
        return itemView;
    }


    @Override
    public int getCount() {
        return list_questionaire.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


}
