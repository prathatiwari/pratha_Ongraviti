package com.exceptionaire.ongraviti.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.myorbit.MyBlockUser;
import com.exceptionaire.ongraviti.core.AppSettings;

/**
 * Created by root on 14/11/16.
 */

public class Setting extends BaseDrawerActivity {

    private TextView logout;
    private TextView header, textview_blockuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.settings, frameLayout);
        initView();
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = CommonDialogs.showAlertDialog(Setting.this, getResources().getString(R.string.ongraviti), "Are you sure you want to logout?");
                Button yes = (Button) dialog.findViewById(R.id.button_ok);
                yes.setText("Yes");
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        AppSettings.setLogin(false);
                        AppSettings.setProfiePicturePath("");
                        AppSettings.setUserProfileName("");
                        AppSettings.setProfileDefaultCategory("");
                        AppSettings.setProfiePictureName("");
                        AppSettings.setProfileStepStatus1(0);
                        AppSettings.setProfileStepStatus2(0);
                        AppSettings.setProfileStepStatus3(0);
                        AppSettings.setProfileStepStatus4(0);
                        AppSettings.setProfileStepStatus5(0);
//                        EditProfileActivity.list_profile_category=null;
                        Intent loginIntent = new Intent(Setting.this, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(loginIntent);
                        finish();
                    }
                });
                Button cancle = (Button) dialog.findViewById(R.id.button_cancel);
                cancle.setText("Cancel");
                cancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

        textview_blockuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Setting.this, MyBlockUser.class);
                startActivity(i);
            }
        });
    }

    private void initView() {
        textview_blockuser = (TextView) findViewById(R.id.textview_blockuser);
        logout = (TextView) findViewById(R.id.logout_button);
        header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText(getResources().getString(R.string.settings));
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }
}
