package com.exceptionaire.ongraviti.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.DatingCardAdapter;
import com.exceptionaire.ongraviti.activities.adapters.GridAdapter;
import com.exceptionaire.ongraviti.activities.base.ArcMenu;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.SlidingUpPanelLayout;
import com.exceptionaire.ongraviti.activities.base.StateChangeListener;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.DatingCard;
import com.exceptionaire.ongraviti.model.MultiProfileInfo;
import com.exceptionaire.ongraviti.model.ResponseAskForDatingNFavouriteUser;
import com.exceptionaire.ongraviti.model.ResponseDatingUser;
import com.exceptionaire.ongraviti.model.ResponseDatingUsersList;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import SwipeCard.SwipeCardView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.activities.base.SlidingUpPanelLayout.PanelState.COLLAPSED;
import static com.exceptionaire.ongraviti.activities.base.SlidingUpPanelLayout.PanelState.DRAGGING;
import static com.exceptionaire.ongraviti.activities.base.SlidingUpPanelLayout.PanelState.EXPANDED;
import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

public class AskForDating extends BaseDrawerActivity implements
        DatingCardAdapter.datingAddtoFavoriteListener, DatingCardAdapter.DatingChangeDataListener {

    String TAG = "AskForDating";
    private DatingCardAdapter cardAdapter;
    SwipeCardView swipeCardView;
    private TextView header1, textAge, textViewUserName,
            textViewOccupation;
    private ImageView coffee, drink, play;
    private Button seeYaLater;
    TextView text_view_play;
    private String status;
    private SlidingUpPanelLayout mLayout;
    String ProfileBio;
    private ArrayList<ResponseDatingUser> listDatingUsers; //actual list
    private ArrayList<ResponseDatingUser> listDatingUsersTemp; //actual list
    private int current_user_index = 0;//, current_user_index_1 = 0;
    private ProgressDialog progressDialog;
    Context context;
    private String videoBioPath = null;
    final String ask_for_type = "1";
    int display_index = 0;
    ResponseDatingUser datingUsers_to_display;
    boolean is_dialog_open = false;
    TextView tv_msg;
    Dialog dialog;
    private LinearLayout linear_ask;
    String receiver_user_id, ask_for_cat;//(1-Coffee,2-Drink), ask_for_type=(1-Dating,2-Business)
    private ImageView imageViewBack;
    private TextView textViewHeader;
    private ImageView image_view_arrow;
    private TextView text_view_name, text_view_age, text_view_city, text_view_status, text_view_des,
            text_view_height, text_view_body, text_view_smoking, text_view_weight, text_view_hair,
            text_view_drinking;
    FrameLayout frame;
    LinearLayout dragView;
    private ImageView imageViewBg;
    Button button_view_profile;
    private static int HeaderIconSelection = 0;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ask_for_dating);
//        arcMenu.setVisibility(View.VISIBLE);
        try {
            context = this;
            initView();
            initListners();
            tv_msg = (TextView) findViewById(R.id.tv_msg);
            callDatingUsersListApi();
            setFlingListener();
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        if (!AppSettings.isOpenListView()) {
            imageViewBg.setVisibility(View.VISIBLE);
        }


        if (!AppSettings.isOpenListView()) {
            frame.postDelayed(new Runnable() {
                @Override
                public void run() {
                    imageViewBg.setVisibility(View.GONE);
                    AppSettings.setOpenListView(true);
                }
            }, 5000);

        } else {
            imageViewBg.setVisibility(View.GONE);
        }
        imageViewBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageViewBg.setVisibility(View.GONE);
            }
        });


        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == EXPANDED) {
                    image_view_arrow.setVisibility(View.VISIBLE);
                    image_view_arrow.setImageResource(R.drawable.down_arrow);

                } else if (newState == COLLAPSED) {
                    image_view_arrow.setVisibility(View.VISIBLE);
                    image_view_arrow.setImageResource(R.drawable.up_arrow);

                } else if (newState == DRAGGING) {
                    image_view_arrow.setVisibility(View.INVISIBLE);
                }
            }
        });
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(COLLAPSED);
            }
        });
    }

    public void setFlingListener() {
        try {
            swipeCardView.setFlingListener(new SwipeCardView.OnCardFlingListener() {
                @Override
                public void onCardExitLeft(Object dataObject) {

                    Log.d(TAG, "## onCardExitRight : " + dataObject);
                    ResponseDatingUser datingUsers = (ResponseDatingUser) dataObject;
                    receiver_user_id = datingUsers.getUserId();
                    ask_for_cat = "1";

                    listDatingUsersTemp.add(datingUsers);
                    cardAdapter.notifyDataSetChanged();

                    if (is_dialog_open) {
                    } else
                        showDialogForActivites(1);
                }

                @Override
                public void onCardExitRight(Object dataObject) {
                    Log.d(TAG, "## onCardExitLeft : ");
                    ResponseDatingUser datingUsers = (ResponseDatingUser) dataObject;
                    receiver_user_id = datingUsers.getUserId();
                    ask_for_cat = "2";

                    listDatingUsersTemp.add(datingUsers);
                    cardAdapter.notifyDataSetChanged();

                    if (is_dialog_open) {
                    } else
                        showDialogForActivites(3);
                }

                @Override
                public void onAdapterAboutToEmpty(int itemsInAdapter) {
                    Log.d(TAG, "## onAdapterAboutToEmpty : " + itemsInAdapter);
//                getDummyData();
                /*fillDataInList();
                cardAdapter.notifyDataSetChanged();*/
                }

                @Override
                public void onScroll(float scrollProgressPercent) {
                    Log.d(TAG, "## onScroll");
                }

                @Override
                public void onCardExitTop(Object dataObject) {

                    Log.d(TAG, "## onCardExitTop : " + dataObject);
                    ResponseDatingUser datingUsers = (ResponseDatingUser) dataObject;
                    receiver_user_id = datingUsers.getUserId();
                    ask_for_cat = "3";

                    listDatingUsersTemp.add(datingUsers);
                    cardAdapter.notifyDataSetChanged();

                    if (is_dialog_open) {
                    } else
                        showDialogForActivites(2);
                }

                @Override
                public void onCardExitBottom(Object dataObject) {
                    Log.d(TAG, "## onCardExitBottom ");
                    DatingCardAdapter.isFavorite = false;
                    ResponseDatingUser datingUsersLastCheck = (ResponseDatingUser) dataObject;
                    String current_userid = datingUsersLastCheck.getUserId();
                    if (!datingUsersLastCheck.getUserId().equals(listDatingUsers.get(listDatingUsers.size() - 1).getUserId())) {
                        for (int i = 0; i < listDatingUsers.size(); i++) {
                            if (current_userid.equals(listDatingUsers.get(i).getUserId())) {
                                display_index = i + 1;
                                datingUsers_to_display = (ResponseDatingUser) listDatingUsers.get(i + 1);
                            }
                        }
                        Log.d(TAG, "## display_index : " + display_index);

                        listDatingUsersTemp.set(listDatingUsersTemp.size() - 1, datingUsers_to_display);
                        listDatingUsersTemp.add(datingUsers_to_display);

                        cardAdapter.notifyDataSetChanged();
                    } else {
                        Log.d(TAG, "## Bottom last ELSE");
                        swipeCardView.setVisibility(View.GONE);
                        dragView.setVisibility(View.GONE);
                        FrameLayout ask_for_dating_framelayout = (FrameLayout) findViewById(R.id.ask_for_dating_framelayout);
                        frame.setBackgroundResource(R.drawable.thats_all_folks);
                        ask_for_dating_framelayout.setVisibility(View.GONE);
                        coffee.setVisibility(View.GONE);
                        drink.setVisibility(View.GONE);
                        arcMenu.setVisibility(View.GONE);
                        text_view_play.setVisibility(View.GONE);
                        seeYaLater.setVisibility(View.GONE);
                        linear_ask.setVisibility(View.GONE);
                        play.setVisibility(View.GONE);
                    }
                }
            });//TODO
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void callDatingUsersListApi() {
        try {
            // WESERVICE_FLAG = 1;  // 2 flag for get profile data
            if (isNetworkAvailable()) {
                showProgressBar();
                Map<String, String> map = new HashMap<>();
                //map.put(KEY_USERNAME, user_email);

                map.put("user_id", AppSettings.getLoginUserId()); //mandatory

                Retrofit retrofit = ApiClient.getClient();
                final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
                Call<ResponseDatingUsersList> accessTokenCall = requestInterface.getDatingUserList(map);
                accessTokenCall.enqueue(new Callback<ResponseDatingUsersList>() {
                    @Override
                    public void onResponse(Call<ResponseDatingUsersList> call, retrofit2.Response<ResponseDatingUsersList> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        if (response != null && response.body() != null) {
                            if (response.body() != null && response.body().getStatus().equals(AppConstants.RESPONSE_SUCCESS)) {
                                Log.d(TAG, "## response : " + response.body().toString() + " ");

                                tv_msg.setVisibility(View.GONE);
                                ResponseDatingUser datingUsers1 = null;
                                if (response.body().getUserList() != null && response.body().getUserList().size() > 0) {
                                    listDatingUsers = response.body().getUserList();
//                    fillDataInList();
                                    if (listDatingUsers.size() > 0) {
                                        linear_ask.setVisibility(View.VISIBLE);
                                        text_view_play.setVisibility(View.VISIBLE);
                                        play.setVisibility(View.VISIBLE);
                                        Log.d(TAG, "## listDatingUsers : " + listDatingUsers.size() + " ");
                                        Log.d(TAG, "## listDatingUsers : " + listDatingUsers.get(0).getProfilePicture() + " ");
//                        listDatingUsersTemp=new ArrayList<>();
                                        if (listDatingUsers.size() > 0) {
                                            for (int i = current_user_index; i < 2; i++) {
                                                datingUsers1 = listDatingUsers.get(current_user_index);
                                                listDatingUsersTemp.add(datingUsers1);
                                                current_user_index = 0;
                                            }
                                            cardAdapter.add(datingUsers1);
                                            cardAdapter.notifyDataSetChanged();
                                        }
                                    } else {

                                        tv_msg.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                swipeCardView.setBackgroundResource(R.drawable.placeholder);
                                swipeCardView.setVisibility(View.VISIBLE);
                                coffee.setVisibility(View.GONE);
                                dragView.setVisibility(View.GONE);
                                drink.setVisibility(View.GONE);
                                play.setVisibility(View.GONE);
                                text_view_play.setVisibility(View.GONE);
                                TextView text_view_seeya = (TextView) findViewById(R.id.text_view_seeya);
                                text_view_seeya.setVisibility(View.GONE);
                                seeYaLater.setVisibility(View.GONE);
                                tv_msg.setVisibility(View.GONE);
                            }
                        } else {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            AppSettings.setLogin(false);
                            swipeCardView.setBackgroundResource(R.drawable.placeholder);
                            swipeCardView.setVisibility(View.VISIBLE);
                            coffee.setVisibility(View.GONE);
                            dragView.setVisibility(View.GONE);
                            drink.setVisibility(View.GONE);
                            play.setVisibility(View.GONE);
                            text_view_play.setVisibility(View.GONE);
                            TextView text_view_seeya = (TextView) findViewById(R.id.text_view_seeya);
                            text_view_seeya.setVisibility(View.GONE);
                            seeYaLater.setVisibility(View.GONE);
                            tv_msg.setVisibility(View.GONE);
                            new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDatingUsersList> call, Throwable t) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Something went wrong, please try after some time.", Toast.LENGTH_SHORT).show();
                        Log.e("Error: ", t.toString());
                    }
                });


                // requestQueue = Volley.newRequestQueue(context);
                // customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.GET_DATING_USERS, map, this, this);
                // requestQueue.add(customVolleyRequest);
            } else {
                Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void callAskForApi(String message) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());
            //WESERVICE_FLAG = 2;  // 2 flag for ask for coffee, drink and confused
            Map<String, String> map = new HashMap<>();
            //map.put(KEY_USERNAME, user_email);

            showProgressBar();
            map.put("user_id", AppSettings.getLoginUserId()); //mandatory//408
            map.put("receiver_user_id", receiver_user_id); //mandatory// 328
            map.put("ask_for_cat", ask_for_cat); //mandatory//1
            map.put("ask_for_type", ask_for_type); //mandatory//1
            map.put("message", message); //mandatory//jdghcjh
            map.put("device_date", currentDateandTime);//mandatory
            Log.d(TAG, "## ask for map : " + map.toString());

            Retrofit retrofit = ApiClient.getClient();
            final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseAskForDatingNFavouriteUser> accessTokenCall = requestInterface.setAskForDatingEvent(map);
            accessTokenCall.enqueue(new Callback<ResponseAskForDatingNFavouriteUser>() {
                @Override
                public void onResponse(Call<ResponseAskForDatingNFavouriteUser> call, retrofit2.Response<ResponseAskForDatingNFavouriteUser> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    is_dialog_open = false;
                    Log.d(TAG, "## response : " + response + " ");

                    status = response.body().getStatus();
                    String msg = response.body().getMsg();
                    if (status.equalsIgnoreCase("success")) {
//                        CommonDialogs.dialog_with_one_btn_without_title(AskForDating.this, getResources().getString(R.string.success));
//                    CommonDialogs.showdialog(context, getResources().getString(R.string.success), msg);
                        Toast.makeText(AskForDating.this, "Your request has been successfully sent.", Toast.LENGTH_LONG).show();
                        swipeCardView.throwBottom();
                    }
                }

                @Override
                public void onFailure(Call<ResponseAskForDatingNFavouriteUser> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                    Log.e("Error: ", t.toString());
                }
            });

            ///  requestQueue = Volley.newRequestQueue(context);
            //  customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.SET_ASK_FOR_DATING_API, map, this, this);
            // requestQueue.add(customVolleyRequest);
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void initListners() {
        try {
//        addtoFavorite.setOnClickListener(this);
            if (isNetworkAvailable()) {
                coffee.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swipeCardView.throwRight();
                        if (is_dialog_open) {
                        } else
                            showDialogForActivites(1);
                    }
                });
            }

            if (isNetworkAvailable()) {
                drink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swipeCardView.throwLeft();
                        if (is_dialog_open) {
                        } else
                            showDialogForActivites(3);
                    }
                });
            }
            if (isNetworkAvailable()) {
                seeYaLater.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swipeCardView.throwBottom();
                    }
                });
            }

            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //TODO set video bio
                    if (isNetworkAvailable()) {
                        if (ProfileBio != null) {
                            if (!ProfileBio.equals("")) {
                                Intent intent1 = new Intent(AskForDating.this, VideoBioActivity.class);
                                intent1.putExtra("videopath", ProfileBio);
                                startActivity(intent1);
                            } else {
                                CommonDialogs.dialog_with_one_btn_without_title(AskForDating.this, "Profile video not available.");
                            }
                        }
                    } else {
                        Toast.makeText(AskForDating.this, getResources().getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
                    }
                }
            });


            imageViewBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });


        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    static void makeToast(Context ctx, String s) {
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }

    private void initView() {
        try {
            imageViewBg = (ImageView) findViewById(R.id.image_view_bg);
            ImageView image_view_arcback = (ImageView) findViewById(R.id.image_view_arcback);
            final OvershootInterpolator[] interpolator = {new OvershootInterpolator()};
            arcMenu = (ArcMenu) findViewById(R.id.arcMenu);
            arcMenu.setRadius(getResources().getDimension(R.dimen.radius));

            arcMenu.setStateChangeListener(new StateChangeListener() {
                @Override
                public void onMenuOpened() {
                    interpolator[0] = new OvershootInterpolator();
                    float rotationAnim = ViewCompat.getRotation(arcMenu.fabMenu);
                    image_view_arcback.setVisibility(View.VISIBLE);
                    ViewCompat.animate(arcMenu.fabMenu).
                            rotation(rotationAnim + 360f).
                            withLayer().
                            setDuration(300).
                            setInterpolator(interpolator[0]).
                            start();

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            arcMenu.fabMenu.setImageDrawable(ContextCompat.getDrawable(AskForDating.this, R.drawable.ic_action_close));
                        }
                    }, 300);
                }

                @Override
                public void onMenuClosed() {
                    image_view_arcback.setVisibility(View.INVISIBLE);
                    arcMenu.fabMenu.setImageDrawable(ContextCompat.getDrawable(AskForDating.this, R.drawable.ic_fab_menu));
                }
            });
            findViewById(R.id.fab_ic_search).setOnClickListener(subMenuClickListener);
            findViewById(R.id.fab_ic_dating).setOnClickListener(subMenuClickListener);
            findViewById(R.id.fab_ic_business).setOnClickListener(subMenuClickListener);

            dragView = (LinearLayout) findViewById(R.id.dragView);
            frame = (FrameLayout) findViewById(R.id.frame);
            text_view_name = (TextView) findViewById(R.id.text_view_name);
            text_view_age = (TextView) findViewById(R.id.text_view_age);
            text_view_city = (TextView) findViewById(R.id.text_view_city);
            text_view_status = (TextView) findViewById(R.id.text_view_status);
            text_view_des = (TextView) findViewById(R.id.text_view_des);
            text_view_height = (TextView) findViewById(R.id.text_view_height);
            text_view_body = (TextView) findViewById(R.id.text_view_body);
            text_view_smoking = (TextView) findViewById(R.id.text_view_smoking);
            text_view_weight = (TextView) findViewById(R.id.text_view_weight);
            text_view_hair = (TextView) findViewById(R.id.text_view_hair);
            text_view_drinking = (TextView) findViewById(R.id.text_view_drinking);
            button_view_profile = (Button) findViewById(R.id.button_view_profile);
            image_view_arrow = (ImageView) findViewById(R.id.image_view_arrow);
            textViewHeader = (TextView) findViewById(R.id.text_view_title);
            textViewHeader.setText(getResources().getString(R.string.dating));
            coffee = (ImageView) findViewById(R.id.ask_for_dating_coffee);
            drink = (ImageView) findViewById(R.id.ask_for_dating_drink);
            play = (ImageView) findViewById(R.id.image_view_play);
            text_view_play = (TextView) findViewById(R.id.text_view_play);
            textAge = (TextView) findViewById(R.id.text_view_no);
            textViewUserName = (TextView) findViewById(R.id.text_view_user_name);
            textViewOccupation = (TextView) findViewById(R.id.text_view_user_occupation);
            seeYaLater = (Button) findViewById(R.id.ask_for_dating_see_ya_later);
            swipeCardView = (SwipeCardView) findViewById(R.id.swipeCardViewDating);
            linear_ask = (LinearLayout) findViewById(R.id.linear_ask);
            imageViewBack = (ImageView) findViewById(R.id.image_view_back);

            listDatingUsersTemp = new ArrayList<>();
            listDatingUsers = new ArrayList<>();
            if (listDatingUsers.size() > 0) {
                for (int i = current_user_index; i < 2; i++) {
                    ResponseDatingUser datingUsers = listDatingUsers.get(current_user_index);
                    listDatingUsersTemp.add(datingUsers);
                    current_user_index = 0;
                    Log.d(TAG, "## listDatingUsersTemp : " + listDatingUsersTemp.size());
                    Log.d(TAG, "## listDatingUsersTemp : " + listDatingUsersTemp.get(0).getProfilePicture());
                }
                current_user_index++;
            }

            cardAdapter = new DatingCardAdapter(AskForDating.this, listDatingUsersTemp);
            cardAdapter.setdatingAddtoFavorite(AskForDating.this);
            cardAdapter.setDatingChangeDataListener(AskForDating.this);
            swipeCardView.setAdapter(cardAdapter);
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private View.OnClickListener subMenuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            arcMenu.toggleMenu();
            switch (v.getId()) {
                case R.id.fab_ic_search:

                    HeaderIconSelection = 0;
                    Intent intentSearch = new Intent(AskForDating.this, SearchActivity.class);
                    startActivity(intentSearch);
                    // finish();
                    break;

                case R.id.fab_ic_dating:
                    HeaderIconSelection = 1;
                    Intent intentDating = new Intent(AskForDating.this, AcceptDatingRequest.class);
                    startActivity(intentDating);
                    finish();
                    break;
                case R.id.fab_ic_business:
                    HeaderIconSelection = 2;
                    Intent intentBusiness = new Intent(AskForDating.this, AskForBusiness.class);
                    startActivity(intentBusiness);
                    // finish();
                    break;
            }
        }
    };

    private void showDialogForActivites(int whichDialog) {
        try {
            int FLAG = whichDialog;

            switch (FLAG) {
                case 1:
                    dialog = new Dialog(context, R.style.DialogStyle);

                    //dialog initialization
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_dating_business);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    // done with initialization

                    Button btnSend = (Button) dialog.findViewById(R.id.btnSend);
                    Button btnCacel = (Button) dialog.findViewById(R.id.btnCancel);

                    final EditText edtMessage = (EditText) dialog.findViewById(R.id.edtMessage);

                    btnSend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            is_dialog_open = false;
                            String dialogText = edtMessage.getText().toString();
                            dialog.dismiss();
                            callAskForApi(dialogText);
                        }
                    });

                    btnCacel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            is_dialog_open = false;
                            dialog.dismiss();
                        }
                    });

                    is_dialog_open = true;
                    dialog.show();

                    break;
                case 3:
                    dialog = new Dialog(context, R.style.DialogStyle);

                    //dialog initialization
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_dating_business);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    // done with initialization
                    RelativeLayout relativeUP = (RelativeLayout) dialog.findViewById(R.id.relativeUP);
                    Button btnSend1 = (Button) dialog.findViewById(R.id.btnSend);
                    Button btnCacel1 = (Button) dialog.findViewById(R.id.btnCancel);
                    relativeUP.setBackgroundResource(R.drawable.popbgdrink);
                    final EditText edtMessage1 = (EditText) dialog.findViewById(R.id.edtMessage);
                    TextView text_view_msg = (TextView) dialog.findViewById(R.id.text_view_msg);
                    text_view_msg.setText("Lemme buy you a drink.");
                    ImageView img_dating = (ImageView) dialog.findViewById(R.id.img_dating);
                    img_dating.setImageResource(R.drawable.drink_blue);

                    btnSend1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            is_dialog_open = false;
                            String dialogText = edtMessage1.getText().toString();
                            dialog.dismiss();
                            callAskForApi(dialogText);
                        }
                    });

                    btnCacel1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            is_dialog_open = false;
                            dialog.dismiss();
                        }
                    });

                    is_dialog_open = true;
                    dialog.show();
                    break;
            }
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void ondatingAddtoFavoriteClickListner(int favUserID) {
        try {
            showProgressBar();
            //WESERVICE_FLAG = 3;  // 3 favorite
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());
            Map<String, String> map = new HashMap<>();

            map.put("user_id", AppSettings.getLoginUserId()); //mandatory
            map.put("favorite_user_id", String.valueOf(favUserID)); //mandatory
            map.put("profile_cat_id", PROFILE_CATEGORY_TWO_DATING);
            map.put("device_date", currentDateandTime);

            Log.d(TAG, "## favorite map : " + map.toString());

            Retrofit retrofit = ApiClient.getClient();
            final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseAskForDatingNFavouriteUser> accessTokenCall = requestInterface.setFavouriteUser(map);
            accessTokenCall.enqueue(new Callback<ResponseAskForDatingNFavouriteUser>() {
                @Override
                public void onResponse(Call<ResponseAskForDatingNFavouriteUser> call, retrofit2.Response<ResponseAskForDatingNFavouriteUser> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    Log.d(TAG, "## response : " + response + " ");

                    status = response.body().getStatus();
                    if (status.equalsIgnoreCase("success")) {
                        Toast.makeText(AskForDating.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseAskForDatingNFavouriteUser> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Something went wrong, please try after some time.", Toast.LENGTH_SHORT).show();
                    Log.e("Error: ", t.toString());
                }
            });
            /// requestQueue = Volley.newRequestQueue(context);
            // customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.SET_FAVORITE_USER, map, this, this);
            // requestQueue.add(customVolleyRequest);
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    @Override
    public void onDataChange(ResponseDatingUser user) {
        if (user != null) {
            if (user.getCName() == null || user.getCName().equals("")) {
                textViewUserName.setText("NA");
                text_view_name.setText("NA");
            } else if (user.getCName() != null) {
                textViewUserName.setText(user.getCName());
                text_view_name.setText(user.getCName().trim());
            } else if (!user.getCName().equals("")) {
                textViewUserName.setText(user.getCName());
                text_view_name.setText(user.getCName().trim());
            }

            if (user.getDateOfBirth() == null || user.getDateOfBirth().equals("")) {
                text_view_age.setText("0");
                textAge.setText("0");
            } else if (user.getDateOfBirth() != null) {
                text_view_age.setText(", " + getAge(user.getDateOfBirth()));
                textAge.setText(getAge(user.getDateOfBirth()));
            } else if (!user.getDateOfBirth().equals("")) {
                text_view_age.setText(", " + getAge(user.getDateOfBirth()));
                textAge.setText(getAge(user.getDateOfBirth()));
            }


            if (user.getCity() == null || user.getCity().equals("")) {
                text_view_city.setText("NA");
            } else if (user.getCity() != null) {
                text_view_city.setText(user.getCity());
            } else if (!user.getCity().equals("")) {
                text_view_city.setText(user.getCity());
            }


            if (user.getRelationshipStatus() == null || user.getRelationshipStatus().equals("")) {
                text_view_status.setText("NA");
            } else if (user.getRelationshipStatus() != null) {
                text_view_status.setText(user.getRelationshipStatus());
            } else if (!user.getRelationshipStatus().equals("")) {
                text_view_status.setText(user.getRelationshipStatus());
            }

            if (user.gettraits() == null || user.gettraits().equals("")) {
                text_view_des.setText("\n \nDescription not available.");
            } else if (user.gettraits() != null) {
                text_view_des.setText(user.gettraits());
            } else if (!user.gettraits().equals("")) {
                text_view_des.setText(user.gettraits());
            }

            if (user.getHeight() == null || user.getHeight().equals("")) {
                text_view_height.setText("NA");
            } else if (user.getHeight() != null) {
                String[] separated = user.getHeight().split("\\(");
                text_view_height.setText(separated[0]);
            } else if (!user.getHeight().equals("")) {
                String[] separated = user.getHeight().split("\\(");
                text_view_height.setText(separated[0]);
            }


            if (user.getBodyType() == null || user.getBodyType().equals("")) {
                text_view_body.setText("NA");
            } else if (user.getBodyType() != null) {
                text_view_body.setText(user.getBodyType());
            } else if (!user.getBodyType().equals("")) {
                text_view_body.setText(user.getBodyType());
            }

            if (user.getSmoking() == null || user.getSmoking().equals("")) {
                text_view_smoking.setText("NA");
            } else if (user.getSmoking() != null) {
                text_view_smoking.setText(user.getSmoking());
            } else if (!user.getSmoking().equals("")) {
                text_view_smoking.setText(user.getSmoking());
            }

            if (user.getWeight() == null || user.getWeight().equals("")) {
                text_view_weight.setText("NA");
            } else if (user.getWeight() != null) {
                text_view_weight.setText(user.getWeight() + " Kg");
            } else if (!user.getWeight().equals("")) {
                text_view_weight.setText(user.getWeight() + " Kg");
            }

            if (user.getHairColor() == null || user.getHairColor().equals("")) {
                text_view_hair.setText("NA");
            } else if (user.getHairColor() != null) {
                text_view_hair.setText(user.getHairColor());
            } else if (!user.getHairColor().equals("")) {
                text_view_hair.setText(user.getHairColor());
            }

            if (user.getDrinking() == null || user.getDrinking().equals("")) {
                text_view_drinking.setText("NA");
            } else if (user.getDrinking() != null) {
                text_view_drinking.setText(user.getDrinking());
            } else if (!user.getDrinking().equals("")) {
                text_view_drinking.setText(user.getDrinking());
            }


            ArrayList<MultiProfileInfo> multiProfileInfos = new ArrayList();
            ArrayList<String> ImageSocial = new ArrayList<>();
            if (!user.getMultipleProfileSet().isEmpty() && user.getMultipleProfileSet().size() != 0 && user.getMultipleProfileSet() != null) {
                multiProfileInfos = user.getMultipleProfileSet();
                if (multiProfileInfos.get(1).getSetProfilePicture().length() != 0 && !multiProfileInfos.get(1).getSetProfilePicture().isEmpty())
                    ImageSocial = new ArrayList<>(multiProfileInfos.get(1).getSetProfilePicture().length());
                for (int i = 0; i < multiProfileInfos.size(); i++) {
                    ImageSocial.add(PROFILE_PICTURE_PATH + "/" + multiProfileInfos.get(i).getSetProfilePicture());
//                    AppSettings.setKeyImagesSocial(ImageSocial);
                }
            }
            ArrayList<String> finalImageSocial = ImageSocial;
//            swipeCardView.setOnItemClickListener(new SwipeCardView.OnItemClickListener() {
//                @Override
//                public void onItemClicked(int itemPosition, Object dataObject) {
//                    if (!finalImageSocial.isEmpty() && finalImageSocial.size() != 0 && finalImageSocial != null) {
//                        new CommonDialogs().displayImageInGrid(AskForDating.this, finalImageSocial);
//                    } else {
//                        CommonDialogs.dialog_with_one_btn_without_title(AskForDating.this, "No more images for this user.");
//                    }
//                }
//            });
            button_view_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentFavUserView = new Intent(AskForDating.this, FavoriteUserInfoActivity.class);
                    intentFavUserView.putExtra("ActivityName", "AskForDating");
                    intentFavUserView.putExtra("dating_user_id", user.getUserId());
                    startActivity(intentFavUserView);
                }
            });
            String occupation = "";
            if (user.getHighestDegree() != null && user.getHighestDegree().length() > 0) {
                occupation = user.getHighestDegree();
            }
            if (user.getCollegeUniversityName() != null && user.getCollegeUniversityName().length() > 0) {
                occupation = occupation + " from " + user.getCollegeUniversityName();
            }
            if (occupation.length() == 0) {
                textViewOccupation.setVisibility(View.GONE);
            } else {
                textViewOccupation.setVisibility(View.VISIBLE);
                textViewOccupation.setText(occupation);
            }
            if (user.getProfile_bio() != null) {
                if (!user.getProfile_bio().equals("")) {
                    ProfileBio = user.getProfile_bio();
                } else {
                    ProfileBio = "";
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private String getAge(String dateOfBirth) {
        String date[] = new String[3];
        if (dateOfBirth.contains("/")) {
            date = dateOfBirth.split("/");
        } else if (dateOfBirth.contains("-")) {
            date = dateOfBirth.split("-");
        }
        int year = Integer.parseInt(date[2]);
        int month = Integer.parseInt(date[1]);
        int day = Integer.parseInt(date[0]);

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return String.valueOf(age);
    }
}