package com.exceptionaire.ongraviti.activities.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.imoji.EmoticonTextView;
import com.exceptionaire.ongraviti.model.Person;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class PeopleNearByAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Person> dataSet;
    private Context context;

    public static class FeedTypeViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewProfileType;
        TextView txtPName, textViewDistance, txtLikeCount;
        EmoticonTextView txtPostDescription;
        RoundedImageView imgProfile;
        RelativeLayout relativeLayoutProfile;
        ImageView image_view_cat_type;

        FeedTypeViewHolder(View itemView) {
            super(itemView);
            relativeLayoutProfile = itemView.findViewById(R.id.relative_Profile);
            imgProfile = itemView.findViewById(R.id.imgProfilePic);
            txtPName = itemView.findViewById(R.id.txtProfileName);
            textViewDistance = itemView.findViewById(R.id.txtDistance);
            imageViewProfileType = itemView.findViewById(R.id.img_profile_type);
            image_view_cat_type = itemView.findViewById(R.id.image_view_cat_type);
        }
    }

    public PeopleNearByAdapter(ArrayList<Person> data, Context context) {
        this.dataSet = data;
        this.context = context;
    }

    public void setListUserPost(List<Person> userPosts) {
        this.dataSet.clear();
        this.dataSet.addAll(userPosts);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_near_by_friend, parent, false);
        return new FeedTypeViewHolder(view);
    }


    @Override
    public int getItemViewType(int position) {
        return 0;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        Person person = dataSet.get(position);


        if (person != null) {

            if (person.getProfilePhoto() != null && !person.getProfilePhoto().isEmpty()) {

                if (!person.getProfilePhoto().contains("https:")) {
                    Picasso.with(context)
                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + person.getProfilePhoto())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(((FeedTypeViewHolder) holder).imgProfile);
                } else if (person.getProfilePhoto().contains("https:")) {
                    Picasso.with(context)
                            .load(person.getProfilePhoto())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(((FeedTypeViewHolder) holder).imgProfile);
                }
            }
            ((FeedTypeViewHolder) holder).txtPName.setText(person.getName());

            switch (person.getProfileType()) {
                case "1":
                    ((FeedTypeViewHolder) holder).imageViewProfileType.setImageResource(R.drawable.ic_locations_green);
                    ((FeedTypeViewHolder) holder).image_view_cat_type.setImageResource(R.drawable.ic_circle_green);
                    break;
                case "2":
                    ((FeedTypeViewHolder) holder).image_view_cat_type.setImageResource(R.drawable.ic_circle_pink);
                    ((FeedTypeViewHolder) holder).imageViewProfileType.setImageResource(R.drawable.ic_location_pink);
                    break;
                case "3":
                    ((FeedTypeViewHolder) holder).image_view_cat_type.setImageResource(R.drawable.ic_circle_blue);
                    ((FeedTypeViewHolder) holder).imageViewProfileType.setImageResource(R.drawable.ic_location_blue);
                    break;
            }


            Random rand = new Random();
            int second = rand.nextInt(4) + 2;

            int first;
            if (second == 2)
                first = 1;
            else
                first = rand.nextInt(second) + 1;

            if (first == second)
                first = first - 1;
            ((FeedTypeViewHolder) holder).textViewDistance.setText("Distance: " + first + " - " + second + " miles");


            ((FeedTypeViewHolder) holder).relativeLayoutProfile.setOnClickListener(view -> {
                Intent intentFavUserView = new Intent(context, FavoriteUserInfoActivity.class);
                intentFavUserView.putExtra("ActivityName", "FavoriteProfile");
                intentFavUserView.putExtra("fav_user_id", person.getUser_id());
                context.startActivity(intentFavUserView);
            });

        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
