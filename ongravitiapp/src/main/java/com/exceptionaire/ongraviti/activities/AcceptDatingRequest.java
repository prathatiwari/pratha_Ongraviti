package com.exceptionaire.ongraviti.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.AcceptDatingRequestAdapter;
import com.exceptionaire.ongraviti.activities.base.ArcMenu;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.StateChangeListener;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.Ask_for_dating_list;
import com.exceptionaire.ongraviti.model.MyOrbitRequest;
import com.exceptionaire.ongraviti.model.ResponseAcceptRejectDatingRequest;
import com.exceptionaire.ongraviti.model.ResponseDatingRequest;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;


public class AcceptDatingRequest extends BaseDrawerActivity {
    private ProgressDialog progressDialog;
    private ArrayList<MyOrbitRequest> orbitRequests;
    private AcceptDatingRequestAdapter acceptDatingRequestAdapter;
    private ListView list_contact;
    private TextView tv_msg;
    private TextView header;
    private static int HeaderIconSelection = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_accept_dating);
        getLayoutInflater().inflate(R.layout.activity_accept_dating, frameLayout);
        arcMenu.setVisibility(View.GONE);
        tv_msg = (TextView) findViewById(R.id.tv_msg);
        header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText("My Dating Request");
        tv_msg.setText(getString(R.string.no_dating_requests));
        acceptDatingRequestAdapter = new AcceptDatingRequestAdapter(new ArrayList<Ask_for_dating_list>(), AcceptDatingRequest.this, this);
        progressDialog = new CustomProgressDialog(AcceptDatingRequest.this, "Loading...");
        list_contact = (ListView) findViewById(R.id.list_view_contact);
        list_contact.setAdapter(acceptDatingRequestAdapter);

        final OvershootInterpolator[] interpolator = {new OvershootInterpolator()};
        arcMenu = (ArcMenu) findViewById(R.id.arcMenu);
        arcMenu.setRadius(getResources().getDimension(R.dimen.radius));

        arcMenu.setStateChangeListener(new StateChangeListener() {
            @Override
            public void onMenuOpened() {
                interpolator[0] = new OvershootInterpolator();
                float rotationAnim = ViewCompat.getRotation(arcMenu.fabMenu);
                ViewCompat.animate(arcMenu.fabMenu).
                        rotation(rotationAnim + 360f).
                        withLayer().
                        setDuration(300).
                        setInterpolator(interpolator[0]).
                        start();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        arcMenu.fabMenu.setImageDrawable(ContextCompat.getDrawable(AcceptDatingRequest.this, R.drawable.ic_action_close));
                    }
                }, 300);
            }

            @Override
            public void onMenuClosed() {
                arcMenu.fabMenu.setImageDrawable(ContextCompat.getDrawable(AcceptDatingRequest.this, R.drawable.ic_fab_menu));
            }
        });
        findViewById(R.id.fab_ic_search).setOnClickListener(subMenuClickListener);
        findViewById(R.id.fab_ic_dating).setOnClickListener(subMenuClickListener);
        findViewById(R.id.fab_ic_business).setOnClickListener(subMenuClickListener);

        if (isNetworkAvailable()) {
            showDatingRequestList();
        } else {
            CommonDialogs.showMessageDialog(AcceptDatingRequest.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    private View.OnClickListener subMenuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            arcMenu.toggleMenu();
            switch (v.getId()) {
                case R.id.fab_ic_search:

                    HeaderIconSelection = 0;
                    Intent intentSearch = new Intent(AcceptDatingRequest.this, SearchActivity.class);
                    startActivity(intentSearch);
                    // finish();
                    break;

                case R.id.fab_ic_dating:
                    HeaderIconSelection = 1;
                    Intent intentDating = new Intent(AcceptDatingRequest.this, AskForDating.class);
                    startActivity(intentDating);
                    finish();
                    break;
                case R.id.fab_ic_business:
                    HeaderIconSelection = 2;
                    Intent intentBusiness = new Intent(AcceptDatingRequest.this, AskForBusiness.class);
                    startActivity(intentBusiness);
                    // finish();
                    break;
            }
        }
    };

    @Override
    public void onResume() {
        if (isNetworkAvailable()) {
            showDatingRequestList();
        } else {
            CommonDialogs.showMessageDialog(AcceptDatingRequest.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
        super.onResume();
    }

    private void showDatingRequestList() {

        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(AcceptDatingRequest.this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseDatingRequest> loginCall = loginRequest.getMyDatingRequestList(AppSettings.getLoginUserId(), "1");
        loginCall.enqueue(new Callback<ResponseDatingRequest>() {
            @Override
            public void onResponse(Call<ResponseDatingRequest> call, retrofit2.Response<ResponseDatingRequest> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        ResponseDatingRequest responseMyOrbitRequests = response.body();
                        if (responseMyOrbitRequests != null && responseMyOrbitRequests.getAsk_for_dating_list() != null && responseMyOrbitRequests.getAsk_for_dating_list().size() > 0) {
                            List<Ask_for_dating_list> myOrbitRequestArrayList = response.body().getAsk_for_dating_list();
                            acceptDatingRequestAdapter.setOrbitRequestList(myOrbitRequestArrayList);
                            tv_msg.setVisibility(View.GONE);
                            list_contact.setVisibility(View.VISIBLE);
                        } else {
                            tv_msg.setVisibility(View.VISIBLE);
                            list_contact.setVisibility(View.GONE);
                        }
                    } else {
                        tv_msg.setVisibility(View.VISIBLE);
                        list_contact.setVisibility(View.GONE);
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseDatingRequest> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Log.d("", "## onErrorResponse : " + t.toString());
            }
        });
    }

    public void acceptRejectDatingRequest(final String senderUserId, String invite_status, String ask_for_date_id, final String flag, final int position) {

        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(AcceptDatingRequest.this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = format.format(new Date());
        if (flag == "0") {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface loginRequest = retrofit.create(ApiInterface.class);
            Call<ResponseAcceptRejectDatingRequest> loginCall = loginRequest.acceptRejectDatingRequest(AppSettings.getLoginUserId(), senderUserId, invite_status, ask_for_date_id, currentDateandTime);
            loginCall.enqueue(new Callback<ResponseAcceptRejectDatingRequest>() {
                @Override
                public void onResponse(Call<ResponseAcceptRejectDatingRequest> call, retrofit2.Response<ResponseAcceptRejectDatingRequest> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Log.e("Response=============", "" + response.body().toString());

                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        final Dialog dialog = CommonDialogs.showMessageDialog(AcceptDatingRequest.this, getResources().getString(R.string.ongravity), response.body().getMsg());
                        Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                        dialog_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                acceptDatingRequestAdapter.removeItem(position);
                            }
                        });

                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        CommonDialogs.showMessageDialog(AcceptDatingRequest.this, getResources().getString(R.string.ongravity), "Something went wrong...Please try again");
                    }
                }

                @Override
                public void onFailure(Call<ResponseAcceptRejectDatingRequest> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    Toast.makeText(AcceptDatingRequest.this, t.toString(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime1 = format1.format(new Date());
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface loginRequest = retrofit.create(ApiInterface.class);
            Call<ResponseAcceptRejectDatingRequest> loginCall = loginRequest.acceptRejectDatingRequest(AppSettings.getLoginUserId(), senderUserId, invite_status, ask_for_date_id, currentDateandTime1);
            loginCall.enqueue(new Callback<ResponseAcceptRejectDatingRequest>() {
                @Override
                public void onResponse(Call<ResponseAcceptRejectDatingRequest> call, retrofit2.Response<ResponseAcceptRejectDatingRequest> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Log.e("Response=============", "" + response.body().toString());

                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        final Dialog dialog = CommonDialogs.showMessageDialog(AcceptDatingRequest.this, getResources().getString(R.string.ongravity), response.body().getMsg());
                        Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                        dialog_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                acceptDatingRequestAdapter.removeItem(position);
                            }
                        });

                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        CommonDialogs.showMessageDialog(AcceptDatingRequest.this, getResources().getString(R.string.ongravity), "Something went wrong...Please try again");
                    }
                }

                @Override
                public void onFailure(Call<ResponseAcceptRejectDatingRequest> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    Toast.makeText(AcceptDatingRequest.this, getResources().getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
