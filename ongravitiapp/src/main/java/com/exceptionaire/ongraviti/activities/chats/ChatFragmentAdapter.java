package com.exceptionaire.ongraviti.activities.chats;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.exceptionaire.ongraviti.R;
import com.halzhang.android.library.BottomTabFragmentPagerAdapter;

/**
 * Created by bhakti on 12/9/2016.
 */

public class ChatFragmentAdapter extends BottomTabFragmentPagerAdapter {
    private ChatsMainActivity m_Context = null;

    public ChatFragmentAdapter(ChatsMainActivity context, FragmentManager fm) {
        super(fm);
        m_Context = context;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return ContactFragment.newInstance();
        else /*if (position == 1)*/
            return ChatFragment.newInstance(m_Context);
    }

    @Override
    public int getPageIcon(int position) {
        if (position == 0)
            return R.drawable.tab_chat;
        else /*if (position == 1)*/
            return R.drawable.tab_contact;
        /*else
            return R.drawable.tab_profile;*/
    }

    @Override
    public int[] getTabViewIds() {
        int[] ids = new int[3];
        ids[0] = R.id.tab0;
        ids[1] = R.id.tab1;
//        ids[3] = R.id.tab3;
        return ids;
    }

    @Override
    public CharSequence getPageTitle(int position) {
//        if (position == 0)
//            return "Chats";
//        else/* if (position == 1)*/
//            return "Initiate chat";
        /*else
            return "Profile";*/
        return null;
    }
}
