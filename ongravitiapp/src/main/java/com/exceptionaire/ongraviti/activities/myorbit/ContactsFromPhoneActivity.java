package com.exceptionaire.ongraviti.activities.myorbit;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.core.OnGravitiApp;
import com.exceptionaire.ongraviti.database.DaoSession;
import com.exceptionaire.ongraviti.database.User_dataDao;
import com.exceptionaire.ongraviti.listener_interface.RemoveDuplicateContactListener;
import com.exceptionaire.ongraviti.model.Contact;
import com.exceptionaire.ongraviti.model.ResponseCompareContactList;
import com.exceptionaire.ongraviti.database.User_data;
import com.google.gson.Gson;

import org.greenrobot.greendao.query.Query;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 11/8/16.
 */

public class ContactsFromPhoneActivity extends BaseActivity {
    private CustomProgressDialog progressDialog;
    private String key;
    public static ResponseCompareContactList responseCompareContactList;
    private DaoSession daoSession;
    private User_dataDao user_dataDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.layout_listview);
        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, "DISPLAY_NAME ASC");

        List<Contact> contactList = new ArrayList<>();
        while (cursor != null && cursor.moveToNext()) {
            String contact_name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String contact_number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String contact_email = " ";
            Contact contact = new Contact(contact_number, contact_name, "", contact_email);
            contactList.add(contact);
        }
        if (!contactList.isEmpty() && contactList.size() > 0) {
            progressDialog = new CustomProgressDialog(ContactsFromPhoneActivity.this, "Syncing Contacts.....");
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            compareUsers(contactList);
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    Boolean isFound = false;
    Boolean isTableEmpty = false;

    private void compareUsers(List<Contact> contactList) {
        progressDialog = new CustomProgressDialog(this, "Loading contacts...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        progressDialog.show();

        RemoveDuplicateContacts removeDuplicateContacts = new RemoveDuplicateContacts(contactList, new RemoveDuplicateContactListener() {
            @Override
            public void getResult(List<Contact> contactList) {

                String someurl = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/compareUsers";
                JSONArray array = new JSONArray();
                JSONObject jsonObject = null;
                try {
                    JSONObject j = new JSONObject();

                    for (int i = 0; i < contactList.size(); i++) {

                        jsonObject = new JSONObject();
                        jsonObject.put("name", contactList.get(i).getContact_name());
                        jsonObject.put("Mobile_nos", contactList.get(i).getContact_number());
                        jsonObject.put("Original_Mobile_nos", contactList.get(i).getOriginal_contact_number());
                        array.put(jsonObject);
                    }
                    key = String.valueOf(j.put("data", array));
                    Log.e("JSON", j.toString());
                    Log.e("Array", "" + array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                StringRequest stringRequest = new StringRequest(
                        Request.Method.POST, someurl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("RESPONSE============", response);

                                if (response != null && !response.equals("")) {

                                    responseCompareContactList = new Gson().fromJson(response, ResponseCompareContactList.class);
                                    if (responseCompareContactList.getStatus().equals("success") && responseCompareContactList.getUser_data() != null && responseCompareContactList.getUser_data().length > 0) {

                                        // get the note DAO
                                        daoSession = ((OnGravitiApp) getApplication()).getDaoSession();
                                        user_dataDao = daoSession.getUser_dataDao();

                                        Query<User_data> user_dataQuery = user_dataDao.queryBuilder().orderAsc(User_dataDao.Properties.Name).build();
                                        List<User_data> user_dataListDB = user_dataQuery.list();
                                        if (user_dataListDB != null && user_dataListDB.size() > 0)
                                            isTableEmpty = false;
                                        else
                                            isTableEmpty = true;

                                        new GetContacts(responseCompareContactList, user_dataListDB, 1).execute();

                                    } else {
                                        Toast.makeText(ContactsFromPhoneActivity.this, "No contacts found !", Toast.LENGTH_LONG).show();
                                        finish();
                                        if (progressDialog != null && progressDialog.isShowing())
                                            progressDialog.dismiss();
                                    }
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();

                            Log.d("", "## onErrorResponse : " + error.toString());

                            error.printStackTrace();
                            String errorMessage = getString(R.string.error_connection_generic);
                            if (error instanceof NoConnectionError)
                                errorMessage = getString(R.string.error_no_connection);
                            else if (error instanceof AuthFailureError)
                                errorMessage = getString(R.string.error_authentication);
                            else if (error instanceof NetworkError)
                                errorMessage = getString(R.string.error_network_error);
                            else if (error instanceof ParseError)
                                errorMessage = getString(R.string.error_parse_error);
                            else if (error instanceof ServerError)
                                errorMessage = getString(R.string.error_server_error);
                            else if (error instanceof TimeoutError)
                                errorMessage = getString(R.string.error_connection_timeout);
                            Toast.makeText(ContactsFromPhoneActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                            Intent intentHome = new Intent(ContactsFromPhoneActivity.this, HomeUserPostsActivity.class);
                            startActivity(intentHome);

                        } catch (Exception e) {
                            Log.e("", "## error : " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> map = new HashMap<>();
                        map.put("user_id", AppSettings.getLoginUserId());
                        map.put("profile_cat_id", "1");
                        map.put("data", key);
                        Log.e("MAP", "" + map);
                        return map;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(ContactsFromPhoneActivity.this);

                stringRequest.setRetryPolicy(new

                        DefaultRetryPolicy(20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

                );
                requestQueue.add(stringRequest);
            }
        });
        removeDuplicateContacts.execute();
    }

    private class RemoveDuplicateContacts extends AsyncTask<Void, Void, List<Contact>> {
        boolean isFound = false;
        private List<Contact> contactList;
        private RemoveDuplicateContactListener removeDuplicateContactListener;


        public RemoveDuplicateContacts(List<Contact> contactList, RemoveDuplicateContactListener removeDuplicateContactListener) {
            this.contactList = contactList;
            this.removeDuplicateContactListener = removeDuplicateContactListener;
        }

        @Override
        protected List<Contact> doInBackground(Void... voids) {

            /*Removing Duplicate from phone Contact list*/
            List<Contact> contactListFiltered = new ArrayList<>();
            for (Contact contact : contactList) {
                isFound = false;
                /*filter digits only if phone number contains any other characters like <-> or <spaces>*/
                contact.setOriginal_contact_number(contact.getContact_number().replaceAll("[^0-9\\+]", ""));
                contact.setContact_number(contact.getContact_number().replaceAll("[^0-9]", ""));

                for (Contact contactNew : contactListFiltered) {
                    if (contactNew.getContact_number().equals(contact.getContact_number())) {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound)
                    contactListFiltered.add(contact);
            }


            return contactListFiltered;
        }

        @Override
        protected void onPostExecute(List<Contact> contactList) {
            super.onPostExecute(contactList);
            removeDuplicateContactListener.getResult(contactList);
        }
    }

    private class GetContacts extends AsyncTask<Void, Void, Void> {

        ResponseCompareContactList responseCompareContactList;
        List<User_data> user_dataListDB;
        int whichArray = 0;

        public GetContacts(ResponseCompareContactList responseCompareContactList, List<User_data> user_dataListDB, int whichArray) {
            this.responseCompareContactList = responseCompareContactList;
            this.user_dataListDB = user_dataListDB;
            this.whichArray = whichArray;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            if (whichArray == 1) {
                if (responseCompareContactList.getUser_data() != null) {
                    User_data[] user_dataArray = responseCompareContactList.getUser_data();

                    for (int i = 0; i < user_dataArray.length; i++) {
                        User_data user_data = user_dataArray[i];
                        user_data.setOriginal_Mobile_nos(user_data.getContact_number());

                        if (isTableEmpty)
                            user_dataDao.insert(user_data);
                        else {
                            isFound = false;
                            for (User_data user_dataNewAlreadyInserted : user_dataListDB) {
                                                            /*filter digits only if phone number contains any other characters like <-> or <spaces>*/
                                if (user_data.getContact_number().equals(user_dataNewAlreadyInserted.getContact_number())) {
                                    isFound = true;
                                    user_data.setInvite_status(user_dataNewAlreadyInserted.getInvite_status());
                                    break;
                                }
                            }
                            if (!isFound)
                                user_dataDao.insertOrReplace(user_data);
                        }
                    }
                }
            } else {
                User_data[] Need_to_orbit_requestArray = responseCompareContactList.getNeed_to_orbit_request();
                for (int i = 0; i < Need_to_orbit_requestArray.length; i++) {

                    User_data need_to_orbit_request = Need_to_orbit_requestArray[i];
                    need_to_orbit_request.setOriginal_Mobile_nos(need_to_orbit_request.getContact_number());

                    if (isTableEmpty)
                        user_dataDao.insert(need_to_orbit_request);
                    else {
                        isFound = false;

                        for (User_data friendContactNewAlreadyInserted : user_dataListDB) {//friendContactListDB) {
                                                            /*filter digits only if phone number contains any other characters like <-> or <spaces>*/
                            if (need_to_orbit_request.getContact_number().equals(friendContactNewAlreadyInserted.getContact_number())) {
                                isFound = true;
                                need_to_orbit_request.setInvite_status(friendContactNewAlreadyInserted.getInvite_status());
                                break;
                            }
                        }
                        if (!isFound)
                            user_dataDao.insertOrReplace(need_to_orbit_request);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (whichArray == 1) {
                new GetContacts(responseCompareContactList, user_dataListDB, 2).execute();
            } else {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Intent intent = new Intent();
                intent.putExtra("friendList", true);
                setResult(213, intent);
                finish();
            }
        }
    }
}
