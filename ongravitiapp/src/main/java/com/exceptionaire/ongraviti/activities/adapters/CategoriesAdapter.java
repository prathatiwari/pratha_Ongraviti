package com.exceptionaire.ongraviti.activities.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.model.Categories;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesHolder> {

    private ArrayList<Categories> listCategories;
    private SparseBooleanArray mSparseBooleanArray;
    private String[] selectedCategoryArray;

    class CategoriesHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        CheckBox mCheckBox;

        CategoriesHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tvTitle);
            mCheckBox = view.findViewById(R.id.chkEnable);
        }
    }

    public CategoriesAdapter(Context context, ArrayList<Categories> categories, String[] arraySelected) {
        this.listCategories = categories;
        mSparseBooleanArray = new SparseBooleanArray();
        this.selectedCategoryArray = arraySelected;
    }

    public ArrayList<Categories> getCheckedItems() {
        ArrayList<Categories> mTempArry = new ArrayList<>();
        for (int i = 0; i < listCategories.size(); i++) {
            if (mSparseBooleanArray.get(i)) {
                mTempArry.add(listCategories.get(i));
            }
        }
        return mTempArry;
    }

    @Override
    public CategoriesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_multi_select, parent, false);

        return new CategoriesHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesHolder holder, int position) {

        Categories categories = listCategories.get(position);
        if (categories != null) {
            holder.tvTitle.setText(listCategories.get(position).toString());
            holder.mCheckBox.setTag(position);

            for (String aSelectedCategoryArray : selectedCategoryArray) {

                if (aSelectedCategoryArray.length() > 0) {
                    if (Integer.parseInt(aSelectedCategoryArray) == Integer.parseInt(categories.getId())) {
                        holder.mCheckBox.setChecked(true);
                        mSparseBooleanArray.put((Integer) holder.mCheckBox.getTag(), true);
                        break;
                    } else {
                        holder.mCheckBox.setChecked(false);
                        mSparseBooleanArray.put((Integer) holder.mCheckBox.getTag(), false);
                    }
                }
            }

            // holder.mCheckBox.setChecked(mSparseBooleanArray.get(position));
            holder.mCheckBox.setOnCheckedChangeListener((compoundButton, isChecked) -> mSparseBooleanArray.put((Integer) compoundButton.getTag(), isChecked));
        }
    }

    @Override
    public int getItemCount() {

        if (listCategories != null)
            if (listCategories.size() > 0)
                return listCategories.size();
        return 0;
    }
}