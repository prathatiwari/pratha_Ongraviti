package com.exceptionaire.ongraviti.activities.base.search;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.myorbit.AddPeopleToOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.ResponseForSearchQuery;
import com.exceptionaire.ongraviti.model.SearchData;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class AddOrbitAutoCompleteTextChangedListener implements TextWatcher {

    public static final String TAG = "CustomAutoCompleteTextChangedListener.java";
    Context context;
    int activityNumber = 0;

    public AddOrbitAutoCompleteTextChangedListener(Context context, int activityNumber) {
        this.context = context;
        this.activityNumber = activityNumber;
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub

    }


    int countFilter = 0;

    @SuppressLint("LongLogTag")
    @Override
    public void onTextChanged(CharSequence userInput, int start, int before, int count) {

        try {
            // if you want to see in the logcat what the user types
            Log.e(TAG, "input:" + userInput);
            if (countFilter == 0) {
                getSearchList(" ");
                countFilter++;
            } else {
                getSearchList(userInput.toString().trim());
            }
            // get suggestions from the database
//            SearchData[] myObjs = mainActivity.databaseH.read(userInput.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSearchList(String queryString) {

//        progressDialog = new com.exceptionaire.ongraviti.activities.base.CommonDialogs().showProgressBar(getActivity());
//        progressDialog.show();

        Retrofit retrofit = ApiClient.getClient();
        final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<ResponseForSearchQuery> accessTokenCall = requestInterface.getSearchReesults(queryString, AppSettings.getLoginUserId());
        accessTokenCall.enqueue(new Callback<ResponseForSearchQuery>() {
            @Override
            public void onResponse(Call<ResponseForSearchQuery> call, Response<ResponseForSearchQuery> response) {

//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.body().getStatus().equals(AppConstants.RESPONSE_SUCCESS)) {

                        ArrayList<SearchData> arrayList = new ArrayList<>(Arrays.asList(response.body().getResult()));
//                        mFAQItemList = arrayList;
//                        mSearchDataAdapter.notifyDataSetChanged();

                        AddPeopleToOrbitActivity mainActivity = ((AddPeopleToOrbitActivity) context);
                        mainActivity.myAdapter.notifyDataSetChanged();
                        mainActivity.itemDataArray = response.body().getResult();
                        mainActivity.myAdapter = new AutocompleteCustomArrayAdapter(mainActivity, 2, R.layout.list_view_row, mainActivity.itemDataArray);
                        mainActivity.list_view_contact.setAdapter(mainActivity.myAdapter);
                    }
                } else {
                    Log.e("Error: ", "No FAQ list available.");
//                    Toast.makeText(getActivity(), "No FAQ list available.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseForSearchQuery> call, Throwable t) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
//                Toast.makeText(getActivity(), "Something went wrong, please try after some time.", Toast.LENGTH_SHORT).show();
                Log.e("Error: ", t.toString());
            }
        });
    }
}