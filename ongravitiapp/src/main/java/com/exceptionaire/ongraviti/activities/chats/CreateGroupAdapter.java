package com.exceptionaire.ongraviti.activities.chats;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.myorbit.PeopleInOrbitFragment;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.OrbitContact;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by root on 16/11/16.
 */

public class CreateGroupAdapter extends BaseAdapter {
    List<OrbitContact> orbitFriendList;
    Context context;
    //    ImageLoader imageLoader = OnGravitiApp.getInstance().getImageLoader();
    private List<OrbitContact> fileredlist;

    public void setOrbitFriendList(List<OrbitContact> orbitFriends) {
        //this.orbitFriendList.clear();
        this.orbitFriendList = orbitFriends;
        fileredlist.clear();
        fileredlist.addAll(orbitFriends);
        notifyDataSetChanged();
    }

    public CreateGroupAdapter(List<OrbitContact> contactList, Context context) {
        this.orbitFriendList = contactList;
        fileredlist = new ArrayList<>();
        this.context = context;
    }

    public void removeItem(int position) {
        orbitFriendList.remove(position);
        notifyDataSetChanged();
    }

    public List<OrbitContact> getSelectedUsers() {
        return orbitFriendList;
    }

    private class Holder {
        TextView txtName;
        CheckBox checkBoxSelectedUser;
        RoundedImageView image;
        RelativeLayout relative_list_item;
    }

    @Override
    public int getCount() {
        return orbitFriendList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return orbitFriendList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final OrbitContact orbitFriend = orbitFriendList.get(position);
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.create_chat_group_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");
            // holder.tvNo = (TextView) convertView.findViewById(R.id.No);

            holder.txtName = (TextView) convertView.findViewById(R.id.txt_orbit_name);
            holder.checkBoxSelectedUser = (CheckBox) convertView.findViewById(R.id.checkboxSelectedMember);
            holder.image = (RoundedImageView) convertView.findViewById(R.id.user_profile_pic);
            holder.relative_list_item = (RelativeLayout) convertView.findViewById(R.id.relative_list_item);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.txtName.setText(orbitFriend.getOrbit_name());
//        Picasso.with(context).load(AppConstants.PROFILE_PICTURE_PATH + "/" + orbitFriend.getProfile_picture()).fit().centerCrop()
//                .placeholder(R.drawable.profile)
//                .error(R.drawable.profile)
//                .into(holder.image);

        if (  orbitFriend.getProfile_picture() != null && !  orbitFriend.getProfile_picture().isEmpty() ) {
            if (!  orbitFriend.getProfile_picture().contains("https:")) {
                Picasso.with(context)
                        .load(AppConstants.PROFILE_PICTURE_PATH + "/" + orbitFriend.getProfile_picture())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            } else if ( orbitFriend.getProfile_picture().contains("https:")) {
                Picasso.with(context)
                        .load(orbitFriend.getProfile_picture())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            }
        }
        holder.relative_list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("adptr", "## relative_list_item click : " + orbitFriend.getOrbit_id());
                Intent intentProfile = new Intent(context, FavoriteUserInfoActivity.class);
                intentProfile.putExtra("ActivityName", "OrbitProfile");
                intentProfile.putExtra("OrbitUserID", "" + orbitFriend.getOrbit_id());
                context.startActivity(intentProfile);
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("adptr", "## image click : " + orbitFriend.getOrbit_id());
                Intent intentProfile = new Intent(context, FavoriteUserInfoActivity.class);
                intentProfile.putExtra("ActivityName", "OrbitProfile");
                intentProfile.putExtra("OrbitUserID", "" + orbitFriend.getOrbit_id());
                context.startActivity(intentProfile);
            }
        });

        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("adptr", "## image click : " + orbitFriend.getOrbit_id());
                Intent intentProfile = new Intent(context, FavoriteUserInfoActivity.class);
                intentProfile.putExtra("ActivityName", "OrbitProfile");
                intentProfile.putExtra("OrbitUserID", "" + orbitFriend.getOrbit_id());
                context.startActivity(intentProfile);
            }
        });

        if (orbitFriend.getSelected() != null)
            holder.checkBoxSelectedUser.setSelected(orbitFriend.getSelected());
        else
            holder.checkBoxSelectedUser.setSelected(false);


        holder.checkBoxSelectedUser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    orbitFriend.setSelected(true);
                } else {
                    orbitFriend.setSelected(false);
                }
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewProfile();
                //context.startActivity(new Intent(context,ViewSocialBasicProfile.class));
            }
        });
        return convertView;
    }

    private void viewProfile() {

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        orbitFriendList.clear();
        if (charText.length() == 0) {
            orbitFriendList.addAll(fileredlist);
        } else {
            for (OrbitContact wp : fileredlist) {
                if (wp.getOrbit_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    orbitFriendList.add(wp);
                }
            }
        }
        if (orbitFriendList.size() == 0) {
            CreateGroupActivity.tv_msg.setVisibility(View.VISIBLE);
        } else {
            CreateGroupActivity.tv_msg.setVisibility(View.GONE);
        }
        notifyDataSetChanged();
    }
}
