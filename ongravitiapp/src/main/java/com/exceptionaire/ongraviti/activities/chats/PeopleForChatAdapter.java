package com.exceptionaire.ongraviti.activities.chats;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.OrbitFriend;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by root on 16/11/16.
 */

public class PeopleForChatAdapter extends BaseAdapter {
    List<OrbitFriend> orbitFriendList;
    Context context;
//    ImageLoader imageLoader = OnGravitiApp.getInstance().getImageLoader();
    private PeopleForChatFragment fragment;
    private List<OrbitFriend> fileredlist;

    public void setOrbitFriendList(List<OrbitFriend> orbitFriends) {
        //this.orbitFriendList.clear();
        this.orbitFriendList = orbitFriends;
        fileredlist.clear();
        fileredlist.addAll(orbitFriends);
        notifyDataSetChanged();
    }

    public PeopleForChatAdapter(List<OrbitFriend> contactList, Context context, PeopleForChatFragment fragment) {
        this.orbitFriendList = contactList;
        fileredlist = new ArrayList<>();
        this.context = context;
        this.fragment = fragment;
    }

    public void removeItem(int position) {
        orbitFriendList.remove(position);
        notifyDataSetChanged();
    }

    private class Holder {
        TextView txtName;
        ImageButton ibtn_remove,ibtn_block;
        RoundedImageView image;
        RelativeLayout relative_list_item;
    }

    @Override
    public int getCount() {
        return orbitFriendList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return orbitFriendList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final OrbitFriend orbitFriend = orbitFriendList.get(position);
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.people_in_orbit_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");
            // holder.tvNo = (TextView) convertView.findViewById(R.id.No);

            holder.txtName = (TextView) convertView.findViewById(R.id.txt_orbit_name);
            holder.ibtn_remove = (ImageButton) convertView.findViewById(R.id.ibtn_remove);
            holder.ibtn_block= (ImageButton) convertView.findViewById(R.id.ibtn_block);
            holder.image = (RoundedImageView) convertView.findViewById(R.id.user_profile_pic);
            holder.relative_list_item=(RelativeLayout)convertView.findViewById(R.id.relative_list_item);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.txtName.setText(orbitFriend.getUserName());

//        Picasso.with(context).load(AppConstants.PROFILE_PICTURE_PATH + "/" + orbitFriend.getProfilePic()).fit().centerCrop()
//                .placeholder(R.drawable.profile)
//                .error(R.drawable.profile)
//                .into(holder.image);

        if ( orbitFriend.getProfilePic() != null && ! orbitFriend.getProfilePic().isEmpty() ) {
            if (! orbitFriend.getProfilePic().contains("https:")) {
                Picasso.with(context)
                        .load(AppConstants.PROFILE_PICTURE_PATH + "/" + orbitFriend.getProfilePic())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            } else if ( orbitFriend.getProfilePic().contains("https:")) {
                Picasso.with(context)
                        .load( orbitFriend.getProfilePic())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            }
        }

        holder.relative_list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("adptr","## relative_list_item click : "+orbitFriend.getUserID());
                Intent intentProfile=new Intent(context, FavoriteUserInfoActivity.class);
                intentProfile.putExtra("ActivityName","OrbitProfile");
                intentProfile.putExtra("OrbitUserID",""+orbitFriend.getUserID());
                context.startActivity(intentProfile);
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("adptr","## image click : "+orbitFriend.getUserID());
                Intent intentProfile=new Intent(context, FavoriteUserInfoActivity.class);
                intentProfile.putExtra("ActivityName","OrbitProfile");
                intentProfile.putExtra("OrbitUserID",""+orbitFriend.getUserID());
                context.startActivity(intentProfile);
            }
        });

        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("adptr","## image click : "+orbitFriend.getUserID());
                Intent intentProfile=new Intent(context, FavoriteUserInfoActivity.class);
                intentProfile.putExtra("ActivityName","OrbitProfile");
                intentProfile.putExtra("OrbitUserID",""+orbitFriend.getUserID());
                context.startActivity(intentProfile);
            }
        });

        holder.ibtn_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure to block "+orbitFriend.getUserName()+" from your orbit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //TODO
                                //removeItem(position);
                                fragment.removeFromOrbit(orbitFriend.getUserID(), position,"3");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        holder.ibtn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure to remove "+orbitFriend.getUserName()+" from your orbit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //TODO
                                //removeItem(position);
                                fragment.removeFromOrbit(orbitFriend.getUserID(), position,"4");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewProfile();
                //context.startActivity(new Intent(context,ViewSocialBasicProfile.class));
            }
        });
        return convertView;
    }

    private void viewProfile() {

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        orbitFriendList.clear();
        if (charText.length() == 0) {
            orbitFriendList.addAll(fileredlist);
        } else {
            for (OrbitFriend wp : fileredlist) {
                if (wp.getUserName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    orbitFriendList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
