package com.exceptionaire.ongraviti.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.PeopleNearByAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.Person;
import com.exceptionaire.ongraviti.utilities.support.CircleImageView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class PeopleNearByMeActivity extends BaseDrawerActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, LocationListener {


    private GoogleMap googleMap;
    private Context context;
    private RadioGroup radioGroup1;
    private ArrayList<Person> personList;
    private RecyclerView recyclerViewNearByFriends;
    private PeopleNearByAdapter peopleNearByAdapter;
    private CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_people_near_by_me, frameLayout);
        TextView header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText(getResources().getString(R.string.people_nearby));
        context = this;
        arcMenu.setVisibility(View.VISIBLE);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        personList = new ArrayList<>();
        recyclerViewNearByFriends = (RecyclerView) findViewById(R.id.recycler_view_near_by_friends);
        peopleNearByAdapter = new PeopleNearByAdapter(personList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PeopleNearByMeActivity.this, OrientationHelper.VERTICAL, false);
        recyclerViewNearByFriends.setLayoutManager(linearLayoutManager);
        recyclerViewNearByFriends.setItemAnimator(new DefaultItemAnimator());
        recyclerViewNearByFriends.setAdapter(peopleNearByAdapter);

        radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioSocial:
                        filter("1");
                        break;
                    case R.id.radioDaing:
                        filter("2");
                        break;
                    case R.id.radioBusiness:
                        filter("3");
                        break;
                    case R.id.radioAll:
                        filter("all");
                        break;
                    default:
                        filter("all");
                        break;
                }
            }
        });
    }

//    @Override
//    public void onResume() {
//        getPersonList();
//        super.onResume();
//    }

    public void filter(String type) {
        List<Person> fileredlist = new ArrayList<>();
        type = type.toLowerCase(Locale.getDefault());
        fileredlist.clear();
        if (type.equalsIgnoreCase("all")) {
            fileredlist.addAll(personList);
        } else {
            for (Person person : personList) {
                if (person.getProfileType().toLowerCase(Locale.getDefault()).equalsIgnoreCase(type)) {
                    fileredlist.add(person);
                }
            }
        }

        addMarkers(fileredlist);
        peopleNearByAdapter.setListUserPost(fileredlist);
    }

    private void addMarkers(List<Person> persons) {
        if (googleMap != null) {
            googleMap.clear();
            for (final Person person : persons) {
                Log.d("markers", person.getName());

                /*Marker marker = googleMap.addMarker(new MarkerOptions()
                        .position(person.getmPosition())
                        .icon(BitmapDescriptorFactory.fromBitmap(writeTextOnDrawable(R.drawable.ic_launcher, person.getName()))));*/

                Marker marker = googleMap.addMarker(new MarkerOptions().position(person.getmPosition())
                        .title(person.getName())
                        .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView("", person.getProfileType(), "")))
                        .anchor(0.5f, 1));

//                marker.showInfoWindow();
                marker.setTag(person.getUser_id());
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(person.getmPosition(), 20));
            }
        }
    }

    public static int convertToPixels(Context context, int nDP) {
        final float conversionScale = context.getResources().getDisplayMetrics().density;

        return (int) ((nDP * conversionScale) + 0.5f);

    }

    private Bitmap getMarkerBitmapFromView(String profilePic, String type, String title) {
        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        final RoundedImageView markerImageView = (RoundedImageView) customMarkerView.findViewById(R.id.profile_image);
        ImageView iv_type = (ImageView) customMarkerView.findViewById(R.id.iv_type);

//        switch (type) {
//            case "3"://busienss
//                iv_type.setImageResource(R.drawable.ic_location_blue);
//                break;
//            case "2"://dating
//                iv_type.setImageResource(R.drawable.ic_location_pink);
//                break;
//            case "1"://social
//                iv_type.setImageResource(R.drawable.ic_locations_green);
//                break;
//        }

        Picasso.with(this)
                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + profilePic)
                .placeholder(R.drawable.profile)
                //.load(AppConstants.PROFILE_PICTURE_PATH + "/" + "b8ae3264e602d8ac8f6ffc9ca1fb4d6f.png")
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                        Log.d("Picasso Image Bitmap", "yes");
                        markerImageView.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

        //markerImageView.setImageResource(resId);
        //Picasso.with(context).load(AppConstants.PROFILE_PICTURE_PATH + "/" + imageUrl).into(markerImageView);
        //markerImageView.setImageUrl(AppConstants.PROFILE_PICTURE_PATH + "/" + imageUrl, imageLoader);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        //Bitmap returnedBitmap = markerImageView.getDrawingCache();

        Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK);
        paint.setTypeface(tf);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTextSize(convertToPixels(context, 13));

        Rect textRect = new Rect();
        paint.getTextBounds(title, 0, title.length(), textRect);

        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);

        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);

        /*if (textRect.width() >= (canvas.getWidth() - 4))     //the padding on either sides is considered as 4, so as to appropriately fit in the text
            paint.setTextSize(convertToPixels(context, 7));  */      //Scaling needs to be used for different dpi's

        //Calculate the positions
        int xPos = (canvas.getWidth() / 2) - 2;     //-2 is for regulating the x position offset

        //"- ((paint.descent() + paint.ascent()) / 2)" is the distance from the baseline to the center.
        int yPos = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2));

        canvas.drawText(title, xPos, yPos, paint);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);

        return returnedBitmap;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        // Enabling MyLocation Layer of Google Map
        googleMap.setMyLocationEnabled(true);
//        googleMap.setOnMarkerClickListener(this);

        //TODO
        //addMarkers(personList);
        getPersonList();
    }

    private void getPersonList() {
        progressDialog = new CustomProgressDialog(this, "Loading...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/get_nearby_people",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            // message = jObject.getString("msg");
                            String status = jObject.getString("status");
                            if (status.equals("success")) {
                                Object obj = jObject.get("nearbylist");
                                if (obj instanceof JSONArray) {
                                    JSONArray requestLists = jObject.getJSONArray("nearbylist");
                                    personList = new ArrayList<Person>();
                                    for (int i = 0; i < requestLists.length(); i++) {
                                        Log.d("requestLists count", "" + i);

                                        JSONObject detailsObject = requestLists.getJSONObject(i);
                                        if (!detailsObject.getString("latitude").equals("") && !detailsObject.getString("longitude").equals("")) {
                                            LatLng position = new LatLng(Double.parseDouble(detailsObject.getString("latitude")), Double.parseDouble(detailsObject.getString("longitude")));
                                            // profile_cat_id = (1-Social,2-Dating,3-Business)
                                            Person request = new Person(detailsObject.getString("user_id"), position, detailsObject.getString("name"), detailsObject.getString("profile_picture"), detailsObject.getString("profile_cat_id"));
                                            personList.add(request);
                                        }
                                    }
                                    if (!personList.isEmpty()) {
                                        addMarkers(personList);

                                        peopleNearByAdapter.setListUserPost(personList);
                                    }
                                } else {
                                }


                            } else if (status.equalsIgnoreCase("error")) {
                                CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongraviti), jObject.getString("msg"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();

                            error.printStackTrace();
                            String errorMessage = getString(R.string.error_connection_generic);
                            if (error instanceof NoConnectionError)
                                errorMessage = getString(R.string.error_no_connection);
                            else if (error instanceof AuthFailureError)
                                errorMessage = getString(R.string.error_authentication);
                            else if (error instanceof NetworkError)
                                errorMessage = getString(R.string.error_network_error);
                            else if (error instanceof ParseError)
                                errorMessage = getString(R.string.error_parse_error);
                            else if (error instanceof ServerError)
                                errorMessage = getString(R.string.error_server_error);
                            else if (error instanceof TimeoutError)
                                errorMessage = getString(R.string.error_connection_timeout);
                            CommonDialogs.dialog_with_one_btn_without_title(PeopleNearByMeActivity.this, errorMessage);
                            /*   Intent intentHome=new Intent(PeopleNearByMeActivity.this,ActivityHomeUserPost.class);
                            startActivity(intentHome);*/
                        } catch (Exception e) {
                            Log.e("notification", "## error : " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", AppSettings.getLoginUserId());
                System.out.println(map);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        /*Location lkn=LocationServices.FusedLocationApi.getLastLocation(googleMap);
        if(myLocation==null){
            myLocation=new Location("");
        }
        if(lkn!=null){

            myLocation.setLatitude(lkn.getLatitude());
            myLocation.setLongitude(lkn.getLongitude());
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                gApiClient, mLocationRequest, this);*/
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        Log.d("Location", "" + location.getLongitude());
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(), location.getLongitude()), 13));

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }
}