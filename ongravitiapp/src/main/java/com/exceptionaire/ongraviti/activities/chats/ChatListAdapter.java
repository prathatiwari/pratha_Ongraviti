package com.exceptionaire.ongraviti.activities.chats;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.chats.ChatsMainActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.OrbitContact;
import com.exceptionaire.ongraviti.quickblox.QBData;
import com.exceptionaire.ongraviti.quickblox.core.utils.Toaster;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.server.Performer;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import com.exceptionaire.ongraviti.activities.chats.ChatActivity;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.core.AppGlobals;
import com.exceptionaire.ongraviti.quickblox.core.ui.adapter.BaseListAdapter;
import com.exceptionaire.ongraviti.quickblox.managers.QbDialogUtils;
import com.squareup.picasso.Picasso;

/**
 * Created by Mikhail on 12/14/2016.
 */

public class ChatListAdapter extends BaseListAdapter<QBChatDialog> {
    private Vector<ViewHolder> m_viewHolder = new Vector<>();
    private LayoutInflater layoutInflater;
    private ChatsMainActivity mActivity;

    private QBUser qbUser = null;

    static class ViewHolder {
        RoundedImageView _userImage;
        TextView _userName;
        TextView _message;
        TextView _date;
        TextView txt_message_time;
        TextView _unreadCount;
        LinearLayout container_contact;
    }

    public ChatListAdapter(Context context, List<QBChatDialog> qbChatDialogList) {
        super(context, qbChatDialogList);
        layoutInflater = LayoutInflater.from(context);
        m_viewHolder.clear();
        mActivity = (ChatsMainActivity) context;
    }

    @Override
    public int getCount() {
        if (objectsList == null)
            return 0;
        return objectsList.size();
    }

    @Override
    public QBChatDialog getItem(int position) {
        return objectsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_chat, null);
            holder = new ViewHolder();
            holder._userImage = (RoundedImageView) convertView.findViewById(R.id.badge_chat);
            holder._userName = (TextView) convertView.findViewById(R.id.txt_contact);
            holder._message = (TextView) convertView.findViewById(R.id.txt_message);
            holder._date = (TextView) convertView.findViewById(R.id.txt_message_date);
            holder.txt_message_time = convertView.findViewById(R.id.txt_message_time);
            holder.container_contact = (LinearLayout) convertView.findViewById(R.id.container_contact);
            holder._unreadCount = (TextView) convertView.findViewById(R.id.text_dialog_unread_count);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final QBChatDialog dialog = getItem(position);
        String strUserNm = QbDialogUtils.getDialogName(dialog);
        holder._userName.setText(strUserNm);
        holder._message.setText(prepareTextLastMessage(dialog));
        long time = dialog.getLastMessageDateSent();
        time = time * 1000;
        String strTime = "";
        String strTime1 = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df1 = new SimpleDateFormat("hh:mm:ss a");
        strTime = df.format(time);
        strTime1 = df1.format(time);

        String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        cal.add(Calendar.DATE, -1);
        String yesterdayDate = dateFormat.format(cal.getTime());
        Log.d("yesterday", yesterdayDate);
        if (strTime.equals(todayDate)) {
            holder._date.setText("Today");
            holder.txt_message_time.setText(strTime1);
        } else if (strTime.equals(yesterdayDate)) {
            holder._date.setText("Yesterday");
            holder.txt_message_time.setText(strTime1);
        } else {
            holder.txt_message_time.setText(strTime1);
            holder._date.setText(strTime);
        }
//        Set<String> dialogsIds = new HashSet<String>();
//        dialogsIds.add("56f3fac3a0eb4786ae00003f");
//        dialogsIds.add("56f3f546a28f9affc0000033");
//
//        QBRestChatService.getTotalUnreadMessagesCount(dialogsIds, null).performAsync(new QBEntityCallback<Integer>() {
//            @Override
//            public void onSuccess(Integer total, Bundle params) {
//                Log.i(TAG, "total unread messages: " + total);
//                Log.i(TAG, "dialog Unread1: " + params.getInt("56f3fac3a0eb4786ae00003f"));
//                Log.i(TAG, "dialog Unread2: " + params.getInt("56f3f546a28f9affc0000033"));
//
//                holder._unreadCount.setText(params.getInt("56f3fac3a0eb4786ae00003f") + "");
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//
//            }
//        });

        Integer unreadMessagesCount = dialog.getUnreadMessageCount();
        if (unreadMessagesCount == null || unreadMessagesCount == 0) {
            holder._unreadCount.setVisibility(View.GONE);
        } else {
            holder._unreadCount.setVisibility(View.VISIBLE);
            holder._unreadCount.setText(String.valueOf(unreadMessagesCount > 99 ? 99 : unreadMessagesCount));
        }
        Integer userId = QbDialogUtils.getOpponentIdForPrivateDialog(dialog);
        final OrbitContact user = mActivity.getUserDataFromUserId(userId);


        if (user != null && user.getQuickblox_id() != null && !user.getQuickblox_id().isEmpty()) {
            try {
                Performer<QBUser> qbUserPerformer = QBUsers.getUser(Integer.parseInt(user.getQuickblox_id()));
                qbUserPerformer.performAsync(new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser qbUserResult, Bundle bundle) {
                        qbUser = qbUserResult;
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Toaster.longToast("This user is not registered to chat server");
                    }
                });

            } catch (Exception e) {
                Toaster.longToast("This user is not registered to chat server");
            }

            String strUrl = dialog.getPhoto();

            if (strUrl != null && !strUrl.isEmpty() && !strUrl.equalsIgnoreCase("null")) {
                QBFile file = new QBFile();
                file.setId(Integer.valueOf(strUrl));

                String publicUrl = file.getPrivateUrl();
//              publicUrl = file.getPublicUrl();

                QBContent.getFile(file).performAsync(new QBEntityCallback<QBFile>() {
                    @Override
                    public void onSuccess(QBFile qbFile, Bundle bundle) {
                        Log.i(TAG, qbFile.getPrivateUrl() + "**********" + qbFile.getPublicUrl());
                        Log.d("mmmmm", qbFile.getPrivateUrl());
                        Glide.with(mActivity)
                                .load(qbFile.getPrivateUrl())
                                .listener(new RequestListener<String, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                        Bitmap bm = BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.profile);
                                        holder._userImage.setImageBitmap(bm);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable resource, String model,
                                                                   Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        return false;
                                    }
                                })
                                .override(AppConstants.PREFERRED_IMAGE_SIZE_PREVIEW, AppConstants.PREFERRED_IMAGE_SIZE_PREVIEW)
                                .error(R.drawable.profile).placeholder(R.drawable.profile)
                                .into(holder._userImage);
                    }

                    @Override
                    public void onError(QBResponseException e) {

                    }
                });
            } else {
                if (user.getProfile_picture() != null) {
                    strUrl = user.getProfile_picture();
                    if (!strUrl.contains("https://graph")) {
                        Picasso.with(mActivity)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + strUrl)
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(holder._userImage);
                    } else if (strUrl.contains("https://graph")) {
                        Picasso.with(mActivity)
                                .load(strUrl)
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(holder._userImage);
                    }
                }
            }

            /*else

            {
                Bitmap bm = BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.profile);
                holder._userImage.setImageBitmap(bm);
            }*/
        }
        m_viewHolder.add(holder);

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (user != null) {
//                    AppGlobals.curChattingUser = user;
//                    ChatActivity.startForResult(mActivity, ChatsMainActivity.REQUEST_DIALOG_ID_FOR_UPDATE, dialog.getDialogId());
//                }
//            }
//        });
        holder.container_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user != null) {
                    Log.i("hello", "" + user.getOrbit_name());
                    AppSettings.setKeyOrbitName(strUserNm);
                    AppGlobals.curChattingUser = user;
                    ChatActivity.startForResult(mActivity, ChatsMainActivity.REQUEST_DIALOG_ID_FOR_UPDATE, dialog.getDialogId());
                }
            }
        });
        return convertView;
    }

    private String prepareTextLastMessage(QBChatDialog chatDialog) {
        if (isLastMessageAttachment(chatDialog)) {
            return "Attachment";
        } else {
            return chatDialog.getLastMessage();
        }
    }

    private boolean isLastMessageAttachment(QBChatDialog dialog) {
        String lastMessage = dialog.getLastMessage();
        Integer lastMessageSenderId = dialog.getLastMessageUserId();
        return TextUtils.isEmpty(lastMessage) && lastMessageSenderId != null;
    }
}
