package com.exceptionaire.ongraviti.activities.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.EditProfileDetailsActivity;
import com.exceptionaire.ongraviti.activities.ProfileMainActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.ShowHidePasswordEditText;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseSetUserInfo;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.google.gson.Gson;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_ERROR;
import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_SUCCESS;

public class EditProfileWhoAmIFragment extends Fragment {
    private ProgressDialog progressDialog;
    String[] arr_relationship_status = {"Select", "Single", "Married", "Currently Separated", "Divorced", "Widow/Widower"};
    String[] arr_religious = {"Select", "Agnostic", "Spiritual", "Atheist", "Christian", "Catholic", "Sikhism", "Buddhism", "Islam", "Chinese traditional religion", "Hinduism"};
    String[] arr_political = {"Select", "Democratic", "Liberal", "Republican", "Undecided", "Not interested"};
    String[] arr_body_type = {"Select", "Slim", "Athletic", "Curvy", "Less offensive"};
    String[] arr_eye_color = {"Select", "Amber", "Black", "Brown", "Green", "Blue", "Grey", "Hazel", "Heterochromic"};
    String[] arr_hair_color = {"Select", "Black", "Dark Blonde", "Dark Brown", "Platinum", "Grey", "Blonde", "Light brown", "Auburn", "Red", "Silver", "Other"};
    String[] arr_height = {"Select", "4'0\"(1.22 mts)", "4'1\"(1.24 mts)", "4'2\"(1.28 mts)", "4'3\"(1.31 mts)", "4'4\"(1.34 mts)", "4'5\"(1.35 mts)", "4'6\"(1.37 mts)", "4'7\"(1.40 mts)", "4'8\"(1.42 mts)", "4'9\"(1.45 mts)", "4'10\"(1.47 mts)", "4'11\"(1.50 mts)", "5'0\"(1.52 mts)", "5'1\"(1.55 mts)", "5'2\"(1.58 mts)", "5'3\"(1.60 mts)", "5'4\"(1.63 mts)", "5'5\"(1.65 mts)", "5'6\"(1.68 mts)", "5'7\"(1.70 mts)", "5'8\"(1.73 mts)", "5'9\"(1.75 mts)", "5'10\"(1.78 mts)", "5'11\"(1.80 mts)", "6'0\"(1.83 mts)", "6'1\"(1.85 mts)", "6'2\"(1.88 mts)", "6'3\"(1.91 mts)", "6'4\"(1.93 mts)", "6'5\"(1.96 mts)", "6'6\"(1.98 mts)", "6'7\"(2.01 mts)", "6'8\"(2.03 mts)", "6'9\"(2.06 mts)", "6'10\"(2.08 mts)", "6'11\"(2.11 mts)", "7' (2.13 mts) plus"};
    String[] arr_smoking = {"Select", "Never", "Daily", "Socially", "Hate it"};
    String[] arr_drinking = {"Select", "Never", "Daily", "Socially", "Hate it"};
    String[] arr_gym = {"Select", "Daily", "Once a Week", "Don’t", "Gains all day"};
    // String[] arr_height = {"Select", "4'0\"(1.22 mts)", "4'1\"(1.24 mts)", "4'2\"(1.28 mts)", "4'3\"(1.31 mts)", "4'4\"(1.34 mts)", "4'5\"(1.35 mts)", "4'6\"(1.37 mts)", "4'7\"(1.40 mts)", "4'8\"(1.42 mts)", "4'9\"(1.45 mts)", "4'10\"(1.47 mts)", "4'11\"(1.50 mts)", "5'0\"(1.52 mts)", "5'1\"(1.55 mts)", "5'2\"(1.58 mts)", "5'3\"(1.60 mts)", "5'4\"(1.63 mts)", "5'5\"(1.65 mts)", "5'6\"(1.68 mts)", "5'7\"(1.70 mts)", "5'8\"(1.73 mts)", "5'9\"(1.75 mts)", "5'10\"(1.78 mts)", "5'11\"(1.80 mts)", "6'0\"(1.83 mts)", "6'1\"(1.85 mts)", "6'2\"(1.88 mts)", "6'3\"(1.91 mts)", "6'4\"(1.93 mts)", "6'5\"(1.96 mts)", "6'6\"(1.98 mts)", "6'7\"(2.01 mts)", "6'8\"(2.03 mts)", "6'9\"(2.06 mts)", "6'10\"(2.08 mts)", "6'11\"(2.11 mts)", "7' (2.13 mts) plus"};
    ResponseGetUserProfileData userProfileDataFromResponse;
    Calendar newDate;
    String genderId = ""; // "0" = male, "1" = female
    String interestedInId = "";
    int age;
    private DatePickerDialog datePicker;

    private RadioGroup radioGroupGender, radioGroupInterest;
    private Button buttonSave, buttonNext;
    private View view;
    private EditText t_view_name, t_view_dob, t_view_weight, edit_text_city, edit_text_traids;
    private RadioButton rdGenMale, rdGenbisexual, rdGenFemale, rdInterestedMale, rdInterestedbisexual, rdInterestedFemale;
    private Spinner spRelationshipStatus, spReligiousBeliefs, spPoliticalBeliefs, spHeight,
            spBodyType, spEyeColor, spHairColor, spSmoking, spDrinking, spGym;
    private SimpleDateFormat dateFormatter;
    private QBUser QuickBloxUser;
    UserBasicInfo userBasicInfo;
    TextView textview_age;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_edit_profile_who_am_i, container, false);
        setActionBar();
        initializeViewComponent();
//        initializeListeners();
        setData();
//        Log.d("amar",AppSettings.getLoginUserId());
        return view;
    }


    private void initializeViewComponent() {
        AppSettings.setProfileStepStatus1(1);


        edit_text_city = view.findViewById(R.id.edit_text_city);
        edit_text_traids = view.findViewById(R.id.edit_text_traids);
        textview_age = view.findViewById(R.id.textview_age);
        t_view_name = view.findViewById(R.id.edit_text_name);
        t_view_dob = view.findViewById(R.id.edit_text_date_of_birth);
        rdGenMale = view.findViewById(R.id.radio_button_gender_male);
        rdGenFemale = view.findViewById(R.id.radio_button_gender_female);
        rdGenbisexual = view.findViewById(R.id.radio_button_gender_bisexual);
        rdInterestedMale = view.findViewById(R.id.radio_button_interest_male);
        rdInterestedFemale = view.findViewById(R.id.radio_button_interest_female);
        rdInterestedbisexual = view.findViewById(R.id.radio_button_interest_bisexual);

        t_view_weight = view.findViewById(R.id.edit_text_weight);
        radioGroupGender = view.findViewById(R.id.radio_group_gender);
        radioGroupInterest = view.findViewById(R.id.radio_group_interest);

        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int id = radioGroup.getCheckedRadioButtonId();
                switch (id) {
                    case R.id.radio_button_gender_male:
                        genderId = "0";
                        break;
                    case R.id.radio_button_gender_female:
                        genderId = "1";
                        break;
                    case R.id.radio_button_gender_bisexual:
                        genderId = "2";
                        break;
                }
            }
        });

        radioGroupInterest.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int id = radioGroup.getCheckedRadioButtonId();
                switch (id) {
                    case R.id.radio_button_interest_male:
                        interestedInId = "0";
                        break;
                    case R.id.radio_button_interest_female:
                        interestedInId = "1";
                        break;
                    case R.id.radio_button_interest_bisexual:
                        interestedInId = "2";
                        break;

                }
            }
        });

        t_view_dob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                datePicker.show();
                return false;
            }
        });

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                t_view_dob.setText(dateFormatter.format(newDate.getTime()));

                Calendar today = Calendar.getInstance();
                age = today.get(Calendar.YEAR) - newDate.get(Calendar.YEAR);
                if (today.get(Calendar.MONTH) < newDate.get(Calendar.MONTH)) {
                    age--;
                } else if (today.get(Calendar.MONTH) == newDate.get(Calendar.MONTH)
                        && today.get(Calendar.DAY_OF_MONTH) < newDate
                        .get(Calendar.DAY_OF_MONTH)) {
                    age--;
                }
                AppSettings.setAge(age);
                textview_age.setText(age + "");
                t_view_dob.setError(null);
            }


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(newCalendar.getTimeInMillis());

        spRelationshipStatus = view.findViewById(R.id.spinner_relationship_status);
        fillDataInSpinner(spRelationshipStatus, arr_relationship_status);

        spReligiousBeliefs = view.findViewById(R.id.spinner_religious_beliefs);
        fillDataInSpinner(spReligiousBeliefs, arr_religious);

        spPoliticalBeliefs = view.findViewById(R.id.spinner_political_beliefs);
        fillDataInSpinner(spPoliticalBeliefs, arr_political);

        spHeight = view.findViewById(R.id.spinner_heights);
        fillDataInSpinner(spHeight, arr_height);

        spBodyType = view.findViewById(R.id.spinner_body_type);
        fillDataInSpinner(spBodyType, arr_body_type);

        spEyeColor = view.findViewById(R.id.spinner_eye_color);
        fillDataInSpinner(spEyeColor, arr_eye_color);

        spHairColor = view.findViewById(R.id.spinner_hair_color);
        fillDataInSpinner(spHairColor, arr_hair_color);

        spSmoking = view.findViewById(R.id.spinner_smoking);
        fillDataInSpinner(spSmoking, arr_smoking);

        spDrinking = view.findViewById(R.id.spinner_drinking);
        fillDataInSpinner(spDrinking, arr_drinking);

        spGym = view.findViewById(R.id.spinner_gym);
        fillDataInSpinner(spGym, arr_gym);

        buttonSave = view.findViewById(R.id.button_save);
        buttonNext = view.findViewById(R.id.button_next);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValid()) {
                    AppSettings.setProfileStepStatus1(2);
                }

                saveDataInSharedPref();


            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new EditProfileILikeToDoFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_edit_profile_details, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }
        });
    }


    private boolean isValid() {

        if (t_view_name.getText().toString().trim() != null &&
                t_view_name.getText().toString().trim().length() == 0) {
            return false;

        } else if (t_view_dob.getText().toString().trim() != null &&
                t_view_dob.getText().toString().trim().length() == 0) {
            return false;

        } else if (genderId != null && genderId.length() == 0) {
            return false;

        } else if (interestedInId != null && interestedInId.length() == 0) {
            return false;

        } else if (spRelationshipStatus.getSelectedItem().toString() != null &&
                spRelationshipStatus.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spReligiousBeliefs.getSelectedItem().toString() != null &&
                spReligiousBeliefs.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spPoliticalBeliefs.getSelectedItem().toString() != null &&
                spPoliticalBeliefs.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spHeight.getSelectedItem().toString() != null &&
                spHeight.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (t_view_weight.getText().toString().trim() != null &&
                t_view_weight.getText().toString().trim().length() == 0) {
            return false;

        } else if (spBodyType.getSelectedItem().toString() != null &&
                spBodyType.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spEyeColor.getSelectedItem().toString() != null &&
                spEyeColor.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spHairColor.getSelectedItem().toString() != null &&
                spHairColor.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spSmoking.getSelectedItem().toString() != null &&
                spSmoking.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spDrinking.getSelectedItem().toString() != null &&
                spDrinking.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spGym.getSelectedItem().toString() != null &&
                spGym.getSelectedItem().toString().equals("Select")) {
            return false;

        } else {
            return true;
        }
    }

    private void showProgressBar() {
        try {
            progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveDataInSharedPref() {

        String data = AppSettings.getProfileDetails();
        Gson gson = new Gson();
        ResponseGetUserProfileData responseGetUserProfileData = gson.fromJson(data, ResponseGetUserProfileData.class);

        String StrHeight = null;
        if (responseGetUserProfileData != null) {
            userBasicInfo = responseGetUserProfileData.getUser_basic_info()[0];
            if (userBasicInfo != null) {


                if (t_view_name.getText().toString().trim() != null) {
                    userBasicInfo.setName(t_view_name.getText().toString().trim());
                } else {
                    userBasicInfo.setName("");
                }
                if (edit_text_traids.getText().toString().trim() != null) {
                    userBasicInfo.setTraits(edit_text_traids.getText().toString().trim());
                } else {
                    userBasicInfo.setTraits("");
                }
                if (edit_text_city.getText().toString().trim() != null) {
                    userBasicInfo.setCity(edit_text_city.getText().toString().trim());
                } else {
                    userBasicInfo.setCity("");
                }
                if (t_view_dob.getText().toString().trim() != null) {
                    userBasicInfo.setDate_of_birth(t_view_dob.getText().toString().trim());
                } else {
                    userBasicInfo.setDate_of_birth("");
                }
                userBasicInfo.setGender(genderId);
                userBasicInfo.setInterested_in(interestedInId);
                if (spRelationshipStatus.getSelectedItem().toString() != null &&
                        !spRelationshipStatus.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setRelationship_status(spRelationshipStatus.getSelectedItem().toString());
                } else {
                    userBasicInfo.setRelationship_status("");
                }

                if (spReligiousBeliefs.getSelectedItem().toString() != null &&
                        !spReligiousBeliefs.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setBeliefs_religious_beliefs(spReligiousBeliefs.getSelectedItem().toString());
                } else {
                    userBasicInfo.setBeliefs_religious_beliefs("");
                }
                if (spPoliticalBeliefs.getSelectedItem().toString() != null &&
                        !spPoliticalBeliefs.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setPolitical_beliefs(spPoliticalBeliefs.getSelectedItem().toString());
                } else {
                    userBasicInfo.setPolitical_beliefs("");
                }
                if (spHeight.getSelectedItem().toString() != null &&
                        !spHeight.getSelectedItem().toString().equals("Select")) {

                    StrHeight = String.valueOf(spHeight.getSelectedItem()).replace("'", ".");
                    StrHeight = StrHeight.replace("\"", "");
                    userBasicInfo.setHeight(StrHeight);
                } else {
                    userBasicInfo.setHeight("");
                }
                if (t_view_weight.getText().toString().trim() != null) {
                    userBasicInfo.setWeight(t_view_weight.getText().toString().trim());
                } else {
                    userBasicInfo.setWeight("");
                }

                if (spBodyType.getSelectedItem().toString() != null &&
                        !spBodyType.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setBody_type(spBodyType.getSelectedItem().toString());
                } else {
                    userBasicInfo.setBody_type("");
                }
                if (spEyeColor.getSelectedItem().toString() != null &&
                        !spEyeColor.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setEye_color(spEyeColor.getSelectedItem().toString());
                } else {
                    userBasicInfo.setEye_color("");
                }
                if (spHairColor.getSelectedItem().toString() != null &&
                        !spHairColor.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setHair_color(spHairColor.getSelectedItem().toString());
                } else {
                    userBasicInfo.setHair_color("");
                }

                if (spSmoking.getSelectedItem().toString() != null &&
                        !spSmoking.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setSmoking(spSmoking.getSelectedItem().toString());
                } else {
                    userBasicInfo.setSmoking("");
                }
                if (spDrinking.getSelectedItem().toString() != null &&
                        !spDrinking.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setDrinking(spDrinking.getSelectedItem().toString());
                } else {
                    userBasicInfo.setDrinking("");
                }
                if (spGym.getSelectedItem().toString() != null &&
                        !spGym.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setGym(spGym.getSelectedItem().toString());
                } else {
                    userBasicInfo.setGym("");
                }

                String json = gson.toJson(responseGetUserProfileData);
                AppSettings.setProfileDetails(json);
            }
        }
        if (validateForm(t_view_name.getText().toString(), AppSettings.getAge())) {
            editProfileApi(StrHeight);
        }
        QuickBloxUser = new QBUser();
        QuickBloxUser.setId(AppSettings.getQuickBloxUserID());
        QuickBloxUser.setFullName(t_view_name.getText().toString());

        QBUsers.updateUser(QuickBloxUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {

            }

            @Override
            public void onError(QBResponseException errors) {

            }
        });
    }


    public boolean validateForm(String name, int age) {
        boolean valid = true;
        try {
            t_view_name.setError(null);
            t_view_dob.setError(null);
            if (name.length() == 0) {
                setErrorMsg("Enter user name.", t_view_name, true);
                valid = false;
            } else if (age <= 17) {
                setErrorMsg("Age should be 18 years and above.", t_view_dob, true);
                valid = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valid;
    }


    public void setErrorMsg(String msg, EditText et, boolean isRequestFocus) {
        int ecolor = Color.RED; // whatever color you want
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        if (isRequestFocus) {
            et.requestFocus();
        }

        et.setError(ssbuilder);
    }

    private void editProfileApi(String strHeight) {
        String relationship, spReligiousBeliefss, spPoliticalBeliefss, spBodyTypes,
                spEyeColors, spHairColors, spSmokings, spDrinkings, spGyms;
        showProgressBar();
        if (spRelationshipStatus.getSelectedItem().toString().equals("Select")) {
            relationship = "NA";
        } else {
            relationship = spRelationshipStatus.getSelectedItem().toString();
        }

        if (spReligiousBeliefs.getSelectedItem().toString().equals("Select")) {
            spReligiousBeliefss = "NA";
        } else {
            spReligiousBeliefss = spReligiousBeliefs.getSelectedItem().toString();
        }


        if (spPoliticalBeliefs.getSelectedItem().toString().equals("Select")) {
            spPoliticalBeliefss = "NA";
        } else {
            spPoliticalBeliefss = spPoliticalBeliefs.getSelectedItem().toString();
        }


        if (spBodyType.getSelectedItem().toString().equals("Select")) {
            spBodyTypes = "NA";
        } else {
            spBodyTypes = spBodyType.getSelectedItem().toString();
        }


        if (spEyeColor.getSelectedItem().toString().equals("Select")) {
            spEyeColors = "NA";
        } else {
            spEyeColors = spEyeColor.getSelectedItem().toString();
        }

        if (spHairColor.getSelectedItem().toString().equals("Select")) {
            spHairColors = "NA";
        } else {
            spHairColors = spHairColor.getSelectedItem().toString();
        }

        if (spSmoking.getSelectedItem().toString().equals("Select")) {
            spSmokings = "NA";
        } else {
            spSmokings = spSmoking.getSelectedItem().toString();
        }

        if (spDrinking.getSelectedItem().toString().equals("Select")) {
            spDrinkings = "NA";
        } else {
            spDrinkings = spDrinking.getSelectedItem().toString();
        }

        if (spGym.getSelectedItem().toString().equals("Select")) {
            spGyms = "NA";
        } else {
            spGyms = spGym.getSelectedItem().toString();
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
        Call<ResponseSetUserInfo> loginCall = getProfileRequest.setUserProfileData1(
                AppSettings.getLoginUserId(),
                t_view_name.getText().toString().trim(),

                edit_text_city.getText().toString().trim(),
                edit_text_traids.getText().toString().trim(),
                t_view_dob.getText().toString().trim(),
                genderId,
                interestedInId,
                relationship,
                spReligiousBeliefss,
                spPoliticalBeliefss,
                strHeight,
                t_view_weight.getText().toString().trim(),
                spBodyTypes,
                spEyeColors,
                spHairColors,
                spSmokings,
                spDrinkings,
                spGyms);
        Log.d("hiiiiiiiii", t_view_name.getText().toString().trim());
        loginCall.enqueue(new Callback<ResponseSetUserInfo>() {
            @Override
            public void onResponse(Call<ResponseSetUserInfo> call, retrofit2.Response<ResponseSetUserInfo> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Fragment fragment = new EditProfileILikeToDoFragment();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.container_edit_profile_details, fragment);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        ft.commit();
                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), response.body().getMsg());
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseSetUserInfo> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    //Set custom action bar
    private void setActionBar() {
    }

    public void setData() {
        if (AppSettings.getRegistrationStatus() != null) {
            if (AppSettings.getRegistrationStatus().equals("yes")) {
                if (AppSettings.getUserProfileName() != null)
                    t_view_name.setText(AppSettings.getUserProfileName());
            }
        }
        Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## setDataInControls");

        userProfileDataFromResponse = ((EditProfileDetailsActivity) getActivity()).getUserProfileDataFromResponse();

        if (userProfileDataFromResponse != null) {

            UserBasicInfo basic_user_info = userProfileDataFromResponse.getUser_basic_info()[0];
            t_view_name.setText(basic_user_info.getName());

            t_view_name.setSelection(t_view_name.getText().length());
            AppSettings.setUserProfileName(basic_user_info.getName());
            t_view_weight.setText(basic_user_info.getWeight());
            t_view_dob.setText(basic_user_info.getDate_of_birth());
            edit_text_city.setText(basic_user_info.getCity());
            edit_text_traids.setText(basic_user_info.getTraits());
            genderId = basic_user_info.getGender();
            if (basic_user_info.getGender() != null) {
                if (basic_user_info.getGender().equals("0")) {
                    rdGenMale.setChecked(true);
                    rdGenFemale.setChecked(false);
                    rdGenbisexual.setChecked(false);
                } else if (basic_user_info.getGender().equals("1")) {
                    rdGenFemale.setChecked(true);
                    rdGenMale.setChecked(false);
                    rdGenbisexual.setChecked(false);
                } else if (basic_user_info.getGender().equals("2")) {
                    rdGenbisexual.setChecked(true);
                    rdGenFemale.setChecked(false);
                    rdGenMale.setChecked(false);
                }
            }

            interestedInId = basic_user_info.getInterested_in();
            if (basic_user_info.getInterested_in() != null) {
                if (basic_user_info.getInterested_in().equals("0")) {
                    rdInterestedMale.setChecked(true);
                    rdInterestedFemale.setChecked(false);
                    rdInterestedbisexual.setChecked(false);
                } else if (basic_user_info.getInterested_in().equals("1")) {
                    rdInterestedFemale.setChecked(true);
                    rdInterestedMale.setChecked(false);
                    rdInterestedbisexual.setChecked(false);
                } else if (basic_user_info.getInterested_in().equals("2")) {
                    rdInterestedFemale.setChecked(false);
                    rdInterestedMale.setChecked(false);
                    rdInterestedbisexual.setChecked(true);

                }
            }

            if (arr_relationship_status.length > 0) {
                for (int index = 0; index < arr_relationship_status.length; index++) {
                    if (basic_user_info.getRelationship_status() != null)
                        if (basic_user_info.getRelationship_status().equals(arr_relationship_status[index])) {
                            spRelationshipStatus.setSelection(index);
                            break;
                        }
                }
            }

            if (arr_religious.length > 0) {
                for (int index = 0; index < arr_religious.length; index++) {
                    if (basic_user_info.getBeliefs_religious_beliefs() != null)
                        if (basic_user_info.getBeliefs_religious_beliefs().equals(arr_religious[index])) {
                            spReligiousBeliefs.setSelection(index);
                            break;
                        }
                }
            }

            if (arr_political.length > 0) {
                for (int index = 0; index < arr_political.length; index++) {
                    if (basic_user_info.getPolitical_beliefs() != null)
                        if (basic_user_info.getPolitical_beliefs().equals(arr_political[index])) {
                            spPoliticalBeliefs.setSelection(index);
                            break;
                        }
                }
            }


            if (basic_user_info.getHeight() != null && basic_user_info.getHeight().length() > 0) {
                String[] arr;
                String initialHeight = basic_user_info.getHeight().replace(".", "'");
                arr = initialHeight.split("\\(");
                String actualHeight = arr[0] + "\"(" + arr[1]; //+ "\"("
                actualHeight = actualHeight.replaceAll("^(.*)'(.*)$", "$1.$2");
                // Log.d(TAG, "## actualHeight : " + actualHeight)
                if (arr_height.length > 0) {
                    for (int index = 0; index < arr_height.length; index++) {
                        if (actualHeight.equals(arr_height[index])) {
                            spHeight.setSelection(index);
                            break;
                        }
                    }
                }
            }

            if (arr_body_type.length > 0) {
                for (int index = 0; index < arr_body_type.length; index++) {
                    if (basic_user_info.getBody_type() != null)
                        if (basic_user_info.getBody_type().equals(arr_body_type[index])) {
                            spBodyType.setSelection(index);
                            break;
                        }
                }
            }
            if (arr_eye_color.length > 0) {
                for (int index = 0; index < arr_eye_color.length; index++) {
                    if (basic_user_info.getEye_color() != null)
                        if (basic_user_info.getEye_color().equals(arr_eye_color[index])) {
                            spEyeColor.setSelection(index);
                            break;
                        }
                }
            }
            if (arr_hair_color.length > 0) {
                for (int index = 0; index < arr_hair_color.length; index++) {
                    if (basic_user_info.getHair_color() != null)
                        if (basic_user_info.getHair_color().equals(arr_hair_color[index])) {
                            spHairColor.setSelection(index);
                            break;
                        }
                }
            }

            if (arr_smoking.length > 0) {
                for (int index = 0; index < arr_smoking.length; index++) {
                    if (basic_user_info.getSmoking() != null)
                        if (basic_user_info.getSmoking().equals(arr_smoking[index])) {
                            spSmoking.setSelection(index);
                            break;
                        }
                }
            }

            if (arr_drinking.length > 0) {
                for (int index = 0; index < arr_drinking.length; index++) {
                    if (basic_user_info.getDrinking() != null)
                        if (basic_user_info.getDrinking().equals(arr_drinking[index])) {
                            spDrinking.setSelection(index);
                            break;
                        }
                }
            }

            if (arr_gym.length > 0) {
                for (int index = 0; index < arr_gym.length; index++) {
                    if (basic_user_info.getGym() != null)
                        if (basic_user_info.getGym().equals(arr_gym[index])) {
                            spGym.setSelection(index);
                            break;
                        }
                }
            }
        }
    }

 /*   public void setDataInControls(ResponseGetUserProfileData responseGetUserProfileData) {
        Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## setDataInControls");

        if (responseGetUserProfileData != null) {

            UserBasicInfo basic_user_info = responseGetUserProfileData.getUser_basic_info()[0];
            t_view_name.setText(basic_user_info.getName());
            t_view_weight.setText(basic_user_info.getWeight());
            t_view_dob.setText(basic_user_info.getDate_of_birth());

            genderId = basic_user_info.getGender();
            if (basic_user_info.getGender().equals("0")) {
                rdGenMale.setChecked(true);
                rdGenFemale.setChecked(false);
            } else {
                rdGenFemale.setChecked(true);
                rdGenMale.setChecked(false);
            }

            interestedInId = basic_user_info.getInterested_in();
            if (basic_user_info.getInterested_in().equals("0")) {
                rdInterestedMale.setChecked(true);
                rdInterestedFemale.setChecked(false);
            } else {
                rdInterestedFemale.setChecked(true);
                rdInterestedMale.setChecked(false);
            }

            if (arr_relationship_status.length > 0) {
                for (int index = 0; index < arr_relationship_status.length; index++) {
                    if (basic_user_info.getRelationship_status().equals(arr_relationship_status[index])) {
                        spRelationshipStatus.setSelection(index);
                        break;
                    }
                }
            }

            if (arr_religious.length > 0) {
                for (int index = 0; index < arr_religious.length; index++) {
                    if (basic_user_info.getBeliefs_religious_beliefs().equals(arr_religious[index])) {
                        spReligiousBeliefs.setSelection(index);
                        break;
                    }
                }
            }

            if (arr_political.length > 0) {
                for (int index = 0; index < arr_political.length; index++) {
                    if (basic_user_info.getPolitical_beliefs().equals(arr_political[index])) {
                        spPoliticalBeliefs.setSelection(index);
                        break;
                    }
                }
            }

            String[] arr;
            String initialHeight = basic_user_info.getHeight().replace(".", "'");
            arr = initialHeight.split("\\(");
            String actualHeight = arr[0] + "\"(" + arr[1]; //+ "\"("
            actualHeight = actualHeight.replaceAll("^(.*)'(.*)$", "$1.$2");
            // Log.d(TAG, "## actualHeight : " + actualHeight)

            if (arr_height.length > 0) {
                for (int index = 0; index < arr_height.length; index++) {
                    if (actualHeight.equals(arr_height[index])) {
                        spHeight.setSelection(index);
                        break;
                    }
                }
            }

            if (arr_body_type.length > 0) {
                for (int index = 0; index < arr_body_type.length; index++) {
                    if (basic_user_info.getBody_type().equals(arr_body_type[index])) {
                        spBodyType.setSelection(index);
                        break;
                    }
                }
            }
            if (arr_eye_color.length > 0) {
                for (int index = 0; index < arr_eye_color.length; index++) {
                    if (basic_user_info.getEye_color().equals(arr_eye_color[index])) {
                        spEyeColor.setSelection(index);
                        break;
                    }
                }
            }
            if (arr_hair_color.length > 0) {
                for (int index = 0; index < arr_hair_color.length; index++) {
                    if (basic_user_info.getHair_color().equals(arr_hair_color[index])) {
                        spHairColor.setSelection(index);
                        break;
                    }
                }
            }

            if (arr_smoking.length > 0) {
                for (int index = 0; index < arr_smoking.length; index++) {
                    if (basic_user_info.getSmoking().equals(arr_smoking[index])) {
                        spSmoking.setSelection(index);
                        break;
                    }
                }
            }

            if (arr_drinking.length > 0) {
                for (int index = 0; index < arr_drinking.length; index++) {
                    if (basic_user_info.getDrinking().equals(arr_drinking[index])) {
                        spDrinking.setSelection(index);
                        break;
                    }
                }
            }

            if (arr_gym.length > 0) {
                for (int index = 0; index < arr_gym.length; index++) {
                    if (basic_user_info.getGym().equals(arr_gym[index])) {
                        spGym.setSelection(index);
                        break;
                    }
                }
            }
        }
    }*/

    public void fillDataInSpinner(Spinner spinner, final String[] category) {
        if (getActivity() != null)
            Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## fillDataInSpinner");
        try {

            ArrayAdapter<String> aa = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, category);

            aa.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(aa);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {

//                    Toast.makeText(context, "onItemSelected : " + category[position] + "position : " + position, Toast.LENGTH_SHORT).show();
                    } else {
                        if (getActivity() != null)
                            Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## onItemSelected : " + category[position]);
                        //Toast.makeText(context, "onItemSelected : " + category[position], Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            if (getActivity() != null)
                Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }
}