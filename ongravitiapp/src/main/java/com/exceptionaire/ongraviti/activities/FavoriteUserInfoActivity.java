package com.exceptionaire.ongraviti.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.CustomPagerQuestAdapter;
import com.exceptionaire.ongraviti.activities.adapters.FavUserPostListAdapter;
import com.exceptionaire.ongraviti.activities.adapters.InterestListAdapter;
import com.exceptionaire.ongraviti.activities.adapters.PostsCommentsAdapter;
import com.exceptionaire.ongraviti.activities.adapters.UserPostListAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.myorbit.MyOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.dialog.NormalDialog;
import com.exceptionaire.ongraviti.dialog.animation.BounceEnter.BounceTopEnter;
import com.exceptionaire.ongraviti.dialog.animation.BounceEnter.SlideBottomExit;
import com.exceptionaire.ongraviti.dialog.listener.OnBtnClickL;
import com.exceptionaire.ongraviti.imoji.EmoticonTextView;
import com.exceptionaire.ongraviti.model.Array_of_comment;
import com.exceptionaire.ongraviti.model.DataModel;
import com.exceptionaire.ongraviti.model.LikeUnlike;
import com.exceptionaire.ongraviti.model.Postinfo;
import com.exceptionaire.ongraviti.model.ProfileInfo;
import com.exceptionaire.ongraviti.model.Question;
import com.exceptionaire.ongraviti.model.ResponseGetCommentsByPostID;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseQuestionnaireList;
import com.exceptionaire.ongraviti.model.ResponseSuccess;
import com.exceptionaire.ongraviti.model.ResponseUserPost;
import com.exceptionaire.ongraviti.model.Set_Comment;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.model.deletePostResponse;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;

import org.apache.http.protocol.HTTP;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

/**
 * Created by root on 6/3/17.
 */

public class FavoriteUserInfoActivity extends BaseDrawerActivity {
    EditText et_comment;
    Context context;
    String TAG = "Fav User Info";
    String fav_user_id;
    //    private SwipeRefreshLayout swipe_refresh_layout;
    private int WEBSERVICE_FLAG;
    private ProgressDialog progressDialog;
    private String status;
    private TextView header;
    private Boolean isLoadingMore = false;
    public static UserBasicInfo userBasicInfo;
    public static ArrayList<ProfileInfo> list_profile_category;
    private String videoBioPath = null;
    ArrayList dataModels;
    private ImageView imgProfilePicture;
    private ImageView imgVideoThumb;
    private ImageView imagePlay;
    RelativeLayout relativeVideo;
    //    TextView txtProfileBioDes;
    RelativeLayout relative_video_bio_label;
    TextView txtUserName;
    WebView webview;
    private Boolean isFromRefresh = false;
    ListView interest;
    private Boolean ifListUpdated = false;
    private boolean loadingMore = false;
    private Boolean inProgress = false;
    private Boolean isCommentShow = false;
    //    private ImageView imgAmIEdit, imgAmIPlus, imgAmIMinus, imgLikePlus, imgLikeMinus, imgLikeEdit, imgLivingPlus, imgLivingMinus, imgLivingEdit, imgWantPlus, imgWantMinus, imgWantEdit;
    private ScrollView scrollView;
    private TextView t_view_name, t_view_dob, t_view_gender, t_view_interested_in, t_view_relationship_status, t_view_religious_beliefs, t_view_political_beliefs, t_view_height, t_view_weight, t_view_body_type, t_view_eye_color, t_view_hair_color, t_view_smoking, t_view_drinking, t_view_gym, t_view_fav_book, t_view_fav_shows, t_view_fav_movies, t_view_contries_travelled, t_view_events_attended, t_view_hobbies, t_view_highest_degree, t_view_colg_school, t_view_degree, t_view_emp_status, t_view_future_goal, t_view_area_of_interest, t_view_what_i_read, t_view_venture_to_look_for, t_view_enterpreneursILik, t_view_events_attended_WIWB, t_view_events_looking_forward_to, t_view_industry_i_want_to_target;
    ImageView imgUnLock, imageViewZoom;
    Dialog dialog;
    Boolean isShown = false;
    ArrayList<Question> list_questionaire;
    RecyclerView recycler_listOfQuestions;
    int maxPageToScroll;
    int minPageToScroll;
    Boolean isDialogOpen = false;
    public static int currentPagerIndex = 0;
    Boolean isAnswersGiven = false;  //this is true when user give 3 answers right from questionaire
    private UserPostListAdapter userPostListAdapter;
    public static ArrayList<String> list_ans = new ArrayList<String>();
    public ViewPager viewPager;
    CardView card_view_autocomplete;
    LinearLayout linearAutocomplete;
    Boolean flagFirstPage = true;
    private RecyclerView recyclerViewPosts;
    private SimpleExoPlayerView videoView;
    private SimpleExoPlayer player;
    private MediaSource videoSource;
    private ArrayList<Postinfo> list_posts = new ArrayList<>();
    private int currentScrollState, currentFirstVisibleItem, currentVisibleItemCount, totalItemCount;
    private int LastListItem = 0;
    TextView text_view_nopost, t_view_name1;
    ImageView textview1, textview2, textview3, textview4, textview5, play_video_vio;
    LinearLayout linear1, linear2, linear3, linear4, linear5;
    EmoticonTextView text_view_status;
    ImageView social_backlground;
    FavUserPostListAdapter favUserPostListAdapter;
    String catId;
    ImageView imageview_add_friend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_fav_user_info, frameLayout);
        context = this;
        try {
            initView();
            initializeListeners();
            if (getIntent() != null) {
                Log.d(TAG, "## getIntent()!=null");

                if (getIntent().getStringExtra("ActivityName").equals("OrbitProfile")) {
                    Log.d(TAG, "## OrbitUserID : " + getIntent().getStringExtra("OrbitUserID"));
                    fav_user_id = getIntent().getStringExtra("OrbitUserID");

                } else if (getIntent().getStringExtra("ActivityName").equals("FavoriteProfile")) {
                    Log.d(TAG, "## fav_user_id : " + getIntent().getStringExtra("fav_user_id"));
                    fav_user_id = getIntent().getStringExtra("fav_user_id");

                } else if (getIntent().getStringExtra("ActivityName").equals("CardAdptr")) {
                    Log.d(TAG, "## card_user_id : " + getIntent().getStringExtra("card_user_id"));
                    fav_user_id = getIntent().getStringExtra("card_user_id");

                } else if (getIntent().getStringExtra("ActivityName").equals("PeopleNearByMe")) {
                    Log.d(TAG, "## card_user_id : " + getIntent().getStringExtra("card_user_id"));
                    fav_user_id = getIntent().getStringExtra("card_user_id");
                } else if (getIntent().getStringExtra("ActivityName").equals("AskForDating")) {
                    Log.d(TAG, "## dating_user_id : " + getIntent().getStringExtra("dating_user_id"));
                    fav_user_id = getIntent().getStringExtra("dating_user_id");

                } else if (getIntent().getStringExtra("ActivityName").equals("OrbitRequest")) {
                    Log.d(TAG, "## dating_user_id : " + getIntent().getStringExtra("dating_user_id"));
                    fav_user_id = getIntent().getStringExtra("OrbitUserRequestID");

                } else if (getIntent().getStringExtra("ActivityName").equals("NotificationsActivity")) {
                    fav_user_id = getIntent().getStringExtra("UserID");

                } else if (getIntent().getStringExtra("ActivityName").equals("SearchOrbitProfile")) {
                    Log.d(TAG, "## OrbitSearchUserID : " + getIntent().getStringExtra("OrbitSearchUserID"));
                    fav_user_id = getIntent().getStringExtra("OrbitSearchUserID");

                } else if (getIntent().getStringExtra("ActivityName").equals("FavoriteActivity")) {
                    Log.d(TAG, "## favUserID : " + getIntent().getStringExtra("favUserID"));
                    fav_user_id = getIntent().getStringExtra("favUserID");

                } else if (getIntent().getStringExtra("ActivityName").equals("AcceptDatingRequestActivity")) {
                    Log.d(TAG, "## DatingUserID : " + getIntent().getStringExtra("DatingUserID"));
                    fav_user_id = getIntent().getStringExtra("DatingUserID");

                }

                if (isNetworkAvailable()) {

                    getAndDisplayUserInfo();

                } else {
                    CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, getResources().getString(R.string.check_internet));
                }


            }
            imageview_add_friend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isNetworkAvailable()) {
                        sendFriendRequest(fav_user_id);
                    } else {
                        CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, getResources().getString(R.string.check_internet));
                    }
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }


    }


    DefaultBandwidthMeter bandwidthMetere;

    private void initView() {
        try {
            social_backlground = (ImageView) findViewById(R.id.social_backlground);
            recyclerViewPosts = (RecyclerView) findViewById(R.id.list_view_userpost);
            videoView = (SimpleExoPlayerView) findViewById(R.id.videoview);
            bandwidthMetere = new DefaultBandwidthMeter();
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create a default LoadControl
            LoadControl loadControl = new DefaultLoadControl();
            // 3. Create the player
            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
            videoView.setPlayer(player);
            text_view_nopost = (TextView) findViewById(R.id.text_view_nopost);
            header = (TextView) findViewById(R.id.toolbar_title_drawer);
            header.setText(getResources().getString(R.string.viewProfile));
            imgUnLock = (ImageView) findViewById(R.id.imgBtnVideoLock);
            imageViewZoom = (ImageView) findViewById(R.id.imgBtnVideoZoom);
            imgProfilePicture = (ImageView) findViewById(R.id.imgProfilePicture);
            imgVideoThumb = (ImageView) findViewById(R.id.imgVideoThumb);
            imagePlay = (ImageView) findViewById(R.id.imgPlay);
            relativeVideo = (RelativeLayout) findViewById(R.id.relative_Video);
            relative_video_bio_label = (RelativeLayout) findViewById(R.id.relative_video_bio_label);
            webview = (WebView) findViewById(R.id.webview);
            txtUserName = (TextView) findViewById(R.id.txtUserName);
//            txtProfileBioDes=(TextView)findViewById(R.id.txtProfileBioDes);
            imageview_add_friend = (ImageView) findViewById(R.id.imageview_add_friend);
            relativeVideo.setVisibility(View.GONE);
            scrollView = (ScrollView) findViewById(R.id.scrollView);
            text_view_status = (EmoticonTextView) findViewById(R.id.text_view_status);
            t_view_name = (TextView) findViewById(R.id.t_view_name);
            t_view_dob = (TextView) findViewById(R.id.t_view_dob);
            t_view_gender = (TextView) findViewById(R.id.t_view_gender);
            t_view_interested_in = (TextView) findViewById(R.id.t_view_interested_in);
            t_view_relationship_status = (TextView) findViewById(R.id.t_view_relationship_status);
            t_view_religious_beliefs = (TextView) findViewById(R.id.t_view_religious_beliefs);
            t_view_political_beliefs = (TextView) findViewById(R.id.t_view_political_beliefs);
            t_view_height = (TextView) findViewById(R.id.t_view_height);
            t_view_weight = (TextView) findViewById(R.id.t_view_weight);
            t_view_body_type = (TextView) findViewById(R.id.t_view_body_type);
            t_view_eye_color = (TextView) findViewById(R.id.t_view_eye_color);
            t_view_hair_color = (TextView) findViewById(R.id.t_view_hair_color);
            t_view_smoking = (TextView) findViewById(R.id.t_view_smoking);
            t_view_drinking = (TextView) findViewById(R.id.t_view_drinking);
            t_view_gym = (TextView) findViewById(R.id.t_view_gym);
            t_view_fav_book = (TextView) findViewById(R.id.t_view_fav_book);
            t_view_fav_shows = (TextView) findViewById(R.id.t_view_fav_shows);
            t_view_fav_movies = (TextView) findViewById(R.id.t_view_fav_movies);
            t_view_contries_travelled = (TextView) findViewById(R.id.t_view_contries_travelled);
            t_view_events_attended = (TextView) findViewById(R.id.t_view_events_attended);
            t_view_hobbies = (TextView) findViewById(R.id.t_view_hobbies);
            t_view_name1 = (TextView) findViewById(R.id.t_view_name1);
            t_view_highest_degree = (TextView) findViewById(R.id.t_view_highest_degree);
            t_view_colg_school = (TextView) findViewById(R.id.t_view_colg_school);
//            t_view_colg_school.setMovementMethod(new ScrollingMovementMethod());
            interest = (ListView) findViewById(R.id.interest);

            dataModels = new ArrayList();

            dataModels.add(new DataModel("Cricket", false));
            dataModels.add(new DataModel("Cooking", true));
            dataModels.add(new DataModel("Music", false));
            dataModels.add(new DataModel("Books", true));
            dataModels.add(new DataModel("News", false));

            InterestListAdapter adapter = new InterestListAdapter(dataModels, getApplicationContext());

            interest.setAdapter(adapter);
            t_view_degree = (TextView) findViewById(R.id.t_view_degree);
            t_view_emp_status = (TextView) findViewById(R.id.t_view_emp_status);
            t_view_future_goal = (TextView) findViewById(R.id.t_view_future_goal);
//            swipe_refresh_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
            t_view_area_of_interest = (TextView) findViewById(R.id.t_view_area_of_interest);
            t_view_what_i_read = (TextView) findViewById(R.id.t_view_what_i_read);
            t_view_venture_to_look_for = (TextView) findViewById(R.id.t_view_venture_to_look_for);
            t_view_enterpreneursILik = (TextView) findViewById(R.id.t_view_enterpreneursILik);
            t_view_events_attended_WIWB = (TextView) findViewById(R.id.t_view_events_attended_WIWB);
            t_view_events_looking_forward_to = (TextView) findViewById(R.id.t_view_events_looking_forward_to);
            t_view_industry_i_want_to_target = (TextView) findViewById(R.id.t_view_industry_i_want_to_target);

            textview1 = (ImageView) findViewById(R.id.text_view1);
            textview2 = (ImageView) findViewById(R.id.text_view2);
            textview3 = (ImageView) findViewById(R.id.text_view3);
            textview4 = (ImageView) findViewById(R.id.text_view4);
            textview5 = (ImageView) findViewById(R.id.text_view5);
            play_video_vio = (ImageView) findViewById(R.id.play_video_vio);
            linear1 = (LinearLayout) findViewById(R.id.linear1);
            linear2 = (LinearLayout) findViewById(R.id.linear2);
            linear3 = (LinearLayout) findViewById(R.id.linear3);
            linear4 = (LinearLayout) findViewById(R.id.linear4);
            linear5 = (LinearLayout) findViewById(R.id.linear5);
            textview1.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceAsColor")
                @Override
                public void onClick(View view) {
                    textview1.setImageResource(R.drawable.who_am_i_select);
                    textview2.setImageResource(R.drawable.what_i_like_doing);
                    textview3.setImageResource(R.drawable.how_do_i_make_living);
                    textview4.setImageResource(R.drawable.what_i_want_to_be);
                    textview5.setImageResource(R.drawable.post);

                    linear1.setVisibility(View.VISIBLE);
                    linear2.setVisibility(View.GONE);
                    linear3.setVisibility(View.GONE);
                    linear4.setVisibility(View.GONE);
                    linear5.setVisibility(View.GONE);
                }
            });
            textview2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    textview2.setImageResource(R.drawable.what_i_like_doing_select);
                    textview1.setImageResource(R.drawable.who_am_i);
                    textview3.setImageResource(R.drawable.how_do_i_make_living);
                    textview4.setImageResource(R.drawable.what_i_want_to_be);
                    textview5.setImageResource(R.drawable.post);

                    linear1.setVisibility(View.GONE);
                    linear2.setVisibility(View.VISIBLE);
                    linear3.setVisibility(View.GONE);
                    linear4.setVisibility(View.GONE);
                    linear5.setVisibility(View.GONE);
                }
            });
            textview3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    textview3.setImageResource(R.drawable.how_do_i_make_living_select);
                    textview2.setImageResource(R.drawable.what_i_like_doing);
                    textview1.setImageResource(R.drawable.who_am_i);
                    textview4.setImageResource(R.drawable.what_i_want_to_be);
                    textview5.setImageResource(R.drawable.post);


                    linear1.setVisibility(View.GONE);
                    linear2.setVisibility(View.GONE);
                    linear3.setVisibility(View.VISIBLE);
                    linear4.setVisibility(View.GONE);
                    linear5.setVisibility(View.GONE);
                }
            });
            textview4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    textview4.setImageResource(R.drawable.what_i_want_to_be_select);
                    textview1.setImageResource(R.drawable.who_am_i);
                    textview2.setImageResource(R.drawable.what_i_like_doing);
                    textview3.setImageResource(R.drawable.how_do_i_make_living);
                    textview5.setImageResource(R.drawable.post);

                    linear1.setVisibility(View.GONE);
                    linear2.setVisibility(View.GONE);
                    linear3.setVisibility(View.GONE);
                    linear4.setVisibility(View.VISIBLE);
                    linear5.setVisibility(View.GONE);
                }
            });
            textview5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    textview3.setImageResource(R.drawable.how_do_i_make_living);
                    textview2.setImageResource(R.drawable.what_i_like_doing);
                    textview1.setImageResource(R.drawable.who_am_i);
                    textview4.setImageResource(R.drawable.what_i_want_to_be);
                    textview5.setImageResource(R.drawable.post_select);

                    linear1.setVisibility(View.GONE);
                    linear2.setVisibility(View.GONE);
                    linear3.setVisibility(View.GONE);
                    linear4.setVisibility(View.GONE);
                    linear5.setVisibility(View.VISIBLE);
                }
            });
            play_video_vio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (videoBioPath != null && !videoBioPath.equals("") &&
                            !videoBioPath.equals("http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/uploads/")) {
                        Intent intent = new Intent(FavoriteUserInfoActivity.this, VideoBioActivity.class);
                        intent.putExtra("videopath", videoBioPath);
                        startActivity(intent);
                    } else {
                        CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, "Profile video not available.");
                    }

                }
            });

        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void initializeListeners() {

        imgUnLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    WEBSERVICE_FLAG = 2;  // 1 flag for set questionaire

                    if (isNetworkAvailable()) {
                        showProgressBar();

                        Retrofit retrofit = ApiClient.getClient();
                        ApiInterface requestInterface = retrofit.create(ApiInterface.class);
                        Call<ResponseQuestionnaireList> retrofitCall = requestInterface.getQuestionnaire(userBasicInfo.getUser_id(), "1", "2");
                        retrofitCall.enqueue(new Callback<ResponseQuestionnaireList>() {
                            @Override
                            public void onResponse(Call<ResponseQuestionnaireList> call, retrofit2.Response<ResponseQuestionnaireList> response) {
                                if (progressDialog != null && progressDialog.isShowing())
                                    progressDialog.dismiss();
                                if (WEBSERVICE_FLAG == 2) {

                                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                                        list_questionaire = response.body().getQuestion();
                                        displayDialog();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseQuestionnaireList> call, Throwable t) {
                                if (progressDialog.isShowing())
                                    progressDialog.dismiss();
                            }
                        });
                    } else {
                        CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, getResources().getString(R.string.check_internet));
                    }
                } catch (Exception ee) {
                    Log.e(TAG, "## e : " + ee.getMessage());
                    ee.printStackTrace();
                }
            }
        });

        imageViewZoom.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isAnswersGiven) {
                            if (videoBioPath != null) {
                                Intent intent = new Intent(FavoriteUserInfoActivity.this, VideoBioActivity.class);
                                intent.putExtra("videopath", videoBioPath);
                                startActivity(intent);
                            } else
                                Toast.makeText(FavoriteUserInfoActivity.this, "Profile video not available.", Toast.LENGTH_SHORT).show();
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, "Please answer the questionnaire to view video bio!");
                        }
                    }
                });

        imgProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list_profile_category != null && list_profile_category.get(1) != null)
                    if (list_profile_category.get(1).getProfile_picture() != null && !list_profile_category.get(1).getProfile_picture().isEmpty())
                        new CommonDialogs().displayImageZoomDialog(FavoriteUserInfoActivity.this, PROFILE_PICTURE_PATH + File.separator + list_profile_category.get(1).getProfile_picture());

            }
        });


        imagePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAnswersGiven) {

                    imgVideoThumb.setVisibility(View.GONE);
                    imagePlay.setVisibility(View.GONE);
                    videoView.setVisibility(View.GONE);
                    player.prepare(videoSource);

                } else {
                    CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, "Please answer the questionnaire to view video bio!");
                }
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        videoView.getPlayer().setPlayWhenReady(true);
        currentPagerIndex = 0;
    }

    public void setUserInfo(ResponseGetUserProfileData responseGetUserProfileData) {
        try {
            if (responseGetUserProfileData != null)
                if (responseGetUserProfileData.getorbitStatus().equals("1")) {
                    imageview_add_friend.setVisibility(View.GONE);
                } else if (responseGetUserProfileData.getorbitStatus().equals("0")) {
                    imageview_add_friend.setVisibility(View.VISIBLE);
                }
            if (responseGetUserProfileData.getUser_basic_info().length > 0) {
                userBasicInfo = responseGetUserProfileData.getUser_basic_info()[0];
            }

            responseGetUserProfileData.getProfile_info();
            if (responseGetUserProfileData.getProfile_info().length > 0) {
                list_profile_category = new ArrayList<>(Arrays.asList(responseGetUserProfileData.getProfile_info()));
            }
            String socialId = null, datingId = null, businessId = null;
            String ActiveCatId = responseGetUserProfileData.getUser_basic_info()[0].getProfile_cat_id();
            if (responseGetUserProfileData.getProfile_info().length > 0) {
                socialId = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
            }
            if (responseGetUserProfileData.getProfile_info().length > 1) {
                datingId = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
            }
            if (responseGetUserProfileData.getProfile_info().length > 2) {
                businessId = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
            }// set all control data from json object
            if (ActiveCatId.equals(socialId)) {
                if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != "")
                    Log.d(TAG, "## AppConstants.PROFILE_PICTURE_PATH / userBasicInfo.getList_profileData().get(0).getProfile_picture() : " + AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                String path = AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture();
//                Picasso.with(this)
//                        .load(path)
//                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                        .into(imgProfilePicture);

                if (!responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                    Picasso.with(context)
                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imgProfilePicture);
                } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                    Picasso.with(context)
                            .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imgProfilePicture);
                }

                imgProfilePicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //open next activity and play video in landscape mode
                        if (path != null) {
                            new CommonDialogs().displayImageZoomDialog(FavoriteUserInfoActivity.this, path);
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, "Profile picture not available.");
                        }
                    }
                });
                Picasso.with(this)
                        .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture())
                        .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(social_backlground);
                social_backlground.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //open next activity and play video in landscape mode
                        if (AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture() != null) {
                            new CommonDialogs().displayImageZoomDialog(FavoriteUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, "Cover picture not available.");
                        }
                    }
                });
                if (!responseGetUserProfileData.getProfile_info()[0].getProfileStatus().isEmpty()) {
                    text_view_status.setText(URLDecoder.decode(
                            responseGetUserProfileData.getProfile_info()[0].getProfileStatus(), "UTF-8"));
                } else {
                    text_view_status.setText("Status not available.");
                }
                videoBioPath = AppConstants.PROFILE_BIO_PATH +
                        responseGetUserProfileData.getProfile_info()[0].getProfile_bio();
            } else if (ActiveCatId.equals(datingId)) {
                if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != "")
                    Log.d(TAG, "## AppConstants.PROFILE_PICTURE_PATH / userBasicInfo.getList_profileData().get(1).getProfile_picture() : " + AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                String path = AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture();
//                Picasso.with(this)
//                        .load(path)
//                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                        .into(imgProfilePicture);


                if (!responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                    Picasso.with(context)
                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imgProfilePicture);
                } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                    Picasso.with(context)
                            .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imgProfilePicture);
                }

                imgProfilePicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //open next activity and play video in landscape mode
                        if (path != null) {
                            new CommonDialogs().displayImageZoomDialog(FavoriteUserInfoActivity.this, path);
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, "Profile picture not available.");
                        }
                    }
                });
                Picasso.with(this)
                        .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture())
                        .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(social_backlground);
                social_backlground.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //open next activity and play video in landscape mode
                        if (AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture() != null) {
                            new CommonDialogs().displayImageZoomDialog(FavoriteUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, "Cover picture not available.");
                        }
                    }
                });
                if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().isEmpty()) {
                    text_view_status.setText(URLDecoder.decode(
                            responseGetUserProfileData.getProfile_info()[1].getProfileStatus(), "UTF-8"));
                } else {
                    text_view_status.setText("Status not available.");
                }
                videoBioPath = AppConstants.PROFILE_BIO_PATH +
                        responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
            } else if (ActiveCatId.equals(businessId)) {
                if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != "")
                    Log.d(TAG, "## AppConstants.PROFILE_PICTURE_PATH / userBasicInfo.getList_profileData().get(1).getProfile_picture() : " + AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                String path = AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture();
//                Picasso.with(this)
//                        .load(path)
//                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                        .into(imgProfilePicture);

                if (!responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                    Picasso.with(context)
                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imgProfilePicture);
                } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                    Picasso.with(context)
                            .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imgProfilePicture);
                }

                imgProfilePicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //open next activity and play video in landscape mode
                        if (path != null) {
                            new CommonDialogs().displayImageZoomDialog(FavoriteUserInfoActivity.this, path);
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, "Profile picture not available.");
                        }
                    }
                });
                Picasso.with(this)
                        .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture())
                        .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(social_backlground);
                social_backlground.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //open next activity and play video in landscape mode
                        if (AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture() != null) {
                            new CommonDialogs().displayImageZoomDialog(FavoriteUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, "Cover picture not available.");
                        }
                    }
                });
                if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().isEmpty()) {
                    text_view_status.setText(URLDecoder.decode(
                            responseGetUserProfileData.getProfile_info()[2].getProfileStatus(), "UTF-8"));
                } else {
                    text_view_status.setText("Status not available.");
                }
                videoBioPath = AppConstants.PROFILE_BIO_PATH +
                        responseGetUserProfileData.getProfile_info()[2].getProfile_bio();
            }

            if (responseGetUserProfileData.getProfile_info().length > 1) {

//            String videoBioPath = "http://techslides.com/demos/sample-videos/small.mp4";

                // Measures bandwidth during playback. Can be null if not required.
                // Produces DataSource instances through which media data is loaded.
                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "OnGraviti"), bandwidthMetere);
                // Produces Extractor instances for parsing the media data.
                ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
                // This is the MediaSource representing the media to be played
                videoSource = new ExtractorMediaSource(android.net.Uri.parse(videoBioPath), dataSourceFactory, extractorsFactory, null, null);
                // Prepare the player with the source.
//            videoView.getPlayer().setPlayWhenReady(true);
            }
            if (responseGetUserProfileData.getProfile_info().length > 1) {
                if (!responseGetUserProfileData.getProfile_info()[1].getBio_thumb().equals("")) {
                    Log.d(TAG, "## video thumb name : " + responseGetUserProfileData.getProfile_info()[1].getBio_thumb());
                    relativeVideo.setVisibility(View.GONE);
                    imgVideoThumb.setVisibility(View.GONE);
                    imagePlay.setVisibility(View.GONE);
                    relative_video_bio_label.setVisibility(View.GONE);
                    Picasso.with(this)
                            .load(AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[1].getThumb())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imgVideoThumb);
                } else {
                    relativeVideo.setVisibility(View.GONE);
                    imgVideoThumb.setVisibility(View.GONE);
                    imagePlay.setVisibility(View.GONE);
                    relative_video_bio_label.setVisibility(View.GONE);
                    imgVideoThumb.setImageDrawable(ContextCompat.getDrawable(FavoriteUserInfoActivity.this, R.drawable.alexandra_daddario_bg));
                }
            }
            t_view_name1.setText(userBasicInfo.getName());
            txtUserName.setText(userBasicInfo.getName());

            if (!userBasicInfo.getName().equals(""))
                t_view_name.setText(userBasicInfo.getName());
            ;
            if (!userBasicInfo.getDate_of_birth().equals(""))
                t_view_dob.setText(userBasicInfo.getDate_of_birth());
            if (!userBasicInfo.getGender().equals("")) {
                if (userBasicInfo.getGender().equals("0")) {
                    t_view_gender.setText("Male");
                } else if (userBasicInfo.getGender().equals("1")) {
                    t_view_gender.setText("Female");
                } else if (userBasicInfo.getGender().equals("2")) {
                    t_view_gender.setText("Bisexual");
                }
            }
            if (!userBasicInfo.getInterested_in().equals("")) {
                if (userBasicInfo.getInterested_in().equals("0"))
                    t_view_interested_in.setText("Male");
                else if (userBasicInfo.getInterested_in().equals("1"))
                    t_view_interested_in.setText("Female");
                else if (userBasicInfo.getInterested_in().equals("2"))
                    t_view_interested_in.setText("Bisexual");
            }

            if (!userBasicInfo.getRelationship_status().equals("")) {
                t_view_relationship_status.setText(userBasicInfo.getRelationship_status());
            }

            if (!userBasicInfo.getBeliefs_religious_beliefs().equals(""))
                t_view_religious_beliefs.setText(userBasicInfo.getBeliefs_religious_beliefs());
            if (!userBasicInfo.getPolitical_beliefs().equals(""))
                t_view_political_beliefs.setText(userBasicInfo.getPolitical_beliefs());
            if (!userBasicInfo.getHeight().equals(""))
                t_view_height.setText(userBasicInfo.getHeight() + " cms");
            if (!userBasicInfo.getWeight().equals(""))
                t_view_weight.setText(userBasicInfo.getWeight());
            if (!userBasicInfo.getBody_type().equals(""))
                t_view_body_type.setText(userBasicInfo.getBody_type());
            if (!userBasicInfo.getEye_color().equals(""))
                t_view_eye_color.setText(userBasicInfo.getEye_color());
            if (!userBasicInfo.getHair_color().equals(""))
                t_view_hair_color.setText(userBasicInfo.getHair_color());
            if (!userBasicInfo.getSmoking().equals(""))
                t_view_smoking.setText(userBasicInfo.getSmoking());
            if (!userBasicInfo.getDrinking().equals(""))
                t_view_drinking.setText(userBasicInfo.getDrinking());
            if (!userBasicInfo.getGym().equals(""))
                t_view_gym.setText(userBasicInfo.getGym());
            if (!userBasicInfo.getFavorite_book().equals(""))
                t_view_fav_book.setText(userBasicInfo.getFavorite_book());
            if (!userBasicInfo.getFavorite_shows().equals(""))
                t_view_fav_shows.setText(userBasicInfo.getFavorite_shows());
            if (!userBasicInfo.getFavorite_movies().equals(""))
                t_view_fav_movies.setText(userBasicInfo.getFavorite_movies());
            if (!userBasicInfo.getCountries_travelled().equals(""))
                t_view_contries_travelled.setText(userBasicInfo.getCountries_travelled());
            if (!userBasicInfo.getEvents_attended().equals(""))
                t_view_events_attended.setText(userBasicInfo.getEvents_attended());
            if (!userBasicInfo.getHobbies().equals(""))
                t_view_hobbies.setText(userBasicInfo.getHobbies());
            if (!userBasicInfo.getHighest_degree().equals(""))
                t_view_highest_degree.setText(userBasicInfo.getHighest_degree());
            if (!userBasicInfo.getCollege_university_name().equals("")) {
                t_view_colg_school.setText(userBasicInfo.getCollege_university_name());
            }
//            t_view_colg_school.setMovementMethod(new ScrollingMovementMethod());
            if (!userBasicInfo.getDegree_name().equals(""))
                t_view_degree.setText(userBasicInfo.getDegree_name());
            if (!userBasicInfo.getEmployment_status().equals(""))
                t_view_emp_status.setText(userBasicInfo.getEmployment_status());
            if (!userBasicInfo.getFuture_goal().equals(""))
                t_view_future_goal.setText(userBasicInfo.getFuture_goal());
            if (!userBasicInfo.getwhat_i_read().equals(""))
                t_view_what_i_read.setText(userBasicInfo.getwhat_i_read());
            if (!userBasicInfo.getArea_of_interest().equals(""))
                t_view_area_of_interest.setText(userBasicInfo.getArea_of_interest());
            if (!userBasicInfo.getInteresting_ventures_to_look().equals(""))
                t_view_venture_to_look_for.setText(userBasicInfo.getInteresting_ventures_to_look());
            if (!userBasicInfo.getEnterpreneurs_i_like().equals(""))
                t_view_enterpreneursILik.setText(userBasicInfo.getEnterpreneurs_i_like());
            if (!userBasicInfo.getEvents_attended_i_want().equals(""))
                t_view_events_attended_WIWB.setText(userBasicInfo.getEvents_attended_i_want());
            if (!userBasicInfo.getEvents_m_looking().equals(""))
                t_view_events_looking_forward_to.setText(userBasicInfo.getEvents_m_looking());
            if (!userBasicInfo.getIndustries_i_want_to_target().equals(""))
                t_view_industry_i_want_to_target.setText(userBasicInfo.getIndustries_i_want_to_target());
            catId = responseGetUserProfileData.getUser_basic_info()[0].getProfile_cat_id();
            if (isNetworkAvailable()) {
                getUserHomePostData(fav_user_id, catId);
            }
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void displayDialog() {
        try {
            dialog = new Dialog(context, R.style.DialogStyle);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.row_unlock_questionaire);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);

            final LinearLayout linearBase = (LinearLayout) dialog.findViewById(R.id.linear_base);
            viewPager = (ViewPager) dialog.findViewById(R.id.viewpagerQuest);
            CustomPagerQuestAdapter adapter = new CustomPagerQuestAdapter(FavoriteUserInfoActivity.this, list_questionaire, new CustomPagerQuestAdapter.AnswerAdptrListener() {
                @Override
                public void btnViewScoreClick(View v, int RightAnsCount) {
                    Log.d(TAG, "## btnViewScoreClick RightAnsCount : " + RightAnsCount);
                    if (RightAnsCount >= 2) {
                        dialog.dismiss();
                        isAnswersGiven = true;
                        CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, getResources().getString(R.string.questionnaire_unlocked_success));
                    } else {
                        dialog.dismiss();
                        CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, getResources().getString(R.string.min_unlock_q_limit));
                    }
                }
            });

            minPageToScroll = viewPager.getCurrentItem();
            maxPageToScroll = list_questionaire.size();

            Log.d(TAG, "## minPageToScroll: " + minPageToScroll);
            Log.d(TAG, "## maxPageToScroll: " + maxPageToScroll);

            final ViewPager.OnPageChangeListener pageChangeListenernew = new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    Log.d(TAG, "## onPageSelected ");
                    Log.d(TAG, "## position : " + position);
                    Log.d(TAG, "## minPageToScroll: " + minPageToScroll);
                    Log.d(TAG, "## maxPageToScroll: " + maxPageToScroll);
                    currentPagerIndex = viewPager.getCurrentItem();
                    Log.d(TAG, "## currentPagerIndex: " + currentPagerIndex);
                    Log.d(TAG, "## ques id : " + list_questionaire.get(position).getQuestion());

                    //check if answer has 4 options than increase height of view pager // and if question is bigger than one line
                    Question current_questionaire = list_questionaire.get(position);
                    List<String> list_options = Arrays.asList(current_questionaire.getQuestion_option().split(","));
                    switch (position) {
                        case 0:
                            flagFirstPage = false;
                            if (current_questionaire.getQuestion().length() < 30) {

                                if (list_options.size() == 2) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._145sdp));
                                    viewPager.requestLayout();
                                } else if (list_options.size() == 4) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._215sdp));
                                    viewPager.requestLayout();
                                }
                            } else if (current_questionaire.getQuestion().length() < 60) {

                                if (list_options.size() == 2) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._175sdp));
                                    viewPager.requestLayout();
                                } else if (list_options.size() == 4) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._230sdp));
                                    viewPager.requestLayout();
                                }
                            } else if (current_questionaire.getQuestion().length() < 90) {

                                if (list_options.size() == 2) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._185sdp));
                                    viewPager.requestLayout();
                                } else if (list_options.size() == 4) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._240sdp));
                                    viewPager.requestLayout();
                                }
                            }
                        default:
                            if (current_questionaire.getQuestion().length() < 30) {

                                if (list_options.size() == 2) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._145sdp));
                                    viewPager.requestLayout();
                                } else if (list_options.size() == 4) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._220sdp));
                                    viewPager.requestLayout();
                                }
                            } else if (current_questionaire.getQuestion().length() < 60) {

                                if (list_options.size() == 2) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._175sdp));
                                    viewPager.requestLayout();
                                } else if (list_options.size() == 4) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._230sdp));
                                    viewPager.requestLayout();
                                }
                            } else if (current_questionaire.getQuestion().length() < 90) {

                                if (list_options.size() == 2) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._185sdp));
                                    viewPager.requestLayout();
                                } else if (list_options.size() == 4) {
                                    viewPager.getLayoutParams().height = Math.round(getResources().getDimension(R.dimen._240sdp));
                                    viewPager.requestLayout();
                                }
                            }
                    }


                    if (CustomPagerQuestAdapter.isCurAnsSelected) {
                        if (position < minPageToScroll) {
                            viewPager.setCurrentItem(minPageToScroll);
                        }
                        minPageToScroll = viewPager.getCurrentItem();
                    } else {
//                    viewPager.setCurrentItem(position - 1);
                        if (flagFirstPage) {
                            viewPager.setCurrentItem(minPageToScroll);

                            if (!isDialogOpen) {
                                isDialogOpen = true;

                                mBasIn = new BounceTopEnter();
                                mBasOut = new SlideBottomExit();

                                final NormalDialog dialog = new NormalDialog(FavoriteUserInfoActivity.this);
                                dialog.isTitleShow(false);

                                dialog.content(getResources().getString(R.string.msg_to_move_unlock))//
                                        .showAnim(mBasIn)//
                                        .dismissAnim(mBasOut)//
                                        .contentGravity(Gravity.CENTER)
                                        .btnNum(1)
                                        .show();

                                dialog.setOnBtnClickL(
                                        new OnBtnClickL() {
                                            @Override
                                            public void onBtnClick() {
                                                Log.d("dialog", "## ok");
                                                isDialogOpen = false;
                                                list_ans.clear();
                                                dialog.dismiss();
                                            }
                                        });
                            }
                        }


                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            };

            viewPager.setOnPageChangeListener(pageChangeListenernew);
            // do this in a runnable to make sure the viewPager's views are already instantiated before triggering the onPageSelected call
            viewPager.post(new Runnable() {
                @Override
                public void run() {
                    pageChangeListenernew.onPageSelected(viewPager.getCurrentItem());
                }
            });

            viewPager.setAdapter(adapter);

            isShown = true;
            dialog.show();
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void getAndDisplayUserInfo() {
        WEBSERVICE_FLAG = 1;

        if (isNetworkAvailable()) {
            showProgressBar();

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseGetUserProfileData> retrofitCall = requestInterface.getUserData(fav_user_id, AppSettings.getLoginUserId());
            retrofitCall.enqueue(new Callback<ResponseGetUserProfileData>() {
                @Override
                public void onResponse(Call<ResponseGetUserProfileData> call, retrofit2.Response<ResponseGetUserProfileData> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (response != null && response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                            setUserInfo(response.body());
                        } else {
                            new CommonDialogs().showMessageDialogNonStatic(FavoriteUserInfoActivity.this, getResources().getString(R.string.ongravity), response.body().getMsg());
                        }
                    } else {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        AppSettings.setLogin(false);
                        new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                    }
                }

                @Override
                public void onFailure(Call<ResponseGetUserProfileData> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });


        } else {
            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, getResources().getString(R.string.check_internet));
        }
    }


    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    @Override
    public void onBackPressed() {
        try {
            videoView.getPlayer().release();
//            Intent i = new Intent(this, HomeUserPostsActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(i);
            finish();
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.getPlayer().setPlayWhenReady(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        videoView.getPlayer().setPlayWhenReady(false);
    }

    private void getUserHomePostData(String userId, String profileCatId) {


        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseUserPost> userPostsCall = loginRequest.get_only_user_posts(userId, profileCatId);
        userPostsCall.enqueue(new Callback<ResponseUserPost>() {
            @Override
            public void onResponse(Call<ResponseUserPost> call, retrofit2.Response<ResponseUserPost> response) {
                setData(response);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseUserPost> call, Throwable t) {
                inProgress = false;
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });


    }

    public void setData(Response<ResponseUserPost> response) {
        ResponseUserPost responseFaqs = response.body();
        if (response.body() != null && response.body().getStatus().equals("success")) {

            responseFaqs.setPostinfo(responseFaqs.getPostinfo());
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            if (responseFaqs.getPostinfo() != null) {
                favUserPostListAdapter = new FavUserPostListAdapter(getActivity(), responseFaqs.getPostinfo());
                recyclerViewPosts.setAdapter(favUserPostListAdapter);
                recyclerViewPosts.setLayoutManager(mLayoutManager);
            } else {
                text_view_nopost.setVisibility(View.VISIBLE);
                text_view_nopost.setText("Post not found.");
//                swipe_refresh_layout.setVisibility(View.GONE);
            }

        } else if (response.body().getStatus().equals("error")) {
            text_view_nopost.setVisibility(View.VISIBLE);
            text_view_nopost.setText("Post not found.");
//            swipe_refresh_layout.setVisibility(View.GONE);
        }
    }

    private void setPostsComments(final String post_id, final String comment, String device_date) {
        if (isNetworkAvailable()) {
//            progressDialog = new CustomProgressDialog(this, "Saving Comment...");
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            progressDialog.show();

            try {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface commentRequest = retrofit.create(ApiInterface.class);
                Call<Set_Comment> userPostsCall = commentRequest.setCommentPosts(AppSettings.getLoginUserId(), post_id, comment, device_date);
                userPostsCall.enqueue(new Callback<Set_Comment>() {

                    @Override
                    public void onResponse(Call<Set_Comment> call, retrofit2.Response<Set_Comment> response) {

                        Log.d(TAG, "## setPostsComments :" + response.body().toString());
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            getPostComments(post_id, "0");
                        } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
//                            if (progressDialog != null && progressDialog.isShowing())
//                                progressDialog.dismiss();
                            CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Something went wrong...try again");
                        }
                    }

                    @Override
                    public void onFailure(Call<Set_Comment> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } catch (Exception e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
            }
        } else {
            CommonDialogs.showMessageDialog(FavoriteUserInfoActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    private Dialog commetsDialog = null;

    public void showCommentDialog(ArrayList<Array_of_comment> list_post_comment, String postID) {

        if (list_post_comment == null)
            list_post_comment = new ArrayList<Array_of_comment>();

        if (commetsDialog == null) {
            commetsDialog = new Dialog(context);
            commetsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            commetsDialog.setContentView(R.layout.speakout_your_mind_comment_dialog);
            commetsDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            ListView lv = (ListView) commetsDialog.findViewById(R.id.lv_comments);
            et_comment = (EditText) commetsDialog.findViewById(R.id.et_comment);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());
            PostsCommentsAdapter postsCommentsAdapter = new PostsCommentsAdapter(list_post_comment, this);
            lv.setAdapter(postsCommentsAdapter);
            Button btn_send = (Button) commetsDialog.findViewById(R.id.btn_send);
            btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateForm(et_comment.getText().toString().trim())) {
                        try {
                            setPostsComments(postID, URLEncoder.encode(et_comment.getText().toString(), HTTP.UTF_8), currentDateandTime);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        et_comment.setText("");
//                        btn_send.setBackgroundColor(R.color.list_divider);
                    }
                }
            });

            commetsDialog.setCancelable(true);
            commetsDialog.setTitle("Comments");
            commetsDialog.show();
            commetsDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    commetsDialog.dismiss();
                    commetsDialog = null;
                }
            });
        } else {
            ListView lv = (ListView) commetsDialog.findViewById(R.id.lv_comments);
            PostsCommentsAdapter postsCommentsAdapter = new PostsCommentsAdapter(list_post_comment, this);
            lv.setAdapter(postsCommentsAdapter);

            ((EditText) commetsDialog.findViewById(R.id.et_comment)).setText("");
        }

        View view = commetsDialog.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //validation for email id and password(edittexts)
    public boolean validateForm(String comment) {
        boolean valid = true;
        try {
            et_comment.setError(null);
            if (comment.length() == 0) {
                setErrorMsg("Please enter Comment.", et_comment, true);
                valid = false;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return valid;
    }

    public void getPostComments(final String post_id, final String pageIndex) {
        if (isNetworkAvailable()) {
//            if (progressDialog == null || !progressDialog.isShowing()) {
//                progressDialog = new CustomProgressDialog(this, "Loading old comments...");
//                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                progressDialog.show();
//            }

            try {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface commentRequest = retrofit.create(ApiInterface.class);
                Call<ResponseGetCommentsByPostID> userPostsCall = commentRequest.getCommentsByPostID(post_id, pageIndex);
                userPostsCall.enqueue(new Callback<ResponseGetCommentsByPostID>() {

                    @Override
                    public void onResponse(Call<ResponseGetCommentsByPostID> call, retrofit2.Response<ResponseGetCommentsByPostID> response) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                        Log.d(TAG, "## setPostsComments :" + response.body().toString());

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            showCommentDialog(response.body().getComment_list(), post_id);
                            if (isNetworkAvailable()) {
                                getUserHomePostData(fav_user_id, catId);
                            }
                        } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                            if (response.body().getComment_list() != null)
                                CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Something went wrong...try again");
                            else
                                showCommentDialog(null, post_id);

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetCommentsByPostID> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } catch (Exception e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
            }
        } else {
            CommonDialogs.showMessageDialog(FavoriteUserInfoActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    public void saveUserPostLike(final String post_id, final int position, final FavUserPostListAdapter.ViewHolder holder) {
        if (isNetworkAvailable()) {
//            progressDialog = new CustomProgressDialog(this, "Please wait...");
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            progressDialog.show();
            Log.d(TAG, "## post_id : " + post_id);

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface loginRequest = retrofit.create(ApiInterface.class);
            Call<LikeUnlike> userPostsCall = loginRequest.setLikeUnlike(AppSettings.getLoginUserId(), post_id);
            userPostsCall.enqueue(new Callback<LikeUnlike>() {
                @Override
                public void onResponse(Call<LikeUnlike> call, retrofit2.Response<LikeUnlike> response) {
//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();

                    Log.d(TAG, "## :" + response.body().toString());
                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        favUserPostListAdapter.addLike(position, holder, response.body().getLike_count(), response.body().getMsg());
                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), response.body().getMsg());
                    }
                }

                @Override
                public void onFailure(Call<LikeUnlike> call, Throwable t) {

//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();
                }
            });

        } else {
            CommonDialogs.showMessageDialog(FavoriteUserInfoActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    public void reportAbusePost(String postId, String postUserId, String userID) {
        showProgressBar();

        try {
            if (isNetworkAvailable()) {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface loginRequest = retrofit.create(ApiInterface.class);
                Call<deletePostResponse> userPostsCall = loginRequest.reportAbusePost(userID, postId, postUserId);
                userPostsCall.enqueue(new Callback<deletePostResponse>() {
                    @Override
                    public void onResponse(Call<deletePostResponse> call, Response<deletePostResponse> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        Log.d(TAG, "## " + response.body().toString());
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, response.body().getMsg());
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, response.body().getMsg());
                        }
                    }

                    @Override
                    public void onFailure(Call<deletePostResponse> call, Throwable t) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                CommonDialogs.dialog_with_one_btn_without_title(FavoriteUserInfoActivity.this, getResources().getString(R.string.check_internet));
            }
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void sendFriendRequest(final String receiver_user_id) {
        progressDialog.setMessage(FavoriteUserInfoActivity.this.getString(R.string.msg_sending_friend_request));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = format.format(new Date());
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<ResponseSuccess> loginCall = apiInterface.sendOrbitRequest(AppSettings.getLoginUserId(), receiver_user_id, currentDateandTime);
        loginCall.enqueue(new Callback<ResponseSuccess>() {
            @Override
            public void onResponse(Call<ResponseSuccess> call, retrofit2.Response<ResponseSuccess> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {//success
                    final Dialog dialog = CommonDialogs.showMessageDialog(FavoriteUserInfoActivity.this, getResources().getString(R.string.ongravity), response.body().getMsg());
                    Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                    dialog_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            imageview_add_friend.setVisibility(View.GONE);
                        }
                    });

                } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                    CommonDialogs.showMessageDialog(FavoriteUserInfoActivity.this, getResources().getString(R.string.ongravity), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Toast.makeText(FavoriteUserInfoActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
            }
        });
    }
}