package com.exceptionaire.ongraviti.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.utilities.CustomVolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ChangePassword extends BaseActivity implements View.OnClickListener,Response.Listener,Response.ErrorListener
{
    private EditText edt_oldPW,edt_newPW,edt_confirmPW;
    private Button btnSubmit;
    private String oldPW,newPW,confirmPW,status,message;
    private ProgressDialog progressDialog;
    private RequestQueue requestQueue;
    private CustomVolleyRequest customVolleyRequest;
    long TIME = 10 * 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.layout_change_password);
        init();

    }
    private void init()
    {
        edt_oldPW= (EditText) findViewById(R.id.edt_changePW_oldPassword);
        edt_newPW= (EditText) findViewById(R.id.edt_changePW_newPassword);
        edt_confirmPW= (EditText) findViewById(R.id.edt_changePW_newCNFRMPassword);
        btnSubmit= (Button) findViewById(R.id.btn_changePW_submit);
        btnSubmit.setOnClickListener(this);
    }

    private void changePassword() {
        oldPW=edt_oldPW.getText().toString();
        newPW=edt_newPW.getText().toString();
        confirmPW=edt_confirmPW.getText().toString();
        showProgressBar();
        Map<String, String> map = new HashMap<String, String>();
        map.put("old_password", oldPW);
        map.put("new_password", newPW);
        map.put("confirm_password",confirmPW);
        map.put("user_id",AppSettings.getLoginUserId());
        requestQueue = Volley.newRequestQueue(ChangePassword.this);
        customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.CHANGE_PASSWORD, map, this, this);
        requestQueue.add(customVolleyRequest);
    }

    private void showProgressBar() {
        progressDialog = new ProgressDialog(ChangePassword.this);
        progressDialog.setMessage("Checking...");
        progressDialog.setProgressStyle(progressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    private void blankEdittext() {
        edt_oldPW.setText("");
        edt_newPW.setText("");
        edt_confirmPW.setText("");
    }


    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_changePW_submit:
                oldPW = edt_oldPW.getText().toString();
                newPW = edt_newPW.getText().toString();
                confirmPW = edt_confirmPW.getText().toString();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                    }
                }, TIME);
                if (validateForm(oldPW,newPW,confirmPW)==true) {
                    changePassword();
                }
                finish();

        }
    }
    public boolean validateForm(String old_password, String new_password , String confirm_password) {
        boolean valid = true;
        edt_oldPW.setError(null);
        edt_newPW.setError(null);
        edt_confirmPW.setError(null);
        if (old_password.length() <= 0)
        {
            setErrorMsg(getResources().getString(R.string.enter_password), edt_oldPW, true);
            valid = false;
        }

        else if (new_password.length() <= 0)
        {
            setErrorMsg(getResources().getString(R.string.enter_password), edt_newPW, true);
            valid = false;
        }
        else if (confirm_password.length() <= 0)
        {
            setErrorMsg(getResources().getString(R.string.enter_password), edt_confirmPW, true);
            valid = false;
        }

        return valid;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("", "## onErrorResponse : " + error.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            error.printStackTrace();
            String errorMessage = getString(R.string.error_connection_generic);
            if (error instanceof NoConnectionError)
                errorMessage = getString(R.string.error_no_connection);
            else if (error instanceof AuthFailureError)
                errorMessage = getString(R.string.error_authentication);
            else if (error instanceof NetworkError)
                errorMessage = getString(R.string.error_network_error);
            else if (error instanceof ParseError)
                errorMessage = getString(R.string.error_parse_error);
            else if (error instanceof ServerError)
                errorMessage = getString(R.string.error_server_error);
            else if (error instanceof TimeoutError)
                errorMessage = getString(R.string.error_connection_timeout);
            Toast.makeText(ChangePassword.this, errorMessage, Toast.LENGTH_LONG).show();
            Intent intentHome=new Intent(ChangePassword.this,HomeUserPostsActivity.class);
            startActivity(intentHome);
        } catch (Exception e) {
            Log.e("", "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response)
    {
        JSONObject jObject = null;
        try {
            jObject = new JSONObject(String.valueOf(response));
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            status = jObject.getString("status");
            message = jObject.getString("msg");
            if (status.equals("success"))
            {
                blankEdittext();
                CommonDialogs.dialog_with_one_btn_without_title(ChangePassword.this, message);

            }
            else if (status.equalsIgnoreCase("error"))
            {
                CommonDialogs.dialog_with_one_btn_without_title(ChangePassword.this, message);
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}
