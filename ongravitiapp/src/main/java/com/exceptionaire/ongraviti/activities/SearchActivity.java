package com.exceptionaire.ongraviti.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.ArcMenu;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.StateChangeListener;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.ResponseForSearchQuery;
import com.exceptionaire.ongraviti.model.SearchData;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

public class SearchActivity extends BaseDrawerActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    //    private SearchView searchView;
    private Button buttonMale;
    private Button buttonFemale, button_bisexual;
    private Button buttonSearch;
    //private boolean isMaleSelected = true;
    private ImageView imageViewBack;
    private ImageView imageViewSearch;
    private EditText editTextSearch;
    private CustomProgressDialog progressDialog;
    private RadioGroup radioGroupProfile;
    private RadioGroup radioGroupSmoking;
    private RadioGroup radioGroupDrinking;
    private RadioGroup radioGroupRelationshipStatus;
    private TextView textViewClear;
    private String profileId = "", smokingId = "", drinkingId = "", relationshipStatusId = "", genderId = "";
    private ArrayList<SearchData> listSearchedData;
    private static int HeaderIconSelection = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        // getLayoutInflater().inflate(R.layout.activity_search, frameLayout);
        arcMenu.setVisibility(View.VISIBLE);

        initialiseComponent();
    }

    private void initialiseComponent() {
        ImageView image_view_arcback = (ImageView) findViewById(R.id.image_view_arcback);
        final OvershootInterpolator[] interpolator = {new OvershootInterpolator()};
        arcMenu = (ArcMenu) findViewById(R.id.arcMenu);
        arcMenu.setRadius(getResources().getDimension(R.dimen.radius));

        arcMenu.setStateChangeListener(new StateChangeListener() {
            @Override
            public void onMenuOpened() {
                interpolator[0] = new OvershootInterpolator();
                float rotationAnim = ViewCompat.getRotation(arcMenu.fabMenu);
                image_view_arcback.setVisibility(View.VISIBLE);
                ViewCompat.animate(arcMenu.fabMenu).
                        rotation(rotationAnim + 360f).
                        withLayer().
                        setDuration(300).
                        setInterpolator(interpolator[0]).
                        start();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        arcMenu.fabMenu.setImageDrawable(ContextCompat.getDrawable(SearchActivity.this, R.drawable.ic_action_close));
                    }
                }, 300);
            }

            @Override
            public void onMenuClosed() {
                image_view_arcback.setVisibility(View.INVISIBLE);
                arcMenu.fabMenu.setImageDrawable(ContextCompat.getDrawable(SearchActivity.this, R.drawable.ic_fab_menu));
            }
        });
//        findViewById(R.id.fab_ic_search).setOnClickListener(subMenuClickListener);
        findViewById(R.id.fab_ic_dating).setOnClickListener(subMenuClickListener);
        findViewById(R.id.fab_ic_business).setOnClickListener(subMenuClickListener);
        findViewById(R.id.fab_ic_datingrequest).setOnClickListener(subMenuClickListener);
        buttonMale = (Button) findViewById(R.id.button_male);
        buttonFemale = (Button) findViewById(R.id.button_female);
        button_bisexual = (Button) findViewById(R.id.button_bisexual);
        imageViewBack = (ImageView) findViewById(R.id.image_view_back);
        buttonSearch = (Button) findViewById(R.id.button_search);
        imageViewSearch = (ImageView) findViewById(R.id.image_view_search);
        editTextSearch = (EditText) findViewById(R.id.edit_text_search);
        radioGroupProfile = (RadioGroup) findViewById(R.id.radio_group_profile);
        radioGroupSmoking = (RadioGroup) findViewById(R.id.radio_group_smoking);
        radioGroupDrinking = (RadioGroup) findViewById(R.id.radio_group_drinking);
        radioGroupRelationshipStatus = (RadioGroup) findViewById(R.id.radio_group_relationship_status);
        textViewClear = (TextView) findViewById(R.id.text_view_clear);


        buttonMale.setOnClickListener(this);
        buttonFemale.setOnClickListener(this);
        button_bisexual.setOnClickListener(this);
        imageViewBack.setOnClickListener(this);
        buttonSearch.setOnClickListener(this);
        radioGroupProfile.setOnCheckedChangeListener(this);
        radioGroupDrinking.setOnCheckedChangeListener(this);
        radioGroupSmoking.setOnCheckedChangeListener(this);
        radioGroupRelationshipStatus.setOnCheckedChangeListener(this);
        textViewClear.setOnClickListener(this);

    }

    private View.OnClickListener subMenuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            arcMenu.toggleMenu();
            switch (v.getId()) {
              /*  case R.id.fab_ic_search:

                    HeaderIconSelection = 0;
                    Intent intentSearch = new Intent(SearchActivity.this, SearchActivity.class);
                    startActivity(intentSearch);
                    // finish();
                    break;*/

                case R.id.fab_ic_dating:
                    HeaderIconSelection = 1;
                    Intent intentDating = new Intent(SearchActivity.this, AskForDating.class);
                    startActivity(intentDating);
                    finish();
                    break;
                case R.id.fab_ic_business:
                    HeaderIconSelection = 2;
                    Intent intentBusiness = new Intent(SearchActivity.this, AskForBusiness.class);
                    startActivity(intentBusiness);
                    // finish();
                    break;
                case R.id.fab_ic_datingrequest:
                    HeaderIconSelection = 3;
                    Intent intentDatingRequest = new Intent(SearchActivity.this, AcceptDatingRequest.class);
                    startActivity(intentDatingRequest);
                    // finish();
                    break;

            }
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.button_male:

                genderId = "0";
                buttonMale.setBackgroundResource(R.drawable.button_background_checked);
                buttonFemale.setBackgroundResource(R.drawable.button_background_unchecked);
                button_bisexual.setBackgroundResource(R.drawable.button_background_unchecked);
                break;

            case R.id.button_female:

                genderId = "1";
                buttonMale.setBackgroundResource(R.drawable.button_background_unchecked);
                buttonFemale.setBackgroundResource(R.drawable.button_background_checked);
                button_bisexual.setBackgroundResource(R.drawable.button_background_unchecked);
                break;
            case R.id.button_bisexual:
                genderId = "2";
                buttonMale.setBackgroundResource(R.drawable.button_background_unchecked);
                buttonFemale.setBackgroundResource(R.drawable.button_background_unchecked);
                button_bisexual.setBackgroundResource(R.drawable.button_background_checked);
                break;
            case R.id.image_view_back:

                finish();
                break;

            case R.id.button_search:

                getAdvancedSearch();
                break;

            case R.id.text_view_clear:

                profileId = "";
                smokingId = "";
                drinkingId = "";
                relationshipStatusId = "";
                genderId = "";
                editTextSearch.setText("");
                buttonMale.setBackgroundResource(R.drawable.button_background_unchecked);
                buttonFemale.setBackgroundResource(R.drawable.button_background_unchecked);
                button_bisexual.setBackgroundResource(R.drawable.button_background_unchecked);
                radioGroupRelationshipStatus.clearCheck();
                radioGroupSmoking.clearCheck();
                radioGroupDrinking.clearCheck();
                radioGroupProfile.clearCheck();
                break;
        }
    }

    private void getAdvancedSearch() {

        showProgressBar();

        Retrofit retrofit = ApiClient.getClient();
        final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<ResponseForSearchQuery> accessTokenCall = requestInterface.getAdvanceSearchResult(editTextSearch.getText().toString().trim(), profileId, genderId, smokingId, drinkingId, relationshipStatusId, AppSettings.getLoginUserId());
        accessTokenCall.enqueue(new Callback<ResponseForSearchQuery>() {
            @Override
            public void onResponse(Call<ResponseForSearchQuery> call, retrofit2.Response<ResponseForSearchQuery> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    if (response.body() != null && response.body().getStatus().equals(AppConstants.RESPONSE_SUCCESS)) {

                        if (response.body().getResult() != null && response.body().getResult().length > 0) {
                            listSearchedData = new ArrayList<>(Arrays.asList(response.body().getResult()));

                            Intent intent = new Intent(SearchActivity.this, SearchedResultActivity.class);
                            intent.putParcelableArrayListExtra("SearchedList", listSearchedData);
                            startActivity(intent);
//                            finish();
                        }
                    } else {
                        Toast.makeText(SearchActivity.this, "No data found that matches your criteria.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseForSearchQuery> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(SearchActivity.this, getResources().getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (radioGroup.getId()) {
            case R.id.radio_group_profile:

                int idProfile = radioGroup.getCheckedRadioButtonId();
                RadioButton buttonProfile = (RadioButton) findViewById(idProfile);
                if (buttonProfile == null) {
                    profileId = "";
                }
                switch (idProfile) {
                    case R.id.radio_button_profile_social:
                        profileId = "1";
                        break;
                    case R.id.radio_button_profile_dating:
                        profileId = "2";
                        break;
                    case R.id.radio_button_profile_business:
                        profileId = "3";
                        break;
                }
                break;

            case R.id.radio_group_smoking:

                int idSmoking = radioGroup.getCheckedRadioButtonId();
                RadioButton buttonSmoking = (RadioButton) findViewById(idSmoking);
                if (buttonSmoking != null)
                    smokingId = String.valueOf(buttonSmoking.getText());
                else smokingId = "";

                break;
            case R.id.radio_group_drinking:

                int idDrinking = radioGroup.getCheckedRadioButtonId();
                RadioButton buttonDrinking = (RadioButton) findViewById(idDrinking);
                if (buttonDrinking != null)
                    drinkingId = String.valueOf(buttonDrinking.getText());
                else drinkingId = "";
                break;

            case R.id.radio_group_relationship_status:

                int idRelationship = radioGroup.getCheckedRadioButtonId();
                RadioButton buttonRelationshipStatus = (RadioButton) findViewById(idRelationship);
                if (buttonRelationshipStatus != null)
                    relationshipStatusId = String.valueOf(buttonRelationshipStatus.getText());
                else relationshipStatusId = "";
                break;
        }
    }
}