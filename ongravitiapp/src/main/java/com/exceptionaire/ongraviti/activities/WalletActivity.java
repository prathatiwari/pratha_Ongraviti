package com.exceptionaire.ongraviti.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by root on 8/2/17.
 */

public class WalletActivity extends BaseDrawerActivity {
    private Context context;
    String Amount = "";

    //paypal payment gateway integration for wallet (add money) on 23 mar '17
    private static final String TAG = "paymentExample";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "ASFzUx-x1rv57s28d1ZmK4HYyht65sEO3vSXcSRg0-5ob1VpbZ6zDTD6AfVHZbHWM052RfCs4fGXbryh";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_wallet, frameLayout);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        TextView header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText("Wallet");

        context = this;
        arcMenu.setVisibility(View.GONE);


        TextView tv_how_to_purchase = (TextView) findViewById(R.id.tv_how_to_purchase);
        tv_how_to_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    public void onAddCreditClick(View view) {
        final Dialog dialog = new Dialog(context, R.style.DialogStyle);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_money);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        ((TextView) dialog.findViewById(R.id.text_view_dialog_header)).setText("Amount");
//            ((TextView) dialog.findViewById(R.id.text_view_dialog_message)).setText(dialogMessage);
        final EditText editAmount = (EditText) dialog.findViewById(R.id.edtAmount);
        final ImageView img500 = (ImageView) dialog.findViewById(R.id.img500);
        final ImageView img1000 = (ImageView) dialog.findViewById(R.id.img1000);
        final ImageView img1500 = (ImageView) dialog.findViewById(R.id.img1500);
        final ImageView img2000 = (ImageView) dialog.findViewById(R.id.img2000);

        img500.setTag("uncheck");
        img1000.setTag("uncheck");
        img1500.setTag("uncheck");
        img2000.setTag("uncheck");

        img500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (img500.getTag().equals("uncheck")) {
                    editAmount.setError(null);
                    img500.setImageResource(R.drawable.d_chk);
                    img500.setTag("check");
                    editAmount.setText("500");

                    if (img1000.getTag().equals("check")) {
                        img1000.setImageResource(R.drawable.chekmark_de);
                        img1000.setTag("uncheck");
                    }
                    if (img1500.getTag().equals("check")) {
                        img1500.setImageResource(R.drawable.chekmark_de);
                        img1500.setTag("uncheck");
                    }
                    if (img2000.getTag().equals("check")) {
                        img2000.setImageResource(R.drawable.chekmark_de);
                        img2000.setTag("uncheck");
                    }
                }
            }
        });

        img1000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (img1000.getTag().equals("uncheck")) {
                    editAmount.setError(null);
                    img1000.setImageResource(R.drawable.d_chk);
                    img1000.setTag("check");
                    editAmount.setText("1000");

                    if (img500.getTag().equals("check")) {
                        img500.setImageResource(R.drawable.chekmark_de);
                        img500.setTag("uncheck");
                    }
                    if (img1500.getTag().equals("check")) {
                        img1500.setImageResource(R.drawable.chekmark_de);
                        img1500.setTag("uncheck");
                    }
                    if (img2000.getTag().equals("check")) {
                        img2000.setImageResource(R.drawable.chekmark_de);
                        img2000.setTag("uncheck");
                    }
                }
            }
        });

        img1500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (img1500.getTag().equals("uncheck")) {
                    editAmount.setError(null);
                    img1500.setImageResource(R.drawable.d_chk);
                    img1500.setTag("check");
                    editAmount.setText("1500");

                    if (img500.getTag().equals("check")) {
                        img500.setImageResource(R.drawable.chekmark_de);
                        img500.setTag("uncheck");
                    }
                    if (img1000.getTag().equals("check")) {
                        img1000.setImageResource(R.drawable.chekmark_de);
                        img1000.setTag("uncheck");
                    }
                    if (img2000.getTag().equals("check")) {
                        img2000.setImageResource(R.drawable.chekmark_de);
                        img2000.setTag("uncheck");
                    }
                }
            }
        });

        img2000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (img2000.getTag().equals("uncheck")) {
                    editAmount.setError(null);
                    img2000.setImageResource(R.drawable.d_chk);
                    img2000.setTag("check");
                    editAmount.setText("2000");

                    if (img500.getTag().equals("check")) {
                        img500.setImageResource(R.drawable.chekmark_de);
                        img500.setTag("uncheck");
                    }
                    if (img1500.getTag().equals("check")) {
                        img1500.setImageResource(R.drawable.chekmark_de);
                        img1500.setTag("uncheck");
                    }
                    if (img1000.getTag().equals("check")) {
                        img1000.setImageResource(R.drawable.chekmark_de);
                        img1000.setTag("uncheck");
                    }
                }
            }
        });

        dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validate edittext
                Amount = editAmount.getText().toString().trim();
                if (Amount.length() == 0) {
                    editAmount.setError("Enter Amount or Select one");
                } else {
                    dialog.dismiss();
                    editAmount.setError(null);
                    Log.d("Wallet", "## Amount :" + Amount);
                     /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
                    PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

                    Intent intent = new Intent(WalletActivity.this, PaymentActivity.class);

                    // send the same configuration for restart resiliency
                    intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

                    intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

                    startActivityForResult(intent, REQUEST_CODE_PAYMENT);
                }
            }
        });

        dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(Amount), "USD", "sample item",
                paymentIntent);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    private PayPalOAuthScopes getOauthScopes() {
        Set<String> scopes = new HashSet<String>(
                Arrays.asList(PayPalOAuthScopes.PAYPAL_SCOPE_EMAIL, PayPalOAuthScopes.PAYPAL_SCOPE_ADDRESS));
        return new PayPalOAuthScopes(scopes);
    }

    protected void displayResultText(String result) {
//        ((TextView)findViewById(R.id.txtResult)).setText("SearchData : " + result);
        Toast.makeText(
                getApplicationContext(),
                result, Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        displayResultText("PaymentConfirmation info received from PayPal");


                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("Future Payment code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("Profile Sharing code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}