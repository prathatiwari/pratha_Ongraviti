package com.exceptionaire.ongraviti.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.SpeakUpYourMindCommentListAdapter;
import com.exceptionaire.ongraviti.activities.adapters.SpeakUpYourMindListAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.imoji.EmojiconEditText;
import com.exceptionaire.ongraviti.imoji.EmojiconGridView;
import com.exceptionaire.ongraviti.imoji.EmojiconsPopup;
import com.exceptionaire.ongraviti.imoji.emoji.Emojicon;
import com.exceptionaire.ongraviti.model.CategoryInterest;
import com.exceptionaire.ongraviti.model.Comment;
import com.exceptionaire.ongraviti.model.FeedInformation;
import com.exceptionaire.ongraviti.model.OrbitFriend;
import com.exceptionaire.ongraviti.model.ResponseGetInterestSubcategory;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;


public class SpeakOutYourMindActivity extends BaseDrawerActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private Context context;
    private CustomProgressDialog progressDialog;
    private SpeakUpYourMindListAdapter speakUpYourMindListAdapter;
    private ListView lv_posts;
    private TextView tv_msg;
    private Spinner mSpinner, spinner_posted_by;
    private ImageView imageViewEmojis;
    private EmojiconEditText et_post;
    private String identityFlag = "1";
    int i = 0;
    private List<FeedInformation> feedInformationList;
    private List<CategoryInterest> categories;
    private String cat_id = "empty";
    private ArrayList<OrbitFriend> orbitFriends;
    private SwipeRefreshLayout swipe_refresh_layout;
    private View rootView;
    private EmojiconsPopup popup;

    private String has_next_data = "0";
    private String next_data = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLayoutInflater().inflate(R.layout.activity_speak_out_your_mind, frameLayout);
        TextView header = (TextView) findViewById(R.id.toolbar_title_drawer);
        rootView = findViewById(R.id.list_parent);
        header.setText(getResources().getString(R.string.speak_out));
        arcMenu.setVisibility(View.GONE);

        imageViewEmojis = (ImageView) findViewById(R.id.ibtn_emojis);
        imageViewEmojis.setOnClickListener(this);
        context = this;
        feedInformationList = new ArrayList<FeedInformation>();
        categories = new ArrayList<CategoryInterest>();

        mSpinner = (Spinner) findViewById(R.id.spinner_category);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cat_id = categories.get(position).getCat_id();
                Log.d("Category", cat_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_posted_by = (Spinner) findViewById(R.id.spinner_posted_by);
        spinner_posted_by.setPrompt("Select");


        spinner_posted_by.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //cat_id = categories.get(position).getCat_id();
                speakUpYourMindListAdapter.filter(orbitFriends.get(position).getUserID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lv_posts = (ListView) findViewById(R.id.lv_posts);
        swipe_refresh_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        tv_msg = (TextView) findViewById(R.id.tv_msg);

        final List feeds = new ArrayList<FeedInformation>();


        tv_msg.setVisibility(View.GONE);
        lv_posts.setVisibility(View.VISIBLE);
        swipe_refresh_layout.setVisibility(View.VISIBLE);


        et_post = (EmojiconEditText) findViewById(R.id.et_post);

        speakUpYourMindListAdapter = new SpeakUpYourMindListAdapter(feeds, this);
        lv_posts.setAdapter(speakUpYourMindListAdapter);

        final CheckBox tv_hide_identity = (CheckBox) findViewById(R.id.tv_hide_identity);
        tv_hide_identity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (tv_hide_identity.isChecked()) {
                    identityFlag = "0";
                } else {
                    identityFlag = "1";
                }
            }
        });

        lv_posts.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view,
                                             int scrollState) {
                // TODO Auto-generated method stub
                int threshold = 1;
                int count = lv_posts.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (lv_posts.getLastVisiblePosition() >= count
                            - threshold) {


                        if (has_next_data.equals("1")) {
                            getSpeakOutYourMindFeeds();
                        }

                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
            }
        });


        swipe_refresh_layout.setOnRefreshListener(this);

        getSpeakOutYourMindCategories();
        getSpeakOutYourMindFeeds();
        showOrbitFriendList();

        popup = new EmojiconsPopup(rootView, this);

        //Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        //If the emoji popup is dismissed, change imageViewEmojis to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
//                changeEmojiKeyboardIcon(imageViewEmojis, R.mipmap.smiley);
                imageViewEmojis.setImageResource(R.drawable.smiley);
            }
        });

        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (et_post == null || emojicon == null) {
                    return;
                }

                int start = et_post.getSelectionStart();
                int end = et_post.getSelectionEnd();
                if (start < 0) {
                    et_post.append(emojicon.getEmoji());
                } else {
                    et_post.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                et_post.dispatchKeyEvent(event);
            }
        });

    }

    public void saveFeed(View view) {
        Log.d("Category", cat_id);
        if (!cat_id.equalsIgnoreCase("empty")) {
            if (et_post.getText().toString() != null && !et_post.getText().toString().isEmpty()) {
                et_post.setError(null);
                Spanned sp = et_post.getText();
                //chats.add(sp);
                //mAdapter.notifyDataSetChanged();
                saveSpeakUpYourMindFeed();
            } else {
                et_post.setError("Can not be empty !");
            }
        } else {
            CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Please select a category");
        }
    }

    public void addLike(String feed_id, int position) {
        saveSpeakUpYourMindLike(feed_id, position);
    }

    public void commentsDialog(final String feed_id, int position) {
        getSpeakOutYourMindComments(feed_id, position);
    }

    private void getSpeakOutYourMindCategories() {
        progressDialog = new CustomProgressDialog(this, "Loading Categories...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/get_speak_out_your_mind_category",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            //   progressDialog.dismiss();

                            System.out.println("get_categories :Response " + response);
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);

                            // message = jObject.getString("msg");
                            String status = jObject.getString("status");
                            if (status.equals("success")) {
                                categories.clear();

                                CategoryInterest category = new CategoryInterest();
                                category.setCat_id("empty");
                                category.setCat_type("Select");
                                categories.add(category);

                                Object obj = jObject.get("catList");
                                if (obj instanceof JSONArray) {
                                    JSONArray requestLists = jObject.getJSONArray("catList");
                                    for (int i = 0; i < requestLists.length(); i++) {
                                        Log.d("catList count", "" + i);

                                        JSONObject detailsObject = requestLists.getJSONObject(i);
                                        CategoryInterest request = new CategoryInterest();
                                        request.setCat_id(detailsObject.getString("speekmind_cat_id"));
                                        request.setCat_type(detailsObject.getString("category"));
                                        request.setCat_status(detailsObject.getString("status"));

                                        categories.add(request);
                                    }

                                    if (categories.size() > 0) {
                                        ArrayAdapter<CategoryInterest> adapter = new ArrayAdapter<CategoryInterest>(context, android.R.layout.simple_spinner_item, categories);
                                        adapter.setDropDownViewResource(R.layout.spinner_item);
                                        mSpinner.setAdapter(adapter); // this will set list of values to spinner
                                    }
                                } else {

                                }

                            } else if (status.equalsIgnoreCase("error")) {
                                //CommonDialogs.showMessageDialog(context, "Error", message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("", "## onErrorResponse : " + error.toString());
                        try {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();

                            error.printStackTrace();
                            String errorMessage = getString(R.string.error_connection_generic);
                            if (error instanceof NoConnectionError)
                                errorMessage = getString(R.string.error_no_connection);
                            else if (error instanceof AuthFailureError)
                                errorMessage = getString(R.string.error_authentication);
                            else if (error instanceof NetworkError)
                                errorMessage = getString(R.string.error_network_error);
                            else if (error instanceof ParseError)
                                errorMessage = getString(R.string.error_parse_error);
                            else if (error instanceof ServerError)
                                errorMessage = getString(R.string.error_server_error);
                            else if (error instanceof TimeoutError)
                                errorMessage = getString(R.string.error_connection_timeout);
                            CommonDialogs.showMessageDialog(SpeakOutYourMindActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                            // Intent intentHome=new Intent(context,ActivityHomeUserPost.class);
                            //startActivity(intentHome);
                        } catch (Exception e) {
                            Log.e("", "## error : " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", AppSettings.getLoginUserId());
                System.out.println(map);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    private void getSpeakOutYourMindFeeds() {
        // progressDialog = new CustomProgressDialog(this, "Loading Feeds...");
        // progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/get_speak_out_your_mind_feed",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            // progressDialog.dismiss();

                            System.out.println("get_notifications :Response " + response);
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);

                            // message = jObject.getString("msg");
                            String status = jObject.getString("status");
                            has_next_data = jObject.getString("has_data");
                            next_data = jObject.getString("next_start");
                            if (status.equals("success")) {
                                if (jObject.has("feed_list")) {
                                    feedInformationList.clear();
                                    JSONArray requestLists = jObject.getJSONArray("feed_list");
                                    FeedInformation request = null;
                                    for (int i = 0; i < requestLists.length(); i++) {
                                        Log.d("feed_list count", "" + i);

                                        JSONObject detailsObject = requestLists.getJSONObject(i);
                                        request = new FeedInformation();

                                        request.setFeed_id(detailsObject.getString("speak_ur_mind_id"));
                                        request.setFeed_userid(detailsObject.getString("user_id"));
                                        request.setFeed_username(detailsObject.getString("name"));
                                        request.setFeed_user_pic(detailsObject.getString("profile_picture"));
                                        request.setFeed_date(detailsObject.getString("post_date"));
                                        request.setFeed_text(detailsObject.getString("post_content"));
                                        request.setIdentity(detailsObject.getString("identity"));
                                        request.setCategory_type(detailsObject.getString("profile_cat_id"));
                                        request.setCategory(detailsObject.getString("speekmind_cat_id"));

                                        request.setCount_of_like(Integer.parseInt(detailsObject.getString("likeCount")));
                                        request.setCount_of_comment(Integer.parseInt(detailsObject.getString("commentCount")));

                                        feedInformationList.add(request);
                                    }

                                    if (feedInformationList.size() > 0) {
                                        speakUpYourMindListAdapter.setFeedInformationList(feedInformationList);
                                        tv_msg.setVisibility(View.GONE);
                                        lv_posts.setVisibility(View.VISIBLE);
//                               swipe_refresh_layout.setVisibility(View.GONE);
                                        swipe_refresh_layout.setRefreshing(false);
                                    } else {
                                        tv_msg.setVisibility(View.VISIBLE);
                                        lv_posts.setVisibility(View.GONE);
                                        swipe_refresh_layout.setVisibility(View.GONE);
                                    }
                                }
                                //progressDialog.dismiss();

                            } else if (status.equalsIgnoreCase("error")) {
                                //CommonDialogs.showMessageDialog(context, "Error", message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        // Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", AppSettings.getLoginUserId());
                map.put("next_start", "");
                System.out.println(map);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    private void saveSpeakUpYourMindFeed() {
        progressDialog = new CustomProgressDialog(this, "Saving Feed...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/set_speak_out_your_mind_feed",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        System.out.println("save feed :Response " + response);
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);

                            // message = jObject.getString("msg");
                            String status = jObject.getString("status");
                            if (status.equals("success")) {
                                et_post.setText("");
                                getSpeakOutYourMindFeeds();

/*
                                final Dialog dialog = CommonDialogs.showRemoveMessageDialog(context, "Success", "Your thoughts are posted");
                                Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                                dialog_ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                        et_post.setText("");
                                        getSpeakOutYourMindFeeds();
                                    }
                                });
*/
                            } else if (status.equalsIgnoreCase("error")) {
                                CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Something went wrong...try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        //Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", AppSettings.getLoginUserId());
                map.put("feed_cat_id", cat_id);
                try {
                    map.put("post_content",
                            URLEncoder.encode(et_post.getText().toString(),
                                    HTTP.UTF_8));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                map.put("identity", identityFlag);
                System.out.println(map);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    private void saveSpeakUpYourMindComment(final String feed_id, final String comment, final int position) {
        progressDialog = new CustomProgressDialog(this, "Saving Comment...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
        Call<ResponseGetInterestSubcategory> loginCall = getProfileRequest.setSpeakOutFeedComment(AppSettings.getLoginUserId(), feed_id, comment);
        loginCall.enqueue(new Callback<ResponseGetInterestSubcategory>() {
            @Override
            public void onResponse(Call<ResponseGetInterestSubcategory> call, retrofit2.Response<ResponseGetInterestSubcategory> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {

                        speakUpYourMindListAdapter.addComment(position);
                        getSpeakOutYourMindComments(feed_id, position);

                    }
                } else {
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(SpeakOutYourMindActivity.this, "Something went wrong..", "Please contact OnGraviti Admin.");
                }
            }

            @Override
            public void onFailure(Call<ResponseGetInterestSubcategory> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });


    }

    private void saveSpeakUpYourMindLike(final String feed_id, final int position) {
        progressDialog = new CustomProgressDialog(this, "Please wait...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/set_speak_out_feed_like",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        System.out.println("save feed :Response " + response);
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);

                            String message = jObject.getString("msg");
                            String status = jObject.getString("status");
                            if (status.equals("success")) {
                                speakUpYourMindListAdapter.addLike(position);

                            } else if (status.equalsIgnoreCase("error")) {
                                CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        //Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", AppSettings.getLoginUserId());
                map.put("speak_ur_mind_id", feed_id);
                System.out.println(map);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    private void showOrbitFriendList() {

        OrbitFriend request = new OrbitFriend();
        request.setUserID("All");
        request.setUserName("Select");

        OrbitFriend request1 = new OrbitFriend();
        request1.setUserID("All");
        request1.setUserName("All");

        OrbitFriend request2 = new OrbitFriend();
        request2.setUserID(AppSettings.getLoginUserId());
        request2.setUserName("Me");

        orbitFriends = new ArrayList<OrbitFriend>();
        orbitFriends.add(request);
        orbitFriends.add(request1);
        orbitFriends.add(request2);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/get_user_orbit",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        System.out.println("get_user_orbit :Response " + response);
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);

                            // message = jObject.getString("msg");
                            String status = jObject.getString("status");
                            if (status.equals("success")) {
                                Object obj = jObject.get("orbit_list");
                                if (obj instanceof JSONArray) {
                                    JSONArray requestLists = jObject.getJSONArray("orbit_list");
                                    for (int i = 0; i < requestLists.length(); i++) {
                                        Log.d("requestLists count", "" + i);

                                        JSONObject detailsObject = requestLists.getJSONObject(i);
                                        OrbitFriend request = new OrbitFriend();
                                        request.setUserName(detailsObject.getString("orbit_name"));
                                        request.setUserID(detailsObject.getString("orbit_id"));
                                        request.setProfilePic(detailsObject.getString("profile_picture"));
                                        request.setCategory_type(detailsObject.getString("profile_cat_id"));
                                        orbitFriends.add(request);
                                    }

                                    if (requestLists.length() > 0) {
                                        ArrayAdapter<OrbitFriend> adapter = new ArrayAdapter<OrbitFriend>(context, android.R.layout.simple_spinner_item, orbitFriends);
                                        adapter.setDropDownViewResource(R.layout.spinner_item);
                                        spinner_posted_by.setAdapter(adapter); // this will set list of values to spinner
                                    }
                                } else {

                                }

                                progressDialog.dismiss();

                            } else if (status.equalsIgnoreCase("error")) {
                                //CommonDialogs.showMessageDialog(context, "Error", message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        //  Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", AppSettings.getLoginUserId());
                System.out.println(map);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }

    private void getSpeakOutYourMindComments(final String feed_id, final int position) {
        progressDialog = new CustomProgressDialog(this, "Loading Comments...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/get_speak_out_your_mind_feed_comments",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ArrayList<Comment> comments = new ArrayList<>();

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        System.out.println("get_comments :Response " + response);
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);

                            // message = jObject.getString("msg");
                            String status = jObject.getString("status");
                            if (status.equals("success")) {
                                Object obj = jObject.get("feed_comment_list");
                                if (obj instanceof JSONArray) {
                                    JSONArray requestLists = jObject.getJSONArray("feed_comment_list");
                                    Comment request = null;
                                    for (int i = 0; i < requestLists.length(); i++) {
                                        Log.d("commentList count", "" + i);

                                        JSONObject detailsObject = requestLists.getJSONObject(i);
                                        request = new Comment();

                                        request.setFeed_comment_id(detailsObject.getString("spk_comment_id"));
                                        request.setComment_by_user_id(detailsObject.getString("comment_by_user_id"));
                                        request.setFeed_comment_text(detailsObject.getString("feed_comment_text"));
                                        request.setFeed_comment_date(detailsObject.getString("feed_comment_date"));
                                        request.setFirst_name(detailsObject.getString("name"));
                                        request.setPicture(detailsObject.getString("profile_picture"));

                                        comments.add(request);
                                    }

                                } else {

                                }

                            } else if (status.equalsIgnoreCase("error")) {
                                //CommonDialogs.showMessageDialog(context, "Error", message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        showCommentDialog(feed_id, comments, position);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        //  Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                        showCommentDialog(feed_id, new ArrayList<Comment>(), position);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", AppSettings.getLoginUserId());
                map.put("speak_ur_mind_id", feed_id);
                System.out.println(map);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    private void showCommentDialog(final String feed_id, ArrayList<Comment> comments, final int position) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.speakout_your_mind_comment_dialog);


        ListView lv = (ListView) dialog.findViewById(R.id.lv_comments);
        SpeakUpYourMindCommentListAdapter speakUpYourMindCommentListAdapter = new SpeakUpYourMindCommentListAdapter(comments, SpeakOutYourMindActivity.this);
        lv.setAdapter(speakUpYourMindCommentListAdapter);

        final EditText et_comment = (EditText) dialog.findViewById(R.id.et_comment);

        Button btn_send = (Button) dialog.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_comment.getText() != null && !et_comment.getText().toString().isEmpty()) {
                    dialog.dismiss();
                    saveSpeakUpYourMindComment(feed_id, et_comment.getText().toString(), position);
                }
            }
        });

        dialog.setCancelable(true);
        dialog.setTitle("Comments");
        dialog.show();
    }

    @Override
    public void onRefresh() {
        getSpeakOutYourMindFeeds();
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.ibtn_emojis:
                if (!popup.isShowing()) {

                    //If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                        imageViewEmojis.setImageResource(R.drawable.ic_action_keyboard);
                        //  changeEmojiKeyboardIcon(imageViewEmojis, R.drawable.ic_action_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else {
                        et_post.setFocusableInTouchMode(true);
                        et_post.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(et_post, InputMethodManager.SHOW_IMPLICIT);
                        imageViewEmojis.setImageResource(R.drawable.ic_action_keyboard);
                        //changeEmojiKeyboardIcon(imageViewEmojis, R.drawable.ic_action_keyboard);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else {
                    popup.dismiss();
                }
                break;

        }
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }
}
