package com.exceptionaire.ongraviti.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.SearchableListDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.fcm.Config;
import com.exceptionaire.ongraviti.listener_interface.ClickListener;
import com.exceptionaire.ongraviti.model.Country;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.exceptionaire.ongraviti.utilities.CustomVolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 24/8/16.
 */
public class GetUserEmail extends BaseActivity implements Response.Listener, Response.ErrorListener {
    //activity for get user email id which is not get by facebook
    private static final String TAG = "GetUserEmail";
    private EditText email_id;
    private Button ok, cancle;
    private TextView email_txt_country_code;
    private LinearLayout linearEmail;
    String facebook_id, name, profile_picure, email, gender, status, user_id, message, profile_status, device_confirm_msg = "yes";
    private RequestQueue requestQueue;
    private CustomVolleyRequest customVolleyRequest;
    private ProgressDialog progressDialog;

    private Intent intent;

    private List<Country> countryList;
    private SearchableListDialog _searchableListDialog;
    private SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<ArrayList<Country>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_email_id);
        LOG_TAG = "GetUserEmail";

        init();
        new AsyncPhoneInitTask(this).execute();
        intent = getIntent();

        //get data from login activity for login with facebook webservice
        if (intent != null) {
            name = intent.getStringExtra("name");
            profile_picure = intent.getStringExtra("profile_pic_url");
            gender = intent.getStringExtra("gender");
            facebook_id = intent.getStringExtra("facebook_user_id");
            email = intent.getStringExtra("email");
        }
        if (email != null && email.trim().length() > 0) {
            linearEmail.setVisibility(View.GONE);
            email_id.setText(email);
        }
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get email id from edittext
                email = email_id.getText().toString().trim();
                if (validateForm(email)) {
                    LoginWithFacebook();
                }

            }
        });
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        email_txt_country_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _searchableListDialog = SearchableListDialog.newInstance(countryList, new ClickListener() {
                    @Override
                    public void onClick(Country country) {
                        email_txt_country_code.setText(String.valueOf(country.getCountryCode()));
                        //country_code = String.valueOf(country.getCountryCode());
                        //country_iso = country.getCountryISO();
                    }
                });
                _searchableListDialog.show((GetUserEmail.this).getFragmentManager(), "TAG");
            }
        });
    }

    private void init() {
        // find view by id all views
        email_id = (EditText) findViewById(R.id.emailIEditTextEmail);
        ok = (Button) findViewById(R.id.emailIDOKButton);
        cancle = (Button) findViewById(R.id.emailIDCancleButton);
        linearEmail = (LinearLayout) findViewById(R.id.linearEmail);
        email_txt_country_code = (TextView) findViewById(R.id.email_txt_country_code);
    }

    //validation for email id and mobile(edittexts)
    public boolean validateForm(String email) {
        email_id.setError(null);
        boolean valid = true;
        String EMAIL_PATTERN = AppConstants.REGEX_EMAIL;
        if (!email.matches(EMAIL_PATTERN)) {
            setErrorMsg(getResources().getString(R.string.valid_email), email_id, true);
            valid = false;
        }
        return valid;
    }

    private void LoginWithFacebook() {
        showProgressBar();
        Map<String, String> map = new HashMap<>();
        map.put("email", email);
        map.put("name", name);
        map.put("profile_pic_url", profile_picure);
//        map.put("contact_number", contact_number);
        map.put("facebook_user_id", facebook_id);
        map.put("device_token_id", "12345678");
        map.put("device_confirm_msg", device_confirm_msg);
        map.put("device_type", "android");
        requestQueue = Volley.newRequestQueue(GetUserEmail.this);
        customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.LOGIN_WITH_FACEBOOK, map, this, this);
        requestQueue.add(customVolleyRequest);
    }

    //hide keyboard after touch any where on screen
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                AppUtils.hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    // blank all edittext after successful login
    private void blankEdittext() {
        email_id.setText("");
    }

    // showing progress bar
    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        error.printStackTrace();
        String errorMessage = getString(R.string.error_connection_generic);
        if (error instanceof NoConnectionError)
            errorMessage = getString(R.string.error_no_connection);
        else if (error instanceof AuthFailureError)
            errorMessage = getString(R.string.error_authentication);
        else if (error instanceof NetworkError)
            errorMessage = getString(R.string.error_network_error);
        else if (error instanceof ParseError)
            errorMessage = getString(R.string.error_parse_error);
        else if (error instanceof ServerError)
            errorMessage = getString(R.string.error_server_error);
        else if (error instanceof TimeoutError)
            errorMessage = getString(R.string.error_connection_timeout);
    }

    @Override
    public void onResponse(Object response) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        JSONObject jObject = null;

        try {
            jObject = new JSONObject(String.valueOf(response));
            Log.e(TAG, response + " ");
            status = jObject.getString("status");
            message = jObject.getString("msg");
            if (status.equalsIgnoreCase("success")) {
                if (message.equalsIgnoreCase("You have successfully login.")) {
                    String user_email = jObject.getJSONObject("user").getString("email_address");
                    String full_contact_number = jObject.getJSONObject("user").getString("full_contact_number");
                    user_id = jObject.getJSONObject("user").getString("user_id");
                    //profile_status = jObject.getString("profile_status");
                    blankEdittext();
                    Log.e(LOG_TAG, "--- User Id : " + user_id);
                    AppSettings.setLoginUserId(user_id);
                    AppSettings.setUserProfileEmail(user_email);
                    AppSettings.setKeyUserProfileContactNumber(full_contact_number);
                    AppSettings.setLogin(true);
                    AppSettings.setFBUserId(user_email);
                    AppSettings.setFBLogin(true);
                    startActivity(new Intent(GetUserEmail.this, HomeUserPostsActivity.class));
                    finish();
                } else if (message.equalsIgnoreCase("Different mobile device exists.")) {
                    LoginWithFacebook();

                }


            } else if (status.equalsIgnoreCase("error")) {
                final Dialog dialog = CommonDialogs.showMessageDialog(GetUserEmail.this, getResources().getString(R.string.ongravity), message);
                dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        dialog.dismiss();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {

        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<Country>(233);
            BufferedReader reader = null;
            TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    final Country c = new Country(mContext, line, i);
                    data.add(c);
                    if (c.getCountryISO().toLowerCase().trim().equals(countryCodeValue.toLowerCase().trim())) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                email_txt_country_code.setText(String.valueOf(c.getCountryCode()));
                            }
                        });

                    }
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            countryList = data;
        }
    }

    // get IMEI number of device
    private String getDeviceID() {
        TelephonyManager tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        Log.e(TAG, countryCodeValue + " ");
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = telephonyManager.getDeviceId();
        Log.e(TAG, deviceId + " ");
        return deviceId;
    }
}
