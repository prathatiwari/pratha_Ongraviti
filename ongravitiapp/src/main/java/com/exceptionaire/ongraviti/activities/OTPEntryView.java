package com.exceptionaire.ongraviti.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppPreference;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.fcm.Config;
import com.exceptionaire.ongraviti.model.DataHolder;
import com.exceptionaire.ongraviti.model.ResponseResendOTP;
import com.exceptionaire.ongraviti.model.SpeedDatingRespose;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_activities.OpponentsActivityVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_service.CallServiceVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.SharedPrefsHelperVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.UsersUtilsVideo;
import com.exceptionaire.ongraviti.quickblox.QBData;
import com.exceptionaire.ongraviti.quickblox.core.utils.SharedPrefsHelper;
import com.exceptionaire.ongraviti.quickblox.services.CallService;
import com.exceptionaire.ongraviti.quickblox.utils.SharedPreferencesUtil;
import com.exceptionaire.ongraviti.quickblox.utils.chat.ChatHelper;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.exceptionaire.ongraviti.utilities.CustomVolleyRequest;
import com.exceptionaire.ongraviti.utilities.LogUtil;
import com.exceptionaire.ongraviti.utilities.ResourceUtil;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class OTPEntryView extends BaseActivity implements Response.ErrorListener, AppConstants, Response.Listener, View.OnClickListener, TextWatcher, View.OnKeyListener {

    private String userEntered, status, message;
    final int PIN_LENGTH = 4;
    boolean keyPadLockedFlag = false;
    private Context context;
    private EditText pinBox0, pinBox1, pinBox2, pinBox3;
    private TextView[] pinBoxArray;
    private Button submit_otp;

    private RequestQueue requestQueue;
    private CustomVolleyRequest customVolleyRequest;
    private ProgressDialog progressDialog;
    private Intent contactIntent;

    private String contact_num;
    private String login;
    private String password;
    private String email;
    private String phoneNumber;

    private String TAG = "OTPEntryView";
    private QBUser QuickBloxUser;
    private QBUser userForSave;
    private String mLogoImagePath = "";
    private AppPreference mPrefs;

    private TextView resend_otp, email_description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        userEntered = "";
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_otp);

        mPrefs = new AppPreference(this);

        email_description = (TextView) findViewById(R.id.email_description);

        contactIntent = getIntent();
        if (contactIntent != null && contactIntent.hasExtra("contact_number")) {
            contact_num = contactIntent.getStringExtra("contact_number");
        }
        if (contactIntent != null && contactIntent.hasExtra("login")) {
            login = contactIntent.getStringExtra("login");
        }
        if (contactIntent != null && contactIntent.hasExtra("password")) {
            password = contactIntent.getStringExtra("password");
        }
        if (contactIntent != null && contactIntent.hasExtra("email")) {
            email = contactIntent.getStringExtra("email");

            String messageToUser = "Please enter the OTP (One Time Password) \n sent to " + email;
            email_description.setText(messageToUser);

        }
        if (contactIntent != null && contactIntent.hasExtra("phoneNumber")) {
            phoneNumber = contactIntent.getStringExtra("phoneNumber");
        }

        pinBox0 = (EditText) findViewById(R.id.pinBox0);
        pinBox1 = (EditText) findViewById(R.id.pinBox1);
        pinBox2 = (EditText) findViewById(R.id.pinBox2);
        pinBox3 = (EditText) findViewById(R.id.pinBox3);

        pinBox0.addTextChangedListener(this);
        pinBox1.addTextChangedListener(this);
        pinBox2.addTextChangedListener(this);
        pinBox3.addTextChangedListener(this);

        pinBox0.setOnKeyListener(this);
        pinBox1.setOnKeyListener(this);
        pinBox2.setOnKeyListener(this);
        pinBox3.setOnKeyListener(this);

        submit_otp = (Button) findViewById(R.id.submit_otp);
        resend_otp = (TextView) findViewById(R.id.resend_otp);

        submit_otp.setOnClickListener(this);
        resend_otp.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        //App not allowed to go back to Parent activity until correct pin entered.
        return;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.activity_pin_entry_view, menu);
        return true;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        error.printStackTrace();
        String errorMessage = getString(R.string.error_connection_generic);

        if (error instanceof NoConnectionError)
            errorMessage = getString(R.string.error_no_connection);
        else if (error instanceof AuthFailureError)
            errorMessage = getString(R.string.error_authentication);
        else if (error instanceof NetworkError)
            errorMessage = getString(R.string.error_network_error);
        else if (error instanceof ParseError)
            errorMessage = getString(R.string.error_parse_error);
        else if (error instanceof ServerError)
            errorMessage = getString(R.string.error_server_error);
        else if (error instanceof TimeoutError)
            errorMessage = getString(R.string.error_connection_timeout);

        System.out.println(errorMessage);
    }

    @Override
    public void onResponse(Object response) {

        JSONObject jObject;
        try {
            jObject = new JSONObject(String.valueOf(response));

            if (jObject.has("status")) {
                status = jObject.getString("status");
                message = jObject.getString("msg");
                if (status.equals(RESPONSE_SUCCESS)) {
                    quickbloxSignUp();
                } else if (status.equalsIgnoreCase(RESPONSE_ERROR)) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    final Dialog dialog = CommonDialogs.showMessageDialog(OTPEntryView.this, getResources().getString(R.string.ongravity), message);
                    dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            return;
                        }
                    });
                }
            } else if (jObject.has("result")) {
                String result = jObject.getString("result");
                if (result.equals("1")) {
                    loginToChat(QuickBloxUser);
//                    loginToChatVideo(QuickBloxUser);
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            } else {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        } catch (JSONException e) {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.submit_otp:
                if (contact_num != null) {
                    if (isNetworkAvailable()) {
                        if (pinBox0.getText().length() != 0 && pinBox1.getText().length() != 0 && pinBox2.getText().length() != 0 && pinBox3.getText().length() != 0) {
                            showProgressBar();
                            //adding values in map
                            String otpUser = pinBox0.getText().toString() + pinBox1.getText().toString() + pinBox2.getText().toString() + pinBox3.getText().toString();
                            Map<String, String> map = new HashMap<>();
                            map.put("contact_number", contact_num);
                            map.put("otpPIN", otpUser);
                            requestQueue = Volley.newRequestQueue(OTPEntryView.this);
                            customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.COMPAIRE_OTP, map, OTPEntryView.this, OTPEntryView.this);
                            requestQueue.add(customVolleyRequest);

                        } else {
                            Toast.makeText(this, "Please Enter OTP", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        CommonDialogs.showMessageDialog(OTPEntryView.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                    }
                }
                break;

            case R.id.resend_otp:

                if (contact_num != null) {
                    if (isNetworkAvailable()) {

                        if (AppSettings.getLoginUserId() != null && !AppSettings.getLoginUserId().isEmpty())
                            resendOTP(AppSettings.getLoginUserId());
                        else {
                            Toast.makeText(this, "User not registered successfully!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        CommonDialogs.showMessageDialog(OTPEntryView.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                    }
                }
                break;
        }
    }

    private void resendOTP(String user_id) {

        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<ResponseResendOTP> eventResponse = apiInterface.resendOTP(user_id);
        eventResponse.enqueue(new Callback<ResponseResendOTP>() {
            @Override
            public void onResponse(Call<ResponseResendOTP> call, retrofit2.Response<ResponseResendOTP> response) {

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        Toast.makeText(OTPEntryView.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseResendOTP> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void afterTextChanged(Editable editable) {


        if (pinBox0.getText().length() == 1)
            pinBox1.requestFocus();
        if (pinBox1.getText().length() == 1)
            pinBox2.requestFocus();
        if (pinBox2.getText().length() == 1)
            pinBox3.requestFocus();

    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        switch (view.getId()) {
            case R.id.pinBox3:
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (pinBox3.getText().length() == 0)
                        pinBox2.requestFocus();
                }
                break;
            case R.id.pinBox2:
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (pinBox2.getText().length() == 0)
                        pinBox1.requestFocus();
                }
                break;
            case R.id.pinBox1:
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (pinBox1.getText().length() == 0)
                        pinBox0.requestFocus();
                }
                break;

        }
        return false;
    }


    private class LockKeyPadOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            for (int i = 0; i < 2; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
//                statusView.setText("");
            //Roll over
            pinBoxArray[0].setText("");
            pinBoxArray[1].setText("");
            pinBoxArray[2].setText("");
            pinBoxArray[3].setText("");

            userEntered = "";
            keyPadLockedFlag = false;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.please_wait));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }


    public void quickbloxSignUp() {

        QuickBloxUser = new QBUser();
        QuickBloxUser.setLogin(login);
        QuickBloxUser.setPassword(password);
        //TODO set user name not contact no
        QuickBloxUser.setFullName(login);
        QuickBloxUser.setPhone(phoneNumber);
        QuickBloxUser.setEmail(email);
//      QuickBloxUser.setWebsite(country);
        StringifyArrayList arrayList = new StringifyArrayList();
        arrayList.add(AppConstants.QB_USERS_TAG);
        QuickBloxUser.setTags(arrayList);
        QBUserSingUp(QuickBloxUser);
    }

    private void QBUserSingUp(final QBUser user) {
        LogUtil.writeDebugLog(TAG, "QBUserSingUp", "start");
        QBUsers.signUpSignInTask(user).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                LogUtil.writeDebugLog(TAG, "QBUserSingUp", "onSuccess");
                QuickBloxUser = qbUser;
                AppSettings.setQuickBloxUserID(qbUser.getId());
                user.setPassword(password);
                setQuickbloxIDToGravitiServer(String.valueOf(qbUser.getId()), String.valueOf(qbUser.getFullName()), password);
            }

            @Override
            public void onError(QBResponseException error) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                LogUtil.writeDebugLog(TAG, "QBUserSingUp", "onError");
                if (error != null && error.getErrors() != null && error.getErrors().get(0) != null)
                    new CommonDialogs().showMessageDialogNonStatic(OTPEntryView.this, getResources().getString(R.string.ongravity), error.getErrors().get(0));
                else if (error.getMessage() != null)
                    new CommonDialogs().showMessageDialogNonStatic(OTPEntryView.this, getResources().getString(R.string.ongravity), error.getMessage());
                else
                    new CommonDialogs().showMessageDialogNonStatic(OTPEntryView.this, getResources().getString(R.string.ongravity), "QBResponseException");
            }
        });
    }

    private void setQuickbloxIDToGravitiServer(String quickbloxID, String quickFullName, String quickbloxPassword) {
        try {
            Map<String, String> map = new HashMap<>();
            map.put("user_id", AppSettings.getLoginUserId());
            map.put("quickblox_id", quickbloxID);
            map.put("full_name", quickFullName);
            map.put("quickblox_password", quickbloxPassword);
            SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
            String regId = pref.getString("regId", null);
            if (regId != null)
                map.put("device_token_id", regId);
            else
                map.put("device_token_id", "");


//            String imei_number = getDeviceID();
//            if (imei_number != null)
//                map.put("imei_number", imei_number);
//            else
//                map.put("imei_number", "");

            map.put("device_type", "android");

            requestQueue = Volley.newRequestQueue(context);
            customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.SET_QUICKBLOX_ID, map, this, this);
            requestQueue.add(customVolleyRequest);
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void loginToChat(final QBUser qbUser) {
        LogUtil.writeDebugLog(TAG, "loginToChat", "start");
        qbUser.setPassword(password);
        userForSave = qbUser;
        startLoginService(qbUser);
        // startLoginServiceVideo(qbUser);

    }

    private void startLoginService(QBUser qbUser) {
        LogUtil.writeDebugLog(TAG, "startLoginService", "start");
        Intent tempIntent = new Intent(this, CallService.class);
        PendingIntent pendingIntent = createPendingResult(AppConstants.EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
        CallService.start(this, qbUser, pendingIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.REQ_PHOTO_FILE) {
                LogUtil.writeDebugLog(TAG, "onActivityResult", "onActivityResult from CameraActivity");
                mLogoImagePath = data.getStringExtra(AppConstants.EK_URL);
                File image = new File(mLogoImagePath);
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
                bitmap = Bitmap.createScaledBitmap(bitmap, 800, 800, true);
//                m_btnSelectAvatar.setImageBitmap(bitmap);
                ResourceUtil.setVideoExtension(FilenameUtils.getExtension(mLogoImagePath));
            }
        } else if (resultCode == AppConstants.EXTRA_LOGIN_RESULT_CODE) {
            LogUtil.writeDebugLog(TAG, "onActivityResult", "onActivityResult from callservice.");
            boolean isLoginSuccess = data.getBooleanExtra(AppConstants.EXTRA_LOGIN_RESULT, false);
            String errorMessage = data.getStringExtra(AppConstants.EXTRA_LOGIN_ERROR_MESSAGE);
            if (isLoginSuccess) {
                userForSave.setPassword(password);
                saveUserData(userForSave);
                saveUserDataVideo(userForSave);
//                QBUserSignIn(userForSave);
                startLoginServiceVideo(QuickBloxUser);
//                signInCreatedUser(userForSave, false);


                //Intent tempIntent1 = new Intent(this, CallServiceVideo.class);
//                PendingIntent pendingIntent1 = createPendingResult(EXTRA_LOGIN_RESULT_CODE_VIDEO, tempIntent1, 0);
                // CallServiceVideo.start(this, userForSave);

            } else {
                new CommonDialogs().showMessageDialogNonStatic(OTPEntryView.this, getResources().getString(R.string.ongravity), getString(R.string.login_chat_login_error) + errorMessage);
            }
        } else if (resultCode == EXTRA_LOGIN_RESULT_CODE_VIDEO) {
            QBUserSignIn(userForSave);
        }
    }

    private void startLoginServiceVideo(QBUser qbUser) {
        Intent tempIntent = new Intent(this, CallServiceVideo.class);
        PendingIntent pendingIntent = createPendingResult(EXTRA_LOGIN_RESULT_CODE_VIDEO, tempIntent, 0);
        CallServiceVideo.start(this, qbUser, pendingIntent);
    }

    private void QBUserSignIn(final QBUser user) {
        LogUtil.writeDebugLog(TAG, "QBUserSignIn", "start");
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                LogUtil.writeDebugLog(TAG, "QBUserSignIn", "onSuccess");
                DataHolder.getInstance().addQbUser(user);
                DataHolder.getInstance().setSignInQbUser(user);
                SharedPreferencesUtil.saveQbUser(user);
                QBData.curQBUser = user;
                setResult(RESULT_OK, new Intent());

                AppSettings.setLogin(true);
                Intent intent = new Intent(OTPEntryView.this, EditProfileDetailsActivity.class);
//                Intent intent = new Intent(OTPEntryView.this, OpponentsActivityVideo.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onError(QBResponseException e) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                LogUtil.writeDebugLog(TAG, "QBUserSignIn", "onError");
                new CommonDialogs().showMessageDialogNonStatic(OTPEntryView.this, getResources().getString(R.string.ongravity), e.getErrors().get(0));
            }
        });
    }


    /********************************************
     * Video Calling
     **********************************************/


    public QBUser createUserWithEnteredData() {
        return createQBUserWithCurrentData(String.valueOf("TestingTwo"),//user name
                String.valueOf("testingtwo@gmail.com"),// user email id
                String.valueOf("ongravity")); // group name
    }

    private QBUser createQBUserWithCurrentData(String userName, String email, String chatRoomName) {
        QBUser qbUser = null;
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(chatRoomName)) {
            StringifyArrayList<String> userTags = new StringifyArrayList<>();
            userTags.add(chatRoomName);

            qbUser = new QBUser();
            qbUser.setFullName(userName);
            qbUser.setEmail(email);
//            qbUser.setLogin(getCurrentDeviceId());
            qbUser.setPassword(DEFAULT_USER_PASSWORD);
            qbUser.setTags(userTags);
        }

        return qbUser;
    }


    public void startSignUpNewUser(final QBUser newUser) {
        requestExecutor.signUpNewUser(newUser, new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser result, Bundle params) {
                        loginToChatVideo(result);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (e.getHttpStatusCode() == ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                            signInCreatedUser(newUser, true);
                        } else {
                            Toast.makeText(OTPEntryView.this, R.string.sign_up_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );
    }

    private void loginToChatVideo(final QBUser qbUser) {
        qbUser.setPassword(DEFAULT_USER_PASSWORD);

        userForSave = qbUser;


        //startLoginServiceVideo(qbUser);
    }

    private void signInCreatedUser(final QBUser user, final boolean deleteCurrentUser) {
        requestExecutor.signInUser(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser result, Bundle params) {
                if (deleteCurrentUser) {
                    removeAllUserData(result);
//                    startOpponentsActivity();

                } else {


                    Log.i("Registered User info", user.toString());
                    Toast.makeText(OTPEntryView.this, user.toString(), Toast.LENGTH_SHORT).show();

                    // startOpponentsActivity();
                }
            }

            @Override
            public void onError(QBResponseException responseException) {
                Toast.makeText(OTPEntryView.this, R.string.sign_up_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void removeAllUserData(final QBUser user) {
        requestExecutor.deleteCurrentUser(user.getId(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                UsersUtilsVideo.removeUserData(getApplicationContext());
                startSignUpNewUser(createUserWithEnteredData());
            }

            @Override
            public void onError(QBResponseException e) {
                Toast.makeText(OTPEntryView.this, R.string.sign_up_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void startOpponentsActivity() {
        OpponentsActivityVideo.start(OTPEntryView.this, false);


        //  finish();
    }


    private void saveUserDataVideo(QBUser qbUser) {
        SharedPrefsHelperVideo sharedPrefsHelperVideo = SharedPrefsHelperVideo.getInstance();
        sharedPrefsHelperVideo.save(PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
        sharedPrefsHelperVideo.saveQbUser(qbUser);
    }


    /**************************************************/
// get IMEI number of device
    private String getDeviceID() {
        String deviceId = null;
        try {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();
            Log.e(TAG, countryCodeValue + " ");
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = telephonyManager.getDeviceId();
            Log.e(TAG, deviceId + " ");
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return deviceId;
    }
}
