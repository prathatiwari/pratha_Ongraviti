package com.exceptionaire.ongraviti.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.FavUserPostListAdapter;
import com.exceptionaire.ongraviti.activities.adapters.InterestListAdapter;
import com.exceptionaire.ongraviti.activities.adapters.PostsCommentsAdapter;
import com.exceptionaire.ongraviti.activities.adapters.SearchedUserPostListAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.myorbit.AddPeopleToOrbitActivity;
import com.exceptionaire.ongraviti.activities.myorbit.MyOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.imoji.EmoticonTextView;
import com.exceptionaire.ongraviti.model.Array_of_comment;
import com.exceptionaire.ongraviti.model.DataModel;
import com.exceptionaire.ongraviti.model.LikeUnlike;
import com.exceptionaire.ongraviti.model.ProfileInfo;
import com.exceptionaire.ongraviti.model.ResponseGetCommentsByPostID;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseSuccess;
import com.exceptionaire.ongraviti.model.ResponseUserPost;
import com.exceptionaire.ongraviti.model.Set_Comment;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.model.deletePostResponse;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.squareup.picasso.Picasso;

import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

public class SearchedUserInfoActivity extends BaseDrawerActivity {
    EditText et_comment;
    private Context context;
    private String TAG = "SearchedUserInfoActivity";
    private String fav_user_id;
    private CustomProgressDialog progressDialog;
    ArrayList dataModels;
    private TextView t_view_name, t_view_dob, t_view_gender, t_view_interested_in, t_view_relationship_status, t_view_religious_beliefs, t_view_political_beliefs, t_view_height, t_view_weight, t_view_body_type, t_view_eye_color, t_view_hair_color, t_view_smoking, t_view_drinking, t_view_gym, t_view_fav_book, t_view_fav_shows, t_view_fav_movies, t_view_contries_travelled, t_view_events_attended, t_view_hobbies, t_view_highest_degree, t_view_colg_school, t_view_degree, t_view_emp_status, t_view_future_goal, t_view_area_of_interest, t_view_what_i_read, t_view_venture_to_look_for, t_view_enterpreneursILik, t_view_events_attended_WIWB, t_view_events_looking_forward_to, t_view_industry_i_want_to_target;
    ImageView textview1, textview2, textview3, textview4, textview5;
    LinearLayout linear1, linear2, linear3, linear4, linear5;
    String user_id = "", name = "";
    ListView interest;
    String profileBio;
    public RoundedImageView roundedImageViewProfilePic;
    private RecyclerView recyclerViewPosts;
    String actualHeight = "";
    TextView t_view_name1;
    EmoticonTextView text_view_status;
    public static UserBasicInfo userBasicInfo;
    public static ProfileInfo profileInfo;
    public static ArrayList<ProfileInfo> list_profile_category;
    TextView text_view_nopost;
    ImageView social_backlground;
    String social, dating, business;
    TextView text_view_cat;
    private ScrollView scrollView;
    ImageView play_video_vio;
    private TextView header;
    String catId;
    SearchedUserPostListAdapter searchedUserPostListAdapter;
    ImageView imageview_add_friend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_searched_user_info, frameLayout);
        header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText("Profile");
        context = this;
        try {
            initializeViewComponents();
            if (getIntent() != null) {
                Log.d(TAG, "## getIntent()!=null");

                if (getIntent().getStringExtra("ActivityName").equals("OrbitProfile")) {
                    Log.d(TAG, "## OrbitUserID : " + getIntent().getStringExtra("OrbitUserID"));
                    fav_user_id = getIntent().getStringExtra("OrbitUserID");
                }
                if (getIntent().hasExtra("social"))
                    social = getIntent().getStringExtra("social");
                if (getIntent().hasExtra("dating"))
                    dating = getIntent().getStringExtra("dating");
                if (getIntent().hasExtra("business"))
                    business = getIntent().getStringExtra("business");

                if (isNetworkAvailable()) {
                    getAndDisplayUserInfo();
                } else {
                    Toast.makeText(context, getResources().getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                }

            }
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void initializeViewComponents() {
        Log.d(TAG, "## initializeViewComponents");
        try {
            recyclerViewPosts = (RecyclerView) findViewById(R.id.list_view_userpost);
            text_view_nopost = (TextView) findViewById(R.id.text_view_nopost);
            social_backlground = (ImageView) findViewById(R.id.social_backlground);
            play_video_vio = (ImageView) findViewById(R.id.play_video_vio);
            roundedImageViewProfilePic = (RoundedImageView) findViewById(R.id.imgProfilePicture);
            //textview display values
            imageview_add_friend = (ImageView) findViewById(R.id.imageview_add_friend);
            t_view_name = (TextView) findViewById(R.id.t_view_name);
            t_view_dob = (TextView) findViewById(R.id.t_view_dob);
            t_view_gender = (TextView) findViewById(R.id.t_view_gender);
            t_view_interested_in = (TextView) findViewById(R.id.t_view_interested_in);
            t_view_relationship_status = (TextView) findViewById(R.id.t_view_relationship_status);
            t_view_religious_beliefs = (TextView) findViewById(R.id.t_view_religious_beliefs);
            t_view_political_beliefs = (TextView) findViewById(R.id.t_view_political_beliefs);
            text_view_cat = (TextView) findViewById(R.id.text_view_cat);
            t_view_height = (TextView) findViewById(R.id.t_view_height);
            text_view_status = (EmoticonTextView) findViewById(R.id.text_view_status);
            t_view_name1 = (TextView) findViewById(R.id.t_view_name1);
            t_view_weight = (TextView) findViewById(R.id.t_view_weight);
            interest = (ListView) findViewById(R.id.interest);
            dataModels = new ArrayList();

            dataModels.add(new DataModel("Cricket", false));
            dataModels.add(new DataModel("Cooking", true));
            dataModels.add(new DataModel("Music", false));
            dataModels.add(new DataModel("Books", true));
            dataModels.add(new DataModel("News", false));

            InterestListAdapter adapter = new InterestListAdapter(dataModels, getApplicationContext());

            interest.setAdapter(adapter);
            t_view_body_type = (TextView) findViewById(R.id.t_view_body_type);
            t_view_eye_color = (TextView) findViewById(R.id.t_view_eye_color);
            t_view_hair_color = (TextView) findViewById(R.id.t_view_hair_color);
            t_view_smoking = (TextView) findViewById(R.id.t_view_smoking);
            t_view_drinking = (TextView) findViewById(R.id.t_view_drinking);
            t_view_gym = (TextView) findViewById(R.id.t_view_gym);
            t_view_fav_book = (TextView) findViewById(R.id.t_view_fav_book);
            t_view_fav_shows = (TextView) findViewById(R.id.t_view_fav_shows);
            t_view_fav_movies = (TextView) findViewById(R.id.t_view_fav_movies);
            t_view_contries_travelled = (TextView) findViewById(R.id.t_view_contries_travelled);
            t_view_events_attended = (TextView) findViewById(R.id.t_view_events_attended);
            t_view_hobbies = (TextView) findViewById(R.id.t_view_hobbies);

            t_view_highest_degree = (TextView) findViewById(R.id.t_view_highest_degree);
            t_view_colg_school = (TextView) findViewById(R.id.t_view_colg_school);

            t_view_degree = (TextView) findViewById(R.id.t_view_degree);
            t_view_emp_status = (TextView) findViewById(R.id.t_view_emp_status);
            t_view_future_goal = (TextView) findViewById(R.id.t_view_future_goal);

            t_view_area_of_interest = (TextView) findViewById(R.id.t_view_area_of_interest);
            t_view_what_i_read = (TextView) findViewById(R.id.t_view_what_i_read);
            t_view_venture_to_look_for = (TextView) findViewById(R.id.t_view_venture_to_look_for);
            t_view_enterpreneursILik = (TextView) findViewById(R.id.t_view_enterpreneursILik);
            t_view_events_attended_WIWB = (TextView) findViewById(R.id.t_view_events_attended_WIWB);
            t_view_events_looking_forward_to = (TextView) findViewById(R.id.t_view_events_looking_forward_to);
            t_view_industry_i_want_to_target = (TextView) findViewById(R.id.t_view_industry_i_want_to_target);


            scrollView = (ScrollView) findViewById(R.id.scrollView);

            textview1 = (ImageView) findViewById(R.id.text_view1);
            textview2 = (ImageView) findViewById(R.id.text_view2);
            textview3 = (ImageView) findViewById(R.id.text_view3);
            textview4 = (ImageView) findViewById(R.id.text_view4);
            textview5 = (ImageView) findViewById(R.id.text_view5);

            linear1 = (LinearLayout) findViewById(R.id.linear1);
            linear2 = (LinearLayout) findViewById(R.id.linear2);
            linear3 = (LinearLayout) findViewById(R.id.linear3);
            linear4 = (LinearLayout) findViewById(R.id.linear4);
            linear5 = (LinearLayout) findViewById(R.id.linear5);
            textview1.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceAsColor")
                @Override
                public void onClick(View view) {
                    textview1.setImageResource(R.drawable.who_am_i_select);
                    textview2.setImageResource(R.drawable.what_i_like_doing);
                    textview3.setImageResource(R.drawable.how_do_i_make_living);
                    textview4.setImageResource(R.drawable.what_i_want_to_be);
                    textview5.setImageResource(R.drawable.post);

                    linear1.setVisibility(View.VISIBLE);
                    linear2.setVisibility(View.GONE);
                    linear3.setVisibility(View.GONE);
                    linear4.setVisibility(View.GONE);
                    linear5.setVisibility(View.GONE);
                }
            });
            textview2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    textview2.setImageResource(R.drawable.what_i_like_doing_select);
                    textview1.setImageResource(R.drawable.who_am_i);
                    textview3.setImageResource(R.drawable.how_do_i_make_living);
                    textview4.setImageResource(R.drawable.what_i_want_to_be);
                    textview5.setImageResource(R.drawable.post);

                    linear1.setVisibility(View.GONE);
                    linear2.setVisibility(View.VISIBLE);
                    linear3.setVisibility(View.GONE);
                    linear4.setVisibility(View.GONE);
                    linear5.setVisibility(View.GONE);
                }
            });
            textview3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    textview3.setImageResource(R.drawable.how_do_i_make_living_select);
                    textview2.setImageResource(R.drawable.what_i_like_doing);
                    textview1.setImageResource(R.drawable.who_am_i);
                    textview4.setImageResource(R.drawable.what_i_want_to_be);
                    textview5.setImageResource(R.drawable.post);


                    linear1.setVisibility(View.GONE);
                    linear2.setVisibility(View.GONE);
                    linear3.setVisibility(View.VISIBLE);
                    linear4.setVisibility(View.GONE);
                    linear5.setVisibility(View.GONE);
                }
            });
            textview4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    textview4.setImageResource(R.drawable.what_i_want_to_be_select);
                    textview1.setImageResource(R.drawable.who_am_i);
                    textview2.setImageResource(R.drawable.what_i_like_doing);
                    textview3.setImageResource(R.drawable.how_do_i_make_living);
                    textview5.setImageResource(R.drawable.post);

                    linear1.setVisibility(View.GONE);
                    linear2.setVisibility(View.GONE);
                    linear3.setVisibility(View.GONE);
                    linear4.setVisibility(View.VISIBLE);
                    linear5.setVisibility(View.GONE);
                }
            });
            textview5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    textview3.setImageResource(R.drawable.how_do_i_make_living);
                    textview2.setImageResource(R.drawable.what_i_like_doing);
                    textview1.setImageResource(R.drawable.who_am_i);
                    textview4.setImageResource(R.drawable.what_i_want_to_be);
                    textview5.setImageResource(R.drawable.post_select);

                    linear1.setVisibility(View.GONE);
                    linear2.setVisibility(View.GONE);
                    linear3.setVisibility(View.GONE);
                    linear4.setVisibility(View.GONE);
                    linear5.setVisibility(View.VISIBLE);
                }
            });

            play_video_vio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (profileBio != null && !profileBio.equals("") &&
                            !profileBio.equals("http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/uploads/")) {
                        Intent intent1 = new Intent(SearchedUserInfoActivity.this, VideoBioActivity.class);
                        intent1.putExtra("videopath", profileBio);
                        startActivity(intent1);
                    } else {
                        CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Profile video not available.");
                    }

                }
            });

            imageview_add_friend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendFriendRequest(fav_user_id);
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void getAndDisplayUserInfo() {

        if (isNetworkAvailable()) {
            showProgressBar();

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseGetUserProfileData> retrofitCall = requestInterface.getUserData(fav_user_id, AppSettings.getLoginUserId());
            retrofitCall.enqueue(new Callback<ResponseGetUserProfileData>() {
                @Override
                public void onResponse(Call<ResponseGetUserProfileData> call, retrofit2.Response<ResponseGetUserProfileData> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (response != null && response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                            setUserInfo(response.body());
                        } else {

                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            //   new CommonDialogs().showMessageDialogNonStatic(SearchedUserInfoActivity.this, "OnGraviti", response.body().getMsg());
                        }
                    } else {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        AppSettings.setLogin(false);
                        new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                    }
                }

                @Override
                public void onFailure(Call<ResponseGetUserProfileData> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, getResources().getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
            //   CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, getResources().getString(R.string.check_internet));
        }
    }

    private void setUserInfo(ResponseGetUserProfileData responseGetUserProfileData) {
        Log.d(TAG, "## setDataInControls");
        try {
            if (responseGetUserProfileData != null)
                if (responseGetUserProfileData.getorbitStatus().equals("1")) {
                    imageview_add_friend.setVisibility(View.GONE);
                } else if (responseGetUserProfileData.getorbitStatus().equals("0")) {
                    imageview_add_friend.setVisibility(View.VISIBLE);
                }
            if (responseGetUserProfileData.getUser_basic_info().length > 0) {
                userBasicInfo = responseGetUserProfileData.getUser_basic_info()[0];
                if (responseGetUserProfileData.getProfile_info().length > 0) {
                    profileInfo = responseGetUserProfileData.getProfile_info()[0];


                    catId = profileInfo.getCategory_id();
                }
            }
            // Call post api
            getUserHomePostData(fav_user_id, catId);
            if (responseGetUserProfileData.getProfile_info().length > 0) {
                //set profile pic according to default category, and profile name in appSettings
                Log.d(TAG, "## userAllProfileData.getUser_id() :: " + userBasicInfo.getUser_id());


            }
            //set data in textviews to view data only

            if (!userBasicInfo.getName().equals("")) {
                t_view_name1.setText(userBasicInfo.getName());
            } else {
                t_view_name1.setText("NA");
            }
            if (!userBasicInfo.getName().equals(""))
                t_view_name.setText(userBasicInfo.getName());
            if (!userBasicInfo.getDate_of_birth().equals(""))
                t_view_dob.setText(userBasicInfo.getDate_of_birth());
            if (!userBasicInfo.getGender().equals("")) {
                if (userBasicInfo.getGender().equals("0")) {
                    t_view_gender.setText("Male");
                } else if (userBasicInfo.getGender().equals("1")) {
                    t_view_gender.setText("Female");
                } else if (userBasicInfo.getGender().equals("2")) {
                    t_view_gender.setText("Bisexual");
                }
            }
            if (!userBasicInfo.getInterested_in().equals("")) {
                if (userBasicInfo.getInterested_in().equals("0"))
                    t_view_interested_in.setText("Male");
                else if (userBasicInfo.getInterested_in().equals("1"))
                    t_view_interested_in.setText("Female");
                else if (userBasicInfo.getInterested_in().equals("2"))
                    t_view_interested_in.setText("Bisexual");
            }

            if (!userBasicInfo.getRelationship_status().equals(""))
                t_view_relationship_status.setText(userBasicInfo.getRelationship_status());
            if (!userBasicInfo.getBeliefs_religious_beliefs().equals(""))
                t_view_religious_beliefs.setText(userBasicInfo.getBeliefs_religious_beliefs());
            if (!userBasicInfo.getPolitical_beliefs().equals(""))
                t_view_political_beliefs.setText(userBasicInfo.getPolitical_beliefs());
            if (!actualHeight.equals("")) {
                t_view_height.setText(actualHeight + " cms");
            } else {
                if (!userBasicInfo.getHeight().equals("")) {
                    String[] arr;
                    String initialHeight = userBasicInfo.getHeight().replace(".", "'");
                    arr = initialHeight.split("\\(");
                    actualHeight = arr[0] + "\"(" + arr[1]; //+ "\"("
                    actualHeight = actualHeight.replaceAll("^(.*)'(.*)$", "$1.$2");
                    Log.d(TAG, "## actualHeight : " + actualHeight);
                    t_view_height.setText(actualHeight + " cms");
                }
            }
            if (!userBasicInfo.getWeight().equals(""))
                t_view_weight.setText(userBasicInfo.getWeight() + " kg");
            if (!userBasicInfo.getBody_type().equals(""))
                t_view_body_type.setText(userBasicInfo.getBody_type());
            if (!userBasicInfo.getEye_color().equals(""))
                t_view_eye_color.setText(userBasicInfo.getEye_color());
            if (!userBasicInfo.getHair_color().equals(""))
                t_view_hair_color.setText(userBasicInfo.getHair_color());
            if (!userBasicInfo.getSmoking().equals(""))
                t_view_smoking.setText(userBasicInfo.getSmoking());
            if (!userBasicInfo.getDrinking().equals(""))
                t_view_drinking.setText(userBasicInfo.getDrinking());
            if (!userBasicInfo.getGym().equals(""))
                t_view_gym.setText(userBasicInfo.getGym());
            if (!userBasicInfo.getFavorite_book().equals(""))
                t_view_fav_book.setText(userBasicInfo.getFavorite_book());
            if (!userBasicInfo.getFavorite_shows().equals(""))
                t_view_fav_shows.setText(userBasicInfo.getFavorite_shows());
            if (!userBasicInfo.getFavorite_movies().equals(""))
                t_view_fav_movies.setText(userBasicInfo.getFavorite_movies());
            if (!userBasicInfo.getEvents_attended().equals(""))
                t_view_events_attended.setText(userBasicInfo.getEvents_attended());
            Log.d(TAG, "## contries : " + userBasicInfo.getCountries_travelled());
            if (!userBasicInfo.getCountries_travelled().equals(""))
                t_view_contries_travelled.setText(userBasicInfo.getCountries_travelled());
            if (!userBasicInfo.getHobbies().equals(""))
                t_view_hobbies.setText(userBasicInfo.getHobbies());
            if (!userBasicInfo.getHighest_degree().equals(""))
                t_view_highest_degree.setText(userBasicInfo.getHighest_degree());

            if (!userBasicInfo.getCollege_university_name().equals(""))
                t_view_colg_school.setText(userBasicInfo.getCollege_university_name());
            if (!userBasicInfo.getDegree_name().equals(""))
                t_view_degree.setText(userBasicInfo.getDegree_name());
            if ((!userBasicInfo.getEmployment_status().equals("")))
                t_view_emp_status.setText(userBasicInfo.getEmployment_status());
            if (!userBasicInfo.getFuture_goal().equals(""))
                t_view_future_goal.setText(userBasicInfo.getFuture_goal());

            if (!userBasicInfo.getArea_of_interest().equals(""))
                t_view_area_of_interest.setText(userBasicInfo.getArea_of_interest());
            if (!userBasicInfo.getwhat_i_read().equals(""))
                t_view_what_i_read.setText(userBasicInfo.getwhat_i_read());
            if (!userBasicInfo.getInteresting_ventures_to_look().equals(""))
                t_view_venture_to_look_for.setText(userBasicInfo.getInteresting_ventures_to_look());
            if (!userBasicInfo.getEnterpreneurs_i_like().equals(""))
                t_view_enterpreneursILik.setText(userBasicInfo.getEnterpreneurs_i_like());
            if (!userBasicInfo.getEvents_attended().equals(""))
                t_view_events_attended_WIWB.setText(userBasicInfo.getEvents_attended());
            if (!userBasicInfo.getEvents_m_looking().equals(""))
                t_view_events_looking_forward_to.setText(userBasicInfo.getEvents_m_looking());
            if (!userBasicInfo.getIndustries_i_want_to_target().equals(""))
                t_view_industry_i_want_to_target.setText(userBasicInfo.getIndustries_i_want_to_target());

            t_view_name.setVisibility(View.VISIBLE);
            t_view_dob.setVisibility(View.VISIBLE);
            t_view_gender.setVisibility(View.VISIBLE);
            t_view_interested_in.setVisibility(View.VISIBLE);
            t_view_relationship_status.setVisibility(View.VISIBLE);
            t_view_religious_beliefs.setVisibility(View.VISIBLE);
            t_view_political_beliefs.setVisibility(View.VISIBLE);
            t_view_height.setVisibility(View.VISIBLE);
            t_view_weight.setVisibility(View.VISIBLE);
            t_view_body_type.setVisibility(View.VISIBLE);
            t_view_eye_color.setVisibility(View.VISIBLE);
            t_view_hair_color.setVisibility(View.VISIBLE);
            t_view_smoking.setVisibility(View.VISIBLE);
            t_view_drinking.setVisibility(View.VISIBLE);
            t_view_gym.setVisibility(View.VISIBLE);

            t_view_fav_book.setVisibility(View.VISIBLE);
            t_view_fav_shows.setVisibility(View.VISIBLE);
            t_view_fav_movies.setVisibility(View.VISIBLE);
            t_view_contries_travelled.setVisibility(View.VISIBLE);
            t_view_events_attended.setVisibility(View.VISIBLE);
            t_view_hobbies.setVisibility(View.VISIBLE);
            t_view_highest_degree.setVisibility(View.VISIBLE);

            t_view_colg_school.setVisibility(View.VISIBLE);
            t_view_degree.setVisibility(View.VISIBLE);
            t_view_emp_status.setVisibility(View.VISIBLE);
            t_view_future_goal.setVisibility(View.VISIBLE);

            t_view_area_of_interest.setVisibility(View.VISIBLE);
            t_view_what_i_read.setVisibility(View.VISIBLE);
            t_view_venture_to_look_for.setVisibility(View.VISIBLE);
            t_view_enterpreneursILik.setVisibility(View.VISIBLE);
            t_view_events_attended_WIWB.setVisibility(View.VISIBLE);
            t_view_events_looking_forward_to.setVisibility(View.VISIBLE);
            t_view_industry_i_want_to_target.setVisibility(View.VISIBLE);


            if (responseGetUserProfileData.getProfile_info().length > 0) {
                String socialId = null, datingId = null, businessId = null;
                String ActiveCatId = responseGetUserProfileData.getUser_basic_info()[0].getProfile_cat_id();
                if (responseGetUserProfileData.getProfile_info().length > 0) {
                    socialId = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
                }
                if (responseGetUserProfileData.getProfile_info().length > 1) {
                    datingId = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
                }
                if (responseGetUserProfileData.getProfile_info().length > 2) {
                    businessId = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
                }// set all control data from json object
                if (ActiveCatId.equals(socialId)) {
                    if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != "")
                        Log.d(TAG, "## AppConstants.PROFILE_PICTURE_PATH / userBasicInfo.getList_profileData().get(0).getProfile_picture() : " + AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                    String path = AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture();
//                    Picasso.with(this)
//                            .load(path)
//                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                            .into(roundedImageViewProfilePic);
                    if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[0].getProfile_picture().isEmpty()) {
                        if (!responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                            Picasso.with(context)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    if (path != null) {
                                        new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, path);
                                    } else {
                                        CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Profile picture not available.");
                                    }
                                }
                            });
                        } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                            Picasso.with(context)
                                    .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null) {
                                        new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, path);
                                    } else {
                                        CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Profile picture not available.");
                                    }
                                }
                            });
                        }
                    }


                    Picasso.with(this)
                            .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture())
                            .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(social_backlground);
                    social_backlground.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //open next activity and play video in landscape mode
                            if (AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture() != null) {
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                            } else {
                                CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Cover picture not available.");
                            }
                        }
                    });
                    if (!responseGetUserProfileData.getProfile_info()[0].getProfileStatus().isEmpty()) {
                        text_view_status.setText(URLDecoder.decode(
                                responseGetUserProfileData.getProfile_info()[0].getProfileStatus(), "UTF-8"));
                    } else {
                        text_view_status.setText("Status not available.");
                    }
                    profileBio = AppConstants.PROFILE_BIO_PATH +
                            responseGetUserProfileData.getProfile_info()[0].getProfile_bio();
                } else if (ActiveCatId.equals(datingId)) {
                    if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != "")
                        Log.d(TAG, "## AppConstants.PROFILE_PICTURE_PATH / userBasicInfo.getList_profileData().get(1).getProfile_picture() : " + AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                    String path = AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture();
//                    Picasso.with(this)
//                            .load(path)
//                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                            .into(roundedImageViewProfilePic);

                    if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[1].getProfile_picture().isEmpty()) {
                        if (!responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(context)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    if (path != null) {
                                        new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, path);
                                    } else {
                                        CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Profile picture not available.");
                                    }
                                }
                            });
                        } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(context)
                                    .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null) {
                                        new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                    } else {
                                        CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Profile picture not available.");
                                    }
                                }
                            });
                        }
                    }


                    Picasso.with(this)
                            .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture())
                            .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(social_backlground);
                    social_backlground.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //open next activity and play video in landscape mode
                            if (AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture() != null) {
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                            } else {
                                CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Cover picture not available.");
                            }
                        }
                    });
                    if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().isEmpty()) {
                        text_view_status.setText(URLDecoder.decode(
                                responseGetUserProfileData.getProfile_info()[1].getProfileStatus(), "UTF-8"));
                    } else {
                        text_view_status.setText("Status not available.");
                    }
                    profileBio = AppConstants.PROFILE_BIO_PATH +
                            responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
                } else if (ActiveCatId.equals(businessId)) {
                    if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != "")
                        Log.d(TAG, "## AppConstants.PROFILE_PICTURE_PATH / userBasicInfo.getList_profileData().get(1).getProfile_picture() : " + AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                    String path = AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture();
//                    Picasso.with(this)
//                            .load(path)
//                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                            .into(roundedImageViewProfilePic);

                    if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[2].getProfile_picture().isEmpty()) {
                        if (!responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(context)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    if (path != null) {
                                        new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, path);
                                    } else {
                                        CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Profile picture not available.");
                                    }
                                }
                            });
                        } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(context)
                                    .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null) {
                                        new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                    } else {
                                        CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Profile picture not available.");
                                    }
                                }
                            });
                        }
                    }


                    Picasso.with(this)
                            .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture())
                            .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(social_backlground);
                    social_backlground.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //open next activity and play video in landscape mode
                            if (AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture() != null) {
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                            } else {
                                CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, "Cover picture not available.");
                            }
                        }
                    });
                    if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().isEmpty()) {
                        text_view_status.setText(URLDecoder.decode(
                                responseGetUserProfileData.getProfile_info()[2].getProfileStatus(), "UTF-8"));
                    } else {
                        text_view_status.setText("Status not available.");
                    }
                    profileBio = AppConstants.PROFILE_BIO_PATH +
                            responseGetUserProfileData.getProfile_info()[2].getProfile_bio();
                }
                list_profile_category = new ArrayList<>(Arrays.asList(responseGetUserProfileData.getProfile_info()));
                Log.d(TAG, "## list_profile_category : " + list_profile_category.size());

                if (social != null && social.equals("social")) {
                    String socialCatId = null;
                    String socialCatId1 = null;
                    String socialCatId2 = null;
                    String idSocial = "1";
                    if (responseGetUserProfileData.getProfile_info().length > 0) {
                        socialCatId = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 1) {
                        socialCatId1 = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 2) {
                        socialCatId2 = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
                    }
                    if (idSocial.equals(socialCatId)) {
                        //index 0
//                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null) {
//                            Picasso.with(this)
//                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
//                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                    .into(roundedImageViewProfilePic);

                            if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[0].getProfile_picture().isEmpty()) {
                                if (!responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                        }
                                    });
                                } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                        }
                                    });
                                }
                            }


                        }

                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture())
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);

                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[0].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[0].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Please update your status.");
                        }
                        profileBio = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[0].getProfile_bio();
                    } else if (idSocial.equals(socialCatId1)) {
                        //index 1
//                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null) {
//                            Picasso.with(this)
//                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
//                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                    .into(roundedImageViewProfilePic);

                            if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[1].getProfile_picture().isEmpty()) {
                                if (!responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                        }
                                    });
                                } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                        }
                                    });
                                }
                            }


                        }

                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture())
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);

                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[1].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Please update your status.");
                        }
                        profileBio = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
                    } else if (idSocial.equals(socialCatId2)) {
                        //index 2
//                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null) {
//                            Picasso.with(this)
//                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
//                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                    .into(roundedImageViewProfilePic);


                            if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[2].getProfile_picture().isEmpty()) {
                                if (!responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                        }
                                    });
                                } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                        }
                                    });
                                }
                            }

                        }
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture())
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);

                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[2].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Please update your status.");
                        }
                        profileBio = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[2].getProfile_bio();
                    }


                } else if (dating != null && dating.equals("dating")) {

                    String idSocial = "2";
                    String socialCatId = null;
                    String socialCatId1 = null;
                    String socialCatId2 = null;
                    if (responseGetUserProfileData.getProfile_info().length > 0) {
                        socialCatId = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 1) {
                        socialCatId1 = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 2) {
                        socialCatId2 = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
                    }
                    if (idSocial.equals(socialCatId)) {
                        //index 0
//                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null) {
//                            Picasso.with(this)
//                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
//                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                    .into(roundedImageViewProfilePic);

                            if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[0].getProfile_picture().isEmpty()) {
                                if (!responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                        }
                                    });
                                } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                        }
                                    });
                                }
                            }


                        }

                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture())
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);

                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[0].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[0].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Please update your status.");
                        }
                        profileBio = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[0].getProfile_bio();
                    } else if (idSocial.equals(socialCatId1)) {
                        //index 1
//                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null) {
//                            Picasso.with(this)
//                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
//                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                    .into(roundedImageViewProfilePic);

                            if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[1].getProfile_picture().isEmpty()) {
                                if (!responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                        }
                                    });
                                } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                        }
                                    });
                                }
                            }


                        }

                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture())
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);

                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[1].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Please update your status.");
                        }
                        profileBio = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
                    } else if (idSocial.equals(socialCatId2)) {
                        //index 2
//                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null) {
//                            Picasso.with(this)
//                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
//                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                    .into(roundedImageViewProfilePic);

                            if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[2].getProfile_picture().isEmpty()) {
                                if (!responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                        }
                                    });
                                } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                        }
                                    });
                                }
                            }


                        }
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture())
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);

                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[2].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Please update your status.");
                        }
                        profileBio = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[2].getProfile_bio();
                    }
//                    AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
//
//                    if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null)
//                        Picasso.with(this)
//                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
//                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                .into(roundedImageViewProfilePic);
//
//                    Picasso.with(this)
//                            .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture())
//                            .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
//                            .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                            .into(social_backlground);
//                    if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().equals("")) {
//                        text_view_status.setText(profileInfo.getProfileStatus());
//                    } else {
//                        text_view_status.setText("Status not available.");
//                    }
//                    text_view_cat.setText("Dating");

                } else if (business != null && business.equals("business")) {

                    String idSocial = "3";
                    String socialCatId = null;
                    String socialCatId1 = null;
                    String socialCatId2 = null;
                    if (responseGetUserProfileData.getProfile_info().length > 0) {
                        socialCatId = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 1) {
                        socialCatId1 = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 2) {
                        socialCatId2 = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
                    }
                    if (idSocial.equals(socialCatId)) {
                        //index 0
//                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null) {
//                            Picasso.with(this)
//                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
//                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                    .into(roundedImageViewProfilePic);

                            if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[0].getProfile_picture().isEmpty()) {
                                if (!responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                        }
                                    });
                                } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                        }
                                    });
                                }
                            }


                        }

                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture())
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);

                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[0].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[0].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Please update your status.");
                        }
                        profileBio = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[0].getProfile_bio();
                    } else if (idSocial.equals(socialCatId1)) {
                        //index 1
//                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null) {
//                            Picasso.with(this)
//                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
//                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                    .into(roundedImageViewProfilePic);

                            if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[1].getProfile_picture().isEmpty()) {
                                if (!responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                        }
                                    });
                                } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                        }
                                    });
                                }
                            }

                        }

                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture())
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);

                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[1].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Please update your status.");
                        }
                        profileBio = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
                    } else if (idSocial.equals(socialCatId2)) {
                        //index 2
//                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                        if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null) {
//                            Picasso.with(this)
//                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
//                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                    .into(roundedImageViewProfilePic);
                            if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[2].getProfile_picture().isEmpty()) {
                                if (!responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                        }
                                    });
                                } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                                    Picasso.with(context)
                                            .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                            .into(roundedImageViewProfilePic);
                                    roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //open next activity and play video in landscape mode
                                            new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                        }
                                    });
                                }
                            }


                        }
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture())
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);

                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(SearchedUserInfoActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[2].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Please update your status.");
                        }
                        profileBio = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[2].getProfile_bio();
                    }
//                    AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
//
//                    if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null)
//                        Picasso.with(this)
//                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
//                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                                .into(roundedImageViewProfilePic);
//
//                    Picasso.with(this)
//                            .load(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture())
//                            .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
//                            .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                            .into(social_backlground);
//                    if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().equals("")) {
//                        text_view_status.setText(profileInfo.getProfileStatus());
//                    } else {
//                        text_view_status.setText("Status not available.");
//                    }
//                    text_view_cat.setText("Business");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    private void getUserHomePostData(String userId, String profileCatId) {


        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseUserPost> userPostsCall = loginRequest.get_only_user_posts(userId, profileCatId);
        userPostsCall.enqueue(new Callback<ResponseUserPost>() {
            @Override
            public void onResponse(Call<ResponseUserPost> call, retrofit2.Response<ResponseUserPost> response) {
                setData(response);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseUserPost> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });


    }

    public void setData(Response<ResponseUserPost> response) {
        ResponseUserPost responseFaqs = response.body();
        if (response.body() != null && response.body().getStatus().equals("success")) {

            responseFaqs.setPostinfo(responseFaqs.getPostinfo());
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            if (responseFaqs.getPostinfo() != null) {
                searchedUserPostListAdapter = new SearchedUserPostListAdapter(getActivity(), responseFaqs.getPostinfo());
                recyclerViewPosts.setAdapter(searchedUserPostListAdapter);
                recyclerViewPosts.setLayoutManager(mLayoutManager);
            } else {
                text_view_nopost.setVisibility(View.VISIBLE);
                text_view_nopost.setText("Post not found.");
//                swipe_refresh_layout.setVisibility(View.GONE);
            }

        } else if (response.body().getStatus().equals("error")) {
            text_view_nopost.setVisibility(View.VISIBLE);
            text_view_nopost.setText("Post not found.");
//            swipe_refresh_layout.setVisibility(View.GONE);
        }
    }

    private void setPostsComments(final String post_id, final String comment, String device_date) {
        if (isNetworkAvailable()) {
//            progressDialog = new CustomProgressDialog(this, "Saving Comment...");
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            progressDialog.show();

            try {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface commentRequest = retrofit.create(ApiInterface.class);
                Call<Set_Comment> userPostsCall = commentRequest.setCommentPosts(AppSettings.getLoginUserId(), post_id, comment, device_date);
                userPostsCall.enqueue(new Callback<Set_Comment>() {

                    @Override
                    public void onResponse(Call<Set_Comment> call, retrofit2.Response<Set_Comment> response) {

                        Log.d(TAG, "## setPostsComments :" + response.body().toString());
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            getPostComments(post_id, "0");
                        } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
//                            if (progressDialog != null && progressDialog.isShowing())
//                                progressDialog.dismiss();
                            CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Something went wrong...try again");
                        }
                    }

                    @Override
                    public void onFailure(Call<Set_Comment> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } catch (Exception e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
            }
        } else {
            CommonDialogs.showMessageDialog(SearchedUserInfoActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    private Dialog commetsDialog = null;

    public void showCommentDialog(ArrayList<Array_of_comment> list_post_comment, String postID) {

        if (list_post_comment == null)
            list_post_comment = new ArrayList<Array_of_comment>();

        if (commetsDialog == null) {
            commetsDialog = new Dialog(context);
            commetsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            commetsDialog.setContentView(R.layout.speakout_your_mind_comment_dialog);
            commetsDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            ListView lv = (ListView) commetsDialog.findViewById(R.id.lv_comments);
            et_comment = (EditText) commetsDialog.findViewById(R.id.et_comment);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());
            PostsCommentsAdapter postsCommentsAdapter = new PostsCommentsAdapter(list_post_comment, this);
            lv.setAdapter(postsCommentsAdapter);
            Button btn_send = (Button) commetsDialog.findViewById(R.id.btn_send);
            btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateForm(et_comment.getText().toString().trim())) {
                        try {
                            setPostsComments(postID, URLEncoder.encode(et_comment.getText().toString(), HTTP.UTF_8), currentDateandTime);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        et_comment.setText("");
//                        btn_send.setBackgroundColor(R.color.list_divider);
                    }
                }
            });

            commetsDialog.setCancelable(true);
            commetsDialog.setTitle("Comments");
            commetsDialog.show();
            commetsDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    commetsDialog.dismiss();
                    commetsDialog = null;
                }
            });
        } else {
            ListView lv = (ListView) commetsDialog.findViewById(R.id.lv_comments);
            PostsCommentsAdapter postsCommentsAdapter = new PostsCommentsAdapter(list_post_comment, this);
            lv.setAdapter(postsCommentsAdapter);

            ((EditText) commetsDialog.findViewById(R.id.et_comment)).setText("");
        }

        View view = commetsDialog.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //validation for email id and password(edittexts)
    public boolean validateForm(String comment) {
        boolean valid = true;
        try {
            et_comment.setError(null);
            if (comment.length() == 0) {
                setErrorMsg("Please enter Comment.", et_comment, true);
                valid = false;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return valid;
    }


    public void getPostComments(final String post_id, final String pageIndex) {
        if (isNetworkAvailable()) {
//            if (progressDialog == null || !progressDialog.isShowing()) {
//                progressDialog = new CustomProgressDialog(this, "Loading old comments...");
//                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                progressDialog.show();
//            }

            try {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface commentRequest = retrofit.create(ApiInterface.class);
                Call<ResponseGetCommentsByPostID> userPostsCall = commentRequest.getCommentsByPostID(post_id, pageIndex);
                userPostsCall.enqueue(new Callback<ResponseGetCommentsByPostID>() {

                    @Override
                    public void onResponse(Call<ResponseGetCommentsByPostID> call, retrofit2.Response<ResponseGetCommentsByPostID> response) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                        Log.d(TAG, "## setPostsComments :" + response.body().toString());

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            showCommentDialog(response.body().getComment_list(), post_id);
                            getUserHomePostData(fav_user_id, catId);
                        } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                            if (response.body().getComment_list() != null)
                                CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Something went wrong...try again");
                            else
                                showCommentDialog(null, post_id);

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetCommentsByPostID> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } catch (Exception e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
            }
        } else {
            CommonDialogs.showMessageDialog(SearchedUserInfoActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    public void saveUserPostLike(final String post_id, final int position, final SearchedUserPostListAdapter.ViewHolder holder) {
        if (isNetworkAvailable()) {
//            progressDialog = new CustomProgressDialog(this, "Please wait...");
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            progressDialog.show();
            Log.d(TAG, "## post_id : " + post_id);

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface loginRequest = retrofit.create(ApiInterface.class);
            Call<LikeUnlike> userPostsCall = loginRequest.setLikeUnlike(AppSettings.getLoginUserId(), post_id);
            userPostsCall.enqueue(new Callback<LikeUnlike>() {
                @Override
                public void onResponse(Call<LikeUnlike> call, retrofit2.Response<LikeUnlike> response) {
//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();

                    Log.d(TAG, "## :" + response.body().toString());
                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        searchedUserPostListAdapter.addLike(position, holder, response.body().getLike_count(), response.body().getMsg());
                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), response.body().getMsg());
                    }
                }

                @Override
                public void onFailure(Call<LikeUnlike> call, Throwable t) {

//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();
                }
            });

        } else {
            CommonDialogs.showMessageDialog(SearchedUserInfoActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            finish();
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void reportAbusePost(String postId, String postUserId, String userID) {
        showProgressBar();

        try {
            if (isNetworkAvailable()) {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface loginRequest = retrofit.create(ApiInterface.class);
                Call<deletePostResponse> userPostsCall = loginRequest.reportAbusePost(userID, postId, postUserId);
                userPostsCall.enqueue(new Callback<deletePostResponse>() {
                    @Override
                    public void onResponse(Call<deletePostResponse> call, Response<deletePostResponse> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        Log.d(TAG, "## " + response.body().toString());
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, response.body().getMsg());
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, response.body().getMsg());
                        }
                    }

                    @Override
                    public void onFailure(Call<deletePostResponse> call, Throwable t) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                CommonDialogs.dialog_with_one_btn_without_title(SearchedUserInfoActivity.this, getResources().getString(R.string.check_internet));
            }
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void sendFriendRequest(final String receiver_user_id) {
        progressDialog.setMessage(SearchedUserInfoActivity.this.getString(R.string.msg_sending_friend_request));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = format.format(new Date());
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<ResponseSuccess> loginCall = apiInterface.sendOrbitRequest(AppSettings.getLoginUserId(), receiver_user_id, currentDateandTime);
        loginCall.enqueue(new Callback<ResponseSuccess>() {
            @Override
            public void onResponse(Call<ResponseSuccess> call, retrofit2.Response<ResponseSuccess> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {//success
                    final Dialog dialog = CommonDialogs.showMessageDialog(SearchedUserInfoActivity.this, getResources().getString(R.string.ongraviti), response.body().getMsg());
                    Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                    dialog_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            imageview_add_friend.setVisibility(View.GONE);
                        }
                    });

                } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                    CommonDialogs.showMessageDialog(SearchedUserInfoActivity.this, getResources().getString(R.string.ongraviti), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Toast.makeText(SearchedUserInfoActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}