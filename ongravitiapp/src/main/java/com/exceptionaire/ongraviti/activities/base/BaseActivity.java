package com.exceptionaire.ongraviti.activities.base;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.exceptionaire.ongraviti.core.AppDetails;
import com.exceptionaire.ongraviti.core.OnGravitiApp;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.QBResRequestExecutorVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.SharedPrefsHelperVideo;
import com.exceptionaire.ongraviti.quickblox.core.utils.SharedPrefsHelper;
import com.exceptionaire.ongraviti.quickblox.utils.QBResRequestExecutor;
import com.quickblox.users.model.QBUser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class BaseActivity extends AppCompatActivity {

    public static int WSCount = 2;
    public String LOG_TAG = "OnGraviti";
    private static final String TAG = BaseActivity.class.getSimpleName();

    protected QBResRequestExecutor requestExecutor;

    public SharedPrefsHelperVideo sharedPrefsHelper;
    protected QBResRequestExecutorVideo requestExecutorVideo;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestExecutor = OnGravitiApp.getInstance().getQbResRequestExecutor();

        requestExecutorVideo = OnGravitiApp.getInstance().getQbResRequestExecutorVideo();
        sharedPrefsHelper = SharedPrefsHelperVideo.getInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        activitySetup();
    }

    private void activitySetup() {
        AppDetails.setActivity(this);
        AppDetails.setContext(this);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return false;
        }
        State networkState = networkInfo.getState();
        return (networkState == State.CONNECTED || networkState == State.CONNECTING);
    }


    public void getrateKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.exceptionaire.ongraviti", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {

        }
    }


    // Alert highlight fields
    public void setErrorMsg(String msg, EditText et, boolean isRequestFocus) {
        int ecolor = Color.RED; // whatever color you want
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        if (isRequestFocus) {
            et.requestFocus();
        }

        et.setError(ssbuilder);
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    public static int getScreenWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        int width = 800;
        if (android.os.Build.VERSION.SDK_INT > 13) {
            display.getSize(size);
            width = size.x;
        } else {
            width = display.getWidth();
        }
        return width;
    }

    // Method to copy File
    public void copyFile(String selectedImagePath, String outputFilePath) throws IOException {
        InputStream in = new FileInputStream(selectedImagePath);
        OutputStream out = new FileOutputStream(outputFilePath);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
//        Toast customToast = new Toast(getBaseContext());
//        customToast = Toast.makeText(getBaseContext(), "Image uploaded successfully.", Toast.LENGTH_LONG);
//        customToast.show();
    }

    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void saveUserData(QBUser qbUser) {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
//        sharedPrefsHelper.save(Constant.PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
        sharedPrefsHelper.saveQbUser(qbUser);
    }

    public void saveUserData(QBUser qbUser, String PREF_CURREN_ROOM_NAME) {
        SharedPrefsHelperVideo sharedPrefsHelperVideo = SharedPrefsHelperVideo.getInstance();
        sharedPrefsHelperVideo.save(PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
        sharedPrefsHelperVideo.saveQbUser(qbUser);
    }

    public QBUser getQBUserData() {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
//        sharedPrefsHelper.save(Constant.PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
        return sharedPrefsHelper.getQbUser();
    }
}
