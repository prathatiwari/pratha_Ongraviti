package com.exceptionaire.ongraviti.activities.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.ResponseDatingUser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

public class DatingCardAdapter extends ArrayAdapter<ResponseDatingUser> {
    public static boolean isFavorite = false;
    private ArrayList<ResponseDatingUser> cards = null;
    private LayoutInflater layoutInflater = null;
    Context context;
    public ImageLoader imageLoader = ImageLoader.getInstance();
    private String TAG = "DatingCardAdapter";
    private ArrayList<String> list_fav_flags = new ArrayList<>();

    private datingAddtoFavoriteListener customListner;
    private DatingChangeDataListener datingChangeDataListener;

    public DatingCardAdapter(Context context, ArrayList<ResponseDatingUser> cards) {
        super(context, -1);
        try {
            this.context = context;
            this.cards = cards;
            this.layoutInflater = LayoutInflater.from(context);
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public interface datingAddtoFavoriteListener {
        void ondatingAddtoFavoriteClickListner(int fav_user_id);
    }

    public interface DatingChangeDataListener {
        void onDataChange(ResponseDatingUser user);
    }

    public void setdatingAddtoFavorite(datingAddtoFavoriteListener listener) {
        this.customListner = listener;
    }

    public void setDatingChangeDataListener(DatingChangeDataListener datingChangeDataListener) {
        this.datingChangeDataListener = datingChangeDataListener;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        Log.d("datingadptr", "##  item count: " + getCount());
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View view = null;
        try {
            Log.d("Dating Adapter", "## getView : " + position);
            final ViewHolder viewHolder = new DatingCardAdapter.ViewHolder();
            final ResponseDatingUser card = cards.get(position);

            view = layoutInflater.inflate(R.layout.dating_card_item, parent, false);
            viewHolder.favorite = (ImageButton) view.findViewById(R.id.ask_for_dating_add_to_favorite);
            viewHolder.txtCustName = view.findViewById(R.id.txtCustName);

            viewHolder.imgCardImage = view.findViewById(R.id.card_image);
            if (!imageLoader.isInited())

            {
                imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
            }
            if (cards.get(position).getFavorite() == 1) {
                list_fav_flags.add(cards.get(position).getUserId());
                viewHolder.favorite.setImageResource(R.drawable.heart_pink);
                isFavorite = true;
            } else {

            }
            try {
                if (card.getCName() != null)
                    viewHolder.txtCustName.setText(card.getCName());
                if (card.getProfilePicture() != null) {
                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.placeholder)
                            .showImageForEmptyUri(R.drawable.placeholder)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppConstants.PROFILE_PICTURE_PATH + "/" + card.getProfilePicture(), viewHolder.imgCardImage, options);
                }

            } catch (Exception e) {
                Log.e(TAG, "## e : " + e.getMessage());
                e.printStackTrace();
            }

            for (int i = 0; i < list_fav_flags.size(); i++) {
                if (cards.get(position).getUserId().equals(list_fav_flags.get(i))) {
                    viewHolder.favorite.setImageResource(R.drawable.heart_pink);
                    isFavorite = true;
                }
            }
            if (this.datingChangeDataListener != null) {
                datingChangeDataListener.onDataChange(cards.get(position));
            }
            viewHolder.txtCustName.setOnClickListener(view1 -> {
                Intent intentFavUserView = new Intent(context, FavoriteUserInfoActivity.class);
                intentFavUserView.putExtra("ActivityName", "FavoriteProfile");
                intentFavUserView.putExtra("fav_user_id", cards.get(position).getUserId());
                context.startActivity(intentFavUserView);
            });

            viewHolder.favorite.setOnClickListener(view12 -> {
                if (!isFavorite) {
                    viewHolder.favorite.setImageResource(R.drawable.heart_pink);
                    list_fav_flags.add(cards.get(position).getUserId());
                    isFavorite = true;
                } else {
                    viewHolder.favorite.setImageResource(R.drawable.star_blank);
                    for (int i = 0; i < list_fav_flags.size(); i++) {
                        if (cards.get(position).getUserId().equals(list_fav_flags.get(i))) {
                            list_fav_flags.remove(cards.get(position).getUserId());
                        }
                    }
                    isFavorite = false;
                }
                if (customListner != null) {
                    customListner.ondatingAddtoFavoriteClickListner(Integer.parseInt(card.getUserId()));
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public ResponseDatingUser getItem(int position) {
        return cards.get(position);
    }

    @Override
    public int getCount() {
        Log.d(TAG, "## cards.size() : " + cards.size());
        return cards.size();
    }

    public class ViewHolder {
        ImageView favorite;
        ImageView imgCardImage;
        TextView txtCustName;

    }
}
