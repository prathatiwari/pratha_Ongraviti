package com.exceptionaire.ongraviti.activities.myorbit;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.OrbitRequestAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.BlockList;
import com.exceptionaire.ongraviti.model.MyOrbitRequest;
import com.exceptionaire.ongraviti.model.ResponseAcceptRejectOrbitRequest;
import com.exceptionaire.ongraviti.model.ResponseBlockUser;
import com.exceptionaire.ongraviti.model.ResponseMyOrbitRequests;
import com.exceptionaire.ongraviti.model.ResponseUnBlockUser;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by root on 2/1/18.
 */

public class MyBlockUser extends BaseDrawerActivity {
    private ProgressDialog progressDialog;
    private ArrayList<BlockList> orbitRequests;
    private BlockUserAdapter blockUserAdapter;
    private ListView list_contact;
    private TextView tv_msg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.layout_blockuser, frameLayout);
        setCationBar();
        blockUserAdapter = new BlockUserAdapter(new ArrayList<BlockList>(), MyBlockUser.this, this);
        progressDialog = new CustomProgressDialog(MyBlockUser.this, "Loading...");

        tv_msg = (TextView) findViewById(R.id.tv_msg);
        tv_msg.setText("You haven't blocked any one yet.");
        list_contact = (ListView) findViewById(R.id.list_view_contact);
        list_contact.setAdapter(blockUserAdapter);
    }

    @Override
    public void onResume() {
        showOrbitRequestList();

        super.onResume();
    }

    private void setCationBar() {
        arcMenu.setVisibility(View.VISIBLE);
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        TextView header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText("Blocking");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void showOrbitRequestList() {

        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(MyBlockUser.this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseBlockUser> loginCall = loginRequest.getMyBlockList(AppSettings.getLoginUserId());
        loginCall.enqueue(new Callback<ResponseBlockUser>() {
            @Override
            public void onResponse(Call<ResponseBlockUser> call, retrofit2.Response<ResponseBlockUser> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {

                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        ResponseBlockUser responseBlockUser = response.body();
                        if (responseBlockUser != null && responseBlockUser.getBlockList() != null && responseBlockUser.getBlockList().size() > 0) {
                            List<BlockList> myOrbitRequestArrayList = response.body().getBlockList();
                            blockUserAdapter.setOrbitRequestList(myOrbitRequestArrayList);
                            tv_msg.setVisibility(View.GONE);
                            list_contact.setVisibility(View.VISIBLE);
                        } else {
                            tv_msg.setVisibility(View.VISIBLE);
                            list_contact.setVisibility(View.GONE);
                        }
                    } else {
                        tv_msg.setVisibility(View.VISIBLE);
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseBlockUser> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Log.d("", "## onErrorResponse : " + t.toString());
            }
        });
    }

    public void acceptRejectOrbitRequest(final String senderUserId, final String flag, final int position) {

        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(MyBlockUser.this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseUnBlockUser> responseUnBlockUserCall = loginRequest.unblockUser(AppSettings.getLoginUserId(), senderUserId, flag);
        responseUnBlockUserCall.enqueue(new Callback<ResponseUnBlockUser>() {
            @Override
            public void onResponse(Call<ResponseUnBlockUser> call, retrofit2.Response<ResponseUnBlockUser> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                    final Dialog dialog = CommonDialogs.showMessageDialog(MyBlockUser.this, getResources().getString(R.string.ongravity), response.body().getMsg());
                    Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                    dialog_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            blockUserAdapter.removeItem(position);
                        }
                    });

                } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                    CommonDialogs.showMessageDialog(MyBlockUser.this, getResources().getString(R.string.ongraviti), "Something went wrong...Please try again.");
                }
            }

            @Override
            public void onFailure(Call<ResponseUnBlockUser> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Toast.makeText(MyBlockUser.this, "Please check your internet connection.", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        try {
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
