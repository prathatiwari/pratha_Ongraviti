package com.exceptionaire.ongraviti.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.HomeUserPostsNewAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.base.ShowHidePasswordEditText;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.imoji.EmojiconEditText;
import com.exceptionaire.ongraviti.model.MultiProfileInfo;
import com.exceptionaire.ongraviti.model.ProfileInfo;
import com.exceptionaire.ongraviti.model.ProfilePictureResponse;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseUpdateCover;
import com.exceptionaire.ongraviti.model.ResponseUpdateStatus;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.AnimationUtils;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.apache.http.protocol.HTTP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

public class ProfileMainActivity extends BaseDrawerActivity implements View.OnClickListener, AppConstants {
    TextView textView_profile_social, textView_profile_dating, textView_profile_business;
    RoundedImageView imageView_profile_social, imageView_profile_dating, imageView_profile_business;
    ImageView textview_uploade_image_social, textview_uploade_image_dating, textview_uploade_image_business;
    ImageView textview_uploade_video_social, textview_uploade_video_dating, textview_uploade_video_business;
    ImageView imageview_video_business, imageview_video_social, imageview_video_dating;
    public static UserBasicInfo userBasicInfo;
    private static final int REQUEST_PICK_VIDEO = 10013;
    private static final int CAMERA_VIDEO_CODE = 102;
    private static final int REQUEST_PICK_IMAGE = 10011;
    private static final int CAMERA_CODE = 101;
    private static final int REQUEST_SAF_PICK_IMAGE = 10012;
    private boolean isEdit;
    public Uri mImageCaptureUri, imageUri;
    private String displayName;
    private File tempFile;
    private String string;
    private File getTempFile;
    private int operation_code;
    public static ArrayList<ProfileInfo> list_profile_category;
    private String videoBioPath = null;
    private String videoBioPath1 = null;
    private String videoBioPath2 = null;
    ImageView imageview_cover_business, imageview_cover_social, imageview_cover_dating;
    ImageView text_view_changecover_social, text_view_changecover_dating, text_view_changecover_business;
    private int ImageViewIDClicked = 0;
    private BitmapFactory.Options options;
    private int ImageViewIDWhileCroppingImage = 0, clickId = 0;
    Boolean flagSocialPic = false;
    Boolean flagDatinglPic = false;
    Boolean flagBusinessPic = false;
    private ImageView imageVideoThumb;
    Boolean flagSocialCover = false;
    Boolean flagDatinglCover = false;
    Boolean flagBusinessCover = false;
    private File imageFile;
    private File post_image, post_video, post_video_thumb;
    private RequestBody requestPhotoFile, requestVideoFile, requestVideoThumb;
    private MultipartBody.Part postImageBody, postVideoBody, postVideoThumbBody;
    private RequestBody descriptionUserId, descriptionCatId;
    private String user_d = null, catagory_id = null;
    private boolean newProfilePicture = false, newVideoBio = false;
    private Intent intent;
    private Uri recordedFileUri;
    private String selectedVideoPath, selectedVideoUrl;
    boolean flagVideoSocial = false;
    boolean flagVideoDating = false;
    boolean flagVideoBusiness = false;
    TextView name3, name1, name2;
    Boolean flagSocialCover1 = false;
    Boolean flagDatinglCover1 = false;
    Boolean flagBusinessCover1 = false;
    Boolean flagSocialPicApi = false;
    Boolean flagDatinglPicApi = false;
    Boolean flagBusinessPicApi = false;
    ImageView image_view_status, image_view_status1, image_view_status2;
    public ImageLoader imageLoader = ImageLoader.getInstance();
    private String TAG = "ProfileMainActivity";
    private CustomProgressDialog progressDialog;
    private LinearLayout linearLayoutLockSocial, linearLayoutLockDating, linearLayoutLockBusiness;
    private final static int socialLock = 1, datingLock = 2, businessLock = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_profile_main, frameLayout);
        arcMenu.setVisibility(View.VISIBLE);
        TextView header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText("Edit Profile");
        initializeViewComponent();
        getUserProfileData();
    }

    @Override
    public void onResume() {

        super.onResume();
        setSelectedProfileOfUser();
    }


    private void initializeViewComponent() {
        findViewById(R.id.button_edit_profile_details).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //AppSettings.setProfileStepStatus1(0);
                // AppSettings.setProfileStepStatus2(0);
                // AppSettings.setProfileStepStatus3(0);
                // AppSettings.setProfileStepStatus4(0);
                // AppSettings.setProfileStepStatus5(0);

                Intent intent = new Intent(ProfileMainActivity.this, EditProfileDetailsActivity.class);
                startActivity(intent);
            }
        });
        imageVideoThumb = (ImageView) findViewById(R.id.imgVideoThumb);

        name3 = (TextView) findViewById(R.id.name);
        name1 = (TextView) findViewById(R.id.name1);
        name2 = (TextView) findViewById(R.id.name2);


//        imageViewSocial = (ImageView) findViewById(R.id.image_view_social);
//        imageViewDating = (ImageView) findViewById(R.id.image_view_dating);
//        imageViewBusiness = (ImageView) findViewById(R.id.image_view_business);
        linearLayoutLockSocial = (LinearLayout) findViewById(R.id.linear_layout_lock_social);
        linearLayoutLockDating = (LinearLayout) findViewById(R.id.linear_layout_lock_dating);
        linearLayoutLockBusiness = (LinearLayout) findViewById(R.id.linear_layout_lock_business);
        image_view_status = (ImageView) findViewById(R.id.image_view_status);
        image_view_status1 = (ImageView) findViewById(R.id.image_view_status1);
        image_view_status2 = (ImageView) findViewById(R.id.image_view_status2);

        textView_profile_social = (TextView) findViewById(R.id.textView_profile_social);
        textView_profile_dating = (TextView) findViewById(R.id.textView_profile_dating);
        textView_profile_business = (TextView) findViewById(R.id.textView_profile_business);

        textview_uploade_video_social = (ImageView) findViewById(R.id.textview_uploade_video_social);
        textview_uploade_video_dating = (ImageView) findViewById(R.id.textview_uploade_video_dating);
        textview_uploade_video_business = (ImageView) findViewById(R.id.textview_uploade_video_business);


        textview_uploade_image_social = (ImageView) findViewById(R.id.textview_uploade_image_social);
        textview_uploade_image_dating = (ImageView) findViewById(R.id.textview_uploade_image_dating);
        textview_uploade_image_business = (ImageView) findViewById(R.id.textview_uploade_image_business);

        imageView_profile_social = (RoundedImageView) findViewById(R.id.imageView_profile_social);
        imageView_profile_dating = (RoundedImageView) findViewById(R.id.imageView_profile_dating);
        imageView_profile_business = (RoundedImageView) findViewById(R.id.imageView_profile_business);

        imageview_video_social = (ImageView) findViewById(R.id.imageview_video_social);
        imageview_video_dating = (ImageView) findViewById(R.id.imageview_video_dating);
        imageview_video_business = (ImageView) findViewById(R.id.imageview_video_business);

        imageview_cover_business = (ImageView) findViewById(R.id.imageview_cover_business);
        imageview_cover_social = (ImageView) findViewById(R.id.imageview_cover_social);
        imageview_cover_dating = (ImageView) findViewById(R.id.imageview_cover_dating);

        text_view_changecover_social = (ImageView) findViewById(R.id.text_view_changecover_social);
        text_view_changecover_dating = (ImageView) findViewById(R.id.text_view_changecover_dating);
        text_view_changecover_business = (ImageView) findViewById(R.id.text_view_changecover_business);

        textView_profile_social.setOnClickListener(this);
        textView_profile_dating.setOnClickListener(this);
        textView_profile_business.setOnClickListener(this);

        textview_uploade_video_social.setOnClickListener(this);
        textview_uploade_video_dating.setOnClickListener(this);
        textview_uploade_video_business.setOnClickListener(this);

        textview_uploade_image_social.setOnClickListener(this);
        textview_uploade_image_dating.setOnClickListener(this);
        textview_uploade_image_business.setOnClickListener(this);

        imageview_video_business.setOnClickListener(this);
        imageview_video_social.setOnClickListener(this);
        imageview_video_dating.setOnClickListener(this);

        text_view_changecover_social.setOnClickListener(this);
        text_view_changecover_dating.setOnClickListener(this);
        text_view_changecover_business.setOnClickListener(this);

        imageView_profile_social.setOnClickListener(this);
        imageView_profile_dating.setOnClickListener(this);
        imageView_profile_business.setOnClickListener(this);

        image_view_status.setOnClickListener(this);
        image_view_status1.setOnClickListener(this);
        image_view_status2.setOnClickListener(this);

//Snehal

//        imageViewSocial.setOnClickListener(this);
//        imageViewDating.setOnClickListener(this);
//        imageViewBusiness.setOnClickListener(this);
        linearLayoutLockSocial.setOnClickListener(this);
        linearLayoutLockDating.setOnClickListener(this);
        linearLayoutLockBusiness.setOnClickListener(this);
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        }
        setSelectedProfileOfUser();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView_profile_social:
                ImageViewIDClicked = R.id.imageView_profile_social;
                checkForPermissionsAndShowOptionDialog();
                flagSocialPic = true;
                flagSocialPicApi = true;
                break;
            case R.id.imageView_profile_dating:
                ImageViewIDClicked = R.id.imageView_profile_dating;
                checkForPermissionsAndShowOptionDialog();
                flagDatinglPic = true;
                flagDatinglPicApi = true;
                break;
            case R.id.imageView_profile_business:
                ImageViewIDClicked = R.id.imageView_profile_business;
                checkForPermissionsAndShowOptionDialog();
                flagBusinessPic = true;
                flagBusinessPicApi = true;
                break;
            case R.id.text_view_changecover_social:
                ImageViewIDClicked = R.id.text_view_changecover_social;
                checkForPermissionsAndShowOptionDialog();
                flagSocialCover = true;
                flagSocialCover1 = true;
                break;
            case R.id.text_view_changecover_dating:
                ImageViewIDClicked = R.id.text_view_changecover_dating;
                checkForPermissionsAndShowOptionDialog();
                flagDatinglCover = true;
                flagDatinglCover1 = true;
                break;
            case R.id.text_view_changecover_business:
                ImageViewIDClicked = R.id.text_view_changecover_business;
                checkForPermissionsAndShowOptionDialog();
                flagBusinessCover = true;
                flagBusinessCover1 = true;
                break;
            case R.id.textView_profile_social:
                Intent i = new Intent(this, ViewProfileActivity.class);
                i.putExtra("social", "social");
                startActivity(i);
                break;
            case R.id.textView_profile_dating:
                Intent i1 = new Intent(this, ViewProfileActivity.class);
                i1.putExtra("dating", "dating");
                startActivity(i1);
                break;
            case R.id.textView_profile_business:
                Intent i2 = new Intent(this, ViewProfileActivity.class);
                i2.putExtra("business", "business");
                startActivity(i2);
                break;
            case R.id.textview_uploade_video_social:

                checkForPermissionsAndShowOptionDialogVideo();
                flagVideoSocial = true;
                break;
            case R.id.textview_uploade_video_dating:
                checkForPermissionsAndShowOptionDialogVideo();
                flagVideoDating = true;
                break;
            case R.id.textview_uploade_video_business:
                checkForPermissionsAndShowOptionDialogVideo();
                flagVideoBusiness = true;
                break;

            case R.id.textview_uploade_image_social:
                ImageViewIDClicked = R.id.textview_uploade_image_social;
                checkForPermissionsAndShowOptionDialog();
                flagSocialPic = true;
                flagSocialPicApi = true;
                break;
            case R.id.textview_uploade_image_dating:
                ImageViewIDClicked = R.id.textview_uploade_image_dating;
                checkForPermissionsAndShowOptionDialog();
                flagDatinglPic = true;
                flagDatinglPicApi = true;
                break;
            case R.id.textview_uploade_image_business:
                ImageViewIDClicked = R.id.textview_uploade_image_business;
                checkForPermissionsAndShowOptionDialog();
                flagBusinessPic = true;
                flagBusinessPicApi = true;
                break;
//TODO same thiing in view profile
            case R.id.imageview_video_social:
                Intent intent = new Intent(ProfileMainActivity.this, VideoBioActivity.class);
                intent.putExtra("videopath", videoBioPath);
                startActivity(intent);
                break;
            case R.id.imageview_video_dating:
                Intent intent1 = new Intent(ProfileMainActivity.this, VideoBioActivity.class);
                intent1.putExtra("videopath", videoBioPath1);
                startActivity(intent1);
                break;
            case R.id.imageview_video_business:
                Intent intent2 = new Intent(ProfileMainActivity.this, VideoBioActivity.class);
                intent2.putExtra("videopath", videoBioPath2);
                startActivity(intent2);
                break;


            case R.id.linear_layout_lock_social:
                showDialogToLockProfile(socialLock);
                break;

            case R.id.linear_layout_lock_dating:
                showDialogToLockProfile(datingLock);
                break;

            case R.id.linear_layout_lock_business:
                showDialogToLockProfile(businessLock);
                break;
            case R.id.image_view_status:
                Dialog dialogStatus = new CommonDialogs().dialogStatus(ProfileMainActivity.this);
                EmojiconEditText editTextStatus = (EmojiconEditText) dialogStatus.findViewById(R.id.edit_text_status);
                editTextStatus.setHint("Write your status here...");
                dialogStatus.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editTextStatus.setError(null);

                        if (editTextStatus.getText().toString().trim().length() == 0) {
                            setErrorMsg("Please Enter Status.", editTextStatus, true);
                        } else {
                            try {
                                setStatus("1", URLEncoder.encode(editTextStatus.getText().toString().trim(), HTTP.UTF_8));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            dialogStatus.dismiss();
                        }
                    }
                });
                dialogStatus.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogStatus.dismiss();
                    }
                });

                break;
            case R.id.image_view_status1:
                Dialog dialogStatus1 = new CommonDialogs().dialogStatus(ProfileMainActivity.this);
                EmojiconEditText editTextStatus1 = (EmojiconEditText) dialogStatus1.findViewById(R.id.edit_text_status);
                editTextStatus1.setHint("Write your status here...");
                dialogStatus1.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editTextStatus1.setError(null);
                        if (editTextStatus1.getText().toString().trim().length() == 0) {
                            setErrorMsg("Please Enter Status.", editTextStatus1, true);
                        } else {
                            try {
                                setStatus("2", URLEncoder.encode(editTextStatus1.getText().toString().trim(), HTTP.UTF_8));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            dialogStatus1.dismiss();
                        }
                    }
                });
                dialogStatus1.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogStatus1.dismiss();
                    }
                });
                break;
            case R.id.image_view_status2:
                Dialog dialogStatus3 = new CommonDialogs().dialogStatus(ProfileMainActivity.this);
                EmojiconEditText editTextStatus3 = (EmojiconEditText) dialogStatus3.findViewById(R.id.edit_text_status);
                editTextStatus3.setHint("Write your status here...");
                dialogStatus3.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editTextStatus3.setError(null);
                        if (editTextStatus3.getText().toString().trim().length() == 0) {
                            setErrorMsg("Please Enter Status.", editTextStatus3, true);
                        } else {
                            try {
                                setStatus("3", URLEncoder.encode(editTextStatus3.getText().toString().trim(), HTTP.UTF_8));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            dialogStatus3.dismiss();
                        }
                    }
                });
                dialogStatus3.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogStatus3.dismiss();
                    }
                });
                break;
        }
    }

    private void showDialogToLockProfile(int lockType) {

        final Dialog dialog = new CommonDialogs().displayLockOptionDialog(ProfileMainActivity.this);
        RoundedImageView lockProfile = (RoundedImageView) dialog.findViewById(R.id.lock_profile);
        RoundedImageView lockVideo = (RoundedImageView) dialog.findViewById(R.id.lock_video);
        if (AppSettings.getProfileDefaultCategory() != null) {
            if (lockType == socialLock) {
                if (AppSettings.getProfileCatPicPathOne() != null) {

                    AppSettings.setProfiePicturePath(AppSettings.getProfileCatPicPathOne());

                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.profile)
                            .showImageForEmptyUri(R.drawable.profile)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppSettings.getProfileCatPicPathOne(), lockProfile, options);

                } else {
                    lockProfile.setImageResource(R.drawable.profile);
                }
            } else if (lockType == datingLock) {
                if (AppSettings.getProfileCatPicPathTWO() != null) {
                    AppSettings.setProfiePicturePath(AppSettings.getProfileCatPicPathTWO());
                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.profile)
                            .showImageForEmptyUri(R.drawable.profile)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppSettings.getProfileCatPicPathTWO(), lockProfile, options);
                } else {
                    lockProfile.setImageResource(R.drawable.profile);
                }
            } else if (lockType == businessLock) {
                if (AppSettings.getProfileCatPicPathThree() != null) {
                    AppSettings.setProfiePicturePath(AppSettings.getProfileCatPicPathThree());

                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.profile)
                            .showImageForEmptyUri(R.drawable.profile)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppSettings.getProfileCatPicPathThree(), lockProfile, options);
                } else {
                    lockProfile.setImageResource(R.drawable.profile);
                }
            }
        }
        lockProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (lockType == socialLock) {

                    Log.d(TAG, "## imgProfileLock");
                    Intent intentQuestionaire = new Intent(ProfileMainActivity.this, QuestionaireActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("profile_cat_id", AppConstants.PROFILE_CATEGORY_ONE_SOCIAL);  // 1-Social,2-Dating,3-Business
                    bundle.putString("is_set_lock", String.valueOf(AppConstants.profile_photo));      //(pic=1,video=2),

                    intentQuestionaire.putExtras(bundle);
                    startActivity(intentQuestionaire);
                    finish();
                } else if (lockType == datingLock) {

                    Log.d(TAG, "## imgProfileLock");
                    Intent intentQuestionaire = new Intent(ProfileMainActivity.this, QuestionaireActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("profile_cat_id", AppConstants.PROFILE_CATEGORY_TWO_DATING);  // 1-Social,2-Dating,3-Business
                    bundle.putString("is_set_lock", String.valueOf(AppConstants.profile_photo));      //(pic=1,video=2),

                    intentQuestionaire.putExtras(bundle);
                    startActivity(intentQuestionaire);
                    finish();
                } else if (lockType == businessLock) {

                    Log.d(TAG, "## imgProfileLock");
                    Intent intentQuestionaire = new Intent(ProfileMainActivity.this, QuestionaireActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("profile_cat_id", AppConstants.PROFILE_CATEGORY_THREE_BUSINESS);  // 1-Social,2-Dating,3-Business
                    bundle.putString("is_set_lock", String.valueOf(AppConstants.profile_photo));      //(pic=1,video=2),

                    intentQuestionaire.putExtras(bundle);
                    startActivity(intentQuestionaire);
                    finish();
                }

                dialog.dismiss();
            }
        });

        lockVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (lockType == socialLock) {

                    Log.d(TAG, "## imgBtnVideoLock");
                    Intent intentQuestionaire1 = new Intent(ProfileMainActivity.this, QuestionaireActivity.class);
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("profile_cat_id", AppConstants.PROFILE_CATEGORY_ONE_SOCIAL);  // 1-Social,2-Dating,3-Business
                    bundle1.putString("is_set_lock", String.valueOf(AppConstants.profile_video));      //(pic=1,video=2),

                    intentQuestionaire1.putExtras(bundle1);
                    startActivity(intentQuestionaire1);
                    finish();

                } else if (lockType == datingLock) {

                    Log.d(TAG, "## imgBtnVideoLock");
                    Intent intentQuestionaire1 = new Intent(ProfileMainActivity.this, QuestionaireActivity.class);
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("profile_cat_id", AppConstants.PROFILE_CATEGORY_TWO_DATING);  // 1-Social,2-Dating,3-Business
                    bundle1.putString("is_set_lock", String.valueOf(AppConstants.profile_video));      //(pic=1,video=2),

                    intentQuestionaire1.putExtras(bundle1);
                    startActivity(intentQuestionaire1);
                    finish();

                } else if (lockType == businessLock) {

                    Log.d(TAG, "## imgBtnVideoLock");
                    Intent intentQuestionaire1 = new Intent(ProfileMainActivity.this, QuestionaireActivity.class);
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("profile_cat_id", AppConstants.PROFILE_CATEGORY_THREE_BUSINESS);  // 1-Social,2-Dating,3-Business
                    bundle1.putString("is_set_lock", String.valueOf(AppConstants.profile_video));      //(pic=1,video=2),

                    intentQuestionaire1.putExtras(bundle1);
                    startActivity(intentQuestionaire1);
                    finish();
                }
                dialog.dismiss();
            }
        });
    }


    public void getUserProfileData() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        if (isNetworkAvailable()) {

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
            Call<ResponseGetUserProfileData> loginCall = getProfileRequest.getUserProfileData(AppSettings.getLoginUserId(), "");
            loginCall.enqueue(new Callback<ResponseGetUserProfileData>() {
                @Override
                public void onResponse(Call<ResponseGetUserProfileData> call, Response<ResponseGetUserProfileData> response) {
                    if (response != null && response.body() != null) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                            setDataInControls(response.body());

                            Gson gson = new Gson();
                            String json = gson.toJson(response.body());
                            AppSettings.setProfileDetails(json);
                        }
                    } else {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        AppSettings.setLogin(false);
                        new CommonDialogs().showMessageDialogNonStatic(ProfileMainActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                    }
                }

                @Override
                public void onFailure(Call<ResponseGetUserProfileData> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(this, getResources().getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void setDataInControls(ResponseGetUserProfileData responseGetUserProfileData) {
        if (responseGetUserProfileData.getUser_basic_info().length > 0) {
            userBasicInfo = responseGetUserProfileData.getUser_basic_info()[0];
            if (userBasicInfo.getName() != null) {
                if (!userBasicInfo.getName().equals("")) {
                    name3.setText(userBasicInfo.getName());
                    name1.setText(userBasicInfo.getName());
                    name2.setText(userBasicInfo.getName());
                } else {
                    name3.setText("User Name");
                    name1.setText("User Name");
                    name2.setText("User Name");
                }
            } else {
                name3.setText("User Name");
                name1.setText("User Name");
                name2.setText("User Name");
            }

            AppSettings.setUserProfileName(userBasicInfo.getName());
            Log.d("nameeeeee1", AppSettings.getUserProfileName());
            BaseDrawerActivity.updateDrawer();
        }
        if (responseGetUserProfileData.getProfile_info().length > 0) {

            if (responseGetUserProfileData.getProfile_info().length > 2) {
                if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[2] != null && responseGetUserProfileData.getProfile_info().length > 2)
                    videoBioPath = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[2].getProfile_bio();

            }
            if (responseGetUserProfileData.getProfile_info().length > 1) {
                if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[1] != null && responseGetUserProfileData.getProfile_info().length > 1)
                    videoBioPath1 = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
            }
            if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[0] != null && responseGetUserProfileData.getProfile_info().length > 0)
                videoBioPath2 = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[0].getProfile_bio();

            //For index 2
            if (responseGetUserProfileData.getProfile_info().length > 2) {
                String FinalCatId = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
                if (FinalCatId.equals("1")) {
                    if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[2] != null) {
                        if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(imageView_profile_social);

                            AppSettings.setProfileCatPicPathOne(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());

                        } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(imageView_profile_social);

                            AppSettings.setProfileCatPicPathOne(responseGetUserProfileData.getProfile_info()[2].getProfile_picture());

                        } else if (AppSettings.getProfileCatPicPathFB() != null || !AppSettings.getProfileCatPicPathFB().equals("")) {
                            Picasso.with(this)
                                    .load(AppSettings.getProfileCatPicPathFB())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(imageView_profile_social);
                        }
                    }

                    AppSettings.setProfileCatCoverPathOne(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());

                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.bg)
                            .showImageForEmptyUri(R.drawable.bg)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture(), imageview_cover_social, options);

                } else if (FinalCatId.equals("2")) {
                    if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[2] != null) {
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_dating);

                        AppSettings.setProfileCatPicPathTwo(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());

                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture(), imageview_cover_dating, options);


                        AppSettings.setProfileCatCoverPathTwo(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                    }
                } else if (FinalCatId.equals("3")) {
                    if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[2] != null) {
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_business);

                        AppSettings.setProfileCatPicPathThree(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());

                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture(), imageview_cover_business, options);


                        AppSettings.setProfileCatCoverPathThree(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                    }
                }
            }

            //For index 1
            if (responseGetUserProfileData.getProfile_info().length > 1) {
                String FinalCatId = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
                if (FinalCatId.equals("1")) {
                    if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[1] != null) {
                        if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(imageView_profile_social);

                            AppSettings.setProfileCatPicPathOne(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(imageView_profile_social);

                            AppSettings.setProfileCatPicPathOne(responseGetUserProfileData.getProfile_info()[1].getProfile_picture());

                        } else if (AppSettings.getProfileCatPicPathFB() != null || !AppSettings.getProfileCatPicPathFB().equals("")) {
                            Picasso.with(this)
                                    .load(AppSettings.getProfileCatPicPathFB())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(imageView_profile_social);
                        }
                        AppSettings.setProfileCatCoverPathOne(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture(), imageview_cover_social, options);


                    }
                } else if (FinalCatId.equals("2")) {
                    if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[1] != null) {
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_dating);

                        AppSettings.setProfileCatPicPathTwo(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());

                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture(), imageview_cover_dating, options);

                        AppSettings.setProfileCatCoverPathTwo(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                    }
                } else if (FinalCatId.equals("3")) {
                    if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[1] != null) {
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_business);

                        AppSettings.setProfileCatPicPathThree(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());

                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture(), imageview_cover_business, options);
                        AppSettings.setProfileCatCoverPathThree(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                    }
                }
            }
            //For index 0

            String FinalCatId = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
            if (FinalCatId.equals("1")) {
                if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[0] != null) {
                    if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_social);

                        AppSettings.setProfileCatPicPathOne(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                    } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                        Picasso.with(this)
                                .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_social);

                        AppSettings.setProfileCatPicPathOne(responseGetUserProfileData.getProfile_info()[0].getProfile_picture());

                    } else if (AppSettings.getProfileCatPicPathFB() != null || !AppSettings.getProfileCatPicPathFB().equals("")) {
                        Picasso.with(this)
                                .load(AppSettings.getProfileCatPicPathFB())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_social);
                    }

                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.bg)
                            .showImageForEmptyUri(R.drawable.bg)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture(), imageview_cover_social, options);
                    AppSettings.setProfileCatCoverPathOne(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                }
            } else if (FinalCatId.equals("2")) {
                if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[0] != null) {
                    Picasso.with(this)
                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imageView_profile_dating);

                    AppSettings.setProfileCatPicPathTwo(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());

                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.bg)
                            .showImageForEmptyUri(R.drawable.bg)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture(), imageview_cover_dating, options);
                    AppSettings.setProfileCatCoverPathTwo(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());

                }
            } else if (FinalCatId.equals("3")) {
                if (responseGetUserProfileData != null && responseGetUserProfileData.getProfile_info()[0] != null) {
                    Picasso.with(this)
                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imageView_profile_business);

                    AppSettings.setProfileCatPicPathThree(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());

                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.bg)
                            .showImageForEmptyUri(R.drawable.bg)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture(), imageview_cover_business, options);
                    AppSettings.setProfileCatCoverPathThree(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                }
            }

        }

    }


///////////TODO/////////////////////

    /******************
     * Image Selection
     ****************/

    @SuppressLint("NewApi")
    private void checkForPermissionsAndShowOptionDialog() {

        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if (!hasPermissions(ProfileMainActivity.this, PERMISSIONS)) {
            requestPermissions(PERMISSIONS, PERMISSION_REQUEST_CODE_CAMERA_STORAGE);
        } else {
            selectImage(ImageViewIDClicked);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void selectImage(final int imageViewID) {
        try {
            final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};

            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(ProfileMainActivity.this);
            builder.setTitle("Add Photo!");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                   /* if (items[item].equals("View Picture")) {
                        if (flagSocialPic == true) {

                            new CommonDialogs().displayImageZoomDialog(ProfileMainActivity.this,
                                    AppSettings.getProfileCatPicPathOne());
                            dialog.dismiss();

                            flagSocialPic = false;

                        } else if (flagDatinglPic == true) {
                            new CommonDialogs().displayImageZoomDialog(ProfileMainActivity.this,
                                    AppSettings.getProfileCatPicPathTWO());
                            dialog.dismiss();

                            flagDatinglPic = false;

                        } else if (flagBusinessPic == true) {
                            new CommonDialogs().displayImageZoomDialog(ProfileMainActivity.this,
                                    AppSettings.getProfileCatPicPathThree());
                            dialog.dismiss();

                            flagBusinessPic = false;

                        } else if (flagSocialCover == true) {
                            if (AppSettings.getProfilePicturePath() != null && !AppSettings.getProfileCatCoverPathOne().isEmpty())
                                new CommonDialogs().displayImageZoomDialog(ProfileMainActivity.this,
                                        AppSettings.getProfileCatCoverPathOne());
                            dialog.dismiss();
                            flagSocialCover = false;

                        } else if (flagDatinglCover == true) {
                            if (AppSettings.getProfilePicturePath() != null && !AppSettings.getProfileCatCoverPathTWO().isEmpty())
                                new CommonDialogs().displayImageZoomDialog(ProfileMainActivity.this,
                                        AppSettings.getProfileCatCoverPathTWO());
                            dialog.dismiss();
                            flagDatinglCover = false;

                        } else if (flagBusinessCover == true) {
                            if (AppSettings.getProfilePicturePath() != null && !AppSettings.getProfileCatCoverPathThree().isEmpty())
                                new CommonDialogs().displayImageZoomDialog(ProfileMainActivity.this,
                                        AppSettings.getProfileCatCoverPathThree());
                            dialog.dismiss();
                            flagBusinessCover = false;

                        }
                    } else*/
                    if (items[item].equals("Capture Photo")) {
                        getCamera(imageViewID);
                        dialog.dismiss();

                    } else if (items[item].equals("Choose from Gallery")) {
                        getGallery(imageViewID);
                        dialog.dismiss();
                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }

                }
            });
            builder.show();
        } catch (Exception ee) {
            ee.printStackTrace();
        }


    }


    public void getCamera(int imageViewID) {
        if (!AppUtils.isDeviceSupportCamera(ProfileMainActivity.this)) {
            Toast.makeText(ProfileMainActivity.this,
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            newProfilePicture = false;
        } else {
            File dumpFolder = new File(PROFILE_PICTURES);
            if (!dumpFolder.exists()) {
                dumpFolder.mkdirs();
            }

            String imageName = "";
            int REQUEST_CODE = 0;
            switch (imageViewID) {
                case R.id.imageView_profile_social:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_social);
                    REQUEST_CODE = CAMERA_REQUEST_AVATAR;
                    break;
                case R.id.imageView_profile_dating:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_dating);
                    REQUEST_CODE = CAMERA_REQUEST_AVATAR;
                    break;
                case R.id.imageView_profile_business:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_business);
                    REQUEST_CODE = CAMERA_REQUEST_AVATAR;
                    break;
                case R.id.textview_uploade_image_social:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_social);
                    REQUEST_CODE = CAMERA_REQUEST_AVATAR;
                    break;
                case R.id.textview_uploade_image_dating:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_dating);
                    REQUEST_CODE = CAMERA_REQUEST_AVATAR;
                    break;
                case R.id.textview_uploade_image_business:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_business);
                    REQUEST_CODE = CAMERA_REQUEST_AVATAR;
                    break;
                case R.id.text_view_changecover_social:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_social);
                    REQUEST_CODE = CAMERA_REQUEST_AVATAR;
                    break;
                case R.id.text_view_changecover_dating:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_dating);
                    REQUEST_CODE = CAMERA_REQUEST_AVATAR;
                    break;
                case R.id.text_view_changecover_business:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_business);
                    REQUEST_CODE = CAMERA_REQUEST_AVATAR;
                    break;
            }

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(PROFILE_PICTURES + File.separator + imageName + ".jpg")));
            startActivityForResult(intent, REQUEST_CODE);
        }
    }


    public void getGallery(int imageViewID) {
        String imageName = "";
        int REQUEST_CODE = 0;
        switch (imageViewID) {
            //TODO call same for another imageView_profile_dating,business
            case R.id.imageView_profile_social:
                imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_social);
                REQUEST_CODE = GALLERY_REQUEST_AVATAR;
                break;
            case R.id.imageView_profile_dating:
                imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_dating);
                REQUEST_CODE = GALLERY_REQUEST_AVATAR;
                break;
            case R.id.imageView_profile_business:
                imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_business);
                REQUEST_CODE = GALLERY_REQUEST_AVATAR;
                break;
            case R.id.textview_uploade_image_social:
                imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_social);
                REQUEST_CODE = GALLERY_REQUEST_AVATAR;
                break;
            case R.id.textview_uploade_image_dating:
                imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_dating);
                REQUEST_CODE = GALLERY_REQUEST_AVATAR;
                break;
            case R.id.textview_uploade_image_business:
                imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_business);
                REQUEST_CODE = GALLERY_REQUEST_AVATAR;
                break;
            case R.id.text_view_changecover_social:
                imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_social);
                REQUEST_CODE = GALLERY_REQUEST_AVATAR;
                break;
            case R.id.text_view_changecover_dating:
                imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_dating);
                REQUEST_CODE = GALLERY_REQUEST_AVATAR;
                break;
            case R.id.text_view_changecover_business:
                imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_business);
                REQUEST_CODE = GALLERY_REQUEST_AVATAR;
                break;

        }

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(galleryIntent, REQUEST_CODE);
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        int IMAGE_SIZE = 0;

        File dir = new File(PROFILE_PICTURES);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (requestCode) {
            case CAMERA_REQUEST_AVATAR:
                if (resultCode == RESULT_OK) {
                    if (flagSocialPic == true) {
                        changeOrientation(R.id.imageView_profile_social, ProfileMainActivity.this.getString(R.string.profile_avatar_social));
                        flagSocialPic = false;
                    } else if (flagDatinglPic == true) {
                        changeOrientation(R.id.imageView_profile_dating, ProfileMainActivity.this.getString(R.string.profile_avatar_dating));
                        flagDatinglPic = false;
                    } else if (flagBusinessPic == true) {
                        changeOrientation(R.id.imageView_profile_business, ProfileMainActivity.this.getString(R.string.profile_avatar_business));
                        flagBusinessPic = false;
                    } else if (flagSocialCover == true) {
                        changeOrientation(R.id.imageview_cover_social, ProfileMainActivity.this.getString(R.string.profile_avatar_social));
                        flagSocialCover = false;
                    } else if (flagDatinglCover == true) {
                        changeOrientation(R.id.imageview_cover_dating, ProfileMainActivity.this.getString(R.string.profile_avatar_dating));
                        flagDatinglCover = false;
                    } else if (flagBusinessCover == true) {
                        changeOrientation(R.id.imageview_cover_business, ProfileMainActivity.this.getString(R.string.profile_avatar_business));
                        flagBusinessCover = false;
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(ProfileMainActivity.this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                    newProfilePicture = false;
                } else {
                    Toast.makeText(ProfileMainActivity.this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                    newProfilePicture = false;
                }
                break;

            case GALLERY_REQUEST_AVATAR:
                if (resultCode == RESULT_OK) {
                    if (flagSocialPic == true) {
                        saveImageAndShowImageView(R.id.imageView_profile_social, ProfileMainActivity.this.getString(R.string.profile_avatar_social), intent);
                        flagSocialPic = false;
                    } else if (flagDatinglPic == true) {
                        saveImageAndShowImageView(R.id.imageView_profile_dating, ProfileMainActivity.this.getString(R.string.profile_avatar_dating), intent);
                        flagDatinglPic = false;
                    } else if (flagBusinessPic == true) {
                        saveImageAndShowImageView(R.id.imageView_profile_business, ProfileMainActivity.this.getString(R.string.profile_avatar_business), intent);
                        flagBusinessPic = false;
                    } else if (flagSocialCover == true) {
                        saveImageAndShowImageView(R.id.imageview_cover_social, ProfileMainActivity.this.getString(R.string.profile_avatar_social), intent);
                        flagSocialCover = false;
                    } else if (flagDatinglCover == true) {
                        saveImageAndShowImageView(R.id.imageview_cover_dating, ProfileMainActivity.this.getString(R.string.profile_avatar_dating), intent);
                        flagDatinglCover = false;
                    } else if (flagBusinessCover == true) {
                        saveImageAndShowImageView(R.id.imageview_cover_business, ProfileMainActivity.this.getString(R.string.profile_avatar_business), intent);
                        flagBusinessCover = false;
                    }

                } else {
                    Toast.makeText(ProfileMainActivity.this, "You haven't picked an Image", Toast.LENGTH_LONG).show();
                    newProfilePicture = false;
                }
                break;

            case 10:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;

                    final Bitmap bitmap = BitmapFactory.decodeFile(BUSINESS_PROFILE_PICTURE,
                            options);
                    if (null != bitmap) {
                        if (flagSocialPic == true) {
                            if (imageView_profile_social != null) {
                                IMAGE_SIZE = getScreenWidth(ProfileMainActivity.this) / 2;
                                if (IMAGE_SIZE <= 0)
                                    IMAGE_SIZE = 400;
                                Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                if (image != null) {
                                    (imageView_profile_social).setImageBitmap(image);
                                    newProfilePicture = true;
                                } else {
                                    Toast.makeText(ProfileMainActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                    newProfilePicture = false;
                                }
                            }
                            flagSocialPic = false;
                        } else if (flagDatinglPic == true) {
                            if (imageView_profile_dating != null) {
                                IMAGE_SIZE = getScreenWidth(ProfileMainActivity.this) / 2;
                                if (IMAGE_SIZE <= 0)
                                    IMAGE_SIZE = 400;
                                Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                if (image != null) {
                                    (imageView_profile_dating).setImageBitmap(image);
                                    newProfilePicture = true;
                                } else {
                                    Toast.makeText(ProfileMainActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                    newProfilePicture = false;
                                }
                            }
                            flagDatinglPic = false;
                        } else if (flagBusinessPic == true) {
                            if (imageView_profile_business != null) {
                                IMAGE_SIZE = getScreenWidth(ProfileMainActivity.this) / 2;
                                if (IMAGE_SIZE <= 0)
                                    IMAGE_SIZE = 400;
                                Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                if (image != null) {
                                    (imageView_profile_business).setImageBitmap(image);
                                    newProfilePicture = true;
                                } else {
                                    Toast.makeText(ProfileMainActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                    newProfilePicture = false;
                                }
                            }
                            flagBusinessPic = false;
                        } else if (flagSocialCover == true) {
                            if (imageview_cover_social != null) {
                                IMAGE_SIZE = getScreenWidth(ProfileMainActivity.this) / 2;
                                if (IMAGE_SIZE <= 0)
                                    IMAGE_SIZE = 400;
                                Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                if (image != null) {
                                    (imageview_cover_social).setImageBitmap(image);
                                    newProfilePicture = true;
                                } else {
                                    Toast.makeText(ProfileMainActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                    newProfilePicture = false;
                                }
                            }
                            flagSocialCover = false;
                        } else if (flagDatinglCover == true) {
                            if (imageview_cover_dating != null) {
                                IMAGE_SIZE = getScreenWidth(ProfileMainActivity.this) / 2;
                                if (IMAGE_SIZE <= 0)
                                    IMAGE_SIZE = 400;
                                Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                if (image != null) {
                                    (imageview_cover_dating).setImageBitmap(image);
                                    newProfilePicture = true;
                                } else {
                                    Toast.makeText(ProfileMainActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                    newProfilePicture = false;
                                }
                            }
                            flagDatinglCover = false;
                        } else if (flagBusinessCover == true) {
                            if (imageview_cover_business != null) {
                                IMAGE_SIZE = getScreenWidth(ProfileMainActivity.this) / 2;
                                if (IMAGE_SIZE <= 0)
                                    IMAGE_SIZE = 400;
                                Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                if (image != null) {
                                    (imageview_cover_business).setImageBitmap(image);
                                    newProfilePicture = true;
                                } else {
                                    Toast.makeText(ProfileMainActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                    newProfilePicture = false;
                                }
                            }
                            flagBusinessCover = false;
                        } else
                            Toast.makeText(ProfileMainActivity.this, "Image View is null !!", Toast.LENGTH_SHORT).show();
                        newProfilePicture = false;
                    }
                }
                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(intent);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    setImage(resultUri);
                    if (flagSocialPicApi == true) {
                        if (SOCIAL_PROFILE_PICTURE != null) {
                            user_d = AppSettings.getLoginUserId();
                            catagory_id = "1";

                            descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), user_d);
                            descriptionCatId = RequestBody.create(MediaType.parse("text/plain"), catagory_id);
                            post_image = new File(SOCIAL_PROFILE_PICTURE);
                            requestPhotoFile = RequestBody.create(MediaType.parse("image/*"), post_image);
                            postImageBody = MultipartBody.Part.createFormData("profile_pic", post_image.getName(), requestPhotoFile);
                            uploadSocialProfilePhoto(descriptionUserId, descriptionCatId, postImageBody);

                            flagSocialPicApi = false;
                        }
                    }
                    if (flagDatinglPicApi == true) {
                        if (DATING_PROFILE_PICTURE != null) {
                            user_d = AppSettings.getLoginUserId();
                            catagory_id = "2";

                            descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), user_d);
                            descriptionCatId = RequestBody.create(MediaType.parse("text/plain"), catagory_id);
                            post_image = new File(DATING_PROFILE_PICTURE);
                            requestPhotoFile = RequestBody.create(MediaType.parse("image/*"), post_image);
                            postImageBody = MultipartBody.Part.createFormData("profile_pic", post_image.getName(), requestPhotoFile);
                            uploadDatingProfilePhoto(descriptionUserId, descriptionCatId, postImageBody);

                            flagDatinglPicApi = false;

                        }
                    }
                    if (flagBusinessPicApi == true) {
                        if (BUSINESS_PROFILE_PICTURE != null) {
                            user_d = AppSettings.getLoginUserId();
                            String catagory_id1 = "3";

                            descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), user_d);
                            descriptionCatId = RequestBody.create(MediaType.parse("text/plain"), catagory_id1);
                            post_image = new File(BUSINESS_PROFILE_PICTURE);
                            requestPhotoFile = RequestBody.create(MediaType.parse("image/*"), post_image);
                            postImageBody = MultipartBody.Part.createFormData("profile_pic", post_image.getName(), requestPhotoFile);
                            uploadBusinessProfilePhoto(descriptionUserId, descriptionCatId, postImageBody);

                            flagBusinessPicApi = false;
                        }
                    }
                    if (flagSocialCover1 == true) {

                        user_d = AppSettings.getLoginUserId();
                        catagory_id = "1";
                        descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), user_d);
                        descriptionCatId = RequestBody.create(MediaType.parse("text/plain"), catagory_id);
                        post_image = new File(resultUri.getPath());
                        requestPhotoFile = RequestBody.create(MediaType.parse("image/*"), post_image);
                        postImageBody = MultipartBody.Part.createFormData("cover_pic", post_image.getName(), requestPhotoFile);
                        uploadSocialCoverPhoto(descriptionUserId, descriptionCatId, postImageBody);

                        flagSocialCover1 = false;
                    }
                    if (flagDatinglCover1 == true) {

                        user_d = AppSettings.getLoginUserId();
                        catagory_id = "2";
                        descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), user_d);
                        descriptionCatId = RequestBody.create(MediaType.parse("text/plain"), catagory_id);
                        post_image = new File(resultUri.getPath());
                        requestPhotoFile = RequestBody.create(MediaType.parse("image/*"), post_image);
                        postImageBody = MultipartBody.Part.createFormData("cover_pic", post_image.getName(), requestPhotoFile);
                        uploadDatingCoverPhoto(descriptionUserId, descriptionCatId, postImageBody);

                        flagDatinglCover1 = false;
                    }
                    if (flagBusinessCover1 == true) {

                        user_d = AppSettings.getLoginUserId();
                        catagory_id = "3";
                        descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), user_d);
                        descriptionCatId = RequestBody.create(MediaType.parse("text/plain"), catagory_id);
                        post_image = new File(resultUri.getPath());
                        requestPhotoFile = RequestBody.create(MediaType.parse("image/*"), post_image);
                        postImageBody = MultipartBody.Part.createFormData("cover_pic", post_image.getName(), requestPhotoFile);
                        uploadBusinessCoverPhoto(descriptionUserId, descriptionCatId, postImageBody);

                        flagBusinessCover1 = false;
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    newProfilePicture = false;
                }
                break;
            /***********************************************************************************************/
            case REQUEST_TAKE_GALLERY_VIDEO:

                if (resultCode == RESULT_OK) {
                    selectedVideoPath = getPath(intent.getData());

                    try {
                        if (selectedVideoPath == null) {
                            Log.e("selected video path = null!", selectedVideoPath);
                            newVideoBio = false;

                        } else {
                            Log.e("selected video path = null!----------------->", selectedVideoPath);

                            newVideoBio = true;
                            copyVideoFile(selectedVideoPath, BUSINESS_VIDEO_BIO);
                            createVideoThumbnail(BUSINESS_VIDEO_BIO);
/*
                            *
                             * try to do something there
                             * selectedVideoPath is path to the selected video*/

                        }
                    } catch (Exception e) {
                        //#debug
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ProfileMainActivity.this, "You haven't picked an Video", Toast.LENGTH_LONG).show();
                    newVideoBio = false;
                }

                break;

            case CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (BUSINESS_VIDEO_BIO != null) {
                        newVideoBio = true;
                        createVideoThumbnail(BUSINESS_VIDEO_BIO);
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    newVideoBio = false;
                    Toast.makeText(ProfileMainActivity.this, "User cancelled the video capture.",
                            Toast.LENGTH_LONG).show();
                } else {
                    newVideoBio = false;
                    Toast.makeText(ProfileMainActivity.this, "Video capture failed.",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }


    }

    private void changeOrientation(int imageViewID, String imageName) {
        int IMAGE_SIZE = 0;
        int rotate = 0;
        imageFile = new File(PROFILE_PICTURES + File.separator + imageName + ".jpg");

        if (imageName != null) {
            Uri imageUri = Uri.fromFile(imageFile);

            try {
                ProfileMainActivity.this.getContentResolver().notifyChange(imageUri, null);

                ExifInterface exif = new ExifInterface(
                        imageFile.getPath());

                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;
                }
                Log.v(LOG_TAG, "Exif orientation: " + orientation);


                /****** Image rotation ****/
                Matrix matrix = new Matrix();
                matrix.postRotate(rotate);

                options = new BitmapFactory.Options();
                options.inSampleSize = 5;


                switch (imageViewID) {
                    case R.id.imageView_profile_social:
                        ImageViewIDWhileCroppingImage = R.id.imageView_profile_social;
                        break;
                    case R.id.imageView_profile_dating:
                        ImageViewIDWhileCroppingImage = R.id.imageView_profile_dating;
                        break;
                    case R.id.imageView_profile_business:
                        ImageViewIDWhileCroppingImage = R.id.imageView_profile_business;
                        break;
                    case R.id.imageview_cover_social:
                        ImageViewIDWhileCroppingImage = R.id.imageview_cover_social;
                        break;
                    case R.id.imageview_cover_dating:
                        ImageViewIDWhileCroppingImage = R.id.imageview_cover_dating;
                        break;
                    case R.id.imageview_cover_business:
                        ImageViewIDWhileCroppingImage = R.id.imageview_cover_business;
                        break;
                    default:
                        ImageViewIDWhileCroppingImage = 0;
                        break;
                }
                CropImage.activity(imageUri)
                        .start(ProfileMainActivity.this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(ProfileMainActivity.this, "Image Path is null !!", Toast.LENGTH_SHORT).show();
            newProfilePicture = false;
        }
    }

    private void saveImageAndShowImageView(int imageViewID, String imageName, Intent intent) {
        int IMAGE_SIZE = 0;

        // Get the Image from data
        Uri selectedImage = intent.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        // Get the cursor
        Cursor cursor = ProfileMainActivity.this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        // Move to first row
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String imgDecodableString = cursor.getString(columnIndex);
        cursor.close();

        String outputImageFile = PROFILE_PICTURES + File.separator + imageName + ".jpg";

        if (outputImageFile != null) {
            try {
                if (!imgDecodableString.equals(outputImageFile)) {

                    switch (imageViewID) {
                        case R.id.imageView_profile_social:
                            ImageViewIDWhileCroppingImage = R.id.imageView_profile_social;
                            break;
                        case R.id.imageView_profile_dating:
                            ImageViewIDWhileCroppingImage = R.id.imageView_profile_dating;
                            break;
                        case R.id.imageView_profile_business:
                            ImageViewIDWhileCroppingImage = R.id.imageView_profile_business;
                            break;
                        case R.id.imageview_cover_social:
                            ImageViewIDWhileCroppingImage = R.id.imageview_cover_social;
                            break;
                        case R.id.imageview_cover_dating:
                            ImageViewIDWhileCroppingImage = R.id.imageview_cover_dating;
                            break;
                        case R.id.imageview_cover_business:
                            ImageViewIDWhileCroppingImage = R.id.imageview_cover_business;
                            break;

                        default:
                            ImageViewIDWhileCroppingImage = 0;
                            break;
                    }
                    CropImage.activity(selectedImage)
                            .start(ProfileMainActivity.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(ProfileMainActivity.this, getResources().getString(R.string.cant_copy_image), Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(ProfileMainActivity.this, "Image Path is null !!", Toast.LENGTH_SHORT).show();
            newProfilePicture = false;
        }
    }

    private void setImage(Uri imageURI) {

        String imageName = "";
        String outPuFilePath = "";
        File file;

        try {
            switch (ImageViewIDWhileCroppingImage) {
                case R.id.imageView_profile_social:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_social);
                    outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";
                    copyFile(imageURI.getPath(), outPuFilePath);
                    file = new File(outPuFilePath);
                    if (file.exists()) {
                        Uri imageUri = Uri.fromFile(file);
                        imageView_profile_social.setImageDrawable(null);
//                        imageView_profile_social.setImageURI(imageUri);
                        newProfilePicture = true;
                        imageFile = file;
                    } else {
                        Toast.makeText(ProfileMainActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        newProfilePicture = false;
                    }
                    break;
                case R.id.imageView_profile_dating:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_dating);
                    outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";
                    copyFile(imageURI.getPath(), outPuFilePath);
                    file = new File(outPuFilePath);
                    if (file.exists()) {
                        Uri imageUri = Uri.fromFile(file);
                        imageView_profile_dating.setImageDrawable(null);
//                        imageView_profile_dating.setImageURI(imageUri);
                        newProfilePicture = true;
                        imageFile = file;
                    } else {
                        Toast.makeText(ProfileMainActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        newProfilePicture = false;
                    }
                    break;
                case R.id.imageView_profile_business:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_business);
                    outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";
                    copyFile(imageURI.getPath(), outPuFilePath);
                    file = new File(outPuFilePath);
                    if (file.exists()) {
                        Uri imageUri = Uri.fromFile(file);
                        imageView_profile_business.setImageDrawable(null);
//                        imageView_profile_business.setImageURI(imageUri);
                        newProfilePicture = true;
                        imageFile = file;
                    } else {
                        Toast.makeText(ProfileMainActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        newProfilePicture = false;
                    }
                    break;
                case R.id.imageview_cover_social:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_social);
                    outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";
                    copyFile(imageURI.getPath(), outPuFilePath);
                    file = new File(outPuFilePath);
                    if (file.exists()) {
                        Uri imageUri = Uri.fromFile(file);
                        imageview_cover_social.setImageDrawable(null);
//                        imageview_cover_social.setImageURI(imageUri);
                        newProfilePicture = true;
                        imageFile = file;
                    } else {
                        Toast.makeText(ProfileMainActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        newProfilePicture = false;
                    }
                    break;
                case R.id.imageview_cover_dating:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_dating);
                    outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";
                    copyFile(imageURI.getPath(), outPuFilePath);
                    file = new File(outPuFilePath);
                    if (file.exists()) {
                        Uri imageUri = Uri.fromFile(file);
                        imageview_cover_dating.setImageDrawable(null);
//                        imageview_cover_dating.setImageURI(imageUri);
                        newProfilePicture = true;
                        imageFile = file;
                    } else {
                        Toast.makeText(ProfileMainActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        newProfilePicture = false;
                    }
                    break;
                case R.id.imageview_cover_business:
                    imageName = ProfileMainActivity.this.getString(R.string.profile_avatar_business);
                    outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";
                    copyFile(imageURI.getPath(), outPuFilePath);
                    file = new File(outPuFilePath);
                    if (file.exists()) {
                        Uri imageUri = Uri.fromFile(file);
                        imageview_cover_business.setImageDrawable(null);
//                        imageview_cover_business.setImageURI(imageUri);
                        newProfilePicture = true;
                        imageFile = file;
                    } else {
                        Toast.makeText(ProfileMainActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        newProfilePicture = false;
                    }
                    break;
            }


        } catch (Exception exception) {
            exception.printStackTrace();
        }
        ImageViewIDWhileCroppingImage = 0;
    }


    /****************upload image*************/

    /*************
     * Post Image
     **************/

    private void uploadSocialProfilePhoto(RequestBody descriptionUserId, RequestBody profile_cat_id, MultipartBody.Part postImageBody) {
        ProgressDialog progressDialog = new CustomProgressDialog(ProfileMainActivity.this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ProfilePictureResponse> callUploadVideo = requestInterface.set_profileImage(descriptionUserId, profile_cat_id, postImageBody);
            callUploadVideo.enqueue(new Callback<ProfilePictureResponse>() {
                @Override
                public void onResponse(Call<ProfilePictureResponse> call, Response<ProfilePictureResponse> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (null != response) {

                        Picasso.with(ProfileMainActivity.this)
                                .load(PROFILE_PICTURE_PATH + "/" + response.body().getUser_data().get(0).getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_social);
                        RequestBody descriptionCatIds = RequestBody.create(MediaType.parse("text/plain"), "1");
//                        uploadMultipleProfilePhoto(descriptionUserId, descriptionCatIds, postImageBody);
                        AppSettings.setProfileCatPicPathOne(PROFILE_PICTURE_PATH + "/" + response.body().getUser_data().get(0).getProfile_picture());
                        BaseDrawerActivity.updateDrawer();
                        Toast.makeText(ProfileMainActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<ProfilePictureResponse> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(ProfileMainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadDatingProfilePhoto(RequestBody descriptionUserId, RequestBody profile_cat_id, MultipartBody.Part postImageBody) {
        ProgressDialog progressDialog = new CustomProgressDialog(ProfileMainActivity.this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ProfilePictureResponse> callUploadVideo = requestInterface.set_profileImage(descriptionUserId, profile_cat_id, postImageBody);
            callUploadVideo.enqueue(new Callback<ProfilePictureResponse>() {
                @Override
                public void onResponse(Call<ProfilePictureResponse> call, Response<ProfilePictureResponse> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (null != response) {

                        Picasso.with(ProfileMainActivity.this)
                                .load(PROFILE_PICTURE_PATH + "/" + response.body().getUser_data().get(0).getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_dating);
                        RequestBody descriptionCatIds = RequestBody.create(MediaType.parse("text/plain"), "2");
//                        uploadMultipleProfilePhoto(descriptionUserId, descriptionCatIds, postImageBody);
                        AppSettings.setProfileCatPicPathTwo(PROFILE_PICTURE_PATH + "/" + response.body().getUser_data().get(0).getProfile_picture());
                        Toast.makeText(ProfileMainActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();

                    }


                }

                @Override
                public void onFailure(Call<ProfilePictureResponse> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(ProfileMainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadBusinessProfilePhoto(RequestBody descriptionUserId, RequestBody profile_cat_id, MultipartBody.Part postImageBody) {
        ProgressDialog progressDialog = new CustomProgressDialog(ProfileMainActivity.this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ProfilePictureResponse> callUploadVideo = requestInterface.set_profileImage(descriptionUserId, profile_cat_id, postImageBody);
            callUploadVideo.enqueue(new Callback<ProfilePictureResponse>() {
                @Override
                public void onResponse(Call<ProfilePictureResponse> call, Response<ProfilePictureResponse> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (null != response) {

                        Picasso.with(ProfileMainActivity.this)
                                .load(PROFILE_PICTURE_PATH + "/" + response.body().getUser_data().get(0).getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageView_profile_business);
                        RequestBody descriptionCatIds = RequestBody.create(MediaType.parse("text/plain"), "3");
//                        uploadMultipleProfilePhoto(descriptionUserId, descriptionCatIds, postImageBody);
                        AppSettings.setProfileCatPicPathThree(PROFILE_PICTURE_PATH + "/" + response.body().getUser_data().get(0).getProfile_picture());
                        Toast.makeText(ProfileMainActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();

                    }


                }

                @Override
                public void onFailure(Call<ProfilePictureResponse> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(ProfileMainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadSocialCoverPhoto(RequestBody descriptionUserId, RequestBody profile_cat_id, MultipartBody.Part postImageBody) {
        ProgressDialog progressDialog = new CustomProgressDialog(ProfileMainActivity.this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        try {
            Retrofit retrofit = ApiClient.getClientForOneMinuteTimeout();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseUpdateCover> callUploadVideo = requestInterface.set_profileCover(descriptionUserId, profile_cat_id, postImageBody);
            callUploadVideo.enqueue(new Callback<ResponseUpdateCover>() {
                @Override
                public void onResponse(Call<ResponseUpdateCover> call, Response<ResponseUpdateCover> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (null != response) {


                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(PROFILE_PICTURE_COVER + "/" + response.body().getUserData().get(0).getCoverPicture(), imageview_cover_social, options);
                        AppSettings.setProfileCatCoverPathOne(PROFILE_PICTURE_COVER + "/" + response.body().getUserData().get(0).getCoverPicture());
                        Toast.makeText(ProfileMainActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();

                    }


                }

                @Override
                public void onFailure(Call<ResponseUpdateCover> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(ProfileMainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadDatingCoverPhoto(RequestBody descriptionUserId, RequestBody profile_cat_id, MultipartBody.Part postImageBody) {
        ProgressDialog progressDialog = new CustomProgressDialog(ProfileMainActivity.this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        try {
            Retrofit retrofit = ApiClient.getClientForOneMinuteTimeout();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseUpdateCover> callUploadVideo = requestInterface.set_profileCover(descriptionUserId, profile_cat_id, postImageBody);
            callUploadVideo.enqueue(new Callback<ResponseUpdateCover>() {
                @Override
                public void onResponse(Call<ResponseUpdateCover> call, Response<ResponseUpdateCover> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (null != response) {


                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(PROFILE_PICTURE_COVER + "/" + response.body().getUserData().get(0).getCoverPicture(), imageview_cover_dating, options);

                        AppSettings.setProfileCatCoverPathTwo(PROFILE_PICTURE_COVER + "/" + response.body().getUserData().get(0).getCoverPicture());
                        Toast.makeText(ProfileMainActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();

                    }


                }

                @Override
                public void onFailure(Call<ResponseUpdateCover> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(ProfileMainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadBusinessCoverPhoto(RequestBody descriptionUserId, RequestBody profile_cat_id, MultipartBody.Part postImageBody) {
        ProgressDialog progressDialog = new CustomProgressDialog(ProfileMainActivity.this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        try {
            Retrofit retrofit = ApiClient.getClientForOneMinuteTimeout();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseUpdateCover> callUploadVideo = requestInterface.set_profileCover(descriptionUserId, profile_cat_id, postImageBody);
            callUploadVideo.enqueue(new Callback<ResponseUpdateCover>() {
                @Override
                public void onResponse(Call<ResponseUpdateCover> call, Response<ResponseUpdateCover> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (null != response) {


                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(PROFILE_PICTURE_COVER + "/" + response.body().getUserData().get(0).getCoverPicture(), imageview_cover_business, options);
                        AppSettings.setProfileCatCoverPathThree(PROFILE_PICTURE_COVER + "/" + response.body().getUserData().get(0).getCoverPicture());
                        Toast.makeText(ProfileMainActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();

                    }


                }

                @Override
                public void onFailure(Call<ResponseUpdateCover> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(ProfileMainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*****************
     * Video Bio Code
     **************************/

    @SuppressLint("NewApi")
    private void checkForPermissionsAndShowOptionDialogVideo() {

        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!hasPermissions(ProfileMainActivity.this, PERMISSIONS)) {
            requestPermissions(PERMISSIONS, REQUEST_TAKE_GALLERY_VIDEO);
        } else {

            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            } else {
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.INTERNAL_CONTENT_URI);
            }
            takeVideo();
        }
    }


    private void takeVideo() {
        try {
            final CharSequence[] items = {/*"Record Video",*/ "Choose from Gallery", "Cancel"};

            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(ProfileMainActivity.this);
            builder.setTitle("Add Video!");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {

                   /* if (items[item].equals("Record Video")) {
                        recordCamera();
                        dialog.dismiss();

                    } else*/
                    if (items[item].equals("Choose from Gallery")) {
                        pickVideo();
                        dialog.dismiss();
                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }

                }
            });
            builder.show();
        } catch (Exception ee) {
            ee.printStackTrace();
        }


    }

    public void pickVideo() {
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("video/*");
        intent.putExtra("return-data", true);
        startActivityForResult(intent, REQUEST_TAKE_GALLERY_VIDEO);
    }


    public void recordCamera() {

        intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        // create a file to save the video
        recordedFileUri = getOutputMediaFileUri(MEDIA_TYPE_RECORD_VIDEO);

        // set the image file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, recordedFileUri);

        // set the video image quality to high
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        // start the Video Capture Intent
        startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
    }


    private Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type) {

        File dir = new File(PROFILE_PICTURES);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        File mediaFile = null;

        if (type == MEDIA_TYPE_RECORD_VIDEO) {
            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(BUSINESS_VIDEO_BIO);


        } else {
            return null;
        }

        return mediaFile;
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = ProfileMainActivity.this.managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }

    public void copyVideoFile(String selectedImagePath, String outputFilePath) throws IOException {
        InputStream in = new FileInputStream(selectedImagePath);
        OutputStream out = new FileOutputStream(outputFilePath);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();

//        Toast.makeText(ProfileMainActivity.this, "Video Transferred", Toast.LENGTH_LONG).show();
    }


    public void createVideoThumbnail(String filePath) {
        Bitmap bmThumbnail;


        // MINI_KIND: 512 x 384 thumbnail
        bmThumbnail = ThumbnailUtils.createVideoThumbnail(filePath,
                MediaStore.Images.Thumbnails.MINI_KIND);
        if (bmThumbnail != null) {
            try {

                bmThumbnail.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(BUSINESS_VIDEO_BIO_THUMB));
                imageVideoThumb.setImageBitmap(bmThumbnail);

                if (newVideoBio) {
                    if (flagVideoSocial == true) {
                        File videoFile = new File(BUSINESS_VIDEO_BIO);
                        File videoThumbFile = new File(BUSINESS_VIDEO_BIO_THUMB);
                        RequestBody requestVideoFile = RequestBody.create(MediaType.parse("video"), videoFile);
                        MultipartBody.Part videoBody = MultipartBody.Part.createFormData("profile_bio", videoFile.getName(), requestVideoFile);
                        RequestBody requestVideoThumbFile = RequestBody.create(MediaType.parse("image*/"), videoThumbFile);
                        MultipartBody.Part videoThumbBody = MultipartBody.Part.createFormData("bio_thumb", videoThumbFile.getName(), requestVideoThumbFile);
                        RequestBody descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), AppSettings.getLoginUserId());
                        RequestBody descriptionProfileCatId = RequestBody.create(MediaType.parse("text/plain"), "1");
                        uploadVideoBio(descriptionUserId, descriptionProfileCatId, videoBody, videoThumbBody);
                        flagVideoSocial = false;
                    } else if (flagVideoDating == true) {
                        File videoFile = new File(BUSINESS_VIDEO_BIO);
                        File videoThumbFile = new File(BUSINESS_VIDEO_BIO_THUMB);
                        RequestBody requestVideoFile = RequestBody.create(MediaType.parse("video"), videoFile);
                        MultipartBody.Part videoBody = MultipartBody.Part.createFormData("profile_bio", videoFile.getName(), requestVideoFile);
                        RequestBody requestVideoThumbFile = RequestBody.create(MediaType.parse("image*/"), videoThumbFile);
                        MultipartBody.Part videoThumbBody = MultipartBody.Part.createFormData("bio_thumb", videoThumbFile.getName(), requestVideoThumbFile);
                        RequestBody descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), AppSettings.getLoginUserId());
                        RequestBody descriptionProfileCatId = RequestBody.create(MediaType.parse("text/plain"), "2");
                        uploadVideoBio(descriptionUserId, descriptionProfileCatId, videoBody, videoThumbBody);
                        flagVideoDating = false;
                    } else if (flagVideoBusiness == true) {
                        File videoFile = new File(BUSINESS_VIDEO_BIO);
                        File videoThumbFile = new File(BUSINESS_VIDEO_BIO_THUMB);
                        RequestBody requestVideoFile = RequestBody.create(MediaType.parse("video"), videoFile);
                        MultipartBody.Part videoBody = MultipartBody.Part.createFormData("profile_bio", videoFile.getName(), requestVideoFile);
                        RequestBody requestVideoThumbFile = RequestBody.create(MediaType.parse("image*/"), videoThumbFile);
                        MultipartBody.Part videoThumbBody = MultipartBody.Part.createFormData("bio_thumb", videoThumbFile.getName(), requestVideoThumbFile);
                        RequestBody descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), AppSettings.getLoginUserId());
                        RequestBody descriptionProfileCatId = RequestBody.create(MediaType.parse("text/plain"), "3");
                        uploadVideoBio(descriptionUserId, descriptionProfileCatId, videoBody, videoThumbBody);
                        flagVideoBusiness = false;
                    }
                } else {
                    Toast.makeText(ProfileMainActivity.this, ProfileMainActivity.this.getResources().getString(R.string.video_error), Toast.LENGTH_SHORT).show();
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


    }


    private void uploadVideoBio(RequestBody user_id, RequestBody profile_cat_id, MultipartBody.Part profile_bio, MultipartBody.Part bio_thumb) {

        CustomProgressDialog progressDialog1 = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog1.show();
        try {
            Retrofit retrofit = ApiClient.getClientForOneMinuteTimeout();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ProfilePictureResponse> callUploadVideo = requestInterface.uploadSocialVideoBio(user_id, profile_cat_id, profile_bio, bio_thumb);
            callUploadVideo.enqueue(new Callback<ProfilePictureResponse>() {
                @Override
                public void onResponse(Call<ProfilePictureResponse> call, Response<ProfilePictureResponse> response) {
                    if (progressDialog1 != null && progressDialog1.isShowing())
                        progressDialog1.dismiss();
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                        Toast.makeText(ProfileMainActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
//                        getUserProfileData();
                    } else {
                        Toast.makeText(ProfileMainActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ProfilePictureResponse> call, Throwable t) {
                    if (progressDialog1 != null && progressDialog1.isShowing())
                        progressDialog1.dismiss();
                    if (t.getMessage() != null)
                        Toast.makeText(ProfileMainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception e) {
            if (progressDialog1 != null && progressDialog1.isShowing())
                progressDialog1.dismiss();
        }
    }

    public static void setSelectedProfileOfUser() {

        if (AppSettings.getProfileDefaultCategory() != null) {
            if (AppSettings.getProfileDefaultCategory().equals(AppConstants.PROFILE_CATEGORY_ONE_SOCIAL)) {
//                imageViewSocial.setImageResource(R.drawable.ic_social_checked);
//                imageViewDating.setImageResource(R.drawable.ic_dating_unchecked);
//                imageViewBusiness.setImageResource(R.drawable.ic_business_unchecked);
            } else if (AppSettings.getProfileDefaultCategory().equals(AppConstants.PROFILE_CATEGORY_TWO_DATING)) {
//                imageViewSocial.setImageResource(R.drawable.ic_social_unchecked);
//                imageViewDating.setImageResource(R.drawable.ic_dating_checked);
//                imageViewBusiness.setImageResource(R.drawable.ic_business_unchecked);
            } else if (AppSettings.getProfileDefaultCategory().equals(AppConstants.PROFILE_CATEGORY_THREE_BUSINESS)) {
//                imageViewSocial.setImageResource(R.drawable.ic_social_unchecked);
//                imageViewDating.setImageResource(R.drawable.ic_dating_unchecked);
//                imageViewBusiness.setImageResource(R.drawable.ic_business_checked);
            }
        }
    }

    private void setStatus(String profileCatId, String profileStatus) {
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseUpdateStatus> userPostsCall = loginRequest.setProfileStatus(AppSettings.getLoginUserId(), profileCatId, profileStatus);
        userPostsCall.enqueue(new Callback<ResponseUpdateStatus>() {
            @Override
            public void onResponse(Call<ResponseUpdateStatus> call, Response<ResponseUpdateStatus> response) {
                setData(response);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseUpdateStatus> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });


    }

    public void setData(Response<ResponseUpdateStatus> response) {
        if (response.body() != null) {
            ResponseUpdateStatus responseUpdateStatus = response.body();
            if (responseUpdateStatus.getStatus().equals("success")) {
                Toast.makeText(this, responseUpdateStatus.getMsg(), Toast.LENGTH_LONG).show();
            }


        }
    }


}