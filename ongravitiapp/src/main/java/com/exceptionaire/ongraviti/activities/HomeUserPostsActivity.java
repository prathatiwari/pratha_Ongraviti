package com.exceptionaire.ongraviti.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.HomeUserPostsNewAdapter;
import com.exceptionaire.ongraviti.activities.adapters.PostsCommentsAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.base.ShowHidePasswordEditText;
import com.exceptionaire.ongraviti.activities.myorbit.AddPeopleToOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.listener_interface.OnLoadMoreListener;
import com.exceptionaire.ongraviti.model.ArrayOfLikes;
import com.exceptionaire.ongraviti.model.Array_of_comment;
import com.exceptionaire.ongraviti.model.HomeUserPosts;
import com.exceptionaire.ongraviti.model.LikeUnlike;
import com.exceptionaire.ongraviti.model.Postinfo;
import com.exceptionaire.ongraviti.model.ResponseGetCommentsByPostID;
import com.exceptionaire.ongraviti.model.ResponseUpdateStatus;
import com.exceptionaire.ongraviti.model.ResponseUserPostList;
import com.exceptionaire.ongraviti.model.Set_Comment;
import com.exceptionaire.ongraviti.model.deletePostResponse;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.AnimationUtils;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.exceptionaire.ongraviti.utilities.LocationService;
import com.squareup.picasso.Picasso;

import org.apache.http.protocol.HTTP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;


/**
 * Created by root on 18/1/17.
 */

public class HomeUserPostsActivity extends BaseDrawerActivity implements View.OnClickListener, AbsListView.OnScrollListener, AppConstants, com.android.volley.Response.Listener, com.android.volley.Response.ErrorListener {

    private RecyclerView recyclerViewPosts;
    private HomeUserPostsNewAdapter homeUserPostsAdapter;
    private Context context;
    private TextView header;
    private CustomProgressDialog progressDialog;

//    private NotificationsListAdapter notificationListAdapter;

    private View rootView;

    String TAG = "ActivityHome";
    private SwipeRefreshLayout swipe_refresh_layout;
    private Boolean inProgress = false;
    private ArrayList<Postinfo> list_posts = new ArrayList<>();
    private int LastListItem = 0;
    EditText et_comment;
    private int currentScrollState, currentFirstVisibleItem, currentVisibleItemCount, totalItemCount;
    private Boolean ifListUpdated = false;
    private Boolean isFromRefresh = false;
    private Boolean isLoadingMore = false;
    private boolean loadingMore = false;
    private Boolean isCommentShow = false;
    private Intent intent;
    TextView tv_msg;
    private boolean newProfilePicture = false;
    private Uri recordedFileUri;
    private String selectedVideoPath, selectedVideoUrl;
    private boolean newVideoBio = false;
    Button btn_send;
    private ImageView relativeImage, relativeVideo, image_view_mood;
    private RelativeLayout relative_text_post;
    private Button add_orbit;
    private ArrayList<ArrayOfLikes> likeArray = new ArrayList<>();
    private ArrayList<Array_of_comment> commentArray = new ArrayList<>();
    private ArrayList<Postinfo> postArray = new ArrayList<>();
    private ArrayList<HomeUserPosts> newHomePost = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_home_user_post, frameLayout);
        rootView = findViewById(R.id.content_main);
        arcMenu.setVisibility(View.VISIBLE);
//        Log.d("hh", AppSettings.getLoginUserId());
        context = this;
        initViews();
        startService(new Intent(getBaseContext(), LocationService.class));

        homeUserPostsAdapter = new HomeUserPostsNewAdapter(list_posts, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeUserPostsActivity.this, OrientationHelper.VERTICAL, false);
        recyclerViewPosts.setLayoutManager(linearLayoutManager);
        recyclerViewPosts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPosts.setAdapter(homeUserPostsAdapter);
        recyclerViewPosts.setHasFixedSize(true);
        recyclerViewPosts.setItemViewCacheSize(10);
        recyclerViewPosts.setDrawingCacheEnabled(true);
        recyclerViewPosts.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        homeUserPostsAdapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                recyclerViewPosts.post(new Runnable() {
                    @Override
                    public void run() {
                        loadMore(list_posts.size()); // a method which requests remote data
                    }
                });
            }
        });
        if (isNetworkAvailable()) {
            getUserHomePostData("0");
        } else {
            CommonDialogs.showMessageDialog(HomeUserPostsActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
        add_orbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AddPeopleToOrbitActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    private void loadMore(int position) {
        if (!inProgress) {
            isLoadingMore = true;
            if (isNetworkAvailable()) {
                list_posts.add(new Postinfo("load"));
                Log.i("Added Item ---->", "added =" + position);
//                homeUserPostsAdapter.setListUserPost(list_posts, position);
                homeUserPostsAdapter.notifyItemInserted(position);
                getUserHomePostData("" + position);
            } else {
                isLoadingMore = false;
                Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItemCount = totalItemCount;
        Log.d(TAG, "## onScroll firstVisibleItem : " + firstVisibleItem);
        Log.d(TAG, "## onScroll plus : " + firstVisibleItem + visibleItemCount);
        Log.d(TAG, "## onScroll LastListItem : " + LastListItem);
    }

    private void isScrollCompleted() {
        Log.d(TAG, "## isScrollCompleted loadingMore  : " + loadingMore);

        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE && this.totalItemCount == (currentFirstVisibleItem + currentVisibleItemCount)) {

            if (!loadingMore) {
                loadingMore = true;
                ifListUpdated = true;
                //   getUserHomePostData(String.valueOf(LastListItem));
            }
        }
    }

    public void initViews() {
        //activity name
        header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText(getResources().getString(R.string.my_world));
        tv_msg = (TextView) findViewById(R.id.tv_msg);
        recyclerViewPosts = (RecyclerView) findViewById(R.id.list_view_userpost);
        swipe_refresh_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        add_orbit = (Button) findViewById(R.id.add_orbit);
        image_view_mood = (ImageView) findViewById(R.id.image_view_mood);
        relativeImage = (ImageView) findViewById(R.id.relativeImage);
        relativeVideo = (ImageView) findViewById(R.id.relativeVideo);
        relative_text_post = (RelativeLayout) findViewById(R.id.relative_text_post);

        relativeImage.setOnClickListener(this);
        relativeVideo.setOnClickListener(this);
        relative_text_post.setOnClickListener(this);
        image_view_mood.setOnClickListener(this);

        if (AppSettings.getProfilePicturePath() != null && !AppSettings.getProfilePicturePath().isEmpty()) {
            Picasso.with(context).load(AppSettings.getProfilePicturePath()).placeholder(R.drawable.profile).into((RoundedImageView) findViewById(R.id.post_profile_pic));
            if (!AppSettings.getProfilePicturePath().contains("https://graph")) {
                Picasso.with(this)
                        .load(AppSettings.getProfilePicturePath())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into((RoundedImageView) findViewById(R.id.post_profile_pic));
            } else if (AppSettings.getProfilePicturePath().contains("https://graph")) {
                String[] separated = AppSettings.getProfilePicturePath().split("https://graph");
                String pathFB = separated[1];
                Picasso.with(this)
                        .load("https://graph" + pathFB)
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into((RoundedImageView) findViewById(R.id.post_profile_pic));
            }

        }

        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isFromRefresh = true;
                getUserHomePostData("0");
            }
        });
        swipe_refresh_layout.setVisibility(View.VISIBLE);
    }


    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    private void getUserHomePostData(final String post_id) {

        if (!isLoadingMore && !isFromRefresh) { //is user do pull to refresh to update the list, we dont need to show progress bar
            showProgressBar();
        }
        loadingMore = true;
        inProgress = true;

        String interestCategories;
        if (AppSettings.getInterestCategories() != null) {
            interestCategories = AppSettings.getInterestCategories();
            Log.d("AppSettings.getInte", AppSettings.getInterestCategories());
        } else {
            interestCategories = "";
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseUserPostList> userPostsCall = loginRequest.get_user_posts(AppSettings.getLoginUserId(), interestCategories, post_id);
        userPostsCall.enqueue(new Callback<ResponseUserPostList>() {
            @Override
            public void onResponse(Call<ResponseUserPostList> call, Response<ResponseUserPostList> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                isFromRefresh = false;
                isLoadingMore = false;
                inProgress = false;
                loadingMore = false;
                ArrayList<Postinfo> list_postsNew = null;
                if (response != null && response.body() != null) {
                    if (response.body() != null && response.body().getNextpoststart() != null) {
                        LastListItem = Integer.parseInt(response.body().getNextpoststart());
                        if (response.body().getPostinfo() != null)
                            list_postsNew = response.body().getPostinfo();
                        tv_msg.setVisibility(View.GONE);
                        if (list_postsNew != null && list_postsNew.size() > 0) {
                            if (post_id.equalsIgnoreCase("0")) {
                                list_posts.clear();
                                list_posts.addAll(list_postsNew);
//                            homeUserPostsAdapter.setListUserPost(list_posts);
                                homeUserPostsAdapter.notifyDataSetChanged();
                                if (LastListItem != 0) {
                                    homeUserPostsAdapter.notifyDataChanged();
                                }
                                if (ifListUpdated) {
                                    recyclerViewPosts.smoothScrollToPosition(0);
                                }
                                swipe_refresh_layout.setRefreshing(false);
                            } else {
                                list_posts.remove(list_posts.size() - 1);

                                list_posts.addAll(list_postsNew);
//                            homeUserPostsAdapter.setListUserPost(list_posts);
                                homeUserPostsAdapter.notifyDataSetChanged();
                                if (LastListItem != 0) {
                                    homeUserPostsAdapter.notifyDataChanged();
                                }
                                if (ifListUpdated) {
                                    recyclerViewPosts.smoothScrollToPosition(0);
                                }
                                swipe_refresh_layout.setRefreshing(false);
                            }
                        } else {
                            recyclerViewPosts.setVisibility(View.GONE);
                            swipe_refresh_layout.setVisibility(View.GONE);
                        }
                        if (isCommentShow) {
                            isCommentShow = false;
//                                showCommentDialog(latest_post_id, latest_position);
                        }
                    } else {
                        tv_msg.setVisibility(View.VISIBLE);
                        if (!post_id.equalsIgnoreCase("0")) {
                            list_posts.remove(list_posts.size() - 1);
                            homeUserPostsAdapter.notifyItemRemoved(list_posts.size() - 1);
                        }
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseUserPostList> call, Throwable t) {
                inProgress = false;
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });


    }

    private void getUserHomePostData1(final String post_id) {

        loadingMore = true;
        inProgress = true;
        String interestCategories;
        if (AppSettings.getInterestCategories() != null) {
            interestCategories = AppSettings.getInterestCategories();
        } else {
            interestCategories = "";
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseUserPostList> userPostsCall = loginRequest.get_user_posts(AppSettings.getLoginUserId(), interestCategories, post_id);
        userPostsCall.enqueue(new Callback<ResponseUserPostList>() {
            @Override
            public void onResponse(Call<ResponseUserPostList> call, Response<ResponseUserPostList> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                isFromRefresh = false;
                isLoadingMore = false;
                inProgress = false;
                loadingMore = false;
                ArrayList<Postinfo> list_postsNew = null;
                if (response != null && response.body() != null) {
                    if (response.body() != null && response.body().getNextpoststart() != null) {
                        LastListItem = Integer.parseInt(response.body().getNextpoststart());
                        if (response.body().getPostinfo() != null)
                            list_postsNew = response.body().getPostinfo();
                        tv_msg.setVisibility(View.GONE);
                        if (list_postsNew != null && list_postsNew.size() > 0) {
                            if (post_id.equalsIgnoreCase("0")) {
                                list_posts.clear();
                                list_posts.addAll(list_postsNew);
//                            homeUserPostsAdapter.setListUserPost(list_posts);
                                homeUserPostsAdapter.notifyDataSetChanged();
                                if (LastListItem != 0) {
                                    homeUserPostsAdapter.notifyDataChanged();
                                }
                                if (ifListUpdated) {
                                    recyclerViewPosts.smoothScrollToPosition(0);
                                }
                                swipe_refresh_layout.setRefreshing(false);
                            } else {
                                list_posts.remove(list_posts.size() - 1);

                                list_posts.addAll(list_postsNew);
//                            homeUserPostsAdapter.setListUserPost(list_posts);
                                homeUserPostsAdapter.notifyDataSetChanged();
                                if (LastListItem != 0) {
                                    homeUserPostsAdapter.notifyDataChanged();
                                }
                                if (ifListUpdated) {
                                    recyclerViewPosts.smoothScrollToPosition(0);
                                }
                                swipe_refresh_layout.setRefreshing(false);
                            }
                        } else {
                            recyclerViewPosts.setVisibility(View.GONE);
                            swipe_refresh_layout.setVisibility(View.GONE);
                        }
                        if (isCommentShow) {
                            isCommentShow = false;
//                                showCommentDialog(latest_post_id, latest_position);
                        }
                    } else {
                        tv_msg.setVisibility(View.VISIBLE);
                        if (!post_id.equalsIgnoreCase("0")) {
                            list_posts.remove(list_posts.size() - 1);
                            homeUserPostsAdapter.notifyItemRemoved(list_posts.size() - 1);
                        }
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseUserPostList> call, Throwable t) {
                inProgress = false;
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });


    }


    private void setPostsComments(final String post_id, final String comment, String device_date) {
        if (isNetworkAvailable()) {
//            progressDialog = new CustomProgressDialog(this, "Saving Comment...");
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            progressDialog.show();

            try {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface commentRequest = retrofit.create(ApiInterface.class);
                Call<Set_Comment> userPostsCall = commentRequest.setCommentPosts(AppSettings.getLoginUserId(), post_id, comment, device_date);
                userPostsCall.enqueue(new Callback<Set_Comment>() {

                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onResponse(Call<Set_Comment> call, Response<Set_Comment> response) {

                        Log.d(TAG, "## setPostsComments :" + response.body().toString());
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            getPostComments(post_id, "0");
                        } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
//                            if (progressDialog != null && progressDialog.isShowing())
//                                progressDialog.dismiss();
                            CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Something went wrong...try again");
                        }
                    }

                    @Override
                    public void onFailure(Call<Set_Comment> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } catch (Exception e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
            }
        } else {
            CommonDialogs.showMessageDialog(HomeUserPostsActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    public void getPostComments(final String post_id, final String pageIndex) {
        if (isNetworkAvailable()) {
//            if (progressDialog == null || !progressDialog.isShowing()) {
//                progressDialog = new CustomProgressDialog(this, "Loading comments...");
//                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                progressDialog.show();
//            }

            try {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface commentRequest = retrofit.create(ApiInterface.class);
                Call<ResponseGetCommentsByPostID> userPostsCall = commentRequest.getCommentsByPostID(post_id, pageIndex);
                userPostsCall.enqueue(new Callback<ResponseGetCommentsByPostID>() {

                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onResponse(Call<ResponseGetCommentsByPostID> call, Response<ResponseGetCommentsByPostID> response) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                        Log.d(TAG, "## setPostsComments :" + response.body().toString());

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            showCommentDialog(response.body().getComment_list(), post_id);
//                            btn_send.setBackgroundColor(R.color.ongraviti_blue);
                            getUserHomePostData1("0");
                        } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                            if (response.body().getComment_list() != null)
                                CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Something went wrong...try again");
                            else
                                showCommentDialog(null, post_id);

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetCommentsByPostID> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } catch (Exception e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
            }
        } else {
            CommonDialogs.showMessageDialog(HomeUserPostsActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    public void saveUserPostLike(final String post_id, final int position, final HomeUserPostsNewAdapter.FeedTypeViewHolder holder) {
        if (isNetworkAvailable()) {
//            progressDialog = new CustomProgressDialog(this, "Please wait...");
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            progressDialog.show();
            Log.d(TAG, "## post_id : " + post_id);

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface loginRequest = retrofit.create(ApiInterface.class);
            Call<LikeUnlike> userPostsCall = loginRequest.setLikeUnlike(AppSettings.getLoginUserId(), post_id);
            userPostsCall.enqueue(new Callback<LikeUnlike>() {
                @Override
                public void onResponse(Call<LikeUnlike> call, Response<LikeUnlike> response) {
//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();

                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        homeUserPostsAdapter.addLike(position, holder, response.body().getLike_count(), response.body().getMsg());
                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), response.body().getMsg());
                    }
                }

                @Override
                public void onFailure(Call<LikeUnlike> call, Throwable t) {
//
//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();
                }
            });

        } else {
            CommonDialogs.showMessageDialog(HomeUserPostsActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    private Dialog commetsDialog = null;

    public void showCommentDialog(ArrayList<Array_of_comment> list_post_comment, String postID) {

        if (list_post_comment == null)
            list_post_comment = new ArrayList<Array_of_comment>();

        if (commetsDialog == null) {
            commetsDialog = new Dialog(context);
            commetsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            commetsDialog.setContentView(R.layout.speakout_your_mind_comment_dialog);
            commetsDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            ListView lv = (ListView) commetsDialog.findViewById(R.id.lv_comments);
            et_comment = (EditText) commetsDialog.findViewById(R.id.et_comment);

            PostsCommentsAdapter postsCommentsAdapter = new PostsCommentsAdapter(list_post_comment, this);
            lv.setAdapter(postsCommentsAdapter);
            btn_send = (Button) commetsDialog.findViewById(R.id.btn_send);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());

            Log.d("currentDateandTime", currentDateandTime);
            btn_send.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceAsColor")
                @Override
                public void onClick(View v) {
                    if (validateForm(et_comment.getText().toString().trim())) {
                        try {
                            setPostsComments(postID, URLEncoder.encode(et_comment.getText().toString(), HTTP.UTF_8), currentDateandTime);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        et_comment.setText("");
//                        btn_send.setBackgroundColor(R.color.list_divider);
                    }
                }
            });

            commetsDialog.setCancelable(true);
            commetsDialog.setTitle("Comments");
            commetsDialog.show();
            commetsDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    commetsDialog.dismiss();
                    commetsDialog = null;
                }
            });
        } else {
            ListView lv = (ListView) commetsDialog.findViewById(R.id.lv_comments);
            PostsCommentsAdapter postsCommentsAdapter = new PostsCommentsAdapter(list_post_comment, this);
            lv.setAdapter(postsCommentsAdapter);

            ((EditText) commetsDialog.findViewById(R.id.et_comment)).setText("");
        }

        View view = commetsDialog.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //validation for email id and password(edittexts)
    public boolean validateForm(String comment) {
        boolean valid = true;
        try {
            et_comment.setError(null);
            if (comment.length() == 0) {
                setErrorMsg("Please enter Comment.", et_comment, true);
                valid = false;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return valid;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relativeImage:
                checkForPermissionsAndShowOptionDialogForImage();
                break;
            case R.id.relativeVideo:
                checkForPermissionsAndShowOptionDialogForVideo();
                break;
            case R.id.relative_text_post:
                startActivity(new Intent(this, PostMediaActivity.class));
                finish();
                break;
            case R.id.image_view_mood:
                startActivity(new Intent(this, PostMediaActivity.class));
                finish();
                break;
        }
    }


    /*************
     * Image
     ***********************/
    private void checkForPermissionsAndShowOptionDialogForImage() {
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if (!AppUtils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            selectImage();
        }
    }

    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    getCamera();
                } else if (options[item].equals("Choose from Gallery")) {
                    getGallery();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    public void getCamera() {
        if (!AppUtils.isDeviceSupportCamera(this)) {
            Toast.makeText(this,
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
        } else {
            File dumpFolder = new File(SPEED_DATING_APP_PATH);
            if (!dumpFolder.exists()) {
                dumpFolder.mkdirs();
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(SPEED_DATING_APP_PATH + File.separator + "post_image_path" + ".jpg")));
            startActivityForResult(intent, CAMERA_REQUEST);
        }
    }

    public void getGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY_REQUEST);
    }

    /************
     * Video
     ********************/


    @SuppressLint("NewApi")
    private void checkForPermissionsAndShowOptionDialogForVideo() {

        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!AppUtils.hasPermissions(getActivity(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, REQUEST_TAKE_GALLERY_VIDEO);
        } else {

            if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            } else {
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.INTERNAL_CONTENT_URI);
            }
            selectVideo();
        }
    }

    private void selectVideo() {

        final CharSequence[] options = {/*"Take Video",*/ "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                /*if (options[item].equals("Take Video")) {
                    recordVideo();
                } else*/
                if (options[item].equals("Choose from Gallery")) {
                    selectVideoFromGallery();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }


    public void selectVideoFromGallery() {

        intent.setAction(Intent.ACTION_PICK);
        intent.setType("video/*");
        intent.putExtra("return-data", true);
        startActivityForResult(intent, REQUEST_TAKE_GALLERY_VIDEO);
    }


    public void recordVideo() {

        intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        // create a file to save the video
        recordedFileUri = getOutputMediaFileUri(MEDIA_TYPE_RECORD_VIDEO);

        // set the image file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, recordedFileUri);

        // set the video image quality to high
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        // start the Video Capture Intent
        startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);

    }

    private static Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        File dir = new File(SPEED_DATING_APP_PATH);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        File mediaFile;

        if (type == MEDIA_TYPE_RECORD_VIDEO) {
            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(POST_VIDEO_PATH);

        } else {
            return null;
        }

        return mediaFile;
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }


    public void createVideoThumbnail(String filePath) {
        Bitmap bmThumbnail;

        // MINI_KIND: 512 x 384 thumbnail
        bmThumbnail = ThumbnailUtils.createVideoThumbnail(filePath,
                MediaStore.Images.Thumbnails.MINI_KIND);
        if (bmThumbnail != null) {
            try {
                bmThumbnail.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(POST_VIDEO_THUMB));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /*************************************************************/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        int IMAGE_SIZE = 0;
       /* if (bottomSheetDialog != null)
            bottomSheetDialog.dismiss();*/

        File dir = new File(SPEED_DATING_APP_PATH);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (POST_IMAGE_PATH != null) {
                        changeOrientation(Uri.fromFile(new File(POST_IMAGE_PATH)));

                        startActivity(new Intent(this, PostMediaActivity.class));
                        finish();

                    } else {
                        newProfilePicture = false;
//                    showMessageDialogInstance(
//                            getResources().getString(R.string.app_name), "Image Path is null !!");
                        Toast.makeText(this, "Image Path is null !!", Toast.LENGTH_SHORT).show();
                    }

                } else if (resultCode == RESULT_CANCELED) {
                    newProfilePicture = false;
                    Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                } else {
                    newProfilePicture = false;
                    Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                }
                break;

            case 10:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;

                    final Bitmap bitmap = BitmapFactory.decodeFile(POST_IMAGE_PATH,
                            options);
                    if (null != bitmap) {
                        if (!new File(POST_IMAGE_PATH).exists()) {
                            IMAGE_SIZE = getScreenWidth(this) / 2;
                            if (IMAGE_SIZE <= 0)
                                IMAGE_SIZE = 400;
                            Bitmap image = AnimationUtils.getScaledBitmap(
                                    bitmap, IMAGE_SIZE, IMAGE_SIZE);
                            if (image != null) {
//                                (imageView_sidePic).setImageBitmap(image);
                                // findViewById(R.id.imageTag).setVisibility(View.GONE);
                            } else {
//                                showMessageDialogInstance(
//                                        getResources().getString(
//                                                R.string.app_name),
//                                        "Invalid image , please pick another image.");
                                Toast.makeText(this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                            }
                        } else
//                            showMessageDialogInstance(getResources()
//                                            .getString(R.string.app_name),
//                                    "Image View is null !!");

                            Toast.makeText(this, "Image View is null !!", Toast.LENGTH_SHORT).show();
                    }

                }
            case GALLERY_REQUEST:
                if (resultCode == RESULT_OK) {
                    // Get the Image from data
                    Uri selectedImage = intent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    // Get the cursor
                    Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    if (SPEED_DATING_APP_PATH != null) {
                        try {
                            if (!imgDecodableString.equals(POST_IMAGE_PATH)) {
                                copyFile(imgDecodableString, POST_IMAGE_PATH);
                                startActivity(new Intent(this, PostMediaActivity.class));
                                finish();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
//                        showMessageDialogInstance(
//                                getResources().getString(R.string.app_name),
//                                getResources().getString(R.string.cant_copy_image));

                            Toast.makeText(this, getResources().getString(R.string.cant_copy_image), Toast.LENGTH_SHORT).show();
                        }
                        IMAGE_SIZE = getScreenWidth(this) / 2;
                        if (IMAGE_SIZE <= 0)
                            IMAGE_SIZE = 500;
                        Bitmap image = null;
                        try {
                            if (!imgDecodableString.equals(POST_IMAGE_PATH)) {
                                image = AnimationUtils.decodeSampledBitmapFromResourceMemOpt(
                                        new FileInputStream(new File(POST_IMAGE_PATH)), IMAGE_SIZE, IMAGE_SIZE);
                            } else {
                                image = BitmapFactory.decodeFile(POST_IMAGE_PATH);
                            }
                        } catch (OutOfMemoryError e) {
                            e.printStackTrace();
//                        showMessageDialogInstance(
//                                getResources().getString(R.string.app_name), "Image is too big");

                            Toast.makeText(this, "Image is too big", Toast.LENGTH_SHORT).show();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (image != null) {
//                            imageView_sidePic.setImageBitmap(image);
                            newProfilePicture = true;
                        } else {
//          1
                            Toast.makeText(this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                            newProfilePicture = false;
                        }
                    } else {
//                    showMessageDialogInstance(
//                            getResources().getString(R.string.app_name), "Image Path is null !!");
                        Toast.makeText(this, "Image Path is null !!", Toast.LENGTH_SHORT).show();
                        newProfilePicture = false;
                    }
                } else {
                    Toast.makeText(this, "You haven't picked an Image", Toast.LENGTH_LONG).show();
                    newProfilePicture = false;
                }

                break;
            case REQUEST_TAKE_GALLERY_VIDEO:

                if (resultCode == RESULT_OK) {
                    selectedVideoPath = getPath(intent.getData());

                    try {
                        if (selectedVideoPath == null) {
                            Log.e("video path = null!", selectedVideoPath);
                            newVideoBio = false;

                            finish();
                        } else {
                            Log.e("video path = null!>", selectedVideoPath);

                            newVideoBio = true;
                            copyFile(selectedVideoPath, POST_VIDEO_PATH);
                            createVideoThumbnail(POST_VIDEO_PATH);
                            startActivity(new Intent(this, PostMediaActivity.class));
                            finish();

                            /**
                             * try to do something there
                             * selectedVideoPath is path to the selected video
                             */
                        }
                    } catch (Exception e) {
                        //#debug
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "You haven't picked an Video", Toast.LENGTH_LONG).show();
                    newVideoBio = false;

                }

                break;

            case CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (POST_VIDEO_PATH != null) {
                        newVideoBio = true;
                        createVideoThumbnail(POST_VIDEO_PATH);
                        startActivity(new Intent(this, PostMediaActivity.class));
                        finish();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    newVideoBio = false;

                    Toast.makeText(this, "User cancelled the video capture.",
                            Toast.LENGTH_LONG).show();
                } else {
                    newVideoBio = false;

                    Toast.makeText(this, "Video capture failed.",
                            Toast.LENGTH_LONG).show();
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    private void changeOrientation(Uri imageUri) {
        int IMAGE_SIZE = 0;
        int rotate = 0;

        try {
            this.getContentResolver().notifyChange(imageUri, null);

            File imageFile = new File(POST_IMAGE_PATH);
            ExifInterface exif = new ExifInterface(
                    imageFile.getPath());

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
            Log.v(LOG_TAG, "Exif orientation: " + orientation);


            /****** Image rotation ****/
            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);


            options = new BitmapFactory.Options();
            options.inSampleSize = 5;
            Bitmap myBitmap = BitmapFactory.decodeFile(POST_IMAGE_PATH, options);
            myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(),
                    matrix, true);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    BitmapFactory.Options options;

    public void reportAbusePost(String userID, String postId, String postUserId) {
        showProgressBar();

        try {
            if (isNetworkAvailable()) {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface loginRequest = retrofit.create(ApiInterface.class);
                Call<deletePostResponse> userPostsCall = loginRequest.reportAbusePost(userID, postId, postUserId);
                userPostsCall.enqueue(new Callback<deletePostResponse>() {
                    @Override
                    public void onResponse(Call<deletePostResponse> call, Response<deletePostResponse> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            CommonDialogs.dialog_with_one_btn_without_title(HomeUserPostsActivity.this, response.body().getMsg());
                        } else {
                            CommonDialogs.dialog_with_one_btn_without_title(HomeUserPostsActivity.this, response.body().getMsg());
                        }
                    }

                    @Override
                    public void onFailure(Call<deletePostResponse> call, Throwable t) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                CommonDialogs.dialog_with_one_btn_without_title(HomeUserPostsActivity.this, getResources().getString(R.string.check_internet));
            }
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    public void setEditPost(String post_id, String visibility_status, String post_content, String emoji, String device_date) {
        CustomProgressDialog progressDialog;
        progressDialog = new CustomProgressDialog(context, context.getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseUpdateStatus> userPostsCall = loginRequest.setEditPost(AppSettings.getLoginUserId(), post_id, visibility_status, post_content, emoji, device_date);
        userPostsCall.enqueue(new Callback<ResponseUpdateStatus>() {
            @Override
            public void onResponse(Call<ResponseUpdateStatus> call, Response<ResponseUpdateStatus> response) {
                setData(response);
                progressDialog.dismiss();
                getUserHomePostData1("0");
            }

            @Override
            public void onFailure(Call<ResponseUpdateStatus> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });


    }

    public void setData(Response<ResponseUpdateStatus> response) {
        if (response.body() != null) {
            ResponseUpdateStatus responseUpdateStatus = response.body();
            if (responseUpdateStatus.getStatus().equals("success")) {
                Toast.makeText(context, responseUpdateStatus.getMsg(), Toast.LENGTH_LONG).show();
            }


        }
    }

}

