package com.exceptionaire.ongraviti.activities.chats;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.fragments.ImageCropActivity;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.OrbitContact;
import com.exceptionaire.ongraviti.model.ResponseOrbitContactList;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.paypal.android.sdk.payments.PayPalService;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by Laxmikant on 24/4/17.
 */

public class CreateGroupActivity extends BaseDrawerActivity {
    private static final String TAG = "Create Group Activity";
    private Context context;
    private ProgressDialog progressDialog;
    private List<OrbitContact> orbitFriends;
    private CreateGroupAdapter createGroupAdapter;
    private ListView list_contact;
    public static TextView tv_msg;
    private FrameLayout frame_search;
    private List<OrbitContact> selectedUsers = new ArrayList<>();
    private RoundedImageView roundedImageViewGroupPic;

    private static final int COUNTRYS_TRAVELLED_REQUEST_CODE = 100;
    private static final int CAMERA_CODE = 101;
    private static final int CAMERA_VIDEO_CODE = 102;

    private static final int REQUEST_PICK_IMAGE = 10011;
    private static final int REQUEST_SAF_PICK_IMAGE = 10012;
    private static final int REQUEST_PICK_VIDEO = 10013;
    public Uri mImageCaptureUri, imageUri;
    private boolean isEdit;
    String displayName;
    private File tempFile;
    String string;
    File getTempFile;
    private Object intentResult;
    private Intent intent;
    private StringBuilder stringBuilder = new StringBuilder();
    private int operation_code;
    private int show_picture_flag;
    String stringUri;
    private EditText editTextGroupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_create_group, frameLayout);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        TextView header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText("Create New Group");
        arcMenu.setVisibility(View.GONE);

        final EditText et_search = (EditText) findViewById(R.id.et_search);
        list_contact = (ListView) findViewById(R.id.list_view_contact);
        frame_search = (FrameLayout) findViewById(R.id.frame_search);
        tv_msg = (TextView) findViewById(R.id.tv_msg);

        context = this;
        createGroupAdapter = new CreateGroupAdapter(new ArrayList<OrbitContact>(), context);
        list_contact.setAdapter(createGroupAdapter);
        getOrbitFriendList();

        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                String text = et_search.getText().toString().toLowerCase(Locale.getDefault());
                createGroupAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            }
        });

        findViewById(R.id.btn_clear_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_search.setText("");
            }
        });

        editTextGroupName = (EditText) findViewById(R.id.editTextGroupName);

        findViewById(R.id.buttonCreateGroupDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextGroupName.getText() != null && !editTextGroupName.getText().toString().isEmpty() && !editTextGroupName.getText().toString().equalsIgnoreCase("")) {
                    selectedUsers.clear();
                    selectedUsers = createGroupAdapter.getSelectedUsers();
                    StringBuilder stringBuilder = new StringBuilder();
                    for (OrbitContact orbitContact : selectedUsers) {
                        if (orbitContact.getSelected() != null)
                            stringBuilder.append(orbitContact.getSelected() ? orbitContact.getQuickblox_id() + "," : "");
                    }
                    stringBuilder.append(stringUri + ",");
                    stringBuilder.append(editTextGroupName.getText().toString());

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", stringBuilder.toString());
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    new CommonDialogs().showMessageDialogNonStatic(CreateGroupActivity.this, getResources().getString(R.string.ongravity), "Please enter group name.");
                }
            }
        });
        roundedImageViewGroupPic = (RoundedImageView) findViewById(R.id.imageViewGroupPicture);
        roundedImageViewGroupPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageOption();
            }
        });
    }

    private void selectImageOption() {
        try {
            final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};

            AlertDialog.Builder builder = new AlertDialog.Builder(CreateGroupActivity.this);
            builder.setTitle("Add Photo!");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {

                    if (items[item].equals("Capture Photo")) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, CAMERA_CODE);

                    } else if (items[item].equals("Choose from Gallery")) {
                        /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                            startActivityForResult(new Intent(Intent.ACTION_GET_CONTENT).setType("image*//*"), REQUEST_PICK_IMAGE);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("image*//*");
                            startActivityForResult(intent, REQUEST_SAF_PICK_IMAGE);
                        }*/

                        startActivityForResult(new Intent(Intent.ACTION_GET_CONTENT).setType("image/*"), REQUEST_PICK_IMAGE);
                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Log.e("Create Group", "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private void getOrbitFriendList() {
        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(context, "Loading...");
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<ResponseOrbitContactList> retrofitCall = requestInterface.getUserOrbit(AppSettings.getLoginUserId());
        retrofitCall.enqueue(new Callback<ResponseOrbitContactList>() {
            @Override
            public void onResponse(Call<ResponseOrbitContactList> call, retrofit2.Response<ResponseOrbitContactList> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                try {
                    Log.e("TAG", response + " ");
                    if (response.body().getStatus().equalsIgnoreCase("success")) {

                        orbitFriends = new ArrayList<>(Arrays.asList(response.body().getOrbit_list()));

                        if (orbitFriends.size() > 0) {
                            frame_search.setVisibility(View.VISIBLE);
                            createGroupAdapter.setOrbitFriendList(orbitFriends);
                            tv_msg.setVisibility(View.GONE);
                            list_contact.setVisibility(View.VISIBLE);
                        } else {
                            tv_msg.setVisibility(View.VISIBLE);
                            list_contact.setVisibility(View.GONE);
                        }

                    } else if (response.body().getStatus().equalsIgnoreCase("error")) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        final Dialog dialog = CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "You dont have any people in your Orbit.");
                        dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseOrbitContactList> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.d(TAG, "## onActivityResult");
            if (resultCode != RESULT_OK) {
                return;
            } else if (data == null) {
                System.out.println("Camera");
            } else if (requestCode == REQUEST_PICK_IMAGE) {

                mImageCaptureUri = data.getData();
                Intent intent = new Intent(CreateGroupActivity.this, ImageCropActivity.class);
                intent.setData(mImageCaptureUri);
                if (!isEdit) {
                    intent.putExtra("action", 1);
                } else {
                    intent.putExtra("action", 2);
                }
                intent.putExtra("activity_name", 10);
                intent.putExtra("request_code", REQUEST_PICK_IMAGE);
                startActivityForResult(intent, 10);
                System.out.println("Gallery Image URI : " + mImageCaptureUri);

            } else if (requestCode == REQUEST_SAF_PICK_IMAGE) {

                mImageCaptureUri = data.getData();
                System.out.println("Gallery Image URI : " + mImageCaptureUri);
                Intent intent = new Intent(CreateGroupActivity.this, ImageCropActivity.class);
                intent.setData(mImageCaptureUri);
                if (!isEdit) {
                    intent.putExtra("action", 1);
                } else {
                    intent.putExtra("action", 2);
                }
                intent.putExtra("activity_name", 10);
                intent.putExtra("request_code", REQUEST_SAF_PICK_IMAGE);
                startActivityForResult(intent, 10);
            } else if (requestCode == CAMERA_CODE) {
                Uri cropURI = AppUtils.onCaptureImageResult(data);
                Intent intent = new Intent(CreateGroupActivity.this, ImageCropActivity.class);
                intent.setData(cropURI);
                if (!isEdit) {
                    intent.putExtra("action", 1);
                } else {
                    intent.putExtra("action", 2);
                }
                intent.putExtra("activity_name", 10);
                intent.putExtra("request_code", CAMERA_CODE);
                startActivityForResult(intent, 10);
            } else {
                intent = data;
                getIntentResult();
            }
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void getIntentResult() {
        stringBuilder = new StringBuilder();
        if (intent != null && intent.hasExtra("Edit")) {
            if (isNetworkAvailable()) {
                isEdit = true;
                show_picture_flag = 0;
                //getDatingProfile();
                //btsubmit.setText(getResources().getString(R.string.update));
            } else {
                CommonDialogs.dialog_with_one_btn_without_title(CreateGroupActivity.this, getResources().getString(R.string.check_internet));
            }

        } else if (intent != null && intent.hasExtra("Output") && intent.hasExtra("WithEdit")) {
            if (isNetworkAvailable()) {
                show_picture_flag = 1;
                //getDatingProfile();
                //btsubmit.setText(getResources().getString(R.string.update));
                //callbackInterface = this;
                imageUri = intent.getData();
                stringUri = imageUri.getPath();
                String user_id = AppSettings.getLoginUserId();
                operation_code = 0;
//                new CommonAsyncTask(CreateGroupActivity.this, callbackInterface)
//                        .execute(
//                                "0",
//                                AppConstants.SET_PROFILE_PICTURE,
//                                user_id,
//                                "1",
//                                stringUri);

            } else {
                CommonDialogs.dialog_with_one_btn_without_title(CreateGroupActivity.this, getResources().getString(R.string.check_internet));
            }
        } else if (intent != null && intent.hasExtra("Output") && intent.hasExtra("WithAdd")) {
            if (isNetworkAvailable()) {
                //callbackInterface = this;
                imageUri = intent.getData();
                stringUri = imageUri.getPath();
                String user_id = AppSettings.getLoginUserId();
                operation_code = 0;

                roundedImageViewGroupPic.setImageURI(imageUri);

//                new CommonAsyncTask(CreateGroupActivity.this, callbackInterface)
//                        .execute(
//                                "0",
//                                AppConstants.SET_PROFILE_PICTURE,
//                                user_id,
//                                "1",
//                                stringUri);
            } else {
                CommonDialogs.dialog_with_one_btn_without_title(CreateGroupActivity.this, getResources().getString(R.string.check_internet));
            }
        } else {
            isEdit = false;

        }
    }


}