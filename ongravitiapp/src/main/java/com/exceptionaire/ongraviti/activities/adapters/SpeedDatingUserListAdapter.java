package com.exceptionaire.ongraviti.activities.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.SelectUserActivity;
import com.exceptionaire.ongraviti.activities.VideoBioActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.listener_interface.OnLoadMoreListener;
import com.exceptionaire.ongraviti.model.ResponseSpeedDatingUserList;
import com.exceptionaire.ongraviti.model.ResponseSuccess;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_activities.OpponentsActivityVideo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by root on 22/3/17.
 */

public class SpeedDatingUserListAdapter extends RecyclerView.Adapter<SpeedDatingUserListAdapter.ViewHolder> implements AppConstants {

    private Context context;
    private Activity activity;
    private ArrayList<ResponseSpeedDatingUserList.User_list> speedDatingUserList;

    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean loading;

    LinearLayoutManager mLayoutManager;


    public SpeedDatingUserListAdapter(Activity activity, ArrayList<ResponseSpeedDatingUserList.User_list> speedDatingUserList, RecyclerView recyclerView) {
        this.activity = activity;
        this.speedDatingUserList = speedDatingUserList;
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);

        int count = mLayoutManager.getChildCount();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_list, parent, false);
        context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Picasso.with(context).load(speedDatingUserList.get(position).getEvent_video_thumb())
                .placeholder(R.drawable.alexandra_daddario_bg).fit().centerCrop().into(holder.user_bio_video_thumb);

        Picasso.with(context).load(speedDatingUserList.get(position).getThumb())
                .placeholder(R.drawable.profile).fit().centerCrop().into(holder.user_profile);

        holder.user_name.setText(speedDatingUserList.get(position).getC_name());
        holder.play_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VideoBioActivity.class);
                intent.putExtra("videopath", speedDatingUserList.get(position).getEvent_video_name());
                context.startActivity(intent);
            }
        });

        holder.buttonCall.setEnabled(true);

        if (speedDatingUserList.get(position).getSender_user_id() == null
                || speedDatingUserList.get(position).getSender_user_id().equalsIgnoreCase("")) {
            holder.buttonCall.setText("Send Request");
        } else if (speedDatingUserList.get(position).getSender_user_id() != null
                && speedDatingUserList.get(position).getReceiver_user_id() != null) {
            switch (speedDatingUserList.get(position).getEvent_flag()) {
                case "0":
                    if (speedDatingUserList.get(position).getSender_user_id().equalsIgnoreCase(AppSettings.getLoginUserId())) {
                        holder.buttonCall.setText("Requested");
                        holder.buttonCall.setEnabled(false);
                    } else
                        holder.buttonCall.setText("Accept Request");
                    break;
                case "1":
                    holder.buttonCall.setText("Call Now");
                    break;
                case "2":
                    holder.buttonCall.setText("Rejected");
                    holder.buttonCall.setEnabled(false);
                    break;
            }
        }

        holder.buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (speedDatingUserList.get(position).getEvent_flag()) {
                    case "":
                        sendSpeedDatingRequest(position, AppSettings.getLoginUserId(), speedDatingUserList.get(position).getUser_id(), AppSettings.getKeyEventId());
                        break;
                    case "0":
                        Dialog dialog = new CommonDialogs().dialogAcceptReject(activity, "", "Respond to " + speedDatingUserList.get(position).getC_name() + "'s speed dating request...");
                        dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                respondToSpeedDatingRequest(position, AppSettings.getLoginUserId(), speedDatingUserList.get(position).getUser_id(), AppSettings.getKeyEventId(), "1");
                            }
                        });
                        dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                respondToSpeedDatingRequest(position, AppSettings.getLoginUserId(), speedDatingUserList.get(position).getUser_id(), AppSettings.getKeyEventId(), "2");
                            }
                        });

                        break;
                    case "1":
                        Intent intent = new Intent(context, OpponentsActivityVideo.class);
                        intent.putExtra("BioImage", speedDatingUserList.get(position).getEvent_video_thumb());
                        intent.putExtra("userImage", speedDatingUserList.get(position).getProfile_picture());
                        intent.putExtra("userName", speedDatingUserList.get(position).getC_name());
                        intent.putExtra("user_id", speedDatingUserList.get(position).getUser_id());
                        intent.putExtra("opponent_qb_id", speedDatingUserList.get(position).getQuickblox_id());
                        intent.putExtra("opponent_qb_name", speedDatingUserList.get(position).getContact_number());
                        context.startActivity(intent);
                        break;
                    case "2":
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return speedDatingUserList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView user_bio_video_thumb, play_video;
        RoundedImageView user_profile;
        TextView user_name;
        Button buttonCall;

        public ViewHolder(View itemView) {
            super(itemView);

            user_bio_video_thumb = (ImageView) itemView.findViewById(R.id.user_bio_video_thumb);
            user_profile = (RoundedImageView) itemView.findViewById(R.id.user_profile);
            user_name = (TextView) itemView.findViewById(R.id.user_name);
            buttonCall = (Button) itemView.findViewById(R.id.button_call);
            play_video = (ImageView) itemView.findViewById(R.id.play_video);
        }
    }

    private void sendSpeedDatingRequest(int position, final String user_id, String receiver_user_id, String event_id) {

        ((SelectUserActivity) context).progressDialog = new CustomProgressDialog(context, context.getResources().getString(R.string.loading));
        ((SelectUserActivity) context).progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ((SelectUserActivity) context).progressDialog.show();

        try {
            Retrofit retrofit = ApiClient.getClient();
            final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseSuccess> getAllUserList = requestInterface.setEventRequest(user_id, receiver_user_id, event_id);
            getAllUserList.enqueue(new Callback<ResponseSuccess>() {
                @Override
                public void onResponse(Call<ResponseSuccess> call, Response<ResponseSuccess> response) {

                    if (((SelectUserActivity) context).progressDialog != null && ((SelectUserActivity) context).progressDialog.isShowing())
                        ((SelectUserActivity) context).progressDialog.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            if (speedDatingUserList.get(position) != null) {
                                ResponseSpeedDatingUserList.User_list user_list = speedDatingUserList.get(position);
                                user_list.setEvent_id(event_id);
                                user_list.setSender_user_id(user_id);
                                user_list.setReceiver_user_id(receiver_user_id);
                                user_list.setEvent_flag("0");
                            }
                            (SpeedDatingUserListAdapter.this).notifyItemChanged(position);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                    if (((SelectUserActivity) context).progressDialog != null && ((SelectUserActivity) context).progressDialog.isShowing())
                        ((SelectUserActivity) context).progressDialog.dismiss();
                }
            });


        } catch (Exception e) {
            if (((SelectUserActivity) context).progressDialog != null && ((SelectUserActivity) context).progressDialog.isShowing())
                ((SelectUserActivity) context).progressDialog.dismiss();
        }

    }

    private void respondToSpeedDatingRequest(int position, final String user_id, String receiver_user_id, String event_id, String event_flag) {

        ((SelectUserActivity) context).progressDialog = new CustomProgressDialog(context, context.getResources().getString(R.string.loading));
        ((SelectUserActivity) context).progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ((SelectUserActivity) context).progressDialog.show();

        Retrofit retrofit = ApiClient.getClient();
        final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<ResponseSuccess> getAllUserList = requestInterface.respondToEventRequest(user_id, receiver_user_id, event_id, event_flag);
        getAllUserList.enqueue(new Callback<ResponseSuccess>() {
            @Override
            public void onResponse(Call<ResponseSuccess> call, Response<ResponseSuccess> response) {

                if (((SelectUserActivity) context).progressDialog != null && ((SelectUserActivity) context).progressDialog.isShowing())
                    ((SelectUserActivity) context).progressDialog.dismiss();

                if (response.body() != null) {
                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                        if (speedDatingUserList.get(position) != null) {
                            ResponseSpeedDatingUserList.User_list user_list = speedDatingUserList.get(position);
                            user_list.setEvent_id(event_id);
                            user_list.setSender_user_id(user_id);
                            user_list.setReceiver_user_id(receiver_user_id);
                            user_list.setEvent_flag(event_flag);
                        }
                        (SpeedDatingUserListAdapter.this).notifyItemChanged(position);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                if (((SelectUserActivity) context).progressDialog != null && ((SelectUserActivity) context).progressDialog.isShowing())
                    ((SelectUserActivity) context).progressDialog.dismiss();
            }
        });
    }
}
