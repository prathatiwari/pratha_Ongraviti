package com.exceptionaire.ongraviti.activities.base;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.adapters.GridAdapter;
import com.exceptionaire.ongraviti.activities.adapters.HomeUserPostsNewAdapter;
import com.exceptionaire.ongraviti.activities.adapters.ImageAdapter;
import com.exceptionaire.ongraviti.activities.adapters.MultiSelectionAdapter;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.dialog.NormalDialog;
import com.exceptionaire.ongraviti.dialog.animation.BaseAnimatorSet;
import com.exceptionaire.ongraviti.dialog.animation.BounceEnter.BounceTopEnter;
import com.exceptionaire.ongraviti.dialog.animation.BounceEnter.SlideBottomExit;
import com.exceptionaire.ongraviti.dialog.listener.OnBtnClickL;
import com.exceptionaire.ongraviti.listener_interface.DialogOkClickListener;
import com.exceptionaire.ongraviti.model.Categories;
import com.exceptionaire.ongraviti.model.Combine;
import com.exceptionaire.ongraviti.model.PicData;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

/**
 * Created by Laxmikant on 1/3/16.
 */
public class CommonDialogs implements AppConstants {
    ImageLoader imageLoader = ImageLoader.getInstance();
    public static BaseAnimatorSet mBasIn;
    public static BaseAnimatorSet mBasOut;


    public static Dialog dialog_with_one_btn_without_title(Context context, String dialogMessage) {
        final Dialog dialog = new Dialog(context, R.style.DialogStyle);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_ok);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialogMessage != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_message)).setText(dialogMessage);

        dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }


    public ProgressDialog showProgressBar(Activity activity) {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        try {
            progressDialog.show();
        } catch (AndroidRuntimeException e) {
        }
        progressDialog.setCancelable(false);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static Dialog showAlertDialog(Activity activity, String dialogHeader, String dialogMessage) {
        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert_ok_cancel);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        if (dialogHeader != null)
            ((TextView) dialog.findViewById(R.id.textView_dialog_heading)).setText(dialogHeader);
        if (dialogMessage != null)
            ((TextView) dialog.findViewById(R.id.textView_dialog_alert_message)).setText(dialogMessage);

        dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }

    public static Dialog showMessageDialog(Context context, String dialogHeader, String dialogMessage) {
        final Dialog dialog = new Dialog(context, R.style.DialogStyle);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_ok);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialogHeader != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_header)).setText(dialogHeader);
        if (dialogMessage != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_message)).setText(dialogMessage);

        dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }

    public Dialog showMessageDialogNonStatic(Context context, String dialogHeader, String dialogMessage) {
        final Dialog dialog = new Dialog(context, R.style.DialogStyle);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_ok);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialogHeader != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_header)).setText(dialogHeader);
        if (dialogMessage != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_message)).setText(dialogMessage);

        dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }

    public void displayImageZoomDialog(Activity activity, String imageURL) {
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        }
        final Dialog dialog = new Dialog(activity, R.style.DialogZoomAnim);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(activity, R.color.white_transparent_70)));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_zoom_image);
        if (!imageURL.isEmpty()) {
            ImageView imageView = (ImageView) dialog.findViewById(R.id.scalable_imageview_zoom);
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showStubImage(R.drawable.placeholder)
                    .showImageForEmptyUri(R.drawable.progress_animation)
                    .resetViewBeforeLoading(true)
                    .cacheInMemory()
                    .cacheOnDisc()
                    .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                    .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                    .displayer(new SimpleBitmapDisplayer()) // default
                    .build();
            imageLoader.displayImage(imageURL, imageView, options);
        }
        dialog.findViewById(R.id.imageView_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        // Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        // This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void displayImageZoomDialog1(Activity activity, ArrayList<String> imageArray) {

        final Dialog dialog = new Dialog(activity, R.style.DialogZoomAnim);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(activity, R.color.white_transparent_70)));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_zoom_image1);
        ViewPager viewPager = (ViewPager) dialog.findViewById(R.id.view_pager);
        ImageAdapter adapter = new ImageAdapter(activity, imageArray);
        viewPager.setAdapter(adapter);
        dialog.findViewById(R.id.imageView_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        // Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        // This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    /*, ArrayList<String> imageArray*/
    public void displayImageInGrid(Activity activity, ArrayList<String> imageArray) {

        final Dialog dialog = new Dialog(activity, R.style.DialogZoomAnim);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(activity, R.color.white_transparent_70)));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_grid_image);

        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL);

        recyclerView.setLayoutManager(layoutManager);

        GridAdapter adapter = new GridAdapter(activity, imageArray);
        recyclerView.setAdapter(adapter);

        dialog.findViewById(R.id.imageView_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        // Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        // This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }


    public Dialog showOKFinishDialog(final Context activity, String dialogHeader, String dialogMessage) {
        final Dialog dialog = new Dialog(activity, R.style.DialogStyle);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_ok);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialogHeader != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_header)).setText(dialogHeader);
        if (dialogMessage != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_message)).setText(dialogMessage);

        dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                activity.finish();
                /*Intent intentHome=new Intent(activity, ActivityHomeUserPost.class);
                activity.startActivity(intentHome);*/
            }
        });
        dialog.show();

        return dialog;
    }

    public static Dialog showContactDialog(final Activity activity, String dialogHeader) {
        final Dialog dialog = new Dialog(activity, R.style.DialogStyle);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_contact_options);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        return dialog;
    }

    public static Dialog showRemoveMessageDialog(Context activity, String dialogHeader, String dialogMessage) {
        final Dialog dialog = new Dialog(activity, R.style.DialogStyle);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_ok);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialogHeader != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_header)).setText(dialogHeader);
        if (dialogMessage != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_message)).setText(dialogMessage);

        dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }

    public Dialog dialogAcceptReject(Activity activity, String title, String dialogMessage) {
        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert_ok_cancel);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        ((Button) dialog.findViewById(R.id.button_ok)).setText("Accept");
        ((Button) dialog.findViewById(R.id.button_cancel)).setText("Reject");

        if (dialogMessage != null)
            ((TextView) dialog.findViewById(R.id.textView_dialog_alert_message)).setText(dialogMessage);


        dialog.show();
        return dialog;
    }

    public Dialog displayLockOptionDialog(Activity activity) {


        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnimation);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_lock_option);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.findViewById(R.id.imageView_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }

    public Dialog dialogStatus(Activity activity) {


        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnimation);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_status);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }

    public Dialog dialogHobbies(Activity activity) {


        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnimation);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_hobbies);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }

    Boolean mainPicIsSelected = false;

    public Dialog displayDeleteProfilePicDialog(Activity activity, List<PicData> picDataList, String mainProfilePic) {

        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete_profile_pic);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        final RoundedImageView mainPic = dialog.findViewById(R.id.profile_picture);
        final RoundedImageView optionOne = dialog.findViewById(R.id.option_one);
        final RoundedImageView optionTwo = dialog.findViewById(R.id.option_two);
        final RoundedImageView optionThree = dialog.findViewById(R.id.option_three);
        final RoundedImageView optionFour = dialog.findViewById(R.id.option_four);
        final RoundedImageView optionFive = dialog.findViewById(R.id.option_five);
        final RoundedImageView optionSix = dialog.findViewById(R.id.option_six);

        final RoundedImageView mainPicBack = dialog.findViewById(R.id.profile_picture_back);
        final RoundedImageView optionOneBack = dialog.findViewById(R.id.option_one_back);
        final RoundedImageView optionTwoBack = dialog.findViewById(R.id.option_two_back);
        final RoundedImageView optionThreeBack = dialog.findViewById(R.id.option_three_back);
        final RoundedImageView optionFourBack = dialog.findViewById(R.id.option_four_back);
        final RoundedImageView optionFiveBack = dialog.findViewById(R.id.option_five_back);
        final RoundedImageView optionSixBack = dialog.findViewById(R.id.option_six_back);

        final RelativeLayout layout1 = dialog.findViewById(R.id.layout1);
        final RelativeLayout layout2 = dialog.findViewById(R.id.layout2);
        final RelativeLayout layout3 = dialog.findViewById(R.id.layout3);
        final RelativeLayout layout4 = dialog.findViewById(R.id.layout4);
        final RelativeLayout layout5 = dialog.findViewById(R.id.layout5);
        final RelativeLayout layout6 = dialog.findViewById(R.id.layout6);

        final List<RoundedImageView> roundedImageViewsBack = new LinkedList<>();
        roundedImageViewsBack.add(optionOneBack);
        roundedImageViewsBack.add(optionTwoBack);
        roundedImageViewsBack.add(optionThreeBack);
        roundedImageViewsBack.add(optionFourBack);
        roundedImageViewsBack.add(optionFiveBack);
        roundedImageViewsBack.add(optionSixBack);


        final List<Combine> roundedImageViews = new LinkedList<>();
        roundedImageViews.add(new Combine(optionOne, false));
        roundedImageViews.add(new Combine(optionTwo, false));
        roundedImageViews.add(new Combine(optionThree, false));
        roundedImageViews.add(new Combine(optionFour, false));
        roundedImageViews.add(new Combine(optionFive, false));
        roundedImageViews.add(new Combine(optionSix, false));

        Picasso.with(activity)
                .load(mainProfilePic)
                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                .into(mainPicBack);
        if (picDataList != null) {
            if (picDataList.size() == 1) {
                layout1.setVisibility(View.VISIBLE);
            } else if (picDataList.size() == 2) {
                layout1.setVisibility(View.VISIBLE);
                layout2.setVisibility(View.VISIBLE);
            } else if (picDataList.size() == 3) {
                layout1.setVisibility(View.VISIBLE);
                layout2.setVisibility(View.VISIBLE);
                layout3.setVisibility(View.VISIBLE);
            } else if (picDataList.size() == 4) {
                layout1.setVisibility(View.VISIBLE);
                layout2.setVisibility(View.VISIBLE);
                layout3.setVisibility(View.VISIBLE);
                layout4.setVisibility(View.VISIBLE);
            } else if (picDataList.size() == 5) {
                layout1.setVisibility(View.VISIBLE);
                layout2.setVisibility(View.VISIBLE);
                layout3.setVisibility(View.VISIBLE);
                layout4.setVisibility(View.VISIBLE);
                layout5.setVisibility(View.VISIBLE);
            } else if (picDataList.size() == 6) {
                layout1.setVisibility(View.VISIBLE);
                layout2.setVisibility(View.VISIBLE);
                layout3.setVisibility(View.VISIBLE);
                layout4.setVisibility(View.VISIBLE);
                layout5.setVisibility(View.VISIBLE);
                layout6.setVisibility(View.VISIBLE);
            }
        }

        int index = 0;
        if (picDataList != null && picDataList.size() > 0)
            for (PicData picData : picDataList) {
                if (picData.getSet_profile_picture() != null) {

                    roundedImageViews.get(index).setSelectedID(picDataList.get(index).getSet_profile_pic_id());
                    Picasso.with(activity)
                            .load(PROFILE_PICTURE_PATH + File.separator + picData.getSet_profile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(roundedImageViewsBack.get(index++));
                }
            }

        dialog.findViewById(R.id.imageView_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        mainPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mainPicIsSelected) {
                    mainPic.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.close_icon));
                    mainPicIsSelected = true;
                } else {
                    mainPic.setImageResource(android.R.color.transparent);
                    mainPicIsSelected = false;
                }
            }
        });


        for (Combine combine : roundedImageViews) {
            combine.getRoundedImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!combine.getSelected()) {
                        combine.getRoundedImageView().setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.close_icon));
                        combine.setSelected(true);
                    } else {
                        combine.getRoundedImageView().setImageResource(android.R.color.transparent);
                        combine.setSelected(false);
                    }
                }
            });
        }

        dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.buttonDeletePic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String profile_cat_id = AppSettings.getProfileDefaultCategory();
                if (profile_cat_id == null) {
                    profile_cat_id = PROFILE_CATEGORY_ONE_SOCIAL;
                }

                String user_id = AppSettings.getLoginUserId();
                StringBuffer stringBuffer = new StringBuffer("");


                for (Combine combine : roundedImageViews) {
                    if (combine.getSelected()) {
                        stringBuffer.append(combine.getSelectedID() + ",");
                    }
                }
                String one = "";
                if (stringBuffer.length() > 0)
                    stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                one = stringBuffer.toString();

                if (one.equalsIgnoreCase("") && !mainPicIsSelected) {
                    Toast.makeText(activity, "Please select image to delete.", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                    String main = mainPicIsSelected ? "1" : "0";
                    ((BaseDrawerActivity) activity).callDeleteProfilePic(user_id, profile_cat_id, one, main);
                }
            }
        });
        dialog.show();
        return dialog;
    }

    public Dialog dialogEditPost(Activity activity) {


        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_editpost);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }

    public Dialog socialLogin(Activity activity, String dialogMessage) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnimation);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forgot_password);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialogMessage != null)
            ((TextView) dialog.findViewById(R.id.text_view_dialog_header)).
                    setText(dialogMessage);
        dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        return dialog;
    }

}
