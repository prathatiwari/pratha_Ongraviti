package com.exceptionaire.ongraviti.activities.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.exceptionaire.ongraviti.R;

import java.io.File;
import java.io.IOException;

import CroppingLibrary.CropImageView;
import CroppingLibrary.callback.CropCallback;
import CroppingLibrary.callback.LoadCallback;
import CroppingLibrary.callback.SaveCallback;
import CroppingLibrary.util.Utils;


public class ImageCropActivity extends Activity {
    private static final String PROGRESS_DIALOG = "ProgressDialog";
    Uri imageURI;
    Intent intent;
    // Views ///////////////////////////////////////////////////////////////////////////////////////
    private CropImageView mCropView;
    private LinearLayout mRootLayout;
    private static final int CODE = 200;
    private int request_code;
    private static final int REQUEST_PICK_IMAGE = 10011;
    private static final int REQUEST_SAF_PICK_IMAGE = 10012;
    private static final int CAMERA_CODE = 101;
    private int action_for, action;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main_image_crop);
        bindViews();
        intent = getIntent();
        if (intent != null && intent.hasExtra("request_code")) {
            imageURI = intent.getData();
            request_code = intent.getIntExtra("request_code", 0);
            action_for = intent.getIntExtra("activity_name", 0);
            action = intent.getIntExtra("action", 0);
            System.out.println("profile" + action_for);
            if (request_code == REQUEST_PICK_IMAGE) {
                mCropView.startLoad(intent.getData(), mLoadCallback);
            } else if (request_code == REQUEST_SAF_PICK_IMAGE) {
                mCropView.startLoad(Utils.ensureUriPermission(this, intent), mLoadCallback);
            } else if (request_code == CAMERA_CODE) {
                mCropView.startLoad(intent.getData(), mLoadCallback);
                mCropView.setImageURI(intent.getData());
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        super.onActivityResult(requestCode, resultCode, result);
        if (requestCode == REQUEST_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            /*showProgress();
            mCropView.startLoad(result.getData(), mLoadCallback);*/
        } else if (requestCode == REQUEST_SAF_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            /*showProgress();
            mCropView.startLoad(Utils.ensureUriPermission(this, result), mLoadCallback);*/
        }
    }

    private void bindViews() {
        mCropView = (CropImageView) findViewById(R.id.cropImageView);
        findViewById(R.id.buttonDone).setOnClickListener(btnListener);
        findViewById(R.id.buttonFitImage).setOnClickListener(btnListener);
        findViewById(R.id.button1_1).setOnClickListener(btnListener);
        findViewById(R.id.button3_4).setOnClickListener(btnListener);
        findViewById(R.id.button4_3).setOnClickListener(btnListener);
        findViewById(R.id.button9_16).setOnClickListener(btnListener);
        findViewById(R.id.button16_9).setOnClickListener(btnListener);
        findViewById(R.id.buttonFree).setOnClickListener(btnListener);
        findViewById(R.id.buttonPickImage).setOnClickListener(btnListener);
        findViewById(R.id.buttonRotateLeft).setOnClickListener(btnListener);
        findViewById(R.id.buttonRotateRight).setOnClickListener(btnListener);
        findViewById(R.id.buttonCustom).setOnClickListener(btnListener);
        findViewById(R.id.buttonCircle).setOnClickListener(btnListener);
        findViewById(R.id.buttonShowCircleButCropAsSquare).setOnClickListener(btnListener);
        mRootLayout = (LinearLayout) findViewById(R.id.layout_root);

        mCropView.setCropMode(CropImageView.CropMode.SQUARE);
    }


    public Uri createSaveUri() {
        File cropFile = new File(this.getExternalFilesDir(null), "cropped.png");
        try {
            cropFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Uri.fromFile(cropFile);
    }


    private final View.OnClickListener btnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.buttonDone:
                    cropImage();
                    break;
                case R.id.buttonFitImage:
                    mCropView.setCropMode(CropImageView.CropMode.FIT_IMAGE);
                    break;
                case R.id.button1_1:
                    mCropView.setCropMode(CropImageView.CropMode.SQUARE);
                    break;
                case R.id.button3_4:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_3_4);
                    break;
                case R.id.button4_3:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_4_3);
                    break;
                case R.id.button9_16:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_9_16);
                    break;
                case R.id.button16_9:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_16_9);
                    break;
                case R.id.buttonCustom:
                    mCropView.setCustomRatio(7, 5);
                    break;
                case R.id.buttonFree:
                    mCropView.setCropMode(CropImageView.CropMode.FREE);
                    break;
                case R.id.buttonCircle:
                    mCropView.setCropMode(CropImageView.CropMode.CIRCLE);
                    break;
                case R.id.buttonShowCircleButCropAsSquare:
                    mCropView.setCropMode(CropImageView.CropMode.CIRCLE_SQUARE);
                    break;
                case R.id.buttonRotateLeft:
                    mCropView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D);
                    break;
                case R.id.buttonRotateRight:
                    mCropView.rotateImage(CropImageView.RotateDegrees.ROTATE_90D);
                    break;
                case R.id.buttonPickImage:
                    break;
            }
        }
    };

    public void cropImage() {
//        showProgress();
        mCropView.startCrop(createSaveUri(), mCropCallback, mSaveCallback);
    }
    // Callbacks ///////////////////////////////////////////////////////////////////////////////////

    private final LoadCallback mLoadCallback = new LoadCallback() {
        @Override
        public void onSuccess() {
        }

        @Override
        public void onError() {
        }
    };

    private final CropCallback mCropCallback = new CropCallback() {
        @Override
        public void onSuccess(Bitmap cropped) {
        }

        @Override
        public void onError() {
        }
    };

    private final SaveCallback mSaveCallback = new SaveCallback() {
        @Override
        public void onSuccess(Uri outputUri) {
            Log.i("Crop Path", "" + outputUri);
            Intent i;
            switch (action_for) {
                case 1:
//                    i = new Intent(ImageCropActivity.this, UserSocialProfile.class);
//                    i.putExtra("Output", "output");
//                    if (action == 1) {
//                        i.putExtra("WithAdd", "add");
//                    } else if (action == 2) {
//                        i.putExtra("WithEdit", "edit");
//                    }
//                    i.setData(outputUri);
//                    startActivity(i);
//                    finish();
//                    break;

                case 2:
//                    i = new Intent(ImageCropActivity.this, DatingProfile.class);
//                    i.putExtra("Output", "output");
//                    if (action == 1) {
//                        i.putExtra("WithAdd", "add");
//                    } else if (action == 2) {
//                        i.putExtra("WithEdit", "edit");
//                    }
//                    i.setData(outputUri);
//                    startActivity(i);
//                    finish();
//                    break;

                case 3:
//                    i = new Intent(ImageCropActivity.this, BusinessEnterpreneurProfile.class);
//                    i.putExtra("Output", "output");
//                    if (action == 1) {
//                        i.putExtra("WithAdd", "add");
//                    } else if (action == 2) {
//                        i.putExtra("WithEdit", "edit");
//                    }
//                    i.setData(outputUri);
//                    startActivity(i);
//                    finish();
//                    break;

                case 4:
//                    i = new Intent(ImageCropActivity.this, EditProfileActivity.class);
//                    i.putExtra("Output", "output");
//                    if (action == 1) {
//                        i.putExtra("WithAdd", "add");
//                    } else if (action == 2) {
//                        i.putExtra("WithEdit", "edit");
//                    }
//                    i.setData(outputUri);
//                    startActivity(i);
//                    finish();
//                    break;

                case 5:

//                    i = new Intent(ImageCropActivity.this, EditProfileActivity.class);
//                    i.putExtra("Output", "output");
//                    if (action == 1) {
//                        i.putExtra("WithAdd", "add");
//                    } else if (action == 2) {
//                        i.putExtra("WithEdit", "edit");
//                    }
//                    i.setData(outputUri);
//                    setResult(RESULT_OK, i);
//                    finish();
//                    break;

                case 6:
                    i = new Intent();
                    i.putExtra("Output", "output");
                    if (action == 1) {
                        i.putExtra("WithAdd", "add");
                    } else if (action == 2) {
                        i.putExtra("WithEdit", "edit");
                    }
                    i.setData(outputUri);
                    setResult(RESULT_OK, i);
                    finish();
                    break;

                case 10:
                    i = new Intent();
                    i.putExtra("Output", "output");
                    if (action == 1) {
                        i.putExtra("WithAdd", "add");
                    } else if (action == 2) {
                        i.putExtra("WithEdit", "edit");
                    }
                    i.setData(outputUri);
                    setResult(RESULT_OK, i);
                    finish();
                    break;

            }
        }

        @Override
        public void onError() {
        }
    };
}