package com.exceptionaire.ongraviti.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.SpeedDatingUserListAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.ResponseSpeedDatingUserList;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SelectUserActivity extends BaseDrawerActivity implements AppConstants, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView user_list;
    private SpeedDatingUserListAdapter speedDatingUserListAdapter;
    private ArrayList<ResponseSpeedDatingUserList.User_list> userListResponses;
    public ProgressDialog progressDialog;
    private String hasNext = "0";
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout swipe_refresh_layout;
    private LinearLayout linearLayoutList;
    private LinearLayout linearLayoutNoListItem;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_select_user, frameLayout);
        arcMenu.setVisibility(View.GONE);
        initialiseComponents();
    }

    private void initialiseComponents() {
        user_list = (RecyclerView) findViewById(R.id.user_list);
        swipe_refresh_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        linearLayoutList = (LinearLayout) findViewById(R.id.linear_layout_list);
        linearLayoutNoListItem = (LinearLayout) findViewById(R.id.linear_layout_no_list_item);
        swipe_refresh_layout.setOnRefreshListener(this);
        layoutManager = new LinearLayoutManager(this);
        user_list.setLayoutManager(layoutManager);
        userListResponses = new ArrayList<>();

        if (isNetworkAvailable()) {

//            getAllUser(""+ AppSettings.getLoginUserId(), /*AppSettings.getKeyEventId()*/"4", hasNext);

            if (AppSettings.getLoginUserId() != null && AppSettings.getKeyEventId() != null) {

                getAllUser(AppSettings.getLoginUserId(), AppSettings.getKeyEventId(), hasNext);

            }
        }
        mLayoutManager = new LinearLayoutManager(this);
        user_list.setLayoutManager(mLayoutManager);

        user_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

    }

    private void getAllUser(final String user_id, String event_id, String purchase_id) {

        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {

            Retrofit retrofit = ApiClient.getClient();
            final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseSpeedDatingUserList> getAllUserList = requestInterface.getUserList(user_id, event_id, purchase_id);
            getAllUserList.enqueue(new Callback<ResponseSpeedDatingUserList>() {
                @Override
                public void onResponse(Call<ResponseSpeedDatingUserList> call, Response<ResponseSpeedDatingUserList> response) {

                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            if (response.body().getUser_list().size() > 0) {

                                linearLayoutList.setVisibility(View.VISIBLE);
                                linearLayoutNoListItem.setVisibility(View.GONE);

                                speedDatingUserListAdapter = new SpeedDatingUserListAdapter(
                                        SelectUserActivity.this, response.body().getUser_list(), user_list);
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                user_list.setLayoutManager(linearLayoutManager);
                                user_list.setItemAnimator(new DefaultItemAnimator());
                                user_list.setHasFixedSize(true);
                                user_list.setAdapter(speedDatingUserListAdapter);
                                speedDatingUserListAdapter.notifyDataSetChanged();

                                hasNext = response.body().getNexthas();
                            } else {
                                linearLayoutList.setVisibility(View.GONE);
                                linearLayoutNoListItem.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseSpeedDatingUserList> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });


        } catch (Exception e) {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        }

    }

/*
    private void getEventUser(String event_id) {

        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {

            Retrofit retrofit = ApiClient.getClient();
            final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseGetOpponentsForDatingEvent> getAllUserList = requestInterface.getEventUserList(event_id);
            getAllUserList.enqueue(new Callback<ResponseGetOpponentsForDatingEvent>() {
                @Override
                public void onResponse(Call<ResponseGetOpponentsForDatingEvent> call, Response<ResponseGetOpponentsForDatingEvent> response) {

                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response.body() != null) {

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            speedDatingUserListAdapter = new SpeedDatingUserListAdapter(response.body().getUser(), user_list);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            user_list.setLayoutManager(linearLayoutManager);
                            user_list.setItemAnimator(new DefaultItemAnimator());
                            user_list.setHasFixedSize(true);
                            user_list.setAdapter(speedDatingUserListAdapter);
                            speedDatingUserListAdapter.notifyDataSetChanged();


                            hasNext = response.body().getHasNext();
                        }
                    }


                }

                @Override
                public void onFailure(Call<ResponseGetOpponentsForDatingEvent> call, Throwable t) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });


        } catch (Exception e) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

    }
*/

    @Override
    public void onRefresh() {
        if (isNetworkAvailable()) {

            hasNext = "0";
//            getAllUser(""+AppSettings.getLoginUserId(), AppSettings.getKeyEventId(), hasNext);

            if (AppSettings.getLoginUserId() != null && AppSettings.getKeyEventId() != null) {

                getAllUser(AppSettings.getLoginUserId(), AppSettings.getKeyEventId(), hasNext);

            }
        }

    }
}
