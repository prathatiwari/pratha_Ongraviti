package com.exceptionaire.ongraviti.activities.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.MyFavoritesActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.myorbit.PeopleInOrbitFragment;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.FavoriteUsers;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;


public class FavoritesAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<FavoriteUsers> cards;
    private final ArrayList<FavoriteUsers> fileredlist = new ArrayList<>();
    private String TAG = "FavoritesAdapter";
    private favoriteDeleteListener customListner;
    private favoriteViewListener customViewListener;

    public FavoritesAdapter(ArrayList<FavoriteUsers> cards, Context mContext) {
        try {
            this.cards = cards;
            fileredlist.addAll(cards);
            this.mContext = mContext;
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public interface favoriteDeleteListener {
        void onfavoriteDeleteClickListner(int position);
    }

    public interface favoriteViewListener {
        void onfavoriteViewClickListner(int position);
    }

    public void setfavoriteDelete(favoriteDeleteListener listener) {
        this.customListner = listener;
    }

    public void setFavoriteUserInfoListener(favoriteViewListener listener) {
        this.customViewListener = listener;
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        View convertView = view;
        try {
            FavoritesAdapter.ViewHolder viewHolder = new FavoritesAdapter.ViewHolder();

            FavoriteUsers card = cards.get(position);

            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                convertView = layoutInflater.inflate(R.layout.favorite_list_item, null);
            }
            viewHolder.image_view_cat_type = convertView.findViewById(R.id.image_view_cat_type);
            viewHolder.linear_layout = convertView.findViewById(R.id.linear_layout);
            viewHolder.delete = convertView.findViewById(R.id.favorite_delete);
            viewHolder.txt_name = convertView.findViewById(R.id.favorite_user_name);
            viewHolder.profile_picture = convertView.findViewById(R.id.favorite_profile_picture);

            viewHolder.txt_name.setText(card.getC_name());
            if (card.getProfile_picture() != null) {
                if (!card.getProfile_picture().contains("https:")) {
                    Picasso.with(mContext)
                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + card.getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(viewHolder.profile_picture);
                } else if (card.getProfile_picture().contains("https:")) {
                    Picasso.with(mContext)
                            .load(card.getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(viewHolder.profile_picture);
                }
            }


            viewHolder.linear_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentProfile = new Intent(mContext, FavoriteUserInfoActivity.class);
                    intentProfile.putExtra("ActivityName", "FavoriteActivity");
                    intentProfile.putExtra("favUserID", "" + card.getFavorite_user_id());
                    mContext.startActivity(intentProfile);
                }
            });
            viewHolder.delete.setOnClickListener(view1 -> {

                Dialog dialog = new CommonDialogs().dialogAcceptReject((Activity) mContext, mContext.getResources().getString(R.string.ongravity), "Are you sure you want to remove this user from your favorites?");
                ((Button) dialog.findViewById(R.id.button_ok)).setText("Yes");
                ((Button) dialog.findViewById(R.id.button_cancel)).setText("No");
                dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (customListner != null) {
                            customListner.onfavoriteDeleteClickListner(position);
                        }
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            });

            viewHolder.profile_picture.setOnClickListener(v -> {
                if (customViewListener != null) {
                    customViewListener.onfavoriteViewClickListner(position);
                }
            });
            switch (card.getCategory_type()) {
                case "2":
                    viewHolder.image_view_cat_type.setImageResource(R.drawable.ic_circle_pink);
                    break;
                case "3":
                    viewHolder.image_view_cat_type.setImageResource(R.drawable.ic_circle_blue);
                    break;
                case "1":
                    viewHolder.image_view_cat_type.setImageResource(R.drawable.ic_circle_green);
                    break;
            }
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }

        return convertView;
    }

    public class ViewHolder {
        ImageButton delete;
        TextView txt_name;
        RoundedImageView profile_picture;
        ImageView image_view_cat_type;
        LinearLayout linear_layout;
    }

    public void filter(String charText) {
        try {
            charText = charText.toLowerCase(Locale.getDefault());
            cards.clear();
            if (charText.length() == 0) {
                cards.addAll(fileredlist);
            } else {
                for (FavoriteUsers wp : fileredlist) {
                    if (wp.getC_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                        cards.add(wp);
                    }
                }
            }
            if (cards.size() == 0) {
                MyFavoritesActivity.tv_msg.setVisibility(View.VISIBLE);
            } else {
                MyFavoritesActivity.tv_msg.setVisibility(View.GONE);
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }
}
