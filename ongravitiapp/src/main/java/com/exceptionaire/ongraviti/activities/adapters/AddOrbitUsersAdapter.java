package com.exceptionaire.ongraviti.activities.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.myorbit.AddPeopleToOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.OrbitFriend;
import com.exceptionaire.ongraviti.model.OrbitRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AddOrbitUsersAdapter extends BaseAdapter {
    private List<OrbitRequest> orbitRequestList;
    private Context context;
    private List<OrbitRequest> fileredlist;

    public AddOrbitUsersAdapter(List<OrbitRequest> contactList, Context context) {
        this.orbitRequestList = contactList;
        this.fileredlist = new ArrayList<>();
        this.context = context;
    }

    private class Holder {
        TextView txtName, tv_mutual_friends;
        Button btn_send_request;
        RoundedImageView image;
    }


    @Override
    public int getCount() {
        return orbitRequestList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return orbitRequestList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {

        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final OrbitRequest orbitRequest = orbitRequestList.get(position);
        if (convertView == null) {
            holder = new AddOrbitUsersAdapter.Holder();
            convertView = layoutInflater.inflate(R.layout.add_orbit_friends_list_item, arg2, false);

            holder.txtName = convertView.findViewById(R.id.txt_orbit_name);
            holder.tv_mutual_friends = convertView.findViewById(R.id.textview_email_address);
            holder.btn_send_request = convertView.findViewById(R.id.ibtn_send_request);
            holder.image = convertView.findViewById(R.id.user_profile_pic);
            convertView.setTag(holder);

        } else {
            holder = (AddOrbitUsersAdapter.Holder) convertView.getTag();
        }
        holder.txtName.setText(orbitRequest.getUserName());


        if (orbitRequest.getProfilePic() != null && !orbitRequest.getProfilePic().isEmpty()) {
            if (!orbitRequest.getProfilePic().contains("https:")) {
                Picasso.with(context)
                        .load(AppConstants.PROFILE_PICTURE_PATH + "/" + orbitRequest.getProfilePic()).fit().centerCrop()
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            } else if (orbitRequest.getProfilePic().contains("https:")) {
                Picasso.with(context)
                        .load(orbitRequest.getProfilePic())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            }
        }
        if (orbitRequest.getOrbitFriendList() != null && !orbitRequest.getOrbitFriendList().isEmpty()) {
            holder.tv_mutual_friends.setText(orbitRequest.getOrbitFriendList().size() + " mutual friend(s)");
        }

        holder.btn_send_request.setOnClickListener(v -> {
            orbitRequest.setFlag("0");
            //TODO call send request
        });

        holder.tv_mutual_friends.setOnClickListener(v -> {
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
            //builderSingle.setIcon(R.drawable.ic_launcher);
            builderSingle.setTitle("Mutual friends");

            final ArrayAdapter<OrbitFriend> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1);
            //arrayAdapter.add("abc");
            arrayAdapter.addAll(orbitRequest.getOrbitFriendList());

            builderSingle.setNegativeButton("close", (dialog, which) -> dialog.dismiss());


            builderSingle.setAdapter(arrayAdapter, (dialog, which) -> {
            });

            builderSingle.show();
        });

        return convertView;
    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        orbitRequestList.clear();
        if (charText.length() == 0) {
            orbitRequestList.addAll(fileredlist);
        } else {
            for (OrbitRequest wp : fileredlist) {
                if (wp.getUserName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    orbitRequestList.add(wp);
                }
            }
        }
        if (orbitRequestList.size() == 0) {
            AddPeopleToOrbitActivity.tv_msg.setVisibility(View.VISIBLE);
        } else {
            AddPeopleToOrbitActivity.tv_msg.setVisibility(View.GONE);
        }
        notifyDataSetChanged();
    }
}
