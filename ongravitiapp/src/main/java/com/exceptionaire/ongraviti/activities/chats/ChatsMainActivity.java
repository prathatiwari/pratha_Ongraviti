package com.exceptionaire.ongraviti.activities.chats;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppDetails;
import com.exceptionaire.ongraviti.core.AppGlobals;
import com.exceptionaire.ongraviti.core.AppPreference;
import com.exceptionaire.ongraviti.model.OrbitContact;
import com.exceptionaire.ongraviti.quickblox.QBData;
import com.exceptionaire.ongraviti.quickblox.activity.BaseActivity;
import com.exceptionaire.ongraviti.quickblox.core.gcm.GooglePlayServicesHelper;
import com.exceptionaire.ongraviti.quickblox.core.utils.SharedPrefsHelper;
import com.exceptionaire.ongraviti.quickblox.core.utils.Toaster;
import com.exceptionaire.ongraviti.quickblox.core.utils.constant.GcmConsts;
import com.exceptionaire.ongraviti.quickblox.db.QbUsersDbManager;
import com.exceptionaire.ongraviti.quickblox.managers.DialogsManager;
import com.exceptionaire.ongraviti.quickblox.managers.QbChatDialogMessageListenerImp;
import com.exceptionaire.ongraviti.quickblox.managers.QbDialogHolder;
import com.exceptionaire.ongraviti.quickblox.managers.QbEntityCallbackImpl;
import com.exceptionaire.ongraviti.quickblox.services.CallService;
import com.exceptionaire.ongraviti.quickblox.utils.PermissionsChecker;
import com.exceptionaire.ongraviti.quickblox.utils.SharedPreferencesUtil;
import com.exceptionaire.ongraviti.quickblox.utils.WebRtcSessionManager;
import com.exceptionaire.ongraviti.quickblox.utils.chat.ChatHelper;
import com.exceptionaire.ongraviti.utilities.LogUtil;
import com.halzhang.android.library.BottomTabIndicator;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.request.QBDialogRequestBuilder;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.core.server.Performer;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

public class ChatsMainActivity extends BaseActivity implements AppConstants, DialogsManager.ManagingDialogsCallbacks {
    public static final String TAG = ChatsMainActivity.class.getSimpleName();
    private static final int REQUEST_SELECT_PEOPLE = 174;
    public static final int REQUEST_DIALOG_ID_FOR_UPDATE = 165;
    public static final int REQUEST_CREATE_GROUP = 32453;

    private ViewPager m_ViewPager = null;
    private BottomTabIndicator bottomTabIndicator = null;
    private ChatFragmentAdapter m_Adapter = null;

    private ActionMode currentActionMode;
    private QBRequestGetBuilder requestBuilder;
    private BroadcastReceiver pushBroadcastReceiver;
    private GooglePlayServicesHelper googlePlayServicesHelper;
    private QBChatDialogMessageListener allDialogsMessagesListener;
    private SystemMessagesListener systemMessagesListener;
    private QBSystemMessagesManager systemMessagesManager;
    private QBIncomingMessagesManager incomingMessagesManager;
    private DialogsManager dialogsManager;
    private Toast mToast;
    private ProgressDialog progressDialog;
    private AppPreference mPrefs;
    private QBUser currentUser;
    private ArrayList<QBUser> currentOpponentsList;
    private QbUsersDbManager dbManager;
    private boolean isRunForCall;
    private WebRtcSessionManager webRtcSessionManager;
    private PermissionsChecker checker;
    public ChatListAdapter chatListAdapter;
    private View rootView;


    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave);

        setContentView(R.layout.activity_main_chat);
        // getLayoutInflater().inflate(R.layout.activity_main_chat, frameLayout);
        rootView = findViewById(R.id.content_main);
        arcMenu.setVisibility(View.VISIBLE);

        m_Adapter = new ChatFragmentAdapter(this, getSupportFragmentManager());
        m_ViewPager = (ViewPager) findViewById(R.id.viewPager);
        m_ViewPager.setAdapter(m_Adapter);
        bottomTabIndicator = (BottomTabIndicator) findViewById(R.id.tab_indicator);
        bottomTabIndicator.setViewPager(m_ViewPager);
        bottomTabIndicator.setCurrentItem(0);

        chatListAdapter = new ChatListAdapter(this, null);
        mPrefs = new AppPreference(this);
        AppDetails.setContext(this);

        setupList();
        LogUtil.writeDebugLog(TAG, "onCreate", "googlePlayServicesHelper");
        googlePlayServicesHelper = new GooglePlayServicesHelper();
        if (googlePlayServicesHelper.checkPlayServicesAvailable(this)) {
            googlePlayServicesHelper.registerForGcm(GCM_SENDER_ID);
        }

        pushBroadcastReceiver = new PushBroadcastReceiver();
        if (isAppSessionActive) {
            allDialogsMessagesListener = new AllDialogsMessageListener();
            systemMessagesListener = new SystemMessagesListener();
        }
        dialogsManager = new DialogsManager();
        initUi();

        initFields();
        checker = new PermissionsChecker(getApplicationContext());
        LogUtil.writeDebugLog(TAG, "onCreate", "end");


        findViewById(R.id.buttonGroupChat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String[] array = {"26572261", "26572180"};
//                startGroupChatActivity(array);

            }
        });

        findViewById(R.id.buttonGoToCreateGroup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatsMainActivity.this, CreateGroupActivity.class);
                startActivityForResult(intent, REQUEST_CREATE_GROUP);
            }
        });

        findViewById(R.id.image_view_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Todo when comming soon remove from spek out find pls uncomment finish and comment intent calling
//                finish();
                Intent i = new Intent(ChatsMainActivity.this, HomeUserPostsActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void initUi() {
        LogUtil.writeDebugLog(TAG, "initUi", "1");
        requestBuilder = new QBRequestGetBuilder();
    }

    private void initFields() {
        LogUtil.writeDebugLog(TAG, "initFields", "start");
        currentUser = sharedPrefsHelper.getQbUser();
        dbManager = QbUsersDbManager.getInstance(getApplicationContext());
        webRtcSessionManager = WebRtcSessionManager.getInstance(this);
    }


    private void setupList() {
        LogUtil.writeDebugLog(TAG, "setupList", "start");
        mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.CENTER, 0, 0);
    }

    protected boolean isUserMe(QBUser user) {
        if (QBData.curQBUser != null) {
            QBUser currentUser = QBData.curQBUser;
            String userID = user.getLogin();
            //for administrator
            if (userID.equals("ibrahima")) {
                return true;
            }
            return currentUser != null && currentUser.getId().equals(user.getId());
        } else {
            return false;
        }
    }

    private void loadDialogsFromQb(final boolean silentUpdate, final boolean clearDialogHolder) {
        LogUtil.writeDebugLog(TAG, "loadDialogsFromQb", "start");
        if (!silentUpdate) {
            showProgressBar();
        }
        ChatHelper.getInstance().getDialogs(requestBuilder, new QBEntityCallback<ArrayList<QBChatDialog>>() {
            @Override
            public void onSuccess(ArrayList<QBChatDialog> dialogs, Bundle bundle) {
                LogUtil.writeDebugLog(TAG, "loadDialogsFromQb", "onSuccess");
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (clearDialogHolder) {
                    QbDialogHolder.getInstance().clear();
                }
                QbDialogHolder.getInstance().addDialogs(dialogs);
                updateDialogsAdapter();
            }

            @Override
            public void onError(QBResponseException e) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                LogUtil.writeDebugLog(TAG, "loadDialogsFromQb", "onError");
                Toast.makeText(ChatsMainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void startChatActivity(final String quickbloxUserID) {

        try {
            Performer<QBUser> qbUserPerformer = QBUsers.getUser(Integer.parseInt(quickbloxUserID));
            qbUserPerformer.performAsync(new QBEntityCallback<QBUser>() {
                @Override
                public void onSuccess(QBUser qbUserOpponent, Bundle bundle) {
                    ArrayList<QBUser> newDialog = new ArrayList<>();
                    newDialog.add(qbUserOpponent);
                    newDialog.add(getQBUserData());
                    QBData.curQBUser = getQBUserData();
                    if (isPrivateDialogExist(newDialog)) {
                        LogUtil.writeDebugLog(TAG, "startReceivedActivity", "start ReceivedActivity");
                        newDialog.remove(ChatHelper.getCurrentUser());
                        QBChatDialog existingPrivateDialog = QbDialogHolder.getInstance().getPrivateDialogWithUser(newDialog.get(0));
                        ChatActivity.startForResult(ChatsMainActivity.this, REQUEST_DIALOG_ID_FOR_UPDATE, existingPrivateDialog.getDialogId());
                    } else {
                        LogUtil.writeDebugLog(TAG, "startReceivedActivity", "create Dialog");
                        createDialog(newDialog, null);
                    }
                }

                @Override
                public void onError(QBResponseException e) {
                    Toaster.longToast("This user is not registered to chat server");
                }
            });

        } catch (Exception e) {
            Toaster.longToast("This user is not registered to chat server");
        }
    }

    ArrayList<QBUser> newDialog = new ArrayList<>();
    int count = 0;

    @SuppressLint("LongLogTag")
    public void startGroupChatActivity(final String[] quickbloxUserID) {
        Log.e("startGroupChatActivity:::", "startGroupChatActivity");
        try {
            newDialog.clear();
            count = 0;
            Log.e("quickbloxUserID::::", quickbloxUserID + "");
            for (int i = 0; i < quickbloxUserID.length - 2; i++) {
                Performer<QBUser> qbUserPerformer = QBUsers.getUser(Integer.parseInt(quickbloxUserID[i]));
                qbUserPerformer.performAsync(new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser qbUserOpponent, Bundle bundle) {
                        count++;
                        newDialog.add(qbUserOpponent);

                        if (count == quickbloxUserID.length - 2) {
                            newDialog.add(getQBUserData());
                            if (isPrivateDialogExist(newDialog)) {
                                LogUtil.writeDebugLog(TAG, "startReceivedActivity", "start ReceivedActivity");
                                newDialog.remove(ChatHelper.getCurrentUser());
                                QBChatDialog existingPrivateDialog = QbDialogHolder.getInstance().getPrivateDialogWithUser(newDialog.get(0));
                                Log.e("newDialogif::::", newDialog + "");
                                ChatActivity.startForResult(ChatsMainActivity.this, REQUEST_DIALOG_ID_FOR_UPDATE, existingPrivateDialog.getDialogId());
                            } else {
                                LogUtil.writeDebugLog(TAG, "startReceivedActivity", "create Dialog");
                                Log.e("newDialogelse::::", newDialog + "");
                                createDialog(newDialog, quickbloxUserID);

                            }
                        }
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Toaster.longToast("This user is not registered to chat server");
                    }
                });
            }
        } catch (Exception e) {
            Toaster.longToast("This user is not registered to chat server");
        }
    }

    private boolean isPrivateDialogExist(ArrayList<QBUser> allSelectedUsers) {
        LogUtil.writeDebugLog(TAG, "isPrivateDialogExist", "1");
        ArrayList<QBUser> selectedUsers = new ArrayList<>();
        selectedUsers.addAll(allSelectedUsers);
        selectedUsers.remove(ChatHelper.getCurrentUser());
        return selectedUsers.size() == 1 && QbDialogHolder.getInstance().hasPrivateDialogWithUser(selectedUsers.get(0));
    }

    private QBChatDialog qbChatDialog;

    private void createDialog(final ArrayList<QBUser> selectedUsers, final String[] quickbloxUserID) {
        LogUtil.writeDebugLog(TAG, "createDialog", "start");
        showProgressBar();
//        Log.e("quickbloxDialogName::::", quickbloxUserID[quickbloxUserID.length - 1] + "");
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        qbChatDialog = dialog;

                        if (quickbloxUserID != null && quickbloxUserID.length >= 4) {
                            final int fileIndex = quickbloxUserID.length - 2;
                            File filePhoto = new File(quickbloxUserID[fileIndex]);
                            Boolean fileIsPublic = true;
                            QBContent.uploadFileTask(filePhoto, fileIsPublic, null).performAsync(new QBEntityCallback<QBFile>() {
                                @Override
                                public void onSuccess(QBFile qbFile, Bundle bundle) {
                                    qbChatDialog.setPhoto(qbFile.getId().toString());
                                    updateDialogueName(quickbloxUserID[fileIndex + 1]);
                                }

                                @Override
                                public void onError(QBResponseException e) {
                                    progressDialog.dismiss();
                                    e.printStackTrace();
                                    Log.e("onError external ::::", e.getMessage());
                                    updateDialogueName(quickbloxUserID[fileIndex + 1]);
                                    // error
                                }
                            });
                        } else {


                            /*final int fileIndex = quickbloxUserID.length - 2;
                            File filePhoto = new File(quickbloxUserID[fileIndex]);
                            Boolean fileIsPublic = true;
                            QBContent.uploadFileTask(filePhoto, fileIsPublic, null).performAsync(new QBEntityCallback<QBFile>() {
                                @Override
                                public void onSuccess(QBFile qbFile, Bundle bundle) {
                                    qbChatDialog.setPhoto(qbFile.getId().toString());

                                    QBDialogRequestBuilder requestBuilder = new QBDialogRequestBuilder();
                                    // add another users
                                    // requestBuilder.removeUsers(22); // Remove yourself (user with ID 22)
                                    qbChatDialog.setName(quickbloxUserID[fileIndex + 1]);

                                    QBRestChatService.updateGroupChatDialog(qbChatDialog, requestBuilder).performAsync(new QBEntityCallback<QBChatDialog>() {
                                        @Override
                                        public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                                            if (progressDialog != null && progressDialog.isShowing())
                                                progressDialog.dismiss();
                                            LogUtil.writeDebugLog(TAG, "createDialog", "onSuccess");
                                            dialogsManager.sendSystemMessageAboutCreatingDialog(systemMessagesManager, qbChatDialog);
                                            LogUtil.writeDebugLog(TAG, "createDialog", "start ReceivedActivity");
                                            ChatActivity.startForResult(ChatsMainActivity.this, REQUEST_DIALOG_ID_FOR_UPDATE, qbChatDialog.getDialogId());
                                        }

                                        @Override
                                        public void onError(QBResponseException e) {

                                        }
                                    });
                                }

                                @Override
                                public void onError(QBResponseException e) {
                                    // error
                                }
                            });*/

                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();

                            LogUtil.writeDebugLog(TAG, "createDialog", "onSuccess");
                            dialogsManager.sendSystemMessageAboutCreatingDialog(systemMessagesManager, qbChatDialog);
                            LogUtil.writeDebugLog(TAG, "createDialog", "start ReceivedActivity");
                            ChatActivity.startForResult(ChatsMainActivity.this, REQUEST_DIALOG_ID_FOR_UPDATE, qbChatDialog.getDialogId());

                        }
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        LogUtil.writeDebugLog(TAG, "createDialog", "start onError");
                    }

                }
        );
    }

    private void updateDialogueName(String s) {
        QBDialogRequestBuilder requestBuilder = new QBDialogRequestBuilder();
        // add another users
        // requestBuilder.removeUsers(22); // Remove yourself (user with ID 22)
        Log.e("quickbloxDialogName::::", s + "");
        qbChatDialog.setName(s);

        QBRestChatService.updateGroupChatDialog(qbChatDialog, requestBuilder).performAsync(new QBEntityCallback<QBChatDialog>() {
            @Override
            public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                LogUtil.writeDebugLog(TAG, "createDialog", "onSuccess");
                dialogsManager.sendSystemMessageAboutCreatingDialog(systemMessagesManager, qbChatDialog);
                LogUtil.writeDebugLog(TAG, "createDialog", "start ReceivedActivity");
                ChatActivity.startForResult(ChatsMainActivity.this, REQUEST_DIALOG_ID_FOR_UPDATE, qbChatDialog.getDialogId());
            }

            @Override
            public void onError(QBResponseException e) {
                progressDialog.dismiss();
                e.printStackTrace();
                Log.e("onError internal ::::", e.getMessage());
            }
        });
    }

    public OrbitContact getUserDataFromUserId(String userId) {
        LogUtil.writeDebugLog(TAG, "getUserDataFromUsername", userId);
        for (int i = 0; i < AppGlobals.mAllUserData.size(); i++) {
            OrbitContact user = AppGlobals.mAllUserData.get(i);
            if (user.getQuickblox_id().equals(userId))
                return user;
        }
        return null;
    }

    public OrbitContact getUserDataFromUserId(Integer userId) {
        for (int i = 0; i < AppGlobals.mAllUserData.size(); i++) {
            OrbitContact user = AppGlobals.mAllUserData.get(i);
            Integer id = Integer.parseInt(user.getQuickblox_id());
            if (id.equals(userId))
                return user;
        }
        return null;
    }

    private void loadUpdatedDialog(String dialogId) {
        LogUtil.writeDebugLog(TAG, "loadUpdatedDialog", "start");
        ChatHelper.getInstance().getDialogById(dialogId, new QbEntityCallbackImpl<QBChatDialog>() {
            @Override
            public void onSuccess(QBChatDialog result, Bundle bundle) {
                LogUtil.writeDebugLog(TAG, "loadUpdatedDialog", "onSuccess");
                Log.d("dialogname", result + "");
                QbDialogHolder.getInstance().addDialog(result);
                updateDialogsAdapter();
            }

            @Override
            public void onError(QBResponseException e) {
                LogUtil.writeDebugLog(TAG, "loadUpdatedDialog", "onError");
            }
        });
    }

    private void userLogout() {
        LogUtil.writeDebugLog(TAG, "userLogout", "start");
        ChatHelper.getInstance().logout(new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                LogUtil.writeDebugLog(TAG, "userLogout", "onSuccess");
                if (googlePlayServicesHelper.checkPlayServicesAvailable()) {
                    googlePlayServicesHelper.unregisterFromGcm(GCM_SENDER_ID);
                }
                SharedPreferencesUtil.removeQbUser();
                QbDialogHolder.getInstance().clear();
                finish();
            }

            @Override
            public void onError(QBResponseException e) {
                LogUtil.writeDebugLog(TAG, "userLogout", "onError");
                reconnectToChatLogout(SharedPreferencesUtil.getQbUser());
            }
        });
    }

    private void reconnectToChatLogout(final QBUser user) {
        LogUtil.writeDebugLog(TAG, "reconnectToChatLogout", "start");
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                LogUtil.writeDebugLog(TAG, "reconnectToChatLogout", "onSuccess");
                userLogout();
            }

            @Override
            public void onError(QBResponseException e) {
                LogUtil.writeDebugLog(TAG, "reconnectToChatLogout", "onError");
                invalidateOptionsMenu();
                reconnectToChatLogout(SharedPreferencesUtil.getQbUser());
            }
        });
    }

    private void updateDialogsList() {
        LogUtil.writeDebugLog(TAG, "updateDialogsList", "1");
        if (isAppSessionActive) {
            loadDialogsFromQb(true, true);
        }
    }

    private void registerQbChatListeners() {
        LogUtil.writeDebugLog(TAG, "registerQbChatListeners", "1");
        incomingMessagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();

        if (incomingMessagesManager != null) {
            incomingMessagesManager.addDialogMessageListener(allDialogsMessagesListener != null
                    ? allDialogsMessagesListener : new AllDialogsMessageListener());
        }

        if (systemMessagesManager != null) {
            systemMessagesManager.addSystemMessageListener(systemMessagesListener != null
                    ? systemMessagesListener : new SystemMessagesListener());
        }

        dialogsManager.addManagingDialogsCallbackListener(this);
    }

    private void unregisterQbChatListeners() {
        LogUtil.writeDebugLog(TAG, "unregisterQbChatListeners", "1");
        if (incomingMessagesManager != null) {
            incomingMessagesManager.removeDialogMessageListrener(allDialogsMessagesListener);
        }
        if (systemMessagesManager != null) {
            systemMessagesManager.removeSystemMessageListener(systemMessagesListener);
        }
        dialogsManager.removeManagingDialogsCallbackListener(this);
    }

    public void updateDialogsAdapter() {
        chatListAdapter.updateList(new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values()));
        LogUtil.writeDebugLog(TAG, "updateDialogsAdapter", "1");

    }

    private void deleteSelectedDialogs(Collection<QBChatDialog> selectedDialogs) {
        ChatHelper.getInstance().deleteDialogs(selectedDialogs, new QBEntityCallback<ArrayList<String>>() {
            @Override
            public void onSuccess(ArrayList<String> dialogsIds, Bundle bundle) {
                QbDialogHolder.getInstance().deleteDialogs(dialogsIds);
                updateDialogsAdapter();
            }

            @Override
            public void onError(QBResponseException e) {
            }
        });
    }

    private void startLoadUsers() {
        String currentRoomName = SharedPrefsHelper.getInstance().get(PREF_CURREN_ROOM_NAME);
        requestExecutor.loadUsersByTag(currentRoomName, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> result, Bundle params) {
                dbManager.saveAllUsers(result, true);
                initUsersList();
            }

            @Override
            public void onError(QBResponseException responseException) {
            }
        });
    }

    private boolean isCurrentOpponentsListActual(ArrayList<QBUser> actualCurrentOpponentsList) {
        LogUtil.writeDebugLog(TAG, "isCurrentOpponentsListActual", "start");
        boolean equalActual = actualCurrentOpponentsList.retainAll(currentOpponentsList);
        boolean equalCurrent = currentOpponentsList.retainAll(actualCurrentOpponentsList);
        return !equalActual && !equalCurrent;
    }

    private void initUsersList() {
        LogUtil.writeDebugLog(TAG, "initUsersList", "1");
        if (currentOpponentsList != null) {
            ArrayList<QBUser> actualCurrentOpponentsList = dbManager.getAllUsers();
            actualCurrentOpponentsList.remove(sharedPrefsHelper.getQbUser());
            if (isCurrentOpponentsListActual(actualCurrentOpponentsList)) {
                return;
            }
        }
        proceedInitUsersList();
    }

    private void proceedInitUsersList() {
        LogUtil.writeDebugLog(TAG, "proceedInitUsersList", "1");
        currentOpponentsList = dbManager.getAllUsers();
        currentOpponentsList.remove(sharedPrefsHelper.getQbUser());
    }

    private boolean isLoggedInChat() {
        LogUtil.writeDebugLog(TAG, "isLoggedInChat", "1");
        if (!QBChatService.getInstance().isLoggedIn()) {
            Toaster.shortToast("dlg_signal_error");
            tryReLoginToChat();
            return false;
        }
        return true;
    }

    private void tryReLoginToChat() {
        LogUtil.writeDebugLog(TAG, "tryReLoginToChat", "1");
        if (sharedPrefsHelper.hasQbUser()) {
            QBUser qbUser = sharedPrefsHelper.getQbUser();
            CallService.start(this, qbUser);
        }
    }

    public void startCall(boolean isVideoCall) {
        LogUtil.writeDebugLog(TAG, "startCall", "start");
        if (isLoggedInChat()) {
            LogUtil.writeDebugLog(TAG, "startCall", "ok");

//            @LAX_CHAT
            /*QBUser user = AppGlobals.curChattingUser.getQBUser();
            Integer userId = user.getId();
            ArrayList<Integer> opponentsList = new ArrayList<Integer>();
            opponentsList.add(userId);
            QBRTCTypes.QBConferenceType conferenceType = QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO;
            QBRTCClient qbrtcClient = QBRTCClient.getInstance(getApplicationContext());
            QBRTCSession newQbRtcSession = qbrtcClient.createNewSessionWithOpponents(opponentsList, conferenceType);
            WebRtcSessionManager.getInstance(this).setCurrentSession(newQbRtcSession);*/

//        PushNotificationSender.sendPushMessage(opponentsList, currentUser.getFullName());
        }
    }

    @Override
    public void onResume() {
        LogUtil.writeDebugLog(TAG, "onResume", "1");
        super.onResume();
        googlePlayServicesHelper.checkPlayServicesAvailable(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(pushBroadcastReceiver,
                new IntentFilter(GcmConsts.ACTION_NEW_GCM_EVENT));
        initUsersList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogUtil.writeDebugLog(TAG, "onPause", "1");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(pushBroadcastReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("onActivityResult:::", "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_DIALOG_ID_FOR_UPDATE) {
                if (data != null) {
                    String dialogId = data.getStringExtra(ChatActivity.EXTRA_DIALOG_ID);
                    loadUpdatedDialog(dialogId);
                } else {
                    updateDialogsList();
                }
            } else if (requestCode == REQUEST_CREATE_GROUP) {
                Log.e("requestCode:::", "requestCode");
                if (data != null) {
                    String dialogIds = data.getStringExtra("result");
                    Log.e("dialogIds", dialogIds);
                    String[] parts = dialogIds.split(",");
                    startGroupChatActivity(parts);

                }
            }
        } else {
            updateDialogsAdapter();
        }
    }

    @Override
    protected void onDestroy() {
        overridePendingTransition(R.anim.activity_leave, R.anim.activity_enter);
        super.onDestroy();
        if (isAppSessionActive) {
            unregisterQbChatListeners();
        }
    }

    @Override
    protected View getSnackbarAnchorView() {
        return findViewById(R.id.layout_root);
    }

    @Override
    public void onSessionCreated(boolean success) {
        LogUtil.writeDebugLog(TAG, "onSessionCreated", "1");
        if (success) {
            QBUser currentUser = ChatHelper.getCurrentUser();
            if (currentUser != null) {

            }
            registerQbChatListeners();
            if (QbDialogHolder.getInstance().getDialogs().size() > 0) {
                loadDialogsFromQb(true, true);
            } else {
                loadDialogsFromQb(false, true);
            }
        }
    }

    @Override
    public void onDialogCreated(QBChatDialog chatDialog) {
        updateDialogsAdapter();
    }

    @Override
    public void onDialogUpdated(String chatDialog) {
        updateDialogsAdapter();
    }

    @Override
    public void onNewDialogLoaded(QBChatDialog chatDialog) {
        updateDialogsAdapter();
    }

    private class PushBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra(GcmConsts.EXTRA_GCM_MESSAGE);
            loadDialogsFromQb(true, true);
            LogUtil.writeDebugLog(TAG, "PushBroadcastReceiver", "onReceive");
        }
    }

    private class SystemMessagesListener implements QBSystemMessageListener {
        @Override
        public void processMessage(final QBChatMessage qbChatMessage) {
            LogUtil.writeDebugLog(TAG, "SystemMessagesListener", "processMessage");
            dialogsManager.onSystemMessageReceived(qbChatMessage);
//            NotificationUtils.showNotification(getApplicationContext(), SplashScreenActivity.class,
//                    ResourceUtils.getString(R.string.notification_title), "SystemMessagesListener",
//                    R.drawable.ic_launcher, 1);
        }

        @Override
        public void processError(QBChatException e, QBChatMessage qbChatMessage) {
            LogUtil.writeDebugLog(TAG, "SystemMessagesListener", "processError");
        }
    }

    private class AllDialogsMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(final String dialogId, final QBChatMessage qbChatMessage, Integer senderId) {
            LogUtil.writeDebugLog(TAG, "AllDialogsMessageListener", "processMessage");
            if (!senderId.equals(ChatHelper.getCurrentUser().getId())) {
                dialogsManager.onGlobalMessageReceived(dialogId, qbChatMessage);
            }
        }
    }

    private QBUser getQBUserData() {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
//        sharedPrefsHelper.save(Constant.PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
        return sharedPrefsHelper.getQbUser();
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

}
