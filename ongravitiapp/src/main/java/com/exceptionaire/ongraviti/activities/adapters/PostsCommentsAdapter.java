package com.exceptionaire.ongraviti.activities.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.Array_of_comment;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class PostsCommentsAdapter extends BaseAdapter {
    private List<Array_of_comment> commentList;
    private Activity activity;
    private List<Array_of_comment> fileredlist;

    public void setCommentList(ArrayList<Array_of_comment> comments) {
        this.commentList = comments;
        fileredlist.clear();
        fileredlist.addAll(comments);
        notifyDataSetChanged();
    }

    public PostsCommentsAdapter(List<Array_of_comment> contactList, Activity activity) {
        this.commentList = contactList;
        fileredlist = new ArrayList<>();
        this.activity = activity;
    }

    public void removeItem(int position) {
        commentList.remove(position);
        notifyDataSetChanged();
    }

    private class Holder {
        TextView txtName, tv_date_time, tv_txt;
        RoundedImageView image;
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return commentList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        Array_of_comment comment = commentList.get(position);
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.speak_out_your_mind_comment_list_item, arg2, false);
            Log.d("In custom adapter", "## adapter");
            holder.txtName = convertView.findViewById(R.id.tv_username);
            holder.tv_txt = convertView.findViewById(R.id.tv_txt);
            holder.tv_date_time = convertView.findViewById(R.id.tv_date_time);
            holder.image = convertView.findViewById(R.id.user_profile_pic);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.txtName.setText(comment.getC_name());
        holder.tv_date_time.setText(comment.getComment_date());
        try {
            if (comment.getComment_content() != null && !comment.getComment_content().equals(""))
                holder.tv_txt.setText(URLDecoder.decode(comment.getComment_content(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (comment.getProfile_picture() != null && !comment.getProfile_picture().isEmpty()) {
            if (!comment.getProfile_picture().contains("https://graph")) {
                Picasso.with(activity)
                        .load(comment.getProfile_picture())
                        .placeholder(R.drawable.profile)
                        .error(R.drawable.profile)
                        .into(holder.image);
            } else if (comment.getProfile_picture().contains("https://graph")) {
                String[] separated = comment.getProfile_picture().split("https://graph");
                String pathFB = separated[1];
                Log.d("uiiiiiiii1:", pathFB);
                Picasso.with(activity)
                        .load("https://graph" + pathFB)
                        .placeholder(R.drawable.profile)
                        .error(R.drawable.profile)
                        .into(holder.image);
            }
        }


        holder.image.setOnClickListener(v -> viewProfile());
        return convertView;
    }

    private void viewProfile() {

    }

}
