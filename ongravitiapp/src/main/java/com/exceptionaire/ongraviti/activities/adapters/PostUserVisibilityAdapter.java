package com.exceptionaire.ongraviti.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;


public class PostUserVisibilityAdapter extends BaseAdapter {
    private int userIcon[];
    private String[] visibiltyUser;
    private LayoutInflater inflter;

    public PostUserVisibilityAdapter(Context applicationContext, int[] flags, String[] countryNames) {
        this.userIcon = flags;
        this.visibiltyUser = countryNames;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return userIcon.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.post_user_visibility_item_layout, null);
        ImageView icon =  view.findViewById(R.id.imageView);
        TextView names =  view.findViewById(R.id.textView);
        icon.setImageResource(userIcon[i]);
        names.setText(visibiltyUser[i]);
        return view;
    }
}