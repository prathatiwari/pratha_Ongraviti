package com.exceptionaire.ongraviti.activities.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.core.OnGravitiApp;
import com.exceptionaire.ongraviti.model.Country;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20/6/16.
 */
public class SearchCountryListAdapter extends BaseAdapter implements Filterable {
    Activity activity;
    private List<Country> originalData = null;
    private List<Country> filteredData = null;
    private ItemFilter mFilter = new ItemFilter();

//    ImageLoader imageLoader = OnGravitiApp.getInstance().getImageLoader();
    private LayoutInflater mLayoutInflater;

    public SearchCountryListAdapter(Activity activity, List<Country> category_interestList) {
        this.activity = activity;
        this.originalData = category_interestList;
        this.filteredData = category_interestList;
    }

    @Override
    public int getCount() {
        if (filteredData == null)
            filteredData = originalData;
        return filteredData.size();
    }

    @Override
    public Object getItem(int i) {
        return filteredData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (mLayoutInflater == null)
            mLayoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.layout_custom_spinner_country_code_layout, null);
            holder = new ViewHolder();
            holder.mImageView = (ImageView) convertView.findViewById(R.id.image);
            holder.mNameView = (TextView) convertView.findViewById(R.id.country_name);
            holder.mCodeView = (TextView) convertView.findViewById(R.id.country_code);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Country country = filteredData.get(position);
        if (country != null) {
            holder.mNameView.setText(country.getName());
            holder.mCodeView.setText(country.getCountryCodeStr());
            holder.mImageView.setImageResource(country.getResId());
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private static class ViewHolder {
        public ImageView mImageView;
        public TextView mNameView;
        public TextView mCodeView;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Country> list = originalData;

            int count = list.size();
            final ArrayList<Country> nlist = new ArrayList<>();

            String filterableString;

            for (Country country : originalData) {
                filterableString = country.getName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(country);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredData = (List<Country>) results.values;
            notifyDataSetChanged();
        }
    }
}
