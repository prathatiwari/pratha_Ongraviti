package com.exceptionaire.ongraviti.activities;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.SearchedUserListAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.model.SearchData;

import java.util.ArrayList;

public class SearchedResultActivity extends BaseDrawerActivity implements View.OnClickListener {

    private ImageView imageViewBack;
    private TextView textViewNoData;
    private RecyclerView recyclerView;
    private SearchedUserListAdapter mAdapter;
    private ArrayList<SearchData> listSearchData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searched_result);
        listSearchData = new ArrayList<>();

        if (this.getIntent().getParcelableArrayListExtra("SearchedList") != null)
            listSearchData = this.getIntent().getParcelableArrayListExtra("SearchedList");

        arcMenu.setVisibility(View.GONE);

        initialiseComponent();
    }

    private void initialiseComponent() {

        imageViewBack = (ImageView) findViewById(R.id.image_view_back);
        textViewNoData = (TextView) findViewById(R.id.text_view_no_data_found);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_searched);


        mAdapter = new SearchedUserListAdapter(this, listSearchData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(SearchedResultActivity.this, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        if (listSearchData != null && listSearchData.size() > 0) {

            textViewNoData.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

        } else {

            recyclerView.setVisibility(View.GONE);
            textViewNoData.setVisibility(View.VISIBLE);
        }
        mAdapter.notifyDataSetChanged();

        imageViewBack.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.image_view_back:

                finish();
                break;
        }
    }
}