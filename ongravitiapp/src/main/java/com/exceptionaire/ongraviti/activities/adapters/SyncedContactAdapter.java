package com.exceptionaire.ongraviti.activities.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.myorbit.ContactSyncFragment;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.database.User_data;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by root on 15/11/16.
 */

public class SyncedContactAdapter extends BaseAdapter {
    private List<User_data> contactList;
    private Context context;
    //    private ImageLoader imageLoader = OnGravitiApp.getInstance().getImageLoader();
    private ContactSyncFragment fragment;

    public SyncedContactAdapter(List<User_data> contactList, Context context, ContactSyncFragment fragment) {
        this.contactList = contactList;
        this.context = context;
        this.fragment = fragment;
    }

    public void addFriendsToList(List<User_data> friendsList) {
        contactList.clear();
        contactList.addAll(friendsList);
        notifyDataSetChanged();
    }

    public void removeContact(int position) {
        contactList.remove(position);
        notifyDataSetChanged();
    }

    public void clearList() {
        contactList.clear();
        notifyDataSetChanged();
    }

    private class Holder {
        TextView txtName;
        TextView txtEmail;
        TextView txtOther;
        TextView btAddtoOrbit, btaInvite;
        RoundedImageView image;
    }

    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return contactList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final User_data user_data = contactList.get(position);
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.sync_contact_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");
            holder.txtName = (TextView) convertView.findViewById(R.id.txt_contact_name);
            holder.txtEmail = (TextView) convertView.findViewById(R.id.txt_contact_email_number);
            holder.txtOther = (TextView) convertView.findViewById(R.id.txt_contact_other);

            holder.btAddtoOrbit = (TextView) convertView.findViewById(R.id.btaddToOrbit);
            holder.btaInvite = (TextView) convertView.findViewById(R.id.buttonInvite);
            holder.image = (RoundedImageView) convertView.findViewById(R.id.user_profile_pic);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        if (user_data.getThumb() != null && !user_data.getThumb().isEmpty() && !user_data.getThumb().equals("")) {
            if (!user_data.getThumb().contains("https:")) {
                Picasso.with(context)
                        .load(AppConstants.PROFILE_PICTURE_THUMB + "/" + user_data.getThumb())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            } else if (user_data.getThumb().contains("https:")) {
                Picasso.with(context)
                        .load(user_data.getThumb())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            }
        }
//        if (user_data.getThumb() != null && !user_data.getThumb().isEmpty() && !user_data.getThumb().equals(""))
//            Picasso.with(context).load(AppConstants.PROFILE_PICTURE_THUMB + "/" + user_data.getThumb()).fit().centerCrop()
//                    .placeholder(R.drawable.profile)
//                    .error(R.drawable.profile)
//                    .into(holder.image);
//        else
//            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.profile));

        holder.btAddtoOrbit.setText(user_data.getInvite_status());

        if (user_data.getInvite_status().equals("Add Friend")) {
            holder.btAddtoOrbit.setVisibility(View.VISIBLE);
            holder.btAddtoOrbit.setText("Send Request");
            holder.btaInvite.setVisibility(View.GONE);
        } else if (user_data.getInvite_status().equals("Invite")) {
            holder.btAddtoOrbit.setVisibility(View.GONE);
            holder.btaInvite.setVisibility(View.VISIBLE);
            if (user_data.getAlreadyInvited() != null && user_data.getAlreadyInvited()) {
                holder.btaInvite.setText("Invited");
            }
        } else {
            holder.btAddtoOrbit.setVisibility(View.GONE);
            holder.btaInvite.setVisibility(View.GONE);
        }

//        holder.txtName.setText((user_data.getEmail_address() == null || user_data.getEmail_address().isEmpty() || user_data.getEmail_address().equals("")) ? user_data.getName() : user_data.getEmail_address());
//        holder.txtName.setText((user_data.getName() == null || user_data.getName().isEmpty() || user_data.getName().equals("")) ? user_data.getEmail_address() : user_data.getName());

        boolean isNameAvailable = (user_data.getName() != null && !user_data.getName().isEmpty() && !user_data.getName().equals(""));
        boolean isEmailIDAvailable = (user_data.getEmail_address() != null && !user_data.getEmail_address().isEmpty() && !user_data.getEmail_address().equals(""));

        if (isNameAvailable)
            holder.txtName.setText(user_data.getName());
        else if (isEmailIDAvailable)
            holder.txtName.setText(user_data.getEmail_address());

        holder.txtOther.setVisibility(View.INVISIBLE);

        if (user_data.getOriginal_Mobile_nos() != null && !user_data.getOriginal_Mobile_nos().isEmpty() && !user_data.getOriginal_Mobile_nos().equals("")) {
            holder.txtEmail.setText(user_data.getOriginal_Mobile_nos());
            holder.txtEmail.setVisibility(View.VISIBLE);
        } else
            holder.txtEmail.setVisibility(View.INVISIBLE);


        holder.btAddtoOrbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.sendFriendRequest(user_data.getUser_id(), position);
            }
        });

        holder.btaInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.inviteFriends(user_data, position);
            }
        });

        return convertView;
    }
}