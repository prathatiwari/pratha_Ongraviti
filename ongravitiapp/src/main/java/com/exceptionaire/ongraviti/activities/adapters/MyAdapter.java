package com.exceptionaire.ongraviti.activities.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.fragments.EditProfileILikeToDoFragment;
import com.exceptionaire.ongraviti.model.StateVO;

import java.util.ArrayList;
import java.util.List;

import static com.exceptionaire.ongraviti.activities.fragments.EditProfileILikeToDoFragment.text_view_hobbies;


public class MyAdapter extends ArrayAdapter<StateVO> {
    private Context mContext;
    private ArrayList<StateVO> listState;
    private MyAdapter myAdapter;
    private boolean isFromView = false;
    private static StringBuilder stringBuilder1;
    public ArrayList<String> selectedHobbied;

    public MyAdapter(Context context, int resource, List<StateVO> objects, ArrayList<String> selectedHobbies) {
        super(context, resource, objects);
        this.mContext = context;
        this.listState = (ArrayList<StateVO>) objects;
        selectedHobbied = selectedHobbies;
        this.myAdapter = this;
        stringBuilder1 = new StringBuilder();

    }


    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {
        StateVO stateVO = listState.get(position);
        final ViewHolder holder;


        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_items, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView
                    .findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView
                    .findViewById(R.id.checkbox);
            if(EditProfileILikeToDoFragment.listVOs.get(position).isSelected())
            {
                holder.mCheckBox.setChecked(true);
            }
            else
            {
                holder.mCheckBox.setChecked(false);
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(listState.get(position).getTitle());

        // To check weather checked event fire from getview() or user input
        isFromView = true;
      //  holder.mCheckBox.setChecked(listState.get(position).isSelected());
        //holder.mCheckBox.setChecked(EditProfileILikeToDoFragment.listVOs.get(position).isSelected());

        isFromView = false;


        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();
                Log.d("getPosition", getPosition + "");
                String ss = stateVO.getTitle().toString();
                if (holder.mCheckBox.isChecked()) {
                    selectedHobbied.add(ss);

                    EditProfileILikeToDoFragment.listVOs.get(position).setSelected(true);
                    Log.d("Test1", selectedHobbied.size() + "");
                } else {
                    EditProfileILikeToDoFragment.listVOs.get(position).setSelected(false);
                    selectedHobbied.remove(ss);
//                    if (stringBuilder1.toString().contains(EditProfileILikeToDoFragment.listVOs.get(position).getTitle())) {
//                        stringBuilder1.r(EditProfileILikeToDoFragment.listVOs.get(position).getTitle());
//                        text_view_hobbies.setText(stringBuilder1.toString());
//
//                    }

                    Log.d("Tests1", selectedHobbied.size() + "");
                }


                Log.d("getPosition", getPosition + ":" + stateVO.getTitle().toString() + ":" + selectedHobbied.size() + ":" +
                        listState.size());
                if (stringBuilder1.toString().length() > 0) {
                    stringBuilder1.append(",");
                }
//                if (!stringBuilder1.toString().contains(stateVO.getTitle().toString())) {
//                    stringBuilder1.append(stateVO.getTitle().toString());
//                    text_view_hobbies.setText(stringBuilder1.toString());
//
//                }
                if (!stringBuilder1.toString().contains(EditProfileILikeToDoFragment.listVOs.get(position).getTitle())) {
                    stringBuilder1.append(EditProfileILikeToDoFragment.listVOs.get(position).getTitle());
                    text_view_hobbies.setText(stringBuilder1.toString());

                }

            }
        });

       /* for (int i = 0; i < selectedHobbied.size(); i++) {
            Log.d("Testtt", selectedHobbied.size() + "");
            Log.d("Testttt", listState.size() + "");
            if (selectedHobbied.get(i).equals(listState.get(position).getTitle())) {
                holder.mCheckBox.setChecked(true);
            }

        }*/
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }

}