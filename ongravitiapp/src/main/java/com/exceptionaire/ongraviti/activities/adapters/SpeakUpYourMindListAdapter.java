package com.exceptionaire.ongraviti.activities.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.SpeakOutYourMindActivity;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.OnGravitiApp;
import com.exceptionaire.ongraviti.imoji.EmoticonTextView;
import com.exceptionaire.ongraviti.model.FeedInformation;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by root on 16/11/16.
 */

public class SpeakUpYourMindListAdapter extends BaseAdapter {
    private List<FeedInformation> feedInformationList;
    private SpeakOutYourMindActivity activity;
//    private ImageLoader imageLoader = OnGravitiApp.getInstance().getImageLoader();
    private List<FeedInformation> fileredlist;
    private SimpleDateFormat formatter, formatterGiven, dateFormatter, dateFormatterMM;

    public void setFeedInformationList(List<FeedInformation> feedInformations) {
        feedInformationList.clear();
        fileredlist.clear();

        feedInformationList.addAll(feedInformations);
        fileredlist.addAll(feedInformations);
        notifyDataSetChanged();
    }

    public SpeakUpYourMindListAdapter(List<FeedInformation> contactList, SpeakOutYourMindActivity activity) {
        this.feedInformationList = contactList;
        fileredlist = new ArrayList<>();
        fileredlist.addAll(contactList);
        this.activity = activity;

        formatterGiven = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter = new SimpleDateFormat("dd/MM/yyyy h:mm a");
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        dateFormatterMM = new SimpleDateFormat("dd MMM h:mm a");
    }

    public void removeItem(int position) {
        feedInformationList.remove(position);
        notifyDataSetChanged();
    }

    public void addFeed(FeedInformation feedInformation) {
        feedInformationList.add(feedInformation);
        notifyDataSetChanged();
    }

    public void addFeedInformationList(List feedInformations) {
        this.feedInformationList = feedInformations;
        fileredlist.addAll(feedInformations);
        notifyDataSetChanged();
    }

    public void addLike(int position) {
        feedInformationList.get(position).setCount_of_like(feedInformationList.get(position).getCount_of_like() + 1);
        notifyDataSetChanged();
    }

    public void addComment(int position) {
        feedInformationList.get(position).setCount_of_comment(feedInformationList.get(position).getCount_of_comment() + 1);
        notifyDataSetChanged();
    }

    public class Holder {
        TextView txtName, tv_date_time, tv_comments_count, tv_likes_count;
        EmoticonTextView tv_txt;
        ImageButton ibtn_comment, ibtn_like;
        ImageView image;
        ImageView image_view_cat_type;
    }

    @Override
    public int getCount() {
        return feedInformationList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return feedInformationList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        final FeedInformation feedInformation = feedInformationList.get(position);
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.speak_out_your_mind_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");
            // holder.tvNo = (TextView) convertView.findViewById(R.id.No);

            holder.txtName = (TextView) convertView.findViewById(R.id.tv_username);
            holder.tv_txt = (EmoticonTextView) convertView.findViewById(R.id.tv_txt);
            holder.tv_date_time = (TextView) convertView.findViewById(R.id.tv_date_time);
            holder.tv_likes_count = (TextView) convertView.findViewById(R.id.tv_likes_count);
            holder.tv_comments_count = (TextView) convertView.findViewById(R.id.tv_comments_count);
            holder.image_view_cat_type = convertView.findViewById(R.id.image_view_cat_type);
            holder.ibtn_comment = (ImageButton) convertView.findViewById(R.id.ibtn_comment);
            holder.ibtn_like = (ImageButton) convertView.findViewById(R.id.ibtn_like);
            holder.image = (RoundedImageView) convertView.findViewById(R.id.user_profile_pic);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        if (feedInformation.getIdentity().equalsIgnoreCase("1")) {
            holder.txtName.setText(feedInformation.getFeed_username());

            Picasso.with(activity)
                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + feedInformation.getFeed_user_pic())
                    .placeholder(R.drawable.profile)
                    .into(holder.image);

        } else {
            holder.txtName.setText("Anonymous");
            holder.image.setImageResource(R.drawable.profile);
        }
        if (feedInformation.getCategory_type().equals("2")) {
            holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_pink);
        } else if (feedInformation.getCategory_type().equals("3")) {
            holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_blue);
        } else if (feedInformation.getCategory_type().equals("1")) {
            holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_green);
        }
        holder.tv_likes_count.setText(Integer.toString(feedInformation.getCount_of_like()));
        holder.tv_comments_count.setText(Integer.toString(feedInformation.getCount_of_comment()));

        Date date = null;
        try {
            date = formatterGiven.parse(feedInformation.getFeed_date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date date1 = new Date();

        String dateString = formatter.format(date);

        String dateString1 = dateFormatter.format(date1);   // 15/03/2017 9:35 AM
        String dateString2 = dateFormatter.format(date);

        String[] dateFormatted = dateString.split(" ");

        Log.d("Date", dateString1 + " " + dateString2);

        if (dateString1.equalsIgnoreCase(dateString2)) {
            dateString = "Today at " + dateFormatted[1] + " " + dateFormatted[2];
        } else {
            dateString = dateFormatterMM.format(date);
            dateString = dateString.substring(0, 6) + " at " + dateString.substring(7, dateString.length());
        }
        holder.tv_date_time.setText(dateString);


//        Spanned item = Html.fromHtml(feedInformation.getFeed_text());
        try {
            holder.tv_txt.setText(URLDecoder.decode(
                    feedInformation.getFeed_text(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        holder.ibtn_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.addLike(feedInformation.getFeed_id(), position);
            }
        });

        holder.ibtn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.commentsDialog(feedInformation.getFeed_id(), position);
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewProfile();
                //context.startActivity(new Intent(context,ViewSocialBasicProfile.class));
            }
        });
        return convertView;
    }

    public static Date getFutureDay(int numberOfYears) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, numberOfYears);
        return c.getTime();
    }

    private void viewProfile() {

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        feedInformationList.clear();
        if (charText.length() == 0 || charText.equalsIgnoreCase("All")) {
            feedInformationList.addAll(fileredlist);
        } else {
            for (FeedInformation wp : fileredlist) {
                if (wp.getFeed_userid().toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText) && wp.getIdentity().equalsIgnoreCase("1")) {
                    feedInformationList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
