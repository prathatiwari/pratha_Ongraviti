package com.exceptionaire.ongraviti.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.myorbit.MyOrbitRequestsFragment;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.MyOrbitRequest;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OrbitRequestAdapter extends BaseAdapter {
    private List<MyOrbitRequest> orbit_request_list;
    Context context;
    private MyOrbitRequestsFragment fragment;

    public void setOrbitRequestList(List<MyOrbitRequest> orbitRequests) {
        this.orbit_request_list.clear();
        this.orbit_request_list = orbitRequests;
        notifyDataSetChanged();
    }

    public OrbitRequestAdapter(List<MyOrbitRequest> contactList, Context context, MyOrbitRequestsFragment fragment) {
        this.orbit_request_list = contactList;
        this.context = context;
        this.fragment = fragment;
    }

    public void removeItem(int position) {
        orbit_request_list.remove(position);
        notifyDataSetChanged();
    }

    private class Holder {
        TextView txtName, tv_mutual_friends;
        ImageButton ibtn_accept, ibtn_reject;
        RoundedImageView image;
        LinearLayout linear_view_profile;
        ImageView image_view_cat_type;
    }

    @Override
    public int getCount() {
        return orbit_request_list.size();
    }

    @Override
    public Object getItem(int arg0) {
        return orbit_request_list.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {

        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final MyOrbitRequest orbitRequest = orbit_request_list.get(position);
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.orbit_request_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");

            holder.txtName = convertView.findViewById(R.id.txt_contact_name);
            holder.tv_mutual_friends = convertView.findViewById(R.id.textview_email_address);
            holder.ibtn_accept = convertView.findViewById(R.id.ibtn_accept);
            holder.ibtn_reject = convertView.findViewById(R.id.ibtn_reject);
            holder.image = convertView.findViewById(R.id.user_profile_pic);
            holder.linear_view_profile = convertView.findViewById(R.id.linear_view_profile);
            holder.image_view_cat_type = convertView.findViewById(R.id.image_view_cat_type);
            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }

        if (orbitRequest.getProfile_picture() != null && !orbitRequest.getProfile_picture().isEmpty()) {
            if (!orbitRequest.getProfile_picture().contains("https:")) {
                Picasso.with(context)
                        .load(AppConstants.PROFILE_PICTURE_PATH + "/" + orbitRequest.getProfile_picture())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            } else if (orbitRequest.getProfile_picture().contains("https:")) {
                Picasso.with(context)
                        .load(orbitRequest.getProfile_picture())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            }
        }

        holder.txtName.setText(orbitRequest.getUser_name());
        switch (orbitRequest.getCategory_type()) {
            case "1":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_green);
                break;
            case "2":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_pink);
                break;
            case "3":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_blue);
                break;
        }

        /*if (orbitRequest.getOrbitFriendList() != null && !orbitRequest.getOrbitFriendList().isEmpty()) {
            holder.tv_mutual_friends.setText(orbitRequest.getOrbitFriendList().size() + " mutual friend(s)");
        }*/

        holder.ibtn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.ibtn_accept.setEnabled(false);
                holder.ibtn_accept.postDelayed(new Runnable() {
                    public void run() {
                        holder.ibtn_accept.setEnabled(true);
                    }
                }, 6000);
                fragment.acceptRejectOrbitRequest(orbitRequest.getUser_id(), "0", position, "Accepting");
            }

        });
        holder.ibtn_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.ibtn_accept.setEnabled(false);
                holder.ibtn_accept.postDelayed(new Runnable() {
                    public void run() {
                        holder.ibtn_accept.setEnabled(true);
                    }
                }, 6000);
                fragment.acceptRejectOrbitRequest(orbitRequest.getUser_id(), "2", position, "Rejecting");
            }

        });
        holder.linear_view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("adptr", "## image click : " + orbitRequest.getUser_id());
                Intent intentProfile = new Intent(context, FavoriteUserInfoActivity.class);
                intentProfile.putExtra("ActivityName", "OrbitRequest");
                intentProfile.putExtra("OrbitUserRequestID", "" + orbitRequest.getUser_id());
                context.startActivity(intentProfile);
            }
        });

        return convertView;
    }
}
