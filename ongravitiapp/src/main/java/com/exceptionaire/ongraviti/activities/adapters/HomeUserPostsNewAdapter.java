package com.exceptionaire.ongraviti.activities.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.ProfileMainActivity;
import com.exceptionaire.ongraviti.activities.VideoBioActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.imoji.EmojiconEditText;
import com.exceptionaire.ongraviti.imoji.EmoticonTextView;
import com.exceptionaire.ongraviti.listener_interface.OnLoadMoreListener;
import com.exceptionaire.ongraviti.model.Postinfo;
import com.exceptionaire.ongraviti.model.ResponseUpdateStatus;
import com.exceptionaire.ongraviti.model.deletePostResponse;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.BitmapTransform;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.squareup.picasso.Picasso;

import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_SUCCESS;
import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;


public class HomeUserPostsNewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String POPUP_CONSTANT = "mPopup";
    private static String POPUP_FORCE_SHOW_ICON = "setForceShowIcon";
    private ArrayList<Postinfo> dataSet;
    private Context context;
    private final int TYPE_LOAD = 1;
    private final int TYPE_FEED = 2;
    private final int TYPE_RSS = 3;
    private boolean isLoading = false, isMoreDataAvailable = true;

    private OnLoadMoreListener loadMoreListener;
    private static final int MAX_WIDTH = 1024;
    private static final int MAX_HEIGHT = 768;
    int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));
    public static ImageLoader imageLoader = ImageLoader.getInstance();


    public static class RSSTypeViewHolder extends RecyclerView.ViewHolder {

        ImageView img_more_options;
        TextView textViewRSSHeading, txtDate;
        WebView textRSSDesc;
        TextView textViewReadMore;
        RoundedImageView imageRSSLogo;

        RSSTypeViewHolder(View itemView) {
            super(itemView);

            imageRSSLogo = itemView.findViewById(R.id.image_view_rss_logo);
            textViewRSSHeading = itemView.findViewById(R.id.text_view_rss_heading);
            textRSSDesc = itemView.findViewById(R.id.emojicon_text_view_rss_description);
            txtDate = itemView.findViewById(R.id.txtDate);
            img_more_options = itemView.findViewById(R.id.img_more_options);
            textViewReadMore = itemView.findViewById(R.id.text_view_read_more);
        }
    }

    private class LoadHolder extends RecyclerView.ViewHolder {
        private LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public static class FeedTypeViewHolder extends RecyclerView.ViewHolder {
        boolean isLike;
        ImageView videoPlayImageButton, imgComment, imgUnlike, img_more_options;
        TextView txtPName, txtDate, txtLikeCount, txtCommentCount;
        EmoticonTextView txtPostDescription;
        ImageView imageViewPostImageVideoThumbnail;
        FrameLayout frame_Video;
        View seperator_view;
        RelativeLayout relative_post_bottom;
        RoundedImageView imgProfile;
        ImageView image_view_cat_type;

        FeedTypeViewHolder(View itemView) {
            super(itemView);
            frame_Video = itemView.findViewById(R.id.frame_Video);
            relative_post_bottom = itemView.findViewById(R.id.relative_post_bottom);
            seperator_view = itemView.findViewById(R.id.seperator_view);
            image_view_cat_type = itemView.findViewById(R.id.image_view_cat_type);
            videoPlayImageButton = itemView.findViewById(R.id.video_play_img_btn);
            imageViewPostImageVideoThumbnail = itemView.findViewById(R.id.video_feed_item_video_image);
            imgProfile = itemView.findViewById(R.id.imgProfilePic);
            imgComment = itemView.findViewById(R.id.imgComment);
            imgUnlike = itemView.findViewById(R.id.imgUnlike);
            txtPName = itemView.findViewById(R.id.txtProfileName);
            txtPostDescription = itemView.findViewById(R.id.expTxtProfileDesc);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtLikeCount = itemView.findViewById(R.id.txtLikeCount);
            txtCommentCount = itemView.findViewById(R.id.txtCommentCount);
            img_more_options = itemView.findViewById(R.id.img_more_options);

            if (!imageLoader.isInited()) {
                imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
            }
        }

    }

    public HomeUserPostsNewAdapter(ArrayList<Postinfo> data, Context context) {
        this.dataSet = data;
        this.context = context;
        int total_types = dataSet.size();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void setListUserPost(List<Postinfo> userPosts) {
//        this.dataSet.clear();
//        this.dataSet.addAll(userPosts);
        notifyDataSetChanged();
    }

    public void setListUserPost(List<Postinfo> userPosts, int addedPosition) {
//        this.dataSet.clear();
//        this.dataSet.addAll(userPosts);
        notifyItemInserted(addedPosition);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case TYPE_FEED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed_type, parent, false);
                return new FeedTypeViewHolder(view);
            case TYPE_RSS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rss_type, parent, false);
                return new RSSTypeViewHolder(view);
            case TYPE_LOAD:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.dialog_progress_view_for_load_more, parent, false);
                return new LoadHolder(view);
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {

        if (dataSet != null && dataSet.size() > 0 && dataSet.size() > position) {

            if (dataSet.get(position) != null && dataSet.get(position).getIsLoadItem() != null) {
                if (dataSet.get(position).getIsLoadItem().equals("load")) {
                    Log.i("LOad Item ---->", "loading =" + position);
                    return TYPE_LOAD;
                }
            } else if (dataSet.get(position) != null && dataSet.get(position).getRes_type() != null) {
                if (dataSet.get(position).getRes_type().equals("FEEDS"))
                    return TYPE_FEED;
                else if (dataSet.get(position).getRes_type().equals("RSS"))
                    return TYPE_RSS;
            }
        }
        return TYPE_LOAD;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        switch (getItemViewType(position)) {
            case TYPE_FEED:
                BindFeedTypeViewHolder(holder, position);
                break;

            case TYPE_RSS:
                bindRSSTypeViewHolder(holder, position);
                break;

        }
    }

    private void bindRSSTypeViewHolder(RecyclerView.ViewHolder holder, int position) {
        Postinfo postinfo = dataSet.get(position);

        ((RSSTypeViewHolder) holder).textViewRSSHeading.setText(postinfo.getPosts_video());
        ((RSSTypeViewHolder) holder).imageRSSLogo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rss_feed));
        final String mimeType = "text/html";
        final String encoding = "UTF-8";

        ((RSSTypeViewHolder) holder).textRSSDesc.setVerticalScrollBarEnabled(false);
        ((RSSTypeViewHolder) holder).textRSSDesc.setHorizontalScrollBarEnabled(false);
        ((RSSTypeViewHolder) holder).textRSSDesc.loadDataWithBaseURL("http://news.google.com/news/", postinfo.getPost_content(), mimeType, encoding, "");
        if (postinfo.getPost_date() != null)
            ((RSSTypeViewHolder) holder).txtDate.setText(postinfo.getPost_date());

        ((RSSTypeViewHolder) holder).textViewReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(postinfo.getPost_thumb()));
                context.startActivity(i);
            }
        });
    }

    private void BindFeedTypeViewHolder(RecyclerView.ViewHolder holder, int position) {
        Postinfo postinfo = dataSet.get(position);
        switch (postinfo.getCategory_type()) {
            case "2":
                ((FeedTypeViewHolder) holder).image_view_cat_type.setImageResource(R.drawable.ic_circle_pink);
                break;
            case "3":
                ((FeedTypeViewHolder) holder).image_view_cat_type.setImageResource(R.drawable.ic_circle_blue);
                break;
            case "1":
                ((FeedTypeViewHolder) holder).image_view_cat_type.setImageResource(R.drawable.ic_circle_green);
                break;
        }
        ((FeedTypeViewHolder) holder).txtPName.setText(postinfo.getPosted_by_username());
        if (postinfo.getposted_by_userpic_profile_picture() != null &&
                !postinfo.getposted_by_userpic_profile_picture().isEmpty()
                && !postinfo.getposted_by_userpic_profile_picture().equals("")) {
            Log.d("ddddddddd", postinfo.getposted_by_userpic_profile_picture());

            if (!postinfo.getposted_by_userpic_profile_picture().contains("https://graph")) {
                Picasso.with(context)
                        .load(postinfo.getposted_by_userpic_profile_picture())
                        .placeholder(R.drawable.profile).error(R.drawable.profile)
                        .into(((FeedTypeViewHolder) holder).imgProfile);
                //Comment text is getting copied  for solving that issue add bellow two lines
                ((FeedTypeViewHolder) holder).txtPostDescription.setVisibility(View.GONE);
                ((FeedTypeViewHolder) holder).imgUnlike.setImageResource(R.drawable.unlike);
            } else if (postinfo.getposted_by_userpic_profile_picture().contains("https://graph")) {
                String[] separated = postinfo.getposted_by_userpic_profile_picture().split("https://graph");
                String pathFB = separated[1];
                Picasso.with(context)
                        .load("https://graph" + pathFB)
                        .placeholder(R.drawable.profile)
                        .error(R.drawable.profile)
                        .into(((FeedTypeViewHolder) holder).imgProfile);
            }


        }
        if (postinfo.getPost_date() != null)
            ((FeedTypeViewHolder) holder).txtDate.setText(postinfo.getPost_date());
        if (postinfo.getPost_content() != null) {
            try {
                if (!postinfo.getPost_content().equals("")) {
                    ((FeedTypeViewHolder) holder).txtPostDescription.setVisibility(View.VISIBLE);
                    ((FeedTypeViewHolder) holder).txtPostDescription.setText(URLDecoder.decode(
                            postinfo.getPost_content(), "UTF-8"));
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (postinfo.getCount_of_like() != null && !postinfo.getCount_of_like().isEmpty())
            ((FeedTypeViewHolder) holder).txtLikeCount.setText(postinfo.getCount_of_like());

        if (postinfo.getCount_of_comments() != null && !postinfo.getCount_of_comments().isEmpty())
            ((FeedTypeViewHolder) holder).txtCommentCount.setText(postinfo.getCount_of_comments());

        if (postinfo.getPosts_video() == null || postinfo.getPosts_video().isEmpty()) {
            if (postinfo.getPosts_pic() == null || postinfo.getPosts_pic().isEmpty()) {
                ((FeedTypeViewHolder) holder).videoPlayImageButton.setVisibility(View.GONE);
                ((FeedTypeViewHolder) holder).imageViewPostImageVideoThumbnail.setVisibility(View.GONE);
            } else {
                ((FeedTypeViewHolder) holder).videoPlayImageButton.setVisibility(View.GONE);
                ((FeedTypeViewHolder) holder).imageViewPostImageVideoThumbnail.setVisibility(View.VISIBLE);

                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .showStubImage(R.drawable.placeholder)
                        .showImageForEmptyUri(R.drawable.placeholder)
                        .resetViewBeforeLoading(true)
                        .cacheInMemory()
                        .cacheOnDisc()
                        .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                        .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                        .displayer(new SimpleBitmapDisplayer()) // default
                        .build();
                imageLoader.displayImage(postinfo.getPosts_pic(), ((FeedTypeViewHolder) holder).imageViewPostImageVideoThumbnail, options);


//                Glide.with(context)
//                        .load(postinfo.getPosts_pic())
////                        .resize(1500, 1500)
//                        .placeholder(R.drawable.placeholder).error(R.drawable.placeholder)
//                        .into(((FeedTypeViewHolder) holder).imageViewPostImageVideoThumbnail);
                ((FeedTypeViewHolder) holder).imageViewPostImageVideoThumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //open next activity and play video in landscape mode
                        if (postinfo.getPosts_pic() != null || !postinfo.getPosts_pic().isEmpty()) {
                            new CommonDialogs().displayImageZoomDialog((HomeUserPostsActivity) context, postinfo.getPosts_pic());
                        }
                    }
                });
            }
        } else {
            ((FeedTypeViewHolder) holder).videoPlayImageButton.setVisibility(View.VISIBLE);
            ((FeedTypeViewHolder) holder).imageViewPostImageVideoThumbnail.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load(postinfo.getPost_thumb())
                    .placeholder(R.drawable.placeholder).error(R.drawable.placeholder)
                    .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                    .resize(size, size)
                    .centerInside()
                    .into(((FeedTypeViewHolder) holder).imageViewPostImageVideoThumbnail);
        }


        ((FeedTypeViewHolder) holder).videoPlayImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open next activity and play video in landscape mode

                Intent intentPlay = new Intent(context, VideoBioActivity.class);
                intentPlay.putExtra("videopath", "" + postinfo.getPosts_video());
                context.startActivity(intentPlay);
            }
        });
        ((FeedTypeViewHolder) holder).img_more_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (postinfo.getPost_user_id().equals(AppSettings.getLoginUserId())) {
                    //creating a popup menu
                    PopupMenu popup1 = new PopupMenu(context, view);
                    try {
                        Field[] fields = popup1.getClass().getDeclaredFields();
                        for (Field field : fields) {
                            if (field.getName().equals(POPUP_CONSTANT)) {
                                field.setAccessible(true);
                                Object menuPopupHelper = field.get(popup1);
                                Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                                Method setForceIcons = classPopupHelper.getMethod(POPUP_FORCE_SHOW_ICON, boolean.class);
                                setForceIcons.invoke(menuPopupHelper, true);

                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    popup1.getMenuInflater().inflate(R.menu.popup_menu, popup1.getMenu());

                    popup1.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.item_edit_post:
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                    String currentDateandTime = format.format(new Date());
                                    Log.d("ajahsasja", currentDateandTime);
                                    Dialog dialogStatus = new CommonDialogs().dialogEditPost((Activity) context);
                                    EditText editTextStatus = dialogStatus.findViewById(R.id.edit_text_status);
                                    editTextStatus.setHint("Write something here...");
                                    try {
                                        editTextStatus.setText(URLDecoder.decode(
                                                postinfo.getPost_content(), "UTF-8"));
                                        editTextStatus.setSelection(editTextStatus.getText().length());
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                    dialogStatus.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            editTextStatus.setError(null);

                                            if (editTextStatus.getText().toString().length() == 0) {
                                            } else {
                                                try {
                                                    ((HomeUserPostsActivity) context).setEditPost(postinfo.getPost_id(), postinfo.getVisiblity_status(), URLEncoder.encode(editTextStatus.getText().toString(), HTTP.UTF_8),
                                                            "", currentDateandTime);
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                                dialogStatus.dismiss();
                                            }
                                        }
                                    });
                                    dialogStatus.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialogStatus.dismiss();
                                        }
                                    });


                                    break;
                                case R.id.item_delete_post:

                                    Dialog dialog = new CommonDialogs().dialogAcceptReject((Activity) context, context.getResources().getString(R.string.ongravity), "Are you sure you want to delete this post?");
                                    ((Button) dialog.findViewById(R.id.button_ok)).setText("Yes");
                                    ((Button) dialog.findViewById(R.id.button_cancel)).setText("No");
                                    dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            deleteUserPost(postinfo.getPost_id(), postinfo.getPost_user_id());
                                            removeAt(((FeedTypeViewHolder) holder).getAdapterPosition());
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                        }
                                    });

                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup1.show();


                } else {
                    Log.d(((BaseDrawerActivity) context).LOG_TAG, "## else match");
                    //creating a popup menu
                    PopupMenu popup = new PopupMenu(context, ((FeedTypeViewHolder) holder).img_more_options);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.popup_other_menu);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.item_report_abuse:

                                    Dialog dialog = new CommonDialogs().dialogAcceptReject((Activity) context, context.getResources().getString(R.string.ongravity), "Are you sure you want to report this post as abuse?");
                                    ((Button) dialog.findViewById(R.id.button_ok)).setText("Yes");
                                    ((Button) dialog.findViewById(R.id.button_cancel)).setText("No");
                                    dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            ((HomeUserPostsActivity) context).reportAbusePost(AppSettings.getLoginUserId(), postinfo.getPost_id(), postinfo.getPost_user_id());

                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                        }
                                    });

                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                }
            }
        });

        int n = Integer.parseInt(postinfo.getCount_of_like());
        if (n >= 1) {
            ((FeedTypeViewHolder) holder).imgUnlike.setImageResource(R.drawable.like);
        }
        ((FeedTypeViewHolder) holder).imgUnlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FeedTypeViewHolder) holder).imgUnlike.setImageResource(R.drawable.like);
                ((HomeUserPostsActivity) context).saveUserPostLike(postinfo.getPost_id(), position, ((FeedTypeViewHolder) holder));

            }
        });

        ((FeedTypeViewHolder) holder).imgComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeUserPostsActivity) context).getPostComments(postinfo.getPost_id(), "0");
            }
        });
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return false;
        }
        NetworkInfo.State networkState = networkInfo.getState();
        return (networkState == NetworkInfo.State.CONNECTED || networkState == NetworkInfo.State.CONNECTING);
    }


    public void deleteUserPost(String postId, String userId) {
//        CustomProgressDialog progressDialog;
//        progressDialog = new CustomProgressDialog(context, context.getResources().getString(R.string.loading));
//        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        progressDialog.show();

        try {
            if (isNetworkAvailable()) {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface loginRequest = retrofit.create(ApiInterface.class);
                Call<deletePostResponse> userPostsCall = loginRequest.delete_user_post(userId, postId);
                userPostsCall.enqueue(new Callback<deletePostResponse>() {
                    @Override
                    public void onResponse(Call<deletePostResponse> call, Response<deletePostResponse> response) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_LONG).show();
//                            CommonDialogs.dialog_with_one_btn_without_title(HomeUserPostsActivity.this, response.body().getMsg());
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_LONG).show();
//                            CommonDialogs.dialog_with_one_btn_without_title(HomeUserPostsActivity.this, response.body().getMsg());
                        }
                    }

                    @Override
                    public void onFailure(Call<deletePostResponse> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } else {
                CommonDialogs.dialog_with_one_btn_without_title((Activity) context, context.getResources().getString(R.string.check_internet));
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public void addLike(int position, FeedTypeViewHolder holder, String likeCount, String message) {
        dataSet.get(position).setCount_of_like(likeCount);
        if (message.equals("Unlike this post.")) {
            dataSet.get(position).setSelf_like_status("0");
            holder.imgUnlike.setImageResource(R.drawable.like);
            holder.isLike = false;
        } else {
            dataSet.get(position).setSelf_like_status("1");
            holder.imgUnlike.setImageResource(R.drawable.unlike);
            holder.isLike = true;
        }
        notifyItemChanged(position);
    }


    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    private void removeAt(int position) {
        dataSet.remove(position);
        notifyItemRemoved(position);

    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void addComment(int position) {
        dataSet.get(position).setCount_of_comments(dataSet.get(position).getCount_of_comments() + 1);
        notifyDataSetChanged();
    }// Alert highlight fields

    public void setErrorMsg(String msg, EditText et, boolean isRequestFocus) {
        int ecolor = Color.RED; // whatever color you want
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        if (isRequestFocus) {
            et.requestFocus();
        }

        et.setError(ssbuilder);
    }


}
