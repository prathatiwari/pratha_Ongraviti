package com.exceptionaire.ongraviti.activities.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.Comment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 16/11/16.
 */

public class SpeakUpYourMindCommentListAdapter extends BaseAdapter {
    private List<Comment> commentList;
    private Activity activity;
//    private ImageLoader imageLoader = OnGravitiApp.getInstance().getImageLoader();
    private List<Comment> fileredlist;

    public void setCommentList(ArrayList<Comment> comments) {
        this.commentList = comments;
        fileredlist.clear();
        fileredlist.addAll(comments);
        notifyDataSetChanged();
    }

    public SpeakUpYourMindCommentListAdapter(List<Comment> contactList, Activity activity) {
        this.commentList = contactList;
        fileredlist = new ArrayList<>();
        this.activity = activity;
    }

    public void removeItem(int position) {
        commentList.remove(position);
        notifyDataSetChanged();
    }

    private class Holder {
        TextView txtName,tv_date_time,tv_txt;
        RoundedImageView image;
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return commentList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        Comment comment = commentList.get(position);
        if (convertView == null) {
            holder = new SpeakUpYourMindCommentListAdapter.Holder();
            convertView = layoutInflater.inflate(R.layout.speak_out_your_mind_comment_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");
            // holder.tvNo = (TextView) convertView.findViewById(R.id.No);

            holder.txtName = (TextView) convertView.findViewById(R.id.tv_username);
            holder.tv_txt= (TextView) convertView.findViewById(R.id.tv_txt);
            holder.tv_date_time = (TextView) convertView.findViewById(R.id.tv_date_time);

            holder.image = (RoundedImageView) convertView.findViewById(R.id.user_profile_pic);
            convertView.setTag(holder);
        } else {
            holder = (SpeakUpYourMindCommentListAdapter.Holder) convertView.getTag();
        }
        holder.txtName.setText(comment.getFirst_name());
        holder.tv_date_time.setText(comment.getFeed_comment_date());
        holder.tv_txt.setText(comment.getFeed_comment_text());

        Picasso.with(activity)
                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + comment.getPicture())
                .placeholder(R.drawable.profile)
                .into(holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewProfile();
                //context.startActivity(new Intent(context,ViewSocialBasicProfile.class));
            }
        });
        return convertView;
    }

    private void viewProfile() {

    }

}
