package com.exceptionaire.ongraviti.activities.base.search;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.myorbit.AddPeopleToOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.SearchData;
import com.squareup.picasso.Picasso;


public class AutocompleteCustomArrayAdapter extends ArrayAdapter<SearchData> {

    final String TAG = "AutocompleteCustomArrayAdapter.java";

    Context mContext;
    int layoutResourceId;
    SearchData data[] = null;
    int activityNumber = 0;
//    private ImageLoader imageLoader = OnGravitiApp.getInstance().getImageLoader();

    public AutocompleteCustomArrayAdapter(Context mContext, int activityNumber, int layoutResourceId, SearchData[] data) {

        super(mContext, layoutResourceId, data);
        this.activityNumber = activityNumber;
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        try {
            /*
             * The convertView argument is essentially a "ScrapView" as described is Lucas post 
             * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
             * It will have a non-null value when ListView is asking you recycle the row layout. 
             * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
             */
            if (convertView == null) {
                // inflate the layout
                LayoutInflater inflater;
                if (activityNumber == 1)
                    inflater = ((BaseDrawerActivity) mContext).getLayoutInflater();
                else
                    inflater = ((AddPeopleToOrbitActivity) mContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }

            // object item based on the position
            final SearchData objectItem = data[position];

            // get the TextView and then set the text (item name) and tag (item ID) values
            TextView textViewItem = (TextView) convertView.findViewById(R.id.txt_orbit_name);
            textViewItem.setText(objectItem.getC_name());
            RelativeLayout relativeLayout = convertView.findViewById(R.id.relative_layout_searched_user);
            RoundedImageView image = (RoundedImageView) convertView.findViewById(R.id.user_profile_pic);
            TextView textview_email_address = (TextView) convertView.findViewById(R.id.textview_email_address);
            Button buttonInvite = (Button) convertView.findViewById(R.id.buttonInvite);
            TextView view_profile = convertView.findViewById(R.id.view_profile);
            if (objectItem.getEmail_address() != null && !objectItem.getEmail_address().isEmpty() && !objectItem.getEmail_address().equals(""))
                textview_email_address.setText(objectItem.getEmail_address());
            else if (objectItem.getFull_contact_number() != null && !objectItem.getFull_contact_number().isEmpty() && !objectItem.getFull_contact_number().equals(""))
                textview_email_address.setText(objectItem.getFull_contact_number());
            else textview_email_address.setText("No contact info available!");
            if (objectItem.getorbit_status().equals("1")) {
                buttonInvite.setVisibility(View.INVISIBLE);
                view_profile.setVisibility(View.VISIBLE);
            } else {
                buttonInvite.setVisibility(View.VISIBLE);
                view_profile.setVisibility(View.INVISIBLE);
            }


            if (objectItem.getProfile_picture() != null && !objectItem.getProfile_picture().isEmpty()) {
                if (!objectItem.getProfile_picture().contains("https:")) {
                    Picasso.with(mContext)
                            .load(AppConstants.PROFILE_PICTURE_PATH + "/" + objectItem.getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(image);
                } else if (objectItem.getProfile_picture().contains("https:")) {
                    Picasso.with(mContext)
                            .load(objectItem.getProfile_picture())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(image);
                }
            }

            buttonInvite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((AddPeopleToOrbitActivity) mContext).sendFriendRequest(objectItem.getUser_id());
                    Log.d("objectItem.getUser_id()", objectItem.getUser_id() + "");
                    Log.d("objectItem.getUser_id1", AppSettings.getLoginUserId() + "");
                }
            });
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentProfile = new Intent(mContext, FavoriteUserInfoActivity.class);
                    intentProfile.putExtra("ActivityName", "SearchOrbitProfile");
                    intentProfile.putExtra("OrbitSearchUserID", "" + objectItem.getUser_id());
                    mContext.startActivity(intentProfile);
                }
            });

            view_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentProfile = new Intent(mContext, FavoriteUserInfoActivity.class);
                    intentProfile.putExtra("ActivityName", "SearchOrbitProfile");
                    intentProfile.putExtra("OrbitSearchUserID", "" + objectItem.getUser_id());
                    mContext.startActivity(intentProfile);
                }
            });
            // in case you want to add some style, you can do something like:

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;

    }
}