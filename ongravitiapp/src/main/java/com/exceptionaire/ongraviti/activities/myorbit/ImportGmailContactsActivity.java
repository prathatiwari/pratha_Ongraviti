package com.exceptionaire.ongraviti.activities.myorbit;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.core.GetAccessToken;
import com.exceptionaire.ongraviti.core.GoogleConstants;
import com.exceptionaire.ongraviti.database.FriendContact;
import com.exceptionaire.ongraviti.model.Gcontacts;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.contacts.ContactEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 12/8/16.
 */
public class ImportGmailContactsActivity extends Activity {
    final String TAG = getClass().getName();
    private Dialog auth_dialog;
    String key;
    public Context mcontex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = this.getSharedPreferences(this.getPackageName(), Context.MODE_PRIVATE);
        mcontex = ImportGmailContactsActivity.this;
        launchAuthDialog();
    }

    private void launchAuthDialog() {
        final Context context = this;
        auth_dialog = new Dialog(context);
        auth_dialog.setCancelable(true);
        auth_dialog.setContentView(R.layout.auth_dialog);
        auth_dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        WebView web = (WebView) auth_dialog.findViewById(R.id.webv);
        web.getSettings().setJavaScriptEnabled(true);
        web.loadUrl(GoogleConstants.OAUTH_URL + "?redirect_uri=" + GoogleConstants.REDIRECT_URI + "&response_type=code&client_id=" + GoogleConstants.CLIENT_ID + "&scope=" + GoogleConstants.OAUTH_SCOPE);
        web.setWebViewClient(new WebViewClient() {
            boolean authComplete = false;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (url.contains("?code=") && authComplete != true) {
                    Uri uri = Uri.parse(url);
                    String authCode = uri.getQueryParameter("code");
                    authComplete = true;
                    auth_dialog.dismiss();
                    new GoogleAuthToken(context).execute(authCode);
                } else if (url.contains("error=access_denied")) {
                    Log.i("", "ACCESS_DENIED_HERE");
                    authComplete = true;
                    auth_dialog.dismiss();
                    finish();
                }
            }
        });
        auth_dialog.show();
    }

    private class GoogleAuthToken extends AsyncTask<String, String, JSONObject> {
        private ProgressDialog pDialog;
        private Context context;

        public GoogleAuthToken(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Contacting Google ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    finish();
                }
            });
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            String authCode = args[0];
            GetAccessToken jParser = new GetAccessToken();
            JSONObject json = jParser.gettoken(GoogleConstants.TOKEN_URL, authCode, GoogleConstants.CLIENT_ID, GoogleConstants.CLIENT_SECRET, GoogleConstants.REDIRECT_URI, GoogleConstants.GRANT_TYPE);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            pDialog.dismiss();
            if (json != null) {
                try {
                    String tok = json.getString("access_token");
                    String expire = json.getString("expires_in");
                    String refresh = json.getString("refresh_token");
                    ContactsService contactsService = new ContactsService(GoogleConstants.APP);
                    contactsService.setHeader("Authorization", "Bearer " + tok);
                    contactsService.setHeader("GData-Version", "3.0");
                    List<ContactEntry> contactEntries = null;
                    try {
                        String uri = GoogleConstants.CONTACTS_URL + "access_token=" + tok + "&alt=json" + "&max-results=1000000000";
                        Log.e("URL", uri);
                        URL feedUrl = new URL(uri);
                        getContacts(uri);
                        //  ContactFeed resultFeed = contactsService.getFeed(feedUrl, ContactFeed.class);
                /*contactEntries = resultFeed.getEntries();*/
                    } catch (Exception e) {
                        pDialog.dismiss();
                        Toast.makeText(context, "Failed to get Contacts", Toast.LENGTH_SHORT).show();
                    }
                    // new GetGoogleContacts(context).execute(tok);
                    //getContacts();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void getContacts(String url) {

            final StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            JSONObject jObject;
                            Log.e("response====", "" + response);

                            List contactLists = new ArrayList<Gcontacts>();

                            try {
                                jObject = new JSONObject(response);
                                String feed = jObject.getJSONObject("feed").getJSONArray("entry").toString();
                                JSONArray jarray = new JSONArray(feed);
                                Gcontacts contact = null;
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject obj = jarray.getJSONObject(i);
                                    contact = new Gcontacts();
                                    if (obj.has("title")) {
                                        JSONObject obj2 = obj.getJSONObject("title");
                                        Log.e("NAME: ", obj2.getString("$t"));
                                        String names = obj2.getString("$t");
                                        contact.setName(names);
                                        contactLists.add(contact);

                                    }

                                    if (obj.has("gd$phoneNumber")) {
                                        JSONArray arr = obj.getJSONArray("gd$phoneNumber");
                                        for (int j = 0; j < arr.length(); j++) {
                                            JSONObject ob = arr.getJSONObject(j);
                                            Log.e("PH : ", ob.getString("$t"));
                                            String phoneNo = ob.getString("$t");
                                            contact.setMobile_nos(phoneNo);
                                            contactLists.add(contact);
                                        }
                                    }
                                    if (obj.has("gd$email")) {
                                        JSONArray arr = obj.getJSONArray("gd$email");
                                        for (int j = 0; j < arr.length(); j++) {
                                            JSONObject ob = arr.getJSONObject(j);
                                            Log.e("EMAIL : ", ob.getString("address"));
                                            String email = ob.getString("address");
                                            contact.setEmail(email);
                                            contactLists.add(contact);
                                        }
                                    }
                                    Log.e("", "----------------------------------");
                                }
                                System.out.println(contactLists);
                                LinkedHashSet<Gcontacts> lhs = new LinkedHashSet<Gcontacts>();
                                lhs.addAll(contactLists);
                                contactLists.clear();
                                contactLists.addAll(lhs);
                                Log.e("Feed=====", feed);


                                System.out.println("After:");
                                System.out.println("ArrayList contains: " + contactLists);
                                // setContactList(contactLists);
                                //Call for comparUsers API
                                if (!contactLists.isEmpty()) {
                                    syncCompareContacts(contactLists);
                                } else {
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }

                                // userFriendLists();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            System.out.println("error");
                            Toast.makeText(ImportGmailContactsActivity.this, "error", Toast.LENGTH_LONG).show();
                        }
                    }) {
                protected Map<String, String> getParams() throws AuthFailureError {
                    return null;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        }

    }

    private void syncCompareContacts(List<Gcontacts> contactLists) {
        String someurl = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/compareUsers";
        final JSONArray array = new JSONArray();
        JSONObject jsonObject = null;
        try {
            JSONObject j = new JSONObject();

            for (int i = 0; i < contactLists.size(); i++) {

                jsonObject = new JSONObject();
                if (contactLists.get(i).getEmail() == null) {
                    jsonObject.put("Mobile_nos", contactLists.get(i).getMobile_nos());
                    jsonObject.put("id", String.valueOf(contactLists.get(i).getId()));
                    jsonObject.put("name", contactLists.get(i).getName());

                } else {
                    jsonObject.put("email", contactLists.get(i).getEmail());
                    jsonObject.put("id", String.valueOf(contactLists.get(i).getId()));
                    jsonObject.put("name", contactLists.get(i).getName());
                }
                array.put(jsonObject);
            }
            key = String.valueOf(j.put("data", array));
            Log.e("JSON", j.toString());
//            Log.e("Array",""+arrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, someurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("RESPONSE============", response);
                        //{"status":"success","user_id":null,"user_data":[],"Need_to_orbit_request":null}
                        // no one is register to ongraviti from ur gmail friend list
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);

                            String status = jObject.getString("status");
                            //message = jObject.getString("msg");
                            String message = "";

                            if (status.equals("success") && jObject.has("user_id")) {

                                ArrayList friendLists = new ArrayList<FriendContact>();
                                if (!jObject.get("user_data").equals(null)) {
                                    JSONArray FriendContact = jObject.getJSONArray("user_data");
                                    for (int i = 0; i < FriendContact.length(); i++) {
                                        JSONObject detailsObject = FriendContact.getJSONObject(i);
                                        com.exceptionaire.ongraviti.database.FriendContact friendContact = new FriendContact();
                                        String username = detailsObject.getString("name");
                                        String userid = detailsObject.getString("user_id");
                                        String emailid = detailsObject.getString("email_address");
                                        String contactNo = detailsObject.getString("contact_number");
                                        String quickbloxId = detailsObject.getString("quickblox_userid");
                                        String invite_status = detailsObject.getString("invite_status");
                                        Log.e("USERNAME========", username);
                                        Log.e("userid========", userid);
                                        Log.e("emailid========", emailid);
                                        Log.e("ContactNo========", contactNo);
                                        Log.e("Quickblox========", quickbloxId);
                                        friendContact.setName(username);
                                        friendContact.setUser_id(userid);
                                        friendContact.setEmail_address(emailid);
                                        friendContact.setContact_number(contactNo);
                                        friendContact.setQuickblox_id(quickbloxId);
                                        friendContact.setRegistered_flag(invite_status);
                                        friendLists.add(friendContact);
                                    }
                                }

                                if (!jObject.get("Need_to_orbit_request").equals(null)) {
                                    JSONArray notOnOnGravtiti = jObject.getJSONArray("Need_to_orbit_request");
                                    for (int i = 0; i < notOnOnGravtiti.length(); i++) {
                                        JSONObject detailsObject = notOnOnGravtiti.getJSONObject(i);
                                        FriendContact friendContact = new FriendContact();
                                        String username = detailsObject.getString("name");
                                        //String userid = detailsObject.getString("user_id");

                                        String emailid = "";
                                        if (detailsObject.has("email_address")) {
                                            if (detailsObject.getString("email_address") != null && !detailsObject.getString("email_address").isEmpty() && !detailsObject.getString("email_address").equals("")) {
                                                emailid = detailsObject.getString("email_address");
                                            }
                                        }

                                        String contactNo = "";
                                        if (detailsObject.has("contact_number")) {
                                            if (detailsObject.getString("contact_number") != null && !detailsObject.getString("contact_number").isEmpty() && !detailsObject.getString("contact_number").equals("")) {
                                                contactNo = detailsObject.getString("contact_number");
                                            }
                                        }

                                        //String quickbloxId = detailsObject.getString("quickblox_userid");
                                        String invite_status = detailsObject.getString("invite_status");

                                        Log.e("USERNAME========", username);
                                        // Log.e("userid========", userid);
                                        //  Log.e("emailid========", emailid);
                                        Log.e("ContactNo========", contactNo);
                                        // Log.e("Quickblox========", quickbloxId);
                                        friendContact.setName(username);
                                        //friendContact.setUser_id(userid);
                                        friendContact.setEmail_address(emailid);
                                        friendContact.setContact_number(contactNo);
                                        //friendContact.setQuickblox_id(quickbloxId);
                                        friendContact.setRegistered_flag(invite_status);
                                        friendLists.add(friendContact);
                                    }
                                }

                                Log.e("SUCCESS", "" + friendLists.toString());
                                Intent intent = new Intent();
                                intent.putExtra("friendList", friendLists);
                                setResult(RESULT_OK, intent);
                                finish();

                            } else if (status.equalsIgnoreCase("error")) {
                                CommonDialogs.showMessageDialog(getApplicationContext(), getResources().getString(R.string.ongravity), message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       /*if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();*/
                        error.printStackTrace();
                        String errorMessage = getString(R.string.error_connection_generic);

                        if (error instanceof NoConnectionError)
                            errorMessage = getString(R.string.error_no_connection);
                        else if (error instanceof AuthFailureError)
                            errorMessage = getString(R.string.error_authentication);
                        else if (error instanceof NetworkError)
                            errorMessage = getString(R.string.error_network_error);
                        else if (error instanceof ParseError)
                            errorMessage = getString(R.string.error_parse_error);
                        else if (error instanceof ServerError)
                            errorMessage = getString(R.string.error_server_error);
                        else if (error instanceof TimeoutError)
                            errorMessage = getString(R.string.error_connection_timeout);
                        System.out.println(errorMessage);
                        Toast.makeText(ImportGmailContactsActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", AppSettings.getLoginUserId());
                map.put("profile_cat_id", AppSettings.getProfileDefaultCategory());
                map.put("data", key);
                Log.e("MAP", "" + map);
                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
