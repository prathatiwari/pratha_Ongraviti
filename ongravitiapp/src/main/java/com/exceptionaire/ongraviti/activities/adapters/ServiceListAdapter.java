package com.exceptionaire.ongraviti.activities.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.ServicesActivity;
import com.exceptionaire.ongraviti.core.OnGravitiApp;
import com.exceptionaire.ongraviti.model.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ServiceListAdapter extends BaseAdapter {
    private List<Service> serviceList;
    private ServicesActivity activity;
//    private ImageLoader imageLoader = OnGravitiApp.getInstance().getImageLoader();
    private List<Service> fileredlist;

    public void setServiceList(List<Service> services) {
        //this.serviceList.clear();
        this.serviceList = services;
        fileredlist.clear();
        fileredlist.addAll(services);
        notifyDataSetChanged();
    }

    public ServiceListAdapter(List<Service> contactList, ServicesActivity activity) {
        this.serviceList = contactList;
        fileredlist = new ArrayList<>();
        this.activity = activity;
    }

    public void removeItem(int position) {
        serviceList.remove(position);
        notifyDataSetChanged();
    }

    private class Holder {
        TextView tv_service_name,tv_description,tv_cost;
        Button btnPurchase;
    }

    @Override
    public int getCount() {
        return serviceList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return serviceList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        final Service service = serviceList.get(position);
        if (convertView == null) {
            holder = new ServiceListAdapter.Holder();
            convertView = layoutInflater.inflate(R.layout.service_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");

            holder.tv_service_name = (TextView) convertView.findViewById(R.id.tv_service_name);
            holder.tv_description = (TextView) convertView.findViewById(R.id.tv_description);
            holder.tv_cost = (TextView) convertView.findViewById(R.id.tv_cost);
            holder.btnPurchase = (Button)convertView.findViewById(R.id.btnPurchase);

            convertView.setTag(holder);
        } else {
            holder = (ServiceListAdapter.Holder) convertView.getTag();
        }

        holder.tv_service_name.setText(service.getServiceName());
        holder.tv_description.setText(service.getServiceDescription());
        holder.tv_cost.setText(service.getServiceCost());
        holder.btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.makePayment(service);
            }
        });
        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        serviceList.clear();
        if (charText.length() == 0) {
            serviceList.addAll(fileredlist);
        } else {
            for (Service wp : fileredlist) {
                if (wp.getServiceName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    serviceList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
