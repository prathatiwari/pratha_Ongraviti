package com.exceptionaire.ongraviti.activities.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;

public class CustomProgressDialog extends ProgressDialog {

    private Animation animRotate;
    private ImageView ivProgress;
    private TextView tvProgress;
    private String message;
    private AnimationDrawable mailAnimation;

    public CustomProgressDialog(Context context, String message) {
        super(context);
        this.message = message;

		/*animRotate = AnimationUtils.loadAnimation(context,
        R.anim.custom_progress_dialog);*/
        setIndeterminate(true);
        setCancelable(false);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_progress_dialog);
        ivProgress = (ImageView) findViewById(R.id.ivProgress);
        ivProgress.setBackgroundResource(R.drawable.anim_progress);
        mailAnimation = (AnimationDrawable) ivProgress.getBackground();
        tvProgress = (TextView) findViewById(R.id.tvProgress);
        tvProgress.setText(message);
    }

    @Override
    public void show() {
        super.show();
        /*ivProgress.startAnimation(animRotate);*/
        ivProgress.post(new Runnable() {
            public void run() {
                if (mailAnimation != null) mailAnimation.start();
            }
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();

        mailAnimation.stop();
    }
}
