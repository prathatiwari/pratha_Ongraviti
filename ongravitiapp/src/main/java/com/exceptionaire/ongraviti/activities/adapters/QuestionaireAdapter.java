package com.exceptionaire.ongraviti.activities.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.dialog.animation.BaseAnimatorSet;
import com.exceptionaire.ongraviti.model.Questionaire;
import com.exceptionaire.ongraviti.utilities.CustomVolleyRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by root on 14/2/17.
 */

public class QuestionaireAdapter extends RecyclerView.Adapter<QuestionaireAdapter.ViewHolder> {
    private ArrayList<Questionaire> list_questionaire;
    private ProgressDialog progressDialog;
    //volly request variables
    private int WESERVICE_FLAG;
    private RequestQueue requestQueue;
    private CustomVolleyRequest customVolleyRequest;
    Context context;
    String TAG = "QuesAdapter";
    String status, message;
    Activity activity;
    QuestionaireAdptrListener onClickListener;

    public static BaseAnimatorSet mBasIn;
    public static BaseAnimatorSet mBasOut;


    public QuestionaireAdapter(Activity activity, Context context, ArrayList<Questionaire> list_questionaire, QuestionaireAdptrListener questionaireAdptrListener) {
        this.activity = activity;
        this.context = context;
        this.list_questionaire = list_questionaire;
        this.onClickListener = questionaireAdptrListener;
    }

    public interface QuestionaireAdptrListener {
        void imgEditOnClick(View v, int position, String ques_id, String type, String otpion, String answer, String question);

        void imgDeleteOnClick(View v, int position, String question_id);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_questionaire, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        try {
//        final int pos=viewHolder.getAdapterPosition();
            final int pos = i;
            Log.d(TAG, "## list_questionaire.size : " + list_questionaire.size());
            viewHolder.txt_question.setText(list_questionaire.get(pos).getQuestion());

            List<String> list_options = Arrays.asList(list_questionaire.get(pos).getQuestion_option().split(","));
            if (this.list_questionaire.get(pos).getQuestion_type().equalsIgnoreCase("0")) {
                viewHolder.txtans1.setText(list_options.get(0));
                viewHolder.txtans2.setText(list_options.get(1));

                viewHolder.linear_ans3.setVisibility(View.GONE);
                viewHolder.linear_ans4.setVisibility(View.GONE);

                viewHolder.txtans1.setTextColor(Color.parseColor("#696969"));
                viewHolder.txtans2.setTextColor(Color.parseColor("#696969"));

                viewHolder.txtans1.setTypeface(null);
                viewHolder.txtans2.setTypeface(null);

                if (list_questionaire.get(pos).getAnswer().equalsIgnoreCase(list_options.get(0))) {
                    viewHolder.txtans1.setTextColor(Color.parseColor("#33691E"));
                    viewHolder.txtans1.setTypeface(null, Typeface.BOLD);
                } else if (list_questionaire.get(pos).getAnswer().equalsIgnoreCase(list_options.get(1))) {
                    viewHolder.txtans2.setTextColor(Color.parseColor("#33691E"));
                    viewHolder.txtans2.setTypeface(null, Typeface.BOLD);
                }
            } else {
                viewHolder.txtans1.setText(list_options.get(0));
                viewHolder.txtans2.setText(list_options.get(1));
                viewHolder.txtans3.setText(list_options.get(2));
                viewHolder.txtans4.setText(list_options.get(3));

                viewHolder.txtans1.setTextColor(Color.parseColor("#696969"));
                viewHolder.txtans2.setTextColor(Color.parseColor("#696969"));
                viewHolder.txtans3.setTextColor(Color.parseColor("#696969"));
                viewHolder.txtans4.setTextColor(Color.parseColor("#696969"));

                viewHolder.txtans1.setTypeface(null);
                viewHolder.txtans2.setTypeface(null);
                viewHolder.txtans3.setTypeface(null);
                viewHolder.txtans4.setTypeface(null);

                // viewHolder.txtans.setText(list_questionaire.get(pos).getAnswer());
                if (list_questionaire.get(pos).getAnswer().equalsIgnoreCase(list_options.get(0))) {
                    viewHolder.txtans1.setTextColor(Color.parseColor("#33691E"));
                    viewHolder.txtans1.setTypeface(null, Typeface.BOLD);
                } else if (list_questionaire.get(pos).getAnswer().equalsIgnoreCase(list_options.get(1))) {
                    viewHolder.txtans2.setTextColor(Color.parseColor("#33691E"));
                    viewHolder.txtans2.setTypeface(null, Typeface.BOLD);
                } else if (list_questionaire.get(pos).getAnswer().equalsIgnoreCase(list_options.get(2))) {
                    viewHolder.txtans3.setTextColor(Color.parseColor("#33691E"));
                    viewHolder.txtans3.setTypeface(null, Typeface.BOLD);
                } else if (list_questionaire.get(pos).getAnswer().equalsIgnoreCase(list_options.get(3))) {
                    viewHolder.txtans4.setTextColor(Color.parseColor("#33691E"));
                    viewHolder.txtans4.setTypeface(null, Typeface.BOLD);
                }
            }

       /* if (list_questionaire.get(i).getQuestion_type().equalsIgnoreCase("0")) {
            viewHolder.txtquestion_type.setText("Radio Button");
        } else {
            viewHolder.txtquestion_type.setText("Drop Down");
        }*/
            viewHolder.imgBtnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //[{"question_type":"1","question_option":"blue,pink,black,green","answer":"pink","question":"fav color"}]
                    Log.d("QAdp", "## imgBtnEdit onClick");
                    String data = "[{\"question_type\":\"" + list_questionaire.get(pos).getQuestion_type() + "\",\"question_option\":\"" + list_questionaire.get(pos).getQuestion_option() + "\",\"answer\":\"" + list_questionaire.get(pos).getAnswer() + "\",\"question\":\"" + list_questionaire.get(pos).getQuestion() + "\"}]";
                    onClickListener.imgEditOnClick(v, pos, list_questionaire.get(pos).getQues_id(), list_questionaire.get(pos).getQuestion_type(), list_questionaire.get(pos).getQuestion_option(), list_questionaire.get(pos).getAnswer(), list_questionaire.get(pos).getQuestion());
//                editItem(pos,list_questionaire.get(pos).getQues_id(),list_questionaire.get(pos).getQuestion_type(),list_questionaire.get(pos).getQuestion_option(),list_questionaire.get(pos).getAnswer(),list_questionaire.get(pos).getQuestion());
                }
            });

            viewHolder.imgBtnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("QAdp", "## imgBtnDelete onClick : " + list_questionaire.get(pos).getQues_id());
                    Log.d("QAdp", "## pos : " + pos);
//                    onClickListener.imgDeleteOnClick(v, pos, list_questionaire.get(pos).getQues_id());
                    //call delete api
                    removeItem(pos);
                }
            });
            viewHolder.setIsRecyclable(false); // for not recycle data
        } catch (Exception e) {
            Log.e(TAG, "## saveUserData error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list_questionaire.size();
    }

    public void addItem(String ques_id, String question, String questionType, String answer, String Question_option) {
        try {
            Log.d(TAG, "## addItem list_questionaire before : " + list_questionaire.size());
            Questionaire questionaire = new Questionaire(ques_id, question, questionType, answer, Question_option);

            list_questionaire.add(questionaire);
            Log.d(TAG, "## addItem list_questionaire after : " + list_questionaire.size());
            Log.d(TAG, "## list_questionaire.get(list_questionaire.size()-1).getQuestion() : " + list_questionaire.get(list_questionaire.size() - 1).getQuestion());
            if (list_questionaire.isEmpty()) {
                notifyDataSetChanged();
            } else {
                notifyItemInserted(list_questionaire.size() - 1);
            }
        } catch (Exception e) {
            Log.e(TAG, "## saveUserData error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void editItem(int position, String quest_id, String question, String questionType, String answer, String Question_option) {
//        notifyItemRemoved(position);
        try {
            Questionaire questionaire = new Questionaire(quest_id, question, questionType, answer, Question_option);
            if (list_questionaire.isEmpty()) {
                list_questionaire.add(questionaire);
            } else {
                list_questionaire.set(position, questionaire);
            }
            notifyItemChanged(position);
//        notifyItemInserted(position);
//        notifyDataSetChanged();
//        notifyItemRemoved(position);
//        notifyItemRangeChanged(position, list_questionaire.size()-1);
        } catch (Exception e) {
            Log.e(TAG, "## saveUserData error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void removeItem(final int position) {
        try {
            /*mBasIn = new BounceTopEnter();
            mBasOut = new SlideBottomExit();

            final NormalDialog dialog = new NormalDialog(activity);
            dialog.isTitleShow(false);

            dialog.content(context.getResources().getString(R.string.confirm_dlt))
                    .showAnim(mBasIn)
                    .dismissAnim(mBasOut)
                    .contentGravity(Gravity.CENTER)
                    .show();

            dialog.setOnBtnClickL(
                    new OnBtnClickL() {
                        @Override
                        public void onBtnClick() {
                            Log.d("dialog","## ok");
                            dialog.dismiss();
                            onClickListener.imgDeleteOnClick(v, position, list_questionaire.get(position).getQues_id());
                            list_questionaire.remove(position);
                            notifyDataSetChanged();
                        }
                    },
                    new OnBtnClickL() {
                        @Override
                        public void onBtnClick() {
                            Log.d("dialog","## cancel");
                            dialog.dismiss();
                        }
                    });*/

            String dialogHeader="Confirm";
            String dialogMessage=context.getResources().getString(R.string.confirm_dlt);
            final Dialog dialog = new Dialog(context, R.style.DialogStyle);

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_ok);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            if (dialogHeader != null)
                ((TextView) dialog.findViewById(R.id.text_view_dialog_header)).setText(dialogHeader);
            if (dialogMessage != null)
                ((TextView) dialog.findViewById(R.id.text_view_dialog_message)).setText(dialogMessage);

            dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                    onClickListener.imgDeleteOnClick(v, position, list_questionaire.get(position).getQues_id());
                    list_questionaire.remove(position);
                    notifyDataSetChanged();
                }
            });
            Button btnCancel=(Button)dialog.findViewById(R.id.button_cancel);
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {
            Log.e(TAG, "## saveUserData error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_question, txtans1, txtans2, txtans3, txtans4, txtans, txtquestion_type;
        LinearLayout linear_ans3, linear_ans4;
        ImageView imgBtnEdit, imgBtnDelete;
        // to hide textviews if radio selected

        public ViewHolder(View view) {
            super(view);
            try {
                txt_question =  view.findViewById(R.id.edtQuestion);
                txtans1 =  view.findViewById(R.id.edtAnswer1);
                txtans2 =  view.findViewById(R.id.edtAnswer2);
                txtans3 =  view.findViewById(R.id.edtAnswer3);
                txtans4 = view.findViewById(R.id.edtAnswer4);
                linear_ans3 =  view.findViewById(R.id.linear_answer3);
                linear_ans4 =  view.findViewById(R.id.linear_answer4);
                imgBtnEdit =  view.findViewById(R.id.imgBtnEdit);
                imgBtnDelete = view.findViewById(R.id.imgBtnDelete);
            } catch (Exception e) {
                Log.e(TAG, "## saveUserData error : " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

}