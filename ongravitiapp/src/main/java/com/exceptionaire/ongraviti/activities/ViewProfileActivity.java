package com.exceptionaire.ongraviti.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.HomeUserPostsNewAdapter;
import com.exceptionaire.ongraviti.activities.adapters.InterestListAdapter;
import com.exceptionaire.ongraviti.activities.adapters.PostsCommentsAdapter;
import com.exceptionaire.ongraviti.activities.adapters.UserPostListAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.Array_of_comment;
import com.exceptionaire.ongraviti.model.DataModel;
import com.exceptionaire.ongraviti.model.LikeUnlike;
import com.exceptionaire.ongraviti.model.MarkerDetails;
import com.exceptionaire.ongraviti.model.ProfileInfo;
import com.exceptionaire.ongraviti.model.ResponseGetCommentsByPostID;
import com.exceptionaire.ongraviti.model.ResponseGetInterestSubcategory;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseUpdateStatus;
import com.exceptionaire.ongraviti.model.ResponseUserPost;
import com.exceptionaire.ongraviti.model.Set_Comment;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.squareup.picasso.Picasso;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.exceptionaire.ongraviti.imoji.EmoticonTextView;

import org.apache.http.protocol.HTTP;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

public class ViewProfileActivity extends BaseDrawerActivity implements AppConstants {
    private ArrayList<ResponseUserPost.Postinfo> mFaqList = new ArrayList<>();

    private static final String TAG = "ViewProfileActivity";
    private EditText edt_contries_travelled;
    UserPostListAdapter faqListAdapter;
    ArrayList dataModels;
    EditText et_comment;
    private TextView txtDrawerName;
    private TextView header;
    private EditText edtEventsLookingForwardTo, edtAreaOfInterest, etEnterpreneuer, etEventsAttndWIWB, etEventsAttended, etLookFor, etDegreeName, etFutureGoal, etFavouriteBook, etFavouriteShows, etFavouriteMovies, etName, etDateOfBirth, edtWeight;
    private TextView t_view_name, t_view_name1, t_view_dob, t_view_gender, t_view_interested_in, t_view_relationship_status, t_view_religious_beliefs, t_view_political_beliefs, t_view_height, t_view_weight, t_view_body_type, t_view_eye_color, t_view_hair_color, t_view_smoking, t_view_drinking, t_view_gym, t_view_fav_book, t_view_fav_shows, t_view_fav_movies, t_view_contries_travelled, t_view_events_attended, t_view_hobbies, t_view_highest_degree, t_view_colg_school, t_view_degree, t_view_emp_status, t_view_future_goal, t_view_area_of_interest, t_view_what_i_read, t_view_venture_to_look_for, t_view_enterpreneursILik, t_view_events_attended_WIWB, t_view_events_looking_forward_to, t_view_industry_i_want_to_target;
    private RadioButton rdGenMale, rdGenFemale, rdInterestedMale, rdInterestedFemale;
    //    private RelativeLayout relativeAmI, relativeLike, relativeLiving, relativeWant, relativeRSSPreference;
    private String videoBioPath = null;
    String user_id = "", name = "", date_of_birth = "", gender = "", interested_in = "", relationship_status = "", beliefs_religious_beliefs = "";
    ListView interest;
    TextView text_view_nopost;
    private Intent intent;
    public static RoundedImageView roundedImageViewProfilePic;
    public Uri mImageCaptureUri, imageUri;
    private String displayName;
    private File tempFile;
    private String string;
    private File getTempFile;
    private int operation_code;
    private List<MarkerDetails> markerDetailsList;
    private String countries_travelled;

    //Google place apis
    private Context context;

    //location variables
    AutoCompleteTextView autoCompleteViewAddressLine;
    private SimpleDateFormat dateFormatter;
    public ImageLoader imageLoader = ImageLoader.getInstance();
    private DatePickerDialog datePicker;
    String actualHeight = "";

    private ProgressDialog progressDialog;
    public static UserBasicInfo userBasicInfo;
    public static ArrayList<ProfileInfo> list_profile_category;
    private RecyclerView recyclerViewPosts;
    private Button btn_clear_search;
    private StringBuilder stringBuilder;
    private ImageView imgSocial, imgDating_icon, imgBusiness_icon;
    ImageView social_backlground;
    Boolean social = false;
    Boolean dating = false;
    Boolean business = false;
    Boolean socialEditPost = false;
    Boolean datingEditPost = false;
    Boolean businessEditPost = false;
    Boolean socialVideoBio = false;
    Boolean datingVideoBio = false;
    Boolean businessVideoBio = false;
    private boolean basicDetails = false, like_doing = false, make_living = false, want_to_be = false;
    TextView text_view_cat;
    private int currentScrollState, currentFirstVisibleItem, currentVisibleItemCount, totalItemCount;
    private LinearLayout swipe_refresh_layout;
    EmoticonTextView text_view_status;
    ImageView textview1, textview2, textview3, textview4, textview5, play_video_vio;
    LinearLayout linear1, linear2, linear3, linear4, linear5;
    TextView text_view_social, text_view_dating, text_view_business;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_profile, frameLayout);
        arcMenu.setVisibility(View.VISIBLE);
        try {
            initializeViewComponents();
            Log.d(TAG, "## AppSettings.getProfileDefaultCategory() :" + AppSettings.getProfileDefaultCategory());
            context = this;
            intent = getIntent();
            stringBuilder = new StringBuilder();
            if (isNetworkAvailable()) {

                getUserProfileData();

            } else {
                CommonDialogs.dialog_with_one_btn_without_title(ViewProfileActivity.this, getResources().getString(R.string.check_internet));
            }


        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        textview1.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                textview1.setImageResource(R.drawable.who_am_i_select);
                textview2.setImageResource(R.drawable.what_i_like_doing);
                textview3.setImageResource(R.drawable.how_do_i_make_living);
                textview4.setImageResource(R.drawable.what_i_want_to_be);
                textview5.setImageResource(R.drawable.post);

                linear1.setVisibility(View.VISIBLE);
                linear2.setVisibility(View.GONE);
                linear3.setVisibility(View.GONE);
                linear4.setVisibility(View.GONE);
                linear5.setVisibility(View.GONE);
            }
        });
        textview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textview2.setImageResource(R.drawable.what_i_like_doing_select);
                textview1.setImageResource(R.drawable.who_am_i);
                textview3.setImageResource(R.drawable.how_do_i_make_living);
                textview4.setImageResource(R.drawable.what_i_want_to_be);
                textview5.setImageResource(R.drawable.post);

                linear1.setVisibility(View.GONE);
                linear2.setVisibility(View.VISIBLE);
                linear3.setVisibility(View.GONE);
                linear4.setVisibility(View.GONE);
                linear5.setVisibility(View.GONE);
            }
        });
        textview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textview3.setImageResource(R.drawable.how_do_i_make_living_select);
                textview2.setImageResource(R.drawable.what_i_like_doing);
                textview1.setImageResource(R.drawable.who_am_i);
                textview4.setImageResource(R.drawable.what_i_want_to_be);
                textview5.setImageResource(R.drawable.post);


                linear1.setVisibility(View.GONE);
                linear2.setVisibility(View.GONE);
                linear3.setVisibility(View.VISIBLE);
                linear4.setVisibility(View.GONE);
                linear5.setVisibility(View.GONE);
            }
        });
        textview4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textview4.setImageResource(R.drawable.what_i_want_to_be_select);
                textview1.setImageResource(R.drawable.who_am_i);
                textview2.setImageResource(R.drawable.what_i_like_doing);
                textview3.setImageResource(R.drawable.how_do_i_make_living);
                textview5.setImageResource(R.drawable.post);

                linear1.setVisibility(View.GONE);
                linear2.setVisibility(View.GONE);
                linear3.setVisibility(View.GONE);
                linear4.setVisibility(View.VISIBLE);
                linear5.setVisibility(View.GONE);
            }
        });
        textview5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                textview3.setImageResource(R.drawable.how_do_i_make_living);
                textview2.setImageResource(R.drawable.what_i_like_doing);
                textview1.setImageResource(R.drawable.who_am_i);
                textview4.setImageResource(R.drawable.what_i_want_to_be);
                textview5.setImageResource(R.drawable.post_select);
                linear1.setVisibility(View.GONE);
                linear2.setVisibility(View.GONE);
                linear3.setVisibility(View.GONE);
                linear4.setVisibility(View.GONE);
                linear5.setVisibility(View.VISIBLE);
            }
        });


        text_view_social.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                social = true;
                socialEditPost = true;
                socialVideoBio = true;
                text_view_social.setTextColor(R.color.black);
                text_view_dating.setTextColor(R.color.colorPrimary);
                text_view_business.setTextColor(R.color.colorPrimary);
                getUserProfileData();
            }
        });
        text_view_dating.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                dating = true;
                datingEditPost = true;
                datingVideoBio = true;
                text_view_social.setTextColor(R.color.colorPrimary);
                text_view_dating.setTextColor(R.color.black);
                text_view_business.setTextColor(R.color.colorPrimary);
                getUserProfileData();
            }
        });
        text_view_business.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                business = true;
                businessVideoBio = true;
                businessEditPost = true;
                text_view_social.setTextColor(R.color.colorPrimary);
                text_view_dating.setTextColor(R.color.colorPrimary);
                text_view_business.setTextColor(R.color.black);
                getUserProfileData();
            }
        });
//        text_view_social.performClick();
        //TODO put video bio
        play_video_vio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (videoBioPath != null && !videoBioPath.equals("") && !videoBioPath.equals("http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/uploads/")) {
                    Intent intent1 = new Intent(ViewProfileActivity.this, VideoBioActivity.class);
                    intent1.putExtra("videopath", videoBioPath);
                    startActivity(intent1);
                } else {
                    CommonDialogs.dialog_with_one_btn_without_title(ViewProfileActivity.this, "Profile video not available.");
                }

//                if (socialVideoBio == true) {
//                    Intent intent2 = new Intent(ViewProfileActivity.this, VideoBioActivity.class);
//                    intent1.putExtra("videopath", videoBioPath);
//                    startActivity(intent2);
//                    socialVideoBio = false;
//                } else if (datingVideoBio == true) {
//                    Intent intent3 = new Intent(ViewProfileActivity.this, VideoBioActivity.class);
//                    intent1.putExtra("videopath", videoBioPath);
//                    startActivity(intent3);
//                    datingVideoBio = false;
//                } else if (businessVideoBio == true) {
//                    Intent intent3 = new Intent(ViewProfileActivity.this, VideoBioActivity.class);
//                    intent1.putExtra("videopath", videoBioPath);
//                    startActivity(intent3);
//                    businessVideoBio = false;
//                }
            }
        });


    }


    private void initializeViewComponents() {
        Log.d(TAG, "## initializeViewComponents");
        try {
            swipe_refresh_layout = (LinearLayout) findViewById(R.id.swipe_refresh_layout);
            recyclerViewPosts = (RecyclerView) findViewById(R.id.list_view_userpost);
            imgSocial = (ImageView) findViewById(R.id.imgSocial_);
            imgDating_icon = (ImageView) findViewById(R.id.imgDating_);
            imgBusiness_icon = (ImageView) findViewById(R.id.imgBusiness_);
            //toolbar = (Toolbar) findViewById(R.id.toolbar);
            //setSupportActionBar(toolbar);

//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            social_backlground = (ImageView) findViewById(R.id.social_backlground);
            text_view_status = (EmoticonTextView) findViewById(R.id.text_view_status);
            edt_contries_travelled = (EditText) findViewById(R.id.edt_contries_travelled);
            txtDrawerName = (TextView) findViewById(R.id.drawer_header_username);
            roundedImageViewProfilePic = (RoundedImageView) findViewById(R.id.imgProfile);
            //textview display values
            play_video_vio = (ImageView) findViewById(R.id.play_video_vio);
            textview1 = (ImageView) findViewById(R.id.text_view1);
            textview2 = (ImageView) findViewById(R.id.text_view2);
            textview3 = (ImageView) findViewById(R.id.text_view3);
            textview4 = (ImageView) findViewById(R.id.text_view4);
            textview5 = (ImageView) findViewById(R.id.text_view5);
            text_view_social = (TextView) findViewById(R.id.text_view_social);
            text_view_dating = (TextView) findViewById(R.id.text_view_dating);
            text_view_business = (TextView) findViewById(R.id.text_view_business);
            linear1 = (LinearLayout) findViewById(R.id.linear1);
            linear2 = (LinearLayout) findViewById(R.id.linear2);
            linear3 = (LinearLayout) findViewById(R.id.linear3);
            linear4 = (LinearLayout) findViewById(R.id.linear4);
            linear5 = (LinearLayout) findViewById(R.id.linear5);

            t_view_name = (TextView) findViewById(R.id.t_view_name);
            t_view_name1 = (TextView) findViewById(R.id.t_view_name1);
            t_view_dob = (TextView) findViewById(R.id.t_view_dob);
            t_view_gender = (TextView) findViewById(R.id.t_view_gender);
            t_view_interested_in = (TextView) findViewById(R.id.t_view_interested_in);
            t_view_relationship_status = (TextView) findViewById(R.id.t_view_relationship_status);
            t_view_religious_beliefs = (TextView) findViewById(R.id.t_view_religious_beliefs);
            t_view_political_beliefs = (TextView) findViewById(R.id.t_view_political_beliefs);
            text_view_cat = (TextView) findViewById(R.id.text_view_cat);
            t_view_height = (TextView) findViewById(R.id.t_view_height);

            t_view_weight = (TextView) findViewById(R.id.t_view_weight);
            interest = (ListView) findViewById(R.id.interest);
            dataModels = new ArrayList();
            text_view_nopost = (TextView) findViewById(R.id.text_view_nopost);
            dataModels.add(new DataModel("Cricket", false));
            dataModels.add(new DataModel("Cooking", true));
            dataModels.add(new DataModel("Music", false));
            dataModels.add(new DataModel("Books", true));
            dataModels.add(new DataModel("News", false));

            InterestListAdapter adapter = new InterestListAdapter(dataModels, getApplicationContext());

            interest.setAdapter(adapter);
            t_view_body_type = (TextView) findViewById(R.id.t_view_body_type);
            t_view_eye_color = (TextView) findViewById(R.id.t_view_eye_color);
            t_view_hair_color = (TextView) findViewById(R.id.t_view_hair_color);
            t_view_smoking = (TextView) findViewById(R.id.t_view_smoking);
            t_view_drinking = (TextView) findViewById(R.id.t_view_drinking);
            t_view_gym = (TextView) findViewById(R.id.t_view_gym);
            t_view_fav_book = (TextView) findViewById(R.id.t_view_fav_book);
            t_view_fav_shows = (TextView) findViewById(R.id.t_view_fav_shows);
            t_view_fav_movies = (TextView) findViewById(R.id.t_view_fav_movies);
            t_view_contries_travelled = (TextView) findViewById(R.id.t_view_contries_travelled);
            t_view_events_attended = (TextView) findViewById(R.id.t_view_events_attended);
            t_view_hobbies = (TextView) findViewById(R.id.t_view_hobbies);

            t_view_highest_degree = (TextView) findViewById(R.id.t_view_highest_degree);
            t_view_colg_school = (TextView) findViewById(R.id.t_view_colg_school);
//            t_view_colg_school.setMovementMethod(new ScrollingMovementMethod());

            t_view_degree = (TextView) findViewById(R.id.t_view_degree);
            t_view_emp_status = (TextView) findViewById(R.id.t_view_emp_status);
            t_view_future_goal = (TextView) findViewById(R.id.t_view_future_goal);

            t_view_area_of_interest = (TextView) findViewById(R.id.t_view_area_of_interest);
            t_view_what_i_read = (TextView) findViewById(R.id.t_view_what_i_read);
            t_view_venture_to_look_for = (TextView) findViewById(R.id.t_view_venture_to_look_for);
            t_view_enterpreneursILik = (TextView) findViewById(R.id.t_view_enterpreneursILik);
            t_view_events_attended_WIWB = (TextView) findViewById(R.id.t_view_events_attended_WIWB);
            t_view_events_looking_forward_to = (TextView) findViewById(R.id.t_view_events_looking_forward_to);
            t_view_industry_i_want_to_target = (TextView) findViewById(R.id.t_view_industry_i_want_to_target);

            rdGenMale = (RadioButton) findViewById(R.id.rbGenderMale);
            rdGenFemale = (RadioButton) findViewById(R.id.rbGenderFemale);
            rdInterestedMale = (RadioButton) findViewById(R.id.rbInterestedInMale);
            rdInterestedFemale = (RadioButton) findViewById(R.id.rbInterestedInFemale);


            autoCompleteViewAddressLine = (AutoCompleteTextView) findViewById(R.id.txt_manual_college);

//        spWeight = (Spinner) findViewById(R.id.spWeight);
            edtWeight = (EditText) findViewById(R.id.edtWeight);


            etEventsAttndWIWB = (EditText) findViewById(R.id.etEnventsAttndWIWB);


            edtAreaOfInterest = (EditText) findViewById(R.id.edtAreaOfInterest);


            etEnterpreneuer = (EditText) findViewById(R.id.edtEnterpreneursILik);

            edtEventsLookingForwardTo = (EditText) findViewById(R.id.edtEventsLookingForwardTo);


            /* spCollegeSchool = (Spinner) findViewById(R.id.spCollegeSchool);
                txtColgValue=(TextView)findViewById(R.id.txtCollegeValue);*/

            etEventsAttended = (EditText) findViewById(R.id.etEventsAttended);

            etLookFor = (EditText) findViewById(R.id.etLookFor);
            etDegreeName = (EditText) findViewById(R.id.etDegreeName);
            etFutureGoal = (EditText) findViewById(R.id.etFutureGoal);
            etFavouriteBook = (EditText) findViewById(R.id.etFavouriteBook);
            etFavouriteShows = (EditText) findViewById(R.id.etFavouriteShows);
            etFavouriteMovies = (EditText) findViewById(R.id.etFavouriteMovies);
            etName = (EditText) findViewById(R.id.etName);
            etDateOfBirth = (EditText) findViewById(R.id.etDateOfBirth);
            etDateOfBirth.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (etDateOfBirth.getRight() - etDateOfBirth.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // show date picker dialog
                            datePicker.show();

                            return true;
                        }
                    }
                    return false;
                }
            });

            dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            Calendar newCalendar = Calendar.getInstance();
            datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    etDateOfBirth.setText(dateFormatter.format(newDate.getTime()));
                }

            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            datePicker.getDatePicker().setMaxDate(newCalendar.getTimeInMillis());


            header = (TextView) findViewById(R.id.toolbar_title_drawer);
            header.setText(getResources().getString(R.string.user_profile));


        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }

//        btn_clear_search = (Button) findViewById(R.id.btn_clear_search);
//        btn_clear_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                stringBuilder.setLength(0);
//                edt_contries_travelled.setText("");
//            }
//        });

        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        }
    }


    public void getUserProfileData() {
        showProgressBar();
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
        Call<ResponseGetUserProfileData> loginCall = getProfileRequest.getUserProfileData(AppSettings.getLoginUserId(), "");
        loginCall.enqueue(new Callback<ResponseGetUserProfileData>() {
            @Override
            public void onResponse(Call<ResponseGetUserProfileData> call, retrofit2.Response<ResponseGetUserProfileData> response) {
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                        setDataInControls(response.body());
                        getRSSPrefrenceList();
                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
//                        Toast.makeText(ViewProfileActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(ViewProfileActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseGetUserProfileData> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }


    @Override
    public void onBackPressed() {
        try {
//            super.onBackPressed();
            Intent i = new Intent(this, ProfileMainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            finish();
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        try {
            progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d(TAG, "## onErrorResponse : " + error.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            error.printStackTrace();
            String errorMessage = getString(R.string.error_connection_generic);
            if (error instanceof NoConnectionError)
                errorMessage = getString(R.string.error_no_connection);
            else if (error instanceof AuthFailureError)
                errorMessage = getString(R.string.error_authentication);
            else if (error instanceof NetworkError)
                errorMessage = getString(R.string.error_network_error);
            else if (error instanceof ParseError)
                errorMessage = getString(R.string.error_parse_error);
            else if (error instanceof ServerError)
                errorMessage = getString(R.string.error_server_error);
            else if (error instanceof TimeoutError)
                errorMessage = getString(R.string.error_connection_timeout);
            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
            Intent intentHome = new Intent(context, HomeUserPostsActivity.class);
            startActivity(intentHome);
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }


    public void setDataInControls(ResponseGetUserProfileData responseGetUserProfileData) {
        Log.d(TAG, "## setDataInControls");
        try {
            // set all control data from json object

            userBasicInfo = responseGetUserProfileData.getUser_basic_info()[0];
            AppSettings.setLoginUserId(userBasicInfo.getUser_id());
            AppSettings.setProfiePictureName(userBasicInfo.getName());
            Log.d(TAG, "## userAllProfileData.getProfile_cat_id() : " + userBasicInfo.getProfile_cat_id());
            AppSettings.setProfileDefaultCategory("" + userBasicInfo.getProfile_cat_id());
            Log.d(TAG, "## AppSettings.getProfiePictureName : " + AppSettings.getProfilePictureName());
            Log.d(TAG, "## AppSettings.getProfileDefaultCategory : " + String.valueOf(AppSettings.getProfileDefaultCategory()));

            //set name in edittext basedrawer
            txtDrawerName.setText(userBasicInfo.getName());

            if (responseGetUserProfileData.getProfile_info().length > 0) {
                //set profile pic according to default category, and profile name in appSettings
                Log.d(TAG, "## userAllProfileData.getUser_id() :: " + userBasicInfo.getUser_id());

                for (int i = 0; i < responseGetUserProfileData.getProfile_info().length; i++) {
                    switch (responseGetUserProfileData.getProfile_info()[i].getCategory_id()) {
                        case PROFILE_CATEGORY_ONE_SOCIAL:
                            AppSettings.setProfileCatPicPathOne(AppConstants.PROFILE_PICTURE_PATH + File.separator + responseGetUserProfileData.getProfile_info()[i].getProfile_picture());
                            Log.d(TAG, "## getProfileCatPicPathOne : " + AppSettings.getProfileCatPicPathOne());
                            if (userBasicInfo.getProfile_cat_id().equalsIgnoreCase(PROFILE_CATEGORY_ONE_SOCIAL))
                                AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[i].getProfile_picture());
//                            socialProfilePicVideoBioFragment.showUpdatedImage();
                            break;

                        case PROFILE_CATEGORY_TWO_DATING:
                            AppSettings.setProfileCatPicPathTwo(AppConstants.PROFILE_PICTURE_PATH + File.separator + responseGetUserProfileData.getProfile_info()[i].getProfile_picture());
                            Log.d(TAG, "## getProfileCatPicPathTWO : " + AppSettings.getProfileCatPicPathTWO());
                            if (userBasicInfo.getProfile_cat_id().equalsIgnoreCase(PROFILE_CATEGORY_TWO_DATING))
                                AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[i].getProfile_picture());
//                            datingProfilePicVideoBioFragment.showUpdatedImage();
                            break;

                        case PROFILE_CATEGORY_THREE_BUSINESS:
                            AppSettings.setProfileCatPicPathThree(AppConstants.PROFILE_PICTURE_PATH + File.separator + responseGetUserProfileData.getProfile_info()[i].getProfile_picture());
                            Log.d(TAG, "## getProfileCatPicPathThree : " + AppSettings.getProfileCatPicPathThree());
                            if (userBasicInfo.getProfile_cat_id().equalsIgnoreCase(PROFILE_CATEGORY_THREE_BUSINESS))
                                AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[i].getProfile_picture());
//                            businessProfilePicVideoBioFragment.showUpdatedImage();
                            break;

                        default:
                            break;
                    }
                }
            }

            //set selected profile to drawer
            if (String.valueOf(AppSettings.getProfileDefaultCategory()).equals(PROFILE_CATEGORY_ONE_SOCIAL)) {
                imgSocial.setImageResource(R.drawable.ic_social_checked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
            } else if (String.valueOf(AppSettings.getProfileDefaultCategory()).equals(PROFILE_CATEGORY_TWO_DATING)) {
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating_checked);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
            } else if (String.valueOf(AppSettings.getProfileDefaultCategory()).equals(PROFILE_CATEGORY_THREE_BUSINESS)) {
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business_checked);
            }

            //set data in textviews to view data only
            if (!userBasicInfo.getName().equals(""))
                t_view_name.setText(userBasicInfo.getName());
            t_view_name1.setText(userBasicInfo.getName());
            if (userBasicInfo.getDate_of_birth() != null) {
                if (!userBasicInfo.getDate_of_birth().equals(""))
                    t_view_dob.setText(userBasicInfo.getDate_of_birth());
            }
            if (userBasicInfo.getGender() != null) {
                if (!userBasicInfo.getGender().equals("")) {
                    if (userBasicInfo.getGender().equals("0")) {
                        t_view_gender.setText("Male");
                    } else if (userBasicInfo.getGender().equals("1")) {
                        t_view_gender.setText("Female");
                    } else if (userBasicInfo.getGender().equals("2")) {
                        t_view_gender.setText("Bisexual");
                    }
                }
            }
            if (userBasicInfo.getInterested_in() != null) {
                if (!userBasicInfo.getInterested_in().equals("")) {
                    if (userBasicInfo.getInterested_in().equals("0"))
                        t_view_interested_in.setText("Male");
                    else if (userBasicInfo.getInterested_in().equals("1"))
                        t_view_interested_in.setText("Female");
                    else if (userBasicInfo.getInterested_in().equals("2"))
                        t_view_interested_in.setText("Bisexual");
                }
            }
            if (userBasicInfo.getRelationship_status() != null) {
                if (!userBasicInfo.getRelationship_status().equals(""))
                    t_view_relationship_status.setText(userBasicInfo.getRelationship_status());
            }
            if (userBasicInfo.getBeliefs_religious_beliefs() != null) {
                if (!userBasicInfo.getBeliefs_religious_beliefs().equals(""))
                    t_view_religious_beliefs.setText(userBasicInfo.getBeliefs_religious_beliefs());
            }
            if (userBasicInfo.getPolitical_beliefs() != null) {
                if (!userBasicInfo.getPolitical_beliefs().equals(""))
                    t_view_political_beliefs.setText(userBasicInfo.getPolitical_beliefs());
            }
            if (userBasicInfo.getHeight() != null) {
                if (!actualHeight.equals("")) {
                    t_view_height.setText(actualHeight + " cms");
                } else {
                    if (!userBasicInfo.getHeight().equals("")) {
                        String[] arr;
                        String initialHeight = userBasicInfo.getHeight().replace(".", "'");
                        arr = initialHeight.split("\\(");
                        actualHeight = arr[0] + "\"(" + arr[1]; //+ "\"("
                        actualHeight = actualHeight.replaceAll("^(.*)'(.*)$", "$1.$2");
                        Log.d(TAG, "## actualHeight : " + actualHeight);
                        t_view_height.setText(actualHeight + " cms");
                    }
                }
            }
            if (userBasicInfo.getWeight() != null) {
                if (!userBasicInfo.getWeight().equals(""))
                    t_view_weight.setText(userBasicInfo.getWeight() + " kg");
            }
            if (userBasicInfo.getBody_type() != null) {
                if (!userBasicInfo.getBody_type().equals(""))
                    t_view_body_type.setText(userBasicInfo.getBody_type());
            }
            if (userBasicInfo.getEye_color() != null) {
                if (!userBasicInfo.getEye_color().equals(""))
                    t_view_eye_color.setText(userBasicInfo.getEye_color());
            }
            if (userBasicInfo.getHair_color() != null) {
                if (!userBasicInfo.getHair_color().equals(""))
                    t_view_hair_color.setText(userBasicInfo.getHair_color());
            }
            if (userBasicInfo.getSmoking() != null) {
                if (!userBasicInfo.getSmoking().equals(""))
                    t_view_smoking.setText(userBasicInfo.getSmoking());
            }
            if (userBasicInfo.getDrinking() != null) {
                if (!userBasicInfo.getDrinking().equals(""))
                    t_view_drinking.setText(userBasicInfo.getDrinking());
            }
            if (userBasicInfo.getGym() != null) {
                if (!userBasicInfo.getGym().equals(""))
                    t_view_gym.setText(userBasicInfo.getGym());
            }
            if (userBasicInfo.getFavorite_book() != null) {
                if (!userBasicInfo.getFavorite_book().equals(""))
                    t_view_fav_book.setText(userBasicInfo.getFavorite_book());
            }
            if (userBasicInfo.getFavorite_shows() != null) {
                if (!userBasicInfo.getFavorite_shows().equals(""))
                    t_view_fav_shows.setText(userBasicInfo.getFavorite_shows());
            }
            if (userBasicInfo.getFavorite_movies() != null) {
                if (!userBasicInfo.getFavorite_movies().equals(""))
                    t_view_fav_movies.setText(userBasicInfo.getFavorite_movies());
            }
            if (userBasicInfo.getEvents_attended() != null) {
                if (!userBasicInfo.getEvents_attended().equals(""))
                    t_view_events_attended.setText(userBasicInfo.getEvents_attended());
            }
            if (userBasicInfo.getCountries_travelled() != null) {
                if (!userBasicInfo.getCountries_travelled().equals(""))
                    t_view_contries_travelled.setText(userBasicInfo.getCountries_travelled());
            }
            if (userBasicInfo.getHobbies() != null) {
                if (!userBasicInfo.getHobbies().equals(""))
                    t_view_hobbies.setText(userBasicInfo.getHobbies());
            }
            if (userBasicInfo.getHighest_degree() != null) {
                if (!userBasicInfo.getHighest_degree().equals(""))
                    t_view_highest_degree.setText(userBasicInfo.getHighest_degree());
            }
            if (userBasicInfo.getCollege_university_name() != null) {
                if (!userBasicInfo.getCollege_university_name().equals(""))
                    t_view_colg_school.setText(userBasicInfo.getCollege_university_name());
            }
            if (userBasicInfo.getDegree_name() != null) {
                if (!userBasicInfo.getDegree_name().equals(""))
                    t_view_degree.setText(userBasicInfo.getDegree_name());
            }
            if (userBasicInfo.getEmployment_status() != null) {
                if ((!userBasicInfo.getEmployment_status().equals("")))
                    t_view_emp_status.setText(userBasicInfo.getEmployment_status());
            }
            if (userBasicInfo.getFuture_goal() != null) {
                if (!userBasicInfo.getFuture_goal().equals(""))
                    t_view_future_goal.setText(userBasicInfo.getFuture_goal());
            }
            if (userBasicInfo.getArea_of_interest() != null) {
                if (!userBasicInfo.getArea_of_interest().equals(""))
                    t_view_area_of_interest.setText(userBasicInfo.getArea_of_interest());
            }
            if (userBasicInfo.getwhat_i_read() != null) {
                if (!userBasicInfo.getwhat_i_read().equals(""))
                    t_view_what_i_read.setText(userBasicInfo.getwhat_i_read());
            }
            if (userBasicInfo.getInteresting_ventures_to_look() != null) {
                if (!userBasicInfo.getInteresting_ventures_to_look().equals(""))
                    t_view_venture_to_look_for.setText(userBasicInfo.getInteresting_ventures_to_look());
            }
            if (userBasicInfo.getEnterpreneurs_i_like() != null) {
                if (!userBasicInfo.getEnterpreneurs_i_like().equals(""))
                    t_view_enterpreneursILik.setText(userBasicInfo.getEnterpreneurs_i_like());
            }
            if (userBasicInfo.getEvents_attended() != null) {
                if (!userBasicInfo.getEvents_attended().equals(""))
                    t_view_events_attended_WIWB.setText(userBasicInfo.getEvents_attended());
            }
            if (userBasicInfo.getEvents_m_looking() != null) {
                if (!userBasicInfo.getEvents_m_looking().equals(""))
                    t_view_events_looking_forward_to.setText(userBasicInfo.getEvents_m_looking());
            }
            if (userBasicInfo.getIndustries_i_want_to_target() != null) {
                if (!userBasicInfo.getIndustries_i_want_to_target().equals(""))
                    t_view_industry_i_want_to_target.setText(userBasicInfo.getIndustries_i_want_to_target());
            }
            t_view_name.setVisibility(View.VISIBLE);
            t_view_dob.setVisibility(View.VISIBLE);
            t_view_gender.setVisibility(View.VISIBLE);
            t_view_interested_in.setVisibility(View.VISIBLE);
            t_view_relationship_status.setVisibility(View.VISIBLE);
            t_view_religious_beliefs.setVisibility(View.VISIBLE);
            t_view_political_beliefs.setVisibility(View.VISIBLE);
            t_view_height.setVisibility(View.VISIBLE);
            t_view_weight.setVisibility(View.VISIBLE);
            t_view_body_type.setVisibility(View.VISIBLE);
            t_view_eye_color.setVisibility(View.VISIBLE);
            t_view_hair_color.setVisibility(View.VISIBLE);
            t_view_smoking.setVisibility(View.VISIBLE);
            t_view_drinking.setVisibility(View.VISIBLE);
            t_view_gym.setVisibility(View.VISIBLE);

            t_view_fav_book.setVisibility(View.VISIBLE);
            t_view_fav_shows.setVisibility(View.VISIBLE);
            t_view_fav_movies.setVisibility(View.VISIBLE);
            t_view_contries_travelled.setVisibility(View.VISIBLE);
            t_view_events_attended.setVisibility(View.VISIBLE);
            t_view_hobbies.setVisibility(View.VISIBLE);
            t_view_highest_degree.setVisibility(View.VISIBLE);

            t_view_colg_school.setVisibility(View.VISIBLE);
            t_view_degree.setVisibility(View.VISIBLE);
            t_view_emp_status.setVisibility(View.VISIBLE);
            t_view_future_goal.setVisibility(View.VISIBLE);

            t_view_area_of_interest.setVisibility(View.VISIBLE);
            t_view_what_i_read.setVisibility(View.VISIBLE);
            t_view_venture_to_look_for.setVisibility(View.VISIBLE);
            t_view_enterpreneursILik.setVisibility(View.VISIBLE);
            t_view_events_attended_WIWB.setVisibility(View.VISIBLE);
            t_view_events_looking_forward_to.setVisibility(View.VISIBLE);
            t_view_industry_i_want_to_target.setVisibility(View.VISIBLE);

            //set data in edit text for edit in future
            etName.setText(userBasicInfo.getName());
            etDateOfBirth.setText(userBasicInfo.getDate_of_birth());
            if (userBasicInfo.getGender() != null) {
                if (userBasicInfo.getGender().equalsIgnoreCase("0")) {
                    //male
                    rdGenMale.setChecked(true);
                } else if (userBasicInfo.getGender().equalsIgnoreCase("1")) {
                    //male
                    rdGenFemale.setChecked(true);
                }
            }
            if (userBasicInfo.getInterested_in() != null) {
                if (userBasicInfo.getInterested_in().equalsIgnoreCase("0")) {
                    //male
                    rdInterestedMale.setChecked(true);
                } else if (userBasicInfo.getInterested_in().equalsIgnoreCase("1")) {
                    //male
                    rdInterestedFemale.setChecked(true);
                }
            }

            //relationship status set selction
//            SetPreSelectionAdptSpin(spRelationshipStatus,arr_relationship_status,userAllProfileData.getRelationship_status());

            // religious beliefs

            //political beliefs

            //height
            Log.d(TAG, "## userAllProfileData.getHeight() : " + userBasicInfo.getHeight());
            Log.d(TAG, "## actualHeight : " + actualHeight);

            edtWeight.setText(userBasicInfo.getWeight());
            etFavouriteBook.setText(userBasicInfo.getFavorite_book());
            etFavouriteShows.setText(userBasicInfo.getFavorite_shows());
            etFavouriteMovies.setText(userBasicInfo.getFavorite_movies());
            edt_contries_travelled.setText(userBasicInfo.getCountries_travelled());
            stringBuilder.append(userBasicInfo.getCountries_travelled());
            etEventsAttended.setText(userBasicInfo.getEvents_attended());
            etEventsAttndWIWB.setText(userBasicInfo.getEvents_attended_i_want());
            etDegreeName.setText(userBasicInfo.getDegree_name());
            edtAreaOfInterest.setText(userBasicInfo.getArea_of_interest());
            etLookFor.setText(userBasicInfo.getInteresting_ventures_to_look());
            etEnterpreneuer.setText(userBasicInfo.getEnterpreneurs_i_like());
            edtEventsLookingForwardTo.setText(userBasicInfo.getEvents_m_looking());
            autoCompleteViewAddressLine.setText(userBasicInfo.getCollege_university_name());
            etFutureGoal.setText(userBasicInfo.getFuture_goal());

            // check if video url is available, then increase size of view pager

            //set view pager height programmatically , if there is video is uploaded
            if (responseGetUserProfileData.getProfile_info().length > 0) {

                list_profile_category = new ArrayList<>(Arrays.asList(responseGetUserProfileData.getProfile_info()));


                AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                String idSocialActive = "1";
                String socialCatId1Active = null, socialCatId2Active = null;
                String socialCatIdActive = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
                if (responseGetUserProfileData.getProfile_info().length > 1) {
                    socialCatId1Active = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
                }
                if (responseGetUserProfileData.getProfile_info().length > 2) {
                    socialCatId2Active = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
                }
                if (idSocialActive.equals(socialCatIdActive)) {
                    //index 0

                    if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(roundedImageViewProfilePic);
                        roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                            }
                        });
                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                    } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                        Picasso.with(this)
                                .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(roundedImageViewProfilePic);
                        roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                            }
                        });
                        AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                    }


                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.bg)
                            .showImageForEmptyUri(R.drawable.bg)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture(), social_backlground, options);

                    social_backlground.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //open next activity and play video in landscape mode
                            new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                        }
                    });
                    if (!responseGetUserProfileData.getProfile_info()[0].getProfileStatus().isEmpty()) {
                        text_view_status.setText(URLDecoder.decode(
                                responseGetUserProfileData.getProfile_info()[0].getProfileStatus(), "UTF-8"));
                    } else {
                        text_view_status.setText("Status not available.");
                    }

                    videoBioPath = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[0].getProfile_bio();
                } else if (idSocialActive.equals(socialCatId1Active)) {
                    //index 1

                    if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(roundedImageViewProfilePic);
                        roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                            }
                        });
                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                    } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                        Picasso.with(this)
                                .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(roundedImageViewProfilePic);
                        roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                            }
                        });
                        AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                    }

                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.bg)
                            .showImageForEmptyUri(R.drawable.bg)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture(), social_backlground, options);
                    social_backlground.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //open next activity and play video in landscape mode
                            new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                        }
                    });
                    if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().isEmpty()) {
                        text_view_status.setText(URLDecoder.decode(
                                responseGetUserProfileData.getProfile_info()[1].getProfileStatus(), "UTF-8"));
                    } else {
                        text_view_status.setText("Status not available.");
                    }
                    videoBioPath = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
                } else if (idSocialActive.equals(socialCatId2Active)) {
                    //index 2

                    if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                        Picasso.with(this)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(roundedImageViewProfilePic);
                        roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                            }
                        });
                        AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                    } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                        Picasso.with(this)
                                .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(roundedImageViewProfilePic);

                        roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                            }
                        });
                        AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                    }
                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showStubImage(R.drawable.bg)
                            .showImageForEmptyUri(R.drawable.bg)
                            .resetViewBeforeLoading(true)
                            .cacheInMemory()
                            .cacheOnDisc()
                            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                            .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                            .displayer(new SimpleBitmapDisplayer()) // default
                            .build();
                    imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture(), social_backlground, options);
                    social_backlground.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //open next activity and play video in landscape mode
                            new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                        }
                    });
                    if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().isEmpty()) {
                        text_view_status.setText(URLDecoder.decode(
                                responseGetUserProfileData.getProfile_info()[2].getProfileStatus(), "UTF-8"));
                    } else {
                        text_view_status.setText("Status not available.");
                    }
                    videoBioPath = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[2].getProfile_bio();
                }
                text_view_cat.setText("Social");
                if (isNetworkAvailable()) {
//                    getUserHomePostData("1");
                } else {
                    CommonDialogs.showMessageDialog(ViewProfileActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                }
                if (social == true) {
                    String idSocial = "1";
                    String socialCatId = null;
                    String socialCatId1 = null;
                    String socialCatId2 = null;
                    if (responseGetUserProfileData.getProfile_info().length > 0) {
                        socialCatId = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 1) {
                        socialCatId1 = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 2) {
                        socialCatId2 = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
                    }
                    if (idSocial.equals(socialCatId)) {
                        //index 0

                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);

                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                        }

                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture(), social_backlground, options);
                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[0].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[0].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Status not available.");
                        }
                        videoBioPath = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[0].getProfile_bio();
                    } else if (idSocial.equals(socialCatId1)) {
                        //index 1

                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);

                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        }

                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture(), social_backlground, options);
                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[1].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Status not available.");
                        }
                        videoBioPath = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
                    } else if (idSocial.equals(socialCatId2)) {
                        //index 2

                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                        }
                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture(), social_backlground, options);
                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[2].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Status not available.");
                        }
                        videoBioPath = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[2].getProfile_bio();
                    } else {
                        Picasso.with(this)
                                .load(R.drawable.profile)
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(roundedImageViewProfilePic);
                        Picasso.with(this)
                                .load(R.drawable.bg)
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);
                        text_view_status.setText("Status not available.");
                    }
                    text_view_cat.setText("Social");
                    if (isNetworkAvailable()) {
                        getUserHomePostData("1");
                    } else {
                        CommonDialogs.showMessageDialog(ViewProfileActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                    }
                    social = false;
                } else if (dating == true) {

                    String idSocial = "2";
                    String socialCatId = null;
                    String socialCatId1 = null;
                    String socialCatId2 = null;
                    if (responseGetUserProfileData.getProfile_info().length > 0) {
                        socialCatId = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
                        Log.d("ids11", socialCatId);
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 1) {
                        socialCatId1 = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
                        Log.d("ids11", socialCatId1);
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 2) {
                        socialCatId2 = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
                        Log.d("ids11", socialCatId2);
                    }
                    Log.d("ids11111", idSocial + " " + socialCatId);
                    if (idSocial.equals(socialCatId)) {
                        //index 0

                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                        }
                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture(), social_backlground, options);
                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[0].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[0].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Status not available.");
                        }
                        videoBioPath = AppConstants.PROFILE_BIO_PATH + responseGetUserProfileData.getProfile_info()[0].getProfile_bio();
                    } else if (idSocial.equals(socialCatId1)) {
                        //index 1

                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        }

                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture(), social_backlground, options);
                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[1].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Status not available.");
                        }
                        videoBioPath = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
                    } else if (idSocial.equals(socialCatId2)) {
                        //index 2

                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);

                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                        }

                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture(), social_backlground, options);
                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[2].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Status not available.");
                        }
                        videoBioPath = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[2].getProfile_bio();
                    } else {
                        Picasso.with(this)
                                .load(R.drawable.profile)
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(roundedImageViewProfilePic);
                        Picasso.with(this)
                                .load(R.drawable.bg)
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);


                        text_view_status.setText("Status not available.");

                    }
                    text_view_cat.setText("Dating");
                    if (isNetworkAvailable()) {
                        getUserHomePostData("2");
                    } else {
                        CommonDialogs.showMessageDialog(ViewProfileActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                    }
                    dating = false;
                } else if (business == true) {

                    String idSocial = "3";
                    String socialCatId = null;
                    String socialCatId1 = null;
                    String socialCatId2 = null;
                    if (responseGetUserProfileData.getProfile_info().length > 0) {
                        socialCatId = responseGetUserProfileData.getProfile_info()[0].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 1) {
                        socialCatId1 = responseGetUserProfileData.getProfile_info()[1].getCategory_id();
                    }
                    if (responseGetUserProfileData.getProfile_info().length > 2) {
                        socialCatId2 = responseGetUserProfileData.getProfile_info()[2].getCategory_id();
                    }
                    if (idSocial.equals(socialCatId)) {
                        //index 0

                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[0].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[0].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[0].getProfile_picture());
                        }

                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture(), social_backlground, options);
                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[0].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[0].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[0].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Status not available.");
                        }
                        videoBioPath = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[0].getProfile_bio();
                    } else if (idSocial.equals(socialCatId1)) {
                        //index 1

                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[1].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[1].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);

                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[1].getProfile_picture());
                        }


                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture(), social_backlground, options);
                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[1].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[1].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[1].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Status not available.");
                        }
                        videoBioPath = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[1].getProfile_bio();
                    } else if (idSocial.equals(socialCatId2)) {
                        //index 2

                        if (AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture() != null && !responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(AppConstants.PROFILE_PICTURE_PATH + "/" + responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                        } else if (responseGetUserProfileData.getProfile_info()[2].getProfile_picture().contains("https:")) {
                            Picasso.with(this)
                                    .load(responseGetUserProfileData.getProfile_info()[2].getProfile_picture())
                                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                    .into(roundedImageViewProfilePic);
                            roundedImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //open next activity and play video in landscape mode
                                    new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, responseGetUserProfileData.getProfile_info()[2].getProfile_picture());
                                }
                            });
                            AppSettings.setProfiePicturePath(responseGetUserProfileData.getProfile_info()[2].getProfile_picture());

                        }
                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.bg)
                                .showImageForEmptyUri(R.drawable.bg)
                                .resetViewBeforeLoading(true)
                                .cacheInMemory()
                                .cacheOnDisc()
                                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                                .displayer(new SimpleBitmapDisplayer()) // default
                                .build();
                        imageLoader.displayImage(AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture(), social_backlground, options);
                        social_backlground.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //open next activity and play video in landscape mode
                                new CommonDialogs().displayImageZoomDialog(ViewProfileActivity.this, AppConstants.PROFILE_PICTURE_COVER + "/" + responseGetUserProfileData.getProfile_info()[2].getCoverPicture());
                            }
                        });
                        if (!responseGetUserProfileData.getProfile_info()[2].getProfileStatus().isEmpty()) {
                            text_view_status.setText(URLDecoder.decode(
                                    responseGetUserProfileData.getProfile_info()[2].getProfileStatus(), "UTF-8"));
                        } else {
                            text_view_status.setText("Status not available.");
                        }
                        videoBioPath = AppConstants.PROFILE_BIO_PATH +
                                responseGetUserProfileData.getProfile_info()[2].getProfile_bio();
                    } else {
                        Picasso.with(this)
                                .load(R.drawable.profile)
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(roundedImageViewProfilePic);
                        Picasso.with(this)
                                .load(R.drawable.bg)
                                .placeholder(R.drawable.bg) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.bg)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(social_backlground);
                        text_view_status.setText("Status not available.");

                    }
                    text_view_cat.setText("Business");
                    if (isNetworkAvailable()) {
                        getUserHomePostData("3");
                    } else {
                        CommonDialogs.showMessageDialog(ViewProfileActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                    }
                    business = false;
                }
            }
            //recreate all fragments already created , to reflect changes

        } catch (
                Exception e)

        {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }

    }


//TODO
//    private SocialProfilePicVideoBioFragment socialProfilePicVideoBioFragment = new SocialProfilePicVideoBioFragment();
//    private DatingProfilePicVideoBioFragment datingProfilePicVideoBioFragment = new DatingProfilePicVideoBioFragment();
//    private BusinessProfilePicVideoBioFragment businessProfilePicVideoBioFragment = new BusinessProfilePicVideoBioFragment();


    public void getRSSPrefrenceList() {


        Retrofit retrofit = ApiClient.getClient();
        ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
        Call<ResponseGetInterestSubcategory> loginCall = getProfileRequest.getInterestSubcategory(AppSettings.getLoginUserId(), "1");
        loginCall.enqueue(new Callback<ResponseGetInterestSubcategory>() {
            @Override
            public void onResponse(Call<ResponseGetInterestSubcategory> call, retrofit2.Response<ResponseGetInterestSubcategory> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                        ResponseGetInterestSubcategory responseGetInterestSubcategory = response.body();


                    }
                } else {
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(ViewProfileActivity.this, "Something went wrong..", "Please contact OnGraviti Admin.");
                }
            }

            @Override
            public void onFailure(Call<ResponseGetInterestSubcategory> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }


    private void getUserHomePostData(String profileCatId) {


        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseUserPost> userPostsCall = loginRequest.get_only_user_posts(AppSettings.getLoginUserId(), profileCatId);
        userPostsCall.enqueue(new Callback<ResponseUserPost>() {
            @Override
            public void onResponse(Call<ResponseUserPost> call, retrofit2.Response<ResponseUserPost> response) {
                setData(response);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseUserPost> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });


    }

    public void setData(retrofit2.Response<ResponseUserPost> response) {
//        if (mFaqList.size() != 0 && !mFaqList.isEmpty()) {
//            mFaqList.clear();
//        }
        if (response.body() != null) {
            ResponseUserPost responseFaqs = response.body();

            responseFaqs.setPostinfo(responseFaqs.getPostinfo());
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            if (responseFaqs.getPostinfo() != null) {

                mFaqList = responseFaqs.getPostinfo();
                faqListAdapter = new UserPostListAdapter(getActivity(), mFaqList);
                recyclerViewPosts.setAdapter(faqListAdapter);
                recyclerViewPosts.setLayoutManager(mLayoutManager);
            } else {
                text_view_nopost.setVisibility(View.VISIBLE);
                text_view_nopost.setText("Post not found.");
                swipe_refresh_layout.setVisibility(View.GONE);
            }

        } else if (response.body().getStatus().equals("error")) {
            text_view_nopost.setVisibility(View.VISIBLE);
            text_view_nopost.setText("Post not found.");
            swipe_refresh_layout.setVisibility(View.GONE);

        }

    }

    private void setPostsComments(final String post_id, final String comment, String device_date) {
        if (isNetworkAvailable()) {
//            progressDialog = new CustomProgressDialog(this, "Saving Comment...");
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            progressDialog.show();

            try {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface commentRequest = retrofit.create(ApiInterface.class);
                Call<Set_Comment> userPostsCall = commentRequest.setCommentPosts(AppSettings.getLoginUserId(), post_id, comment, device_date);
                userPostsCall.enqueue(new Callback<Set_Comment>() {

                    @Override
                    public void onResponse(Call<Set_Comment> call, retrofit2.Response<Set_Comment> response) {

                        Log.d(TAG, "## setPostsComments :" + response.body().toString());
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            getPostComments(post_id, "0");
                        } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
//                            if (progress != null && progressDialog.isShowing())
//                                progressDialog.dismiss();
                            CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Something went wrong...try again");
                        }
                    }

                    @Override
                    public void onFailure(Call<Set_Comment> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } catch (Exception e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
            }
        } else {
            CommonDialogs.showMessageDialog(ViewProfileActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    private Dialog commetsDialog = null;

    public void showCommentDialog(ArrayList<Array_of_comment> list_post_comment, String postID) {

        if (list_post_comment == null)
            list_post_comment = new ArrayList<Array_of_comment>();

        if (commetsDialog == null) {
            commetsDialog = new Dialog(context);
            commetsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            commetsDialog.setContentView(R.layout.speakout_your_mind_comment_dialog);
            commetsDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            ListView lv = (ListView) commetsDialog.findViewById(R.id.lv_comments);
            et_comment = (EditText) commetsDialog.findViewById(R.id.et_comment);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());
            PostsCommentsAdapter postsCommentsAdapter = new PostsCommentsAdapter(list_post_comment, this);
            lv.setAdapter(postsCommentsAdapter);
            Button btn_send = (Button) commetsDialog.findViewById(R.id.btn_send);
            btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateForm(et_comment.getText().toString().trim())) {
                        try {
                            setPostsComments(postID, URLEncoder.encode(et_comment.getText().toString(), HTTP.UTF_8), currentDateandTime);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        et_comment.setText("");
//                        btn_send.setBackgroundColor(R.color.list_divider);
                    }
                }
            });

            commetsDialog.setCancelable(true);
            commetsDialog.setTitle("Comments");
            commetsDialog.show();
            commetsDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    commetsDialog.dismiss();
                    commetsDialog = null;
                }
            });
        } else {
            ListView lv = (ListView) commetsDialog.findViewById(R.id.lv_comments);
            PostsCommentsAdapter postsCommentsAdapter = new PostsCommentsAdapter(list_post_comment, this);
            lv.setAdapter(postsCommentsAdapter);

            ((EditText) commetsDialog.findViewById(R.id.et_comment)).setText("");
        }

        View view = commetsDialog.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //validation for email id and password(edittexts)
    public boolean validateForm(String comment) {
        boolean valid = true;
        try {
            et_comment.setError(null);
            if (comment.length() == 0) {
                setErrorMsg("Please enter Comment.", et_comment, true);
                valid = false;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return valid;
    }

    public void getPostComments(final String post_id, final String pageIndex) {
        if (isNetworkAvailable()) {
//            if (progressDialog == null || !progressDialog.isShowing()) {
//                progressDialog = new CustomProgressDialog(this, "Loading comments...");
//                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                progressDialog.show();
//            }

            try {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface commentRequest = retrofit.create(ApiInterface.class);
                Call<ResponseGetCommentsByPostID> userPostsCall = commentRequest.getCommentsByPostID(post_id, pageIndex);
                userPostsCall.enqueue(new Callback<ResponseGetCommentsByPostID>() {

                    @Override
                    public void onResponse(Call<ResponseGetCommentsByPostID> call, retrofit2.Response<ResponseGetCommentsByPostID> response) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                        Log.d(TAG, "## setPostsComments :" + response.body().toString());

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            showCommentDialog(response.body().getComment_list(), post_id);

                        } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                            if (response.body().getComment_list() != null)
                                CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), "Something went wrong...try again");
                            else
                                showCommentDialog(null, post_id);

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetCommentsByPostID> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } catch (Exception e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
            }
        } else {
            CommonDialogs.showMessageDialog(ViewProfileActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    public void saveUserPostLike(final String post_id, final int position, final UserPostListAdapter.ViewHolder holder) {
        if (isNetworkAvailable()) {
//            progressDialog = new CustomProgressDialog(this, "Please wait...");
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            progressDialog.show();
            Log.d(TAG, "## post_id : " + post_id);

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface loginRequest = retrofit.create(ApiInterface.class);
            Call<LikeUnlike> userPostsCall = loginRequest.setLikeUnlike(AppSettings.getLoginUserId(), post_id);
            userPostsCall.enqueue(new Callback<LikeUnlike>() {
                @Override
                public void onResponse(Call<LikeUnlike> call, retrofit2.Response<LikeUnlike> response) {
//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();

                    Log.d(TAG, "## :" + response.body().toString());
                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        faqListAdapter.addLike(position, holder, response.body().getLike_count(), response.body().getMsg());
                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity), response.body().getMsg());
                    }
                }

                @Override
                public void onFailure(Call<LikeUnlike> call, Throwable t) {

//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();
                }
            });

        } else {
            CommonDialogs.showMessageDialog(ViewProfileActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
        }
    }

    public void setEditPost(String post_id, String visibility_status, String post_content, String emoji, String device_time) {
        CustomProgressDialog progressDialog;
        progressDialog = new CustomProgressDialog(context, context.getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseUpdateStatus> userPostsCall = loginRequest.setEditPost(AppSettings.getLoginUserId(), post_id, visibility_status, post_content, emoji, device_time);
        userPostsCall.enqueue(new Callback<ResponseUpdateStatus>() {
            @Override
            public void onResponse(Call<ResponseUpdateStatus> call, Response<ResponseUpdateStatus> response) {
                setData1(response);
                progressDialog.dismiss();
                if (socialEditPost == true) {
                    getUserHomePostData("1");
                    socialEditPost = false;
                } else if ((datingEditPost == true)) {
                    getUserHomePostData("2");
                    datingEditPost = false;
                } else if (businessEditPost == true) {
                    getUserHomePostData("3");
                    businessEditPost = false;
                }
            }


            @Override
            public void onFailure(Call<ResponseUpdateStatus> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });


    }

    public void setData1(Response<ResponseUpdateStatus> response) {
        if (response.body() != null) {
            ResponseUpdateStatus responseUpdateStatus = response.body();
            if (responseUpdateStatus.getStatus().equals("success")) {
                Toast.makeText(context, responseUpdateStatus.getMsg(), Toast.LENGTH_LONG).show();
            }


        }
    }
}
