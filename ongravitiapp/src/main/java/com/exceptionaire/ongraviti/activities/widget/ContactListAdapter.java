package com.exceptionaire.ongraviti.activities.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.chats.ChatListAdapter;
import com.exceptionaire.ongraviti.activities.chats.ChatsMainActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.OrbitContact;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

public class ContactListAdapter extends ArrayAdapter<OrbitContact> {

    private int resource; // store the resource layout id for 1 row
    private boolean inSearchMode = false;

    private ContactsSectionIndexer indexer = null;
    private ChatsMainActivity mActivity;

    public ContactListAdapter(Context context, int resource, List<OrbitContact> orbitFriendList) {
        super(context, resource, orbitFriendList);
        this.resource = resource;

        // need to sort the items array first, then pass it to the indexer
        Collections.sort(orbitFriendList, new ContactItemComparator());
        setIndexer(new ContactsSectionIndexer(orbitFriendList));

        mActivity = (ChatsMainActivity) context;
    }

    // get the section textview from row view
    // the section view will only be shown for the first item
    public TextView getSectionTextView(View rowView) {
        TextView sectionTextView = (TextView) rowView.findViewById(R.id.txt_section);
        return sectionTextView;
    }

    public void showSectionViewIfFirstItem(View rowView, OrbitContact item, int position) {
        TextView sectionTextView = getSectionTextView(rowView);

        // if in search mode then dun show the section header
        if (inSearchMode) {
            sectionTextView.setVisibility(View.GONE);
        } else {
            // if first item then show the header

            if (indexer.isFirstItemInSection(position)) {

                String sectionTitle = indexer.getSectionTitle(item.getOrbit_name());
                sectionTextView.setText(sectionTitle);
                sectionTextView.setVisibility(View.GONE);

            } else
                sectionTextView.setVisibility(View.GONE);
        }
    }

    // do all the data population for the row here
    // subclass overwrite this to draw more items
    public void populateDataForRow(View parentView, OrbitContact item, int position) {
        // default just draw the item only
        View infoView = parentView.findViewById(R.id.container_contact);
        TextView nameView = (TextView) infoView.findViewById(R.id.txt_contact);
        LinearLayout container_contact = infoView.findViewById(R.id.container_contact);
        nameView.setText(item.getOrbit_name());
        final ImageView userImage = (ImageView) infoView.findViewById(R.id.badge_contact);
        //@LAX_CHAT
        String strUrl = item.getProfile_picture();
        if (strUrl != null && !strUrl.isEmpty()) {
            Log.d("imagerrrrr", strUrl);
            if (!strUrl.contains("https://graph")) {
                Picasso.with(mActivity)
                        .load(AppConstants.PROFILE_PICTURE_PATH + "/" + strUrl)
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(userImage);
            } else if (strUrl.contains("https://graph")) {
                Picasso.with(mActivity)
                        .load(strUrl)
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(userImage);
            }
//            Glide.with(mActivity)
//                    .load(AppConstants.PROFILE_PICTURE_THUMB + "/" + strUrl)
//                    .listener(new RequestListener<String, GlideDrawable>() {
//                        @Override
//                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            Bitmap bm = BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.profile);
//                            userImage.setImageBitmap(bm);
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(GlideDrawable resource, String model,
//                                                       Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            return false;
//                        }
//                    })
//                    .override(AppConstants.PREFERRED_IMAGE_SIZE_PREVIEW, AppConstants.PREFERRED_IMAGE_SIZE_PREVIEW)
//                    .error(R.drawable.ic_action_close1)
//                    .into(userImage);
        } else {
            Bitmap bm = BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.profile);
            userImage.setImageBitmap(bm);
        }
    }

    // this should be override by subclass if necessary
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewGroup parentView;
        OrbitContact item = getItem(position);

        //Log.i("ContactListAdapter", "item: " + item.getItemForIndex());

        if (convertView == null) {
            parentView = new LinearLayout(getContext()); // Assumption: the resource parent id is a linear layout
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(inflater);
            vi.inflate(resource, parentView, true);
        } else {
            parentView = (LinearLayout) convertView;
        }

        // for the very first section item, we will draw a section on top
        showSectionViewIfFirstItem(parentView, item, position);

        // set row items here
        populateDataForRow(parentView, item, position);
        return parentView;

    }

    public boolean isInSearchMode() {
        return inSearchMode;
    }

    public void setInSearchMode(boolean inSearchMode) {
        this.inSearchMode = inSearchMode;
    }

    public ContactsSectionIndexer getIndexer() {
        return indexer;
    }

    public void setIndexer(ContactsSectionIndexer indexer) {
        this.indexer = indexer;
    }


}
