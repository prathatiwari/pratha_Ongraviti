package com.exceptionaire.ongraviti.activities.myorbit;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.OrbitRequestAdapter;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.MyOrbitRequest;
import com.exceptionaire.ongraviti.model.ResponseAcceptRejectOrbitRequest;
import com.exceptionaire.ongraviti.model.ResponseMyOrbitRequests;
import com.exceptionaire.ongraviti.model.ResponseSuccess;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by root on 06/02/17.
 */

public class MyOrbitRequestsFragment extends Fragment implements AppConstants {

    private ProgressDialog progressDialog;
    private ArrayList<MyOrbitRequest> orbitRequests;
    private OrbitRequestAdapter orbitAdapter;
    private ListView list_contact;
    private TextView tv_msg;

    public MyOrbitRequestsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orbitAdapter = new OrbitRequestAdapter(new ArrayList<MyOrbitRequest>(), getActivity(), this);
        progressDialog = new CustomProgressDialog(getActivity(), "Loading...");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.layout_listview, container, false);
        tv_msg = (TextView) rootView.findViewById(R.id.tv_msg);
        tv_msg.setText(getString(R.string.no_orbit_requests));
        list_contact = (ListView) rootView.findViewById(R.id.list_view_contact);
        list_contact.setAdapter(orbitAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        showOrbitRequestList();

        super.onResume();
    }

    private void showOrbitRequestList() {

        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface loginRequest = retrofit.create(ApiInterface.class);
        Call<ResponseMyOrbitRequests> loginCall = loginRequest.getMyOrbitRequestList(AppSettings.getLoginUserId());
        loginCall.enqueue(new Callback<ResponseMyOrbitRequests>() {
            @Override
            public void onResponse(Call<ResponseMyOrbitRequests> call, retrofit2.Response<ResponseMyOrbitRequests> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {

                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        ResponseMyOrbitRequests responseMyOrbitRequests = response.body();
                        if (responseMyOrbitRequests != null && responseMyOrbitRequests.getOrbit_list() != null && responseMyOrbitRequests.getOrbit_list().size() > 0) {
                            List<MyOrbitRequest> myOrbitRequestArrayList = response.body().getOrbit_list();
                            orbitAdapter.setOrbitRequestList(myOrbitRequestArrayList);
                            tv_msg.setVisibility(View.GONE);
                            list_contact.setVisibility(View.VISIBLE);
                        } else {
                            tv_msg.setVisibility(View.VISIBLE);
                            list_contact.setVisibility(View.GONE);
                        }
                    } else {
                        tv_msg.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseMyOrbitRequests> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Log.d("", "## onErrorResponse : " + t.toString());
            }
        });
    }

    public void acceptRejectOrbitRequest(final String senderUserId, final String flag, final int position, String action) {

        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        if (flag == "0") {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface loginRequest = retrofit.create(ApiInterface.class);
            Call<ResponseAcceptRejectOrbitRequest> loginCall = loginRequest.acceptORRejectOrbitRequest(AppSettings.getLoginUserId(), senderUserId, PROFILE_CATEGORY_ONE_SOCIAL, flag, currentDateandTime);
            loginCall.enqueue(new Callback<ResponseAcceptRejectOrbitRequest>() {
                @Override
                public void onResponse(Call<ResponseAcceptRejectOrbitRequest> call, retrofit2.Response<ResponseAcceptRejectOrbitRequest> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
//                    Log.e("Response=============", "" + response.body().toString());

                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        final Dialog dialog = CommonDialogs.showMessageDialog(getActivity(), getResources().getString(R.string.ongravity), response.body().getMsg().getMessage());
                        Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                        dialog_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                orbitAdapter.removeItem(position);
                            }
                        });

                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        CommonDialogs.showMessageDialog(getActivity(), "Error", "Something went wrong...Please try again");
                    }
                }

                @Override
                public void onFailure(Call<ResponseAcceptRejectOrbitRequest> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                 //   Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface loginRequest = retrofit.create(ApiInterface.class);
            Call<ResponseSuccess> loginCall = loginRequest.rejectOrbitRequest(AppSettings.getLoginUserId(), senderUserId, PROFILE_CATEGORY_ONE_SOCIAL, flag, currentDateandTime);
            loginCall.enqueue(new Callback<ResponseSuccess>() {
                @Override
                public void onResponse(Call<ResponseSuccess> call, retrofit2.Response<ResponseSuccess> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Log.e("Response=============", "" + response.body().toString());


                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                        final Dialog dialog = CommonDialogs.showMessageDialog(getActivity(), getResources().getString(R.string.ongravity), response.body().getMsg());
                        Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                        dialog_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                orbitAdapter.removeItem(position);
                            }
                        });

                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        CommonDialogs.showMessageDialog(getActivity(), getResources().getString(R.string.ongravity), "Something went wrong...Please try again");
                    }
                }

                @Override
                public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                  //  Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
