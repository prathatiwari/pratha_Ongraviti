package com.exceptionaire.ongraviti.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.PostUserVisibilityAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.imoji.EmojiconEditText;
import com.exceptionaire.ongraviti.imoji.EmojiconGridView;
import com.exceptionaire.ongraviti.imoji.EmojiconsPopup;
import com.exceptionaire.ongraviti.imoji.emoji.Emojicon;
import com.exceptionaire.ongraviti.model.UserPostResponse;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.squareup.picasso.Picasso;

import org.apache.http.protocol.HTTP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

/**
 * Created by root on 27/3/17.
 */
public class PostMediaActivity extends BaseActivity implements AppConstants, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private String TAG = "ActPostMedia";
    boolean isKeyboardOpen = true;
    private ImageView ibtn_emojis, imgPost, imgDelete, play_video, back_button;
    private Spinner visibility_users;
    RoundedImageView user_profile_image;
    private TextView user_name, send_post;
    private EmojiconEditText post_text;

    private VideoView videoview;
    private CustomProgressDialog progressDialog;

    private String user_d = null;
    private String post_type = null;
    private String emoji = null;


    private MultipartBody.Part postImageBody, postVideoBody, postVideoThumbBody;
    private BitmapFactory.Options options;
    private RequestBody descriptionUserId, descriptionPostType, descriptionPostContent, descriptionEmoji, descriptionVisibiltyUser, deviceDate, catId;
    private MediaController media_Controller;
    private Bitmap myBitmap;
    private RelativeLayout imageViewLayout, video_layout;
    private File post_image, post_video, post_video_thumb;
    private RequestBody requestPhotoFile, requestVideoFile, requestVideoThumb;
    private LinearLayout dragView;
    private RelativeLayout relativeImage, relativeVideo;

    private PostUserVisibilityAdapter postUserVisibilityAdapter;

    private String[] visibiltyUser = {"Public", "My Orbit", "Only Me"};
    private int userIcons[] = {R.drawable.post_world, R.drawable.post_friend, R.drawable.post_friend};

    private String visibilty_User = null;

    private Intent intent;

    private boolean newProfilePicture = false;
    private Uri recordedFileUri;
    private String selectedVideoPath, selectedVideoUrl;
    private boolean newVideoBio = false;

    private EmojiconsPopup popup;
    private View rootView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_media);
        initialize();


    }


    public void initialize() {
        rootView = findViewById(R.id.main_root);
        user_profile_image = (RoundedImageView) findViewById(R.id.user_profile_image);
        user_name = (TextView) findViewById(R.id.user_name);
        visibility_users = (Spinner) findViewById(R.id.visibility_users);
        ibtn_emojis = (ImageView) findViewById(R.id.ibtn_emojis);
        send_post = (TextView) findViewById(R.id.send_post);
        dragView = (LinearLayout) findViewById(R.id.dragView);
        back_button = (ImageView) findViewById(R.id.back_button);


        post_text = (EmojiconEditText) findViewById(R.id.post_text);
        post_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isKeyboardOpen = true;
            }
        });
        if (isKeyboardOpen) {
            dragView.setVisibility(View.GONE);
        } else {
            dragView.setVisibility(View.VISIBLE);
        }
        imgPost = (ImageView) findViewById(R.id.imgPost);
        imgDelete = (ImageView) findViewById(R.id.imgDelete);
        play_video = (ImageView) findViewById(R.id.play_video);
        videoview = (VideoView) findViewById(R.id.videoview);

        imageViewLayout = (RelativeLayout) findViewById(R.id.imageViewLayout);
        video_layout = (RelativeLayout) findViewById(R.id.video_layout);

        relativeImage = (RelativeLayout) findViewById(R.id.relativeImage);
        relativeVideo = (RelativeLayout) findViewById(R.id.relativeVideo);

        postUserVisibilityAdapter = new PostUserVisibilityAdapter(this, userIcons, visibiltyUser);
        visibility_users.setAdapter(postUserVisibilityAdapter);


        popup = new EmojiconsPopup(rootView, this);
        if (AppSettings.getUserProfileName() != null)
            user_name.setText(AppSettings.getUserProfileName());

        //Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        //If the emoji popup is dismissed, change ibtn_emojis to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
//                changeEmojiKeyboardIcon(ibtn_emojis, R.mipmap.smiley);
                ibtn_emojis.setImageResource(R.drawable.smiley);
            }
        });

        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (post_text == null || emojicon == null) {
                    return;
                }

                int start = post_text.getSelectionStart();
                int end = post_text.getSelectionEnd();
                if (start < 0) {
                    post_text.append(emojicon.getEmoji());
                } else {
                    post_text.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                post_text.dispatchKeyEvent(event);
            }
        });


        if (AppSettings.getLoginUserId() != null) {
            if (AppSettings.getProfilePictureName() != null) {
                user_name.setText(AppSettings.getProfilePictureName());
            }

            if (AppSettings.getProfilePicturePath() != null && !AppSettings.getProfilePicturePath().isEmpty()) {

                if (!AppSettings.getProfilePicturePath().contains("https://graph")) {
                    Picasso.with(this)
                            .load(AppSettings.getProfilePicturePath())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(user_profile_image);
                } else if (AppSettings.getProfilePicturePath().contains("https://graph")) {
                    String[] separated = AppSettings.getProfilePicturePath().split("https://graph");
                    String pathFB = separated[1];
                    Picasso.with(this)
                            .load("https://graph" + pathFB)
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(user_profile_image);
                }
            }
        }
        checkStatus();

        play_video.setOnClickListener(this);
        imgDelete.setOnClickListener(this);
        send_post.setOnClickListener(this);
        dragView.setOnClickListener(this);
        back_button.setOnClickListener(this);
        ibtn_emojis.setOnClickListener(this);
        visibility_users.setOnItemSelectedListener(this);

        relativeImage.setOnClickListener(this);
        relativeVideo.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // checkStatus();
    }

    public void onPostClick() {
        if (isNetworkAvailable()) {

            if (AppSettings.getLoginUserId() != null) {
                Log.d(TAG, "## onPostClick");
                try {
                    emoji = URLEncoder.encode(post_text.getText().toString().trim(), HTTP.UTF_8);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                user_d = AppSettings.getLoginUserId();
                descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), user_d);
                descriptionPostContent = RequestBody.create(MediaType.parse("text/plain"), emoji);
                descriptionVisibiltyUser = RequestBody.create(MediaType.parse("text/plain"), visibilty_User);
                descriptionEmoji = RequestBody.create(MediaType.parse("text/plain"), emoji);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String currentDateandTime = format.format(new Date());
                Log.d("ajahsasjas", currentDateandTime);
                deviceDate = RequestBody.create(MediaType.parse("text/plain"), currentDateandTime);
                catId = RequestBody.create(MediaType.parse("text/plain"), AppSettings.getProfileDefaultCategory());
                if (new File(POST_IMAGE_PATH).exists()) {
                    post_text.setHint("Say something about this photo...");
                    post_type = "1";

                    descriptionPostType = RequestBody.create(MediaType.parse("text/plain"), post_type);
                    post_image = new File(POST_IMAGE_PATH);
                    requestPhotoFile = RequestBody.create(MediaType.parse("image/*"), post_image);
                    postImageBody = MultipartBody.Part.createFormData("posts_pic", post_image.getName(), requestPhotoFile);
                    uploadSocialPhoto(descriptionUserId, descriptionPostType, descriptionPostContent, descriptionEmoji, catId, descriptionVisibiltyUser, postImageBody, deviceDate);
                } else if (new File(POST_VIDEO_PATH).exists() && new File(POST_VIDEO_THUMB).exists()) {
                    post_text.setHint("Say something about this video...");
                    post_type = "2";
                    descriptionPostType = RequestBody.create(MediaType.parse("text/plain"), post_type);
                    post_video = new File(POST_VIDEO_PATH);
                    post_video_thumb = new File(POST_VIDEO_THUMB);

                    requestVideoFile = RequestBody.create(MediaType.parse("video/*"), post_video);
                    requestVideoThumb = RequestBody.create(MediaType.parse("image/*"), post_video_thumb);

                    postVideoBody = MultipartBody.Part.createFormData("posts_video", post_video.getName(), requestVideoFile);
                    postVideoThumbBody = MultipartBody.Part.createFormData("post_video_thumb", post_video_thumb.getName(), requestVideoThumb);

                    uploadSocialVideo(descriptionUserId, descriptionPostType, descriptionPostContent, descriptionEmoji, catId, descriptionVisibiltyUser, postVideoBody, postVideoThumbBody, deviceDate);

                } else {
                    post_text.setHint("What's on your mind ?");
                    post_type = "0";
                    if (post_text.getText().toString().trim().length() != 0) {
                        simplePost(user_d, post_type, emoji, emoji, AppSettings.getProfileDefaultCategory(), visibilty_User, currentDateandTime);
                    } else {
                        Toast.makeText(this, "Please say something here.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else {
            CommonDialogs.dialog_with_one_btn_without_title(PostMediaActivity.this, getResources().getString(R.string.check_internet));
        }
    }


    /*************
     * Post Image
     **************/

    private void uploadSocialPhoto(RequestBody descriptionUserId, RequestBody descriptionPostType, RequestBody descriptionPostContent, RequestBody descriptionEmoji, RequestBody profile_ct_id, RequestBody visibility_status, MultipartBody.Part postImageBody, RequestBody device_date) {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<UserPostResponse> callUploadVideo = requestInterface.set_postsImage(descriptionUserId, descriptionPostType, descriptionPostContent, descriptionEmoji, profile_ct_id, visibility_status, postImageBody, device_date);
            callUploadVideo.enqueue(new Callback<UserPostResponse>() {
                @Override
                public void onResponse(Call<UserPostResponse> call, Response<UserPostResponse> response) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equals("success")) {
                            if (new File(POST_IMAGE_PATH).exists()) {
                                new File(POST_IMAGE_PATH).delete();
                            }
                            startActivity(new Intent(PostMediaActivity.this, HomeUserPostsActivity.class));
                            finish();
                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (response.body().getStatus().equals("error")) {

                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserPostResponse> call, Throwable t) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "## onFailure e : " + t.getMessage());
                    t.printStackTrace();
                }
            });


        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

/************************************************************************************/

    /*************
     * Post Image
     **************/

    private void uploadSocialVideo(RequestBody descriptionUserId, RequestBody descriptionPostType, RequestBody descriptionPostContent, RequestBody descriptionEmoji, RequestBody profile_ct_id, RequestBody visibility_status, MultipartBody.Part postVideoBody, MultipartBody.Part postVideoThumbBody, RequestBody device_date) {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<UserPostResponse> callUploadVideo = requestInterface.set_postsVideo(descriptionUserId, descriptionPostType, descriptionPostContent, descriptionEmoji, profile_ct_id, visibility_status, postVideoBody, postVideoThumbBody, device_date);
            callUploadVideo.enqueue(new Callback<UserPostResponse>() {
                @Override
                public void onResponse(Call<UserPostResponse> call, Response<UserPostResponse> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                            if (new File(POST_VIDEO_PATH).exists() && new File(POST_VIDEO_THUMB).exists()) {
                                new File(POST_VIDEO_PATH).delete();
                                new File(POST_VIDEO_THUMB).delete();
                            }
                            startActivity(new Intent(PostMediaActivity.this, HomeUserPostsActivity.class));
                            finish();
                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (response.body().getStatus().equals(RESPONSE_ERROR)) {

                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserPostResponse> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "## onFailure e : " + t.getMessage());
                    t.printStackTrace();
                }
            });


        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    /******************************************************************************************************/
    /*************
     * Simple Post
     *************/


    private void simplePost(String user_id, String post_type, String post_content, String emoji, String profileCtId, String visibility_status, String device_date) {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<UserPostResponse> getPostRequest = requestInterface.set_postContent(user_id, post_type, post_content, emoji, profileCtId, visibility_status, device_date);
            getPostRequest.enqueue(new Callback<UserPostResponse>() {
                @Override
                public void onResponse(Call<UserPostResponse> call, Response<UserPostResponse> response) {

                    if (progressDialog.isShowing())
                        progressDialog.dismiss();


                    if (response.body() != null) {

                        if (response.body().getStatus().equals("success")) {
                            startActivity(new Intent(PostMediaActivity.this, HomeUserPostsActivity.class));
                            finish();
                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (response.body().getStatus().equals("error")) {

                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    }

                }

                @Override
                public void onFailure(Call<UserPostResponse> call, Throwable t) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });


        } catch (Exception e) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

    }

    @Override
    protected void onDestroy() {
        if (new File(POST_IMAGE_PATH).exists()) {
            new File(POST_IMAGE_PATH).delete();
        } else if (new File(POST_VIDEO_PATH).exists() && new File(POST_VIDEO_THUMB).exists()) {
            new File(POST_VIDEO_PATH).delete();
            new File(POST_VIDEO_THUMB).delete();
        }
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.play_video:

                video_layout.setVisibility(View.VISIBLE);
                imageViewLayout.setVisibility(View.GONE);
                play_video.setVisibility(View.GONE);
                imgPost.setVisibility(View.GONE);
                videoview.setVisibility(View.VISIBLE);

                media_Controller = new MediaController(this);
                media_Controller.setAnchorView(play_video);
                Uri video = Uri.parse(POST_VIDEO_PATH);
                videoview.setMediaController(media_Controller);
                videoview.setVideoURI(video);

                videoview.start();
                break;
            case R.id.imgDelete:

                Animation bottomUp = AnimationUtils.loadAnimation(this,
                        R.anim.up_from_bottom);

                dragView.startAnimation(bottomUp);
                dragView.setVisibility(View.VISIBLE);
                if (new File(POST_IMAGE_PATH).exists()) {
                    new File(POST_IMAGE_PATH).delete();
                    imageViewLayout.setVisibility(View.GONE);

                } else if (new File(POST_VIDEO_PATH).exists() && new File(POST_VIDEO_THUMB).exists()) {
                    new File(POST_VIDEO_PATH).delete();
                    new File(POST_VIDEO_THUMB).delete();
                    video_layout.setVisibility(View.GONE);
                    play_video.setVisibility(View.GONE);
                    imgPost.setImageBitmap(null);
                }
                checkStatus();

                break;
            case R.id.send_post:
                onPostClick();
                break;
            case R.id.back_button:

                startActivity(new Intent(this, HomeUserPostsActivity.class));
                finish();

                break;
            case R.id.relativeImage:
                checkForPermissionsAndShowOptionDialogForImage();
                break;
            case R.id.relativeVideo:
                checkForPermissionsAndShowOptionDialogForVideo();
                break;
            case R.id.ibtn_emojis:

                if (!popup.isShowing()) {

                    //If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                        ibtn_emojis.setImageResource(R.drawable.ic_action_keyboard);
                        //  changeEmojiKeyboardIcon(ibtn_emojis, R.drawable.ic_action_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else {
                        post_text.setFocusableInTouchMode(true);
                        post_text.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(post_text, InputMethodManager.SHOW_IMPLICIT);
                        ibtn_emojis.setImageResource(R.drawable.ic_action_keyboard);
                        //changeEmojiKeyboardIcon(ibtn_emojis, R.drawable.ic_action_keyboard);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else {
                    popup.dismiss();
                }

                break;

        }


    }

    private void checkStatus() {

        if (new File(POST_IMAGE_PATH).exists()) {
            post_text.setHint("Say something about this photo...");
            video_layout.setVisibility(View.GONE);
            imageViewLayout.setVisibility(View.VISIBLE);
            imgPost.setVisibility(View.VISIBLE);
            videoview.setVisibility(View.GONE);
            myBitmap = BitmapFactory.decodeFile(POST_IMAGE_PATH, options);
            imgDelete.setVisibility(View.VISIBLE);
            play_video.setVisibility(View.GONE);

            imgPost.setImageBitmap(myBitmap);
            Animation bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_down);

            dragView.startAnimation(bottomUp);
            dragView.setVisibility(View.GONE);

        } else if (new File(POST_VIDEO_PATH).exists() && new File(POST_VIDEO_THUMB).exists()) {
            post_text.setHint("Say something about this video...");
            imageViewLayout.setVisibility(View.VISIBLE);
            video_layout.setVisibility(View.GONE);
            play_video.setVisibility(View.VISIBLE);
            imgPost.setVisibility(View.VISIBLE);

            myBitmap = BitmapFactory.decodeFile(POST_VIDEO_THUMB, options);

            imgPost.setImageBitmap(myBitmap);

            Animation bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_down);
            imgDelete.setVisibility(View.VISIBLE);

            dragView.startAnimation(bottomUp);
            dragView.setVisibility(View.GONE);
        } /*else if (isKeyboardOpen) {
            dragView.setVisibility(View.GONE);
            imgDelete.setVisibility(View.GONE);
        }*/ else {

            post_text.setHint("What's on your mind ?");
            imgDelete.setVisibility(View.GONE);
            dragView.setVisibility(View.VISIBLE);
            Animation bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.up_from_bottom);
            imgDelete.setVisibility(View.GONE);
            dragView.startAnimation(bottomUp);
        }

    }

    @Override
    public void onBackPressed() {
        isKeyboardOpen = false;
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            visibilty_User = "0";
        } else if (position == 1) {
            visibilty_User = "1";
        } else if (position == 2) {
            visibilty_User = "2";
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /*************
     * Image
     ***********************/
    private void checkForPermissionsAndShowOptionDialogForImage() {
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if (!AppUtils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            selectImage();
        }
    }

    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    getCamera();
                } else if (options[item].equals("Choose from Gallery")) {
                    getGallery();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    public void getCamera() {
        if (!AppUtils.isDeviceSupportCamera(this)) {
            Toast.makeText(this,
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
        } else {
            File dumpFolder = new File(SPEED_DATING_APP_PATH);
            if (!dumpFolder.exists()) {
                dumpFolder.mkdirs();
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(SPEED_DATING_APP_PATH + File.separator + "post_image_path" + ".jpg")));
            startActivityForResult(intent, CAMERA_REQUEST);
        }
    }

    public void getGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY_REQUEST);
    }

    /************
     * Video
     ********************/


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkForPermissionsAndShowOptionDialogForVideo() {

        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!AppUtils.hasPermissions(getActivity(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, REQUEST_TAKE_GALLERY_VIDEO);
        } else {

            if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            } else {
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.INTERNAL_CONTENT_URI);
            }
            selectVideo();
        }
    }

    private void selectVideo() {

        final CharSequence[] options = {/*"Take Video",*/ "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                /*if (options[item].equals("Take Video")) {
                    recordVideo();
                } else*/
                if (options[item].equals("Choose from Gallery")) {
                    selectVideoFromGallery();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }


    public void selectVideoFromGallery() {

        intent.setAction(Intent.ACTION_PICK);
        intent.setType("video/*");
        intent.putExtra("return-data", true);
        startActivityForResult(intent, REQUEST_TAKE_GALLERY_VIDEO);
    }


    public void recordVideo() {

        intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        // create a file to save the video
        recordedFileUri = getOutputMediaFileUri(MEDIA_TYPE_RECORD_VIDEO);

        // set the image file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, recordedFileUri);

        // set the video image quality to high
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        // start the Video Capture Intent
        startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);

    }

    private static Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        File dir = new File(SPEED_DATING_APP_PATH);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        File mediaFile;

        if (type == MEDIA_TYPE_RECORD_VIDEO) {
            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(POST_VIDEO_PATH);

        } else {
            return null;
        }

        return mediaFile;
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }


    public void createVideoThumbnail(String filePath) {
        Bitmap bmThumbnail;


        // MINI_KIND: 512 x 384 thumbnail
        bmThumbnail = ThumbnailUtils.createVideoThumbnail(filePath,
                MediaStore.Images.Thumbnails.MINI_KIND);
        if (bmThumbnail != null) {
            try {

                bmThumbnail.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(POST_VIDEO_THUMB));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        imageViewLayout.setVisibility(View.VISIBLE);
        video_layout.setVisibility(View.GONE);
        play_video.setVisibility(View.VISIBLE);
        imgPost.setVisibility(View.VISIBLE);

        myBitmap = BitmapFactory.decodeFile(POST_VIDEO_THUMB, options);

        imgPost.setImageBitmap(myBitmap);

    }


    /*************************************************************/


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        int IMAGE_SIZE = 0;
       /* if (bottomSheetDialog != null)
            bottomSheetDialog.dismiss();*/

        File dir = new File(SPEED_DATING_APP_PATH);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (POST_IMAGE_PATH != null) {
                        changeOrientation(Uri.fromFile(new File(POST_IMAGE_PATH)));
                    } else {
                        newProfilePicture = false;
//                    showMessageDialogInstance(
//                            getResources().getString(R.string.app_name), "Image Path is null !!");
                        Toast.makeText(this, "Image Path is null !!", Toast.LENGTH_SHORT).show();
                    }

                } else if (resultCode == RESULT_CANCELED) {
                    newProfilePicture = false;
                    Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                } else {
                    newProfilePicture = false;
                    Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                }
                break;

            case 10:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;

                    final Bitmap bitmap = BitmapFactory.decodeFile(POST_IMAGE_PATH,
                            options);
                    if (null != bitmap) {
                        if (!new File(POST_IMAGE_PATH).exists()) {
                            IMAGE_SIZE = getScreenWidth(this) / 2;
                            if (IMAGE_SIZE <= 0)
                                IMAGE_SIZE = 400;
                            Bitmap image = com.exceptionaire.ongraviti.utilities.AnimationUtils.getScaledBitmap(
                                    bitmap, IMAGE_SIZE, IMAGE_SIZE);
                            if (image != null) {
//                                (imageView_sidePic).setImageBitmap(image);
                                // findViewById(R.id.imageTag).setVisibility(View.GONE);
                            } else {
//                                showMessageDialogInstance(
//                                        getResources().getString(
//                                                R.string.app_name),
//                                        "Invalid image , please pick another image.");
                                Toast.makeText(this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                            }
                        } else
//                            showMessageDialogInstance(getResources()
//                                            .getString(R.string.app_name),
//                                    "Image View is null !!");

                            Toast.makeText(this, "Image View is null !!", Toast.LENGTH_SHORT).show();
                    }

                }
            case GALLERY_REQUEST:
                if (resultCode == RESULT_OK) {
                    // Get the Image from data
                    Uri selectedImage = intent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    // Get the cursor
                    Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    if (SPEED_DATING_APP_PATH != null) {
                        try {
                            if (!imgDecodableString.equals(POST_IMAGE_PATH)) {
                                copyFile(imgDecodableString, POST_IMAGE_PATH);
                                checkStatus();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
//                        showMessageDialogInstance(
//                                getResources().getString(R.string.app_name),
//                                getResources().getString(R.string.cant_copy_image));

                            Toast.makeText(this, getResources().getString(R.string.cant_copy_image), Toast.LENGTH_SHORT).show();
                        }
                        IMAGE_SIZE = getScreenWidth(this) / 2;
                        if (IMAGE_SIZE <= 0)
                            IMAGE_SIZE = 500;
                        Bitmap image = null;
                        try {
                            if (!imgDecodableString.equals(POST_IMAGE_PATH)) {
                                image = com.exceptionaire.ongraviti.utilities.AnimationUtils.decodeSampledBitmapFromResourceMemOpt(
                                        new FileInputStream(new File(POST_IMAGE_PATH)), IMAGE_SIZE, IMAGE_SIZE);
                            } else {
                                image = BitmapFactory.decodeFile(POST_IMAGE_PATH);
                            }
                        } catch (OutOfMemoryError e) {
                            e.printStackTrace();
//                        showMessageDialogInstance(
//                                getResources().getString(R.string.app_name), "Image is too big");

                            Toast.makeText(this, "Image is too big", Toast.LENGTH_SHORT).show();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (image != null) {
//                            imageView_sidePic.setImageBitmap(image);
                            newProfilePicture = true;
                        } else {
//          1
                            Toast.makeText(this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                            newProfilePicture = false;
                        }
                    } else {
//                    showMessageDialogInstance(
//                            getResources().getString(R.string.app_name), "Image Path is null !!");
                        Toast.makeText(this, "Image Path is null !!", Toast.LENGTH_SHORT).show();
                        newProfilePicture = false;
                    }
                } else {
                    Toast.makeText(this, "You haven't picked an Image", Toast.LENGTH_LONG).show();
                    newProfilePicture = false;
                }

                break;
            case REQUEST_TAKE_GALLERY_VIDEO:

                if (resultCode == RESULT_OK) {
                    selectedVideoPath = getPath(intent.getData());

                    try {
                        if (selectedVideoPath == null) {
                            Log.e("video path = null!", selectedVideoPath);
                            newVideoBio = false;

                            finish();
                        } else {
                            Log.e("video path = nul", selectedVideoPath);

                            newVideoBio = true;
                            copyFile(selectedVideoPath, POST_VIDEO_PATH);
                            createVideoThumbnail(POST_VIDEO_PATH);


                            /**
                             * try to do something there
                             * selectedVideoPath is path to the selected video
                             */
                        }
                    } catch (Exception e) {
                        //#debug
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "You haven't picked an Video", Toast.LENGTH_LONG).show();
                    newVideoBio = false;

                }

                break;

            case CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (POST_VIDEO_PATH != null) {
                        newVideoBio = true;
                        createVideoThumbnail(POST_VIDEO_PATH);

                    }
                } else if (resultCode == RESULT_CANCELED) {
                    newVideoBio = false;

                    Toast.makeText(this, "User cancelled the video capture.",
                            Toast.LENGTH_LONG).show();
                } else {
                    newVideoBio = false;

                    Toast.makeText(this, "Video capture failed.",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }

        checkStatus();
    }

    private void changeOrientation(Uri imageUri) {
        int IMAGE_SIZE = 0;
        int rotate = 0;

        try {
            this.getContentResolver().notifyChange(imageUri, null);

            File imageFile = new File(POST_IMAGE_PATH);
            ExifInterface exif = new ExifInterface(
                    imageFile.getPath());

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
            Log.v(LOG_TAG, "Exif orientation: " + orientation);


            /****** Image rotation ****/
            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);

            options = new BitmapFactory.Options();
            options.inSampleSize = 5;
            myBitmap = BitmapFactory.decodeFile(POST_IMAGE_PATH, options);
            myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(),
                    matrix, true);
            video_layout.setVisibility(View.GONE);
            imageViewLayout.setVisibility(View.VISIBLE);
            imgPost.setVisibility(View.VISIBLE);
            videoview.setVisibility(View.GONE);

            imgPost.setImageBitmap(myBitmap);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }
}
