package com.exceptionaire.ongraviti.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.EnrollEventResponse;
import com.exceptionaire.ongraviti.model.SpeedDatingRespose;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.Connectivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class SpeedDatingActivity extends BaseDrawerActivity implements View.OnClickListener, AppConstants {

    private Button enroll;
    private TextView check_internet_speed;
    private Connectivity connectivity;
    private String speed, eventId = null;
    private ProgressDialog progressDialog;
    private LinearLayout main_event_layout, no_event_layout;
    private TextView details_text_view, event_date_value, event_start_time_value, event_end_time_value, event_amount_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_speed_dating, frameLayout);
        arcMenu.setVisibility(View.GONE);

        initialiseComponents();
    }

    private void initialiseComponents() {
        connectivity = new Connectivity();
        enroll = (Button) findViewById(R.id.enroll);
        check_internet_speed = (TextView) findViewById(R.id.check_internet_speed);

        details_text_view = (TextView) findViewById(R.id.details_text_view);
        event_date_value = (TextView) findViewById(R.id.event_date_value);
        event_start_time_value = (TextView) findViewById(R.id.event_start_time_value);
        event_end_time_value = (TextView) findViewById(R.id.event_end_time_value);
        event_amount_value = (TextView) findViewById(R.id.event_amount_value);
        main_event_layout = (LinearLayout) findViewById(R.id.main_event_layout);
        no_event_layout = (LinearLayout) findViewById(R.id.no_event_layout);

        main_event_layout.setVisibility(View.GONE);
        no_event_layout.setVisibility(View.GONE);
        enroll.setOnClickListener(this);
        check_internet_speed.setOnClickListener(this);


        if (isNetworkAvailable()) {

            if (AppSettings.getLoginUserId() != null) {

                String user_id = AppSettings.getLoginUserId();

                getEvents(user_id);
            }
        } else {
            CommonDialogs.dialog_with_one_btn_without_title(SpeedDatingActivity.this, getResources().getString(R.string.check_internet));
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.enroll:


                if (isNetworkAvailable()) {

                    if (AppSettings.getLoginUserId() != null && eventId != null) {

                        enrollOnEvent(AppSettings.getLoginUserId(), eventId);
                    }

                } else {
                    CommonDialogs.dialog_with_one_btn_without_title(SpeedDatingActivity.this, getResources().getString(R.string.check_internet));
                }
                break;
            case R.id.check_internet_speed:
                checkInternetSpeed();
                break;
        }
    }

    private void checkInternetSpeed() {

        if (connectivity.isConnected(this)) {
            if (connectivity.isConnectedWifi(this)) {

                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                int speedMbps = wifiInfo.getLinkSpeed();

                Toast.makeText(this, "" + speedMbps + " mbps", Toast.LENGTH_SHORT).show();
//                speed = connectivity.isConnectedFast(this);
//                Toast.makeText(this, speed, Toast.LENGTH_SHORT).show();

            } else if (connectivity.isConnectedMobile(this)) {
                speed = connectivity.isConnectedFast(this);
                Toast.makeText(this, speed, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "No Network Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getEvents(String user_id) {

        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<SpeedDatingRespose> eventResponse = apiInterface.getEventList(user_id);
            eventResponse.enqueue(new Callback<SpeedDatingRespose>() {
                @Override
                public void onResponse(Call<SpeedDatingRespose> call, Response<SpeedDatingRespose> response) {

                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response.body() != null) {

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            main_event_layout.setVisibility(View.VISIBLE);
                            no_event_layout.setVisibility(View.GONE);

                            int i = 0;
                            eventId = response.body().getEvent_details().get(i).getEvent_id();
                            AppSettings.setKeyEventId(eventId);
                            details_text_view.setText(response.body().getEvent_details().get(i).getEvent_desc());
                            event_date_value.setText(response.body().getEvent_details().get(i).getEvent_date());
                            event_start_time_value.setText(response.body().getEvent_details().get(i).getEvent_st_time());
                            event_end_time_value.setText(response.body().getEvent_details().get(i).getEvent_end_time());
                            event_amount_value.setText("$" + response.body().getEvent_details().get(i).getAmount());
                        } else if (response.body().getStatus().equals(RESPONSE_ERROR)) {
                            no_event_layout.setVisibility(View.VISIBLE);
                            main_event_layout.setVisibility(View.GONE);
                            AppSettings.setKeyEventId(null);
                        }
                    }
                }

                @Override
                public void onFailure(Call<SpeedDatingRespose> call, Throwable t) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });

        } catch (Exception e) {

            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }


    }


    private void enrollOnEvent(String user_id, String event_id) {

        if (progressDialog != null) {
            progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        try {

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<EnrollEventResponse> enrollEventResponseCall = apiInterface.enrollEvent(user_id, event_id);
            enrollEventResponseCall.enqueue(new Callback<EnrollEventResponse>() {
                @Override
                public void onResponse(Call<EnrollEventResponse> call, Response<EnrollEventResponse> response) {

                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response.body() != null) {

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            AppSettings.setEnroll(true);
                            Toast.makeText(getApplicationContext(), "purchased Event", Toast.LENGTH_SHORT).show();
                            if (response.body().getUpload_video_status().equals("1")) {

                                startActivity(new Intent(getApplicationContext(), SelectUserActivity.class));
                                finish();
//                                startActivity(new Intent(getApplicationContext(), OpponentsActivityVideo.class));
                            } else if (response.body().getUpload_video_status().equals("0")) {
                                startActivity(new Intent(getApplicationContext(), AddVideoBioActivity.class));
                            }

                        } else if (response.body().getStatus().equals(RESPONSE_ERROR)) {
                            Toast.makeText(getApplicationContext(), "No yet purchased Event", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(SpeedDatingActivity.this, EnrollActivity.class));


                        }
                    }
                }

                @Override
                public void onFailure(Call<EnrollEventResponse> call, Throwable t) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();

        }
    }
}

