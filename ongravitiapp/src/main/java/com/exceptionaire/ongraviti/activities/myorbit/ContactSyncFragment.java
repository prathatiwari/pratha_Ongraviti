package com.exceptionaire.ongraviti.activities.myorbit;

/**
 * Created by Laxmikant on 15/11/16.
 */

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.adapters.SyncedContactAdapter;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.core.OnGravitiApp;
import com.exceptionaire.ongraviti.database.DaoSession;
import com.exceptionaire.ongraviti.database.User_data;
import com.exceptionaire.ongraviti.database.User_dataDao;
import com.exceptionaire.ongraviti.model.ResponseSuccess;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import org.greenrobot.greendao.query.Query;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;

public class ContactSyncFragment extends Fragment {
    // private ImageButton contact_sync_listviewfacebook, gmail, phone;//linkedin,
    private ListView contact_sync_listview;
    private TextView tv_msg;
    private SyncedContactAdapter syncedContactAdapter;
    private CustomProgressDialog progressDialog;
    private RadioGroup radioGroup1;
    private DaoSession daoSession;
    private User_dataDao user_dataDao;
    private ImageView buttonRefreshList;
    private int syncbntclick = 0;

    public ContactSyncFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new CustomProgressDialog(getActivity(), "Loading...");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.contact_sync, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {
        daoSession = ((OnGravitiApp) getActivity().getApplication()).getDaoSession();
        user_dataDao = daoSession.getUser_dataDao();

        tv_msg = (TextView) rootView.findViewById(R.id.tv_msg);
        contact_sync_listview = (ListView) rootView.findViewById(R.id.contact_sync_listview);
        syncedContactAdapter = new SyncedContactAdapter(new ArrayList<User_data>(), getActivity(), ContactSyncFragment.this);
        contact_sync_listview.setAdapter(syncedContactAdapter);

        buttonRefreshList = (ImageView) rootView.findViewById(R.id.buttonRefreshList);
        buttonRefreshList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (radioGroup1.getCheckedRadioButtonId() == R.id.radioPhone) {
                    syncbntclick = 1;
                    Intent intent = new Intent(getActivity(), ContactsFromPhoneActivity.class);
                    startActivityForResult(intent, 213);
                } else if (radioGroup1.getCheckedRadioButtonId() == R.id.radioGmail) {
                    Intent in = new Intent(getActivity(), ImportGmailContactsActivity.class);
                    startActivityForResult(in, 213);
                }
            }
        });

        radioGroup1 = (RadioGroup) rootView.findViewById(R.id.radioGroup1);
        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                   /* case R.id.radioFacebook:
                        CommonDialogs.showMessageDialog(getActivity(), "Message", getActivity().getString(R.string.msg_fb_cotacts));
                        syncedContactAdapter.addFriendsToList(new ArrayList<FriendContact>());
                        break;*/
                    case R.id.radioGmail:
                        syncedContactAdapter.clearList();
                        contact_sync_listview.setVisibility(View.INVISIBLE);
                        tv_msg.setVisibility(View.VISIBLE);
                        tv_msg.setText("Comming Soon...");
                        break;
                    case R.id.radioPhone:
                        Query<User_data> user_dataQuery = user_dataDao.queryBuilder().build();
                        ArrayList<User_data> user_dataArrayListDBFresh = (ArrayList<User_data>) user_dataQuery.list();

                        Log.e("FriendList........", "" + user_dataArrayListDBFresh);
                        for (int i = 0; i < user_dataArrayListDBFresh.size(); i++) {
                            Log.e("Friend Name", user_dataArrayListDBFresh.get(i).getName());
                            Log.e("phone No", user_dataArrayListDBFresh.get(i).getContact_number());
                        }
                        syncedContactAdapter.addFriendsToList(user_dataArrayListDBFresh);
                        contact_sync_listview.setVisibility(View.VISIBLE);
                        tv_msg.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }
            }
        });
        radioGroup1.check(R.id.radioPhone);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 213) {
            if (resultCode == RESULT_OK) {

                Boolean contactsUpdted = data.getBooleanExtra("friendList", false);

                Query<User_data> user_dataQuery = user_dataDao.queryBuilder().build();
                ArrayList<User_data> user_dataArrayListDBFresh = (ArrayList<User_data>) user_dataQuery.list();

                Log.e("FriendList", "" + user_dataArrayListDBFresh);
                for (int i = 0; i < user_dataArrayListDBFresh.size(); i++) {
                    Log.e("Friend_Name_Test ", user_dataArrayListDBFresh.get(i).getName() + "" + contactsUpdted);
                    Log.e("phone No", user_dataArrayListDBFresh.get(i).getContact_number());
                }
                syncedContactAdapter.addFriendsToList(user_dataArrayListDBFresh);

                contact_sync_listview.setVisibility(View.VISIBLE);
                syncedContactAdapter.notifyDataSetChanged();
                tv_msg.setVisibility(View.GONE);
            } else {
                contact_sync_listview.setVisibility(View.GONE);
                tv_msg.setVisibility(View.VISIBLE);
            }
        }
    }

    public void sendFriendRequest(final String receiver_user_id, final int position) {
        progressDialog.setMessage(getActivity().getString(R.string.msg_sending_friend_request));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = format.format(new Date());

        Retrofit retrofit = ApiClient.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<ResponseSuccess> loginCall = apiInterface.sendOrbitRequest(AppSettings.getLoginUserId(), receiver_user_id, currentDateandTime);
        loginCall.enqueue(new Callback<ResponseSuccess>() {
            @Override
            public void onResponse(Call<ResponseSuccess> call, retrofit2.Response<ResponseSuccess> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body().getStatus().equals(AppConstants.RESPONSE_SUCCESS)) {//success
                    final Dialog dialog = CommonDialogs.showMessageDialog(getActivity(), getResources().getString(R.string.ongravity), response.body().getMsg());
                    Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                    dialog_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            syncedContactAdapter.removeContact(position);
                        }
                    });

                } else if (response.body().getStatus().equalsIgnoreCase(AppConstants.RESPONSE_ERROR)) {
//                    CommonDialogs.showMessageDialog(getActivity(), getResources().getString(R.string.ongravity), response.body().getMsg());
                    Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void inviteFriends(User_data friendContact, int position) {
        String emailID = friendContact.getEmail_address();
        String contact_number = friendContact.getContact_number();

        if (contact_number != null && !contact_number.isEmpty() && !contact_number.equals("")) {
            sendMessage(friendContact, "Hello I would like to invite you to Join the revolutionized new social app Ongraviti and become one of my orbits. get it now from https://play.google.com/store/apps/developer?id=Ongraviti%20LLC&hl=en");
        } else if (emailID != null && !emailID.isEmpty() && !emailID.equals("")) {
            composeMail(emailID);
        }
    }


    private void sendMessage(final User_data user_data, String message) {

        String SENT = "SEND_REQUEST";
        String DELIVERED = "DELIVERY_REQUEST";

        PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(DELIVERED), 0);


// ---when the SMS has been sent---
        getActivity().registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context arg0, Intent arg1) {
                        int resultCode = getResultCode();
                        switch (resultCode) {
                            case Activity.RESULT_OK:

                                break;
                            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                                break;
                            case SmsManager.RESULT_ERROR_NO_SERVICE:
                                break;
                            case SmsManager.RESULT_ERROR_NULL_PDU:
                                break;
                            case SmsManager.RESULT_ERROR_RADIO_OFF:
                                break;
                        }
                    }
                }, new IntentFilter(SENT));
        // ---when the SMS has been delivered---
        getActivity().registerReceiver(
                new BroadcastReceiver() {

                    @Override
                    public void onReceive(Context arg0, Intent arg1) {
                        int resultCode = getResultCode();
                        switch (resultCode) {
                            case Activity.RESULT_OK:
                                user_data.setAlreadyInvited(true);
                                user_dataDao.insertOrReplace(user_data);
                                break;
                            case Activity.RESULT_CANCELED:
                                break;
                        }
                    }
                }, new IntentFilter(DELIVERED));

//        SmsManager sms = SmsManager.getDefault();
//        sms.sendTextMessage(user_data.getOriginal_Mobile_nos(), null, message, sentPI, deliveredPI);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("smsto:" + Uri.encode(user_data.getOriginal_Mobile_nos())));
            intent.putExtra("sms_body", message);
            startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, message);
            intent.setType("text/*");
            startActivity(intent);
        }

    }


    private void composeMail(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, "onGraviti");
        intent.putExtra(Intent.EXTRA_TEXT, "Hello i would like to invite you on onGraviti app. get it now from https://play.google.com/store/apps/developer?id=Ongraviti%20LLC&hl=en");
        final PackageManager pm = getActivity().getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (syncbntclick == 1) {
            Query<User_data> user_dataQuery = user_dataDao.queryBuilder().build();
            ArrayList<User_data> user_dataArrayListDBFresh = (ArrayList<User_data>) user_dataQuery.list();
            syncedContactAdapter.addFriendsToList(user_dataArrayListDBFresh);
            //syncedContactAdapter.notifyDataSetChanged();
            syncbntclick = 0;
//            Toast.makeText(getActivity(), "onResume()", Toast.LENGTH_SHORT).show();
        }

    }
}
