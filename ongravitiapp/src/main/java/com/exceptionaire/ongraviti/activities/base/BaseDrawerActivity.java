package com.exceptionaire.ongraviti.activities.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.AcceptDatingRequest;
import com.exceptionaire.ongraviti.activities.AskForBusiness;
import com.exceptionaire.ongraviti.activities.AskForDating;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.LoginActivity;
import com.exceptionaire.ongraviti.activities.MyFavoritesActivity;
import com.exceptionaire.ongraviti.activities.NotificationsActivity;
import com.exceptionaire.ongraviti.activities.PeopleNearByMeActivity;
import com.exceptionaire.ongraviti.activities.ProfileMainActivity;
import com.exceptionaire.ongraviti.activities.SearchActivity;
import com.exceptionaire.ongraviti.activities.SearchedUserInfoActivity;
import com.exceptionaire.ongraviti.activities.Setting;
import com.exceptionaire.ongraviti.activities.ViewProfileActivity;
import com.exceptionaire.ongraviti.activities.adapters.DrawerItemCustomAdapter;
import com.exceptionaire.ongraviti.activities.base.search.AutocompleteCustomArrayAdapter;
import com.exceptionaire.ongraviti.activities.base.search.CustomAutoCompleteTextChangedListener;
import com.exceptionaire.ongraviti.activities.base.search.CustomAutoCompleteView;
import com.exceptionaire.ongraviti.activities.base.search.DatabaseHandler;
import com.exceptionaire.ongraviti.activities.chats.ChatsMainActivity;
import com.exceptionaire.ongraviti.activities.myorbit.ContactsFromPhoneActivity;
import com.exceptionaire.ongraviti.activities.myorbit.ImportGmailContactsActivity;
import com.exceptionaire.ongraviti.activities.myorbit.MyOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.dialog.animation.BaseAnimatorSet;
import com.exceptionaire.ongraviti.dialog.animation.BounceEnter.BounceTopEnter;
import com.exceptionaire.ongraviti.dialog.animation.BounceEnter.SlideBottomExit;
import com.exceptionaire.ongraviti.model.DrawerModel;
import com.exceptionaire.ongraviti.model.PicData;
import com.exceptionaire.ongraviti.model.ProfilePictureResponse;
import com.exceptionaire.ongraviti.model.ResponseDeleteProfilePic;
import com.exceptionaire.ongraviti.model.ResponseOptionImageUpload;
import com.exceptionaire.ongraviti.model.ResponseSetDefaultProfileCategory;
import com.exceptionaire.ongraviti.model.ResponseSuccess;
import com.exceptionaire.ongraviti.model.ResponseSwitchProfileImage;
import com.exceptionaire.ongraviti.model.SearchData;
import com.exceptionaire.ongraviti.model.UserProfileCategory;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.AnimationUtils;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.activities.adapters.SearchAutocompleteAdapter.autocomplete;
import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;
import static com.exceptionaire.ongraviti.core.OnGravitiApp.context;

public class BaseDrawerActivity extends BaseActivity implements AppConstants, Response.Listener, Response.ErrorListener {
    private File post_image, post_video, post_video_thumb;
    private RequestBody requestPhotoFile, requestVideoFile, requestVideoThumb;
    private MultipartBody.Part postImageBody, postVideoBody, postVideoThumbBody;
    public String LOG_TAG = "OnGraviti";
    private String[] mNavigationDrawerItemTitles;
    public DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    public FrameLayout frameLayout;
    public Toolbar toolbar;
    public int pos;
    private File imageFile;
    private int ImageViewIDWhileCroppingImage = 0, clickId = 0;
    public ImageView imgSearch, imgDating, imgBusiness, imgNotification, close_search;
    private TextView toolbar_title_drawer;
    public SearchData[] itemDataArray;
    private RequestBody descriptionUserId, descriptionCatId;
    String TAG = "BaseDActivity";
    static Boolean flagDrawerOpened = false;
    private BitmapFactory.Options options;
    //volly request variables
    private int WESERVICE_FLAG;
    private ProgressDialog progressDialog;
    public static ArrayList<UserProfileCategory> list_profile_category;
    boolean doubleBackToExitPressedOnce = false;
    private static int HeaderIconSelection = 0; // 1=Dating, 2=Business , 3= Notification
    private ImageView imgSocial, imgDating_icon, imgBusiness_icon;
    TextView text_view_socials, text_view_datings, text_view_businesss;
    private int ImageViewIDClicked = 0;
    public BaseAnimatorSet mBasIn;
    public BaseAnimatorSet mBasOut;
    public static TextView txtDrawerName;
    ImageView imageview_viewprofile;
    public ArcMenu arcMenu;
    private boolean newProfilePicture = false;
    private String user_d = null, catagory_id = null;
    Boolean flagSocialPic = false;
    Boolean flagDatinglPic = false;
    Boolean flagBusinessPic = false;

    /*
     * Change to type CustomAutoCompleteView instead of AutoCompleteTextView
     * since we are extending to customize the view and disable filter
     * The same with the XML view, type will be CustomAutoCompleteView
     */
    public CustomAutoCompleteView myAutoComplete;

    // adapter for auto-complete
    public ArrayAdapter<SearchData> myAdapter;

    // for database operations
    public DatabaseHandler databaseH;
    private RelativeLayout relativeLayoutProfilePic;
    private RoundedImageView imageViewDefaultProfilePicture;
    private ImageView imageViewDefaultAdd, imageViewDelete;
    private int countForOptionalImages = 1;
    private ArrayList<RoundedImageView> optionPictures = new ArrayList<>();
    private int selectedIndex = 0;
    private Boolean isPrimary = false;
    private float angle = 0f;
    private float transitionX = 0f;
    private float transitionY = 0f;
    private int picWidth = 0;
    private int picHeight = 0;

    @SuppressLint({"ClickableViewAccessibility", "ResourceAsColor"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
//        mNavigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (ListView) findViewById(R.id.left_drawer);
            frameLayout = (FrameLayout) findViewById(R.id.content_frame);
            toolbar_title_drawer = (TextView) findViewById(R.id.toolbar_title_drawer);
            determineScreenDensity();
            initArcMenu();


            try {

                // instantiate database handler
                databaseH = new DatabaseHandler(this);
                // autocompletetextview is in activity_main.xml
                myAutoComplete = (CustomAutoCompleteView) findViewById(R.id.autoCompleteTextView);
                myAutoComplete.setFocusable(true);
                myAutoComplete.setDropDownWidth(3000);
//                Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
//                mCursorDrawableRes.set(myAutoComplete, R.drawable.ic_edit_white);
                myAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {

                        RelativeLayout rl = (RelativeLayout) arg1;
                        TextView tv = (TextView) rl.getChildAt(1);
                        myAutoComplete.setText(tv.getText().toString());
                        Log.d("adptr", "## relative_list_item click : " + itemDataArray[pos].getUser_id());

                        if (itemDataArray[pos].getUser_id().equalsIgnoreCase(AppSettings.getLoginUserId())) {
                            Intent i = new Intent(BaseDrawerActivity.this, ViewProfileActivity.class);
                            if (itemDataArray[pos].getProfile_cat_id().equalsIgnoreCase("1")) {
                                i.putExtra("social", "social");
                            } else if (itemDataArray[pos].getProfile_cat_id().equalsIgnoreCase("2")) {
                                i.putExtra("dating", "dating");
                            } else if (itemDataArray[pos].getProfile_cat_id().equalsIgnoreCase("3")) {
                                i.putExtra("business", "business");
                            }
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();
                        } else {
                            Intent intentProfile = new Intent(BaseDrawerActivity.this, SearchedUserInfoActivity.class);
                            if (itemDataArray[pos].getProfile_cat_id().equalsIgnoreCase("1")) {
                                intentProfile.putExtra("social", "social");
                            } else if (itemDataArray[pos].getProfile_cat_id().equalsIgnoreCase("2")) {
                                intentProfile.putExtra("dating", "dating");
                            } else if (itemDataArray[pos].getProfile_cat_id().equalsIgnoreCase("3")) {
                                intentProfile.putExtra("business", "business");
                            }
                            intentProfile.putExtra("ActivityName", "OrbitProfile");
                            intentProfile.putExtra("OrbitUserID", "" + itemDataArray[pos].getUser_id());
                            BaseDrawerActivity.this.startActivity(intentProfile);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                            finish();
                        }
                        /*if (!itemDataArray[pos].getUser_id().equalsIgnoreCase(AppSettings.getLoginUserId())) {

                            Intent intentProfile = new Intent(BaseDrawerActivity.this, FavoriteUserInfoActivity.class);
                            intentProfile.putExtra("ActivityName", "OrbitProfile");
                            intentProfile.putExtra("OrbitUserID", "" + itemDataArray[pos].getUser_id());
                            BaseDrawerActivity.this.startActivity(intentProfile);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();

                        } else {
                            Intent intentProfile = new Intent(BaseDrawerActivity.this, EditProfileActivity.class);
                            startActivity(intentProfile);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();

                        }*/
                    }
                });

                // add the listener so it will tries to suggest while the user types
                myAutoComplete.addTextChangedListener(new CustomAutoCompleteTextChangedListener(BaseDrawerActivity.this, 1));

                // itemDataArray has no value at first
                itemDataArray = new SearchData[0];

                // set the custom ArrayAdapter
                myAdapter = new AutocompleteCustomArrayAdapter(this, 1, R.layout.list_view_row_home, itemDataArray);
                myAutoComplete.setAdapter(myAdapter);

            } catch (Exception e) {
                e.printStackTrace();
            }

            imgSearch = (ImageView) findViewById(R.id.imgSearch);
            imgDating = (ImageView) findViewById(R.id.imgDating);
            imgBusiness = (ImageView) findViewById(R.id.imgBusiness);
            imgNotification = (ImageView) findViewById(R.id.imgNotification);
            close_search = (ImageView) findViewById(R.id.close);
            close_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toolbar_title_drawer.setVisibility(View.VISIBLE);
                    imgSearch.setVisibility(View.VISIBLE);
                    imgDating.setVisibility(View.VISIBLE);
                    imgBusiness.setVisibility(View.VISIBLE);
                    imgNotification.setVisibility(View.VISIBLE);
                    close_search.setVisibility(View.GONE);
                    myAutoComplete.setVisibility(View.GONE);
                    myAutoComplete.setText("");
                }
            });

            imgSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    toolbar_title_drawer.setVisibility(View.GONE);
                    imgSearch.setVisibility(View.GONE);
                    imgDating.setVisibility(View.GONE);
                    imgBusiness.setVisibility(View.GONE);
                    imgNotification.setVisibility(View.GONE);
                    close_search.setVisibility(View.VISIBLE);
                    myAutoComplete.setVisibility(View.VISIBLE);

                }
            });

            Log.d(TAG, "## onCreate HeaderIconSelection : " + HeaderIconSelection);
            if (HeaderIconSelection == 0) {
                imgBusiness.setImageResource(R.drawable.ic_business);
                imgNotification.setImageResource(R.drawable.ic_notification);
                imgDating.setImageResource(R.drawable.ic_dating);

            } else if (HeaderIconSelection == 1) {
                imgDating.setImageResource(R.drawable.ic_dating);
                imgBusiness.setImageResource(R.drawable.ic_business);
                imgNotification.setImageResource(R.drawable.ic_notification);

            } else if (HeaderIconSelection == 2) {
                imgBusiness.setImageResource(R.drawable.ic_business);
                imgNotification.setImageResource(R.drawable.ic_notification);
                imgDating.setImageResource(R.drawable.ic_dating);

            } else if (HeaderIconSelection == 3) {
                imgNotification.setImageResource(R.drawable.ic_notification);
                imgBusiness.setImageResource(R.drawable.ic_business);
                imgDating.setImageResource(R.drawable.ic_dating);
            }

            imgDating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    HeaderIconSelection = 1;

                    Intent intentDating = new Intent(BaseDrawerActivity.this, AskForDating.class);
                    startActivity(intentDating);
                    // finish();
                }
            });
            imgBusiness.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    HeaderIconSelection = 2;

                    Intent intentBusiness = new Intent(BaseDrawerActivity.this, AskForBusiness.class);
                    startActivity(intentBusiness);
                    // finish();
                }
            });
            imgNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HeaderIconSelection = 3;

                    startActivity(new Intent(BaseDrawerActivity.this, NotificationsActivity.class));
                    finish();
                }
            });

            LayoutInflater inflater = getLayoutInflater();
            View listHeaderView = inflater.inflate(R.layout.drawer_header, null, false);
            View listFotterView = inflater.inflate(R.layout.drawer_bottom, null, false);
            imageview_viewprofile = (ImageView) listHeaderView.findViewById(R.id.imageview_viewprofile);
            txtDrawerName = (TextView) listHeaderView.findViewById(R.id.drawer_header_username);
            imageViewDelete = (ImageView) listHeaderView.findViewById(R.id.image_view_delete); // delete profile picture
            imageViewDefaultAdd = (ImageView) listHeaderView.findViewById(R.id.image_view_default_add); // set profile picture
            imageViewDefaultProfilePicture = (RoundedImageView) listHeaderView.findViewById(R.id.drawer_header_profile_picture); // set profile picture
            TextView textViewName = listFotterView.findViewById(R.id.textViewName);
            textViewName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mBasIn = new BounceTopEnter();
                    mBasOut = new SlideBottomExit();

                    mDrawerLayout.closeDrawers();

                    Dialog dialog = new CommonDialogs().dialogAcceptReject(BaseDrawerActivity.this, getResources().getString(R.string.ongravity), "Are you sure you want to logout?");
                    ((Button) dialog.findViewById(R.id.button_ok)).setText("Ok");
                    ((Button) dialog.findViewById(R.id.button_cancel)).setText("Cancel");
                    dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            AppSettings.setLoginTypePatchForQuickEnter(null);
                            AppSettings.setLogin(false);
                            AppSettings.setProfiePicturePath(null);
                            AppSettings.setProfileCatPicPathFB(null);
                            AppSettings.setUserProfileName(null);
                            AppSettings.setProfileDetails(null);
                            AppSettings.setUserProfileEmail(null);
                            AppSettings.setProfileDefaultCategory(null);
                            AppSettings.setProfiePictureName(null);
                            AppSettings.setProfileCatPicPathOne(null);
                            AppSettings.setProfileCatPicPathTwo(null);
                            AppSettings.setProfileCatPicPathThree(null);
                            AppSettings.setOpenListView(false);
                            AppSettings.setInterestCategories(null);
                            AppSettings.setProfileCatCoverPathOne(null);
                            AppSettings.setProfileCatCoverPathTwo(null);
                            AppSettings.setProfileCatCoverPathThree(null);
                            AppSettings.setProfilePicForCatId(null, null);
                            AppSettings.setCoverPicForCatId(null, null);
                            Intent loginIntent = new Intent(BaseDrawerActivity.this, LoginActivity.class);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(loginIntent);
                            finish();
                        }
                    });
                    dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                }
            });
            relativeLayoutProfilePic = (RelativeLayout) listHeaderView.findViewById(R.id.drawer_header_profile_picture_parent); // set profile picture
            Log.d("Baseadpt", "## AppSettings.getProfilePictureName()" + AppSettings.getProfilePictureName());
            Log.d("Baseadpt", "## AppSettings.getProfileDefaultCategory() : " + String.valueOf(AppSettings.getProfileDefaultCategory()));

            mDrawerList.addHeaderView(listHeaderView);
            mDrawerList.addFooterView(listFotterView);

            imgSocial = (ImageView) listHeaderView.findViewById(R.id.imgSocial_);
            imgDating_icon = (ImageView) listHeaderView.findViewById(R.id.imgDating_);
            imgBusiness_icon = (ImageView) listHeaderView.findViewById(R.id.imgBusiness_);
            text_view_socials = (TextView) listHeaderView.findViewById(R.id.text_view_socials);
            text_view_businesss = (TextView) listHeaderView.findViewById(R.id.text_view_businesss);
            text_view_datings = (TextView) listHeaderView.findViewById(R.id.text_view_datings);
            //find which default profile is active now, and change its bg
            if (String.valueOf(AppSettings.getProfileDefaultCategory()).equals("1")) {
                Log.d("Baseadpt", "## if 1 ");
                imgSocial.setImageResource(R.drawable.ic_social_checked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
//                text_view_socials.setTextColor(R.color.selected);
//                text_view_datings.setTextColor(R.color.white);
//                text_view_businesss.setTextColor(R.color.white);

            } else if (String.valueOf(AppSettings.getProfileDefaultCategory()).equals("2")) {
                Log.d("Baseadpt", "## if 2 ");
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating_checked);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
//                text_view_socials.setTextColor(R.color.white);
//                text_view_datings.setTextColor(R.color.selected);
//                text_view_businesss.setTextColor(R.color.white);
            } else if (String.valueOf(AppSettings.getProfileDefaultCategory()).equals("3")) {
                Log.d("Baseadpt", "## if 3 ");
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business_checked);
//                text_view_socials.setTextColor(R.color.white);
//                text_view_datings.setTextColor(R.color.white);
//                text_view_businesss.setTextColor(R.color.selected);
            }
            Button image_view_editprofile = listHeaderView.findViewById(R.id.image_view_editprofile);
            image_view_editprofile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(BaseDrawerActivity.this, ProfileMainActivity.class);
                    startActivity(i);
                }
            });
           /* username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(BaseDrawerActivity.this, EditProfileActivity.class));
                }
            });
*/
//TODO
            DrawerModel[] drawerItem = new DrawerModel[8];
            drawerItem[0] = new DrawerModel(R.drawable.home_selector, getResources().getString(R.string.my_world), R.drawable.no_comming_soon);
            drawerItem[1] = new DrawerModel(R.drawable.peoplenearby_selector, getResources().getString(R.string.people_nearby), R.drawable.no_comming_soon);
            drawerItem[2] = new DrawerModel(R.drawable.myorbit_selector, getResources().getString(R.string.myorbit), R.drawable.no_comming_soon);
//            drawerItem[3] = new DrawerModel(R.drawable.favorite_selector, getResources().getString(R.string.txtfavorites), R.drawable.no_comming_soon);
            drawerItem[3] = new DrawerModel(R.drawable.speak_selector, getResources().getString(R.string.speak_out), R.drawable.comming_soon);
            drawerItem[4] = new DrawerModel(R.drawable.chat_selector, getResources().getString(R.string.chat), R.drawable.no_comming_soon);
            drawerItem[5] = new DrawerModel(R.drawable.speed_dating_selector, getResources().getString(R.string.speed_dating), R.drawable.comming_soon);
            drawerItem[6] = new DrawerModel(R.drawable.services_selector, getResources().getString(R.string.service), R.drawable.comming_soon);
            drawerItem[7] = new DrawerModel(R.drawable.ic_settings, getResources().getString(R.string.settings), R.drawable.no_comming_soon);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(true);
            DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter
                    (this, R.layout.list_view_item_row, drawerItem);
            mDrawerList.setAdapter(adapter);
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerLayout.setDrawerListener(mDrawerToggle);
//        mTitle = mNavigationDrawerItemTitles[12];
            System.out.println("Title :" + mTitle);
            setupDrawerToggle();

//            showProgressBar();
//            getUserProfileData();
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }

        checkProfileUploadedImages();

        imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String path = "";
                String category = "1";
                if (AppSettings.getProfileDefaultCategory() == null)
                    category = "1";
                else
                    category = AppSettings.getProfileDefaultCategory();

                switch (category) {
                    case PROFILE_CATEGORY_ONE_SOCIAL:
                        path = AppSettings.getProfileCatPicPathOne();
                        break;

                    case PROFILE_CATEGORY_TWO_DATING:
                        path = AppSettings.getProfileCatPicPathTWO();
                        break;

                    case PROFILE_CATEGORY_THREE_BUSINESS:
                        path = AppSettings.getProfileCatPicPathThree();
                        break;

                    default:
                        path = AppSettings.getProfileCatPicPathOne();
                        break;
                }
                List<PicData> picDataList = new Gson().fromJson(AppSettings.getOptionPicArray(), new TypeToken<List<PicData>>() {
                }.getType());
                if (picDataList != null) {
                    final Dialog dialogDeletePic = new CommonDialogs().displayDeleteProfilePicDialog(BaseDrawerActivity.this, picDataList, path);
                } else if (path != null) {
                    final Dialog dialogDeletePic = new CommonDialogs().displayDeleteProfilePicDialog(BaseDrawerActivity.this, null, path);

                } else {
                    Toast.makeText(BaseDrawerActivity.this, "No profile picture to delete.", Toast.LENGTH_LONG).show();
                }
            }
        });
        relativeLayoutProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mDrawerLayout.closeDrawers();
                String path = "";
                String category = "1";
                if (AppSettings.getProfileDefaultCategory() == null)
                    category = "1";
                else
                    category = AppSettings.getProfileDefaultCategory();

                switch (category) {
                    case PROFILE_CATEGORY_ONE_SOCIAL:
                        path = AppSettings.getProfileCatPicPathOne();
                        break;

                    case PROFILE_CATEGORY_TWO_DATING:
                        path = AppSettings.getProfileCatPicPathTWO();
                        break;

                    case PROFILE_CATEGORY_THREE_BUSINESS:
                        path = AppSettings.getProfileCatPicPathThree();
                        break;

                    default:
                        path = AppSettings.getProfileCatPicPathOne();
                        break;
                }
              /*  if (path == null || path.isEmpty() || path.equalsIgnoreCase("")) {
                    ImageViewIDClicked = R.id.drawer_header_profile_picture;
                    isPrimary = true;
                    checkForPermissionsAndShowOptionDialog();
                } else {
                    isPrimary = false;*/
                if (path != null) {
                    new CommonDialogs().displayImageZoomDialog(BaseDrawerActivity.this, path);
                } else {
                    Toast.makeText(BaseDrawerActivity.this, "Profile picture not available.", Toast.LENGTH_LONG).show();
                }
               /* }*/
            }
        });
        if (AppSettings.getProfileDefaultCategory() != null) {
            setDefaultProfileLocally();
        }
        imageview_viewprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BaseDrawerActivity.this, ViewProfileActivity.class);
                startActivity(i);
            }
        });

        txtDrawerName.setText("View Profile");
        if (AppSettings.getUserProfileName() != null) {
            if (!AppSettings.getUserProfileName().equals("")) {
                txtDrawerName.setText(AppSettings.getUserProfileName());
            } else {
                txtDrawerName.setText("View Profile");
            }
            txtDrawerName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(BaseDrawerActivity.this, ViewProfileActivity.class);
                    startActivity(i);
                }
            });
        } else {
            txtDrawerName.setText("View Profile");
            txtDrawerName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(BaseDrawerActivity.this, ViewProfileActivity.class);
                    startActivity(i);
                }
            });
        }

    }

    public static void updateDrawer() {
        if (AppSettings.getUserProfileName() != null) {
            if (!AppSettings.getUserProfileName().equals("")) {
                txtDrawerName.setText(AppSettings.getUserProfileName());
            } else {
                txtDrawerName.setText("View Profile");
            }
        } else {
            txtDrawerName.setText("View Profile");
        }
    }

    private void checkProfileUploadedImages() {
        String path = "";
        String category = "1";
        if (AppSettings.getProfileDefaultCategory() == null)
            category = "1";
        else
            category = AppSettings.getProfileDefaultCategory();

        switch (category) {
            case PROFILE_CATEGORY_ONE_SOCIAL:
                path = AppSettings.getProfileCatPicPathOne();
                countForOptionalImages = AppSettings.getCountOptionPicCategoryOneSocial();
                break;

            case PROFILE_CATEGORY_TWO_DATING:
                path = AppSettings.getProfileCatPicPathTWO();
                countForOptionalImages = AppSettings.getCountOptionPicCategoryTwoDating();
                break;

            case PROFILE_CATEGORY_THREE_BUSINESS:
                path = AppSettings.getProfileCatPicPathThree();
                countForOptionalImages = AppSettings.getCountOptionPicCategoryThreeBusiness();
                break;

//            default:
//                path = AppSettings.getProfileCatPicPathOne();
//                countForOptionalImages = AppSettings.getCountOptionPicCategoryOneSocial();
//                break;
        }

//        Log.d("Path123", path);
//        String CurrentString = path;
//        String[] separated = CurrentString.split("media/");
//        Log.d("Path123", separated[1]);
//        if (path != null && !path.isEmpty() && !path.equalsIgnoreCase("")) {
//            Picasso.with(this)
//                    .load(separated[1])
//                    .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
//                    .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
//                    .into(imageViewDefaultProfilePicture);


        if (path != null && !path.isEmpty() && !path.equalsIgnoreCase("")) {
            Log.d("myimage", path);
            if (!path.contains("https://graph")) {
                Picasso.with(this)
                        .load(path)
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(imageViewDefaultProfilePicture);
                imageViewDefaultAdd.setVisibility(View.GONE);
            } else if (path.contains("https://graph")) {
                Log.d("myimage1", path);
                String[] separated = path.split("https://graph");
                String pathFB = separated[1];
                Picasso.with(this)
                        .load("https://graph" + pathFB)
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(imageViewDefaultProfilePicture);
                imageViewDefaultAdd.setVisibility(View.GONE);
            }
            // TODO uncomment  placeRoundedPictures() for multiple pictures
            // TODO set visibility visible in image_view_default_add and image_view_delete in drawer_header layout
//            placeRoundedPictures();
        } else {
            // TODO imageViewDefaultAdd.setVisibility(View.VISIBLE) for more images
            imageViewDefaultAdd.setVisibility(View.GONE);
        }
    }

    private void placeRoundedPictures() {
        final FrameLayout main = (FrameLayout) findViewById(R.id.main_click);
        int numViews = 6;
        int index = 0;


        final int childCount = main.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = main.getChildAt(i);
            if (view instanceof RoundedImageView) {
                if (view.getId() != R.id.drawer_header_profile_picture) {
                    view.setVisibility(View.INVISIBLE);
                    main.removeView(view);
                }
            }
        }


        List<PicData> picDataList = new Gson().fromJson(AppSettings.getOptionPicArray(), new TypeToken<List<PicData>>() {
        }.getType());

//        for (int imageIndex = 0; imageIndex < numViews; imageIndex++) {
        if (picDataList != null && picDataList.size() != 0) {
            for (PicData picData : picDataList) {

                String fixCategory = "1";
                String currentCategory = AppSettings.getProfileDefaultCategory();
                if (currentCategory != null && !currentCategory.equalsIgnoreCase("") && !currentCategory.isEmpty()) {
                    switch (currentCategory) {
                        case PROFILE_CATEGORY_ONE_SOCIAL:
                            fixCategory = PROFILE_CATEGORY_ONE_SOCIAL;
                            break;
                        case PROFILE_CATEGORY_TWO_DATING:
                            fixCategory = PROFILE_CATEGORY_TWO_DATING;
                            break;
                        case PROFILE_CATEGORY_THREE_BUSINESS:
                            fixCategory = PROFILE_CATEGORY_THREE_BUSINESS;
                            break;
                        default:
                            fixCategory = PROFILE_CATEGORY_ONE_SOCIAL;
                            break;
                    }
                } else {
                    fixCategory = PROFILE_CATEGORY_ONE_SOCIAL;
                }

                if (picData.getCategory_id().equalsIgnoreCase(fixCategory)) {
                    // Create some quick TextViews that can be placed.
                    RoundedImageView roundedImageView = new RoundedImageView(this);
                    roundedImageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.add_plus));

                    // Force the views to a nice size (150x100 px) that fits my display.
                    // This should of course be done in a display size independent way.
                    FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(picWidth, picHeight);
                    // Place all views in the center of the layout. We'll transform them
                    // away from there in the code below.
                    lp.gravity = Gravity.CENTER;
                    // Set layout params on view.
                    roundedImageView.setLayoutParams(lp);

                    // Calculate the angle of the current view. Adjust by 90 degrees to
                    // get View 0 at the top. We need the angle in degrees and radians.
                    float angleDeg = index * 180.0f / numViews - angle;
                    float angleRad = (float) (angleDeg * Math.PI / 180.0f);
                    // Calculate the position of the view, offset from center (300 px from
                    // center). Again, this should be done in a display size independent way.
                    roundedImageView.setTranslationX(transitionX * (float) Math.cos(angleRad));
                    roundedImageView.setTranslationY(transitionY * (float) Math.sin(angleRad));
                    // Set the rotation of the view.
                    main.addView(roundedImageView);
                    optionPictures.add(roundedImageView);

                    Picasso.with(this)
                            .load(PROFILE_PICTURE_PATH + File.separator + picData.getSet_profile_picture())
                            .placeholder(R.drawable.add_plus).resize(100, 100) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.add_plus)     //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(roundedImageView);

                    selectedIndex = index;
                    if (index > countForOptionalImages)
                        roundedImageView.setVisibility(View.INVISIBLE);

                    roundedImageView.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            String userID = AppSettings.getLoginUserId();
                            String currentCategory = AppSettings.getProfileDefaultCategory();
                            if (currentCategory != null && !currentCategory.equalsIgnoreCase("") && !currentCategory.isEmpty()) {
                                switch (currentCategory) {
                                    case PROFILE_CATEGORY_ONE_SOCIAL:
                                        currentCategory = PROFILE_CATEGORY_ONE_SOCIAL;
                                        break;
                                    case PROFILE_CATEGORY_TWO_DATING:
                                        currentCategory = PROFILE_CATEGORY_TWO_DATING;
                                        break;
                                    case PROFILE_CATEGORY_THREE_BUSINESS:
                                        currentCategory = PROFILE_CATEGORY_THREE_BUSINESS;
                                        break;
                                    default:
                                        currentCategory = PROFILE_CATEGORY_ONE_SOCIAL;
                                        break;
                                }
                            } else {
                                currentCategory = PROFILE_CATEGORY_ONE_SOCIAL;
                            }
                            if (userID != null)
                                callSwitchAPI(userID, currentCategory, picData.getSet_profile_pic_id(), picData.getSet_profile_picture());
                        }
                    });
                    index++;
                }
            }
            if (index < 6) {
                RoundedImageView roundedImageView = new RoundedImageView(this);
                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(picWidth, picHeight);
                lp.gravity = Gravity.CENTER;
                roundedImageView.setLayoutParams(lp);
                float angleDeg = index * 180.0f / numViews - angle;
                float angleRad = (float) (angleDeg * Math.PI / 180.0f);
                roundedImageView.setTranslationX(transitionX * (float) Math.cos(angleRad));
                roundedImageView.setTranslationY(transitionY * (float) Math.sin(angleRad));
                main.addView(roundedImageView);
                optionPictures.add(roundedImageView);
                roundedImageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.add_plus));
                selectedIndex = index;
                if (index > countForOptionalImages)
                    roundedImageView.setVisibility(View.INVISIBLE);
                roundedImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkForPermissionsAndShowOptionDialog();
                    }
                });
            }
        } else {
            RoundedImageView roundedImageView = new RoundedImageView(this);
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(picWidth, picHeight);
            lp.gravity = Gravity.CENTER;
            roundedImageView.setLayoutParams(lp);
            float angleDeg = index * 180.0f / numViews - angle;
            float angleRad = (float) (angleDeg * Math.PI / 180.0f);
            roundedImageView.setTranslationX(transitionX * (float) Math.cos(angleRad));
            roundedImageView.setTranslationY(transitionY * (float) Math.sin(angleRad));
            main.addView(roundedImageView);
            optionPictures.add(roundedImageView);
            roundedImageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.add_plus));
            selectedIndex = index;
            if (index > countForOptionalImages)
                roundedImageView.setVisibility(View.INVISIBLE);

            roundedImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkForPermissionsAndShowOptionDialog();
                }
            });

            index++;
        }
    }


    OvershootInterpolator interpolator = new OvershootInterpolator();

    private void initArcMenu() {
        arcMenu = (ArcMenu) findViewById(R.id.arcMenu);
        arcMenu.setRadius(getResources().getDimension(R.dimen.radius));
        ImageView image_view_arcback = (ImageView) findViewById(R.id.image_view_arcback);
        arcMenu.setStateChangeListener(new StateChangeListener() {
            @Override
            public void onMenuOpened() {
                interpolator = new OvershootInterpolator();
                float rotationAnim = ViewCompat.getRotation(arcMenu.fabMenu);
                image_view_arcback.setVisibility(View.VISIBLE);
                ViewCompat.animate(arcMenu.fabMenu).
                        rotation(rotationAnim + 360f).
                        withLayer().
                        setDuration(300).
                        setInterpolator(interpolator).
                        start();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        arcMenu.fabMenu.setImageDrawable(ContextCompat.getDrawable(BaseDrawerActivity.this, R.drawable.ic_action_close));
                    }
                }, 300);
            }

            @Override
            public void onMenuClosed() {
                image_view_arcback.setVisibility(View.INVISIBLE);
                arcMenu.fabMenu.setImageDrawable(ContextCompat.getDrawable(BaseDrawerActivity.this, R.drawable.ic_fab_menu));
            }
        });

        findViewById(R.id.fab_ic_search).setOnClickListener(subMenuClickListener);
        findViewById(R.id.fab_ic_dating).setOnClickListener(subMenuClickListener);
        findViewById(R.id.fab_ic_business).setOnClickListener(subMenuClickListener);
        findViewById(R.id.fab_ic_datingrequest).setOnClickListener(subMenuClickListener);
    }

    private View.OnClickListener subMenuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            arcMenu.toggleMenu();
            switch (v.getId()) {
                case R.id.fab_ic_search:

                    HeaderIconSelection = 0;
                    Intent intentSearch = new Intent(BaseDrawerActivity.this, SearchActivity.class);
                    startActivity(intentSearch);
                    // finish();
                    break;

                case R.id.fab_ic_dating:
                    HeaderIconSelection = 1;
                    Intent intentDating = new Intent(BaseDrawerActivity.this, AskForDating.class);
                    startActivity(intentDating);
                    // finish();
                    break;
                case R.id.fab_ic_business:
                    HeaderIconSelection = 2;
                    Intent intentBusiness = new Intent(BaseDrawerActivity.this, AskForBusiness.class);
                    startActivity(intentBusiness);
                    // finish();
                    break;
                case R.id.fab_ic_datingrequest:
                    HeaderIconSelection = 3;
                    Intent intentDatingRequest = new Intent(BaseDrawerActivity.this, AcceptDatingRequest.class);
                    startActivity(intentDatingRequest);
                    // finish();
                    break;
            }
        }
    };


    @SuppressLint("ResourceAsColor")
    public void setDefaultProfileLocally() {
        try {

            if (AppSettings.getProfileDefaultCategory().equals(AppConstants.PROFILE_CATEGORY_ONE_SOCIAL)) {
                if (AppSettings.getProfileCatPicPathOne() != null) {
                    AppSettings.setProfiePicturePath(AppSettings.getProfileCatPicPathOne());
                    Log.d(TAG, "## AppSettings.getProfileCatPicPathOne() :" + AppSettings.getProfileCatPicPathOne());
                    if (!AppSettings.getProfileCatPicPathOne().contains("https://graph")) {
                        Picasso.with(this)
                                .load(AppSettings.getProfileCatPicPathOne())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageViewDefaultProfilePicture);
                        imageViewDefaultAdd.setVisibility(View.GONE);
                    } else if (AppSettings.getProfileCatPicPathOne().contains("https://graph")) {
                        String[] separated = AppSettings.getProfileCatPicPathOne().split("https://graph");
                        String pathFB = separated[1];
                        Picasso.with(this)
                                .load("https://graph" + pathFB)
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(imageViewDefaultProfilePicture);
                        imageViewDefaultAdd.setVisibility(View.GONE);
                    }
                } else {
                    imageViewDefaultProfilePicture.setImageResource(R.drawable.profile);
                }
                //set selected profile to drawer
                imgSocial.setImageResource(R.drawable.ic_social_checked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
//                text_view_socials.setTextColor(R.color.selected);
//                text_view_datings.setTextColor(R.color.white);
//                text_view_businesss.setTextColor(R.color.white);
            } else if (AppSettings.getProfileDefaultCategory().equals(AppConstants.PROFILE_CATEGORY_TWO_DATING)) {
                if (AppSettings.getProfileCatPicPathTWO() != null) {
                    AppSettings.setProfiePicturePath(AppSettings.getProfileCatPicPathTWO());
                    Log.d(TAG, "## AppSettings.getProfileCatPicPathTWO() :" + AppSettings.getProfileCatPicPathTWO());
//                roundedImageViewProfilePic.setImageUrl(AppSettings.getProfilePicturePath(), imageLoader);

                    Picasso.with(this)
                            .load(AppSettings.getProfileCatPicPathTWO())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imageViewDefaultProfilePicture);
                } else {
                    imageViewDefaultProfilePicture.setImageResource(R.drawable.profile);
                }
                //set selected profile to drawer
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating_checked);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
//                text_view_socials.setTextColor(R.color.white);
//                text_view_datings.setTextColor(R.color.selected);
//                text_view_businesss.setTextColor(R.color.white);
            } else if (AppSettings.getProfileDefaultCategory().equals(AppConstants.PROFILE_CATEGORY_THREE_BUSINESS)) {
                if (AppSettings.getProfileCatPicPathThree() != null) {
                    AppSettings.setProfiePicturePath(AppSettings.getProfileCatPicPathThree());
                    Log.d(TAG, "## AppSettings.getProfileCatPicPathThree() :" + AppSettings.getProfileCatPicPathThree());
//                roundedImageViewProfilePic.setImageUrl(AppSettings.getProfilePicturePath(), imageLoader);

                    Picasso.with(this)
                            .load(AppSettings.getProfileCatPicPathThree())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(imageViewDefaultProfilePicture);
                } else {
                    imageViewDefaultProfilePicture.setImageResource(R.drawable.profile);
                }
                //set selected profile to drawer
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business_checked);
//                text_view_socials.setTextColor(R.color.white);
//                text_view_datings.setTextColor(R.color.white);
//                text_view_businesss.setTextColor(R.color.selected);
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    public void onClickSocialCategory(View view) {
        try {
            AppSettings.setProfileDefaultCategory(AppConstants.PROFILE_CATEGORY_ONE_SOCIAL);
            CallSetDefaultProfileApi();
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void onClickDatingCategory(View view) {
        try {
            AppSettings.setProfileDefaultCategory(AppConstants.PROFILE_CATEGORY_TWO_DATING);
            CallSetDefaultProfileApi();
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void onClickBusinessCategory(View view) {
        try {
            AppSettings.setProfileDefaultCategory(AppConstants.PROFILE_CATEGORY_THREE_BUSINESS);
            CallSetDefaultProfileApi();
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }


    public void CallSetDefaultProfileApi() {
        showProgressBar();
        Retrofit retrofit = ApiClient.getClient();
        final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<ResponseSetDefaultProfileCategory> accessTokenCall = requestInterface.setUserDefaultProfile(AppSettings.getLoginUserId(), String.valueOf(AppSettings.getProfileDefaultCategory()));
        accessTokenCall.enqueue(new Callback<ResponseSetDefaultProfileCategory>() {
            @Override
            public void onResponse(Call<ResponseSetDefaultProfileCategory> call, retrofit2.Response<ResponseSetDefaultProfileCategory> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null && response.body().getStatus().equals(RESPONSE_SUCCESS)) {
                    Log.d(TAG, "## response : " + response.body().toString() + " ");
                    if (response.body().getProfile_cat_id() != null && !response.body().getProfile_cat_id().isEmpty() && !response.body().getProfile_cat_id().equals("")) {

                        AppSettings.setProfileDefaultCategory(response.body().getProfile_cat_id());
                        if (response.body().getProfile_cat_id().equals(AppConstants.PROFILE_CATEGORY_ONE_SOCIAL)) {

                            AppSettings.setProfileCatPicPathOne(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getProfile_picture());
                            Toast.makeText(BaseDrawerActivity.this, "Social profile activated", Toast.LENGTH_SHORT).show();

                        } else if (response.body().getProfile_cat_id().equals(AppConstants.PROFILE_CATEGORY_TWO_DATING)) {

                            AppSettings.setProfileCatPicPathTwo(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getProfile_picture());
                            Toast.makeText(BaseDrawerActivity.this, "Dating profile activated", Toast.LENGTH_SHORT).show();

                        } else if (response.body().getProfile_cat_id().equals(AppConstants.PROFILE_CATEGORY_THREE_BUSINESS)) {

                            AppSettings.setProfileCatPicPathThree(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getProfile_picture());
                            Toast.makeText(BaseDrawerActivity.this, "Business profile activated", Toast.LENGTH_SHORT).show();

                        }

                        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

                        if (cn.getClassName().equals(ProfileMainActivity.class.getName())) {
                            ProfileMainActivity.setSelectedProfileOfUser();
                        }
                    }
                    setDefaultProfileLocally();
                    //  Toast.makeText(BaseDrawerActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    //  CommonDialogs.dialog_with_one_btn_without_title(BaseDrawerActivity.this, response.body().getMsg());
                } else {
                    Toast.makeText(BaseDrawerActivity.this, "Profile activation failed ", Toast.LENGTH_SHORT).show();

                    // CommonDialogs.dialog_with_one_btn_without_title(BaseDrawerActivity.this, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<ResponseSetDefaultProfileCategory> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(getActivity(), "Something went wrong, please try after some time.", Toast.LENGTH_SHORT).show();
                Log.e("Error: ", t.toString());
            }
        });
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }


    public static boolean isDrawerOpen() {
        return flagDrawerOpened;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mDrawerLayout.closeDrawers();

            /*if (AppSettings.getProfilePicturePath() != "") {
                roundedImageViewProfilePic.setImageUrl(AppSettings.getProfilePicturePath(), imageLoader);
            }*/
        } catch (Exception e) {
            Log.e(TAG, "## onClick error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d(TAG, "## onErrorResponse : " + error.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            error.printStackTrace();
            String errorMessage = getString(R.string.error_connection_generic);
            if (error instanceof NoConnectionError)
                errorMessage = getString(R.string.error_no_connection);
            else if (error instanceof AuthFailureError)
                errorMessage = getString(R.string.error_authentication);
            else if (error instanceof NetworkError)
                errorMessage = getString(R.string.error_network_error);
            else if (error instanceof ParseError)
                errorMessage = getString(R.string.error_parse_error);
            else if (error instanceof ServerError)
                errorMessage = getString(R.string.error_server_error);
            else if (error instanceof TimeoutError)
                errorMessage = getString(R.string.error_connection_timeout);
            Toast.makeText(BaseDrawerActivity.this, errorMessage, Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response) {
        Log.d(TAG, "## onResponse : " + response.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            JSONObject jObject;
            if (WESERVICE_FLAG == 2) {  // get profile data
                try {
//                    Log.d(TAG, "## response : " + response + " ");
                    jObject = new JSONObject(String.valueOf(response));

                    String status = jObject.getString("status");
                    String message = jObject.getString("msg");
                    if (status.equalsIgnoreCase(AppConstants.RESPONSE_SUCCESS)) {

                        new CommonDialogs().showOKFinishDialog(BaseDrawerActivity.this, getResources().getString(R.string.ongravity), message).show();

                        Log.d(TAG, "## AppSettings.getProfileDefaultCategory() : " + String.valueOf(AppSettings.getProfileDefaultCategory()));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "## e : " + e.getMessage());
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
//            mTitle=mNavigationDrawerItemTitles[position];
        }

    }

    private void selectItem(int position) {
        try {
            Log.d(TAG, "## position : " + position);
            Fragment fragment = null;
            pos = position;
            switch (position) {
                case 1:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseDrawerActivity.this, HomeUserPostsActivity.class));
                    overridePendingTransition(R.anim.slide_in_right,
                            R.anim.slide_out_left);
                    finish();
                    break;
                case 2:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseDrawerActivity.this, PeopleNearByMeActivity.class));
                    overridePendingTransition(R.anim.slide_in_right,
                            R.anim.slide_out_left);
                    finish();
                    break;
           /* case 2:
                startActivity(new Intent(BaseDrawerActivity.this, FavouriteActivity.class));
                overridePendingTransition(R.anim.slide_in_right,
                        R.anim.slide_out_left);
                finish();
                break;*/
                case 3:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseDrawerActivity.this, MyOrbitActivity.class));
                    overridePendingTransition(R.anim.slide_in_right,
                            R.anim.slide_out_left);
                    finish();
                    break;
//                case 4:
//                    HeaderIconSelection = 0;
//                    startActivity(new Intent(BaseDrawerActivity.this, MyFavoritesActivity.class));
//                    overridePendingTransition(R.anim.slide_in_right,
//                            R.anim.slide_out_left);
//                    finish();
//                    break;
                case 4:
                    HeaderIconSelection = 0;
//                    startActivity(new Intent(BaseDrawerActivity.this, SpeakOutYourMindActivity.class));
//                    overridePendingTransition(R.anim.slide_in_right,
//                            R.anim.slide_out_left);
//                    finish();
                    break;
                case 5:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseDrawerActivity.this, ChatsMainActivity.class));
                    finish();
                    break;

                case 7:
                    HeaderIconSelection = 0;
//                    startActivity(new Intent(BaseDrawerActivity.this, ServicesActivity.class));
//                    finish();
//                    break;
                    break;
                case 6:
                    HeaderIconSelection = 0;
//                    startActivity(new Intent(BaseDrawerActivity.this, OpponentsActivityVideo.class));
//                    startActivity(new Intent(BaseDrawerActivity.this, SpeedDatingActivity.class));
//                    finish();
                    break;
                case 8:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseDrawerActivity.this, Setting.class));
                    overridePendingTransition(R.anim.slide_in_right,
                            R.anim.slide_out_left);
                    finish();
                    break;
                case 11:
                    HeaderIconSelection = 0;
                    final Dialog dialogs1 = CommonDialogs.showContactDialog(BaseDrawerActivity.this, "Select Contact Option");
                    TextView gmailContacts = (TextView) dialogs1.findViewById(R.id.dialogContactOptionGmailContact);
                    gmailContacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent in = new Intent(BaseDrawerActivity.this, ImportGmailContactsActivity.class);
                            startActivity(in);
                        }
                    });
                    TextView facebookContacts = (TextView) dialogs1.findViewById(R.id.dialogContactOptionFbContact);
                    facebookContacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Toast.makeText(BaseDrawerActivity.this, "No Permission Access Denied", Toast.LENGTH_LONG).show();

                        }
                    });
                    TextView linkedInContacts = (TextView) dialogs1.findViewById(R.id.dialogContactOptionLinkedInContact);
                    linkedInContacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(BaseDrawerActivity.this, "No Permission Access Denied", Toast.LENGTH_LONG).show();
                        }
                    });
                    TextView PhoneContacts = (TextView) dialogs1.findViewById(R.id.dialogContactOptionContactFromPhone);
                    PhoneContacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(BaseDrawerActivity.this, ContactsFromPhoneActivity.class));
                            dialogs1.dismiss();
                        }
                    });

                    break;
                case 10:
                    Log.d(TAG, "## case 10");
                    HeaderIconSelection = 0;
                    mBasIn = new BounceTopEnter();
                    mBasOut = new SlideBottomExit();

                    mDrawerLayout.closeDrawers();

                    Dialog dialog = new CommonDialogs().dialogAcceptReject(BaseDrawerActivity.this, getResources().getString(R.string.ongravity), "Are you sure you want to logout?");
                    ((Button) dialog.findViewById(R.id.button_ok)).setText("Ok");
                    ((Button) dialog.findViewById(R.id.button_cancel)).setText("Cancel");
                    dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            AppSettings.setLogin(false);
                            AppSettings.setLoginTypePatchForQuickEnter(null);
                            AppSettings.setProfiePicturePath(null);
                            AppSettings.setUserProfileName(null);
                            AppSettings.setProfileDetails(null);
                            AppSettings.setUserProfileEmail(null);
                            AppSettings.setProfileCatPicPathFB(null);
                            AppSettings.setProfileDefaultCategory(null);
                            AppSettings.setProfiePictureName(null);
                            AppSettings.setProfileCatPicPathOne(null);
                            AppSettings.setProfileCatPicPathTwo(null);
                            AppSettings.setInterestCategories(null);
                            AppSettings.setProfileCatPicPathThree(null);
                            AppSettings.setProfileCatCoverPathOne(null);
                            AppSettings.setProfileCatCoverPathTwo(null);
                            AppSettings.setProfileCatCoverPathThree(null);
                            AppSettings.setOpenListView(false);
                            AppSettings.setProfilePicForCatId(null, null);
                            AppSettings.setCoverPicForCatId(null, null);
//                            EditProfileActivity.list_profile_category = null;
                            Intent loginIntent = new Intent(BaseDrawerActivity.this, LoginActivity.class);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(loginIntent);
                            finish();
                        }
                    });
                    dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    break;
                case 14:
                    HeaderIconSelection = 0;
                    Log.d(TAG, "## case 14");
                    break;

                default:
                    break;
            }

            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                mDrawerList.setItemChecked(position, true);
                mDrawerList.setSelection(position);
//            setTitle(mNavigationDrawerItemTitles[position]);
                System.out.println("Title :" + mNavigationDrawerItemTitles[position]);
                mDrawerLayout.closeDrawer(mDrawerList);

            } else {
                Log.e("MainActivity", "Error in creating fragment");
            }
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
//        getSupportActionBar().setTitle(mTitle);
    }

    void setupDrawerToggle() {
        try {
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_open) {

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    flagDrawerOpened = true;
                    Log.d(TAG, "## onDrawerOpened");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                    if (imm.isAcceptingText()) {
                        Log.d(TAG, "## if imm.isAcceptingText()");
                        hideSoftKeyboard(BaseDrawerActivity.this);
                    }
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    flagDrawerOpened = false;
                }
            };

        /*getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);*/

            mDrawerToggle.setDrawerIndicatorEnabled(false);
            toolbar.setNavigationIcon(R.drawable.menu);
//        toolbar.setLogo(R.mipmap.my_world);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            });
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
//        super.onBackPressed();
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawers();
            } else {
//            finish();
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Press once again to exit the app", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }


    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    @SuppressLint("NewApi")
    private void checkForPermissionsAndShowOptionDialog() {

        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if (!hasPermissions(BaseDrawerActivity.this, PERMISSIONS)) {
            requestPermissions(PERMISSIONS, PERMISSION_REQUEST_CODE_CAMERA_STORAGE);
        } else {
            selectImage(ImageViewIDClicked);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void selectImage(final int imageViewID) {
        try {
            final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};

            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(BaseDrawerActivity.this);
            builder.setTitle("Add Photo!");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals("View Picture")) {

                    } else if (items[item].equals("Capture Photo")) {
                        getCameraInBaseDrawer();
                        dialog.dismiss();

                    } else if (items[item].equals("Choose from Gallery")) {
                        getGalleryInBaseDrawer();
                        dialog.dismiss();
                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }

                }
            });
            builder.show();
        } catch (Exception ee) {
            ee.printStackTrace();
        }


    }

    public void getCameraInBaseDrawer() {
        if (!AppUtils.isDeviceSupportCamera(BaseDrawerActivity.this)) {
            Toast.makeText(BaseDrawerActivity.this,
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
        } else {
            File dumpFolder = new File(PROFILE_PICTURES);
            if (!dumpFolder.exists()) {
                dumpFolder.mkdirs();
            }

            String imageName = "";

            String defaultCategory = AppSettings.getProfileDefaultCategory();
            if (defaultCategory == null)
                defaultCategory = "0";

            if (isPrimary) {
                switch (defaultCategory) {
                    case PROFILE_CATEGORY_ONE_SOCIAL:
                        imageName = BaseDrawerActivity.this.getString(R.string.profile_avatar_social);
                        break;
                    case PROFILE_CATEGORY_TWO_DATING:
                        imageName = BaseDrawerActivity.this.getString(R.string.profile_avatar_dating);
                        break;
                    case PROFILE_CATEGORY_THREE_BUSINESS:
                        imageName = BaseDrawerActivity.this.getString(R.string.profile_avatar_business);
                        break;
                    default:
                        imageName = BaseDrawerActivity.this.getString(R.string.profile_avatar_social);
                        break;
                }
            } else {
                imageName = BaseDrawerActivity.this.getString(R.string.option_pic);
            }

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(PROFILE_PICTURES + File.separator + imageName + ".jpg")));
            startActivityForResult(intent, CAMERA_REQUEST_AVATAR_BASE);
        }
    }


    public void getGalleryInBaseDrawer() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY_REQUEST_AVATAR_BASE);
    }
   /* @Override
    protected void onPause() {
    }

    @Override
    protected void onStop() {
    }*/

    @SuppressLint("LongLogTag")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        int IMAGE_SIZE = 0;

        File dir = new File(PROFILE_PICTURES);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (requestCode) {
            case CAMERA_REQUEST_AVATAR_BASE:

                if (resultCode == RESULT_OK) {
                    String defaultCategory = AppSettings.getProfileDefaultCategory();
                    if (defaultCategory == null)
                        defaultCategory = "0";

                    if (isPrimary) {
                        switch (defaultCategory) {
                            case PROFILE_CATEGORY_ONE_SOCIAL:
                                imageFile = new File(PROFILE_PICTURES + File.separator + BaseDrawerActivity.this.getString(R.string.profile_avatar_social) + ".jpg");
                                Uri imageUri = Uri.fromFile(imageFile);
                                CropImage.activity(imageUri)
                                        .start(BaseDrawerActivity.this);
//                                changeOrientation(R.id.drawer_header_profile_picture, BaseDrawerActivity.this.getString(R.string.profile_avatar_social));
                                break;
                            case PROFILE_CATEGORY_TWO_DATING:
                                imageFile = new File(PROFILE_PICTURES + File.separator + BaseDrawerActivity.this.getString(R.string.profile_avatar_dating) + ".jpg");
                                Uri imageUri2 = Uri.fromFile(imageFile);
                                CropImage.activity(imageUri2)
                                        .start(BaseDrawerActivity.this);
//                                changeOrientation(R.id.drawer_header_profile_picture, BaseDrawerActivity.this.getString(R.string.profile_avatar_dating));
                                break;
                            case PROFILE_CATEGORY_THREE_BUSINESS:
                                imageFile = new File(PROFILE_PICTURES + File.separator + BaseDrawerActivity.this.getString(R.string.profile_avatar_business) + ".jpg");
                                Uri imageUri3 = Uri.fromFile(imageFile);
                                CropImage.activity(imageUri3)
                                        .start(BaseDrawerActivity.this);
//                                changeOrientation(R.id.drawer_header_profile_picture, BaseDrawerActivity.this.getString(R.string.profile_avatar_business));
                                break;
                            default:
                                imageFile = new File(PROFILE_PICTURES + File.separator + BaseDrawerActivity.this.getString(R.string.profile_avatar_social) + ".jpg");
                                Uri imageUri4 = Uri.fromFile(imageFile);
                                CropImage.activity(imageUri4)
                                        .start(BaseDrawerActivity.this);
//                                changeOrientation(R.id.drawer_header_profile_picture, BaseDrawerActivity.this.getString(R.string.profile_avatar_social));
                                break;
                        }
                    } else {
                        imageFile = new File(PROFILE_PICTURES + File.separator + BaseDrawerActivity.this.getString(R.string.option_pic) + ".jpg");
                        Uri imageUri5 = Uri.fromFile(imageFile);
                        CropImage.activity(imageUri5)
                                .start(BaseDrawerActivity.this);
//                        changeOrientation(optionPictures.get(optionPictures.size() - 1).getId(), BaseDrawerActivity.this.getString(R.string.option_pic));
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(BaseDrawerActivity.this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(BaseDrawerActivity.this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                }
                break;

            case GALLERY_REQUEST_AVATAR_BASE:
                if (resultCode == RESULT_OK) {

                    String defaultCategory = AppSettings.getProfileDefaultCategory();
                    if (defaultCategory == null)
                        defaultCategory = "0";
                    if (isPrimary) {
                        switch (defaultCategory) {
                            case PROFILE_CATEGORY_ONE_SOCIAL:
                                saveImageAndShowImageView(R.id.imageView_profile_social, BaseDrawerActivity.this.getString(R.string.profile_avatar_social), intent);
                                break;
                            case PROFILE_CATEGORY_TWO_DATING:
                                saveImageAndShowImageView(R.id.imageView_profile_dating, BaseDrawerActivity.this.getString(R.string.profile_avatar_dating), intent);
                                break;
                            case PROFILE_CATEGORY_THREE_BUSINESS:
                                saveImageAndShowImageView(R.id.imageView_profile_business, BaseDrawerActivity.this.getString(R.string.profile_avatar_business), intent);
                                break;
                            default:
                                saveImageAndShowImageView(R.id.imageView_profile_social, BaseDrawerActivity.this.getString(R.string.profile_avatar_social), intent);
                                break;
                        }
                    } else {
                        saveImageAndShowImageView(optionPictures.get(optionPictures.size() - 1).getId(), BaseDrawerActivity.this.getString(R.string.option_pic), intent);
                    }
                } else {
                    Toast.makeText(BaseDrawerActivity.this, "You haven't picked an Image", Toast.LENGTH_LONG).show();
                }
                break;

            case 10:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;

                    final Bitmap bitmap = BitmapFactory.decodeFile(BUSINESS_PROFILE_PICTURE,
                            options);
                    if (null != bitmap) {

                        String defaultCategory = AppSettings.getProfileDefaultCategory();
                        if (defaultCategory == null)
                            defaultCategory = "0";

                        if (isPrimary) {
                            switch (defaultCategory) {
                                case PROFILE_CATEGORY_ONE_SOCIAL:
                                    if (imageViewDefaultProfilePicture != null) {
                                        IMAGE_SIZE = getScreenWidth(BaseDrawerActivity.this) / 2;
                                        if (IMAGE_SIZE <= 0)
                                            IMAGE_SIZE = 400;
                                        Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                        if (image != null) {
                                            (imageViewDefaultProfilePicture).setImageBitmap(image);
                                        } else {
                                            Toast.makeText(BaseDrawerActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    break;
                                case PROFILE_CATEGORY_TWO_DATING:
                                    if (imageViewDefaultProfilePicture != null) {
                                        IMAGE_SIZE = getScreenWidth(BaseDrawerActivity.this) / 2;
                                        if (IMAGE_SIZE <= 0)
                                            IMAGE_SIZE = 400;
                                        Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                        if (image != null) {
                                            imageViewDefaultProfilePicture.setImageBitmap(image);
                                        } else {
                                            Toast.makeText(BaseDrawerActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    break;
                                case PROFILE_CATEGORY_THREE_BUSINESS:
                                    if (imageViewDefaultProfilePicture != null) {
                                        IMAGE_SIZE = getScreenWidth(BaseDrawerActivity.this) / 2;
                                        if (IMAGE_SIZE <= 0)
                                            IMAGE_SIZE = 400;
                                        Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                        if (image != null) {
                                            imageViewDefaultProfilePicture.setImageBitmap(image);
                                        } else {
                                            Toast.makeText(BaseDrawerActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    break;
                                default:
                                    Toast.makeText(BaseDrawerActivity.this, "Image View is null !!", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                            isPrimary = false;
                        } else {
                            if (optionPictures.get(optionPictures.size() - 1) != null) {
                                IMAGE_SIZE = getScreenWidth(BaseDrawerActivity.this) / 2;
                                if (IMAGE_SIZE <= 0)
                                    IMAGE_SIZE = 400;
                                Bitmap image = AnimationUtils.getScaledBitmap(bitmap, IMAGE_SIZE, IMAGE_SIZE);
                                if (image != null) {
                                    optionPictures.get(optionPictures.size() - 1).setImageBitmap(image);
                                } else {
                                    Toast.makeText(BaseDrawerActivity.this, "Invalid image , please pick another image.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }
                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:

                CropImage.ActivityResult result = CropImage.getActivityResult(intent);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    setImage(resultUri);
                }
        }
    }

    private void changeOrientation(int imageViewID, String imageName) {
        int IMAGE_SIZE = 0;
        int rotate = 0;
        imageFile = new File(PROFILE_PICTURES + File.separator + imageName + ".jpg");

        if (imageName != null) {
            Uri imageUri = Uri.fromFile(imageFile);

            try {
                BaseDrawerActivity.this.getContentResolver().notifyChange(imageUri, null);

                ExifInterface exif = new ExifInterface(
                        imageFile.getPath());

                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;
                }
                Log.v(LOG_TAG, "Exif orientation: " + orientation);


                /****** Image rotation ****/
                Matrix matrix = new Matrix();
                matrix.postRotate(rotate);

                options = new BitmapFactory.Options();
                options.inSampleSize = 5;


                switch (imageViewID) {
                    case R.id.imageView_profile_social:
                        ImageViewIDWhileCroppingImage = R.id.imageView_profile_social;
                        break;
                    case R.id.imageView_profile_dating:
                        ImageViewIDWhileCroppingImage = R.id.imageView_profile_dating;
                        break;
                    case R.id.imageView_profile_business:
                        ImageViewIDWhileCroppingImage = R.id.imageView_profile_business;
                        break;
//                    case R.id.imageview_cover_social:
//                        ImageViewIDWhileCroppingImage = R.id.imageview_cover_social;
//                        break;
//                    case R.id.imageview_cover_dating:
//                        ImageViewIDWhileCroppingImage = R.id.imageview_cover_dating;
//                        break;
//                    case R.id.imageview_cover_business:
//                        ImageViewIDWhileCroppingImage = R.id.imageview_cover_business;
//                        break;
                    default:
                        ImageViewIDWhileCroppingImage = 0;
                        break;
                }
                CropImage.activity(imageUri)
                        .start(BaseDrawerActivity.this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(BaseDrawerActivity.this, "Image Path is null !!", Toast.LENGTH_SHORT).show();
            newProfilePicture = false;
        }
    }

    private void saveImageAndShowImageView(int imageViewID, String imageName, Intent intent) {
        int IMAGE_SIZE = 0;

        // Get the Image from data
        Uri selectedImage = intent.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        // Get the cursor
        Cursor cursor = BaseDrawerActivity.this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        // Move to first row
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String imgDecodableString = cursor.getString(columnIndex);
        cursor.close();

        String outputImageFile = PROFILE_PICTURES + File.separator + imageName + ".jpg";

        if (outputImageFile != null) {
            try {
                if (!imgDecodableString.equals(outputImageFile)) {

                    switch (imageViewID) {
                        case R.id.imageView_profile_social:
                            ImageViewIDWhileCroppingImage = R.id.imageView_profile_social;
                            break;
                        case R.id.imageView_profile_dating:
                            ImageViewIDWhileCroppingImage = R.id.imageView_profile_dating;
                            break;
                        case R.id.imageView_profile_business:
                            ImageViewIDWhileCroppingImage = R.id.imageView_profile_business;
                            break;
//                        case R.id.imageview_cover_social:
//                            ImageViewIDWhileCroppingImage = R.id.imageview_cover_social;
//                            break;
//                        case R.id.imageview_cover_dating:
//                            ImageViewIDWhileCroppingImage = R.id.imageview_cover_dating;
//                            break;
//                        case R.id.imageview_cover_business:
//                            ImageViewIDWhileCroppingImage = R.id.imageview_cover_business;
//                            break;

                        default:
                            ImageViewIDWhileCroppingImage = 0;
                            break;
                    }
                    CropImage.activity(selectedImage)
                            .start(BaseDrawerActivity.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(BaseDrawerActivity.this, getResources().getString(R.string.cant_copy_image), Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(BaseDrawerActivity.this, "Image Path is null !!", Toast.LENGTH_SHORT).show();
            newProfilePicture = false;
        }
    }

    private void setImage(Uri imageURI) {
        File post_image;
        RequestBody requestPhotoFile;
        MultipartBody.Part postImageBody;
        String imageName = "";
        String outPuFilePath = "";
        File file;

        try {
            String user_d = null, catagory_id = null;
            String defaultCategory = AppSettings.getProfileDefaultCategory();
            if (defaultCategory == null)
                defaultCategory = "0";
            if (isPrimary) {
                switch (defaultCategory) {
                    case PROFILE_CATEGORY_ONE_SOCIAL:
                        imageName = BaseDrawerActivity.this.getString(R.string.profile_avatar_social);
                        outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";

                        catagory_id = PROFILE_CATEGORY_ONE_SOCIAL;
                        copyFile(imageURI.getPath(), outPuFilePath);
                        file = new File(outPuFilePath);
                        if (file.exists()) {
                            Uri imageUri = Uri.fromFile(file);
                            imageViewDefaultProfilePicture.setImageDrawable(null);
//                        imageViewDefaultProfilePicture.setImageURI(imageUri);
                            imageFile = file;
                        } else {
                            Toast.makeText(BaseDrawerActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case PROFILE_CATEGORY_TWO_DATING:
                        imageName = BaseDrawerActivity.this.getString(R.string.profile_avatar_dating);
                        outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";

                        catagory_id = PROFILE_CATEGORY_TWO_DATING;
                        copyFile(imageURI.getPath(), outPuFilePath);
                        file = new File(outPuFilePath);
                        if (file.exists()) {
                            Uri imageUri = Uri.fromFile(file);
                            imageViewDefaultProfilePicture.setImageDrawable(null);
//                        imageViewDefaultProfilePicture.setImageURI(imageUri);
                            imageFile = file;
                        } else {
                            Toast.makeText(BaseDrawerActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case PROFILE_CATEGORY_THREE_BUSINESS:
                        imageName = BaseDrawerActivity.this.getString(R.string.profile_avatar_business);
                        outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";

                        catagory_id = PROFILE_CATEGORY_THREE_BUSINESS;
                        copyFile(imageURI.getPath(), outPuFilePath);
                        file = new File(outPuFilePath);
                        if (file.exists()) {
                            Uri imageUri = Uri.fromFile(file);
                            imageViewDefaultProfilePicture.setImageDrawable(null);
//                        imageViewDefaultProfilePicture.setImageURI(imageUri);
                            imageFile = file;
                        } else {
                            Toast.makeText(BaseDrawerActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        imageName = BaseDrawerActivity.this.getString(R.string.profile_avatar_social);
                        outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";

                        catagory_id = PROFILE_CATEGORY_ONE_SOCIAL;
                        copyFile(imageURI.getPath(), outPuFilePath);
                        file = new File(outPuFilePath);
                        if (file.exists()) {
                            Uri imageUri = Uri.fromFile(file);
                            imageViewDefaultProfilePicture.setImageDrawable(null);
//                        imageViewDefaultProfilePicture.setImageURI(imageUri);
                            imageFile = file;
                        } else {
                            Toast.makeText(BaseDrawerActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

                user_d = AppSettings.getLoginUserId();
                RequestBody descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), user_d);
                RequestBody descriptionCatId = RequestBody.create(MediaType.parse("text/plain"), catagory_id);
                post_image = new File(outPuFilePath);
                requestPhotoFile = RequestBody.create(MediaType.parse("image/*"), post_image);
                postImageBody = MultipartBody.Part.createFormData("profile_pic", post_image.getName(), requestPhotoFile);
                uploadPrimaryProfilePhoto(descriptionUserId, descriptionCatId, postImageBody);

            } else {
                imageName = BaseDrawerActivity.this.getString(R.string.option_pic);
                outPuFilePath = PROFILE_PICTURES + File.separator + imageName + ".jpg";

                catagory_id = PROFILE_CATEGORY_ONE_SOCIAL;
                copyFile(imageURI.getPath(), outPuFilePath);
                file = new File(outPuFilePath);
                if (file.exists()) {
                    Uri imageUri = Uri.fromFile(file);
                    if (optionPictures.get(optionPictures.size() - 1) != null)
                        optionPictures.get(optionPictures.size() - 1).setImageDrawable(null);
//                        imageViewDefaultProfilePicture.setImageURI(imageUri);
                    imageFile = file;
                } else {
                    Toast.makeText(BaseDrawerActivity.this, "Image was not cropped successfully.", Toast.LENGTH_SHORT).show();
                }


                user_d = AppSettings.getLoginUserId();

                RequestBody descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), user_d);
                RequestBody descriptionCatId = RequestBody.create(MediaType.parse("text/plain"), catagory_id);
                post_image = new File(outPuFilePath);
                requestPhotoFile = RequestBody.create(MediaType.parse("image/*"), post_image);
                postImageBody = MultipartBody.Part.createFormData("profile_pic", post_image.getName(), requestPhotoFile);
                uploadOptionProfilePhoto(descriptionUserId, descriptionCatId, postImageBody);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void uploadPrimaryProfilePhoto(RequestBody descriptionUserId, RequestBody
            profile_cat_id, MultipartBody.Part postImageBody) {
        progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ProfilePictureResponse> callUploadVideo = requestInterface.set_profileImage(descriptionUserId, profile_cat_id, postImageBody);
            callUploadVideo.enqueue(new Callback<ProfilePictureResponse>() {
                @Override
                public void onResponse(Call<ProfilePictureResponse> call, retrofit2.Response<ProfilePictureResponse> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    isPrimary = false;

                    if (null != response) {

                        String defaultCategory = AppSettings.getProfileDefaultCategory();
                        if (defaultCategory == null)
                            defaultCategory = "0";

                        switch (defaultCategory) {
                            case PROFILE_CATEGORY_ONE_SOCIAL:
                                AppSettings.setProfileCatPicPathOne(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountMultiple_profile());
                                break;
                            case PROFILE_CATEGORY_TWO_DATING:
                                AppSettings.setProfileCatPicPathTwo(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                AppSettings.setCountOptionPicCategoryTwoDating(response.body().getCountMultiple_profile());
                                break;
                            case PROFILE_CATEGORY_THREE_BUSINESS:
                                AppSettings.setProfileCatPicPathThree(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                AppSettings.setCountOptionPicCategoryThreeBusiness(response.body().getCountMultiple_profile());
                                break;
                            default:
                                AppSettings.setProfileCatPicPathOne(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountMultiple_profile());
                                break;
                        }

                        if (response.body().getMulti_profile_info_cat() != null && response.body().getMulti_profile_info_cat().size() > 0) {
                            Gson gson = new Gson();
                            String jsonInString = gson.toJson(response.body().getMulti_profile_info_cat());
                            AppSettings.setOptionPicArray(jsonInString);
                        }
                        checkProfileUploadedImages();
                    }
                }

                @Override
                public void onFailure(Call<ProfilePictureResponse> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    Log.e(TAG, "## onFailure e : " + t.getMessage());
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            e.printStackTrace();
        }
    }

    private void uploadOptionProfilePhoto(RequestBody descriptionUserId, RequestBody
            profile_cat_id, MultipartBody.Part postImageBody) {
        progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseOptionImageUpload> callUploadVideo = requestInterface.setOptionProfileImage(descriptionUserId, profile_cat_id, postImageBody);
            callUploadVideo.enqueue(new Callback<ResponseOptionImageUpload>() {
                @Override
                public void onResponse(Call<ResponseOptionImageUpload> call, retrofit2.Response<ResponseOptionImageUpload> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (null != response) {

                        String currentCategory = AppSettings.getProfileDefaultCategory();
                        if (currentCategory != null && !currentCategory.equalsIgnoreCase("") && !currentCategory.isEmpty()) {
                            switch (currentCategory) {
                                case PROFILE_CATEGORY_ONE_SOCIAL:
                                    AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountProfilePicSet());
                                    break;
                                case PROFILE_CATEGORY_TWO_DATING:
                                    AppSettings.setCountOptionPicCategoryTwoDating(response.body().getCountProfilePicSet());
                                    break;
                                case PROFILE_CATEGORY_THREE_BUSINESS:
                                    AppSettings.setCountOptionPicCategoryThreeBusiness(response.body().getCountProfilePicSet());
                                    break;
                                default:
                                    AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountProfilePicSet());
                                    break;
                            }
                        } else {
                            AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountProfilePicSet());
                        }

                        if (response.body().getPicData() != null && response.body().getPicData().size() > 0) {
                            Gson gson = new Gson();
                            String jsonInString = gson.toJson(response.body().getPicData());
                            AppSettings.setOptionPicArray(jsonInString);
                        }
                        checkProfileUploadedImages();
                    }
                }

                @Override
                public void onFailure(Call<ResponseOptionImageUpload> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(BaseDrawerActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callSwitchAPI(String userID, String profile_cat_id, final String optionImageID, final String optionPicURL) {
        progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseSwitchProfileImage> callUploadVideo = requestInterface.switchProfileImage(userID, profile_cat_id, optionImageID);
            callUploadVideo.enqueue(new Callback<ResponseSwitchProfileImage>() {
                @Override
                public void onResponse(Call<ResponseSwitchProfileImage> call, retrofit2.Response<ResponseSwitchProfileImage> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {

                        String currentCategory = AppSettings.getProfileDefaultCategory();
                        if (currentCategory != null && !currentCategory.equalsIgnoreCase("") && !currentCategory.isEmpty()) {
                            switch (currentCategory) {
                                case PROFILE_CATEGORY_ONE_SOCIAL:
                                    AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountMultiple_profile());
                                    AppSettings.setProfileCatPicPathOne(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                    break;
                                case PROFILE_CATEGORY_TWO_DATING:
                                    AppSettings.setCountOptionPicCategoryTwoDating(response.body().getCountMultiple_profile());
                                    AppSettings.setProfileCatPicPathTwo(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                    break;
                                case PROFILE_CATEGORY_THREE_BUSINESS:
                                    AppSettings.setCountOptionPicCategoryThreeBusiness(response.body().getCountMultiple_profile());
                                    AppSettings.setProfileCatPicPathThree(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                    break;
                                default:
                                    AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountMultiple_profile());
                                    AppSettings.setProfileCatPicPathOne(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                    break;
                            }
                        } else {
                            AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountMultiple_profile());
                            AppSettings.setProfileCatPicPathOne(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                        }

                        if (response.body().getMulti_profile_info_cat() != null && response.body().getMulti_profile_info_cat().size() > 0) {
                            Gson gson = new Gson();
                            String jsonInString = gson.toJson(response.body().getMulti_profile_info_cat());
                            AppSettings.setOptionPicArray(jsonInString);
                        }
                        checkProfileUploadedImages();
                    }
                }

                @Override
                public void onFailure(Call<ResponseSwitchProfileImage> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    Log.e(TAG, "## onFailure e : " + t.getMessage());
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            e.printStackTrace();
        }
    }

    public void callDeleteProfilePic(String userID, String profile_cat_id, final String optionImageID, String mainSeletec) {
        progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {
            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseDeleteProfilePic> callUploadVideo = requestInterface.deleteProfileImage(userID, profile_cat_id, optionImageID, mainSeletec);
            callUploadVideo.enqueue(new Callback<ResponseDeleteProfilePic>() {
                @Override
                public void onResponse(Call<ResponseDeleteProfilePic> call, retrofit2.Response<ResponseDeleteProfilePic> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {

                        String currentCategory = AppSettings.getProfileDefaultCategory();
                        if (currentCategory != null && !currentCategory.equalsIgnoreCase("") && !currentCategory.isEmpty()) {
                            switch (currentCategory) {
                                case PROFILE_CATEGORY_ONE_SOCIAL:
                                    AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountMultiple_profile());
                                    AppSettings.setProfileCatPicPathOne(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                    break;
                                case PROFILE_CATEGORY_TWO_DATING:
                                    AppSettings.setCountOptionPicCategoryTwoDating(response.body().getCountMultiple_profile());
                                    AppSettings.setProfileCatPicPathTwo(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                    break;
                                case PROFILE_CATEGORY_THREE_BUSINESS:
                                    AppSettings.setCountOptionPicCategoryThreeBusiness(response.body().getCountMultiple_profile());
                                    AppSettings.setProfileCatPicPathThree(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                    break;
                                default:
                                    AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountMultiple_profile());
                                    AppSettings.setProfileCatPicPathOne(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                                    break;
                            }
                        } else {
                            AppSettings.setCountOptionPicCategoryOneSocial(response.body().getCountMultiple_profile());
                            AppSettings.setProfileCatPicPathOne(PROFILE_PICTURE_PATH + File.separator + response.body().getDefault_profile_pic());
                        }

                        if (response.body().getMulti_profile_info_cat() != null && response.body().getMulti_profile_info_cat().size() > 0) {
                            Gson gson = new Gson();
                            String jsonInString = gson.toJson(response.body().getMulti_profile_info_cat());
                            AppSettings.setOptionPicArray(jsonInString);
                        }
                        checkProfileUploadedImages();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDeleteProfilePic> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    Log.e(TAG, "## onFailure e : " + t.getMessage());
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            e.printStackTrace();
        }
    }

    private void determineScreenDensity() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;
        if (density >= DisplayMetrics.DENSITY_LOW && density < DisplayMetrics.DENSITY_MEDIUM) {
//            Toast.makeText(this, "DENSITY_LOW:" + String.valueOf(density), Toast.LENGTH_LONG).show();
            angle = 75.0f;
            transitionX = 110.0f;
            transitionY = 110.0f;
            picWidth = 40;
            picHeight = 40;
        } else if (density >= DisplayMetrics.DENSITY_MEDIUM && density < DisplayMetrics.DENSITY_HIGH) {
//            Toast.makeText(this, "DENSITY_MEDIUM: " + String.valueOf(density), Toast.LENGTH_LONG).show();
            angle = 75.0f;
            transitionX = 125.0f;
            transitionY = 125.0f;
            picWidth = 40;
            picHeight = 40;
        } else if (density >= DisplayMetrics.DENSITY_HIGH && density < DisplayMetrics.DENSITY_XHIGH) {
//            Toast.makeText(this, "DENSITY_HIGH: " + String.valueOf(density), Toast.LENGTH_LONG).show();
            angle = 75.0f;
            transitionX = 140.0f;
            transitionY = 140.0f;
            picWidth = 45;
            picHeight = 45;
        } else if (density >= DisplayMetrics.DENSITY_XHIGH && density < DisplayMetrics.DENSITY_XXHIGH) {
//            Toast.makeText(this, "Density XHIGH: " + String.valueOf(density), Toast.LENGTH_LONG).show();
            angle = 75.0f;
            transitionX = 150.0f;
            transitionY = 150.0f;
            picWidth = 50;
            picHeight = 50;
        } else if (density >= DisplayMetrics.DENSITY_XXHIGH && density < DisplayMetrics.DENSITY_XXXHIGH) {
//            Toast.makeText(this, "Density XXHIGH: " + String.valueOf(density), Toast.LENGTH_LONG).show();
            angle = 75.0f;
            transitionX = 215.0f;
            transitionY = 215.0f;
            picWidth = 80;
            picHeight = 80;
        } else if (density >= DisplayMetrics.DENSITY_XXXHIGH) {
//            Toast.makeText(this, "Density XXXHIGH: " + String.valueOf(density), Toast.LENGTH_LONG).show();
            angle = 75.0f;
            transitionX = 240.0f;
            transitionY = 140.0f;
            picWidth = 110;
            picHeight = 110;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    public void sendFriendRequest(final String receiver_user_id) {
        progressDialog.setMessage(BaseDrawerActivity.this.getString(R.string.msg_sending_friend_request));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = format.format(new Date());
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<ResponseSuccess> loginCall = apiInterface.sendOrbitRequest(AppSettings.getLoginUserId(), receiver_user_id, currentDateandTime);
        loginCall.enqueue(new Callback<ResponseSuccess>() {
            @Override
            public void onResponse(Call<ResponseSuccess> call, retrofit2.Response<ResponseSuccess> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {//success
                    final Dialog dialog = CommonDialogs.showMessageDialog(BaseDrawerActivity.this, getResources().getString(R.string.ongraviti), response.body().getMsg());
                    Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                    dialog_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
//                            startActivity(new Intent(BaseDrawerActivity.this, MyOrbitActivity.class));
//                            overridePendingTransition(R.anim.slide_in_right,
//                                    R.anim.slide_out_left);
                            finish();
                        }
                    });

                } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                    CommonDialogs.showMessageDialog(BaseDrawerActivity.this, getResources().getString(R.string.ongraviti), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Toast.makeText(BaseDrawerActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}