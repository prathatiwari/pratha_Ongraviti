package com.exceptionaire.ongraviti.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.EnrollEventResponse;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_activities.OpponentsActivityVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_service.CallServiceVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.SharedPrefsHelperVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.UsersUtilsVideo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.model.QBUser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

public class AddVideoBioActivity extends BaseDrawerActivity implements View.OnClickListener, AppConstants {

    private LinearLayout upload_video, record_video;
    private Button buy_session, buy_next;

    private String selectedVideoPath, selectedVideoUrl;
    private ImageView upload_video_thumb, record_video_thumb;
    private int clickId;
    private Uri recordedFileUri;
    private Intent intent;
    private int cameraId = 0;
    private Camera camera;
    private ProgressDialog progressDialog;
    private boolean newVideoBio = false;
    private QBUser userForSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_add_video_bio);
        getLayoutInflater().inflate(R.layout.activity_add_video_bio, frameLayout);
        arcMenu.setVisibility(View.GONE);
        initialiseComponents();
    }

    private void initialiseComponents() {
        upload_video = (LinearLayout) findViewById(R.id.upload_video);
        record_video = (LinearLayout) findViewById(R.id.record_video);
        buy_session = (Button) findViewById(R.id.buy_session);
        upload_video_thumb = (ImageView) findViewById(R.id.upload_video_thumb);
        record_video_thumb = (ImageView) findViewById(R.id.record_video_thumb);

        upload_video.setOnClickListener(this);
        record_video.setOnClickListener(this);
        buy_session.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.upload_video:
                clickId = R.id.upload_video;
                checkForPermissionsAndShowOptionDialog();
                break;

            case R.id.record_video:
                clickId = R.id.record_video;
                checkForPermissionsAndShowOptionDialog();
                break;

            case R.id.buy_session:


                if (isNetworkAvailable()) {
                    if (AppSettings.getLoginUserId() != null && AppSettings.getKeyEventId() != null) {

                        if (newVideoBio) {

                            File videoFile = new File(SPEED_DATING_VIDEO_BIO);
                            File videoThumbFile = new File(SPEED_DATING_VIDEO_BIO_THUMB);

                            RequestBody requestVideoFile = RequestBody.create(MediaType.parse("video"), videoFile);

                            MultipartBody.Part videoBody = MultipartBody.Part.createFormData("event_video_name", videoFile.getName(), requestVideoFile);

                            RequestBody requestVideoThumbFile = RequestBody.create(MediaType.parse("image"), videoThumbFile);

                            MultipartBody.Part videoThumbBody = MultipartBody.Part.createFormData("event_video_thumb", videoThumbFile.getName(), requestVideoThumbFile);

                            RequestBody descriptionUserId = RequestBody.create(MediaType.parse("text/plain"), AppSettings.getLoginUserId());

                            RequestBody descriptionEventId = RequestBody.create(MediaType.parse("text/plain"), AppSettings.getKeyEventId());


                            uploadVideoBio(descriptionUserId, descriptionEventId, videoBody, videoThumbBody);
                        } else {
                            Toast.makeText(this, getApplicationContext().getResources().getString(R.string.video_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                } else {
                    CommonDialogs.showMessageDialog(this, getResources().getString(R.string.ongraviti), getResources().getString(R.string.check_internet)).show();
                }

//                startSignUpNewUser(createUserWithEnteredData());
                break;
        }

    }

    private void checkForPermissionsAndShowOptionDialog() {

        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, REQUEST_TAKE_GALLERY_VIDEO);
        } else {

            if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            } else {
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.INTERNAL_CONTENT_URI);
            }
            if (clickId == R.id.upload_video) {
                selectVideoFromGallery();
            } else if (clickId == R.id.record_video) {
                recordVideo();
            }
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void selectVideoFromGallery() {

        intent.setAction(Intent.ACTION_PICK);
        intent.setType("video/*");
        intent.putExtra("return-data", true);
        startActivityForResult(intent, REQUEST_TAKE_GALLERY_VIDEO);
    }

    public void recordVideo() {

        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            Toast.makeText(this, "Front camera on this device", Toast.LENGTH_LONG)
                    .show();
            intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            // create a file to save the video
            recordedFileUri = getOutputMediaFileUri(MEDIA_TYPE_RECORD_VIDEO);

            // set the image file name

            intent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_FRONT);
            intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
            intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, recordedFileUri);
            // set the video image quality to high
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

            // start the Video Capture Intent
            startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);

        } else {
            Toast.makeText(this, "No Front camera on this device", Toast.LENGTH_LONG)
                    .show();
            intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            // create a file to save the video
            recordedFileUri = getOutputMediaFileUri(MEDIA_TYPE_RECORD_VIDEO);

            // set the image file name
            intent.putExtra(MediaStore.EXTRA_OUTPUT, recordedFileUri);

            // set the video image quality to high
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

            // start the Video Capture Intent
            startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
        }

    }

    private static Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        File dir = new File(SPEED_DATING_APP_PATH);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        File mediaFile;

        if (type == MEDIA_TYPE_RECORD_VIDEO) {
            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(SPEED_DATING_VIDEO_BIO);

        } else {
            return null;
        }

        return mediaFile;
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        File dir = new File(SPEED_DATING_APP_PATH);
        try {
            if (dir.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (requestCode) {

            case REQUEST_TAKE_GALLERY_VIDEO:

                if (resultCode == RESULT_OK) {
                    selectedVideoPath = getPath(intent.getData());

                    try {
                        if (selectedVideoPath == null) {
                            Log.e("selected video path = null!", selectedVideoPath);
                            newVideoBio = false;
                            upload_video_thumb.setImageResource(R.drawable.upload);
                            record_video_thumb.setImageResource(R.drawable.record);
                            finish();
                        } else {
                            Log.e("selected video path = null!----------------->", selectedVideoPath);

                            newVideoBio = true;
                            copyFile(selectedVideoPath, SPEED_DATING_VIDEO_BIO);
                            createVideoThumbnail(SPEED_DATING_VIDEO_BIO);

                            /**
                             * try to do something there
                             * selectedVideoPath is path to the selected video
                             */
                        }
                    } catch (Exception e) {
                        //#debug
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "You haven't picked an Video", Toast.LENGTH_LONG).show();
                    newVideoBio = false;
                    upload_video_thumb.setImageResource(R.drawable.upload);
                    record_video_thumb.setImageResource(R.drawable.record);
                }

                break;

            case CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (SPEED_DATING_VIDEO_BIO != null) {
                        newVideoBio = true;
                        createVideoThumbnail(SPEED_DATING_VIDEO_BIO);
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    newVideoBio = false;
                    upload_video_thumb.setImageResource(R.drawable.upload);
                    record_video_thumb.setImageResource(R.drawable.record);
                    Toast.makeText(this, "User cancelled the video capture.",
                            Toast.LENGTH_LONG).show();
                } else {
                    newVideoBio = false;
                    upload_video_thumb.setImageResource(R.drawable.upload);
                    record_video_thumb.setImageResource(R.drawable.record);
                    Toast.makeText(this, "Video capture failed.",
                            Toast.LENGTH_LONG).show();
                }
                break;

        }

        if (resultCode == EXTRA_LOGIN_RESULT_CODE) {
            boolean isLoginSuccess = intent.getBooleanExtra(EXTRA_LOGIN_RESULT, false);
            String errorMessage = intent.getStringExtra(EXTRA_LOGIN_ERROR_MESSAGE);

            if (isLoginSuccess) {
                saveUserData(userForSave, PREF_CURREN_ROOM_NAME);
                signInCreatedUser(userForSave, false);
            } else {
                Toast.makeText(AddVideoBioActivity.this, R.string.login_chat_login_error, Toast.LENGTH_SHORT).show();

//                userNameEditText.setText(userForSave.getFullName());
//                chatRoomNameEditText.setText(userForSave.getTags().get(0));
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }

    public void copyFile(String selectedImagePath, String outputFilePath) throws IOException {
        InputStream in = new FileInputStream(selectedImagePath);
        OutputStream out = new FileOutputStream(outputFilePath);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();

//        Toast.makeText(getActivity(), "Video Transferred", Toast.LENGTH_LONG).show();
    }


    public void createVideoThumbnail(String filePath) {
        Bitmap bmThumbnail;


        // MINI_KIND: 512 x 384 thumbnail
        bmThumbnail = ThumbnailUtils.createVideoThumbnail(filePath,
                MediaStore.Images.Thumbnails.MINI_KIND);
        if (bmThumbnail != null) {
            try {

                bmThumbnail.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(SPEED_DATING_VIDEO_BIO_THUMB));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (clickId == R.id.upload_video) {
            upload_video_thumb.setImageBitmap(bmThumbnail);
            record_video_thumb.setImageResource(R.drawable.record);
        } else if (clickId == R.id.record_video) {
            record_video_thumb.setImageBitmap(bmThumbnail);
            upload_video_thumb.setImageResource(R.drawable.upload);
        }


    }


    /************
     * Upload Bio Video
     ***********/

    private void uploadVideoBio(RequestBody user_id, RequestBody event_id, MultipartBody.Part videoBio, MultipartBody.Part videoBioThumb) {

        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();

        try {

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<EnrollEventResponse> callUploadVideo = requestInterface.uploadVideoBio(user_id, event_id, videoBio, videoBioThumb);
            callUploadVideo.enqueue(new Callback<EnrollEventResponse>() {
                @Override
                public void onResponse(Call<EnrollEventResponse> call, Response<EnrollEventResponse> response) {

                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (response.body() != null) {

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), SelectUserActivity.class));
//                            startActivity(new Intent(getApplicationContext(), OpponentsActivityVideo.class));
                        } else if (response.body().getStatus().equals(RESPONSE_ERROR)) {

                            Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EnrollEventResponse> call, Throwable t) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }


    /************
     * Quick Blox Registration
     **************/


    public QBUser createUserWithEnteredData() {
        return createQBUserWithCurrentData(String.valueOf("TestingTwo"),//user name
                String.valueOf("testingtwo@gmail.com"),// user email id
                String.valueOf("ongravity")); // group name
    }

    private QBUser createQBUserWithCurrentData(String userName, String email, String chatRoomName) {
        QBUser qbUser = null;
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(chatRoomName)) {
            StringifyArrayList<String> userTags = new StringifyArrayList<>();
            userTags.add(chatRoomName);

            qbUser = new QBUser();
            qbUser.setFullName(userName);
            qbUser.setEmail(email);
//            qbUser.setLogin(getCurrentDeviceId());
            qbUser.setPassword(DEFAULT_USER_PASSWORD);
            qbUser.setTags(userTags);
        }

        return qbUser;
    }


    public void startSignUpNewUser(final QBUser newUser) {
        requestExecutor.signUpNewUser(newUser, new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser result, Bundle params) {
                        loginToChat(result);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (e.getHttpStatusCode() == ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                            signInCreatedUser(newUser, true);
                        } else {
                            Toast.makeText(AddVideoBioActivity.this, R.string.sign_up_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );
    }

    private void loginToChat(final QBUser qbUser) {
        qbUser.setPassword(DEFAULT_USER_PASSWORD);

        userForSave = qbUser;


        startLoginService(qbUser);
    }

    private void startLoginService(QBUser qbUser) {
        Intent tempIntent = new Intent(this, CallServiceVideo.class);
        PendingIntent pendingIntent = createPendingResult(EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
        CallServiceVideo.start(this, qbUser, pendingIntent);
    }


    private void signInCreatedUser(final QBUser user, final boolean deleteCurrentUser) {
        requestExecutor.signInUser(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser result, Bundle params) {
                if (deleteCurrentUser) {
                    removeAllUserData(result);
//                    startOpponentsActivity();

                } else {


                    Log.i("Registered User info", user.toString());
                    Toast.makeText(AddVideoBioActivity.this, user.toString(), Toast.LENGTH_SHORT).show();

                    startOpponentsActivity();
                }
            }

            @Override
            public void onError(QBResponseException responseException) {
                Toast.makeText(AddVideoBioActivity.this, R.string.sign_up_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void removeAllUserData(final QBUser user) {
        requestExecutor.deleteCurrentUser(user.getId(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                UsersUtilsVideo.removeUserData(getApplicationContext());
                startSignUpNewUser(createUserWithEnteredData());
            }

            @Override
            public void onError(QBResponseException e) {
                Toast.makeText(AddVideoBioActivity.this, R.string.sign_up_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void startOpponentsActivity() {
        OpponentsActivityVideo.start(AddVideoBioActivity.this, false);
        //  finish();
    }
}
