package com.exceptionaire.ongraviti.activities.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.EditProfileDetailsActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseSetUserInfo;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_ERROR;
import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_SUCCESS;


public class EditProfileMyLivingFragment extends android.support.v4.app.Fragment {

    private Button buttonSave, buttonPrevious, buttonNext;
    private View view;
    private ProgressDialog progressDialog;
    Spinner spinner_highest_degree, spinner_employment_status;
    EditText edit_text_degree_name, edit_text_future_goals, editTextCollege;

    String[] arr_emp_status = {"Select", "Employed", "Unemployed", "Self-employed"};
    String[] arr_highest_degree = {"Select", "Bachelors", "Masters", "PhD", "Other"};
    private ResponseGetUserProfileData userProfileDataFromResponse;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_edit_profile_my_living, container, false);
        setActionBar();
        initializeViewComponent();

        setData();
        return view;
    }


    private void initializeViewComponent() {
        AppSettings.setProfileStepStatus3(1);

        editTextCollege = (EditText) view.findViewById(R.id.edit_text_college);
        edit_text_degree_name = (EditText) view.findViewById(R.id.edit_text_degree_name);
        edit_text_future_goals = (EditText) view.findViewById(R.id.edit_text_future_goals);

        spinner_highest_degree = (Spinner) view.findViewById(R.id.spinner_highest_degree);
        fillDataInSpinner(spinner_highest_degree, arr_highest_degree);

        spinner_employment_status = (Spinner) view.findViewById(R.id.spinner_employment_status);
        fillDataInSpinner(spinner_employment_status, arr_emp_status);

        buttonSave = view.findViewById(R.id.button_save);
        buttonPrevious = view.findViewById(R.id.button_prev);
        buttonNext = view.findViewById(R.id.button_next);
        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new EditProfileILikeToDoFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_edit_profile_details, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();

            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValid()) {
                    AppSettings.setProfileStepStatus3(2);
                }

                saveDataInSharedPref();


            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new EditProfileILikeToBeFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_edit_profile_details, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }
        });
    }

    private boolean isValid() {

        if (editTextCollege.getText().toString().trim() != null &&
                editTextCollege.getText().toString().trim().length() == 0) {
            return false;

        } else if (edit_text_degree_name.getText().toString().trim() != null &&
                edit_text_degree_name.getText().toString().trim().length() == 0) {
            return false;

        } else if (spinner_highest_degree.getSelectedItem().toString() != null &&
                spinner_highest_degree.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spinner_employment_status.getSelectedItem().toString() != null &&
                spinner_employment_status.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (edit_text_future_goals.getText().toString().trim() != null &&
                edit_text_future_goals.getText().toString().trim().length() == 0) {
            return false;

        } else {
            return true;
        }
    }

    //Set custom action bar
    private void setActionBar() {
    }

    private void setData() {

        userProfileDataFromResponse = ((EditProfileDetailsActivity) getActivity()).getUserProfileDataFromResponse();
        if (userProfileDataFromResponse != null) {
            UserBasicInfo basic_user_info = userProfileDataFromResponse.getUser_basic_info()[0];


            editTextCollege.setText(basic_user_info.getCollege_university_name());
            edit_text_degree_name.setText(basic_user_info.getDegree_name());
            edit_text_future_goals.setText(basic_user_info.getFuture_goal());

            if (arr_highest_degree.length > 0) {
                if (basic_user_info.getHighest_degree() != null)
                    for (int index = 0; index < arr_highest_degree.length; index++) {
                        if (basic_user_info.getHighest_degree() != null)
                            if (basic_user_info.getHighest_degree().equals(arr_highest_degree[index])) {
                                spinner_highest_degree.setSelection(index);
                                break;
                            }
                    }
            }
            if (arr_emp_status.length > 0) {
                if (basic_user_info.getEmployment_status() != null)
                    for (int index = 0; index < arr_emp_status.length; index++) {
                        if (basic_user_info.getEmployment_status() != null)
                            if (basic_user_info.getEmployment_status().equals(arr_emp_status[index])) {
                                spinner_employment_status.setSelection(index);
                                break;
                            }
                    }
            }
        }
    }

    private void showProgressBar() {
        try {
            progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveDataInSharedPref() {
        String data = AppSettings.getProfileDetails();
        Gson gson = new Gson();
        ResponseGetUserProfileData responseGetUserProfileData = gson.fromJson(data, ResponseGetUserProfileData.class);

        if (responseGetUserProfileData != null) {
            UserBasicInfo userBasicInfo = responseGetUserProfileData.getUser_basic_info()[0];
            if (userBasicInfo != null) {
                if (editTextCollege.getText().toString().trim() != null) {
                    userBasicInfo.setCollege_university_name(editTextCollege.getText().toString().trim());
                } else {
                    userBasicInfo.setCollege_university_name("");
                }
                if (edit_text_degree_name.getText().toString().trim() != null) {
                    userBasicInfo.setDegree_name(edit_text_degree_name.getText().toString().trim());
                } else {
                    userBasicInfo.setDegree_name("");
                }
                if (edit_text_future_goals.getText().toString().trim() != null) {
                    userBasicInfo.setFuture_goal(edit_text_future_goals.getText().toString().trim());
                } else {
                    userBasicInfo.setFuture_goal("");
                }

                if (spinner_highest_degree.getSelectedItem().toString() != null &&
                        !spinner_highest_degree.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setHighest_degree(spinner_highest_degree.getSelectedItem().toString());
                } else {
                    userBasicInfo.setHighest_degree("");
                }

                if (spinner_employment_status.getSelectedItem().toString() != null &&
                        !spinner_employment_status.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setEmployment_status(spinner_employment_status.getSelectedItem().toString());
                } else {
                    userBasicInfo.setEmployment_status("");
                }
                String json = gson.toJson(responseGetUserProfileData);
                AppSettings.setProfileDetails(json);
            }
        }

        showProgressBar();
        String degree;
        if (spinner_highest_degree.getSelectedItem().toString().equals("Select")) {
            degree = "NA";
        } else {
            degree = spinner_highest_degree.getSelectedItem().toString();
        }

        String employment_status;
        if (spinner_employment_status.getSelectedItem().toString().equals("Select")) {
            employment_status = "NA";
        } else {
            employment_status = spinner_employment_status.getSelectedItem().toString();
        }
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
        Call<ResponseSetUserInfo> loginCall = getProfileRequest.setUserProfileData3(
                AppSettings.getLoginUserId(),
                degree,
                edit_text_degree_name.getText().toString().trim(),
                editTextCollege.getText().toString().trim(),
                employment_status,
                edit_text_future_goals.getText().toString().trim());

        loginCall.enqueue(new Callback<ResponseSetUserInfo>() {
            @Override
            public void onResponse(Call<ResponseSetUserInfo> call, retrofit2.Response<ResponseSetUserInfo> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Fragment fragment = new EditProfileILikeToBeFragment();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.container_edit_profile_details, fragment);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        ft.commit();
                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), response.body().getMsg());
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseSetUserInfo> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    public void fillDataInSpinner(Spinner spinner, final String[] category) {
        if (getActivity() != null)
            Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## fillDataInSpinner");
        try {

            ArrayAdapter<String> aa = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, category);

            aa.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(aa);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {

//                    Toast.makeText(context, "onItemSelected : " + category[position] + "position : " + position, Toast.LENGTH_SHORT).show();
                    } else {
                        if (getActivity() != null)
                            Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## onItemSelected : " + category[position]);
                        //Toast.makeText(context, "onItemSelected : " + category[position], Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            if (getActivity() != null)
                Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }
}