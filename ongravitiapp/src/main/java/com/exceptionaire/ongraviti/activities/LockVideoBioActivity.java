package com.exceptionaire.ongraviti.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.VideoBioAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.model.VideoPOJO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 12/9/16.
 */
public class LockVideoBioActivity extends BaseActivity implements View.OnClickListener,VideoBioAdapter.VideoBioCustomButtonListner
{
    private Spinner answer_type,dropdown_answer;
    private EditText question,option_one,option_two,option_three,option_four;
    private RadioGroup answer_yes_no;
    private RadioButton yes,no,yesORno;
    private Button add_answer,submit;
    private LinearLayout dropdown,radio_group,parent_layout;
    private int spinner_position;
    private String[] dropdown_answer_array=new String[4];
    private ListView mListView;
    private List<VideoPOJO> lockVideoBioList;
    private VideoPOJO videoPOJO;
    private VideoBioAdapter videoBio;
    private String answerType,desiredAnswerDropdown,desiredAnswerRadio;
    private int FLAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lock_videobio_answer);
        initView();
        ArrayAdapter<String> answer_arraArrayAdapter=new ArrayAdapter<String>(LockVideoBioActivity.this,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(R.array.answer_type));
        answer_type.setAdapter(answer_arraArrayAdapter);
        answer_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
            spinner_position=position;
            answerType=answer_type.getSelectedItem().toString();

                if(spinner_position==0)
                {
                    dropdown.setVisibility(View.GONE);
                    radio_group.setVisibility(View.VISIBLE);
                    FLAG=0;
                }
                else if(spinner_position==1)
                {
                    radio_group.setVisibility(View.GONE);
                    dropdown.setVisibility(View.VISIBLE);
                    FLAG=1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        dropdown_answer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                showToast(dropdown_answer_array[i]);
                desiredAnswerDropdown=dropdown_answer_array[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void initView()
    {
        answer_type= (Spinner) findViewById(R.id.select_question_type);
        dropdown_answer= (Spinner) findViewById(R.id.dropdown_desired_answer);
        question= (EditText) findViewById(R.id.videoBioQuestion);
        option_one= (EditText) findViewById(R.id.dropdown_option_one);
        option_two= (EditText) findViewById(R.id.dropdown_option_two);
        option_three= (EditText) findViewById(R.id.dropdown_option_three);
        option_four= (EditText) findViewById(R.id.dropdown_option_four);
        answer_yes_no= (RadioGroup) findViewById(R.id.radio_group_desired_answer);
        yes= (RadioButton) findViewById(R.id.radio_desired_answer_yes);
        no= (RadioButton) findViewById(R.id.radio_desired_answer_no);
        add_answer= (Button) findViewById(R.id.btnAddNewQuestion);
        submit= (Button) findViewById(R.id.btnlockVideoBioSubmit);
        dropdown= (LinearLayout) findViewById(R.id.dropdown_linear);
        radio_group= (LinearLayout) findViewById(R.id.radio_linear);
        parent_layout= (LinearLayout) findViewById(R.id.parentLayout);
        mListView= (ListView) findViewById(R.id.listOfQuestions);
        dropdown.setVisibility(View.GONE);
        radio_group.setVisibility(View.GONE);
        option_one.addTextChangedListener(textWatcher);
        option_two.addTextChangedListener(textWatcher);
        option_three.addTextChangedListener(textWatcher);
        option_four.addTextChangedListener(textWatcher);
        add_answer.setOnClickListener(this);
        lockVideoBioList=new ArrayList<VideoPOJO>();


    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String s1 = option_one.getText().toString();
            String s2 = option_two.getText().toString();
            String s3 = option_three.getText().toString();
            String s4 = option_four.getText().toString();

            if (s1.equals("") || s2.equals("") || s3.equals("")|| s4.equals(""))
            {
                dropdown_answer.setEnabled(false);
            } else
            {
                dropdown_answer_array[0]=s1;
                dropdown_answer_array[1]=s2;
                dropdown_answer_array[2]=s3;
                dropdown_answer_array[3]=s4;
                dropdown_answer.setEnabled(true);
                ArrayAdapter<String> adp=new ArrayAdapter<String>(LockVideoBioActivity.this,android.R.layout.simple_spinner_dropdown_item,dropdown_answer_array);
                dropdown_answer.setAdapter(adp);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }

    };

    public boolean validateForm(String option1, String option2,String option3,String option4) {
        option_one.setError(null);
        option_two.setError(null);
        option_three.setError(null);
        option_four.setError(null);
        boolean valid = true;
        if (option1.length() < 1)
        {
            setErrorMsg(getResources().getString(R.string.enter_answer), option_one, true);
            valid = false;
        } else if (option2.length() < 1)
        {
            setErrorMsg(getResources().getString(R.string.enter_answer), option_two, true);
            valid = false;
        }
        if (option3.length() < 1)
        {
            setErrorMsg(getResources().getString(R.string.enter_answer), option_three, true);
            valid = false;
        } else if (option4.length() < 1)
        {
            setErrorMsg(getResources().getString(R.string.enter_answer), option_four, true);
            valid = false;
        }
        return valid;
    }


    @Override
    public void onClick(View view) {
        if(view==add_answer)
        {

            if(FLAG==0)
            {
                int selectedId = answer_yes_no.getCheckedRadioButtonId();
                yesORno = (RadioButton) findViewById(selectedId);
                desiredAnswerRadio=yesORno.getText().toString();
                System.out.println(desiredAnswerRadio);
                lockVideoBioList.add(new VideoPOJO(question.getText().toString(),answerType,"Yes","No","","",desiredAnswerRadio,FLAG));
                videoBio=new VideoBioAdapter(LockVideoBioActivity.this,lockVideoBioList);
                videoBio.setVideoBioCustomButtonListner(LockVideoBioActivity.this);
                mListView.setAdapter(videoBio);
                videoBio.notifyDataSetChanged();
            }
            else if(FLAG==1)
            {
                lockVideoBioList.add(new VideoPOJO(question.getText().toString(),answerType,option_one.getText().toString(),option_two.getText().toString(),option_three.getText().toString(),option_four.getText().toString(),desiredAnswerDropdown,FLAG));
                videoBio=new VideoBioAdapter(LockVideoBioActivity.this,lockVideoBioList);
                videoBio.setVideoBioCustomButtonListner(LockVideoBioActivity.this);
                mListView.setAdapter(videoBio);
                videoBio.notifyDataSetChanged();
            }

        }
    }


    @Override
    public void onButtonClickListner(int position)
    {
        videoPOJO=lockVideoBioList.get(position);
        int question_flag;
        question_flag=videoPOJO.getQuestion_flag();
        if(question_flag==0)
        {
            question.setText(videoPOJO.getQuestion());



        }
        if(question_flag==1)
        {
            question.setText(videoPOJO.getQuestion());
            option_one.setText(videoPOJO.getAnswer_type_one());
            option_two.setText(videoPOJO.getAnswer_type_two());
            option_three.setText(videoPOJO.getAnswer_type_three());
            option_four.setText(videoPOJO.getAnswer_type_four());
            dropdown_answer.setSelection(question_flag);


        }
    }
}
