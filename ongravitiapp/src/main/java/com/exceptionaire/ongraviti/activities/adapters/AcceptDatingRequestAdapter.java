package com.exceptionaire.ongraviti.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.AcceptDatingRequest;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.Ask_for_dating_list;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AcceptDatingRequestAdapter extends BaseAdapter {
    private List<Ask_for_dating_list> orbit_request_list;
    Context context;
    private AcceptDatingRequest acceptDatingRequest;

    public void setOrbitRequestList(List<Ask_for_dating_list> orbitRequests) {
        this.orbit_request_list.clear();
        this.orbit_request_list = orbitRequests;
        notifyDataSetChanged();
    }

    public AcceptDatingRequestAdapter(List<Ask_for_dating_list> contactList, Context context, AcceptDatingRequest acceptDatingRequest) {
        this.orbit_request_list = contactList;
        this.context = context;
        this.acceptDatingRequest = acceptDatingRequest;
    }

    public void removeItem(int position) {
        orbit_request_list.remove(position);
        notifyDataSetChanged();
    }

    private class Holder {
        TextView txtName, tv_mutual_friends;
        ImageButton ibtn_accept, ibtn_reject;
        RoundedImageView image;
        ImageView image_view_cat_type;
        LinearLayout linear_view_profile;
    }

    @Override
    public int getCount() {
        return orbit_request_list.size();
    }

    @Override
    public Object getItem(int arg0) {
        return orbit_request_list.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {

        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final Ask_for_dating_list orbitRequest = orbit_request_list.get(position);
        if (convertView == null) {
            holder = new AcceptDatingRequestAdapter.Holder();
            convertView = layoutInflater.inflate(R.layout.orbit_request_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");

            holder.txtName = convertView.findViewById(R.id.txt_contact_name);
            holder.tv_mutual_friends = convertView.findViewById(R.id.textview_email_address);
            holder.ibtn_accept = convertView.findViewById(R.id.ibtn_accept);
            holder.ibtn_reject = convertView.findViewById(R.id.ibtn_reject);
            holder.image = convertView.findViewById(R.id.user_profile_pic);
            holder.linear_view_profile = convertView.findViewById(R.id.linear_view_profile);
            holder.image_view_cat_type = convertView.findViewById(R.id.image_view_cat_type);
            convertView.setTag(holder);

        } else {
            holder = (AcceptDatingRequestAdapter.Holder) convertView.getTag();
        }
        holder.txtName.setText(orbitRequest.getUser_name());
        holder.tv_mutual_friends.setText(orbitRequest.getask_for_date_message());
        switch (orbitRequest.getProfile_cat_id()) {
            case "1":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_green);
                break;
            case "2":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_pink);
                break;
            case "3":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_blue);
                break;
        }
        if (orbitRequest.getProfile_picture() != null) {
            if (!orbitRequest.getProfile_picture().contains("https://graph")) {
                Picasso.with(context)
                        .load(AppConstants.PROFILE_PICTURE_PATH + "/" + orbitRequest.getProfile_picture())
                        .placeholder(R.drawable.profile)
                        .into(holder.image);
            } else if (orbitRequest.getProfile_picture().contains("https://graph")) {
                Picasso.with(context)
                        .load(orbitRequest.getProfile_picture())
                        .placeholder(R.drawable.profile)
                        .into(holder.image);
            }
        }

        /*if (orbitRequest.getOrbitFriendList() != null && !orbitRequest.getOrbitFriendList().isEmpty()) {
            holder.tv_mutual_friends.setText(orbitRequest.getOrbitFriendList().size() + " mutual friend(s)");
        }*/

        holder.linear_view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentProfile = new Intent(context, FavoriteUserInfoActivity.class);
                intentProfile.putExtra("ActivityName", "AcceptDatingRequestActivity");
                intentProfile.putExtra("DatingUserID", "" + orbitRequest.getUser_id());
                context.startActivity(intentProfile);
            }
        });

        holder.ibtn_accept.setOnClickListener(v -> acceptDatingRequest.acceptRejectDatingRequest(orbitRequest.getUser_id(), "3", orbitRequest.getAsk_for_date_id(), "0", position));

        holder.ibtn_reject.setOnClickListener(v -> acceptDatingRequest.acceptRejectDatingRequest(orbitRequest.getUser_id(), "2", orbitRequest.getAsk_for_date_id(), "2", position));


        return convertView;
    }
}
