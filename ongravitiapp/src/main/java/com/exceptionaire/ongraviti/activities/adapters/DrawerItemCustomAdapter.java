package com.exceptionaire.ongraviti.activities.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.DrawerModel;
import com.exceptionaire.ongraviti.quickblox.managers.QbDialogUtils;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import java.util.HashSet;
import java.util.Set;


public class DrawerItemCustomAdapter extends ArrayAdapter<DrawerModel> {

    private Context mContext;
    private int layoutResourceId;
    private DrawerModel data[] = null;

    public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, DrawerModel[] data) {

        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View listItem;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);

        ImageView imageViewIcon = listItem.findViewById(R.id.imageViewIcon);
        TextView textViewName = listItem.findViewById(R.id.textViewName);
        TextView textViewCount = listItem.findViewById(R.id.textViewCount1);
        ImageView imageViewIcon1 = listItem.findViewById(R.id.imageViewIcon1);
        DrawerModel folder = data[position];
        if (position == 4) {
            textViewCount.setVisibility(View.VISIBLE);
        } else {
            textViewCount.setVisibility(View.INVISIBLE);
        }
        if (position == 3 || position == 5 || position == 6) {
            imageViewIcon1.setVisibility(View.VISIBLE);
        } else {
            imageViewIcon1.setVisibility(View.GONE);
        }
        QBRestChatService.getTotalUnreadMessagesCount(null, null).performAsync(new QBEntityCallback<Integer>() {
            @Override
            public void onSuccess(Integer total, Bundle params) {
                textViewCount.setText(total + "");
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });

        imageViewIcon.setImageResource(folder.icon);
        textViewName.setText(folder.name);
        imageViewIcon1.setImageResource(folder.comming_soon);

        return listItem;
    }
}

