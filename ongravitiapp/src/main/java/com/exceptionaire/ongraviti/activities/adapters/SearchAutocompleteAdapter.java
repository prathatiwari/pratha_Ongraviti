package com.exceptionaire.ongraviti.activities.adapters;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.model.PlaceIDDescription;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.exceptionaire.ongraviti.core.AppConstants.PLACES_API_BASE;

/**
 * Created by Laxmikant on 1/4/16.
 */
public class SearchAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {


    public static ArrayList<PlaceIDDescription> resultListPlaces;
    ArrayList<String> resultList;
    public static String LOG_TAG = "AutocompleteAdapte";
    public static String lat, longt;
    public static int radius;

    public SearchAutocompleteAdapter(Context context, int textViewResourceId, String lat, String longt, int radius) {
        super(context, textViewResourceId);
        this.lat = lat;
        this.longt = longt;
        this.radius = radius;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    resultList = autocomplete(constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }


    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;
        ArrayList<PlaceIDDescription> resultListPlaceIDDescriptions = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {


            StringBuilder sb = new StringBuilder(PLACES_API_BASE + AppConstants.TYPE_AUTOCOMPLETE + AppConstants.OUT_JSON);  //+ Constants.TYPE_AUTOCOMPLETE + Constants.OUT_JSON)
//			sb.append("&components=country:in");
            sb.append("?location=" + lat + "," + longt);        //18.568807,73.7750902
            sb.append("&radius=" + radius);
            sb.append("&key=" + AppConstants.AUTOCOMPLETE_API_KEY_FOR_PLACES);
            sb.append("&input=" + URLEncoder.encode(input.replace(" ", "%20"), "utf8"));

            URL url = new URL(sb.toString());
            Log.d("Adptr", "## url : " + url.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "## Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "## Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            Log.d("Gogladptr", "## Places response:" + jsonResults);

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the PlaceIDDescription descriptions from the results
            resultList = new ArrayList<>(predsJsonArray.length());
            resultListPlaces = new ArrayList<>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                Log.d("Gogladptr", "## " + predsJsonArray.getJSONObject(i).getString("description"));
                Log.d("Gogladptr", "## ============================================================");

                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                resultListPlaces.add(new PlaceIDDescription(predsJsonArray.getJSONObject(i).getString("place_id"), predsJsonArray.getJSONObject(i).getString("description")));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, " ## Cannot process JSON results", e);
        }

        return resultList;
    }
}
