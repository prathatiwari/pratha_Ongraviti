package com.exceptionaire.ongraviti.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.CustomPagerAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.SearchableListDialog;
import com.exceptionaire.ongraviti.activities.base.ShowHidePasswordEditText;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.fcm.Config;
import com.exceptionaire.ongraviti.listener_interface.ClickListener;
import com.exceptionaire.ongraviti.model.Country;
import com.exceptionaire.ongraviti.model.LoginResponse;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.AppUtils;
import com.exceptionaire.ongraviti.utilities.CustomVolleyRequest;
import com.exceptionaire.ongraviti.utilities.LocationService;
import com.exceptionaire.ongraviti.utilities.LogUtil;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by bhakti gade on 20/6/16.
 */
public class SignUpActivity extends BaseActivity implements View.OnClickListener, Response.ErrorListener, Response.Listener {
    // declaring variables
    private AutoScrollViewPager autoScrollViewPager;
    private LinearLayout linearCountDots;
    private EditText edtEmailId, edtContectNumber;
    private ShowHidePasswordEditText edtPassword, cnfrmPassword;
    private String reg_mobile, reg_email_id, reg_password, reg_cnfrmPassword;
    private TextView txt_SignIn, txtCountryCode;
    private Button reg_btnSubmit;
    private long TIME = 10 * 1000;
    private RequestQueue requestQueue;
    private CustomVolleyRequest customVolleyRequest;
    private ProgressDialog progressDialog;
    private Context context = SignUpActivity.this;
    private List<Country> countryList;
    private SearchableListDialog _searchableListDialog;
    private SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<>();
    //1 for sign up and 2 for save quickblox details on server
    private int WEBSERVICE_FLAG;
    private int dotsCount;
    private ImageView[] dots;
    private CustomPagerAdapter mAdapter;
    String TAG = "sign up";
    boolean doubleBackToExitPressedOnce = false;

    private Button btnLoginWithFB;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            setContentView(R.layout.sign_up);

            callbackManager = CallbackManager.Factory.create();
            initComponents();
            initListeners();

            //Login manager for login with facebook
            LoginManager.getInstance().registerCallback(
                    callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            requestUserInfo(loginResult.getAccessToken());
                        }

                        @Override
                        public void
                        onCancel() {
                            Toast.makeText(SignUpActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(FacebookException error) {
                            // TODO Auto-generated method stub
                        }
                    });

        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.REQ_PHOTO_FILE) {
                LogUtil.writeDebugLog(TAG, "onActivityResult", "onActivityResult from CameraActivity");
//                mLogoImagePath = data.getStringExtra(AppConstants.EK_URL);
//                File image = new File(mLogoImagePath);
//                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//                Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
//                bitmap = Bitmap.createScaledBitmap(bitmap, 800, 800, true);
////                m_btnSelectAvatar.setImageBitmap(bitmap);
//                ResourceUtil.setVideoExtension(FilenameUtils.getExtension(mLogoImagePath));
            } else {
                try {
                    callbackManager.onActivityResult(requestCode, resultCode, data);
                } catch (Exception e) {
                    Log.e(TAG, "## error : " + e.getMessage());
                    e.printStackTrace();
                }
            }
        } else if (resultCode == AppConstants.EXTRA_LOGIN_RESULT_CODE) {
            LogUtil.writeDebugLog(TAG, "onActivityResult", "onActivityResult from callservice.");
            boolean isLoginSuccess = data.getBooleanExtra(AppConstants.EXTRA_LOGIN_RESULT, false);
            String errorMessage = data.getStringExtra(AppConstants.EXTRA_LOGIN_ERROR_MESSAGE);
//            if (isLoginSuccess) {
//                saveUserData(userForSave);
//                setAvatar();
//            } else {
//                Toaster.longToast(getString(R.string.login_chat_login_error) + errorMessage);
//            }
        }

    }

    @Override
    protected void onStart() {
        try {
            super.onStart();
            autoScrollViewPager.startAutoScroll();
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        try {
            super.onStop();
            autoScrollViewPager.stopAutoScroll();
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press once again to exit the app", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initComponents() {
        try {
            // find view by id all views
            autoScrollViewPager = (AutoScrollViewPager) findViewById(R.id.signup_view_pager);
            linearCountDots = (LinearLayout) findViewById(R.id.linearCountDots);
            edtContectNumber = (EditText) findViewById(R.id.userRegistrationBasicInfoContactNumber);
            edtEmailId = (EditText) findViewById(R.id.basic_registration_edt_reg_email_id);
            edtPassword = (ShowHidePasswordEditText) findViewById(R.id.basic_registration_edt_reg_password);
            cnfrmPassword = (ShowHidePasswordEditText) findViewById(R.id.basic_registration_edt_cnfrm_pw);
            btnLoginWithFB = (Button) findViewById(R.id.basic_registration_btn_fb_login);

            edtContectNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        hideKeyboard(v);
                    }
                }
            });

            edtEmailId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        hideKeyboard(v);
                    }
                }
            });

            edtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        hideKeyboard(v);
                    }
                }
            });

            cnfrmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        hideKeyboard(v);
                    }
                }
            });

            edtPassword.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
//                    ShowHidePasswordEditText.isShowingPassword=false;
                    if (edtPassword.getText().length() > 0) {
                        edtPassword.showPasswordVisibilityIndicator(true);
                    } else {
                        edtPassword.showPasswordVisibilityIndicator(false);
                    }

                    Log.d(TAG, "## onTouch : ");
                    return false;
                }
            });

            cnfrmPassword.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
//                    ShowHidePasswordEditText.isShowingPassword=false;
                    if (cnfrmPassword.getText().length() > 0) {
                        cnfrmPassword.showPasswordVisibilityIndicator(true);
                    } else {
                        cnfrmPassword.showPasswordVisibilityIndicator(false);
                    }
                    return false;
                }
            });
//        edtPassword.setHintTextColor(getResources().getColor(R.color.OnGravitiGray));
            edtPassword.setTypeface(edtEmailId.getTypeface());
            cnfrmPassword.setTypeface(edtEmailId.getTypeface());
            edtPassword.setFilters(new InputFilter[]{AppUtils.ignoreFirstWhiteSpace()});
            cnfrmPassword.setFilters(new InputFilter[]{AppUtils.ignoreFirstWhiteSpace()});
            reg_btnSubmit = (Button) findViewById(R.id.basic_registration_btn_reg_submit);
            txt_SignIn = (TextView) findViewById(R.id.user_basic_registration_txt_sign_in);
            txtCountryCode = (TextView) findViewById(R.id.userRegistrationBasicInfoCountryCode);
            //set header name for custom title bar
            new AsyncPhoneInitTask(this).execute();
            //get country code from dat file

            mAdapter = new CustomPagerAdapter(SignUpActivity.this);
            autoScrollViewPager.setAdapter(mAdapter);
            autoScrollViewPager.setInterval(5000);
            autoScrollViewPager.setBorderAnimation(false);
            setUiPageViewController();
            autoScrollViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.bullet_d));
                    }
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.bullet_a));
                }

                @Override
                public void onPageSelected(int position) {
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void initListeners() {
        try {


            btnLoginWithFB.setOnClickListener(this);

            reg_btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    reg_mobile = edtContectNumber.getText().toString().trim();
                    reg_email_id = edtEmailId.getText().toString().trim();
                    reg_password = edtPassword.getText().toString();
                    reg_cnfrmPassword = cnfrmPassword.getText().toString();
                    //disable button for 10 seconds
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            reg_btnSubmit.setEnabled(true);
                        }
                    }, TIME);
                    //Checking internet connection
                    if (isNetworkAvailable()) {
                        //validation for views
                        if (validateForm(reg_mobile, reg_email_id, reg_password, reg_cnfrmPassword)) {
                            userRegistration();
                        }
                    } else {
                        CommonDialogs.showMessageDialog(SignUpActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                    }
                }
            });

            txt_SignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            txt_SignIn.setEnabled(true);
                        }
                    }, TIME);
                    startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                    finish();
                }
            });


            txtCountryCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    _searchableListDialog = SearchableListDialog.newInstance(countryList, new ClickListener() {
                        @Override
                        public void onClick(Country country) {
                            txtCountryCode.setText(String.valueOf(country.getCountryCode()));
                            //country_code = String.valueOf(country.getCountryCode());
                            //country_iso = country.getCountryISO();
                        }
                    });
                    _searchableListDialog.show((SignUpActivity.this).getFragmentManager(), "TAG");
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }


    private void userRegistration() {
        try {
            WEBSERVICE_FLAG = 1;
            reg_email_id = edtEmailId.getText().toString().trim();
            reg_password = edtPassword.getText().toString().trim();
            reg_mobile = edtContectNumber.getText().toString().trim();
            showProgressBar();
            //adding values in map
            Map<String, String> map = new HashMap<>();
            map.put("email_address", reg_email_id);
            map.put("contact_number", txtCountryCode.getText().toString().trim() + "-" + reg_mobile);
            map.put("password", reg_password);
            requestQueue = Volley.newRequestQueue(context);
            customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.SIGNUP, map, this, this);
            requestQueue.add(customVolleyRequest);
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.please_wait));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    private String getCountryCode() {
        TelephonyManager tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        return countryCodeValue;
    }

    public boolean validateForm(String mobile, String email, String password, String cPassword) {
        boolean valid = true;
        try {
            edtContectNumber.setError(null);
            edtEmailId.setError(null);
            edtPassword.setError(null);
            cnfrmPassword.setError(null);
            edtContectNumber.setError(null);

            String EMAIL_PATTERN = AppConstants.REGEX_EMAIL;
            String PHONE = AppConstants.REGEX_PHONE_NUMBER;
            if (mobile.length() == 0) {
                setErrorMsg("Enter Mobile No.", edtContectNumber, true);
                valid = false;
            } else if (!mobile.matches(PHONE)) {
                setErrorMsg("Enter Valid Mobile No.", edtContectNumber, true);
                valid = false;
            } else if (email.length() == 0) {
                setErrorMsg("Enter Email ID.", edtEmailId, true);
                valid = false;
            } else if (!email.matches(EMAIL_PATTERN)) {
                setErrorMsg(getResources().getString(R.string.valid_email), edtEmailId, true);
                valid = false;
            } else if (password.length() == 0) {
                setErrorMsg("Enter Password.", edtPassword, true);
                valid = false;
            } else if (edtPassword.getText().toString().contains(" ")) {
                setErrorMsg("Spaces are not allowed.", edtPassword, true);
                valid = false;
            } else if (password.length() < 8) {
                setErrorMsg("Password must be at least 8 characters long.", edtPassword, true);
                valid = false;
            } else if (!password.trim().equals(cPassword.trim())) {
                setErrorMsg("Password & Confirm Password do not match.", cnfrmPassword, true);
                valid = false;
            }
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return valid;
    }

    private void blankEdittext() {
        edtEmailId.setText("");
        edtPassword.setText("");
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("", "## onErrorResponse : " + error.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            error.printStackTrace();
            String errorMessage = getString(R.string.error_connection_generic);
            if (error instanceof NoConnectionError)
                errorMessage = getString(R.string.error_no_connection);
            else if (error instanceof AuthFailureError)
                errorMessage = getString(R.string.error_authentication);
            else if (error instanceof NetworkError)
                errorMessage = getString(R.string.error_network_error);
            else if (error instanceof ParseError)
                errorMessage = getString(R.string.error_parse_error);
            else if (error instanceof ServerError)
                errorMessage = getString(R.string.error_server_error);
            else if (error instanceof TimeoutError)
                errorMessage = getString(R.string.error_connection_timeout);
            new CommonDialogs().showMessageDialogNonStatic(SignUpActivity.this, getResources().getString(R.string.ongravity), errorMessage);
        } catch (Exception e) {
            Log.e("", "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        if (WEBSERVICE_FLAG == 1) {
            JSONObject jObject;
            try {
                jObject = new JSONObject(String.valueOf(response));

                status = jObject.getString("status");
                message = jObject.getString("msg");
                if (status.equals("success")) {

                    AppSettings.setProfileDefaultCategory("1");
                    user_id = jObject.getString("userid");
                    Toast.makeText(this, jObject.getString("otp"), Toast.LENGTH_SHORT).show();
                    AppSettings.setLoginUserId(user_id);
                    startService(new Intent(getBaseContext(), LocationService.class));
                    final Dialog dialog = CommonDialogs.showMessageDialog(SignUpActivity.this, getResources().getString(R.string.ongravity), message);


                    dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            Intent optEntryIntent = new Intent(SignUpActivity.this, OTPEntryView.class);
                            optEntryIntent.putExtra("contact_number", edtContectNumber.getText().toString());

                            LogUtil.writeDebugLog(TAG, "signUp", "start");
                            String login = edtContectNumber.getText().toString().trim();
//                          String name = m_txtFullName.getText().toString().trim();
                            String password = edtPassword.getText().toString();
                            String email = edtEmailId.getText().toString();
                            String phoneNumber = edtContectNumber.getText().toString();
//                          String country = m_txtCountry.getText().toString();
                            optEntryIntent.putExtra("login", login);
                            optEntryIntent.putExtra("password", password);
                            optEntryIntent.putExtra("email", email);
                            optEntryIntent.putExtra("phoneNumber", phoneNumber);

                            blankEdittext();
                            startActivity(optEntryIntent);
                        }
                    });

                } else if (status.equalsIgnoreCase("error")) {
                    CommonDialogs.showMessageDialog(SignUpActivity.this, getResources().getString(R.string.ongravity), message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (WEBSERVICE_FLAG == 2) {
            JSONObject jObject;
            try {
                jObject = new JSONObject(String.valueOf(response));
                status = jObject.getString("result");
                message = jObject.getString("msg");
                if (Integer.parseInt(status) == 1) {
                    blankEdittext();
                    finish();
                } else if (Integer.parseInt(status) == 0) {
                    CommonDialogs.showMessageDialog(SignUpActivity.this, getResources().getString(R.string.ongravity), message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        try {
            View v = getCurrentFocus();

            if (v != null &&
                    (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                    v instanceof EditText &&
                    !v.getClass().getName().startsWith("")) {
                int scrcoords[] = new int[2];
                v.getLocationOnScreen(scrcoords);
                float x = ev.getRawX() + v.getLeft() - scrcoords[0];
                float y = ev.getRawY() + v.getTop() - scrcoords[1];

                if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                    AppUtils.hideKeyboard(this);
            }
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {
        if (view == btnLoginWithFB) {
            btnLoginWithFB.setEnabled(false);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    btnLoginWithFB.setEnabled(true);
                }
            }, TIME);
//            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList());
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_friends", "email", "public_profile"));
        }
    }

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {

        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<Country>(233);
            BufferedReader reader = null;
            TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    final Country c = new Country(mContext, line, i);
                    data.add(c);
                    if (c.getCountryISO().toLowerCase().trim().equals(countryCodeValue.toLowerCase().trim())) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                txtCountryCode.setText(String.valueOf(c.getCountryCode()));
                            }
                        });
                    }
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            countryList = data;
        }
    }

    private void setUiPageViewController() {
        try {
            dotsCount = mAdapter.getCount();
            dots = new ImageView[dotsCount];

            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.bullet_d));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(4, 0, 4, 0);

                linearCountDots.addView(dots[i], params);
            }

            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.bullet_a));
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private String ID = "id";
    private String NAME = "name";
    private String PICTURE = "picture";
    private String EMAIL = "email";
    private String BIRTHDAY = "birthday";
    private String GENDER = "gender";
    private String KEY_USERNAME = "email_address";
    private String KEY_PASSWORD = "password";
    private String flag, mobile, password, status, user_id, message, imei_number, profile_status, facebook_id, name, profile_picure, email, birthdate, gender, contact_number;
    private JSONObject json;
    private String REQUEST_FIELDS = TextUtils.join(",", new String[]{ID, NAME, PICTURE, EMAIL, BIRTHDAY, GENDER});
    private String FIELDS = "fields";

    // get the data of user when pressed login with facebook
    private void requestUserInfo(AccessToken accessToken) {
        try {
            GraphRequest request = GraphRequest.newMeRequest(accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object,
                                                GraphResponse response) {
                            // TODO Auto-generated method stub
                            json = response.getJSONObject();
                            try {
                                Log.e("h", json.toString());
                                profile_picure = json.getJSONObject("picture").getJSONObject("data").getString("url");
                                name = json.getString("name");
                                gender = json.getString("gender");
                                facebook_id = json.getString("id");
                                Log.e("name", name);
                                Log.e("facebook_id", facebook_id);
                                Log.e("profile_picure", profile_picure);
                                if (json.getString("email") != null && !json.getString("email").equals("") && !json.getString("email").equals(" ")) {
                                    try {
                                        email = json.getString("email");
                                        imei_number = "";
                                        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                                        String regId = pref.getString("regId", null);

//                                        doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg);

                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }

                                imei_number = "";
                                SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                                String regId = pref.getString("regId", null);

//                                doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg);

                            } catch (JSONException e) {
                                if (AppSettings.isFBLogin()) {
                                    email = AppSettings.getFBEmail();
                                    contact_number = AppSettings.getKeyUserProfileContactNumber();
                                    //if get all fields from facebook then user will login to directly application
                                    imei_number = "";
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                                    String regId = pref.getString("regId", null);
//                                    doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg);
                                } else {
                                    //if facebook will not provide users email for that time we will ask for enter email manually
                                    Intent intent = new Intent(SignUpActivity.this, GetUserEmail.class);
                                    intent.putExtra("name", name);
                                    intent.putExtra("profile_pic_url", profile_picure);
                                    intent.putExtra("gender", gender);
                                    intent.putExtra("facebook_user_id", facebook_id);
                                    intent.putExtra("email", email);
                                    startActivity(intent);
                                }
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString(FIELDS, REQUEST_FIELDS);
            request.setParameters(parameters);
            request.executeAsync();
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private String device_confirm_msg = "yes";

   /* private void doLoginFB(String emailId, String user_name, String profile_pic_url, String contactNumber, String facebook_user_id, String imeiNumber, String device_token_id, String deviceConfirm_msg) {
        if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        try {

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<LoginResponse> fbLoginCall = requestInterface.fbLogin(emailId, user_name, profile_pic_url, contactNumber, facebook_user_id, device_token_id, deviceConfirm_msg, "android");
            fbLoginCall.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    try {
                        device_confirm_msg = "yes";
                        Log.e(TAG, response + " ");
                        status = response.body().getStatus();
                        message = response.body().getMsg();
                        if (response.body().getStatus().equalsIgnoreCase(AppConstants.RESPONSE_SUCCESS)) {
                            if (response.body().getMsg().equalsIgnoreCase("You have successfully login.")) {
                                blankEdittext();
                                Log.e(LOG_TAG, "--- User Id : " + user_id);
                                if (AppSettings.getRegistrationStatus().equals("yes")) {
                                    AppSettings.setUserProfileName(user_name);
                                    AppSettings.setProfileCatPicPathFB(profile_pic_url);
                                    AppSettings.setProfileDefaultCategory(response.body().getUser().getProfile_cat_id());
                                    AppSettings.setLoginUserId(response.body().getUser().getId());
                                    AppSettings.setUserProfileEmail(response.body().getUser().getEmail_address());
                                    AppSettings.setKeyUserProfileContactNumber(response.body().getUser().getFull_contact_number());
                                    AppSettings.setLogin(true);
                                    AppSettings.setFBUserId(response.body().getUser().getEmail_address());
                                    AppSettings.setFBLogin(true);
                                    startActivity(new Intent(SignUpActivity.this, EditProfileDetailsActivity.class));
                                    finish();

                                } else {
                                    AppSettings.setUserProfileName(response.body().getUser().getC_name());
                                    AppSettings.setUserProfileName(response.body().getUser().getC_name());
                                    if (response.body().getUser().getProfile_picture() != null || !response.body().getUser().getProfile_picture().equals("")) {
                                        AppSettings.setProfileCatPicPathOne(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getUser().getProfile_picture());
                                    } else {
                                        AppSettings.setProfileCatPicPathFB(profile_pic_url);
                                    }
                                    AppSettings.setProfileDefaultCategory(response.body().getUser().getProfile_cat_id());
                                    AppSettings.setLoginUserId(response.body().getUser().getId());
                                    AppSettings.setUserProfileEmail(response.body().getUser().getEmail_address());
                                    AppSettings.setKeyUserProfileContactNumber(response.body().getUser().getFull_contact_number());
                                    AppSettings.setLogin(true);
                                    AppSettings.setFBUserId(response.body().getUser().getEmail_address());
                                    AppSettings.setFBLogin(true);
                                    startActivity(new Intent(SignUpActivity.this, HomeUserPostsActivity.class));
                                    finish();
                                }

                            } else if (response.body().getMsg().equalsIgnoreCase("Different mobile device exists.")) {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                                final String regId = pref.getString("regId", null);
                                device_confirm_msg = "Yes";

                                doLoginFB(email, name, profile_picure, contact_number, facebook_id, imei_number, regId, device_confirm_msg);
                            }


                        } else if (response.body().getStatus().equalsIgnoreCase("error")) {

                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();

                            //if account is inactive flag will 0 ==== opening pin view
                            flag = response.body().getFlag();
                            if (Integer.parseInt(flag) == 0) {
                                Intent pinIntent = new Intent(SignUpActivity.this, OTPEntryView.class);
                                pinIntent.putExtra("contact_number", edtContectNumber.getText().toString());
                                blankEdittext();
                                startActivity(pinIntent);
                            } else {
                                final Dialog dialog = CommonDialogs.showMessageDialog(SignUpActivity.this, getResources().getString(R.string.ongravity), response.body().getMsg());
                                dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }*/
}
