package com.exceptionaire.ongraviti.activities.chats;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.myorbit.AddPeopleToOrbitActivity;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.OrbitFriend;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by root on 06/02/17.
 */

public class PeopleForChatFragment extends Fragment {

    private ProgressDialog progressDialog;
    private ArrayList<OrbitFriend> orbitFriends;
    private PeopleForChatAdapter orbitAdapter;
    private ListView list_contact;
    private TextView tv_msg;
    private FrameLayout frame_search;

    public PeopleForChatFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orbitAdapter = new PeopleForChatAdapter(new ArrayList<OrbitFriend>(), getActivity(), PeopleForChatFragment.this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_people_in_orbit, container, false);
        final EditText et_search = (EditText) rootView.findViewById(R.id.et_search);
        tv_msg = (TextView) rootView.findViewById(R.id.tv_msg);
        list_contact = (ListView) rootView.findViewById(R.id.list_view_contact);
        frame_search = (FrameLayout) rootView.findViewById(R.id.frame_search);

        ImageButton ibtn_add_friend = (ImageButton) rootView.findViewById(R.id.button_add_friend);
        ibtn_add_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddPeopleToOrbitActivity.class));
                getActivity().finish();
            }
        });
        list_contact.setAdapter(orbitAdapter);
        showOrbitFriendList();

        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                String text = et_search.getText().toString().toLowerCase(Locale.getDefault());
                orbitAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            }
        });

        Button btn_clear_search = (Button) rootView.findViewById(R.id.btn_clear_search);
        btn_clear_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_search.setText("");
            }
        });

        return rootView;

    }

    private void showOrbitFriendList() {
        progressDialog = new CustomProgressDialog(getActivity(), "Loading...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/get_user_orbit",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        System.out.println("get_user_orbit :Response " + response);
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);

                            // message = jObject.getString("msg");
                            String status = jObject.getString("status");
                            if (status.equals("success")) {
                                Object obj = jObject.get("orbit_list");
                                if (obj instanceof JSONArray) {
                                    JSONArray requestLists = jObject.getJSONArray("orbit_list");
                                    orbitFriends = new ArrayList<OrbitFriend>();
                                    OrbitFriend request = null;
                                    for (int i = 0; i < requestLists.length(); i++) {
                                        Log.d("requestLists count", "" + i);

                                        JSONObject detailsObject = requestLists.getJSONObject(i);
                                        request = new OrbitFriend();

                                        request.setUserName(detailsObject.getString("orbit_name"));
                                        request.setUserID(detailsObject.getString("orbit_id"));
                                        request.setProfilePic(detailsObject.getString("profile_picture"));

                                        orbitFriends.add(request);
                                    }

                                    if (requestLists.length() > 0) {
                                        frame_search.setVisibility(View.VISIBLE);
                                        orbitAdapter.setOrbitFriendList(orbitFriends);
                                        tv_msg.setVisibility(View.GONE);
                                        list_contact.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    tv_msg.setVisibility(View.VISIBLE);
                                    list_contact.setVisibility(View.GONE);
                                }

                                progressDialog.dismiss();

                            } else if (status.equalsIgnoreCase("error")) {
                                //CommonDialogs.showMessageDialog(getActivity(), "Error", message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("", "## onErrorResponse : " + error.toString());
                        try {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();

                            error.printStackTrace();
                            String errorMessage = getString(R.string.error_connection_generic);
                            if (error instanceof NoConnectionError)
                                errorMessage = getString(R.string.error_no_connection);
                            else if (error instanceof AuthFailureError)
                                errorMessage = getString(R.string.error_authentication);
                            else if (error instanceof NetworkError)
                                errorMessage = getString(R.string.error_network_error);
                            else if (error instanceof ParseError)
                                errorMessage = getString(R.string.error_parse_error);
                            else if (error instanceof ServerError)
                                errorMessage = getString(R.string.error_server_error);
                            else if (error instanceof TimeoutError)
                                errorMessage = getString(R.string.error_connection_timeout);
                            CommonDialogs.showMessageDialog(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.check_internet)).show();
                            /* Intent intentHome=new Intent(getActivity(),HomeUserPostsActivity.class);
                            startActivity(intentHome);*/
                        } catch (Exception e) {
                            Log.e("", "## error : " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", AppSettings.getLoginUserId());
                System.out.println(map);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    public void removeFromOrbit(final String friendUserId, final int position, final String flag_type) {
        progressDialog = new CustomProgressDialog(getActivity(), "Loading...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/set_block_delete_to_orbit",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response=============", "" + response);
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            String status = jObject.getString("status");
                            String message = jObject.getString("msg");

                            if (status.equals("success")) {

                                final Dialog dialog = CommonDialogs.showRemoveMessageDialog(getActivity(), getResources().getString(R.string.ongravity), message);
                                Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                                dialog_ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                        orbitAdapter.removeItem(position);
                                    }
                                });

                            } else if (status.equalsIgnoreCase("error")) {
                                //  CommonDialogs.showMessageDialog((Activity) context, "Error", message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();

                map.put("user_id", AppSettings.getLoginUserId());
                map.put("sender_user_id", friendUserId);
                map.put("flag_type", flag_type);

                System.out.println(map);
                return map;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

}
