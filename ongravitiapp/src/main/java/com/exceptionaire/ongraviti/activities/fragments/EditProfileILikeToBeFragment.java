package com.exceptionaire.ongraviti.activities.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.EditProfileDetailsActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseSetUserInfo;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_ERROR;
import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_SUCCESS;


public class EditProfileILikeToBeFragment extends android.support.v4.app.Fragment {

    private Button buttonSave, buttonPrevious, buttonNext;
    private View view;
    private ProgressDialog progressDialog;
    Spinner spinner_what_i_read, spinner_industry_i_want_to_target, spinner_events_attended;
    EditText edit_text_events_looking_for, edit_text_area_of_interest, edit_text_entrepreneurs_i_like_to_meet, edit_text_venture_to_look_for;
    private ResponseGetUserProfileData userProfileDataFromResponse;

    String[] arr_events_attended = {"Select"};
    String[] arr_area_of_interest = {"Select"};
    String[] arr_entrepreneurs = {"Select"};
    String[] arr_events_m_looking_forward = {"Select"};
    String[] arr_industries_i_want = {"Select", "Advertising Industry", "Agriculture Industry", "Automobile Industry", "Banking Industry", "Biotechnology Industry", "Chocolate Industry", "Construction Industry", "Dairy Industry", "Electric Industry", "Fashion Industry", "Film Industry", "IT Industry", "Music Industry"};
    String[] arr_whatIRead = {"Select", "Science fiction", "Drama", "Romance", "Mystery", "Horror", "Self help", "Action and Adventure"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_edit_profile_i_like_to_be, container, false);
        setActionBar();
        initializeViewComponent();
        setData();
        return view;
    }


    private void initializeViewComponent() {
        AppSettings.setProfileStepStatus4(1);

        spinner_industry_i_want_to_target = (Spinner) view.findViewById(R.id.spinner_industry_i_want_to_target);
        edit_text_events_looking_for = (EditText) view.findViewById(R.id.edit_text_events_looking_for);
        spinner_events_attended = (Spinner) view.findViewById(R.id.spinner_events_attended);
        edit_text_entrepreneurs_i_like_to_meet = (EditText) view.findViewById(R.id.edit_text_entrepreneurs_i_like_to_meet);
        spinner_what_i_read = (Spinner) view.findViewById(R.id.spinner_what_i_read);
        edit_text_area_of_interest = (EditText) view.findViewById(R.id.edit_text_are_of_interest);
        edit_text_venture_to_look_for = (EditText) view.findViewById(R.id.edit_text_venture_to_look_for);

        fillDataInSpinner(spinner_industry_i_want_to_target, arr_industries_i_want);
        fillDataInSpinner(spinner_what_i_read, arr_whatIRead);
        //  fillDataInSpinner(spinner_area_of_interest, arr_area_of_interest);
        //  fillDataInSpinner(spinner_events_attended, arr_events_attended);
        // fillDataInSpinner(spinner_entrepreneurs_i_like_to_meet, arr_entrepreneurs);
        // fillDataInSpinner(spinner_events_looking_for, arr_events_m_looking_forward);


        buttonSave = view.findViewById(R.id.button_save);
        buttonNext = view.findViewById(R.id.button_next);
        buttonPrevious = view.findViewById(R.id.button_prev);

        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new EditProfileMyLivingFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_edit_profile_details, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();

            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValid()) {
                    AppSettings.setProfileStepStatus4(2);
                }

                saveDataInSharedPref();


            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new EditProfileMyInterestsFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_edit_profile_details, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }
        });
    }

    private boolean isValid() {

        if (edit_text_area_of_interest.getText().toString().trim() != null &&
                edit_text_area_of_interest.getText().toString().trim().length() == 0) {
            return false;

        } else if (edit_text_venture_to_look_for.getText().toString().trim() != null &&
                edit_text_venture_to_look_for.getText().toString().trim().length() == 0) {
            return false;

        } else if (edit_text_entrepreneurs_i_like_to_meet.getText().toString().trim() != null &&
                edit_text_entrepreneurs_i_like_to_meet.getText().toString().trim().length() == 0) {
            return false;

        } else if (edit_text_events_looking_for.getText().toString().trim() != null &&
                edit_text_events_looking_for.getText().toString().trim().length() == 0) {
            return false;

        } else if (spinner_what_i_read.getSelectedItem().toString() != null &&
                spinner_what_i_read.getSelectedItem().toString().equals("Select")) {
            return false;

        } else if (spinner_industry_i_want_to_target.getSelectedItem().toString() != null &&
                spinner_industry_i_want_to_target.getSelectedItem().toString().equals("Select")) {
            return false;

        } else {
            return true;
        }
    }

    //Set custom action bar
    private void setActionBar() {
    }

    private void showProgressBar() {
        try {
            progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveDataInSharedPref() {
        String data = AppSettings.getProfileDetails();
        Gson gson = new Gson();
        ResponseGetUserProfileData responseGetUserProfileData = gson.fromJson(data, ResponseGetUserProfileData.class);

        if (responseGetUserProfileData != null) {
            UserBasicInfo userBasicInfo = responseGetUserProfileData.getUser_basic_info()[0];
            if (userBasicInfo != null) {
                if (edit_text_area_of_interest.getText().toString().trim() != null) {
                    userBasicInfo.setArea_of_interest(edit_text_area_of_interest.getText().toString().trim());
                } else {
                    userBasicInfo.setArea_of_interest("");
                }
                if (edit_text_venture_to_look_for.getText().toString().trim() != null) {
                    userBasicInfo.setInteresting_ventures_to_look(edit_text_venture_to_look_for.getText().toString().trim());
                } else {
                    userBasicInfo.setInteresting_ventures_to_look("");
                }
                if (edit_text_entrepreneurs_i_like_to_meet.getText().toString().trim() != null) {
                    userBasicInfo.setEnterpreneurs_i_like(edit_text_entrepreneurs_i_like_to_meet.getText().toString().trim());
                } else {
                    userBasicInfo.setEnterpreneurs_i_like("");
                }
                if (edit_text_events_looking_for.getText().toString().trim() != null) {
                    userBasicInfo.setEvents_m_looking(edit_text_events_looking_for.getText().toString().trim());
                } else {
                    userBasicInfo.setEvents_m_looking("");
                }

                if (spinner_what_i_read.getSelectedItem().toString() != null &&
                        !spinner_what_i_read.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setwhat_i_read(spinner_what_i_read.getSelectedItem().toString());
                } else {
                    userBasicInfo.setwhat_i_read("");
                }

                if (spinner_industry_i_want_to_target.getSelectedItem().toString() != null &&
                        !spinner_industry_i_want_to_target.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setIndustries_i_want_to_target(spinner_industry_i_want_to_target.getSelectedItem().toString());
                } else {
                    userBasicInfo.setIndustries_i_want_to_target("");
                }
                String json = gson.toJson(responseGetUserProfileData);
                AppSettings.setProfileDetails(json);
            }
        }

        showProgressBar();

        String whatiread;
        if (spinner_what_i_read.getSelectedItem().toString().equals("Select")) {
            whatiread = "NA";
        } else {
            whatiread = spinner_what_i_read.getSelectedItem().toString();
        }
        String industry_i_want_to_target;
        if (spinner_industry_i_want_to_target.getSelectedItem().toString().equals("Select")) {
            industry_i_want_to_target = "NA";
        } else {
            industry_i_want_to_target = spinner_industry_i_want_to_target.getSelectedItem().toString();
        }
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
        Call<ResponseSetUserInfo> loginCall = getProfileRequest.setUserProfileData4(
                AppSettings.getLoginUserId(),


                edit_text_area_of_interest.getText().toString().trim(),
                edit_text_venture_to_look_for.getText().toString().trim(),
                edit_text_entrepreneurs_i_like_to_meet.getText().toString().trim(),
                "",
                edit_text_events_looking_for.getText().toString().trim(),
                industry_i_want_to_target,
                "",
                whatiread
        );

        loginCall.enqueue(new Callback<ResponseSetUserInfo>() {
            @Override
            public void onResponse(Call<ResponseSetUserInfo> call, retrofit2.Response<ResponseSetUserInfo> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Fragment fragment = new EditProfileMyInterestsFragment();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.container_edit_profile_details, fragment);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        ft.commit();
                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), response.body().getMsg());
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseSetUserInfo> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    private void setData() {

        userProfileDataFromResponse = ((EditProfileDetailsActivity) getActivity()).getUserProfileDataFromResponse();
        if (userProfileDataFromResponse != null) {

            UserBasicInfo basic_user_info = userProfileDataFromResponse.getUser_basic_info()[0];

            edit_text_area_of_interest.setText(basic_user_info.getArea_of_interest());
            edit_text_venture_to_look_for.setText(basic_user_info.getInteresting_ventures_to_look());
            edit_text_entrepreneurs_i_like_to_meet.setText(basic_user_info.getEnterpreneurs_i_like());
            edit_text_events_looking_for.setText(basic_user_info.getEvents_m_looking());

            if (arr_whatIRead.length > 0) {
                for (int index = 0; index < arr_whatIRead.length; index++) {
                    if (basic_user_info.getwhat_i_read() != null)
                        if (basic_user_info.getwhat_i_read().equals(arr_whatIRead[index])) {
                            spinner_what_i_read.setSelection(index);
                            break;
                        }
                }
            }

            if (arr_industries_i_want.length > 0) {
                for (int index = 0; index < arr_industries_i_want.length; index++) {
                    if (basic_user_info.getIndustries_i_want_to_target() != null)
                        if (basic_user_info.getIndustries_i_want_to_target().equals(arr_industries_i_want[index])) {
                            spinner_industry_i_want_to_target.setSelection(index);
                            break;
                        }
                }
            }
        }
    }

    public void fillDataInSpinner(Spinner spinner, final String[] category) {
        if (getActivity() != null)
            Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## fillDataInSpinner");
        try {

            ArrayAdapter<String> aa = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, category);

            aa.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(aa);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {

//                    Toast.makeText(context, "onItemSelected : " + category[position] + "position : " + position, Toast.LENGTH_SHORT).show();
                    } else {
                        if (getActivity() != null)
                            Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## onItemSelected : " + category[position]);
                        //Toast.makeText(context, "onItemSelected : " + category[position], Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            if (getActivity() != null)
                Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }
}