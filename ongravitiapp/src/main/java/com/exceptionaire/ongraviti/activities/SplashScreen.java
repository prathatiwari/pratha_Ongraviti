package com.exceptionaire.ongraviti.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.chats.ChatsMainActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.core.OnGravitiApp;
import com.exceptionaire.ongraviti.fcm.Config;
import com.exceptionaire.ongraviti.fcm.NotificationUtils;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_activities.OpponentsActivityVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_service.CallServiceVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.SharedPrefsHelperVideo;
import com.exceptionaire.ongraviti.quickblox.QBData;
import com.exceptionaire.ongraviti.quickblox.core.utils.SharedPrefsHelper;
import com.exceptionaire.ongraviti.quickblox.core.utils.Toaster;
import com.exceptionaire.ongraviti.quickblox.services.CallService;
import com.exceptionaire.ongraviti.quickblox.utils.chat.ChatHelper;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.session.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bhakti on 18/7/16.
 */
public class SplashScreen extends BaseActivity implements AppConstants {
    private static int SPLASH_TIME_OUT = 100;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;

    /*QB Code*/
    private ProgressDialog progressDialog;
    private QBUser mQBUser;
    private Intent intent;

    private SharedPrefsHelperVideo sharedPrefsHelperVideo;

    private static final String TAG = SplashScreen.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.exceptionaire.ongraviti",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        sampleConfigIsCorrect();

        sharedPrefsHelperVideo = SharedPrefsHelperVideo.getInstance();
        getrateKeyHash();

        if (Build.VERSION.SDK_INT >= 23) {
            askMultipleConditions();
        }
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.splash);

        /*QB Code*/
        if (isNetworkAvailable()) {

            fcmRegistration();
            createSession();
            if (sharedPrefsHelper.hasQbUser()) {
                startLoginServiceVideo(sharedPrefsHelper.getQbUser());
            }
        } else {
            dialog = new CommonDialogs().showMessageDialogNonStatic(this, getResources().getString(R.string.app_name), getResources().getString(R.string.check_internet));
            dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    finish();
                }
            });
        }

    }

    private void fcmRegistration() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };
        displayFirebaseRegId();
    }

    private Dialog dialog = null;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void askMultipleConditions() {
        List<String> permissionsNeeded = new ArrayList<>();

        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("Network State");
        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
            permissionsNeeded.add("Read SMS");
        if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
            permissionsNeeded.add("Receive SMS");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("Internet");
        if (!addPermission(permissionsList, Manifest.permission.WAKE_LOCK))
            permissionsNeeded.add("Wake Lock");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                    /*showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });*/
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_NETWORK_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.INTERNET, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WAKE_LOCK, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WAKE_LOCK) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted

                } /*else if(perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED)
                {
                    // Permission Denied
                    String message = "You need to grant access to " + Manifest.permission.ACCESS_FINE_LOCATION;
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_FINE_LOCATION);
                                }
                            });
                }
                else if(perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED)
                {
                    // Permission Denied
                    String message = "You need to grant access to " + Manifest.permission.ACCESS_COARSE_LOCATION;
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_COARSE_LOCATION);
                                }
                            });
                }
                else if(perms.get(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_DENIED)
                {
                    // Permission Denied
                    String message = "You need to grant access to " + Manifest.permission.ACCESS_NETWORK_STATE;
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_NETWORK_STATE);
                                }
                            });
                }
                else if(perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_DENIED)
                {
                    // Permission Denied
                    String message = "You need to grant access to " + Manifest.permission.READ_SMS;
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.READ_SMS);
                                }
                            });
                }
                else if(perms.get(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_DENIED)
                {
                    // Permission Denied
                    String message = "You need to grant access to " + Manifest.permission.RECEIVE_SMS;
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.RECEIVE_SMS);
                                }
                            });
                }
                else if(perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)
                {
                    // Permission Denied
                    String message = "You need to grant access to " + Manifest.permission.CAMERA;
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.CAMERA);
                                }
                            });
                }
                else if(perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
                {
                    // Permission Denied
                    String message = "You need to grant access to " + Manifest.permission.WRITE_EXTERNAL_STORAGE;
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                                }
                            });
                }else if(perms.get(Manifest.permission.WAKE_LOCK) == PackageManager.PERMISSION_DENIED)
                {
                    // Permission Denied
                    String message = "You need to grant access to " + Manifest.permission.WAKE_LOCK;
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WAKE_LOCK);
                                }
                            });
                }
                else if(perms.get(Manifest.permission.INTERNET) == PackageManager.PERMISSION_DENIED)
                {
                    // Permission Denied
                    String message = "You need to grant access to " + Manifest.permission.INTERNET;
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int result = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.INTERNET);
                                }
                            });
                }*/
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    /*@Override
    public void onBackPressed() {
        Intent i = new Intent(this, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }*/


    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    private void createSession() {

//        showProgressBar();
        QBChatService chatService = QBChatService.getInstance();
        chatService.setDebugEnabled(true);

        QBChatService.ConfigurationBuilder chatserviceConfigurationBuilder = new QBChatService.ConfigurationBuilder();
        chatserviceConfigurationBuilder.setSocketTimeout(100000);
        QBChatService.setConfigurationBuilder(chatserviceConfigurationBuilder);

        QBAuth.createSession().performAsync(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession result, Bundle params) {
                List<String> tags = new ArrayList<>();
                tags.add(AppConstants.QB_USERS_TAG);
                QBPagedRequestBuilder pagedRequestBuilder = new QBPagedRequestBuilder();
                pagedRequestBuilder.setPage(1);
                pagedRequestBuilder.setPerPage(100);//you can retrieve max 100 users per query.

                QBUsers.getUsersByTags(tags, pagedRequestBuilder).performAsync(new QBEntityCallback<ArrayList<QBUser>>() {
                    @Override
                    public void onSuccess(ArrayList<QBUser> result, Bundle params) {
                        Log.e("User_List", "" + result);
                        Log.e("User_List.params", "" + params);
                        QBData.qbUsers = result;
                        signIn();
                    }

                    @Override
                    public void onError(QBResponseException errors) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                        Toaster.longToast("Loading users error!");
                    }
                });
            }

            @Override
            public void onError(QBResponseException e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
                if (e.getErrors() != null && e.getErrors().size() > 0)
                    Toaster.longToast(e.getErrors().get(0));
            }
        });
    }

    public void signIn() {
//        mQBUser = getQBUserData();

        if(AppSettings.getLoginTypePatchForQuickEnter()!=null){

            if(AppSettings.getLoginTypePatchForQuickEnter().equals(AppConstants.LOGIN_TYPE_FOR_QUICK_NORMAL)){
                mQBUser = getQBUserData();
            }else if ((AppSettings.getLoginTypePatchForQuickEnter().equals(AppConstants.LOGIN_TYPE_FOR_QUICK_FB))){
                if(AppSettings.getFbQuickUserPatch()!=null) {
                    Gson gson = new Gson();
                    String json = AppSettings.getFbQuickUserPatch();
                    QBUser obj = gson.fromJson(json, QBUser.class);
                    mQBUser=obj;
                    AppSettings.setLogin(true);
                }
            }
        }


/*
        if(getQBUserData()!=null) {
            mQBUser = getQBUserData();
        }else {
            if(AppSettings.getFbQuickUserPatch()!=null) {
                Gson gson = new Gson();
                String json = AppSettings.getFbQuickUserPatch();
                QBUser obj = gson.fromJson(json, QBUser.class);
                mQBUser=obj;
            }
        }*/
//        Toast.makeText(this, ""+AppSettings.isLogin(), Toast.LENGTH_SHORT).show();

        if (mQBUser != null && AppSettings.isLogin()) {
            startLoginService(mQBUser);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();
                    intent = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    protected void startLoginService(QBUser qbUser) {
        Intent tempIntent = new Intent(this, CallService.class);
        PendingIntent pendingIntent = createPendingResult(AppConstants.EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
        CallService.start(this, qbUser, pendingIntent);
    }

    private void gotoLoginActivity() {
        AppSettings.setLogin(false);
        intent = new Intent(SplashScreen.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.EXTRA_LOGIN_RESULT_CODE) {
            boolean isLoginSuccess = data.getBooleanExtra(AppConstants.EXTRA_LOGIN_RESULT, false);
            String errorMessage = data.getStringExtra(AppConstants.EXTRA_LOGIN_ERROR_MESSAGE);
            if (isLoginSuccess) {
                saveQBUserData(mQBUser);
                QBUserSignIn(mQBUser);
            } else {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();

                Toaster.longToast(getString(R.string.login_chat_login_error) + errorMessage);
                gotoLoginActivity();
            }
        }
    }

    private void QBUserSignIn(QBUser user) {
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
               /* if (sharedPrefsHelper.hasQbUser()) {
                    startLoginServiceVideo(sharedPrefsHelper.getQbUser());
                }
*/
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();

                Intent intent = new Intent(SplashScreen.this, HomeUserPostsActivity.class);
//                Intent intent = new Intent(SplashScreen.this, OpponentsActivityVideo.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onError(QBResponseException e) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
                Toaster.longToast(e.getMessage());
                gotoLoginActivity();
            }
        });
    }

    private void saveQBUserData(QBUser qbUser) {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
        if (qbUser != null && qbUser.getTags() != null && qbUser.getTags().size() > 0) {
            sharedPrefsHelper.save(PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
            sharedPrefsHelper.saveQbUser(qbUser);
        }
    }

    protected boolean sampleConfigIsCorrect() {
        return OnGravitiApp.getInstance().getQbConfigs() != null;
    }


    public QBUser createUserWithEnteredData(String name, String email, String tagName) {
        return createQBUserWithCurrentData(String.valueOf(name),//user name
                String.valueOf(email),// user email id
                String.valueOf(tagName)); // group name
    }

    private QBUser createQBUserWithCurrentData(String userName, String email, String chatRoomName) {
        QBUser qbUser = null;
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(chatRoomName)) {
            StringifyArrayList<String> userTags = new StringifyArrayList<>();
            userTags.add(chatRoomName);

            qbUser = new QBUser();
            qbUser.setFullName(userName);
            qbUser.setEmail(email);
//            qbUser.setLogin(getCurrentDeviceId());
            qbUser.setPassword(DEFAULT_USER_PASSWORD);
            qbUser.setTags(userTags);
        }

        return qbUser;
    }

    protected void startLoginServiceVideo(QBUser qbUser) {

    /*    Intent tempIntent1 = new Intent(this, CallServiceVideo.class);
               *//* PendingIntent pendingIntent1 = createPendingResult(EXTRA_LOGIN_RESULT_CODE_VIDEO, tempIntent1, 0);*//*
         CallServiceVideo.start(this, qbUser);*/
        CallServiceVideo.start(this, qbUser);
    }

    /*************************************************/

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e("TAG", "Firebase reg id: " + regId);

//        if (!TextUtils.isEmpty(regId))
//            txtRegId.setText("Firebase Reg Id: " + regId);
//        else
//            txtRegId.setText("Firebase Reg Id is not received yet!");
    }
}
