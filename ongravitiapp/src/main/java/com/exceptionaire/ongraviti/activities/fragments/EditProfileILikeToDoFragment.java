package com.exceptionaire.ongraviti.activities.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.EditProfileDetailsActivity;
import com.exceptionaire.ongraviti.activities.adapters.HomeUserPostsNewAdapter;
import com.exceptionaire.ongraviti.activities.adapters.MyAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.SearchableListDialog;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.imoji.EmojiconEditText;
import com.exceptionaire.ongraviti.listener_interface.ClickListener;
import com.exceptionaire.ongraviti.model.Country;
import com.exceptionaire.ongraviti.model.PurchaseEventDetail;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseSetUserInfo;
import com.exceptionaire.ongraviti.model.StateVO;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.google.gson.Gson;

import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static android.content.Context.TELEPHONY_SERVICE;
import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_ERROR;
import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_SUCCESS;


public class EditProfileILikeToDoFragment extends Fragment implements View.OnClickListener {
    private Button buttonSave, buttonPrevious, buttonNext;
    private View view;
    ResponseGetUserProfileData userProfileDataFromResponse;
    private EditText editTextFavouriteBook, editTextFavouriteShows, editTextFavouriteMovie, editTextCountriesTravelled;
    private Spinner spinnerEventsAttended;
    String[] arr_events_attended = {"Select"};
    String[] arr_hobbies = new String[]{"Horse riding", "Reading", "Computer", "Watching Sports", "Painting", "Boating", "Golf", "Traveling"};
    private CustomProgressDialog progressDialog;
    public static EditText text_view_hobbies;
    private StringBuilder stringBuilder;
    ListView listview;
    public static ArrayList<StateVO> listVOs = new ArrayList<>();
    SparseBooleanArray sparseBooleanArray;
    ResponseGetUserProfileData responseGetUserProfileData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_edit_profile_i_like_to_do, container, false);
        setActionBar();
        initializeViewComponent();
        // initializeListeners();
        setData();
        //       getPurchasedEventList();

        return view;
    }


    //Set custom action bar
    private void setActionBar() {
    }

    private void initializeViewComponent() {
        AppSettings.setProfileStepStatus2(1);
        text_view_hobbies = view.findViewById(R.id.text_view_hobbies);
        editTextFavouriteBook = view.findViewById(R.id.edit_text_favourite_book);
        editTextFavouriteShows = view.findViewById(R.id.edit_text_favourite_shows);
        editTextFavouriteMovie = view.findViewById(R.id.edit_text_favourite_movie);
        editTextCountriesTravelled = view.findViewById(R.id.edit_text_countries_travelled);
        editTextCountriesTravelled.setOnClickListener(this);

//        fillDataInSpinner(spinnerHobbies, arr_hobbies);

        spinnerEventsAttended = view.findViewById(R.id.spinner_events_attended);
        fillDataInSpinner(spinnerEventsAttended, arr_events_attended);

        stringBuilder = new StringBuilder();


        buttonSave = view.findViewById(R.id.button_save);
        buttonPrevious = view.findViewById(R.id.button_prev);
        buttonNext = view.findViewById(R.id.button_next);
        listVOs.clear();
        for (int i = 0; i < arr_hobbies.length; i++) {
            StateVO stateVO = new StateVO();
            stateVO.setTitle(arr_hobbies[i]);
            stateVO.setSelected(false);
            listVOs.add(stateVO);

            Log.e("List_reload", "reloaddddd");
        }


        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new EditProfileWhoAmIFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_edit_profile_details, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();

            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValid()) {
                    AppSettings.setProfileStepStatus2(2);
                }

                saveDataInSharedPref();


            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new EditProfileMyLivingFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_edit_profile_details, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }
        });
//        text_view_hobbies.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Dialog dialogStatus = new CommonDialogs().dialogHobbies(getActivity());
//                listview = dialogStatus.findViewById(R.id.list_view);
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>
//                        (getActivity(),
//                                android.R.layout.simple_list_item_multiple_choice,
//                                android.R.id.text1, arr_hobbies);
//                listview.setAdapter(adapter);
//
////                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////                    @Override
////                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                        sparseBooleanArray = listview.getCheckedItemPositions();
////
////                        String ValueHolder = "";
////
////                        int i = 0;
////
////                        while (i < sparseBooleanArray.size()) {
////
////                            if (sparseBooleanArray.valueAt(i)) {
////
////                                ValueHolder += arr_hobbies[sparseBooleanArray.keyAt(i)] + ",";
////                            }
////
////                            i++;
////                        }
////
////                        ValueHolder = ValueHolder.replaceAll("(,)*$", "");
////                        AppSettings.setKeySelectedIds(sparseBooleanArray);
////                        Toast.makeText(getActivity(), "ListView Selected Values = " + AppSettings.getKeySelectedIds().size(), Toast.LENGTH_LONG).show();
////                        text_view_hobbies.setText(ValueHolder);
////
////
////                    }
////                });
//                dialogStatus.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        dialogStatus.dismiss();
//                    }
//                });
//                dialogStatus.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        dialogStatus.dismiss();
//                    }
//                });
//            }
//        });
    }

    private boolean isValid() {

        if (editTextFavouriteBook.getText().toString().trim() != null &&
                editTextFavouriteBook.getText().toString().trim().length() == 0) {
            return false;

        } else if (editTextFavouriteShows.getText().toString().trim() != null &&
                editTextFavouriteShows.getText().toString().trim().length() == 0) {
            return false;

        } else if (editTextFavouriteMovie.getText().toString().trim() != null &&
                editTextFavouriteMovie.getText().toString().trim().length() == 0) {
            return false;

        } else if (editTextCountriesTravelled.getText().toString().trim() != null &&
                editTextCountriesTravelled.getText().toString().trim().length() == 0) {
            return false;

        } else if (spinnerEventsAttended.getSelectedItem().toString() != null &&
                spinnerEventsAttended.getSelectedItem().toString().equals("Select")) {
            return false;

        } else {
            return true;
        }
    }

//    public static void getSelectedHobbies(String hobbies, boolean flagHobbies) {
//        if (stringBuilder1.toString().length() > 0) {
//            stringBuilder1.append(", ");
//        }
//        if (!stringBuilder1.toString().contains(hobbies)) {
//            stringBuilder1.append(hobbies);
//            text_view_hobbies.setText(stringBuilder1.toString());
//            if (flagHobbies) {
//                selectedHobbied.add(hobbies);
//                Log.d("hey", selectedHobbied.size() + "");
//                flagHobbies = false;
//            } else {
//                selectedHobbied.remove(hobbies);
//                Log.d("hey1", selectedHobbied.size() + "");
//            }
//
//        }
//
//
//    }

    private void showProgressBar() {
        try {
            progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveDataInSharedPref() {
        String data = AppSettings.getProfileDetails();
        Gson gson = new Gson();
        responseGetUserProfileData = gson.fromJson(data, ResponseGetUserProfileData.class);

        if (responseGetUserProfileData != null) {
            UserBasicInfo userBasicInfo = responseGetUserProfileData.getUser_basic_info()[0];
            if (userBasicInfo != null) {
                if (editTextFavouriteBook.getText().toString().trim() != null) {
                    userBasicInfo.setFavorite_book(editTextFavouriteBook.getText().toString().trim());
                } else {
                    userBasicInfo.setFavorite_book("");
                }
                if (editTextFavouriteShows.getText().toString().trim() != null) {
                    userBasicInfo.setFavorite_shows(editTextFavouriteShows.getText().toString().trim());
                } else {
                    userBasicInfo.setFavorite_shows("");
                }
                if (editTextFavouriteMovie.getText().toString().trim() != null) {
                    userBasicInfo.setFavorite_movies(editTextFavouriteMovie.getText().toString().trim());
                } else {
                    userBasicInfo.setFavorite_movies("");
                }
                if (editTextCountriesTravelled.getText().toString().trim() != null) {
                    userBasicInfo.setCountries_travelled(editTextCountriesTravelled.getText().toString().trim());
                } else {
                    userBasicInfo.setCountries_travelled("");
                }


                if (spinnerEventsAttended.getSelectedItem().toString() != null &&
                        !spinnerEventsAttended.getSelectedItem().toString().equals("Select")) {
                    userBasicInfo.setEvents_attended(spinnerEventsAttended.getSelectedItem().toString());
                } else {
                    userBasicInfo.setEvents_attended("");
                }

                if (text_view_hobbies.getText().toString() != null &&
                        !text_view_hobbies.getText().toString().equals("")) {
                    userBasicInfo.setHobbies(text_view_hobbies.getText().toString());

                } else {
                    userBasicInfo.setHobbies("");
                }
                String json = gson.toJson(responseGetUserProfileData);
                AppSettings.setProfileDetails(json);
            }
        }

        showProgressBar();
        String event;
        if (spinnerEventsAttended.getSelectedItem().toString().equals("Select")) {
            event = "NA";
        } else {
            event = spinnerEventsAttended.getSelectedItem().toString();
        }
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
        Call<ResponseSetUserInfo> loginCall = getProfileRequest.setUserProfileData2(
                AppSettings.getLoginUserId(),
                editTextFavouriteBook.getText().toString().trim(),
                editTextFavouriteShows.getText().toString().trim(),
                editTextFavouriteMovie.getText().toString().trim(),
                editTextCountriesTravelled.getText().toString().trim(),
                event,
                text_view_hobbies.getText().toString());

        loginCall.enqueue(new Callback<ResponseSetUserInfo>() {
            @Override
            public void onResponse(Call<ResponseSetUserInfo> call, retrofit2.Response<ResponseSetUserInfo> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Fragment fragment = new EditProfileMyLivingFragment();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.container_edit_profile_details, fragment);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        ft.commit();
                    } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), response.body().getMsg());
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseSetUserInfo> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }


    private void setData() {

        userProfileDataFromResponse = ((EditProfileDetailsActivity) getActivity()).getUserProfileDataFromResponse();
        if (userProfileDataFromResponse != null) {

            UserBasicInfo basic_user_info = userProfileDataFromResponse.getUser_basic_info()[0];
            if (basic_user_info != null) {
                editTextFavouriteBook.setText(basic_user_info.getFavorite_book());
                text_view_hobbies.setText(basic_user_info.getHobbies());

                editTextFavouriteShows.setText(basic_user_info.getFavorite_shows());
                editTextFavouriteMovie.setText(basic_user_info.getFavorite_movies());
                editTextCountriesTravelled.setText(basic_user_info.getCountries_travelled());
                // spinnerEventsAttended.setText(basic_user_info.getEvents_attended());
                stringBuilder.append(basic_user_info.getCountries_travelled());

//                if (arr_hobbies.length > 0) {
//                    for (int index = 0; index < arr_hobbies.length; index++) {
//                        if (basic_user_info.getHobbies().equals(arr_hobbies[index])) {
//                            listview.setSelection(index);
//                            break;
//                        }
//                    }
//                }
            }
            PurchaseEventDetail[] purchaseEvents = userProfileDataFromResponse.getPurchaseEventDetails();
            if (purchaseEvents != null && purchaseEvents.length > 0) {

                arr_events_attended = new String[purchaseEvents.length + 1];
                arr_events_attended[0] = "Select";
                for (int index = 0; index < purchaseEvents.length; index++) {

                    arr_events_attended[index + 1] = purchaseEvents[index].getTitle();
                }
                fillDataInSpinner(spinnerEventsAttended, arr_events_attended);
                if (arr_events_attended.length > 0) {
                    for (int index = 0; index < arr_events_attended.length; index++) {
                        if (basic_user_info.getEvents_attended() != null)
                        if (basic_user_info.getEvents_attended().equals(arr_events_attended[index])) {
                            spinnerEventsAttended.setSelection(index);
                            break;
                        }
                    }
                }
            }
//            text_view_hobbies.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Dialog dialogStatus = new CommonDialogs().dialogHobbies(getActivity());
//                    listview = dialogStatus.findViewById(R.id.list_view);
//                    if (userProfileDataFromResponse != null )
//                        if( userProfileDataFromResponse.gethobbies() != null) {
//                            for (int i = 0; i < listVOs.size(); i++) {
//                                for (int j = 0; j < userProfileDataFromResponse.gethobbies().size(); j++) {
//                                    if (listVOs.get(i).getTitle().equals(userProfileDataFromResponse.gethobbies().get(j))) {
//                                        listVOs.get(i).setSelected(true);
//                                        //responseGetUserProfileData.gethobbies().remove(j);
//                                    }
//                                }
//                            }
//                        }
//                   /* for (int i = 0; i < arr_hobbies.length; i++) {
//                        StateVO stateVO = new StateVO();
//                        stateVO.setTitle(arr_hobbies[i]);
//                        stateVO.setSelected(false);
//                        listVOs.add(stateVO);
//                    }*/
//                    //  if (userProfileDataFromResponse.gethobbies().size() > 0) {
//                    MyAdapter myAdapter = new MyAdapter(getActivity(), 0,
//                            listVOs, userProfileDataFromResponse.gethobbies());
//                    listview.setAdapter(myAdapter);
//                    // }
//                    dialogStatus.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
////                            text_view_hobbies.setText("");
//                            dialogStatus.dismiss();
//                        }
//                    });
//                    dialogStatus.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            dialogStatus.dismiss();
//                        }
//                    });
//                }
//            });
        }


    }

    public void fillDataInSpinner(Spinner spinner, final String[] category) {
        if (getActivity() != null)
            Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## fillDataInSpinner");
        try {

            ArrayAdapter<String> aa = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, category);

            aa.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(aa);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {

//                    Toast.makeText(context, "onItemSelected : " + category[position] + "position : " + position, Toast.LENGTH_SHORT).show();
                    } else {
                        if (getActivity() != null)
                            Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## onItemSelected : " + category[position]);
                        //Toast.makeText(context, "onItemSelected : " + category[position], Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            if (getActivity() != null)
                Log.d(((BaseDrawerActivity) getActivity()).LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit_text_countries_travelled:

                // Log.d(TAG, "## edt_contries_travelled clicked");
                if (countryList == null || countryList.isEmpty())
                    new AsyncPhoneInitTask(getActivity()).execute();
                else
                    _searchableListDialog.show((getActivity()).getFragmentManager(), "TAG");
                break;
        }
    }


    private List<Country> countryList;
    private SearchableListDialog _searchableListDialog;
    private SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<ArrayList<Country>>();

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {

        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<Country>(233);
            BufferedReader reader = null;
            TelephonyManager tm = (TelephonyManager) getActivity().getSystemService(TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    final Country c = new Country(mContext, line, i);
                    data.add(c);
                    /*if (c.getCountryISO().toLowerCase().trim().equals(countryCodeValue.toLowerCase().trim())) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                //txtCountryCode.setText(String.valueOf(c.getCountryCode()));
                            }
                        });

                    }*/
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            countryList = data;
            _searchableListDialog = SearchableListDialog.newInstance(countryList, new ClickListener() {
                @Override
                public void onClick(Country country) {
                    if (stringBuilder.toString().length() > 0) {
                        stringBuilder.append(", ");
                    }
                    if (!stringBuilder.toString().contains(country.getName())) {
                        stringBuilder.append(country.getName());
                        editTextCountriesTravelled.setText(stringBuilder.toString());
                    }
                    //country_code = String.valueOf(country.getCountryCode());
                    //country_iso = country.getCountryISO();
                }
            });
            _searchableListDialog.show((getActivity()).getFragmentManager(), "TAG");
        }
    }

}