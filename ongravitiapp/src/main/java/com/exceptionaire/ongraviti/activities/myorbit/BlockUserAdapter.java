package com.exceptionaire.ongraviti.activities.myorbit;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.BlockList;
import com.exceptionaire.ongraviti.model.MyOrbitRequest;
import com.exceptionaire.ongraviti.model.ResponseAcceptRejectOrbitRequest;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class BlockUserAdapter extends BaseAdapter {
    private List<BlockList> blockListList;
    Context context;
    private MyBlockUser fragment;

    public void setOrbitRequestList(List<BlockList> orbitRequests) {
        this.blockListList.clear();
        this.blockListList = orbitRequests;
        notifyDataSetChanged();
    }

    public BlockUserAdapter(List<BlockList> contactList, Context context, MyBlockUser fragment) {
        this.blockListList = contactList;
        this.context = context;
        this.fragment = fragment;
    }

    public void removeItem(int position) {
        blockListList.remove(position);
        notifyDataSetChanged();
    }

    private class Holder {
        TextView txtName, tv_mutual_friends;
        ImageButton ibtn_accept;
        RoundedImageView image;
        LinearLayout linear_view_profile;
        ImageView image_view_cat_type;
    }

    @Override
    public int getCount() {
        return blockListList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return blockListList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {

        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final BlockList blockList = blockListList.get(position);
        if (convertView == null) {
            holder = new BlockUserAdapter.Holder();
            convertView = layoutInflater.inflate(R.layout.block_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");

            holder.txtName = convertView.findViewById(R.id.txt_contact_name);
            holder.tv_mutual_friends = convertView.findViewById(R.id.textview_email_address);
            holder.ibtn_accept = convertView.findViewById(R.id.ibtn_accept);
            holder.image = convertView.findViewById(R.id.user_profile_pic);
            holder.linear_view_profile = convertView.findViewById(R.id.linear_view_profile);
            holder.image_view_cat_type = convertView.findViewById(R.id.image_view_cat_type);
            convertView.setTag(holder);

        } else {
            holder = (BlockUserAdapter.Holder) convertView.getTag();
        }
//        if (!orbitRequest.getProfile_picture().isEmpty()) {
//        Picasso.with(context)
//                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + blockList.getProfile_picture())
//                .placeholder(R.drawable.profile).error(R.drawable.profile)
//                .into(holder.image);
//        }

        if (blockList.getProfile_picture() != null && !blockList.getProfile_picture().isEmpty()) {
            if (!blockList.getProfile_picture().contains("https:")) {
                Picasso.with(context)
                        .load(AppConstants.PROFILE_PICTURE_PATH + "/" + blockList.getProfile_picture())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            } else if (blockList.getProfile_picture().contains("https:")) {
                Picasso.with(context)
                        .load(blockList.getProfile_picture())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            }
        }

        holder.txtName.setText(blockList.getC_name());
        switch (blockList.getProfile_cat_id()) {
            case "1":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_green);
                break;
            case "2":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_pink);
                break;
            case "3":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_blue);
                break;
        }

        /*if (orbitRequest.getOrbitFriendList() != null && !orbitRequest.getOrbitFriendList().isEmpty()) {
            holder.tv_mutual_friends.setText(orbitRequest.getOrbitFriendList().size() + " mutual friend(s)");
        }*/
        if (AppSettings.getLoginUserId().equals(blockList.getSender_user_id())) {
            holder.ibtn_accept.setOnClickListener(v -> fragment.acceptRejectOrbitRequest(blockList.getReceiver_user_id(), "unblock", position));
        } else if (AppSettings.getLoginUserId().equals(blockList.getReceiver_user_id())) {
            holder.ibtn_accept.setOnClickListener(v -> fragment.acceptRejectOrbitRequest(blockList.getSender_user_id(), "unblock", position));
        }
        //
//        holder.ibtn_reject.setOnClickListener(v -> fragment.acceptRejectOrbitRequest(orbitRequest.getUser_id(), "2", position, "Rejecting"));
//        holder.linear_view_profile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.d("adptr", "## image click : " + orbitRequest.getUser_id());
//                Intent intentProfile = new Intent(context, FavoriteUserInfoActivity.class);
//                intentProfile.putExtra("ActivityName", "OrbitRequest");
//                intentProfile.putExtra("OrbitUserRequestID", "" + orbitRequest.getUser_id());
//                context.startActivity(intentProfile);
//            }
//        });

        return convertView;
    }


}
