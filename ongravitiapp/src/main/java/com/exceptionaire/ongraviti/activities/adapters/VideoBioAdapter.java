package com.exceptionaire.ongraviti.activities.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.model.VideoPOJO;

import java.util.List;

/**
 * Created by root on 12/9/16.
 */
public class VideoBioAdapter extends BaseAdapter
{
    List<VideoPOJO> videoBioArrayList;
    Context mContext;
    VideoBioCustomButtonListner customListner;
    public VideoBioAdapter(Context mContext, List<VideoPOJO> videoBioArrayList ) {
        this.videoBioArrayList = videoBioArrayList;
        this.mContext = mContext;
    }
    public interface VideoBioCustomButtonListner {
        public void onButtonClickListner(int position);
    }
    public void setVideoBioCustomButtonListner(VideoBioCustomButtonListner listener) {
        this.customListner = listener;
    }

    @Override
    public int getCount()
    {
        return videoBioArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int postion, View convertView, ViewGroup viewGroup)
    {

        View view=convertView;
        Button edit;
        final int pos;
        final TextView question,answer,question_type,option;
        if(convertView==null)
        {
            LayoutInflater layoutInflater= LayoutInflater.from(mContext);
            view=layoutInflater.inflate(R.layout.custom_question_list,null);
        }
        edit=(Button) view.findViewById(R.id.custom_layout_question_edit_button);
        question=(TextView) view.findViewById(R.id.custom_layout_question);
        answer=(TextView) view.findViewById(R.id.custom_layout_question_answer);
        question_type=(TextView) view.findViewById(R.id.custom_layout_question_type);
        option=(TextView) view.findViewById(R.id.custom_layout_question_option);
        final VideoPOJO lockVideoBio=videoBioArrayList.get(postion);
        question.setText(lockVideoBio.getQuestion());
        answer.setText(lockVideoBio.getAnswer());
        question_type.setText(lockVideoBio.getQuestion_type());
        option.setText(lockVideoBio.getAnswer_type_one() + " , " + lockVideoBio.getAnswer_type_two()+ " , " +lockVideoBio.getAnswer_type_three()+ " , " +lockVideoBio.getAnswer_type_four());
        Log.e("Get View","Bhakti");
        pos=postion;
        edit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                if (customListner != null)
                {
                    customListner.onButtonClickListner(pos);
                }

            }
        });
        return view;
    }
}
