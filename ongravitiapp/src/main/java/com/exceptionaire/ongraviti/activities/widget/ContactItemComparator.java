package com.exceptionaire.ongraviti.activities.widget;


import java.util.Comparator;

import com.exceptionaire.ongraviti.model.OrbitContact;
import com.exceptionaire.ongraviti.model.OrbitFriend;
import com.exceptionaire.ongraviti.model.UserData;

public class ContactItemComparator implements Comparator<OrbitContact> {
    @Override
    public int compare(OrbitContact contact1, OrbitContact contact2) {
        return contact1.getOrbit_id().compareTo(contact2.getOrbit_id());
    }
}
