package com.exceptionaire.ongraviti.activities.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;


public class Custom_Spinner_Adapter extends BaseAdapter {
    Context context;
    private String[] ids;
    private String[] choice;


    public Custom_Spinner_Adapter(Context context, String[] ids, String[] spinner_data) {
        this.context = context;
        this.ids = ids;
        this.choice = spinner_data;
    }

    @Override
    public int getCount() {
        return choice.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            view = layoutInflater.inflate(R.layout.layout_custom_spinner_layout, null);
        }

        TextView txt_id = view.findViewById(R.id.custom_spinner_txt_view_Id);
        TextView txt_data = view.findViewById(R.id.custom_spinner_txt_view_txtField);

        txt_data.setText(choice[position]);
        txt_id.setText(ids[position]);
        txt_id.setVisibility(View.GONE);
        return view;
    }

    public void setError(View view, CharSequence sequence) {
        TextView error = view.findViewById(R.id.custom_spinner_txt_view_txtField);
        error.setError(sequence);
    }
}
