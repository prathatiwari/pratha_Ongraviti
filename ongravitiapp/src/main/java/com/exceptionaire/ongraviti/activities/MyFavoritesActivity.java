package com.exceptionaire.ongraviti.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.FavoritesAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.FavoriteUsers;
import com.exceptionaire.ongraviti.utilities.CustomVolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by root on 6/3/17.
 */

public class MyFavoritesActivity extends BaseDrawerActivity implements FavoritesAdapter.favoriteDeleteListener, Response.Listener, Response.ErrorListener, FavoritesAdapter.favoriteViewListener {
    private ListView listView;
    private ArrayList<FavoriteUsers> list_favoriteUsers;
    private FavoriteUsers favoriteModel;
    private FavoritesAdapter adapter;
    private Intent intent;
    private String username;
    private int profile_pic;
    private int WEBSERVICE_FLAG;
    private ProgressDialog progressDialog;
    private RequestQueue requestQueue;
    private CustomVolleyRequest customVolleyRequest;
    private String status, message;
    private TextView header;
    String TAG = "FavoriteUser";
   public static TextView tv_msg;
    Context context;
    EditText et_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.favorite_list, frameLayout);
        context = this;
        arcMenu.setVisibility(View.VISIBLE);

        try {
            initView();
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        loadFavoriteList("0");
        super.onResume();
    }

    private void initView() {
        try {
            et_search = (EditText) findViewById(R.id.et_search);
            header = (TextView) findViewById(R.id.toolbar_title_drawer);
            header.setText(getResources().getString(R.string.favourite));
            listView = (ListView) findViewById(R.id.favorite_list);
            list_favoriteUsers = new ArrayList<FavoriteUsers>();
            tv_msg = (TextView) findViewById(R.id.tv_msg);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

            et_search.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {
                    if (et_search.getText().length() > 0) {
                        String text = et_search.getText().toString().toLowerCase(Locale.getDefault());
                        if (adapter != null)
                            adapter.filter(text);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                                              int arg2, int arg3) {
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onfavoriteDeleteClickListner(int position) {
        try {
            Log.d(TAG, "## onfavoriteDeleteClickListner position : " + position);
            if (isNetworkAvailable()) {
                WEBSERVICE_FLAG = 2;
                Map<String, String> map = new HashMap<>();
                map.put("user_id", AppSettings.getLoginUserId());
                map.put("favorite_id", list_favoriteUsers.get(position).getFavorite_id());
                requestQueue = Volley.newRequestQueue(MyFavoritesActivity.this);
                customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.DELETE_FAVORITE, map, this, this);
                requestQueue.add(customVolleyRequest);
//        startActivity(new Intent(MyFavoritesActivity.this, AskForDating.class));

                list_favoriteUsers.remove(position);
                adapter.notifyDataSetChanged();
                loadFavoriteList("0");
            } else {
                CommonDialogs.dialog_with_one_btn_without_title(MyFavoritesActivity.this, getResources().getString(R.string.check_internet));
            }
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onfavoriteViewClickListner(int position) {
        try {
            Log.d(TAG, "## onfavoriteViewClickListner position : " + position);

            Intent intentFavUserView = new Intent(MyFavoritesActivity.this, FavoriteUserInfoActivity.class);
            intentFavUserView.putExtra("ActivityName", "FavoriteProfile");
            intentFavUserView.putExtra("fav_user_id", list_favoriteUsers.get(position).getFavorite_user_id());
            startActivity(intentFavUserView);
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    //load favorite list
    private void loadFavoriteList(String fav_id) {
        try {
            if (isNetworkAvailable()) {
                showProgressBar();
                WEBSERVICE_FLAG = 1;
                Map<String, String> map = new HashMap<>();
                map.put("user_id", AppSettings.getLoginUserId());
                map.put("favorite_id", fav_id);
                requestQueue = Volley.newRequestQueue(MyFavoritesActivity.this);
                customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.GET_FAV_USERS_LINK, map, this, this);
                requestQueue.add(customVolleyRequest);
            } else {
                CommonDialogs.dialog_with_one_btn_without_title(MyFavoritesActivity.this, getResources().getString(R.string.check_internet));
            }
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d(TAG, "## onErrorResponse : " + error.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            error.printStackTrace();
            String errorMessage = getString(R.string.error_connection_generic);
            if (error instanceof NoConnectionError)
                errorMessage = getString(R.string.error_no_connection);
            else if (error instanceof AuthFailureError)
                errorMessage = getString(R.string.error_authentication);
            else if (error instanceof NetworkError)
                errorMessage = getString(R.string.error_network_error);
            else if (error instanceof ParseError)
                errorMessage = getString(R.string.error_parse_error);
            else if (error instanceof ServerError)
                errorMessage = getString(R.string.error_server_error);
            else if (error instanceof TimeoutError)
                errorMessage = getString(R.string.error_connection_timeout);
            CommonDialogs.showMessageDialog(context, getResources().getString(R.string.ongravity),errorMessage).show();
//            Intent intentHome = new Intent(context, HomeUserPostsActivity.class);
//            startActivity(intentHome);
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void setFavoritesAdapter() {
        try {
            adapter = new FavoritesAdapter(list_favoriteUsers, context);
            adapter.setfavoriteDelete(MyFavoritesActivity.this);
            adapter.setFavoriteUserInfoListener(MyFavoritesActivity.this);
            listView.setAdapter(adapter);
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response) {
        try {
            Log.d(TAG, "## response :  " + response.toString());
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            if (WEBSERVICE_FLAG == 1) {
                JSONObject jObject;
                try {
                    jObject = new JSONObject(String.valueOf(response));
                    status = jObject.getString("status");

                    if (status.equals("success")) {
                        Log.d(TAG, "## status.equals(success) :  ");
                        FavoriteUsers favoriteUsers = new FavoriteUsers(jObject);
                        list_favoriteUsers = favoriteUsers.getList_fav_users();
                        tv_msg.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        setFavoritesAdapter();

                    } else if (status.equalsIgnoreCase("error")) {
                        tv_msg.setVisibility(View.VISIBLE);
//                    Toast.makeText(FavoriteUser.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (WEBSERVICE_FLAG == 2) {
                JSONObject jObject;
                try {
                    jObject = new JSONObject(String.valueOf(response));
                    status = jObject.getString("status");
                    message = jObject.getString("msg");
                    Log.e(TAG, "## " + response + " ");
                    if (status.equals("success")) {
//                    finish();
                        Log.d(TAG, "## list_favoriteUsers.size() : " + list_favoriteUsers.size());
                        if (list_favoriteUsers.size() > 0) {
                        } else {
                            listView.setVisibility(View.GONE);
                            tv_msg.setVisibility(View.VISIBLE);
                        }
                        CommonDialogs.dialog_with_one_btn_without_title(MyFavoritesActivity.this, message);

                    } else if (status.equalsIgnoreCase("error")) {
//                    Toast.makeText(FavoriteUser.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }
}
