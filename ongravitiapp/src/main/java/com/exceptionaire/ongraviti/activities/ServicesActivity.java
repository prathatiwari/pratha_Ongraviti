package com.exceptionaire.ongraviti.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.ServiceListAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.Service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 24/02/17.
 */

public class ServicesActivity extends BaseDrawerActivity {

    private CustomProgressDialog progressDialog;
    private ArrayList<Service> services;
    private ServiceListAdapter serviceAdapter;
    private ListView list_services;
    private TextView tv_msg;
    private Context context;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_services, frameLayout);
        TextView header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText(getResources().getString(R.string.title_activity_services));
        context = this;
        arcMenu.setVisibility(View.VISIBLE);


        tv_msg = (TextView) findViewById(R.id.tv_msg);
        list_services = (ListView) findViewById(R.id.list_services);

        serviceAdapter = new ServiceListAdapter(new ArrayList<Service>(), this);
        list_services.setAdapter(serviceAdapter);

        //showServiceList();

        //TODO
        Service service = new Service();
        service.setServiceName("Lock your video feature");
        service.setServiceDescription(getString(R.string.lorem_ipsum));
        service.setServiceCost("$20");

        List services = new ArrayList<Service>();
        services.add(service);
        services.add(service);
        services.add(service);
        serviceAdapter.setServiceList(services);

        tv_msg.setVisibility(View.GONE);
        list_services.setVisibility(View.VISIBLE);

    }

    public void makePayment(Service service) {
//        Toast.makeText(context, "Purchase clicked", Toast.LENGTH_SHORT).show();
    }

    private void showServiceList() {
        progressDialog = new CustomProgressDialog(this, "Loading Services...");
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/get_services",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        System.out.println("get_services :Response " + response);
                        JSONObject jObject;
                        try {
                            jObject = new JSONObject(response);
                            System.out.println(response);

                            // message = jObject.getString("msg");
                            String status = jObject.getString("status");
                            if (status.equals("success")) {
                                Object obj = jObject.get("service_list");
                                if (obj instanceof JSONArray) {
                                    JSONArray requestLists = jObject.getJSONArray("service_list");
                                    services = new ArrayList<Service>();
                                    Service request = null;
                                    for (int i = 0; i < requestLists.length(); i++) {
                                        Log.d("requestLists count", "" + i);

                                        JSONObject detailsObject = requestLists.getJSONObject(i);
                                        request = new Service();

                                        request.setServiceId(detailsObject.getString("service_id"));
                                        request.setServiceName(detailsObject.getString("service_name"));
                                        request.setServiceDescription(detailsObject.getString("service_description"));
                                        request.setServiceCost(detailsObject.getString("service_cost"));

                                        services.add(request);
                                    }

                                    if (services.size() > 0) {
                                        serviceAdapter.setServiceList(services);
                                        tv_msg.setVisibility(View.GONE);
                                        list_services.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    tv_msg.setVisibility(View.VISIBLE);
                                    list_services.setVisibility(View.GONE);
                                }

                                progressDialog.dismiss();

                            } else if (status.equalsIgnoreCase("error")) {
                                //CommonDialogs.showMessageDialog(context, "Error", message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("serviceactivity", "## onErrorResponse : " + error.toString());
                        try {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();

                            error.printStackTrace();
                            String errorMessage = getString(R.string.error_connection_generic);
                            if (error instanceof NoConnectionError)
                                errorMessage = getString(R.string.error_no_connection);
                            else if (error instanceof AuthFailureError)
                                errorMessage = getString(R.string.error_authentication);
                            else if (error instanceof NetworkError)
                                errorMessage = getString(R.string.error_network_error);
                            else if (error instanceof ParseError)
                                errorMessage = getString(R.string.error_parse_error);
                            else if (error instanceof ServerError)
                                errorMessage = getString(R.string.error_server_error);
                            else if (error instanceof TimeoutError)
                                errorMessage = getString(R.string.error_connection_timeout);
                            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                            Intent intentHome = new Intent(context, HomeUserPostsActivity.class);
                            startActivity(intentHome);
                        } catch (Exception e) {
                            Log.e("serviceactivity", "## error : " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", AppSettings.getLoginUserId());
                System.out.println(map);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
