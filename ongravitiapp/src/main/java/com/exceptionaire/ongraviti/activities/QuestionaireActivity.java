package com.exceptionaire.ongraviti.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.Custom_Spinner_Adapter;
import com.exceptionaire.ongraviti.activities.adapters.QuestionaireAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.Questionaire;
import com.exceptionaire.ongraviti.model.VideoPOJO;
import com.exceptionaire.ongraviti.utilities.CustomVolleyRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 10/2/17.
 */

public class QuestionaireActivity extends BaseDrawerActivity implements View.OnClickListener, Response.Listener, Response.ErrorListener {

    Context context;
    Button btnAddNewQuestion, btnlockVideoBioSubmit;
    RecyclerView recycler_listOfQuestions;

    String status, message, ques_id;
    int question_flag;
    String TAG = "QuestionaireActivity";

    //dialog variables
    Spinner answer_type, dropdown_answer;
    EditText question, option_one, option_two, option_three, option_four;
    int spinner_position;
    Custom_Spinner_Adapter custom_spinner_adapter;
    RadioGroup answer_yes_no;
    RadioButton yes, no, yesORno;
    String[] array;
    LinearLayout dropdown, radio_group, parent_layout;
    String answerType, desiredAnswerDropdown, desiredAnswerRadio, que;
    int intAnswerType;
    int FLAG, position;
    final String[] dropdown_answer_array = new String[4];
    int REQUEST_CODE = 300, EDIT_REQUEST = 100;
    String one, two, three, four;
    Intent edit_intent;
    String[] selectQuestionType;
    int flag_value;
    String select_question_type;
    int i;
    private boolean isShown = false;

    private ProgressDialog progressDialog;
    private Button add_answer;
    TextView btnSubmitQ, btnCancelDialog;
    private ListView mListView;
    private List<VideoPOJO> lockVideoBioList;
    JSONObject j = null;
    Dialog dialog;

    //volly request variables
    private int WESERVICE_FLAG;
    private RequestQueue requestQueue;
    private CustomVolleyRequest customVolleyRequest;
    private String profile_cat_id, is_set_lock;
    private String jsonObjectString;

    private ArrayList<Questionaire> list_questionaire;
    QuestionaireAdapter adapter;
    private View view;
    private boolean addQuestionaire = false;
    private Paint p = new Paint();
    String AllOptions;
    Boolean ifEditQues = false;
    protected LinearLayout mLlControlHeight;
    protected View mOnCreateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_questionaire, frameLayout);
        arcMenu.setVisibility(View.GONE);

        try {
            init();
            context = this;

            //get profile category id and lock type(pic, video)
            if (getIntent() != null) {
                Bundle data = getIntent().getExtras();

                if (!data.getString("profile_cat_id").isEmpty()) {
                    profile_cat_id = data.getString("profile_cat_id");
                    Log.d(TAG, "## profile_cat_id: " + profile_cat_id);
                }
                if (!data.getString("is_set_lock").isEmpty()) {
                    is_set_lock = data.getString("is_set_lock");
                    Log.d(TAG, "## is_set_lock: " + data.getString("is_set_lock"));
                }
            }

            //get questoinaire data
            getExistingQuestionaire();

        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void getExistingQuestionaire() {
        try {
            WESERVICE_FLAG = 2;  // 1 flag for set questionaire

            if (isNetworkAvailable()) {
                showProgressBar();
                Map<String, String> map = new HashMap<>();

                map.put("user_id", AppSettings.getLoginUserId());
                map.put("profile_cat_id", profile_cat_id);
                map.put("question_for", is_set_lock);
                Log.d(TAG, "## get questionaire : " + map.toString());

                requestQueue = Volley.newRequestQueue(context);
                customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.GET_QUESTIONAIRE, map, QuestionaireActivity.this, QuestionaireActivity.this);
                requestQueue.add(customVolleyRequest);
            } else {
                CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, getResources().getString(R.string.check_internet));
            }
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response) {
        Log.d(TAG, "## onResponse : " + response.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            JSONObject jObject;
            if (WESERVICE_FLAG == 1) { // set questionaire
                try {
                    Log.d(TAG, "## response : " + response + " ");
                    jObject = new JSONObject(String.valueOf(response));
                    isShown = false;
                    status = jObject.getString("status");
                    message = jObject.getString("msg");
                    ques_id = jObject.getString("ques_id");

                    if (status.equalsIgnoreCase("success")) {
                        CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, message);
                        Log.d(TAG, "## addQuestionaire flag : " + addQuestionaire);
                        Log.d(TAG, "## ifEditQues flag : " + ifEditQues);
                        if (ifEditQues) {
                        } else {
                            if (addQuestionaire) { // if user added new questionare than update recycler view
                                if (adapter != null) {
                                    adapter.addItem(ques_id, que, String.valueOf(intAnswerType), desiredAnswerRadio, AllOptions);
                                } else {
                                    Questionaire questionaire = new Questionaire(ques_id, que, String.valueOf(intAnswerType), desiredAnswerRadio, AllOptions);
                                    setQuestionaireAdapter(true, null, questionaire);
                                }
                            }
                        }
                    } else if (status.equals("error")) {
                        CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, message);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "## e : " + e.getMessage());
                    e.printStackTrace();
                }
            } else if (WESERVICE_FLAG == 2) {  // get questionaire data
                try {
                    jObject = new JSONObject(String.valueOf(response));

                    status = jObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        setQuestionaireAdapter(false, jObject, null);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "## e : " + e.getMessage());
                    e.printStackTrace();
                }
            } else if (WESERVICE_FLAG == 3) {  // delete questionaire data
                try {
                    jObject = new JSONObject(String.valueOf(response));

                    status = jObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, getResources().getString(R.string.que_deleted));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "## e : " + e.getMessage());
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d(TAG, "## onErrorResponse : " + error.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            error.printStackTrace();
            String errorMessage = getString(R.string.error_connection_generic);
            if (error instanceof NoConnectionError)
                errorMessage = getString(R.string.error_no_connection);
            else if (error instanceof AuthFailureError)
                errorMessage = getString(R.string.error_authentication);
            else if (error instanceof NetworkError)
                errorMessage = getString(R.string.error_network_error);
            else if (error instanceof ParseError)
                errorMessage = getString(R.string.error_parse_error);
            else if (error instanceof ServerError)
                errorMessage = getString(R.string.error_server_error);
            else if (error instanceof TimeoutError)
                errorMessage = getString(R.string.error_connection_timeout);
            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
            Intent intentHome = new Intent(context, HomeUserPostsActivity.class);
            startActivity(intentHome);
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    // get data from web service and set all data in recycler view
    public void setQuestionaireAdapter(Boolean flag, JSONObject jsonObject, Questionaire questionaire1) {// flag to check if list is set after api call or after adding item
        try {
            if (!flag) {
                Questionaire questionaire = new Questionaire(jsonObject);
                list_questionaire = questionaire.getList_questionaire();
            } else {
                list_questionaire = new ArrayList<>();
                list_questionaire.add(questionaire1);
            }
            if (list_questionaire.isEmpty()) {

            } else {
                recycler_listOfQuestions = (RecyclerView) findViewById(R.id.recycler_listOfQuestions);
                recycler_listOfQuestions.setHasFixedSize(true);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recycler_listOfQuestions.setLayoutManager(layoutManager);
                adapter = new QuestionaireAdapter(QuestionaireActivity.this, context, list_questionaire, new QuestionaireAdapter.QuestionaireAdptrListener() {
                    @Override
                    public void imgEditOnClick(View v, int position, String ques_id, String type, String option, String answer, String question) {
                        Log.d(TAG, "## imgEditOnClick at position " + position);
                        Log.d(TAG, "## data : " + type);

                        setQuestions(true, position, ques_id, type, option, answer, question);
                    }

                    @Override
                    public void imgDeleteOnClick(View v, int position, String ques_id) {
                        Log.d(TAG, "## imgDeleteOnClick at position " + position);
                        Log.d(TAG, "## ques_id : " + ques_id);
                        sendDeleteQuestionaireRequest(ques_id);
                    }
                });
                RecyclerView.ItemAnimator animator = recycler_listOfQuestions.getItemAnimator();
                if (animator instanceof SimpleItemAnimator) {
                    ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
                }
                recycler_listOfQuestions.setAdapter(adapter);
//            initSwipe();
            }
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    private void initSwipe() {
        try {
            ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    int position = viewHolder.getAdapterPosition();

                    if (direction == ItemTouchHelper.LEFT) {
                        adapter.removeItem(position);
                    } else {
                        removeView();
                    /*edit_position = position;
                    alertDialog.setTitle("Edit Country");
                    et_country.setText(countries.get(position));
                    alertDialog.show();*/
                    }
                }

                @Override
                public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                    Bitmap icon;
                    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                        View itemView = viewHolder.itemView;
                        float height = (float) itemView.getBottom() - (float) itemView.getTop();
                        float width = height / 3;

                        if (dX > 0) {
                            p.setColor(Color.parseColor("#388E3C"));
                            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                            c.drawRect(background, p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_edit);
                            RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                            c.drawBitmap(icon, null, icon_dest, p);
                        } else {
                            p.setColor(Color.parseColor("#D32F2F"));
                            RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                            c.drawRect(background, p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.remove);
                            RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                            c.drawBitmap(icon, null, icon_dest, p);
                        }
                    }
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            };
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
            itemTouchHelper.attachToRecyclerView(recycler_listOfQuestions);
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    private void removeView() {
        try {
            if (view.getParent() != null) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void sendDeleteQuestionaireRequest(String ques_id) {
        try {
            WESERVICE_FLAG = 3;  // 3 flag for delete questionaire

            if (isNetworkAvailable()) {
                showProgressBar();
                Map<String, String> map = new HashMap<>();

                map.put("user_id", AppSettings.getLoginUserId());
                map.put("ques_id", ques_id);

                requestQueue = Volley.newRequestQueue(context);
                customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.DELETE_QUESTIONAIRE, map, QuestionaireActivity.this, QuestionaireActivity.this);
                requestQueue.add(customVolleyRequest);
            } else {
                CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, context.getString(R.string.check_internet));
            }
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void init() {
        try {
            recycler_listOfQuestions = (RecyclerView) findViewById(R.id.recycler_listOfQuestions);
            btnAddNewQuestion = (Button) findViewById(R.id.btnAddNewQuestion);
            TextView txtActivityName = (TextView) findViewById(R.id.toolbar_title_drawer);
            txtActivityName.setText("Questionaire");
//        btnlockVideoBioSubmit = (Button) findViewById(R.id.btnlockVideoBioSubmit);

            btnAddNewQuestion.setOnClickListener(this);
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        try {
            if (view == btnAddNewQuestion) {
                //add new question
                if (list_questionaire != null) {
                    Log.d(TAG, "## list_questionaire.size() : " + list_questionaire.size());
                    if (list_questionaire.size() >= 5) {
                        CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, getResources().getString(R.string.str_limit_que));
                    } else {
                        if (!isShown)  //check if already dialog is open or not
                            setQuestions(false, 0, "", "", "", "", "");
                    }
                } else {
                    if (!isShown)  //check if already dialog is open or not
                        setQuestions(false, 0, "", "", "", "", "");
                }
            }
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (list_questionaire != null) {
                if (list_questionaire.size() < 2) {
                    CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, getResources().getString(R.string.str_lock_msg));
                } else {
                    Intent i = new Intent(this, ProfileMainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }
            }else {
                CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, getResources().getString(R.string.str_lock_msg));
            }
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void setQuestions(final boolean ifEdit, final int position, final String ques_id, final String type, final String option, final String answer, final String qquestion) {
        try {
            //for swing animation
            /*mLlControlHeight = new LinearLayout(context);
            mLlControlHeight.setOrientation(LinearLayout.VERTICAL);

            LinearLayout linearLayout=(LinearLayout)findViewById(R.id.linear_base);
            mLlControlHeight.addView(linearLayout);

            BaseAnimatorSet baseAnimatorSet=new Swing();
            baseAnimatorSet.listener(new BaseAnimatorSet.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }
            }).playOn(mLlControlHeight);*/

            ifEditQues = ifEdit;
            dialog = new Dialog(context, R.style.DialogStyle);

            final RadioGroup radioGroup;

            //dialog initialization
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.row_lock_questionaire);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            // done with initialization
            answer_type = (Spinner) dialog.findViewById(R.id.select_question_type);
            dropdown_answer = (Spinner) dialog.findViewById(R.id.dropdown_desired_answer);
            question = (EditText) dialog.findViewById(R.id.videoBioQuestion);
            option_one = (EditText) dialog.findViewById(R.id.dropdown_option_one);
            option_two = (EditText) dialog.findViewById(R.id.dropdown_option_two);
            option_three = (EditText) dialog.findViewById(R.id.dropdown_option_three);
            option_four = (EditText) dialog.findViewById(R.id.dropdown_option_four);
            answer_yes_no = (RadioGroup) dialog.findViewById(R.id.radio_group_desired_answer);
            yes = (RadioButton) dialog.findViewById(R.id.radio_desired_answer_yes);
            no = (RadioButton) dialog.findViewById(R.id.radio_desired_answer_no);
            dropdown = (LinearLayout) dialog.findViewById(R.id.dropdown_linear);
            radio_group = (LinearLayout) dialog.findViewById(R.id.radio_linear);
            parent_layout = (LinearLayout) dialog.findViewById(R.id.parentLayout);
            radioGroup = (RadioGroup) dialog.findViewById(R.id.radio_group_desired_answer);

            dropdown.setVisibility(View.GONE);
            radio_group.setVisibility(View.GONE);
            option_one.addTextChangedListener(textWatcher);
            option_two.addTextChangedListener(textWatcher);
            option_three.addTextChangedListener(textWatcher);
            option_four.addTextChangedListener(textWatcher);
            btnSubmitQ = (TextView) dialog.findViewById(R.id.btn_question_submit);
            btnCancelDialog = (TextView) dialog.findViewById(R.id.btn_cancel);
            selectQuestionType = getResources().getStringArray(R.array.answer_type);
            array = getResources().getStringArray(R.array.ids);
//            custom_spinner_adapter = new Custom_Spinner_Adapter(context, array, selectQuestionType);
            fillDataInSpinner(answer_type, selectQuestionType);

            btnCancelDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "## onClick btnCancelDialog");
                    dialog.dismiss();
                    isShown = false;
                }
            });

            dropdown_answer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

//                    showToast(dropdown_answer_array[i]);
                    desiredAnswerDropdown = dropdown_answer_array[i];
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            // if edit icon clicked from adapter
            if (ifEdit) {
                question.setText(qquestion);
                Log.d(TAG, "## type : " + type);
                Log.d(TAG, "## selectQuestionType RADIO : " + selectQuestionType[1]);
                Log.d(TAG, "## selectQuestionType drop down : " + selectQuestionType[2]);
                if (type.equalsIgnoreCase("0")) {
                    Log.d(TAG, "## if");
                    //radio
                    //relationship status set selction
                    SetPreSelectionInSpinner(answer_type, selectQuestionType, "Radio Button");
                    if (answer.equalsIgnoreCase("Yes")) {
                        yes.setChecked(true);
                    } else {
                        no.setChecked(true);
                    }
                } else {
                    Log.d(TAG, "## else");
                    //drop down
                    //relationship status set selction
                    SetPreSelectionInSpinner(answer_type, selectQuestionType, "Dropdown Menu");

                    //split options
                    List<String> list_options = Arrays.asList(option.split(","));
                    option_one.setText(list_options.get(0));
                    option_two.setText(list_options.get(1));
                    option_three.setText(list_options.get(2));
                    option_four.setText(list_options.get(3));

                    String[] split_arr = option.split(",");
                    SetPreSelectionInSpinner(dropdown_answer, split_arr, answer);
                }
            }

            btnSubmitQ.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "## onClick submit q ans FLAG : " + FLAG);
                    //submit radio answer
                    if (FLAG == 0) {
                        que = question.getText().toString().trim();

                        if (que.matches("")) {
                            question.setError(getResources().getString(R.string.str_question_));
                            if (question.requestFocus()) {
                                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                            }
                        } else {
                            int selectedId = answer_yes_no.getCheckedRadioButtonId();
                            yesORno = (RadioButton) dialog.findViewById(selectedId);
                            if (radioGroup.getCheckedRadioButtonId() == -1) {
                                CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, getResources().getString(R.string.str_radio));
                            } else {
                                desiredAnswerRadio = yesORno.getText().toString();
                                AllOptions = "Yes,No";
                                //call web service
                                // if edit icon clicked from adapter
                                if (ifEdit) {
                                    jsonObjectString = "[{\"question_type\":\"" + intAnswerType + "\",\"question_option\":\"" + AllOptions + "\",\"answer\":\"" + desiredAnswerRadio + "\",\"question\":\"" + que + "\",\"ques_id\":\"" + ques_id + "\"}]";
                                    addQuestionaire = false;
                                } else {
                                    addQuestionaire = true;
                                    jsonObjectString = "[{\"question_type\":\"" + intAnswerType + "\",\"question_option\":\"" + AllOptions + "\",\"answer\":\"" + desiredAnswerRadio + "\",\"question\":\"" + que + "\"}]";
                                }

                                WESERVICE_FLAG = 1;  // 1 flag for set questionaire

                                //change data in dataset// if edit icon clicked from adapter
                                if (ifEdit) {
                                    Log.d(TAG, "## que: " + que);
                                    Log.d(TAG, "## intAnswerType: " + intAnswerType);
                                    Log.d(TAG, "## desiredAnswerRadio: " + desiredAnswerRadio);
                                    Log.d(TAG, "## AllOptions: " + AllOptions);
                                    adapter.editItem(position, ques_id, que, String.valueOf(intAnswerType), desiredAnswerRadio, AllOptions);
                                }

                                if (isNetworkAvailable()) {
                                    showProgressBar();
                                    Map<String, String> map = new HashMap<>();

                                    map.put("user_id", AppSettings.getLoginUserId());
                                    map.put("profile_cat_id", profile_cat_id);
                                    map.put("is_set_lock", is_set_lock);

                                    map.put("data", jsonObjectString);
                                    Log.d(TAG, "## map : " + map.toString());

                                    requestQueue = Volley.newRequestQueue(context);
                                    customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.SET_QUETIONAIRE, map, QuestionaireActivity.this, QuestionaireActivity.this);
                                    requestQueue.add(customVolleyRequest);

                                } else {
                                    CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, getResources().getString(R.string.check_internet));
                                }
                                dialog.dismiss();
                            }
                        }
                    }
                    //submit dropdown answer
                    else if (FLAG == 1) {
                        que = question.getText().toString().trim();
                        one = option_one.getText().toString().trim();
                        two = option_two.getText().toString().trim();
                        three = option_three.getText().toString().trim();
                        four = option_four.getText().toString().trim();
                        if (validateTextView(que, one, two, three, four)) {
                            //call web service
                            AllOptions = one + "," + two + "," + three + "," + four;
                            //call web service
                            if (ifEdit) {
                                jsonObjectString = "[{\"question_type\":\"" + intAnswerType + "\",\"question_option\":\"" + AllOptions + "\",\"answer\":\"" + desiredAnswerRadio + "\",\"question\":\"" + que + "\",\"ques_id\":\"" + ques_id + "\"}]";
                                addQuestionaire = false;
                            } else {
                                addQuestionaire = true;
                                jsonObjectString = "[{\"question_type\":\"" + intAnswerType + "\",\"question_option\":\"" + AllOptions + "\",\"answer\":\"" + desiredAnswerDropdown + "\",\"question\":\"" + que + "\"}]";
                            }

                            WESERVICE_FLAG = 1;  // 1 flag for set questionaire

                            //change data in dataset// if edit icon clicked from adapter
                            if (ifEdit) {
                                Log.d(TAG, "## que: " + que);
                                Log.d(TAG, "## intAnswerType: " + intAnswerType);
                                Log.d(TAG, "## desiredAnswerDropdown: " + desiredAnswerDropdown);
                                Log.d(TAG, "## AllOptions: " + AllOptions);
                                adapter.editItem(position, ques_id, que, String.valueOf(intAnswerType), desiredAnswerDropdown, AllOptions);
                            }

                            if (isNetworkAvailable()) {
                                showProgressBar();
                                Map<String, String> map = new HashMap<>();

                                map.put("user_id", AppSettings.getLoginUserId());
                                map.put("profile_cat_id", profile_cat_id);
                                map.put("is_set_lock", is_set_lock);

                                map.put("data", jsonObjectString);

                                Log.d(TAG, "## map : " + map.toString());

                                requestQueue = Volley.newRequestQueue(context);
                                customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.SET_QUETIONAIRE, map, QuestionaireActivity.this, QuestionaireActivity.this);
                                requestQueue.add(customVolleyRequest);
                            } else {
                                CommonDialogs.dialog_with_one_btn_without_title(QuestionaireActivity.this, getResources().getString(R.string.check_internet));
                            }

                            dialog.dismiss();
                            /*setResult(REQUEST_CODE, intent);
                            finish();*/
                        }
                    } else if (FLAG == 3) {
                        int selectedId = answer_yes_no.getCheckedRadioButtonId();

                        yesORno = (RadioButton) dialog.findViewById(selectedId);
                        desiredAnswerRadio = yesORno.getText().toString();
                        que = question.getText().toString().trim();
                        Intent intent = new Intent(context, LockVideoBioActivity.class);
                        intent.putExtra("Flag", 0);
                        intent.putExtra("Question", que);
                        intent.putExtra("Answer_type", answerType);
                        intent.putExtra("Option_One", "Yes");
                        intent.putExtra("Option_Two", "No");
                        intent.putExtra("Answer", desiredAnswerRadio);
                        intent.putExtra("Position", position);
                        setResult(EDIT_REQUEST, intent);
                        finish();
                    } else if (FLAG == 4) {
                        que = question.getText().toString().trim();
                        Intent intent = new Intent(context, LockVideoBioActivity.class);
                        intent.putExtra("Flag", 1);
                        intent.putExtra("Question", que);
                        intent.putExtra("Answer_type", answerType);
                        intent.putExtra("Option_One", option_one.getText().toString().trim());
                        intent.putExtra("Option_Two", option_two.getText().toString().trim());
                        intent.putExtra("Option_Three", option_three.getText().toString().trim());
                        intent.putExtra("Option_Four", option_four.getText().toString().trim());
                        intent.putExtra("Answer", desiredAnswerDropdown);
                        intent.putExtra("Position", position);
                        setResult(EDIT_REQUEST, intent);
                        finish();
                    }

                }
            });
            isShown = true;
            dialog.show();

            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
            Window window = dialog.getWindow();
            window.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void SetPreSelectionInSpinner(Spinner spinner, final String[] category, String selection) {
        try {
            ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, category);

            aa.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);

            spinner.setAdapter(aa);

            if (!selection.equals(null)) {
                int spinnerPosition = aa.getPosition(selection);
                spinner.setSelection(spinnerPosition);
            }

        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void fillDataInSpinner(Spinner spinner, final String[] category) {
        try {
            ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, category);

            aa.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);

            spinner.setAdapter(aa);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    spinner_position = position;
                    answerType = answer_type.getSelectedItem().toString();

                    if (spinner_position == 1) {
                        intAnswerType = 0;
                        dropdown.setVisibility(View.GONE);
                        radio_group.setVisibility(View.VISIBLE);
                        //radio button
                        FLAG = 0;
                    } else if (spinner_position == 2) {
                        intAnswerType = 1;
                        radio_group.setVisibility(View.GONE);
                        dropdown.setVisibility(View.VISIBLE);
                        //spinner
                        FLAG = 1;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private boolean validateTextView(String que, String one, String two, String three, String four) {
        boolean valid = true;
        try {
            question.setError(null);
            option_one.setError(null);
            option_two.setError(null);
            option_three.setError(null);
            option_four.setError(null);

            if (que.matches("")) {
                question.setError(getResources().getString(R.string.str_question_));
                if (question.requestFocus()) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
                valid = false;
            } else if (one.matches("")) {
                option_one.setError(getResources().getString(R.string.str_a1));
                if (option_one.requestFocus()) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
                valid = false;
            } else if (two.matches("")) {
                option_two.setError(getResources().getString(R.string.str_a2));
                if (option_two.requestFocus()) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
                valid = false;
            } else if (three.matches("")) {
                option_three.setError(getResources().getString(R.string.str_a3));
                if (option_three.requestFocus()) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
                valid = false;
            } else if (four.matches("")) {
                option_four.setError(getResources().getString(R.string.str_a4));
                if (option_four.requestFocus()) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
                valid = false;
            }
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }
        return valid;
    }

    public TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String s1 = option_one.getText().toString().trim();
            String s2 = option_two.getText().toString().trim();
            String s3 = option_three.getText().toString().trim();
            String s4 = option_four.getText().toString().trim();

            if (s1.equals("") || s2.equals("") || s3.equals("") || s4.equals("")) {
                dropdown_answer.setEnabled(false);
            } else {
                dropdown_answer_array[0] = s1;
                dropdown_answer_array[1] = s2;
                dropdown_answer_array[2] = s3;
                dropdown_answer_array[3] = s4;
                dropdown_answer.setEnabled(true);
                ArrayAdapter<String> adp = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, dropdown_answer_array);
                dropdown_answer.setAdapter(adp);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };


    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }


}
