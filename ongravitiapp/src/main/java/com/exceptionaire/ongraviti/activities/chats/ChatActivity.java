package com.exceptionaire.ongraviti.activities.chats;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.widget.AttachmentPreviewAdapterView;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppGlobals;
import com.exceptionaire.ongraviti.core.AppPreference;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.OrbitContact;
import com.exceptionaire.ongraviti.quickblox.activity.BaseActivity;
import com.exceptionaire.ongraviti.quickblox.adapter.ChatAdapter;
import com.exceptionaire.ongraviti.quickblox.adapter.ChatAttachmentPreviewAdapter;
import com.exceptionaire.ongraviti.quickblox.core.utils.Toaster;
import com.exceptionaire.ongraviti.quickblox.core.utils.imagepick.ImagePickHelper;
import com.exceptionaire.ongraviti.quickblox.core.utils.imagepick.OnImagePickedListener;
import com.exceptionaire.ongraviti.quickblox.managers.QbChatDialogMessageListenerImp;
import com.exceptionaire.ongraviti.quickblox.managers.QbDialogHolder;
import com.exceptionaire.ongraviti.quickblox.utils.chat.ChatHelper;
import com.exceptionaire.ongraviti.quickblox.utils.qb.PaginationHistoryListener;
import com.exceptionaire.ongraviti.quickblox.utils.qb.VerboseQbChatConnectionListener;
import com.exceptionaire.ongraviti.utilities.AudioRecorder;
import com.exceptionaire.ongraviti.utilities.LogUtil;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;

public class ChatActivity extends BaseActivity implements AppConstants, View.OnClickListener
        , EmojiconGridFragment.OnEmojiconClickedListener
        , EmojiconsFragment.OnEmojiconBackspaceClickedListener
        , OnImagePickedListener {
    public static final String TAG = "ReceivedActivity";
    public static final String EXTRA_DIALOG_ID = "dialogId";
    public static final String IS_DRAFT_MESSAGE = "is_draft_message";
    public static final String STRING_DRAFT_MESSAGE = "string_draft_message";
    private static final String PROPERTY_SAVE_TO_HISTORY = "save_to_history";
    private static final String PROPERTY_NOT_SAVE_TO_HISTORY = "no_save_to_history";
    private static final int REQUEST_CODE_ATTACHMENT = 721;

    private ChatAdapter chatAdapter;
    private ChatAttachmentPreviewAdapter attachmentPreviewAdapter;
    private ConnectionListener chatConnectionListener;
    private QBChatDialog qbChatDialog;
    private ArrayList<QBChatMessage> unShownMessages;
    private int skipPagination = 0;
    private ChatMessageListener chatMessageListener;

    private EditText m_txtMessage = null;
    //    private ImageButton m_btnAudio = null;
    private ImageButton m_btnEmoji = null;
    private ImageButton m_btnSend = null;
    private FrameLayout m_emojiconLayout;
    private ListView m_chatListView;
//    private TextView m_btnToTalk;


    private boolean m_bEmojiKeyboard = false;
    private String mReceivedImagePath = "";
    private String sendFilePath;
    private ProgressDialog progressDialog;
    private AppPreference mPrefs;
    private ChatActivity instance;
    private boolean isAudioInputType;
    private AudioRecorder m_audioRecorder;
    private LinearLayout attachmentPreviewContainerLayout;
    private View rootView;

    public static void startForResult(Activity activity, int code, String dialogId) {
        Intent intent = new Intent(activity, ChatActivity.class);
        intent.putExtra(ChatActivity.EXTRA_DIALOG_ID, dialogId);
        activity.startActivityForResult(intent, code);
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

        setContentView(R.layout.activity_chat);
        // getLayoutInflater().inflate(R.layout.activity_chat, frameLayout);
        rootView = findViewById(R.id.content_main);

        arcMenu.setVisibility(View.GONE);

        LogUtil.writeDebugLog(TAG, "onCreate", "start");
        instance = this;
        mPrefs = new AppPreference(this);
        OrbitContact user = AppGlobals.curChattingUser;

        m_txtMessage = (EditText) findViewById(R.id.txt_chat);
        m_emojiconLayout = (FrameLayout) findViewById(R.id.emojicons);
//        m_btnAudio = (ImageButton) findViewById(R.id.button_record_audio);
        m_btnEmoji = (ImageButton) findViewById(R.id.button_emoji);
        m_btnSend = (ImageButton) findViewById(R.id.button_send);
        m_chatListView = (ListView) findViewById(R.id.list_chat_view);
        m_txtMessage.setOnClickListener(this);
//        m_btnAudio.setOnClickListener(this);
        m_btnEmoji.setOnClickListener(this);
        m_btnSend.setOnClickListener(this);
//TODO change name
//        if (user != null && user.getOrbit_name() != null)
        ActionBar(AppSettings.getKeyOrbitName());
//        else
//            ActionBar("The great name");
        qbChatDialog = QbDialogHolder.getInstance().getChatDialogById(getIntent().getStringExtra(EXTRA_DIALOG_ID));
        chatMessageListener = new ChatMessageListener();

        qbChatDialog.addMessageListener(chatMessageListener);
        initChatConnectionListener();
        initViews();
        initChat();
    }

    public void ActionBar(String title) {
        ActionBar toolBar = getSupportActionBar();
        if (toolBar != null) {
            toolBar.setDisplayShowHomeEnabled(false);
            toolBar.setDisplayShowTitleEnabled(false);
            toolBar.setDisplayShowCustomEnabled(true);
            toolBar.setCustomView(R.layout.default_actionbar);
            Toolbar parent = (Toolbar) toolBar.getCustomView().getParent();
            parent.setContentInsetsAbsolute(0, 0);
        }
        TextView txtTitle = (TextView) findViewById(R.id.text_view_title);
        txtTitle.setText(title);
        ImageView buttonBack = (ImageView) findViewById(R.id.image_view_back);
//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/materialicons.ttf");
//        buttonBack.setTypeface(font);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChatActivity.this, ChatsMainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void showEmojiconFragment(boolean isVisible) {
        if (isVisible) {
            getSupportFragmentManager().beginTransaction().replace(R.id.emojicons, EmojiconsFragment.newInstance(false)).commit();
            m_emojiconLayout.setVisibility(View.VISIBLE);
        } else
            m_emojiconLayout.setVisibility(View.GONE);
    }

    private void sendDialogId() {
        LogUtil.writeDebugLog(TAG, "sendDialogId", "1");
        Intent result = new Intent();
        result.putExtra(EXTRA_DIALOG_ID, qbChatDialog.getDialogId());
        setResult(RESULT_OK, result);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            /*case R.id.btn_to_talk:
                break;
            case R.id.button_record_audio:
                isAudioInputType = !isAudioInputType;
//                setAudioMessage();
                break;*/
            case R.id.txt_chat:
                showEmojiconFragment(false);
                break;

            case R.id.button_emoji:
                m_bEmojiKeyboard = !m_bEmojiKeyboard;
                showEmojiconFragment(m_bEmojiKeyboard);
                if (m_bEmojiKeyboard) {
                    m_txtMessage.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(m_txtMessage.getWindowToken(), 0);
                } else {
                    m_txtMessage.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(m_txtMessage, InputMethodManager.SHOW_IMPLICIT);
                }
                break;
            case R.id.button_send:
                onSendChatClick();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        LogUtil.writeDebugLog(TAG, "onBackPressed", "1");
        releaseChat();
        sendDialogId();
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    public void onSendChatClick() {
        int totalAttachmentsCount = attachmentPreviewAdapter.getCount();
        Collection<QBAttachment> uploadedAttachments = attachmentPreviewAdapter.getUploadedAttachments();
        if (!uploadedAttachments.isEmpty()) {
            if (uploadedAttachments.size() == totalAttachmentsCount) {
                for (QBAttachment attachment : uploadedAttachments) {
                    sendChatMessage(null, attachment);
                }
            } else {
                Toaster.shortToast(R.string.chat_wait_for_attachments_to_upload);
            }
        }

        String text = m_txtMessage.getText().toString().trim();
        if (!TextUtils.isEmpty(text)) {
            sendChatMessage(text, null);
        }
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you really want to delete this chat ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteChat();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle("Delete");
        alert.show();
    }

    public void showMessage(QBChatMessage message) {
        if (chatAdapter != null) {
            chatAdapter.add(message);
            chatAdapter.notifyDataSetChanged();
            scrollMessageListDown();
        } else {
            if (unShownMessages == null) {
                unShownMessages = new ArrayList<>();
            }
            unShownMessages.add(message);
        }
    }

    private void initViews() {
        LogUtil.writeDebugLog(TAG, "initViews", "1");
        attachmentPreviewContainerLayout = _findViewById(R.id.layout_attachment_preview_container);
        attachmentPreviewAdapter = new ChatAttachmentPreviewAdapter(this,
                new ChatAttachmentPreviewAdapter.OnAttachmentCountChangedListener() {
                    @Override
                    public void onAttachmentCountChanged(int count) {
                        attachmentPreviewContainerLayout.setVisibility(count == 0 ? View.GONE : View.VISIBLE);

                    }
                },
                new ChatAttachmentPreviewAdapter.OnAttachmentUploadErrorListener() {
                    @Override
                    public void onAttachmentUploadError(QBResponseException e) {
                    }
                });
        AttachmentPreviewAdapterView previewAdapterView = _findViewById(R.id.adapter_view_attachment_preview);
        previewAdapterView.setAdapter(attachmentPreviewAdapter);
        isAudioInputType = true;
//        setAudioMessage();
    }

    private void sendChatMessage(String text, QBAttachment attachment) {
        LogUtil.writeDebugLog(TAG, "sendChatMessage", "start");
        QBChatMessage chatMessage = new QBChatMessage();
        if (attachment != null) {
            chatMessage.addAttachment(attachment);
        } else {
            chatMessage.setBody(text);
        }
        chatMessage.setProperty(PROPERTY_SAVE_TO_HISTORY, "1");
        chatMessage.setDateSent(System.currentTimeMillis() / 1000);
        chatMessage.setMarkable(true);

//        if (!QBDialogType.PRIVATE.equals(qbChatDialog.getType()) && !qbChatDialog.isJoined()) {
//            Toaster.shortToast("You're still joining a group chat, please wait a bit");
//            return;
//        }

        if (QBDialogType.PUBLIC_GROUP.equals(qbChatDialog.getType())) {
            if (!qbChatDialog.isJoined()) {
                Toaster.shortToast("You're still joining a group chat, please wait a bit");
                return;
            }
        }


        try {
            LogUtil.writeDebugLog(TAG, "sendChatMessage", "qbChatDialog.sendMessage");
            if (qbChatDialog == null)
                Toaster.shortToast("Can't send a message, You are not connected to chat. Please login again.");
            qbChatDialog.sendMessage(chatMessage);

            if (QBDialogType.PRIVATE.equals(qbChatDialog.getType())) {
                showMessage(chatMessage);
            }

            if (attachment != null) {
                attachmentPreviewAdapter.remove(attachment);
            } else {
                m_txtMessage.setText("");
            }
        } catch (SmackException.NotConnectedException e) {
            Toaster.shortToast("Can't send a message, You are not connected to chat. Please login again.");
        }
    }

    private void initChat() {
        LogUtil.writeDebugLog(TAG, "initChat", "1");
        switch (qbChatDialog.getType()) {
            case PRIVATE:
                loadDialogUsers();
                break;
            case GROUP:
                joinGroupChat();
                break;
            default:
                Toaster.shortToast(String.format("%s %s", getString(R.string.chat_unsupported_type), qbChatDialog.getType().name()));
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_chat, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.send_attachment:
                onAttachmentsClick();
                return true;
            case R.id.delete_chat:
                showDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void releaseChat() {
        LogUtil.writeDebugLog(TAG, "releaseChat", "1");
        qbChatDialog.removeMessageListrener(chatMessageListener);
        if (!QBDialogType.PRIVATE.equals(qbChatDialog.getType())) {
        }
    }

    private void updateDialog(final ArrayList<QBUser> selectedUsers) {
        ChatHelper.getInstance().updateDialogUsers(qbChatDialog, selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        qbChatDialog = dialog;
                        loadDialogUsers();
                    }

                    @Override
                    public void onError(QBResponseException e) {
                    }
                }
        );
    }

    private void loadDialogUsers() {


        LogUtil.writeDebugLog(TAG, "loadDialogUsers", "start");
        ChatHelper.getInstance().getUsersFromDialog(qbChatDialog, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> users, Bundle bundle) {
                LogUtil.writeDebugLog(TAG, "loadDialogUsers", "onSuccess");
                loadChatHistory();
            }

            @Override
            public void onError(QBResponseException e) {
                LogUtil.writeDebugLog(TAG, "loadDialogUsers", "onError");
            }
        });
    }

    private void joinGroupChat() {
        if (progressDialog == null || !progressDialog.isShowing()) {
            showProgressBar();
        }

        ChatHelper.getInstance().join(qbChatDialog, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle b) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                loadDialogUsers();
            }

            @Override
            public void onError(QBResponseException e) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

//                snackbar = showErrorSnackbar(R.string.connection_error, e, null);
            }
        });
    }


    private void loadChatHistory() {
        LogUtil.writeDebugLog(TAG, "loadChatHistory", "start");

        ChatHelper.getInstance().loadChatHistory(qbChatDialog, skipPagination, new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                LogUtil.writeDebugLog(TAG, "loadChatHistory", "onSuccess");
                Collections.reverse(messages);
                if (chatAdapter == null) {
                    chatAdapter = new ChatAdapter(ChatActivity.this, qbChatDialog, messages);
                    chatAdapter.setPaginationHistoryListener(new PaginationHistoryListener() {
                        @Override
                        public void downloadMore() {
                            loadChatHistory();
                        }
                    });
                    chatAdapter.setOnItemInfoExpandedListener(new ChatAdapter.OnItemInfoExpandedListener() {
                        @Override
                        public void onItemInfoExpanded(final int position) {
                            if (isLastItem(position)) {
                                // HACK need to allow info textview visibility change so posting it via handler
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //mListViewChat.setSelection(position);
                                        m_chatListView.smoothScrollToPosition(position);
                                    }
                                });
                            } else {
                                m_chatListView.smoothScrollToPosition(position);
                            }
                        }

                        private boolean isLastItem(int position) {
                            return position == chatAdapter.getCount() - 1;
                        }
                    });
                    if (unShownMessages != null && !unShownMessages.isEmpty()) {
                        List<QBChatMessage> chatList = chatAdapter.getList();
                        for (QBChatMessage message : unShownMessages) {
                            if (!chatList.contains(message)) {
                                chatAdapter.add(message);
                            }
                        }
                    }
                    m_chatListView.setAdapter(chatAdapter);
                    m_chatListView.setDivider(null);
                } else {
                    chatAdapter.addList(messages);
                    m_chatListView.setSelection(messages.size());
                }
            }

            @Override
            public void onError(QBResponseException e) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                LogUtil.writeDebugLog(TAG, "loadChatHistory", "onError");
                skipPagination -= ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
            }
        });
        skipPagination += ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
    }

    private void scrollMessageListDown() {
        m_chatListView.setSelection(m_chatListView.getCount() - 1);
    }

    private void deleteChat() {
        ChatHelper.getInstance().deleteDialog(qbChatDialog, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onError(QBResponseException e) {
            }
        });
    }

    private void initChatConnectionListener() {
        LogUtil.writeDebugLog(TAG, "initChatConnectionListener", "1");
        showProgressBar();

        chatConnectionListener = new VerboseQbChatConnectionListener(getSnackbarAnchorView()) {
            @Override
            public void reconnectionSuccessful() {
                LogUtil.writeDebugLog(TAG, "reconnectionSuccessful", "1");
                super.reconnectionSuccessful();
                skipPagination = 0;
                chatAdapter = null;
                switch (qbChatDialog.getType()) {
                    case PRIVATE:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadChatHistory();
                            }
                        });
                        break;

                    case GROUP:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadChatHistory();
                            }
                        });
                        break;
                }
            }
        };
    }


    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public void showProgressDialog() {
        progressDialog.show();
    }

    public void sendAttachedFile() {
        onSendChatClick();
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(m_txtMessage, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(m_txtMessage);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        if (qbChatDialog != null) {
            outState.putString(EXTRA_DIALOG_ID, qbChatDialog.getDialogId());
        }
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (qbChatDialog == null) {
            qbChatDialog = QbDialogHolder.getInstance().getChatDialogById(savedInstanceState.getString(EXTRA_DIALOG_ID));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtil.writeDebugLog(TAG, "onResume", "1");
        ChatHelper.getInstance().addConnectionListener(chatConnectionListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogUtil.writeDebugLog(TAG, "onPause", "1");
        ChatHelper.getInstance().removeConnectionListener(chatConnectionListener);
    }

    @Override
    public void onSessionCreated(boolean success) {
        LogUtil.writeDebugLog(TAG, "onSessionCreated", "1");
        if (success) {
            initChat();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        LogUtil.writeDebugLog(TAG, "onActivityResult", "1");
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_PHOTO_FILE) {
                sendFilePath = data.getStringExtra(EK_URL);
                LogUtil.writeDebugLog(TAG, "onActivityResult", "ok");
                File file = new File(sendFilePath);
                attachmentPreviewAdapter.add(file);
                progressDialog.show();
            }
        }
    }

    public class ChatMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
            if (qbChatMessage.getBody() != null && (qbChatMessage.getBody().equals(SEND_VIDEO_VIEW_REQUEST)
                    || qbChatMessage.getBody().equals(SEND_VIDEO_SHOW_END)
                    || qbChatMessage.getBody().equals(SEND_VIDEO_SHOW_START)
            )) {

            } else {
                showMessage(qbChatMessage);
            }
        }
    }

    public void onAttachmentsClick() {
        new ImagePickHelper().pickAnImage(this, REQUEST_CODE_ATTACHMENT);
    }

    @Override
    public void onImagePicked(int requestCode, File file) {
        switch (requestCode) {
            case REQUEST_CODE_ATTACHMENT:
                attachmentPreviewAdapter.add(file);
                break;
        }
    }

    @Override
    public void onImagePickError(int requestCode, Exception e) {
    }

    @Override
    public void onImagePickClosed(int requestCode) {
        // ignore
    }
}
