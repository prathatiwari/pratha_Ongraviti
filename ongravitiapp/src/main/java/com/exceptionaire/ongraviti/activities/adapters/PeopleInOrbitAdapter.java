package com.exceptionaire.ongraviti.activities.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.LoginActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.myorbit.PeopleInOrbitFragment;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.OrbitFriend;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PeopleInOrbitAdapter extends BaseAdapter {
    private List<OrbitFriend> orbitFriendList;
    Context context;
    private PeopleInOrbitFragment fragment;
    private List<OrbitFriend> fileredlist;

    public void setOrbitFriendList(List<OrbitFriend> orbitFriends) {
        this.orbitFriendList = orbitFriends;
        fileredlist.clear();
        fileredlist.addAll(orbitFriends);
        notifyDataSetChanged();
    }

    public PeopleInOrbitAdapter(List<OrbitFriend> contactList, Context context, PeopleInOrbitFragment fragment) {
        this.orbitFriendList = contactList;
        fileredlist = new ArrayList<>();
        this.context = context;
        this.fragment = fragment;
    }

    public void removeItem(int position) {
        orbitFriendList.remove(position);
        notifyDataSetChanged();
    }

    private class Holder {
        TextView txtName;
        ImageButton ibtn_remove, ibtn_block;
        RoundedImageView image;
        RelativeLayout relative_list_item;
        TextView textViewStatus;
        ImageView image_view_cat_type;
    }

    @Override
    public int getCount() {
        return orbitFriendList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return orbitFriendList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @SuppressLint({"ResourceAsColor", "SetTextI18n"})
    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        final Holder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final OrbitFriend orbitFriend = orbitFriendList.get(position);
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.people_in_orbit_list_item, arg2, false);
            Log.d("In custom adapter", " adapter");
            holder.textViewStatus = convertView.findViewById(R.id.text_view_status);
            holder.txtName = convertView.findViewById(R.id.txt_orbit_name);
            holder.ibtn_remove = convertView.findViewById(R.id.ibtn_remove);
            holder.ibtn_block = convertView.findViewById(R.id.ibtn_block);
            holder.image = convertView.findViewById(R.id.user_profile_pic);
            holder.relative_list_item = convertView.findViewById(R.id.relative_list_item);
            holder.image_view_cat_type = convertView.findViewById(R.id.image_view_cat_type);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        Log.d(":imageeeeee", orbitFriend.getProfilePic());
        if (orbitFriend.getProfilePic() != null && !orbitFriend.getProfilePic().isEmpty()) {
            if (!orbitFriend.getProfilePic().contains("https:")) {
                Picasso.with(context)
                        .load(AppConstants.PROFILE_PICTURE_PATH + "/" + orbitFriend.getProfilePic())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            } else if (orbitFriend.getProfilePic().contains("https:")) {
                Picasso.with(context)
                        .load(orbitFriend.getProfilePic())
                        .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.image);
            }
        }

        switch (orbitFriend.getCategory_type()) {
            case "1":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_green);
                break;
            case "2":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_pink);
                break;
            case "3":
                holder.image_view_cat_type.setImageResource(R.drawable.ic_circle_blue);
                break;
        }
        holder.txtName.setText(orbitFriend.getUserName());
        if (!orbitFriend.getProfileStatus().equals("")) {
            holder.textViewStatus.setText(orbitFriend.getProfileStatus());
        } else {
            holder.textViewStatus.setText("Status not available.");
        }


        holder.relative_list_item.setOnClickListener(v -> {
            Log.d("adptr", "## relative_list_item click : " + orbitFriend.getUserID());
            Intent intentProfile = new Intent(context, FavoriteUserInfoActivity.class);
            intentProfile.putExtra("ActivityName", "OrbitProfile");
            intentProfile.putExtra("OrbitUserID", "" + orbitFriend.getUserID());
            context.startActivity(intentProfile);
        });

        holder.image.setOnClickListener(v -> {
            Log.d("adptr", "## image click : " + orbitFriend.getUserID());
            Intent intentProfile = new Intent(context, FavoriteUserInfoActivity.class);
            intentProfile.putExtra("ActivityName", "OrbitProfile");
            intentProfile.putExtra("OrbitUserID", "" + orbitFriend.getUserID());
            context.startActivity(intentProfile);
        });

        holder.txtName.setOnClickListener(v -> {
            Log.d("adptr", "## image click : " + orbitFriend.getUserID());
            Intent intentProfile = new Intent(context, FavoriteUserInfoActivity.class);
            intentProfile.putExtra("ActivityName", "OrbitProfile");
            intentProfile.putExtra("OrbitUserID", "" + orbitFriend.getUserID());
            context.startActivity(intentProfile);
        });

        holder.ibtn_block.setOnClickListener(v -> {


            Dialog dialog = new CommonDialogs().dialogAcceptReject((Activity) context, context.getResources().getString(R.string.ongravity), "Are you sure to block " + orbitFriend.getUserName() + " from your orbit?");
            ((Button) dialog.findViewById(R.id.button_ok)).setText("Yes");
            ((Button) dialog.findViewById(R.id.button_cancel)).setText("No");
            dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.removeFromOrbit(orbitFriend.getUserID(), position, "3");
                    dialog.dismiss();
                }
            });
            dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        });

        holder.ibtn_remove.setOnClickListener(v -> {

            Dialog dialog = new CommonDialogs().dialogAcceptReject((Activity) context, context.getResources().getString(R.string.ongravity), "Are you sure to remove " + orbitFriend.getUserName() + " from your orbit?");
            ((Button) dialog.findViewById(R.id.button_ok)).setText("Yes");
            ((Button) dialog.findViewById(R.id.button_cancel)).setText("No");
            dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.removeFromOrbit(orbitFriend.getUserID(), position, "4");
                    dialog.dismiss();
                }
            });
            dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        });

        holder.image.setOnClickListener(v -> {
            viewProfile();
            //context.startActivity(new Intent(context,ViewSocialBasicProfile.class));
        });
        return convertView;
    }

    private void viewProfile() {

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        orbitFriendList.clear();
        if (charText.length() == 0) {
            orbitFriendList.addAll(fileredlist);
        } else {
            for (OrbitFriend wp : fileredlist) {
                if (wp.getUserName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    orbitFriendList.add(wp);
                }
            }
        }
        if (orbitFriendList.size() == 0) {
            PeopleInOrbitFragment.tv_msg.setVisibility(View.VISIBLE);
        } else {
            PeopleInOrbitFragment.tv_msg.setVisibility(View.GONE);
        }
        notifyDataSetChanged();
    }
}
