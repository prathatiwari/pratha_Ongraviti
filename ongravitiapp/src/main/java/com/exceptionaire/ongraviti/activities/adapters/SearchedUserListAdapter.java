package com.exceptionaire.ongraviti.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.SearchedResultActivity;
import com.exceptionaire.ongraviti.activities.SearchedUserInfoActivity;
import com.exceptionaire.ongraviti.activities.ViewProfileActivity;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.myorbit.AddPeopleToOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.SearchData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SearchedUserListAdapter extends RecyclerView.Adapter<SearchedUserListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SearchData> listSearchData;

    public SearchedUserListAdapter(Context context, ArrayList<SearchData> searchData) {

        this.context = context;
        this.listSearchData = searchData;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView circularNetworkImageView;
        private TextView textViewName;
        private TextView textViewEmailId;
        private Button buttonInvite;
        private RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.txt_orbit_name);
            circularNetworkImageView = itemView.findViewById(R.id.user_profile_pic);
            textViewEmailId = itemView.findViewById(R.id.textview_email_address);
            buttonInvite = itemView.findViewById(R.id.buttonInvite);
            relativeLayout = itemView.findViewById(R.id.relative_layout_searched_user);
        }
    }

    @Override
    public SearchedUserListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_view_row_home, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchedUserListAdapter.ViewHolder holder, int position) {

        if (listSearchData != null && listSearchData.size() > 0) {
            if (listSearchData.get(position) != null) {
                holder.textViewName.setText(listSearchData.get(position).getC_name());
                if (listSearchData.get(position).getEmail_address() != null && !listSearchData.get(position).getEmail_address().isEmpty() && !listSearchData.get(position).getEmail_address().equals(""))
                    holder.textViewEmailId.setText(listSearchData.get(position).getEmail_address());
                else if (listSearchData.get(position).getFull_contact_number() != null && !listSearchData.get(position).getFull_contact_number().isEmpty() && !listSearchData.get(position).getFull_contact_number().equals(""))
                    holder.textViewEmailId.setText(listSearchData.get(position).getFull_contact_number());
                else holder.textViewEmailId.setText(R.string.no_contact_aailable);

//                if (listSearchData.get(position).getProfile_picture() != null) {
//                    Picasso.with(context).load(AppConstants.PROFILE_PICTURE_PATH + "/" + listSearchData.get(position).getProfile_picture()).fit().centerCrop()
//                            .placeholder(R.drawable.profile)
//                            .error(R.drawable.profile)
//                            .into(holder.circularNetworkImageView);
//                }
                if (listSearchData.get(position).getProfile_picture() != null) {
                    if (!listSearchData.get(position).getProfile_picture().contains("https:")) {
                        Picasso.with(context)
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + listSearchData.get(position).getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(holder.circularNetworkImageView);
                    } else if (listSearchData.get(position).getProfile_picture().contains("https:")) {
                        Picasso.with(context)
                                .load(listSearchData.get(position).getProfile_picture())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(holder.circularNetworkImageView);
                    }
                }
                holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (listSearchData.get(position).getUser_id().equalsIgnoreCase(AppSettings.getLoginUserId())) {

                            Intent i = new Intent(context, ViewProfileActivity.class);
                            if (listSearchData.get(position).getProfile_cat_id().equalsIgnoreCase("1")) {
                                i.putExtra("social", "social");
                            } else if (listSearchData.get(position).getProfile_cat_id().equalsIgnoreCase("2")) {
                                i.putExtra("dating", "dating");
                            } else if (listSearchData.get(position).getProfile_cat_id().equalsIgnoreCase("3")) {
                                i.putExtra("business", "business");
                            }
                            context.startActivity(i);
                            ((SearchedResultActivity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            ((SearchedResultActivity) context).finish();

                        } else {

                            Intent intentProfile = new Intent(context, SearchedUserInfoActivity.class);
                            intentProfile.putExtra("ActivityName", "OrbitProfile");
                            intentProfile.putExtra("OrbitUserID", "" + listSearchData.get(position).getUser_id());
                            context.startActivity(intentProfile);
                            ((SearchedResultActivity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            ((SearchedResultActivity) context).finish();
                        }
                    }
                });
//                if (listSearchData.get(0).getorbit_status().equals("1")) {
//                    holder.buttonInvite.setVisibility(View.INVISIBLE);
//                } else {
//                    holder.buttonInvite.setVisibility(View.VISIBLE);
//                }
//                holder.buttonInvite.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        ((AddPeopleToOrbitActivity) context).sendFriendRequest(listSearchData.get(position).getUser_id());
//                    }
//                });
            }
        }
    }

    @Override
    public int getItemCount() {

        if (listSearchData != null) {
            if (listSearchData.size() > 0)
                return listSearchData.size();
        }
        return 0;
    }
}
