package com.exceptionaire.ongraviti.activities.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.EditProfileDetailsActivity;
import com.exceptionaire.ongraviti.activities.ProfileMainActivity;
import com.exceptionaire.ongraviti.activities.adapters.CategoriesAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.Categories;
import com.exceptionaire.ongraviti.model.ResponseGetInterestSubcategory;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseSetUserInfo;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_ERROR;
import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_SUCCESS;
import static com.facebook.FacebookSdk.getApplicationContext;


public class EditProfileMyInterestsFragment extends android.support.v4.app.Fragment {

    private Button buttonNext, buttonPrevious;
    private View view;
    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    // private MultiSelectionAdapter<Categories> mAdapter;
    private CategoriesAdapter categoriesAdapter;
    private ArrayList<Categories> listCategories;
    private String[] selectedCategoryArray;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_edit_profile_my_interests, container, false);
        setActionBar();
        initializeViewComponent();
        if (((BaseActivity) getActivity()).isNetworkAvailable()) {
            getRSSPreferenceList();
        } else {
            Toast.makeText(getActivity(), R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void initializeViewComponent() {
        AppSettings.setProfileStepStatus5(1);

        recyclerView = (RecyclerView) view.findViewById(R.id.list_view_category);

        listCategories = new ArrayList<>();
        selectedCategoryArray = new String[]{};

        ResponseGetUserProfileData userProfileDataFromResponse = ((EditProfileDetailsActivity) getActivity()).getUserProfileDataFromResponse();
        if (userProfileDataFromResponse != null) {

            UserBasicInfo basic_user_info = userProfileDataFromResponse.getUser_basic_info()[0];
            if (basic_user_info != null) {

                String categories = basic_user_info.get_interest_category();
                if (categories != null)
                    selectedCategoryArray = categories.split(",");
            }
        }

        categoriesAdapter = new CategoriesAdapter(getActivity(), listCategories, selectedCategoryArray);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(categoriesAdapter);

        buttonNext = view.findViewById(R.id.button_next);
        buttonPrevious = view.findViewById(R.id.button_prev);

        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new EditProfileILikeToBeFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container_edit_profile_details, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();

            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ArrayList<Categories> mArrayProducts = categoriesAdapter.getCheckedItems();

                if (mArrayProducts != null && mArrayProducts.size() > 0) {
                    AppSettings.setProfileStepStatus5(2);
                }

//                Log.d(MainActivity.class.getSimpleName(), "Selected Items: " + mArrayProducts.toString());
                StringBuilder selectedInterestCategories = new StringBuilder("");
                StringBuilder selectedInterestCategoriesStr = new StringBuilder("");
                for (Categories categories : mArrayProducts) {
                    selectedInterestCategories.append(categories.getId() + ",");
                    selectedInterestCategoriesStr.append(categories.getSub_category() + ",");
                }
                if (selectedInterestCategories != null && selectedInterestCategories.length() > 0)
                    selectedInterestCategories.deleteCharAt(selectedInterestCategories.length() - 1);
                if (selectedInterestCategoriesStr != null && selectedInterestCategoriesStr.length() > 0)
                    selectedInterestCategoriesStr.deleteCharAt(selectedInterestCategoriesStr.length() - 1);

                AppSettings.setInterestCategories(selectedInterestCategories.toString());

                String data = AppSettings.getProfileDetails();
                Gson gson = new Gson();
                ResponseGetUserProfileData responseGetUserProfileData = gson.fromJson(data, ResponseGetUserProfileData.class);

                if (responseGetUserProfileData != null) {
                    UserBasicInfo userBasicInfo = responseGetUserProfileData.getUser_basic_info()[0];
                    if (userBasicInfo != null) {

                        userBasicInfo.set_interest_category(selectedInterestCategories.toString());

                        String json = gson.toJson(responseGetUserProfileData);
                        AppSettings.setProfileDetails(json);
                    }
                }
                if (((BaseActivity) getActivity()).isNetworkAvailable()) {
//                    ((EditProfileDetailsActivity) getActivity()).saveUserProfileDataToServer();

                    showProgressBar();

                    Retrofit retrofit = ApiClient.getClient();
                    ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
                    Call<ResponseSetUserInfo> loginCall = getProfileRequest.setUserProfileData5(
                            AppSettings.getLoginUserId(),
                            selectedInterestCategories.toString()
                    );

                    loginCall.enqueue(new Callback<ResponseSetUserInfo>() {
                        @Override
                        public void onResponse(Call<ResponseSetUserInfo> call, retrofit2.Response<ResponseSetUserInfo> response) {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            if (response != null && response.body() != null) {
                                if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                                    Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getActivity(), ProfileMainActivity.class);
                                    startActivity(i);
                                } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                                    if (progressDialog != null && progressDialog.isShowing())
                                        progressDialog.dismiss();
                                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), response.body().getMsg());
                                }
                            } else {
                                if (progressDialog != null && progressDialog.isShowing())
                                    progressDialog.dismiss();
                                AppSettings.setLogin(false);
                                new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseSetUserInfo> call, Throwable t) {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    });

                } else {
                    Toast.makeText(getActivity(), R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //Set custom action bar
    private void setActionBar() {

    }

    public void getRSSPreferenceList() {

        showProgressBar();
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
        Call<ResponseGetInterestSubcategory> loginCall = getProfileRequest.getInterestSubcategory(AppSettings.getLoginUserId(), "1");
        loginCall.enqueue(new Callback<ResponseGetInterestSubcategory>() {
            @Override
            public void onResponse(Call<ResponseGetInterestSubcategory> call, retrofit2.Response<ResponseGetInterestSubcategory> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {

                        listCategories.addAll(response.body().getCategories());
                        categoriesAdapter.notifyDataSetChanged();/*
                        mAdapter = new MultiSelectionAdapter<>(getActivity(), response.body().getCategories());
                        mListView.setAdapter(mAdapter);*/
                        /*CommonDialogs commonDialogs = new CommonDialogs();
                        Dialog dialog = commonDialogs.showMultiSelectionListDialog(getActivity(), "Interest Categories", response.body().getCategories(), new DialogOkClickListener() {
                            @Override
                            public void onClick(String selectedInterestCategories, String categories) {
                                Toast.makeText(getActivity(), "Selected Categories: " + categories, Toast.LENGTH_LONG).show();
                                AppSettings.setInterestCategories(selectedInterestCategories);
                            }
                        });*/
                    }
                } else {
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), "Something went wrong..", "Please contact OnGraviti Admin.");
                }
            }

            @Override
            public void onFailure(Call<ResponseGetInterestSubcategory> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    private void showProgressBar() {
        try {
            progressDialog = new CustomProgressDialog(getActivity(), getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        } catch (Exception e) {
            Log.e("MyInterest", "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }
}