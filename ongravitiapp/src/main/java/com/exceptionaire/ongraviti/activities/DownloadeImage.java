package com.exceptionaire.ongraviti.activities;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Pratha on 1/1/2018.
 */

public class DownloadeImage extends AsyncTask {

    ImageDownloadListener listener;

    public DownloadeImage(ImageDownloadListener listener) {
        this.listener = listener;
    }

    @Override
    protected File doInBackground(Object[] objects) {
        return downloadFromUrl((String) objects[0], (String) objects[1]);
    }


    @Override
    protected void onPostExecute(Object o) {
        File file = (File) o;
        Log.d("DownloadManager", "downloaded file::" + file.getAbsolutePath());

        listener.onImageDownloaded(file);
    }

    public File downloadFromUrl(String DownloadUrl, String fileName) {

        try {
            File root = android.os.Environment.getExternalStorageDirectory();

            File dir = new File(root.getAbsolutePath() + "/picture");
            if (dir.exists() == false) {
                dir.mkdirs();
            }

            URL url = new URL(DownloadUrl); //you can write here any link
            File file = new File(dir, fileName);

            long startTime = System.currentTimeMillis();
            Log.d("DownloadManager", "download begining");
            Log.d("DownloadManager", "download url:" + url);
            Log.d("DownloadManager", "downloaded file name:" + fileName);

           /* Open a connection to that URL. */
            URLConnection ucon = url.openConnection();

           /*
            * Define InputStreams to read from the URLConnection.
            */
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

           /*
            * Read bytes to the Buffer until there is nothing more to read(-1).
            */
            ByteArrayBuffer baf = new ByteArrayBuffer(5000);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }


           /* Convert the Bytes read to a String. */
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baf.toByteArray());
            fos.flush();
            fos.close();
            Log.d("DownloadManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");
            return file;

        } catch (IOException e) {
            Log.d("DownloadManager", "Error: " + e);
            return null;
        }
    }


    public interface ImageDownloadListener {
        void onImageDownloaded(File file);
    }
}
