package com.exceptionaire.ongraviti.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.fragments.EditProfileWhoAmIFragment;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.ResponseGetUserProfileData;
import com.exceptionaire.ongraviti.model.ResponseSetUserInfo;
import com.exceptionaire.ongraviti.model.UserBasicInfo;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditProfileDetailsActivity extends BaseDrawerActivity {

    private ProgressDialog progressDialog;
    private EditProfileWhoAmIFragment fragment;
    private ResponseGetUserProfileData responseGetUserProfileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_edit_profile_details, frameLayout);

        LOG_TAG = "Edit Profile Details";
        setCationBar();
       /* if (isNetworkAvailable()) {
            getUserProfileData();

        } else {
            Toast.makeText(this, R.string.error_no_internet_connection, Toast.LENGTH_SHORT).show();
        }*/
        getUserProfileData();
        fragment = new EditProfileWhoAmIFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container_edit_profile_details, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }


    private void setCationBar() {
        arcMenu.setVisibility(View.VISIBLE);
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        TextView header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText("Edit Profile");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(EditProfileDetailsActivity.this, ProfileMainActivity.class);
        startActivity(i);
    }


//    public void getUserProfileData() {
//        Log.d(LOG_TAG, "## getUserProfileData");
//        showProgressBar();
//
//        Retrofit retrofit = ApiClient.getClient();
//        ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
//        Call<ResponseGetUserProfileData> loginCall = getProfileRequest.getUserProfileData(AppSettings.getLoginUserId());
//        loginCall.enqueue(new Callback<ResponseGetUserProfileData>() {
//            @Override
//            public void onResponse(Call<ResponseGetUserProfileData> call, retrofit2.Response<ResponseGetUserProfileData> response) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
//                if (response != null && response.body() != null) {
//                    if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
////                        setDataInControls(response.body());
//                        // converts object to json string
//                        String jsonStr = new Gson().toJson(response.body());
//                        AppSettings.setProfile(jsonStr);
//                        Log.i(LOG_TAG, "Profile -- >" + jsonStr);
//                        fragment.setDataInControls(response.body());
//                        responseGetUserProfileData = response.body();
//                        Gson gson = new Gson();
//                        String json = gson.toJson(responseGetUserProfileData);
//                        AppSettings.setProfileDetails(json);
//                    }
//                } else {
//                    AppSettings.setLogin(false);
//                    new CommonDialogs().showMessageDialogNonStatic(EditProfileDetailsActivity.this, "Something went wrong..", "Please contact OnGraviti Admin.");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseGetUserProfileData> call, Throwable t) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
//            }
//        });
//    }

    private void showProgressBar() {
        try {
            progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        } catch (Exception e) {
            Log.e(LOG_TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public ResponseGetUserProfileData getUserProfileDataFromResponse() {

        String data = AppSettings.getProfileDetails();
        Gson gson = new Gson();
        responseGetUserProfileData = gson.fromJson(data, ResponseGetUserProfileData.class);
        return responseGetUserProfileData;
    }

    /*
        public void setUserDetails() {
            Gson gson = new Gson();
            String json = gson.toJson(responseGetUserProfileData);
            AppSettings.setProfileDetails(json);
        }*/
    public void getUserProfileData() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        if (isNetworkAvailable()) {

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
            Call<ResponseGetUserProfileData> loginCall = getProfileRequest.getUserProfileData(AppSettings.getLoginUserId(), "");
            loginCall.enqueue(new Callback<ResponseGetUserProfileData>() {
                @Override
                public void onResponse(Call<ResponseGetUserProfileData> call, Response<ResponseGetUserProfileData> response) {
                    if (response != null && response.body() != null) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {

                            Gson gson = new Gson();
                            String json = gson.toJson(response.body());
                            AppSettings.setProfileDetails(json);
                        }
                    } else {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        new CommonDialogs().showMessageDialogNonStatic(EditProfileDetailsActivity.this, getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                    }
                }

                @Override
                public void onFailure(Call<ResponseGetUserProfileData> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(this, getResources().getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void saveUserProfileDataToServer() {
        Log.d(LOG_TAG, "## getUserProfileData");

        String data = AppSettings.getProfileDetails();
        Gson gson = new Gson();
        ResponseGetUserProfileData responseGetUserProfileData = gson.fromJson(data, ResponseGetUserProfileData.class);
        if (responseGetUserProfileData != null) {
            UserBasicInfo basic_user_info = responseGetUserProfileData.getUser_basic_info()[0];

            if (basic_user_info != null) {
                showProgressBar();

                Retrofit retrofit = ApiClient.getClient();
                ApiInterface getProfileRequest = retrofit.create(ApiInterface.class);
                Call<ResponseSetUserInfo> loginCall = getProfileRequest.setUserProfileData(
                        AppSettings.getLoginUserId(),
                        basic_user_info.getName(),
                        basic_user_info.getCity(),
                        basic_user_info.getTraits(),
                        basic_user_info.getDate_of_birth(),
                        basic_user_info.getGender(),
                        basic_user_info.getInterested_in(),
                        basic_user_info.getRelationship_status(),
                        basic_user_info.getBeliefs_religious_beliefs(),
                        basic_user_info.getPolitical_beliefs(),
                        basic_user_info.getHeight(),
                        basic_user_info.getWeight(),
                        basic_user_info.getBody_type(),
                        basic_user_info.getEye_color(),
                        basic_user_info.getHair_color(),
                        basic_user_info.getSmoking(),
                        basic_user_info.getDrinking(),
                        basic_user_info.getGym(),
                        basic_user_info.getFavorite_book(),
                        basic_user_info.getFavorite_shows(),
                        basic_user_info.getFavorite_movies(),
                        basic_user_info.getCountries_travelled(),
                        basic_user_info.getEvents_attended(),
                        basic_user_info.getHobbies(),
                        basic_user_info.getHighest_degree(),
                        basic_user_info.getDegree_name(),
                        basic_user_info.getCollege_university_name(),
                        basic_user_info.getEmployment_status(),
                        basic_user_info.getFuture_goal(),
                        basic_user_info.getArea_of_interest(),
                        basic_user_info.getInteresting_ventures_to_look(),
                        basic_user_info.getEnterpreneurs_i_like(),
                        basic_user_info.getEvents_attended_i_want(),
                        basic_user_info.getEvents_m_looking(),
                        basic_user_info.getIndustries_i_want_to_target(),
                        basic_user_info.getBooks_i_like(),
                        basic_user_info.getwhat_i_read(),
                        basic_user_info.get_interest_category());

                loginCall.enqueue(new Callback<ResponseSetUserInfo>() {
                    @Override
                    public void onResponse(Call<ResponseSetUserInfo> call, retrofit2.Response<ResponseSetUserInfo> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        if (response != null && response.body() != null) {
                            if (response.body().getStatus().equalsIgnoreCase(RESPONSE_SUCCESS)) {
                                Toast.makeText(EditProfileDetailsActivity.this, "your information stored successfully", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        } else {
                            new CommonDialogs().showMessageDialogNonStatic(EditProfileDetailsActivity.this, "Something went wrong..", "Please contact OnGraviti Admin.");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseSetUserInfo> call, Throwable t) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            }
        } else {
            final Dialog dialog = CommonDialogs.showRemoveMessageDialog(EditProfileDetailsActivity.this, getResources().getString(R.string.ongravity), "Server not responding please try again after some time.");
            Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
            dialog_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
    }
}