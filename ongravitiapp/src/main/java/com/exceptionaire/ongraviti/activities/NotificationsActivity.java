package com.exceptionaire.ongraviti.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.chats.ChatsMainActivity;
import com.exceptionaire.ongraviti.activities.myorbit.MyOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.listener_interface.OnLoadMoreListener;
import com.exceptionaire.ongraviti.model.NotificationDetails;
import com.exceptionaire.ongraviti.model.ResponseNotificationsList;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

/**
 * Created by root on 13/02/17.
 */

public class NotificationsActivity extends BaseDrawerActivity implements AppConstants {

    private CustomProgressDialog progressDialog;
    private RecyclerView mRecyclerView;
    private List<NotificationDetails> notificationDetailsListMaster = new ArrayList<>();
    //    private ImageLoader imageLoader = OnGravitiApp.getInstance().getImageLoader();
    private NotificationsListAdapter notificationListAdapter;
    private Boolean isLoadingItemAdded = false;
    private String nextPageItem = "0";
    TextView tv_msg;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_notifications, frameLayout);
        TextView header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText(getResources().getString(R.string.my_notifications));

        initialiseComponent();
    }

    public void initialiseComponent() {
        tv_msg = (TextView) findViewById(R.id.tv_msg);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleViewNotifications);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        notificationListAdapter = new NotificationsListAdapter();
        mRecyclerView.setAdapter(notificationListAdapter);

        notificationListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("haint", "Load More");

                if (!nextPageItem.equals("NA")) {
                    notificationDetailsListMaster.add(null);
                    notificationListAdapter.notifyItemInserted(notificationDetailsListMaster.size() - 1);
                    isLoadingItemAdded = true;

                    //Load more data for reyclerview
                    getNotificationsList(nextPageItem);
                } else {
//                    Toast.makeText(NotificationsActivity.this, getString(R.string.no_more_data), Toast.LENGTH_SHORT).show();
                }
            }
        });

        getNotificationsList(nextPageItem);
    }


    private static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView textViewNotificationText, tv_date;
        RelativeLayout relativeLayoutNotification;
        RoundedImageView roundedImageView;
        View view_cat_type;

        UserViewHolder(View itemView) {
            super(itemView);
            relativeLayoutNotification = (RelativeLayout) itemView.findViewById(R.id.notifications_list_parent);
            textViewNotificationText = (TextView) itemView.findViewById(R.id.textview_notificaton_text);
            roundedImageView = (RoundedImageView) itemView.findViewById(R.id.user_profile_pic);
            view_cat_type = (View) itemView.findViewById(R.id.view_cat_type);
            tv_date = itemView.findViewById(R.id.tv_date);
        }
    }

    private static class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, HomeUserPostsActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }


    private class NotificationsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final int
                VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;

        private OnLoadMoreListener mOnLoadMoreListener;

        private boolean isLoading;
        private int visibleThreshold = 1;
        private int lastVisibleItem, totalItemCount;

        NotificationsListAdapter() {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }

        void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        @Override
        public int getItemViewType(int position) {
            return notificationDetailsListMaster.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(NotificationsActivity.this).inflate(R.layout.item_notification_list, parent, false);
                return new UserViewHolder(view);
            } else if (viewType == VIEW_TYPE_LOADING) {
                View view = LayoutInflater.from(NotificationsActivity.this).inflate(R.layout.layout_loading_item, parent, false);
                return new LoadingViewHolder(view);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof UserViewHolder) {
                NotificationDetails notificationDetails = notificationDetailsListMaster.get(position);
                UserViewHolder userViewHolder = (UserViewHolder) holder;
                if (notificationDetails.getCategory_type().equals("1")) {
                    userViewHolder.view_cat_type.setBackgroundResource(R.color.MediumSeaGreen);
                } else if (notificationDetails.getCategory_type().equals("2")) {
                    userViewHolder.view_cat_type.setBackgroundResource(R.color.colorAccent);
                } else if (notificationDetails.getCategory_type().equals("3")) {
                    userViewHolder.view_cat_type.setBackgroundResource(R.color.colorPrimary);
                }

                userViewHolder.tv_date.setText(notificationDetails.getUpdated_date());
                userViewHolder.textViewNotificationText.setText(notificationDetails.getNotification());
//                Picasso.with(getActivity()).load(AppConstants.PROFILE_PICTURE_PATH + "/" + notificationDetails.getProfile_pic()).fit().centerCrop()
//                        .placeholder(R.drawable.profile)
//                        .error(R.drawable.profile)
//                        .into(userViewHolder.roundedImageView);
                if (notificationDetails.getProfile_pic() != null) {
                    if (!notificationDetails.getProfile_pic().contains("https:")) {
                        Picasso.with(getActivity())
                                .load(AppConstants.PROFILE_PICTURE_PATH + "/" + notificationDetails.getProfile_pic())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(userViewHolder.roundedImageView);
                    } else if (notificationDetails.getProfile_pic().contains("https:")) {
                        Picasso.with(getActivity())
                                .load(notificationDetails.getProfile_pic())
                                .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                                .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                                .into(userViewHolder.roundedImageView);
                    }
                }

                userViewHolder.relativeLayoutNotification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String action = notificationDetails.getaction();
                        if (action.equals(NOTIFICATION_ACTION_FAVOURITE)) {
                            Intent resultIntent = new Intent(getApplicationContext(), FavoriteUserInfoActivity.class);
                            resultIntent.putExtra("ActivityName", "OrbitProfile");
                            resultIntent.putExtra("OrbitUserID", "" + notificationDetails.getSender_user_id());
                            startActivity(resultIntent);
                        } else if (action.equals(NOTIFICATION_ACTION_ORBIT_REQUEST)) {
                            Intent resultIntent = new Intent(getApplicationContext(), MyOrbitActivity.class);
                            resultIntent.putExtra("page_index", "1");
                            startActivity(resultIntent);
                        } else if (action.equals(NOTIFICATION_ACTION_ORBIT_REQUEST_ACCEPTED)) {
                            Intent resultIntent = new Intent(getApplicationContext(), FavoriteUserInfoActivity.class);
                            resultIntent.putExtra("ActivityName", "OrbitProfile");
                            resultIntent.putExtra("OrbitUserID", "" + notificationDetails.getSender_user_id());
                            startActivity(resultIntent);
                        } else if (action.equals(NOTIFICATION_ACTION_ASK_DATING)) {
                            Intent resultIntent = new Intent(getApplicationContext(), AcceptDatingRequest.class);
                            resultIntent.putExtra("ActivityName", "OrbitProfile");
                            resultIntent.putExtra("OrbitUserID", "" + notificationDetails.getSender_user_id());
                            startActivity(resultIntent);
                        } else if (action.equals(NOTIFICATION_ACTION_DATING_REQUEST_ACCEPTED)) {
                            Intent resultIntent = new Intent(getApplicationContext(), ChatsMainActivity.class);
                            resultIntent.putExtra("ActivityName", "OrbitProfile");
                            resultIntent.putExtra("OrbitUserID", "" + notificationDetails.getSender_user_id());
                            startActivity(resultIntent);
                        }/*else {
                        Intent i = new Intent(NotificationsActivity.this, FavoriteUserInfoActivity.class);
                            i.putExtra("ActivityName", "NotificationsActivity");
                            i.putExtra("UserID", notificationDetails.getSender_user_id());
                            startActivity(i);
                        }*/
                    }
                });

            } else if (holder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
            }
        }

        @Override
        public int getItemCount() {
            return notificationDetailsListMaster == null ? 0 : notificationDetailsListMaster.size();
        }

        void setLoaded() {
            isLoading = false;
        }
    }

    private void getNotificationsList(String pageNextCnt) {

        if (!isLoadingItemAdded) {
            progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }

        Log.e(LOG_TAG, " User ID ==== > " + AppSettings.getLoginUserId());

        Retrofit retrofit = ApiClient.getClient();
        final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<ResponseNotificationsList> accessTokenCall = requestInterface.getNotificationsList(AppSettings.getLoginUserId(), pageNextCnt);
        accessTokenCall.enqueue(new Callback<ResponseNotificationsList>() {
            @Override
            public void onResponse(Call<ResponseNotificationsList> call, retrofit2.Response<ResponseNotificationsList> response) {

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                boolean flagIsnextPageAvailable = false;
                if (response != null && response.body() != null) {
                    if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                        if (response.body().getNotificationCount() != null && !response.body().getNotificationCount().isEmpty() && !response.body().getNotificationCount().equalsIgnoreCase("")) {
                            int count = Integer.parseInt(response.body().getNotificationCount());
                            if (count > 10)
                                flagIsnextPageAvailable = true;
                        }

                        if (response.body().getNotification_details() != null) {
                            List<NotificationDetails> notificationDetailsList = new ArrayList<>(Arrays.asList(response.body().getNotification_details()));
                            if (flagIsnextPageAvailable) {
                                nextPageItem = notificationDetailsList.get(notificationDetailsList.size() - 1).getNotification_id();
                            } else {
                                nextPageItem = "NA";
                            }
                            //Remove loading item
                            if (isLoadingItemAdded) {
                                notificationDetailsListMaster.remove(notificationDetailsListMaster.size() - 1);
                                notificationListAdapter.notifyItemRemoved(notificationDetailsListMaster.size());
                                isLoadingItemAdded = false;
                            }

                            notificationDetailsListMaster.addAll(notificationDetailsList);
                            notificationListAdapter.setLoaded();
                            notificationListAdapter.notifyDataSetChanged();

//                            AppSettings.setMobileNo(response.body().getData()[0].get());
//                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
//                            finish();
                        } else {
                            tv_msg.setVisibility(View.VISIBLE);
//                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        tv_msg.setVisibility(View.VISIBLE);
//                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    AppSettings.setLogin(false);
                    new CommonDialogs().showMessageDialogNonStatic(getActivity(), getResources().getString(R.string.ongravity), getResources().getString(R.string.error_server_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseNotificationsList> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                Log.e("Error: ", t.toString());
            }
        });
    }
}
