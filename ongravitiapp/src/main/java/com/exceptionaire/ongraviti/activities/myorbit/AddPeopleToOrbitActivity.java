package com.exceptionaire.ongraviti.activities.myorbit;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.AddOrbitUsersAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.search.AddOrbitAutoCompleteTextChangedListener;
import com.exceptionaire.ongraviti.activities.base.search.AutocompleteCustomArrayAdapter;
import com.exceptionaire.ongraviti.activities.base.search.DatabaseHandler;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.OrbitRequest;
import com.exceptionaire.ongraviti.model.ResponseAcceptRejectOrbitRequest;
import com.exceptionaire.ongraviti.model.ResponseSuccess;
import com.exceptionaire.ongraviti.model.SearchData;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by Laxmikant on 16/6/17.
 */

public class AddPeopleToOrbitActivity extends BaseDrawerActivity {

    private ProgressDialog progressDialog;
    private ArrayList<OrbitRequest> orbitRequests;
    private AddOrbitUsersAdapter orbitAdapter;
    public ListView list_view_contact;
    private Context context;
    private TextView header;
    public static TextView tv_msg;
    public EditText myAutoComplete;
    public ArrayAdapter<SearchData> myAdapter;

    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        rootView = getLayoutInflater().inflate(R.layout.activity_add_orbit_friends, frameLayout);

        initViews();
        initSearchFunction();
    }

    private void initViews() {
        header = (TextView) findViewById(R.id.toolbar_title_drawer);
        header.setText("Add People In Orbit");
        orbitAdapter = new AddOrbitUsersAdapter(new ArrayList<OrbitRequest>(), this);
        progressDialog = new CustomProgressDialog(this, "Loading...");
        list_view_contact = (ListView) findViewById(R.id.list_view_contact);
        list_view_contact.setAdapter(orbitAdapter);
        tv_msg = (TextView) rootView.findViewById(R.id.tv_msg);
        final EditText et_search = (EditText) findViewById(R.id.et_search);
        ImageButton ibtn_invite = (ImageButton) findViewById(R.id.ibtn_invite);
        ibtn_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(context, InviteAFriendActivity.class));
//                finish();
            }
        });

        //showOrbitFriendList();

        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                String text = et_search.getText().toString().toLowerCase(Locale.getDefault());
                orbitAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }
        });
    }

    private void initSearchFunction() {
        databaseH = new DatabaseHandler(this);
        myAutoComplete = (EditText) rootView.findViewById(R.id.autoCompleteTextView);
        myAutoComplete.addTextChangedListener(new AddOrbitAutoCompleteTextChangedListener(AddPeopleToOrbitActivity.this, 2));

        itemDataArray = new SearchData[0];
        myAdapter = new AutocompleteCustomArrayAdapter(this, 2, R.layout.list_view_row, itemDataArray);
        list_view_contact.setAdapter(myAdapter);
//        myAutoComplete.setText(" ");
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    public void sendFriendRequest(final String receiver_user_id) {
        progressDialog.setMessage(AddPeopleToOrbitActivity.this.getString(R.string.msg_sending_friend_request));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = format.format(new Date());
        Retrofit retrofit = ApiClient.getClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<ResponseSuccess> loginCall = apiInterface.sendOrbitRequest(AppSettings.getLoginUserId(), receiver_user_id, currentDateandTime);
        loginCall.enqueue(new Callback<ResponseSuccess>() {
            @Override
            public void onResponse(Call<ResponseSuccess> call, retrofit2.Response<ResponseSuccess> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {//success
                    final Dialog dialog = CommonDialogs.showMessageDialog(AddPeopleToOrbitActivity.this, getResources().getString(R.string.ongraviti), response.body().getMsg());
                    Button dialog_ok = (Button) dialog.findViewById(R.id.button_ok);
                    dialog_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
//                            startActivity(new Intent(AddPeopleToOrbitActivity.this, MyOrbitActivity.class));
//                            overridePendingTransition(R.anim.slide_in_right,
//                                    R.anim.slide_out_left);
                            finish();
                        }
                    });

                } else if (response.body().getStatus().equalsIgnoreCase(RESPONSE_ERROR)) {
                    CommonDialogs.showMessageDialog(AddPeopleToOrbitActivity.this, getResources().getString(R.string.ongraviti), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<ResponseSuccess> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Toast.makeText(AddPeopleToOrbitActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
