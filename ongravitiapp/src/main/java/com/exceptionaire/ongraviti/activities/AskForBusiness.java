package com.exceptionaire.ongraviti.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.adapters.DatingCardAdapter;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.ResponseAskForDatingNFavouriteUser;
import com.exceptionaire.ongraviti.model.ResponseDatingUser;
import com.exceptionaire.ongraviti.model.ResponseDatingUsersList;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import SwipeCard.SwipeCardView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

public class AskForBusiness extends BaseDrawerActivity implements
        DatingCardAdapter.datingAddtoFavoriteListener, DatingCardAdapter.DatingChangeDataListener {

    String TAG = "AskForBusiness";
    private DatingCardAdapter cardAdapter;
    SwipeCardView swipeCardView;
    private TextView header, textAge, textViewUserName,
            textViewOccupation;
    private ImageView coffee, drink, play;
    private Button seeYaLater;
    private String status;

    private ArrayList<ResponseDatingUser> listDatingUsers; //actual list
    private ArrayList<ResponseDatingUser> listDatingUsersTemp; //actual list
    private int current_user_index = 0;//, current_user_index_1 = 0;

    //volly request variables
    // private int WESERVICE_FLAG;
    // private RequestQueue requestQueue;
    // private CustomVolleyRequest customVolleyRequest;
    private ProgressDialog progressDialog;
    Context context;

    final String ask_for_type = "2";
    int display_index = 0;
    ResponseDatingUser datingUsers_to_display;
    boolean is_dialog_open = false;
    TextView tv_msg;
    Dialog dialog;
    LinearLayout linear_ask;
    String receiver_user_id, ask_for_cat;//(1-Coffee,2-Drink), ask_for_type=(1-Dating,2-Business)
    private ImageView imageViewBack;
    private TextView textViewHeader;
    FrameLayout ask_for_dating_framelayout;
    FrameLayout frame;
    LinearLayout dragView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ask_for_dating);
        //  getLayoutInflater().inflate(R.layout.ask_for_dating, frameLayout);
        arcMenu.setVisibility(View.VISIBLE);
        try {
            context = this;
//        fill Data In List
//        fillDataInList();
            initView();
            frame = (FrameLayout) findViewById(R.id.frame);
            frame.setBackgroundResource(R.drawable.business_img);
            ask_for_dating_framelayout.setVisibility(View.INVISIBLE);
            seeYaLater.setVisibility(View.INVISIBLE);
            dragView = (LinearLayout) findViewById(R.id.dragView);
            dragView.setVisibility(View.INVISIBLE);
            initListners();
            tv_msg = (TextView) findViewById(R.id.tv_msg);
            tv_msg.setVisibility(View.VISIBLE);
            //TODO uncomment api calling
//            callDatingUsersListApi();

            setFlingListener();
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void setFlingListener() {
        try {
            swipeCardView.setFlingListener(new SwipeCardView.OnCardFlingListener() {
                @Override
                public void onCardExitLeft(Object dataObject) {

                    Log.d(TAG, "## onCardExitLeft : ");
                    ResponseDatingUser datingUsers = (ResponseDatingUser) dataObject;
                    receiver_user_id = datingUsers.getUserId();
                    ask_for_cat = "2";

                    listDatingUsersTemp.add(datingUsers);
                    cardAdapter.notifyDataSetChanged();

                    if (is_dialog_open) {
                    } else
                        showDialogForActivites(3);
                }

                @Override
                public void onCardExitRight(Object dataObject) {

                    Log.d(TAG, "## onCardExitRight : " + dataObject);
                    ResponseDatingUser datingUsers = (ResponseDatingUser) dataObject;
                    receiver_user_id = datingUsers.getUserId();
                    ask_for_cat = "1";

                    listDatingUsersTemp.add(datingUsers);
                    cardAdapter.notifyDataSetChanged();

                    if (is_dialog_open) {
                    } else
                        showDialogForActivites(1);
                }

                @Override
                public void onAdapterAboutToEmpty(int itemsInAdapter) {
                    Log.d(TAG, "## onAdapterAboutToEmpty : " + itemsInAdapter);
//                getDummyData();
                /*fillDataInList();
                cardAdapter.notifyDataSetChanged();*/
                }

                @Override
                public void onScroll(float scrollProgressPercent) {
                    Log.d(TAG, "## onScroll");
                }

                @Override
                public void onCardExitTop(Object dataObject) {

                    Log.d(TAG, "## onCardExitTop : " + dataObject);
                    ResponseDatingUser datingUsers = (ResponseDatingUser) dataObject;
                    receiver_user_id = datingUsers.getUserId();
                    ask_for_cat = "3";

                    listDatingUsersTemp.add(datingUsers);
                    cardAdapter.notifyDataSetChanged();

                    if (is_dialog_open) {
                    } else
                        showDialogForActivites(2);
                }

                @Override
                public void onCardExitBottom(Object dataObject) {
                    Log.d(TAG, "## onCardExitBottom ");

                    ResponseDatingUser datingUsersLastCheck = (ResponseDatingUser) dataObject;
                    String current_userid = datingUsersLastCheck.getUserId();
                    if (!datingUsersLastCheck.getUserId().equals(listDatingUsers.get(listDatingUsers.size() - 1).getUserId())) {
                        for (int i = 0; i < listDatingUsers.size(); i++) {
                            if (current_userid.equals(listDatingUsers.get(i).getUserId())) {
                                display_index = i + 1;
                                datingUsers_to_display = (ResponseDatingUser) listDatingUsers.get(i + 1);
                            }
                        }
                        Log.d(TAG, "## display_index : " + display_index);

                        listDatingUsersTemp.set(listDatingUsersTemp.size() - 1, datingUsers_to_display);
                        listDatingUsersTemp.add(datingUsers_to_display);

                        cardAdapter.notifyDataSetChanged();
                    } else {
                        Log.d(TAG, "## Bottom last ELSE");
                        swipeCardView.setVisibility(View.GONE);
                        frameLayout.setBackgroundResource(R.drawable.thats_all_folks);
//                    swipeCardView.setBackgroundResource(R.drawable.thats_all_folks);
                        coffee.setVisibility(View.GONE);
                        drink.setVisibility(View.GONE);
//                        confused.setVisibility(View.GONE);
                        seeYaLater.setVisibility(View.GONE);
                        linear_ask.setVisibility(View.GONE);
                        play.setVisibility(View.GONE);
                    }
                }
            });

            // Optionally add an OnItemClickListener
            swipeCardView.setOnItemClickListener(
                    new SwipeCardView.OnItemClickListener() {
                        @Override
                        public void onItemClicked(int itemPosition, Object dataObject) {
                       /* DatingCard card = (DatingCard) dataObject;
                        makeToast(AskForBusiness.this, card.name);*/
                        }
                    });
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void callAskForApi(String message) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());
            //  WESERVICE_FLAG = 2;  // 2 flag for ask for coffee, drink and confused
            Map<String, String> map = new HashMap<>();
            //map.put(KEY_USERNAME, user_email);

            showProgressBar();
            map.put("user_id", AppSettings.getLoginUserId()); //mandatory
            map.put("receiver_user_id", receiver_user_id); //mandatory
            map.put("ask_for_cat", ask_for_cat); //mandatory
            map.put("ask_for_type", ask_for_type); //mandatory
            map.put("message", message);
            map.put("device_date", currentDateandTime);//mandatory

            Log.d(TAG, "## ask for map : " + map.toString());

            Retrofit retrofit = ApiClient.getClient();
            final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseAskForDatingNFavouriteUser> accessTokenCall = requestInterface.setAskForDatingEvent(map);
            accessTokenCall.enqueue(new Callback<ResponseAskForDatingNFavouriteUser>() {
                @Override
                public void onResponse(Call<ResponseAskForDatingNFavouriteUser> call, retrofit2.Response<ResponseAskForDatingNFavouriteUser> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    is_dialog_open = false;
                    Log.d(TAG, "## response : " + response + " ");

                    status = response.body().getStatus();
                    String msg = response.body().getMsg();
                    if (status.equalsIgnoreCase("success")) {
                        CommonDialogs.dialog_with_one_btn_without_title(AskForBusiness.this, getResources().getString(R.string.success));
//                    CommonDialogs.showdialog(context, getResources().getString(R.string.success), msg);
                    }
                }

                @Override
                public void onFailure(Call<ResponseAskForDatingNFavouriteUser> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Something went wrong, please try after some time.", Toast.LENGTH_SHORT).show();
                    Log.e("Error: ", t.toString());
                }
            });

            // requestQueue = Volley.newRequestQueue(context);
            // customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.SET_ASK_FOR_DATING_API, map, this, this);
            // requestQueue.add(customVolleyRequest);
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void initListners() {
        try {
//        addtoFavorite.setOnClickListener(this);
            coffee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    swipeCardView.throwRight();
                    if (is_dialog_open) {
                    } else
                        showDialogForActivites(1);
                }
            });
           /* confused.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    swipeCardView.throwTop();
                    if (is_dialog_open) {
                    } else
                        showDialogForActivites(2);
                }
            });*/
            drink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    swipeCardView.throwLeft();
                    if (is_dialog_open) {
                    } else
                        showDialogForActivites(3);
                }
            });
            seeYaLater.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    swipeCardView.throwBottom();
                }
            });
            /*gotoFavorites.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(AskForBusiness.this, MyFavoritesActivity.class));
                }
            });*/
            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(AskForBusiness.this, "play", Toast.LENGTH_SHORT).show();
                }
            });
            imageViewBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    static void makeToast(Context ctx, String s) {
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void initView() {
        try {
            ask_for_dating_framelayout = (FrameLayout) findViewById(R.id.ask_for_dating_framelayout);
            //  header = (TextView) findViewById(R.id.toolbar_title_drawer);
            //  header.setText(getResources().getString(R.string.business));
//        addtoFavorite = (ImageButton) findViewById(R.id.ask_for_dating_add_to_favorite);
            textViewHeader = (TextView) findViewById(R.id.text_view_title);
            textViewHeader.setText(getResources().getString(R.string.business));
            coffee = (ImageView) findViewById(R.id.ask_for_dating_coffee);
            drink = (ImageView) findViewById(R.id.ask_for_dating_drink);
            play = (ImageView) findViewById(R.id.image_view_play);
            textAge = (TextView) findViewById(R.id.text_view_no);
            textViewUserName = (TextView) findViewById(R.id.text_view_user_name);
            textViewOccupation = (TextView) findViewById(R.id.text_view_user_occupation);
//            confused = (ImageView) findViewById(R.id.ask_for_dating_confused);
            seeYaLater = (Button) findViewById(R.id.ask_for_dating_see_ya_later);
//            txtUserName = (TextView) findViewById(R.id.go_to_favorites);
            swipeCardView = (SwipeCardView) findViewById(R.id.swipeCardViewDating);
            linear_ask = (LinearLayout) findViewById(R.id.linear_ask);
            imageViewBack = (ImageView) findViewById(R.id.image_view_back);

            listDatingUsersTemp = new ArrayList<>();
            listDatingUsers = new ArrayList<>();
            if (listDatingUsers.size() > 0) {
                for (int i = current_user_index; i < 2; i++) {
                    ResponseDatingUser datingUsers = listDatingUsers.get(current_user_index);
                    listDatingUsersTemp.add(datingUsers);
                    current_user_index = 0;
                    Log.d(TAG, "## listDatingUsersTemp : " + listDatingUsersTemp.size());
                    Log.d(TAG, "## listDatingUsersTemp : " + listDatingUsersTemp.get(0).getProfilePicture());
                }
                current_user_index++;
            }

            cardAdapter = new DatingCardAdapter(AskForBusiness.this, listDatingUsersTemp);
            cardAdapter.setdatingAddtoFavorite(AskForBusiness.this);
            cardAdapter.setDatingChangeDataListener(AskForBusiness.this);
            swipeCardView.setAdapter(cardAdapter);

        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }


    private void showDialogForActivites(int whichDialog) {
        try {
            int FLAG = whichDialog;

            switch (FLAG) {
                case 1:
                    dialog = new Dialog(context, R.style.DialogStyle);

                    //dialog initialization
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_dating_business);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    // done with initialization

                    Button btnSend = (Button) dialog.findViewById(R.id.btnSend);
                    Button btnCacel = (Button) dialog.findViewById(R.id.btnCancel);

                    final EditText edtMessage = (EditText) dialog.findViewById(R.id.edtMessage);

                    btnSend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (edtMessage.length() == 0) {
                                edtMessage.setError("Please enter message");
                            } else {
                                is_dialog_open = false;
                                String dialogText = edtMessage.getText().toString();
                                dialog.dismiss();
                                callAskForApi(dialogText);
                            }
                        }
                    });

                    btnCacel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            is_dialog_open = false;
                            dialog.dismiss();
                        }
                    });

                    is_dialog_open = true;
                    dialog.show();

                    break;
                case 3:
                    dialog = new Dialog(context, R.style.DialogStyle);

                    //dialog initialization
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_dating_business);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    // done with initialization

                    Button btnSend1 = (Button) dialog.findViewById(R.id.btnSend);
                    Button btnCacel1 = (Button) dialog.findViewById(R.id.btnCancel);

                    final EditText edtMessage1 = (EditText) dialog.findViewById(R.id.edtMessage);

                    ImageView img_dating = (ImageView) dialog.findViewById(R.id.img_dating);
                    img_dating.setImageResource(R.drawable.drink_blue);

                    btnSend1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (edtMessage1.length() == 0) {
                                edtMessage1.setError("Please enter message");
                            } else {
                                is_dialog_open = false;
                                String dialogText = edtMessage1.getText().toString();
                                dialog.dismiss();
                                callAskForApi(dialogText);
                            }
                        }
                    });

                    btnCacel1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            is_dialog_open = false;
                            dialog.dismiss();
                        }
                    });

                    is_dialog_open = true;
                    dialog.show();
                    break;
            }
        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void ondatingAddtoFavoriteClickListner(int favUserID) {
        try {
            showProgressBar();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = format.format(new Date());
            //  WESERVICE_FLAG = 3;  // 3 favorite
            Map<String, String> map = new HashMap<>();

            map.put("user_id", AppSettings.getLoginUserId()); //mandatory
            map.put("favorite_user_id", String.valueOf(favUserID)); //mandatory
            map.put("profile_cat_id", PROFILE_CATEGORY_THREE_BUSINESS);
            map.put("device_date", currentDateandTime);

            Log.d(TAG, "## favorite map : " + map.toString());

            Retrofit retrofit = ApiClient.getClient();
            final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            Call<ResponseAskForDatingNFavouriteUser> accessTokenCall = requestInterface.setFavouriteUser(map);
            accessTokenCall.enqueue(new Callback<ResponseAskForDatingNFavouriteUser>() {
                @Override
                public void onResponse(Call<ResponseAskForDatingNFavouriteUser> call, retrofit2.Response<ResponseAskForDatingNFavouriteUser> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    Log.d(TAG, "## response : " + response + " ");

                    status = response.body().getStatus();
                    String msg = response.body().getMsg();
                    if (status.equalsIgnoreCase("success")) {
                        CommonDialogs.dialog_with_one_btn_without_title(AskForBusiness.this, getResources().getString(R.string.success));
//                    CommonDialogs.showdialog(context, getResources().getString(R.string.success), msg);
                    }
                }

                @Override
                public void onFailure(Call<ResponseAskForDatingNFavouriteUser> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Something went wrong, please try after some time.", Toast.LENGTH_SHORT).show();
                    Log.e("Error: ", t.toString());
                }
            });


            // requestQueue = Volley.newRequestQueue(context);
            //customVolleyRequest = new CustomVolleyRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.SET_FAVORITE_USER, map, this, this);
            // requestQueue.add(customVolleyRequest);

        } catch (Exception e) {
            Log.e(TAG, "## e : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }


    @Override
    public void onDataChange(ResponseDatingUser user) {
        if (user != null) {
            String age = getAge(user.getDateOfBirth());
            textViewUserName.setText(user.getCName());
            textAge.setText(Html.fromHtml("<sup>" + age + "</sup>"));
            String occupation = "";
            if (user.getHighestDegree() != null && user.getHighestDegree().length() > 0) {
                occupation = user.getHighestDegree();
            }
            if (user.getCollegeUniversityName() != null && user.getCollegeUniversityName().length() > 0) {
                occupation = occupation + " from " + user.getCollegeUniversityName();
            }
            if (occupation.length() == 0) {
                textViewOccupation.setVisibility(View.GONE);
            } else {
                textViewOccupation.setVisibility(View.VISIBLE);
                textViewOccupation.setText(occupation);
            }

        }
    }

    private String getAge(String dateOfBirth) {

        int age = 0;
        if (dateOfBirth.length() > 0) {
            String date[] = new String[3];
            if (dateOfBirth.contains("/")) {
                date = dateOfBirth.split("/");
            } else if (dateOfBirth.contains("-")) {
                date = dateOfBirth.split("-");
            }
            int year = Integer.parseInt(date[2]);
            int month = Integer.parseInt(date[1]);
            int day = Integer.parseInt(date[0]);

            Calendar dob = Calendar.getInstance();
            Calendar today = Calendar.getInstance();

            dob.set(year, month, day);

            age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--;
            }
        }
        return String.valueOf(age);
    }
}