package com.exceptionaire.ongraviti.activities.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.VideoBioActivity;
import com.exceptionaire.ongraviti.activities.ViewProfileActivity;
import com.exceptionaire.ongraviti.activities.base.BaseDrawerActivity;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.imoji.EmojiconEditText;
import com.exceptionaire.ongraviti.imoji.EmoticonTextView;
import com.exceptionaire.ongraviti.model.Postinfo;
import com.exceptionaire.ongraviti.model.ResponseUpdateStatus;
import com.exceptionaire.ongraviti.model.ResponseUserPost;
import com.exceptionaire.ongraviti.model.deletePostResponse;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.BitmapTransform;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.squareup.picasso.Picasso;

import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppConstants.RESPONSE_SUCCESS;
import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;


public class UserPostListAdapter extends
        RecyclerView.Adapter<UserPostListAdapter.ViewHolder> {
    private Context context;
    boolean isLike;
    private ArrayList<ResponseUserPost.Postinfo> mFaqList;
    private static final int MAX_WIDTH = 1024;
    private static final int MAX_HEIGHT = 768;
    int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));
    private static String POPUP_CONSTANT = "mPopup";
    private static String POPUP_FORCE_SHOW_ICON = "setForceShowIcon";
    public ImageLoader imageLoader = ImageLoader.getInstance();

    public class ViewHolder extends RecyclerView.ViewHolder {
        boolean isLike;
        ImageView image_view_cat_type;
        ImageView videoPlayImageButton, imgComment, imgShare, imgUnlike, img_more_options;
        TextView txtPName, txtDate, txtLikeCount, txtCommentCount;
        EmoticonTextView txtPostDescription;
        ImageView imageViewPostImageVideoThumbnail;
        FrameLayout frame_Video;
        TextView text_view_read_more;
        View seperator_view;
        RelativeLayout relative_post_bottom;
        RoundedImageView imgProfile;

        public ViewHolder(View view) {
            super(view);
            image_view_cat_type = itemView.findViewById(R.id.image_view_cat_type);
            frame_Video = (FrameLayout) itemView.findViewById(R.id.frame_Video);
            relative_post_bottom = (RelativeLayout) itemView.findViewById(R.id.relative_post_bottom);
            seperator_view = (View) itemView.findViewById(R.id.seperator_view);
            imgUnlike = (ImageView) itemView.findViewById(R.id.imgUnlike);
            videoPlayImageButton = (ImageView) itemView.findViewById(R.id.video_play_img_btn);
            imageViewPostImageVideoThumbnail = (ImageView) itemView.findViewById(R.id.video_feed_item_video_image);
            imgProfile = (RoundedImageView) itemView.findViewById(R.id.imgProfilePic);
            imgComment = (ImageView) itemView.findViewById(R.id.imgComment);
            imgShare = (ImageView) itemView.findViewById(R.id.imgShare);
            txtPName = (TextView) itemView.findViewById(R.id.txtProfileName);
            txtPostDescription = (EmoticonTextView) itemView.findViewById(R.id.expTxtProfileDesc);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtLikeCount = (TextView) itemView.findViewById(R.id.txtLikeCount);
            txtCommentCount = (TextView) itemView.findViewById(R.id.txtCommentCount);
            img_more_options = (ImageView) itemView.findViewById(R.id.img_more_options);
            if (!imageLoader.isInited()) {
                imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
            }
        }
    }

    public UserPostListAdapter(Context context, ArrayList<ResponseUserPost.Postinfo> FaqList) {
        this.context = context;
        mFaqList = FaqList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_feed_type, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ResponseUserPost.Postinfo postinfo = mFaqList.get(position);
        holder.txtPName.setText(postinfo.getUserName());
//        if (postinfo.getProfilePicture() != null)
//            Picasso.with(context)
//                    .load(postinfo.getProfilePicture())
//                    .placeholder(R.drawable.profile)
//                    .into(holder.imgProfile);

        if (postinfo.getProfilePicture() != null) {
            Log.d("uiiiiiiii:", postinfo.getProfilePicture());
            if (!postinfo.getProfilePicture().contains("https://graph")) {
                Picasso.with(context)
                        .load(postinfo.getProfilePicture())
                        .placeholder(R.drawable.profile)
                        .error(R.drawable.profile)
                        .into(holder.imgProfile);
            } else if (postinfo.getProfilePicture().contains("https://graph")) {
                String[] separated = postinfo.getProfilePicture().split("https://graph");
                String pathFB = separated[1];
                Log.d("uiiiiiiii1:", pathFB);
                Picasso.with(context)
                        .load("https://graph" + pathFB)
                        .placeholder(R.drawable.profile)
                        .error(R.drawable.profile)
                        .into(holder.imgProfile);
            }
        }
        //Comment text is getting copied  for solving that issue add bellow two lines
        (holder).imgUnlike.setImageResource(R.drawable.unlike);
        if (postinfo.getPostDate() != null)
            (holder).txtDate.setText(postinfo.getPostDate());
        if (postinfo.getPostContent() != null) {
            try {
                (holder).txtPostDescription.setText(URLDecoder.decode(
                        postinfo.getPostContent(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (postinfo.getCountOfLike() != null && !postinfo.getCountOfLike().isEmpty())
            (holder).txtLikeCount.setText(postinfo.getCountOfLike());

        if (postinfo.getCountOfComments() != null)
            (holder).txtCommentCount.setText(postinfo.getCountOfComments() + "");


        if (postinfo.getPostsVideo() == null || postinfo.getPostsVideo().isEmpty()) {
            if (postinfo.getPostsPic() == null || postinfo.getPostsPic().isEmpty()) {
                (holder).videoPlayImageButton.setVisibility(View.GONE);
                (holder).imageViewPostImageVideoThumbnail.setVisibility(View.GONE);
            } else {
                (holder).videoPlayImageButton.setVisibility(View.GONE);
                (holder).imageViewPostImageVideoThumbnail.setVisibility(View.VISIBLE);

                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .showStubImage(R.drawable.placeholder)
                        .showImageForEmptyUri(R.drawable.placeholder)
                        .resetViewBeforeLoading(true)
                        .cacheInMemory()
                        .cacheOnDisc()
                        .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                        .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                        .displayer(new SimpleBitmapDisplayer()) // default
                        .build();
                imageLoader.displayImage(postinfo.getPostsPic(), (holder).imageViewPostImageVideoThumbnail, options);


                (holder).imageViewPostImageVideoThumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //open next activity and play video in landscape mode
                        if (postinfo.getPostsPic() != null || !postinfo.getPostsPic().isEmpty()) {
                            new CommonDialogs().displayImageZoomDialog((ViewProfileActivity) context, postinfo.getPostsPic());
                        }
                    }
                });
            }
        } else {
            (holder).videoPlayImageButton.setVisibility(View.VISIBLE);
            (holder).imageViewPostImageVideoThumbnail.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load(postinfo.getPostThumb())
                    .placeholder(R.drawable.placeholder)
                    .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                    .resize(size, size)
                    .centerInside()
                    .into((holder).imageViewPostImageVideoThumbnail);
        }

        (holder).videoPlayImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open next activity and play video in landscape mode

                Intent intentPlay = new Intent(context, VideoBioActivity.class);
                intentPlay.putExtra("videopath", "" + postinfo.getPostsVideo());
                context.startActivity(intentPlay);
            }
        });
        holder.img_more_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (postinfo.getPostUserId().equals(AppSettings.getLoginUserId())) {
                    //creating a popup menu
                    PopupMenu popup1 = new PopupMenu(context, view);
                    try {
                        Field[] fields = popup1.getClass().getDeclaredFields();
                        for (Field field : fields) {
                            if (field.getName().equals(POPUP_CONSTANT)) {
                                field.setAccessible(true);
                                Object menuPopupHelper = field.get(popup1);
                                Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                                Method setForceIcons = classPopupHelper.getMethod(POPUP_FORCE_SHOW_ICON, boolean.class);
                                setForceIcons.invoke(menuPopupHelper, true);

                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    popup1.getMenuInflater().inflate(R.menu.popup_menu, popup1.getMenu());

                    popup1.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.item_edit_post:
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                    String currentDateandTime = format.format(new Date());
                                    Dialog dialogStatus = new CommonDialogs().dialogEditPost((Activity) context);
                                    EditText editTextStatus = dialogStatus.findViewById(R.id.edit_text_status);
                                    editTextStatus.setHint("Write something here...");
                                    try {
                                        editTextStatus.setText(URLDecoder.decode(
                                                postinfo.getPostContent(), "UTF-8"));
                                        editTextStatus.setSelection(editTextStatus.getText().length());
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                    dialogStatus.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            editTextStatus.setError(null);

                                            if (editTextStatus.getText().toString().length() == 0) {
                                            } else {
                                                try {
                                                    ((ViewProfileActivity) context).setEditPost(postinfo.getPostId(), postinfo.getvisibility_status(), URLEncoder.encode(editTextStatus.getText().toString(), HTTP.UTF_8), "", currentDateandTime);
                                                } catch (UnsupportedEncodingException e) {
                                                    e.printStackTrace();
                                                }
                                                dialogStatus.dismiss();
                                            }
                                        }
                                    });
                                    dialogStatus.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialogStatus.dismiss();
                                        }
                                    });


                                    break;
                                case R.id.item_delete_post:

                                    Dialog dialog = new CommonDialogs().dialogAcceptReject((Activity) context, context.getResources().getString(R.string.ongravity), "Are you sure you want to delete this post?");
                                    ((Button) dialog.findViewById(R.id.button_ok)).setText("Yes");
                                    ((Button) dialog.findViewById(R.id.button_cancel)).setText("No");
                                    dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            deleteUserPost(postinfo.getPostId(), postinfo.getPostUserId());
                                            removeAt((holder).getAdapterPosition());
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                        }
                                    });

                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup1.show();


                }
            }
        });

        int n = Integer.parseInt(postinfo.getCountOfLike());
        if (n >= 1) {
            (holder).imgUnlike.setImageResource(R.drawable.like);
        }
        (holder).imgUnlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewProfileActivity) context).saveUserPostLike(postinfo.getPostId(), position, holder);
            }
        });

        (holder).imgComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewProfileActivity) context).getPostComments(postinfo.getPostId(), "0");
            }
        });
        if (postinfo.getprofile_cat_id() != null) {
            switch (postinfo.getprofile_cat_id()) {
                case "2":
                    (holder).image_view_cat_type.setImageResource(R.drawable.ic_circle_pink);
                    break;
                case "3":
                    (holder).image_view_cat_type.setImageResource(R.drawable.ic_circle_blue);
                    break;
                case "1":
                    (holder).image_view_cat_type.setImageResource(R.drawable.ic_circle_green);
                    break;
            }

        }
    }

    public void addLike(int position, ViewHolder holder, String likeCount, String message) {
        mFaqList.get(position).setCountOfLike(likeCount);
        if (message.equals("Unlike this post.")) {
            mFaqList.get(position).setSelfLikeStatus("0");
            (holder).imgUnlike.setImageResource(R.drawable.like);
            holder.isLike = false;
        } else {
            mFaqList.get(position).setSelfLikeStatus("1");
            holder.imgUnlike.setImageResource(R.drawable.unlike);
            holder.isLike = true;
        }
        notifyItemChanged(position);
    }

    private void removeAt(int position) {
        mFaqList.remove(position);
        notifyItemRemoved(position);

    }

    @Override
    public int getItemCount() {
        return mFaqList.size();
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return false;
        }
        NetworkInfo.State networkState = networkInfo.getState();
        return (networkState == NetworkInfo.State.CONNECTED || networkState == NetworkInfo.State.CONNECTING);
    }

    public void deleteUserPost(String postId, String userId) {
//        CustomProgressDialog progressDialog;
//        progressDialog = new CustomProgressDialog(context, context.getResources().getString(R.string.loading));
//        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        progressDialog.show();

        try {
            if (isNetworkAvailable()) {
                Retrofit retrofit = ApiClient.getClient();
                ApiInterface loginRequest = retrofit.create(ApiInterface.class);
                Call<deletePostResponse> userPostsCall = loginRequest.delete_user_post(userId, postId);
                userPostsCall.enqueue(new Callback<deletePostResponse>() {
                    @Override
                    public void onResponse(Call<deletePostResponse> call, Response<deletePostResponse> response) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();

                        if (response.body().getStatus().equals(RESPONSE_SUCCESS)) {

                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_LONG).show();
//                            CommonDialogs.dialog_with_one_btn_without_title(HomeUserPostsActivity.this, response.body().getMsg());
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_LONG).show();
//                            CommonDialogs.dialog_with_one_btn_without_title(HomeUserPostsActivity.this, response.body().getMsg());
                        }
                    }

                    @Override
                    public void onFailure(Call<deletePostResponse> call, Throwable t) {
//                        if (progressDialog != null && progressDialog.isShowing())
//                            progressDialog.dismiss();
                    }
                });

            } else {
                CommonDialogs.dialog_with_one_btn_without_title((Activity) context, context.getResources().getString(R.string.check_internet));
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }
}