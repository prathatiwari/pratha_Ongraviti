package com.exceptionaire.ongraviti.database;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;

/**
 * Created by Laxmikant on 20/9/16.
 */

@Entity(indexes = {
        @Index(value = "contact_number")
})
public class User_data implements Parcelable {
    private String contact_number;
    private String profile_picture;
    private String invite_status;
    private String quickblox_user;
    private String name;
    private String user_id;
    private String thumb;
    private String email_address;
    private Boolean alreadyInvited;
    private String Original_Mobile_nos;

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getInvite_status() {
        return invite_status;
    }

    public void setInvite_status(String invite_status) {
        this.invite_status = invite_status;
    }

    public String getQuickblox_user() {
        return quickblox_user;
    }

    public void setQuickblox_user(String quickblox_user) {
        this.quickblox_user = quickblox_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public Boolean getAlreadyInvited() {
        return alreadyInvited;
    }

    public void setAlreadyInvited(Boolean alreadyInvited) {
        this.alreadyInvited = alreadyInvited;
    }

    public String getOriginal_Mobile_nos() {
        return Original_Mobile_nos;
    }

    public void setOriginal_Mobile_nos(String Original_Mobile_nos) {
        this.Original_Mobile_nos = Original_Mobile_nos;
    }

    @Override
    public String toString() {
        return "ClassPojo [contact_number = " + contact_number + ", profile_picture = " + profile_picture + ", invite_status = " + invite_status + ", quickblox_user = " + quickblox_user + ", name = " + name + ", user_id = " + user_id + ", thumb = " + thumb + ", email_address = " + email_address + ", alreadyInvited = " + alreadyInvited + "]";
    }


    @Generated(hash = 2078084824)
    public User_data() {
    }

    @Generated(hash = 739856796)
    public User_data(String contact_number, String profile_picture, String invite_status, String quickblox_user, String name, String user_id, String thumb, String email_address, Boolean alreadyInvited, String Original_Mobile_nos) {
        this.contact_number = contact_number;
        this.profile_picture = profile_picture;
        this.invite_status = invite_status;
        this.quickblox_user = quickblox_user;
        this.name = name;
        this.user_id = user_id;
        this.thumb = thumb;
        this.email_address = email_address;
        this.alreadyInvited = alreadyInvited;
        this.Original_Mobile_nos = Original_Mobile_nos;
    }

    protected User_data(Parcel parcel) {
        this.contact_number = parcel.readString();
        this.profile_picture = parcel.readString();
        this.invite_status = parcel.readString();
        this.quickblox_user = parcel.readString();
        this.name = parcel.readString();
        this.user_id = parcel.readString();
        this.thumb = parcel.readString();
        this.email_address = parcel.readString();
        this.alreadyInvited = parcel.readByte() != 0;
        this.Original_Mobile_nos = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.contact_number);
        parcel.writeString(this.profile_picture);
        parcel.writeString(this.invite_status);
        parcel.writeString(this.quickblox_user);
        parcel.writeString(this.name);
        parcel.writeString(this.user_id);
        parcel.writeString(this.thumb);
        parcel.writeString(this.email_address);
        if (alreadyInvited != null)
            parcel.writeByte((byte) (alreadyInvited ? 1 : 0));
        parcel.writeString(this.Original_Mobile_nos);
    }

    public static final Creator<User_data> CREATOR = new Creator<User_data>() {
        @Override
        public User_data createFromParcel(Parcel in) {
            return new User_data(in);
        }

        @Override
        public User_data[] newArray(int size) {
            return new User_data[size];
        }
    };

}