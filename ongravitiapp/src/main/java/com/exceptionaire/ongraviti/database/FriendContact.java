package com.exceptionaire.ongraviti.database;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;

/**
 * Created by Laxmikant on 20/9/16.
 */

@Entity(indexes = {
        @Index(value = "contact_number")
})
public class FriendContact implements Parcelable {

    private String user_id;
    private String quickblox_id;
    private String name;
    private String email_address;
    private String registered_flag;
    private String profile_pic;
    private String contact_number;
    private String contact_no;
    private String original_number;
    private int position;
    private Boolean is_invited;

    public String getOriginal_number() {
        return original_number;
    }

    public void setOriginal_number(String original_number) {
        this.original_number = original_number;
    }

    public Boolean getIs_invited() {
        return is_invited;
    }

    public void setIs_invited(Boolean is_invited) {
        this.is_invited = is_invited;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getRegistered_flag() {
        return registered_flag;
    }

    public void setRegistered_flag(String registered_flag) {
        this.registered_flag = registered_flag;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getQuickblox_id() {
        return quickblox_id;
    }

    public void setQuickblox_id(String quickblox_id) {
        this.quickblox_id = quickblox_id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.user_id);
        dest.writeString(this.quickblox_id);
        dest.writeString(this.name);
        dest.writeString(this.email_address);
        dest.writeString(this.registered_flag);
        dest.writeString(this.profile_pic);
        dest.writeString(this.contact_number);
        dest.writeString(this.contact_no);
        dest.writeString(this.original_number);
        dest.writeInt(this.position);
        if (is_invited != null)
            dest.writeByte((byte) (is_invited ? 1 : 0));
        dest.writeByte((byte) 0);
    }


    protected FriendContact(Parcel parcel) {
        this.user_id = parcel.readString();
        this.quickblox_id = parcel.readString();
        this.name = parcel.readString();
        this.email_address = parcel.readString();
        this.registered_flag = parcel.readString();
        this.profile_pic = parcel.readString();
        this.contact_number = parcel.readString();
        this.contact_no = parcel.readString();
        this.original_number = parcel.readString();
        this.position = parcel.readInt();
        this.is_invited = parcel.readByte() != 0;
    }


    @Generated(hash = 1390362265)
    public FriendContact() {
    }

    @Generated(hash = 1543674076)
    public FriendContact(String user_id, String quickblox_id, String name, String email_address, String registered_flag, String profile_pic,
                         String contact_number, String contact_no, String original_number, int position, Boolean is_invited) {
        this.user_id = user_id;
        this.quickblox_id = quickblox_id;
        this.name = name;
        this.email_address = email_address;
        this.registered_flag = registered_flag;
        this.profile_pic = profile_pic;
        this.contact_number = contact_number;
        this.contact_no = contact_no;
        this.original_number = original_number;
        this.position = position;
        this.is_invited = is_invited;
    }

    public static final Parcelable.Creator<FriendContact> CREATOR = new Parcelable.Creator<FriendContact>() {
        @Override
        public FriendContact createFromParcel(Parcel source) {
            return new FriendContact(source);
        }

        @Override
        public FriendContact[] newArray(int size) {
            return new FriendContact[size];
        }
    };
}
