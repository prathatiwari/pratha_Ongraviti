package com.exceptionaire.ongraviti.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by bhakti gade on 26/9/16.
 */
public class AppUtils {
    
    public static InputFilter ignoreFirstWhiteSpace() {
        return new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                for (int i = start; i < end; i++) {
                    if (Character.isWhitespace(source.charAt(i))) {
                        if (dstart == 0)
                            return "";
                    }
                }
                return null;
            }
        };
    }
    public static void hideSoftKeyboard(Activity activity)
    {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static void showProgressBar(Activity activity,int flag) {
        ProgressDialog progressDialog;
        progressDialog = new CustomProgressDialog(activity, activity.getResources().getString(R.string.please_wait));
        if(flag==0)
        {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }
        else if(flag==1)
        {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }
   public static Uri onCaptureImageResult(Intent data)
   {
       Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
       ByteArrayOutputStream bytes = new ByteArrayOutputStream();
       thumbnail.compress(Bitmap.CompressFormat.PNG, 90, bytes);

       File destination = new File(Environment.getExternalStorageDirectory(),
               System.currentTimeMillis() + ".jpg");

       FileOutputStream fo;
       try {
           destination.createNewFile();
           fo = new FileOutputStream(destination);
           fo.write(bytes.toByteArray());
           fo.close();
       } catch (FileNotFoundException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       Uri uri=Uri.fromFile(destination);
       return uri;
   }
    public static String getDeviceID(Activity activity)
    {
        TelephonyManager tm = (TelephonyManager)activity.getSystemService(activity.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        System.out.println("Country code" +countryCodeValue);
        TelephonyManager telephonyManager = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId= telephonyManager.getDeviceId();
        System.out.println("device Id"+deviceId);
        return deviceId;


    }
    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    public static double getLattitudeFromAddress(Context context, String strAddress) {
        double lattitude = 0,longitude = 0;
        Geocoder coder = new Geocoder(context);
        List<Address> addresses;
        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
            } else {
                Address location = addresses.get(0);
                lattitude = location.getLatitude();
                longitude = location.getLongitude();
                Log.e("Lat", "" + lattitude);
                Log.e("Lng", "" + longitude);
                LatLng latLng = new LatLng(lattitude, longitude);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return lattitude;
    }
    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
    public static double getLongitudeFromAddress(Context context, String strAddress) {
        double lattitude,longitude = 0;
        Geocoder coder = new Geocoder(context);
        List<Address> addresses;
        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
            } else {
                Address location = addresses.get(0);
                lattitude = location.getLatitude();
                longitude = location.getLongitude();
                Log.e("Lat", "" + lattitude);
                Log.e("Lng", "" + longitude);
                LatLng latLng = new LatLng(lattitude, longitude);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return longitude;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isDeviceSupportCamera(Context context) {
        if (context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
}
