package com.exceptionaire.ongraviti.utilities;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by root on 4/4/17.
 */
public class AnimationUtils {

    public static Animation expand(final View v, final int heightInDps, int duration, final Resources resources) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        final int targetHeight;
        if (heightInDps <= 0 || resources == null)
            targetHeight = v.getMeasuredHeight();
        else
            targetHeight = dpToPx(resources, 200);

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (heightInDps <= 0 || resources == null)
                    v.getLayoutParams().height = interpolatedTime == 1
                            ? LinearLayout.LayoutParams.WRAP_CONTENT
                            : (int) (targetHeight * interpolatedTime);
                else
                    v.getLayoutParams().height = targetHeight;
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        if (duration <= 0)
            a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density) * 10);
        else
            a.setDuration(duration);
        v.startAnimation(a);
        return a;
    }

    // Horizontal expand
    public static Animation expandHorizontal(final View v, final int widthInDps, int duration, final Resources resources) {
        v.measure(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetWidth;
        if (widthInDps <= 0 || resources == null)
            targetWidth = v.getMeasuredWidth();
        else
            targetWidth = dpToPx(resources, 200);

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (widthInDps <= 0 || resources == null)
                    v.getLayoutParams().width = interpolatedTime == 1
                            ? LinearLayout.LayoutParams.WRAP_CONTENT
                            : (int) (targetWidth * interpolatedTime);
                else
                    v.getLayoutParams().width = targetWidth;
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        if (duration <= 0)
            a.setDuration((int) (targetWidth / v.getContext().getResources().getDisplayMetrics().density) * 10);
        else
            a.setDuration(duration);
        v.startAnimation(a);
        return a;
    }


    public static Animation collapse(final View v, int heightInDps, int duration, Resources resources) {
        final int initialHeight;
//		if(heightInDps <= 0 || resources == null)
        initialHeight = v.getMeasuredHeight();
//		else
//			initialHeight = dpToPx(resources, heightInDps);

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        if (duration <= 0)
            a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density) * 10);
        else
            a.setDuration(duration);

        v.startAnimation(a);
        return a;
    }

    // Collapse horizontal
    public static Animation collapseHorizontal(final View v, int widthInDps, int duration, Resources resources) {
        final int initialWidth;
//		if(heightInDps <= 0 || resources == null)
        initialWidth = v.getMeasuredWidth();
//		else
//			initialHeight = dpToPx(resources, heightInDps);

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().width = initialWidth - (int) (initialWidth * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        if (duration <= 0)
            a.setDuration((int) (initialWidth / v.getContext().getResources().getDisplayMetrics().density) * 10);
        else
            a.setDuration(duration);

        v.startAnimation(a);
        return a;
    }

//    public static void activityExit(Activity activity) {
//        activity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
//    }
//
//    public static void activityEnter(Activity activity) {
//        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
//    }
//
//    public static void activityExitVertical(Activity activity) {
//        activity.overridePendingTransition(R.anim.slide_top_middle_full, R.anim.slide_middle_bottom_full);
//    }
//
//    public static void activityEnterVertical(Activity activity) {
//        activity.overridePendingTransition(R.anim.slide_bottom_middle_full, R.anim.slide_middle_top_full);
//    }

    // Translate animate Views
    public static Animation animateViewFly(View viewToAnimate, float fromX, float toX,
                                           float fromY, float toY, int typeRelative, int animationDuration,
                                           int animationRepeatCount) {
        viewToAnimate.setVisibility(View.VISIBLE);
        Animation animation = new TranslateAnimation(typeRelative, fromX, typeRelative, toX, typeRelative, fromY, typeRelative, toY);
        animation.setDuration(animationDuration);
        animation.setRepeatCount(animationRepeatCount);
        animation.setInterpolator(new LinearInterpolator());
        viewToAnimate.startAnimation(animation);
        return animation;
    }

    // convert dp to px
    public static int dpToPx(Resources resources, int dp) {
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    //Class to hide / show bottom bar
    public static class HideShowBottomBar {

        private View view = null;
        private boolean hidingLayout = false;
        private boolean startHide = false;
        private int duration = 300;

        public HideShowBottomBar(View view) {
            this.view = view;
        }

        public HideShowBottomBar(View view, int duration) {
            this.view = view;
            this.duration = duration;
        }

        public boolean isHiding() {
            return this.hidingLayout;
        }

        public void setHiding(boolean hiding) {
            this.hidingLayout = hiding;
        }

        // Hide bottom bar with animation
        public Animation hideBottombar() {
            if (view.getVisibility() != View.GONE && !hidingLayout && !startHide) {
                startHide = true;
                Animation hide = AnimationUtils.animateViewFly(view, 0, 0, 0, 1f, Animation.RELATIVE_TO_SELF, duration, 0);
                hide.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        view.setVisibility(View.GONE);
                        hidingLayout = true;
                        startHide = false;
                    }
                });
                return hide;
            }
            return null;
        }

        // show bottom bar with animation
        public Animation showBottombar() {
            if (view.getVisibility() == View.GONE && hidingLayout) {
                Animation show = AnimationUtils.animateViewFly(view, 0, 0, 1f, 0f, Animation.RELATIVE_TO_SELF, duration, 0);
                show.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        view.setVisibility(View.VISIBLE);
                        hidingLayout = false;
                    }
                });
                return show;
            }
            return null;
        }

    }


    //Class to hide / show top bar
    public static class HideShowTopBar {

        private View view = null;
        private boolean hidingLayout = false;
        private boolean startHide = false;
        private int duration = 300;

        public HideShowTopBar(View view) {
            this.view = view;
        }

        public HideShowTopBar(View view, int duration) {
            this.view = view;
            this.duration = duration;
        }

        public boolean isHiding() {
            return this.hidingLayout;
        }

        public void setHiding(boolean hiding) {
            this.hidingLayout = hiding;
        }

        // Hide top bar with animation
        public Animation hideTopbar() {
            if (view.getVisibility() != View.GONE && !hidingLayout && !startHide) {
                startHide = true;
                Animation hide = AnimationUtils.animateViewFly(view, 0, 0, 0, -2f, Animation.RELATIVE_TO_SELF, duration, 0);
                hide.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        view.setVisibility(View.GONE);
                        hidingLayout = true;
                        startHide = false;
                    }
                });
                return hide;
            }
            return null;
        }

        // show Top bar with animation
        public Animation showTopbar() {
            if (view.getVisibility() == View.GONE && hidingLayout) {
                Animation show = AnimationUtils.animateViewFly(view, 0, 0, -2f, 0f, Animation.RELATIVE_TO_SELF, duration, 0);
                show.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        view.setVisibility(View.VISIBLE);
                        hidingLayout = false;
                    }
                });
                return show;
            }
            return null;
        }

    }

    // Bounce animation
    public static Animation bounceView(View viewToAnimate, float fromX, float toX,
                                       float fromY, float toY, int typeRelative, int animationDuration,
                                       int animationRepeatCount) {
        viewToAnimate.setVisibility(View.VISIBLE);
        Animation animation = new TranslateAnimation(typeRelative, fromX, typeRelative, toX, typeRelative, fromY, typeRelative, toY);
        animation.setDuration(animationDuration);
        animation.setRepeatCount(animationRepeatCount);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setInterpolator(new BounceInterpolator());
        viewToAnimate.startAnimation(animation);
        return animation;
    }


    public static Bitmap getScaledBitmap(Bitmap bitmap, int reqWidth, int reqHeight) {
        if (bitmap != null) {
            Matrix m = new Matrix();
            m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, reqWidth, reqHeight), Matrix.ScaleToFit.CENTER);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
        }
        return null;
    }

    public static Bitmap getScaledBitmap(Bitmap bitmap, int reqWidth, int reqHeight, Matrix m) {
        if (bitmap != null) {
            m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, reqWidth, reqHeight), Matrix.ScaleToFit.CENTER);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
        }
        return null;
    }

    @SuppressWarnings("deprecation")
    public static Bitmap decodeSampledBitmapFromResourceMemOpt(
            InputStream inputStream, int reqWidth, int reqHeight) {

        byte[] byteArr = new byte[0];
        byte[] buffer = new byte[1024];
        int len;
        int count = 0;

        try {
            while ((len = inputStream.read(buffer)) > -1) {
                if (len != 0) {
                    if (count + len > byteArr.length) {
                        byte[] newbuf = new byte[(count + len) * 2];
                        System.arraycopy(byteArr, 0, newbuf, 0, count);
                        byteArr = newbuf;
                    }

                    System.arraycopy(buffer, 0, byteArr, count, len);
                    count += len;
                }
            }

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(byteArr, 0, count, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            return BitmapFactory.decodeByteArray(byteArr, 0, count, options);

        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }


        return inSampleSize;
    }


    // Download image from url
    public static File downloadImage(String img_url, String filePath) {

        File casted_image = null;
        try {
            casted_image = new File(filePath);
            if (casted_image.exists()) {
                casted_image.delete();
            }
            casted_image.createNewFile();

            FileOutputStream fos = new FileOutputStream(casted_image);

            URL url = new URL(img_url);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.connect();
            InputStream in = connection.getInputStream();

            byte[] buffer = new byte[1024];
            int size = 0;
            while ((size = in.read(buffer)) > 0) {
                fos.write(buffer, 0, size);
            }
            fos.close();
            return casted_image;

        } catch (Exception e) {

            System.out.print(e);
            // e.printStackTrace();

        }
        return casted_image;
    }

//	public static class GenericCollapseExpandAnimator implements ViewTreeObserver.OnPreDrawListener{
//
//		LinearLayout layoutToAnimate;
//		View layoutHeader;
//		ValueAnimator mAnimator;
//
//		public GenericCollapseExpandAnimator(LinearLayout layoutBody, View layoutHeader){
//			this.layoutToAnimate = layoutBody;
//			this.layoutHeader = layoutHeader;
//			this.layoutHeader.getViewTreeObserver().addOnPreDrawListener(this);
//			this.layoutHeader.setOnClickListener(new View.OnClickListener() {
//
//	            @Override
//	            public void onClick(View v) {
//	                if (layoutToAnimate.getVisibility()==View.GONE){
//	                	expand();
//
//	                }else{
//	                	collapse();
//	                }
//	            }
//	        });
//
//
//		}
//
//		public boolean isOpen(){
//			if(layoutToAnimate.getVisibility()==View.VISIBLE)
//				return true;
//			else
//				return false;
//		}
//
//		@Override
//		public boolean onPreDraw() {
//			// TODO Auto-generated method stub
//			layoutToAnimate.getViewTreeObserver().removeOnPreDrawListener(this);
//			layoutToAnimate.setVisibility(View.GONE);
//
//	        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
//			final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
//			layoutToAnimate.measure(widthSpec, heightSpec);
//
//			mAnimator = slideAnimator(0, layoutToAnimate.getMeasuredHeight());
//	        return true;
//
//		}
//
//		private void expand() {
//			//set Visible
//			mAnimator.start();
//			layoutToAnimate.setVisibility(View.VISIBLE);
//		}
//
//		@SuppressLint("NewApi")
//		public void collapse() {
//			int finalHeight = layoutToAnimate.getHeight();
//
//			ValueAnimator mAnimator = slideAnimator(finalHeight, 0);
//
//			mAnimator.addListener(new Animator.AnimatorListener() {
//				@Override
//				public void onAnimationEnd(Animator animator) {
//					//Height=0, but it set visibility to GONE
//					layoutToAnimate.setVisibility(View.GONE);
//				}
//
//				@Override
//				public void onAnimationStart(Animator animator) {
//				}
//
//				@Override
//				public void onAnimationCancel(Animator animator) {
//				}
//
//				@Override
//				public void onAnimationRepeat(Animator animator) {
//				}
//			});
//			mAnimator.start();
//		}
//
//
//		private ValueAnimator slideAnimator(int start, int end) {
//
//			ValueAnimator animator = ValueAnimator.ofInt(start, end);
//
//
//			animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//				@Override
//				public void onAnimationUpdate(ValueAnimator valueAnimator) {
//					//Update Height
//					int value = (Integer) valueAnimator.getAnimatedValue();
//
//					ViewGroup.LayoutParams layoutParams = layoutToAnimate.getLayoutParams();
//					layoutParams.height = value;
//					layoutToAnimate.setLayoutParams(layoutParams);
//				}
//			});
//			return animator;
//		}
//
//
//	}

}
