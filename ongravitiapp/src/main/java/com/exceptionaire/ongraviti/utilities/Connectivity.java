package com.exceptionaire.ongraviti.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.exceptionaire.ongraviti.core.AppConstants;

/**
 * Created by root on 24/3/17.
 */

public class Connectivity implements AppConstants {
    /**
     * Get the network info
     *
     * @param context
     * @return
     */
    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }
    /**
     * Check if there is any connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Check if there is any connectivity to a Wifi network
     *
     * @param context //     * @param type
     * @return
     */
    public static boolean isConnectedWifi(Context context) {
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    /**
     * Check if there is any connectivity to a mobile network
     *
     * @param context //     * @param type
     * @return
     */
    public static boolean isConnectedMobile(Context context) {
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    /**
     * Check if there is fast connectivity
     *
     * @param context
     * @return
     */

    public static String isConnectedFast(Context context) {
        String speed = "";

        NetworkInfo info = Connectivity.getNetworkInfo(context);
        if (info != null)
            speed = Connectivity.isConnectionFast(info.getType(), info.getSubtype());

        if (info.isConnected()) {
            return speed;
        }

        return null;
    }

    /**
     * Check if the connection is fast
     *
     * @param type
     * @param subType
     * @return
     */
    public static String isConnectionFast(int type, int subType) {
        if (type == ConnectivityManager.TYPE_WIFI) {//-100mbps

            return HUNDRED_MBPS;
        } else if (type == ConnectivityManager.TYPE_MOBILE) {
            switch (subType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return FIFTY_TO_HUNDRED_KBPS; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return FOURTEEN_TO_SIXTY_FOUR_KBPS; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return FIFTY_TO_HUNDRED_KBPS; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return FOUR_HUNDRED_TO_ONE_THOUSAND_KBPS; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return SIX_HUNDRED_TO_FOURTEEN_HUNDRED_KBPS; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return HUNDRED_KBPS; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return TWO_TO_FOURTEEN_MBPS; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return SEVEN_HUNDRED_TO_SEVENTEEN_HUNDRED_KBPS; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return ONE_TO_TWENTY_THREE_MBPS; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return SIX_HUNDRED_TO_FOURTEEN_HUNDRED_KBPS; // ~ 400-7000 kbps
            /*
             * Above API level 7, make sure to set android:targetSdkVersion
			 * to appropriate level to use these
			 */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return ONE_TO_TWO_MBPS; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return FIVE_MBPS; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return TEN_TO_TWENTY_MBPS; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return TWENTY_FIVE_KBPS; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return TEN_PLUS_MBPS; // ~ 10+ Mbps

                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return null;
            }
        } else {
            return null;
        }
    }


}