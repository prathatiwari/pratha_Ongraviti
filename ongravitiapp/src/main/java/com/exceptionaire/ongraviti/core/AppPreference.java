package com.exceptionaire.ongraviti.core;

/**
 * Created by Administrator on 12/14/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.exceptionaire.ongraviti.model.RecentMessageData;
import com.exceptionaire.ongraviti.utilities.LogUtil;

import java.util.ArrayList;

public class AppPreference {
    public static final String TAG = "AppPreference";

    public static final String CAMERA_RESOLUTION_INDEX = "camera_resolution_index";
    //Send&Received Data
    //Setting
    public static final String NOTIFICATIONS = "notifications";
    public static final String NOTIFICATION_RING = "notification_ring";
    public static final String NOTIFICATION_VIBRATE = "notification_vibrate";


    private static String APP_SHARED_PREFS;
    private static SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefsEditor;

    public AppPreference(Context context) {
        APP_SHARED_PREFS = context.getApplicationContext().getPackageName();
        mPrefs = context.getSharedPreferences(APP_SHARED_PREFS,
                Activity.MODE_PRIVATE);
        mPrefsEditor = mPrefs.edit();
    }

    public static void initialize(SharedPreferences pref) {
        mPrefs = pref;
    }

    // check contain
    public static boolean contains(String key) {
        return mPrefs.contains(key);
    }

    // int
    public static int getInt(String key, int def) {
        return mPrefs.getInt(key, def);
    }

    public static void setInt(String key, int value) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }


    public Boolean getNotifications() {
        SharedPreferences.Editor editor = mPrefs.edit();
        boolean on = mPrefs.getBoolean(NOTIFICATIONS, true);
        return on;
    }


    public Boolean getNotificationRing() {
        SharedPreferences.Editor editor = mPrefs.edit();
        boolean on = mPrefs.getBoolean(NOTIFICATION_RING, true);
        return on;
    }

    public Boolean getNotificationVibrate() {
        SharedPreferences.Editor editor = mPrefs.edit();
        boolean on = mPrefs.getBoolean(NOTIFICATION_VIBRATE, true);
        return on;
    }

}

