package com.exceptionaire.ongraviti.core;

import android.util.SparseBooleanArray;

import com.quickblox.users.model.QBUser;

import java.util.ArrayList;

/**
 * Created by bhakti on 26/2/16.
 */
public class AppSettings extends AppSettingsCore implements AppConstants {
    private static String COUNT_OPTION_PIC_CATEGORY_ONE_SOCIAL = "option_pic_category_1";
    private static String COUNT_OPTION_PIC_CATEGORY_TWO_DATING = "option_pic_category_2";
    private static String COUNT_OPTION_PIC_CATEGORY_THREE_BUSINESS = "option_pic_category_3";
    private static String OPTION_PIC_ARRAY = "option_pic_array";
    private static String KEY_IS_LOGIN = "is_login";
    private static String KEY_LOGIN_USER_ID = "userid";
    private static String KEY_USER_PROFILE_NAME = "profile_name";
    private static String KEY_USER_PROFILE_EMAIL = "profile_email";
    private static String KEY_LOGIN_USER_PASSWORD = "password";
    private static String KEY_FB_LOGIN = "facebook_login_email";
    private static String KEY_FB_ID = "facebook_login_id";
    private static String KEY_IS_FB_LOGIN = "is_fb_login";
    private static String PROFILE_DEFAULT_CATEGORY = "profile_default_category";
    private static String PROFILE_PICTURE_NAME = "profile_picture_name";
    private static String PROFILE_PICTURE_THUMB_NAME = "profile_picture_thumb_name";
    private static String KEY_IS_PROFILE_PICTURE_UPLOADED = "is_profile_pic_uploaded";
    private static String PROFILE_PICTURE_FILE_PATH = "path";
    private static String KEY_USER_PROFILE_CONTACT_NUMBER = "profile_contact_number";
    private static String KEY_EVENT_ID = "event_id";
    private static String KEY_ORBIT_NAME = "OrbitName";
    private static String INTEREST_CATEGORIES = "interest_categories";
    private static String PROFILE_PIC_CATEGORY_ONE_FB = "Profile_Pic_Category_fb";
    private static String PROFILE_PIC_CATEGORY_ONE_SOCIAL = "Profile_Pic_Category_1";
    private static String PROFILE_PIC_CATEGORY_TWO_DATING = "Profile_Pic_Category_2";
    private static String PROFILE_PIC_CATEGORY_THREE_BUSINESS = "Profile_Pic_Category_3";
    private static SparseBooleanArray KEY_SELECTED_IDS = new SparseBooleanArray();
    private static String PROFILE_STEP_STATUS_1 = "profile_step_1";
    private static String PROFILE_STEP_STATUS_2 = "profile_step_2";
    private static String PROFILE_STEP_STATUS_3 = "profile_step_3";
    private static String PROFILE_STEP_STATUS_4 = "profile_step_4";
    private static String PROFILE_STEP_STATUS_5 = "profile_step_5";
    private static String KEY_ENROLL = "enroll";
    private static String EDIT_PROFILE_DETAILS = "edit_profile_details";
    private static String QUICK_BLOX_ID = "quickbloxid";
    private static String AGE = "age";
    private static String KEY_IS_OPEN_LIST = "is_open_list";
    private static String COMPLETE_PROFILE = "complete_profile";
    private static String PROFILE_COVER_CATEGORY_ONE_SOCIAL = "Profile_Cover_Category_1";
    private static String PROFILE_COVER_CATEGORY_TWO_DATING = "Profile_Cove_Category_2";
    private static String PROFILE_COVER_CATEGORY_THREE_BUSINESS = "Profile_Cover_Category_3";
    private static String REGISTRATION_STATUS = "registration_status";
    private static ArrayList<String> KEY_SELECTED_IMAGE_SOCIAL = new ArrayList<String>();
    private static ArrayList<String> KEY_SELECTED_IMAGE_DATING = new ArrayList<String>();
    private static ArrayList<String> KEY_SELECTED_IMAGE_BUSINESS = new ArrayList<String>();

    private static String FB_PASSWORD_FOR_QUICK_BLOX = "fb_quick_pass";

    private static String FB_QUICK_USER_PATCH = "qb_user_fb";


    private static String LOGIN_TYPE_PATCH_FOR_QUICK_ENTER = "quick_enter_login_type";


    private static String FB_USER_EMAIL_WITH_MOBILE_LOGIN="fb_user_email";



/*Prashil */

private static String FB_REGISTRATION_STATUS="fb_registration_status";

    public static String getFbRegistrationStatus() {
        return getString(FB_REGISTRATION_STATUS,null);
    }

    public static void setFbRegistrationStatus(String fbRegistrationStatus) {
        setString(FB_REGISTRATION_STATUS , fbRegistrationStatus);
    }

    public static String getFbUserEmailWithMobileLogin() {
        return getString(FB_USER_EMAIL_WITH_MOBILE_LOGIN,null);
    }

    public static void setFbUserEmailWithMobileLogin(String fbUserEmailWithMobileLogin) {
        setString(FB_USER_EMAIL_WITH_MOBILE_LOGIN, fbUserEmailWithMobileLogin);
    }

    public static String getLoginTypePatchForQuickEnter() {
        return getString(LOGIN_TYPE_PATCH_FOR_QUICK_ENTER, null);
    }

    public static void setLoginTypePatchForQuickEnter(String loginTypePatchForQuickEnter) {
        setString(LOGIN_TYPE_PATCH_FOR_QUICK_ENTER, loginTypePatchForQuickEnter);
    }

    public static String getFbQuickUserPatch() {
        return getString(FB_QUICK_USER_PATCH, null);
    }

    public static void setFbQuickUserPatch(String fbQuickUserPatch) {
        setString(FB_QUICK_USER_PATCH, fbQuickUserPatch);
    }

    private static String QUICK_PASSWORD = "quick_pass";

    public static String getQuickPassword() {
        return getString(QUICK_PASSWORD, null);
    }

    public static void setQuickPassword(String quickPassword) {
        setString(QUICK_PASSWORD, quickPassword);
    }

    public static SparseBooleanArray getKeySelectedIds() {
        return KEY_SELECTED_IDS;
    }

    public static void setKeySelectedIds(SparseBooleanArray keySelectedIds) {
        KEY_SELECTED_IDS = keySelectedIds;
    }


    public static ArrayList<String> getKeyImagesBusiness() {
        return KEY_SELECTED_IMAGE_BUSINESS;
    }

    public static void setKeyImagesBusiness(ArrayList<String> keySelectedImageBusiness) {
        KEY_SELECTED_IMAGE_BUSINESS = keySelectedImageBusiness;
    }

    public static ArrayList<String> getKeyImagesDating() {
        return KEY_SELECTED_IMAGE_DATING;
    }

    public static void setKeyImagesDating(ArrayList<String> keySelectedImageDating) {
        KEY_SELECTED_IMAGE_DATING = keySelectedImageDating;
    }

    public static ArrayList<String> getKeyImagesSocial() {
        return KEY_SELECTED_IMAGE_SOCIAL;
    }

    public static void setKeyImagesSocial(ArrayList<String> keySelectedImageSocial) {
        KEY_SELECTED_IMAGE_SOCIAL = keySelectedImageSocial;
    }

    public static String getProfile() {
        return getString(COMPLETE_PROFILE, null);
    }

    public static void setProfile(String profile) {
        setString(COMPLETE_PROFILE, profile);
    }


    public static void setProfileStepStatus1(int status) {
        setInt(PROFILE_STEP_STATUS_1, status);
    }


    public static void setProfileStepStatus2(int status) {
        setInt(PROFILE_STEP_STATUS_2, status);
    }


    public static void setProfileStepStatus3(int status) {
        setInt(PROFILE_STEP_STATUS_3, status);
    }


    public static void setProfileStepStatus4(int status) {
        setInt(PROFILE_STEP_STATUS_4, status);
    }


    public static void setProfileStepStatus5(int status) {
        setInt(PROFILE_STEP_STATUS_5, status);
    }


    public static String getLoginUserPassword() {
        return getString(KEY_LOGIN_USER_PASSWORD, null);
    }

    public static void setLoginUserPassword(String loginUserPassword) {
        setString(KEY_LOGIN_USER_PASSWORD, loginUserPassword);
    }

    public static String getInterestCategories() {
        return getString(INTEREST_CATEGORIES, null);
    }

    public static void setInterestCategories(String interestCategories) {
        setString(INTEREST_CATEGORIES, interestCategories);
    }

    public static String getKeyEventId() {
        return getString(KEY_EVENT_ID, null);
    }

    public static void setKeyEventId(String keyEventId) {
        setString(KEY_EVENT_ID, keyEventId);
    }

    public static String getLoginUserId() {
        return getString(KEY_LOGIN_USER_ID, null);
    }

    public static void setLoginUserId(String LoginUserId) {
        setString(KEY_LOGIN_USER_ID, LoginUserId);
    }

    public static void setLogin(boolean value) {
        setBoolean(KEY_IS_LOGIN, value);
    }

    public static boolean isLogin() {
        return getBoolean(KEY_IS_LOGIN, false);
    }


    public static void setUserProfileName(String name) {
        setString(KEY_USER_PROFILE_NAME, name);
    }

    public static String getUserProfileName() {
        return getString(KEY_USER_PROFILE_NAME, null);
    }


    public static void setUserProfileEmail(String name) {
        setString(KEY_USER_PROFILE_EMAIL, name);
    }

    public static String getUserProfileEmail() {
        return getString(KEY_USER_PROFILE_EMAIL, null);
    }

    public static String getFBEmail() {
        return getString(KEY_FB_LOGIN, null);
    }

    public static void setFBUserId(String email) {
        setString(KEY_FB_LOGIN, email);
    }


    public static String getFBUserIds() {
        return getString(KEY_FB_ID, null);
    }

    public static void setFBUserIds(String id) {
        setString(KEY_FB_ID, id);
    }

    public static void setFBLogin(boolean value) {
        setBoolean(KEY_IS_FB_LOGIN, value);
    }

    public static boolean isFBLogin() {
        return getBoolean(KEY_IS_FB_LOGIN, false);
    }

    public static void setKeyUserProfileContactNumber(String contactNumber) {
        setString(KEY_USER_PROFILE_CONTACT_NUMBER, contactNumber);
    }

    public static String getKeyUserProfileContactNumber() {
        return getString(KEY_USER_PROFILE_CONTACT_NUMBER, null);
    }

    public static void setProfiePictureName(String picture_name) {
        setString(PROFILE_PICTURE_NAME, picture_name);
    }

    public static void setProfiePictureThumb(String picture_name) {
        setString(PROFILE_PICTURE_THUMB_NAME, picture_name);
    }

    public static String getProfilePictureName() {
        return getString(PROFILE_PICTURE_NAME, null);
    }

    public static void setProfiePicturePath(String path) {
        setString(PROFILE_PICTURE_FILE_PATH, path);
    }

    public static String getProfilePicturePath() {
        return getString(PROFILE_PICTURE_FILE_PATH, null);
    }

    public static String getProfilePictureThumbName() {
        return getString(PROFILE_PICTURE_THUMB_NAME, null);
    }

    public static void setProfilePictureUploaded(boolean value) {
        setBoolean(KEY_IS_PROFILE_PICTURE_UPLOADED, value);
    }

    public static boolean isProfilePictureUploaded() {
        return getBoolean(KEY_IS_PROFILE_PICTURE_UPLOADED, false);
    }

    public static String getKeyIsLogin() {
        return KEY_IS_LOGIN;
    }

    public static void setKeyIsLogin(String keyIsLogin) {
        KEY_IS_LOGIN = keyIsLogin;
    }

    public static String getProfileDefaultCategory() {
        return getString(PROFILE_DEFAULT_CATEGORY, null);
    }

    public static void setProfileDefaultCategory(String profileDefaultCategory) {
        setString(PROFILE_DEFAULT_CATEGORY, profileDefaultCategory);
    }

    public static String getProfileCatPicPathOne() {
        return getString(PROFILE_PIC_CATEGORY_ONE_SOCIAL, null);
    }

    public static void setProfileCatPicPathOne(String profileDefaultCategory) {
        setString(PROFILE_PIC_CATEGORY_ONE_SOCIAL, profileDefaultCategory);
    }

    public static String getProfileCatPicPathTWO() {
        return getString(PROFILE_PIC_CATEGORY_TWO_DATING, null);
    }

    public static void setProfileCatPicPathTwo(String profileDefaultCategory) {
        setString(PROFILE_PIC_CATEGORY_TWO_DATING, profileDefaultCategory);
    }

    public static String getProfileCatPicPathThree() {
        return getString(PROFILE_PIC_CATEGORY_THREE_BUSINESS, null);
    }

    public static void setProfileCatPicPathThree(String profileDefaultCategory) {
        setString(PROFILE_PIC_CATEGORY_THREE_BUSINESS, profileDefaultCategory);
    }

    public static String getProfileCatCoverPathOne() {
        return getString(PROFILE_COVER_CATEGORY_ONE_SOCIAL, null);
    }

    public static void setProfileCatCoverPathOne(String profileDefaultCategory) {
        setString(PROFILE_COVER_CATEGORY_ONE_SOCIAL, profileDefaultCategory);
    }

    public static String getProfileCatCoverPathTWO() {
        return getString(PROFILE_COVER_CATEGORY_TWO_DATING, null);
    }

    public static void setProfileCatCoverPathTwo(String profileDefaultCategory) {
        setString(PROFILE_COVER_CATEGORY_TWO_DATING, profileDefaultCategory);
    }

    public static String getProfileCatCoverPathThree() {
        return getString(PROFILE_COVER_CATEGORY_THREE_BUSINESS, null);
    }

    public static void setProfileCatCoverPathThree(String profileDefaultCategory) {
        setString(PROFILE_COVER_CATEGORY_THREE_BUSINESS, profileDefaultCategory);
    }

    public static String getProfileDetails() {
        return getString(EDIT_PROFILE_DETAILS, null);
    }

    public static void setProfileDetails(String status) {
        setString(EDIT_PROFILE_DETAILS, status);
    }

    public static String getKeyOrbitName() {
        return getString(KEY_ORBIT_NAME, null);
    }

    public static void setKeyOrbitName(String keyOrbitName) {
        setString(KEY_ORBIT_NAME, keyOrbitName);
    }

    public static void setEnroll(boolean enroll) {
        setBoolean(KEY_ENROLL, enroll);
    }

    public static boolean getEnroll() {
        return getBoolean(KEY_ENROLL, false);
    }

    public static String getOptionPicArray() {
        return getString(OPTION_PIC_ARRAY, null);
    }

    public static void setOptionPicArray(String optionPicArray) {
        setString(OPTION_PIC_ARRAY, optionPicArray);
    }

    public static int getCountOptionPicCategoryOneSocial() {
        return getInt(COUNT_OPTION_PIC_CATEGORY_ONE_SOCIAL, -1);
    }

    public static void setCountOptionPicCategoryOneSocial(int countOptionPicCategoryOneSocial) {
        setInt(COUNT_OPTION_PIC_CATEGORY_ONE_SOCIAL, countOptionPicCategoryOneSocial);
    }

    public static int getCountOptionPicCategoryTwoDating() {
        return getInt(COUNT_OPTION_PIC_CATEGORY_TWO_DATING, -1);
    }

    public static void setCountOptionPicCategoryTwoDating(int countOptionPicCategoryTwoDating) {
        setInt(COUNT_OPTION_PIC_CATEGORY_TWO_DATING, countOptionPicCategoryTwoDating);
    }

    public static int getCountOptionPicCategoryThreeBusiness() {
        return getInt(COUNT_OPTION_PIC_CATEGORY_THREE_BUSINESS, -1);
    }

    public static void setCountOptionPicCategoryThreeBusiness(int countOptionPicCategoryThreeBusiness) {
        setInt(COUNT_OPTION_PIC_CATEGORY_THREE_BUSINESS, countOptionPicCategoryThreeBusiness);
    }


    public static String getProfilePicForCatId(String catid) {
        return getString(catid, null);
    }

    public static void setProfilePicForCatId(String catid, String profilePic) {
        setString(catid, profilePic);
    }

    public static String getCoverPicForCatId(String catid) {
        return getString(catid, null);
    }

    public static void setCoverPicForCatId(String catid, String profilePic) {
        setString(catid, profilePic);
    }

    public static int getQuickBloxUserID() {
        return getInt(QUICK_BLOX_ID, 0);
    }

    public static void setQuickBloxUserID(int id) {
        setInt(QUICK_BLOX_ID, id);
    }

    public static void setOpenListView(boolean value) {
        setBoolean(KEY_IS_OPEN_LIST, value);
    }

    public static boolean isOpenListView() {
        return getBoolean(KEY_IS_OPEN_LIST, false);
    }

    public static String getRegistrationStatus() {
        return getString(REGISTRATION_STATUS, null);
    }

    public static void setRegistrationStatus(String registrationStatus) {
        setString(REGISTRATION_STATUS, registrationStatus);
    }


    public static String getProfileCatPicPathFB() {
        return getString(PROFILE_PIC_CATEGORY_ONE_FB, null);
    }

    public static void setProfileCatPicPathFB(String profileDefaultfb) {
        setString(PROFILE_PIC_CATEGORY_ONE_FB, profileDefaultfb);
    }


    public static int getAge() {
        return getInt(AGE, 0);
    }

    public static void setAge(int age) {
        setInt(AGE, age);
    }

}
