package com.exceptionaire.ongraviti.core;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;

/**
 * Created by bhakti on 26/2/16.
 */
public class AppSettingsCore {

    private static final String SHARED_PREFERENCES_NAME = "on_graviti";

    private static SharedPreferences getPreferences() {
        try {
            if (AppDetails.getContext() != null)
                return AppDetails.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static SharedPreferences.Editor getEditor() {
        return getPreferences().edit();
    }

    public static void setBoolean(String keyValue, boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(keyValue, value);
        editor.commit();
    }

    public static boolean getBoolean(String keyValue, boolean defaultValue) {
        SharedPreferences sharedPreferences = getPreferences();
        return sharedPreferences.getBoolean(keyValue, defaultValue);
    }

    public static void setString(String keyValue, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(keyValue, value);
        editor.commit();
    }

    public static String getString(String key, String defValue) {
        SharedPreferences pref = getPreferences();
        return pref.getString(key, defValue);
    }

    public static void setInt(String keyValue, int value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putInt(keyValue, value);
        editor.commit();
    }

    public static int getInt(String keyValue, int defValue) {
        SharedPreferences pref = getPreferences();
        return pref.getInt(keyValue, defValue);
    }

    public static void removePrefData() {
        SharedPreferences.Editor editor = getEditor();
        editor.clear();
        editor.commit();
    }


   /* public static void setQBUSER(String keyValue, QBUser value) {
        SharedPreferences.Editor editor = getEditor();
        Gson gson = new Gson();
        String json = gson.toJson(value);
        editor.putString(keyValue, json);
        editor.commit();
    }

    public static String getQBUSER(String key, QBUser defValue) {
        SharedPreferences pref = getPreferences();
        Gson gson = new Gson();

        return pref.getString(key, defValue.toString());

    }*/
}
