package com.exceptionaire.ongraviti.core;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.exceptionaire.ongraviti.database.DaoMaster;
import com.exceptionaire.ongraviti.database.DaoSession;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_model.QbConfigsVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.CoreConfigUtilsVideo;
import com.exceptionaire.ongraviti.quick_blox_video_chat.quick_utils.QBResRequestExecutorVideo;
import com.exceptionaire.ongraviti.utilities.FileUtils;
import com.exceptionaire.ongraviti.quickblox.utils.QBResRequestExecutor;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.core.ServiceZone;

import org.greenrobot.greendao.database.Database;


public class OnGravitiApp extends Application implements AppConstants, ActivityLifecycleCallbacks {
    public static final String TAG = OnGravitiApp.class.getSimpleName();
    private RequestQueue requestQueue;

    public static final boolean ENCRYPTED = false;

    private DaoSession daoSession;

    //@LAX    private QBUser currentUser;
    private QBResRequestExecutor qbResRequestExecutor;
    private QBResRequestExecutorVideo qbResRequestExecutorVideo;
    private static OnGravitiApp mInstance;
    public static Context context;
    public static String mPackageName;

    private static final String QB_CONFIG_DEFAULT_FILE_NAME = "qb_config.json";
    private QbConfigsVideo qbConfigs;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        initApplication();
        initQbConfigs();
        FontUtils.loadFont(getApplicationContext(), "fonts/helvetica_normal.ttf");
        AppDetails.setContext(getApplicationContext());
        registerActivityLifecycleCallbacks(this);
        FileUtils.createApplicationFolder();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, ENCRYPTED ? "notes-db-encrypted" : "ongraviti-db");
        Database db = ENCRYPTED ? helper.getEncryptedWritableDb("super-secret") : helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();


        // Set QuickBlox code //
//@LAX        QBSettings.getInstance().fastConfigInit("50856", "3cwVGvj85nVAuGj", "ZWdhwfZc6OahCzW");
        initCredentials(QB_APP_ID, QB_AUTH_KEY, QB_AUTH_SECRET, QB_ACCOUNT_KEY);
        context = this.getApplicationContext();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        AppGlobals.SCREEN_WIDTH = display.getWidth();
        AppGlobals.SCREEN_HEIGHT = display.getHeight();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        AppPreference.initialize(pref);
        mPackageName = context.getPackageName();

        AppGlobals.init();
    }


    private void initApplication() {
        mInstance = this;
    }

    public void initCredentials(String APP_ID, String AUTH_KEY, String AUTH_SECRET, String ACCOUNT_KEY) {
        QBSettings.getInstance().init(getApplicationContext(), APP_ID, AUTH_KEY, AUTH_SECRET);
//        QBSettings.getInstance().fastConfigInit("51013", "F62UjPafDSCmB-U", "aru4JB5XzZARfAz");
        QBSettings.getInstance().setAccountKey(ACCOUNT_KEY);
        if (!TextUtils.isEmpty(qbConfigs.getApiDomain()) && !TextUtils.isEmpty(qbConfigs.getChatDomain())) {
            QBSettings.getInstance().setEndpoints(qbConfigs.getApiDomain(), qbConfigs.getChatDomain(), ServiceZone.PRODUCTION);
            QBSettings.getInstance().setZone(ServiceZone.PRODUCTION);
        }

    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    public static synchronized OnGravitiApp getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;
    }

    public synchronized QBResRequestExecutor getQbResRequestExecutor() {
        return qbResRequestExecutor == null
                ? qbResRequestExecutor = new QBResRequestExecutor()
                : qbResRequestExecutor;
    }

    public synchronized QBResRequestExecutorVideo getQbResRequestExecutorVideo() {
        return qbResRequestExecutorVideo == null
                ? qbResRequestExecutorVideo = new QBResRequestExecutorVideo()
                : qbResRequestExecutorVideo;
    }



    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
//        AppSettings.setAppIsOpen(true);
    }

    @Override
    public void onActivityPaused(Activity activity) {
//        AppSettings.setAppIsOpen(false);
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }


    private void initQbConfigs() {
        Log.e(TAG, "QB CONFIG FILE NAME: " + getQbConfigFileName());
        qbConfigs = CoreConfigUtilsVideo.getCoreConfigsOrNull(getQbConfigFileName());
    }

    public QbConfigsVideo getQbConfigs() {
        return qbConfigs;
    }

    protected String getQbConfigFileName() {
        return QB_CONFIG_DEFAULT_FILE_NAME;
    }
}