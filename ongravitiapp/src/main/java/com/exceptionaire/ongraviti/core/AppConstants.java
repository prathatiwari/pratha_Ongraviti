package com.exceptionaire.ongraviti.core;

import android.os.Environment;

import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.quickblox.core.utils.ResourceUtils;

import java.io.File;

/**
 * Created by Laxmikant on 26/2/16.
 */

public interface AppConstants {
    /*for finding area*/
    int GOOGLE_API_CLIENT_ID = 0;
    int Radius_GooglePlacesAPI = 10000;

    int profile_photo = 1;
    int profile_video = 2;

    String AUTOCOMPLETE_API_KEY_FOR_PLACES = "AIzaSyCawGUjpJpjyFMcL5NGmulzctklj86OWWU";
    String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    String PLACES_API_DETAILS = "https://maps.googleapis.com/maps/api/place/details/json";
    String TYPE_AUTOCOMPLETE = "/autocomplete";
    String OUT_JSON = "/json";

    String APP_PATH = Environment.getExternalStorageDirectory() + File.separator + "OnGraviti";
    String PROFILE_PICTURES = APP_PATH + File.separator + "profile pictures";

    String SOCIAL_PROFILE_PICTURE = PROFILE_PICTURES + File.separator + "social_pic" + ".jpg";
    String SOCIAL_VIDEO_BIO = PROFILE_PICTURES + File.separator + "social_video_bio" + ".mp4";
    String SOCIAL_VIDEO_BIO_THUMB = PROFILE_PICTURES + File.separator + "social_video_bio_thumb" + ".jpg";

    String DATING_PROFILE_PICTURE = PROFILE_PICTURES + File.separator + "dating_pic" + ".jpg";
    String DATING_VIDEO_BIO = PROFILE_PICTURES + File.separator + "dating_video_bio" + ".mp4";
    String DATING_VIDEO_BIO_THUMB = PROFILE_PICTURES + File.separator + "dating_video_bio_thumb" + ".jpg";

    String BUSINESS_PROFILE_PICTURE = PROFILE_PICTURES + File.separator + "business_pic" + ".jpg";
    String BUSINESS_VIDEO_BIO = PROFILE_PICTURES + File.separator + "business_video_bio" + ".mp4";
    String BUSINESS_VIDEO_BIO_THUMB = PROFILE_PICTURES + File.separator + "business_video_bio_thumb" + ".jpg";

    String REGEX_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    String REGEX_NAME = "^[a-zA-Z\\s?\\'?a-zA-Z]+";
    String REGEX_CHARACTERS_ONLY = "[a-zA-Z][a-zA-Z]+";
    String REGEX_PHONE_NUMBER = "[0-9]{10,15}";
    String REGEX_ADDRESS = ".{1,150}";
    String REGEX_COMMENTS = ".{1,150}";

    /*  Media Base URL's  */
    String POST_IMAGES_THUMB_PATH = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/images/posts_pics/thumbs/";
    String PROFILE_PICTURE_PATH = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/images/profile_pics/media";
    String PROFILE_PICTURE_THUMB = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/images/profile_pics/thumbs";
    String PROFILE_BIO_PATH_S = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/uploads/";
    String PROFILE_BIO_PATH = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/uploads/";
    String PROFILE_PICTURE_COVER = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/images/profile_pics/cover";

    /* API's Used */
    String BASE_URL = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/";
    String get_user_profile_pic_bio = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/get_user_profile_pic_bio";
    String SET_PROFILE_PICTURE = "http://ec2-18-217-245-96.us-east-2.compute.amazonaws.com/api/set_profile_pic";
    String SET_QUETIONAIRE = "set_questionnaire";
    String GET_QUESTIONAIRE = "get_questionnaire";
    String DELETE_QUESTIONAIRE = "delete_questionnaire";
    String COMPAIRE_OTP = "set_compare_OTP";
    String SET_QUICKBLOX_ID = "set_quickbloxid";
    String LOGIN_WITH_FACEBOOK = "login_fb";
    String SIGNUP = "registerUser";
    String URL_FORGOT_PASSWORD = "forgot_password";
    String CHANGE_PASSWORD = "change_password";
    String GET_USER_PROFILE = "get_user_profiles";
    String GET_INTEREST_CATEGORY = "get_interest_category";
    String SET_USER_DEFAULT_INFO = "set_user_default_info";
    String GET_INTEREST_RSS = "get_interest_rss_by_category_id";
    String SET_USER_BUSINESS_INFO = "set_user_business_info";
    String SET_USER_DATING_INFO = "set_user_dating_info";
    String SET_USER_SOCIAL_INFO = "set_user_social_info";
    String USER_POST_LIST = "userpostlist";
    String SET_SHARE_STORY = "set_posts";
    String SET_PROFILE_BIO = BASE_URL + "set_profile_bio";
    String USER_POST_LIKE = "set_like_posts";
    String GET_DATING_USERS = "get_dating_user_list";
    String SET_ASK_FOR_DATING_API = "set_ask_for_dating_event";
    String SET_FAVORITE_USER = "set_favourites_user";
    String GET_FAV_USERS_LINK = "get_favourites_user";
    String DELETE_FAVORITE = "delete_favourites_user";
    String GET_BUSINESS_USERS = "get_business_user_list";

    String SPEED_DATING_APP_PATH = APP_PATH + File.separator + "SpeedDatingVideoBio";
    String SPEED_DATING_VIDEO_BIO = SPEED_DATING_APP_PATH + File.separator + "speed_dating_bio_video" + ".mp4";
    String SPEED_DATING_VIDEO_BIO_THUMB = SPEED_DATING_APP_PATH + File.separator + "speed_dating_bio_video_thumb" + ".jpg";

    String POST_IMAGE_PATH = SPEED_DATING_APP_PATH + File.separator + "post_image_path" + ".jpg";
    String POST_VIDEO_PATH = SPEED_DATING_APP_PATH + File.separator + "post_video_path" + ".mp4";
    String POST_VIDEO_THUMB = SPEED_DATING_APP_PATH + File.separator + "post_video_thumb" + ".jpg";


    /******************
     * Internet Speed's
     ********************/
    String FOURTEEN_TO_SIXTY_FOUR_KBPS = "14 to 64 Kbps";
    String TWENTY_FIVE_KBPS = "25 Kbps";
    String FIFTY_TO_HUNDRED_KBPS = "50 to 100 Kbps";
    String HUNDRED_KBPS = "100 Kbps";
    String FOUR_HUNDRED_TO_ONE_THOUSAND_KBPS = "400 to 1000 Kbps";
    String SIX_HUNDRED_TO_FOURTEEN_HUNDRED_KBPS = "600 to 1400 Kbps";
    String SEVEN_HUNDRED_TO_SEVENTEEN_HUNDRED_KBPS = "700 to 1700 Kbps";

    String ONE_TO_TWO_MBPS = "1 to 2 MBPS";
    String FIVE_MBPS = "5 MBPS";
    String TEN_PLUS_MBPS = "10+ MBPS";
    String TEN_TO_TWENTY_MBPS = "10 to 20 MBPS";
    String TWO_TO_FOURTEEN_MBPS = "2 to 14 MBPS";
    String ONE_TO_TWENTY_THREE_MBPS = "1 to 23 MBPS";
    String HUNDRED_MBPS = "100 Mbps";

    int CAMERA_REQUEST = 1010;
    int GALLERY_REQUEST = 1000;
    int REQUEST_TAKE_GALLERY_VIDEO = 2011;
    int MEDIA_TYPE_RECORD_VIDEO = 2;
    int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 2012;

    String RESPONSE_SUCCESS = "success";
    String RESPONSE_ERROR = "error";

    /*QUICKBLOX CONSTANTS*/
    String GCM_SENDER_ID = "25726407566";

    String EXTRA_COMMAND_TO_SERVICE = "command_for_service";
    int COMMAND_NOT_FOUND = 0;
    int COMMAND_LOGIN = 1;
    int COMMAND_LOGOUT = 2;
    String EXTRA_IS_STARTED_FOR_CALL = "isRunForCall";
    String ALREADY_LOGGED_IN = "You have already logged in chat";

    enum StartConversationReason {
        INCOME_CALL_FOR_ACCEPTION,
        OUTCOME_CALL_MADE
    }

    //CALL ACTIVITY CLOSE REASONS
    int CALL_ACTIVITY_CLOSE_WIFI_DISABLED = 1001;
    String WIFI_DISABLED = "wifi_disabled";

    int MAX_OPPONENTS_COUNT = 6;

    String PREF_CURREN_ROOM_NAME = "current_room_name";
    String PREF_CURRENT_TOKEN = "current_token";
    String PREF_TOKEN_EXPIRATION_DATE = "token_expiration_date";

    String EXTRA_QB_USER = "qb_user";
    String EXTRA_PENDING_INTENT = "pending_Intent";

    String EXTRA_IS_INCOMING_CALL = "conversation_reason";

    String EXTRA_LOGIN_RESULT = "login_result";
    String EXTRA_LOGIN_ERROR_MESSAGE = "login_error_message";
    int EXTRA_LOGIN_RESULT_CODE = 1002;
    int EXTRA_LOGIN_RESULT_CODE_VIDEO = 1003;

    /*
Key for View Request and Show
 */
    String SEND_VIDEO_VIEW_REQUEST = "#####*****#####";
    String SEND_VIDEO_SHOW_START = "#####*******#####";
    String SEND_VIDEO_SHOW_END = "#####*********#####";

    String EK_ACTIVITY_ID = "EK_ACTIVITY_ID";
    String EK_CATEGORY = "EK_CATEGORY";
    String EK_FLAG = "EK_FLAG";
    String EK_NAME = "EK_NAME";
    String EK_RADIUS = "EK_RADIUS";
    String EK_SENDER_ID = "EK_SENDER_ID";
    String EK_URL = "EK_URL";
    String REQ_VIDEO_CAMERAACTIVITY_TYPE = "EK_VIDEO_URL";
    String REQ_IMAGE_CAMERAACTIVITY_TYPE = "EK_IMAGE_URL";
    String REQ_VIDEO_TYPE = "EK_VIDEO_FILE";
    String REQ_IMAGE_TYPE = "EK_IMAGE_FILE";
    String REQ_INPUT_TYPE = "INPUT_TYPE";
    String REQ_INPUT_STRING = "INPUT_STRING";

    String GCM_PROJECT_NUMBER = "631185970441";
    String FIREBASE_USERS = "users";
    String FIREBASE_POSTS = "posts";

    int REQ_IMAGE_FROM_CAMERA = 10001;
    int REQ_IMAGE_FROM_GALLERY = 10002;
    int REQ_IMAGE_FROM_GALLERY_CROP = 10003;
    int REQ_VIDEO_FROM_GALLERY = 10004;
    int REQ_PHOTO_FILE = 10005;
    int REQ_VIDEO_FILE = 10006;
    int REQ_INPUT_VALUE = 10100;

    String[] PERMISSIONS = {android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO};

    String QB_APP_ID = "50856";
    String QB_AUTH_KEY = "3cwVGvj85nVAuGj";
    String QB_AUTH_SECRET = "ZWdhwfZc6OahCzW";
    String QB_ACCOUNT_KEY = "SN7s3qXbDALMcXUmdowo";

    String QB_USERS_TAG = "ongravity";
    String QB_USERS_PASSWORD = "111111111";

    int PREFERRED_IMAGE_SIZE_PREVIEW = ResourceUtils.getDimen(R.dimen.chat_attachment_preview_size);
    int PREFERRED_IMAGE_SIZE_VIEW = ResourceUtils.getDimen(R.dimen.chat_attachment_view_size);
    int PREFERRED_IMAGE_SIZE_FULL = ResourceUtils.dpToPx(320);

    /*
     * Camera
     */
    int CAPTURE_PHOTO = 1;
    int CAPTURE_VIDEO = 2;
    int CAPTURE_VIDEO_START = 3;
    int CAPTURE_VIDEO_STOP = 4;

    String DEFAULT_USER_PASSWORD = "x6Bt0VDy5";
    int ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS = 422;

    String PROFILE_CATEGORY_ONE_SOCIAL = "1";
    String PROFILE_CATEGORY_TWO_DATING = "2";
    String PROFILE_CATEGORY_THREE_BUSINESS = "3";

    String RADIO_SELECTION_STATUS_ZERO = "0";
    String RADIO_SELECTION_STATUS_ONE = "1";

    int PERMISSION_REQUEST_CODE_CAMERA_STORAGE = 1;
    int CAMERA_REQUEST_AVATAR = 1015;
    int GALLERY_REQUEST_AVATAR = 1005;
    int PERMISSION_REQUEST_CODE_CAMERA_STORAGE_BASE = 1;
    int CAMERA_REQUEST_AVATAR_BASE = 2215;
    int GALLERY_REQUEST_AVATAR_BASE = 2205;

    String NOTIFICATION_ACTION_FAVOURITE = "favourite";
    String NOTIFICATION_ACTION_ASK_DATING = "ask_for_event";
    String NOTIFICATION_ACTION_ORBIT_REQUEST = "orbit_request";
    String NOTIFICATION_ACTION_ORBIT_REQUEST_ACCEPTED = "orbit_request_accept";
    String NOTIFICATION_ACTION_DATING_REQUEST_ACCEPTED = "dating_request_accept";
    String NOTIFICATION_ACTION_DATING_REQUEST = "dating_request";
    String NOTIFICATION_ACTION_DATING_REQUEST_REJECTED = "dating_request_rejected";


    String LOGIN_TYPE_FOR_QUICK_FB="FB_QUICK";
    String LOGIN_TYPE_FOR_QUICK_NORMAL="NORMAL_QUICK";

}