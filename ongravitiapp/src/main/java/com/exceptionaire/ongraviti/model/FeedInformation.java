package com.exceptionaire.ongraviti.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 10/11/16.
 */

public class FeedInformation implements Serializable {
    String feed_userid;
    String feed_id;
    String feed_username;
    String feed_media;
    String feed_mediatype;
    String feed_text;
    String feed_date;
    String feed_user_pic;
    String feed_comment;
    int feed_comment_count;
    int count_of_like;
    int count_of_comment;
    String profile_cat_id;

    public int getCount_of_comment() {
        return count_of_comment;
    }

    public void setCount_of_comment(int count_of_comment) {
        this.count_of_comment = count_of_comment;
    }

    int self_like_status;
    private String category;
    private String identity;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getImg_height() {
        return img_height;
    }

    public void setImg_height(String img_height) {
        this.img_height = img_height;
    }

    public String getImg_width() {
        return img_width;
    }

    public void setImg_width(String img_width) {
        this.img_width = img_width;
    }

    String img_height;
    String img_width;

    public String getNextpoststart() {
        return nextpoststart;
    }

    public void setNextpoststart(String nextpoststart) {
        this.nextpoststart = nextpoststart;
    }


    public int getResultpostcount() {
        return resultpostcount;
    }

    public void setResultpostcount(int resultpostcount) {
        this.resultpostcount = resultpostcount;
    }

    int resultpostcount;
    String nextpoststart;

    public boolean isClickedLike() {
        return clickedLike;
    }

    public void setClickedLike(boolean clickedLike) {
        this.clickedLike = clickedLike;
    }

    public boolean clickedLike;
    ArrayList<Likers> likes;
    ArrayList<Comment> comments;


    public FeedInformation() {
    }


    public FeedInformation(String feed_userid, String feed_id, String feed_username, String feed_text,
                           String feed_mediatype, String feed_media, String feed_user_pic, String feed_date, String feed_comment,
                           int feed_comment_count, int count_of_like, int self_like_status, String profile_cat_id) {

        this.feed_userid = feed_userid;
        this.feed_id = feed_id;
        this.feed_username = feed_username;
        this.feed_text = feed_text;
        this.feed_mediatype = feed_mediatype;
        this.feed_media = feed_media;
        this.feed_user_pic = feed_user_pic;
        this.feed_date = feed_date;
        this.feed_comment = feed_comment;
        this.feed_comment_count = feed_comment_count;
        this.count_of_like = count_of_like;
        this.self_like_status = self_like_status;
        this.profile_cat_id = profile_cat_id;


    }

    public int getCount_of_like() {
        return count_of_like;
    }

    public void setCount_of_like(int count_of_like) {
        this.count_of_like = count_of_like;
    }

    public int getSelf_like_status() {
        return self_like_status;
    }

    public void setSelf_like_status(int self_like_status) {
        this.self_like_status = self_like_status;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public ArrayList<Likers> getLikes() {
        return likes;
    }

    public void setLikes(ArrayList<Likers> likes) {
        this.likes = likes;
    }


    public int getFeed_comment_count() {
        return feed_comment_count;
    }

    public void setFeed_comment_count(int feed_comment_count) {
        this.feed_comment_count = feed_comment_count;
    }

    public String getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(String feed_id) {
        this.feed_id = feed_id;
    }

    public String getFeed_comment() {
        return feed_comment;
    }

    public void setFeed_comment(String feed_comment) {
        this.feed_comment = feed_comment;
    }

    public void setFeed_userid(String feed_userid) {
        this.feed_userid = feed_userid;
    }

    public String getFeed_userid() {
        return feed_userid;
    }

    public void setFeed_username(String feed_username) {
        this.feed_username = feed_username;
    }

    public String getFeed_username() {
        return feed_username;
    }

    public void setCategory_type(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }

    public String getCategory_type() {
        return profile_cat_id;
    }

    public String getFeed_text() {
        return feed_text;
    }

    public void setFeed_text(String feed_text) {
        this.feed_text = feed_text;
    }

    public String getFeed_media() {
        return feed_media;
    }

    public void setFeed_media(String feed_media) {
        this.feed_media = feed_media;
    }

    public String getFeed_mediatype() {
        return feed_mediatype;
    }

    public void setFeed_mediatype(String feed_mediatype) {
        this.feed_mediatype = feed_mediatype;
    }

    public String getFeed_user_pic() {
        return feed_user_pic;
    }

    public void setFeed_user_pic(String feed_user_pic) {
        this.feed_user_pic = feed_user_pic;
    }

    public String getFeed_date() {
        return feed_date;
    }

    public void setFeed_date(String feed_date) {
        this.feed_date = feed_date;
    }

}