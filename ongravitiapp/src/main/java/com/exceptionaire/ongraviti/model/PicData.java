package com.exceptionaire.ongraviti.model;

public class PicData
{
    private String set_profile_pic_id;
    private String set_thumb;
    private String category_id;
    private String user_id;
    private String set_profile_picture;

    public String getSet_profile_pic_id ()
    {
        return set_profile_pic_id;
    }

    public void setSet_profile_pic_id (String set_profile_pic_id)
    {
        this.set_profile_pic_id = set_profile_pic_id;
    }

    public String getSet_thumb ()
    {
        return set_thumb;
    }

    public void setSet_thumb (String set_thumb)
    {
        this.set_thumb = set_thumb;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public void setCategory_id (String category_id)
    {
        this.category_id = category_id;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getSet_profile_picture ()
    {
        return set_profile_picture;
    }

    public void setSet_profile_picture (String set_profile_picture)
    {
        this.set_profile_picture = set_profile_picture;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [set_profile_pic_id = "+set_profile_pic_id+", set_thumb = "+set_thumb+", category_id = "+category_id+", user_id = "+user_id+", set_profile_picture = "+set_profile_picture+"]";
    }
}