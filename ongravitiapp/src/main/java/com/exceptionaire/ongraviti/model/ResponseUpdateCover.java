package com.exceptionaire.ongraviti.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseUpdateCover {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("user_data")
    @Expose
    private List<UserDatum> userData = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<UserDatum> getUserData() {
        return userData;
    }

    public void setUserData(List<UserDatum> userData) {
        this.userData = userData;
    }


    public class UserDatum {

        @SerializedName("profile_pic_id")
        @Expose
        private String profilePicId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("profile_picture")
        @Expose
        private String profilePicture;
        @SerializedName("cover_picture")
        @Expose
        private String coverPicture;
        @SerializedName("thumb")
        @Expose
        private String thumb;
        @SerializedName("profile_bio")
        @Expose
        private String profileBio;
        @SerializedName("bio_thumb")
        @Expose
        private String bioThumb;
        @SerializedName("caption_text")
        @Expose
        private String captionText;
        @SerializedName("bio_desc")
        @Expose
        private String bioDesc;

        public String getProfilePicId() {
            return profilePicId;
        }

        public void setProfilePicId(String profilePicId) {
            this.profilePicId = profilePicId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }

        public String getCoverPicture() {
            return coverPicture;
        }

        public void setCoverPicture(String coverPicture) {
            this.coverPicture = coverPicture;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getProfileBio() {
            return profileBio;
        }

        public void setProfileBio(String profileBio) {
            this.profileBio = profileBio;
        }

        public String getBioThumb() {
            return bioThumb;
        }

        public void setBioThumb(String bioThumb) {
            this.bioThumb = bioThumb;
        }

        public String getCaptionText() {
            return captionText;
        }

        public void setCaptionText(String captionText) {
            this.captionText = captionText;
        }

        public String getBioDesc() {
            return bioDesc;
        }

        public void setBioDesc(String bioDesc) {
            this.bioDesc = bioDesc;
        }

    }
}