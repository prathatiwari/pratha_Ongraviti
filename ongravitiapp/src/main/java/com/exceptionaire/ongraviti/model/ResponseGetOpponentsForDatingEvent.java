package com.exceptionaire.ongraviti.model;

public class ResponseGetOpponentsForDatingEvent {
    private String status;
    private User[] user;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User[] getUser() {
        return user;
    }

    public void setUser(User[] user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", user = " + user + "]";
    }
}