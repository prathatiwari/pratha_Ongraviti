package com.exceptionaire.ongraviti.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 16/11/16.
 */

public class OrbitFriend {
    private String userID;
    private String userName;
    private String profilePic;
    private String quickbloxID;
    private String profileType;
    private String profileStatus;
    private String profile_cat_id;

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getQuickbloxID() {
        return quickbloxID;
    }

    public void setQuickbloxID(String quickbloxID) {
        this.quickbloxID = quickbloxID;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public void setCategory_type(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }

    public String getCategory_type() {
        return profile_cat_id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }


    @Override
    public String toString() {
        return getUserName();
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }
}
