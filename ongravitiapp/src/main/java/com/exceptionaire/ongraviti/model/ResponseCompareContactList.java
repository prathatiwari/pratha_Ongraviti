package com.exceptionaire.ongraviti.model;

import com.exceptionaire.ongraviti.database.User_data;

public class ResponseCompareContactList {
    private User_data[] user_data;

    private com.exceptionaire.ongraviti.database.User_data[] Need_to_orbit_request;

    private String status;

    private String user_id;

    private String msg;

    public User_data[] getUser_data() {
        return user_data;
    }

    public void setUser_data(User_data[] user_data) {
        this.user_data = user_data;
    }

    public User_data[] getNeed_to_orbit_request() {
        return Need_to_orbit_request;
    }

    public void setNeed_to_orbit_request(User_data[] Need_to_orbit_request) {
        this.Need_to_orbit_request = Need_to_orbit_request;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ClassPojo [user_data = " + user_data + ", Need_to_orbit_request = " + Need_to_orbit_request + ", status = " + status + ", user_id = " + user_id + ", msg = " + msg + "]";
    }
}