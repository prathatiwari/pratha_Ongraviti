package com.exceptionaire.ongraviti.model;

public class ResponseOrbitContactList {
    private String status;

    private OrbitContact[] orbit_list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public OrbitContact[] getOrbit_list() {
        return orbit_list;
    }

    public void setOrbit_list(OrbitContact[] orbit_list) {
        this.orbit_list = orbit_list;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", orbit_list = " + orbit_list + "]";
    }
}