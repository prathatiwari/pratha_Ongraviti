package com.exceptionaire.ongraviti.model;

public class ResponseResendOTP {
    private String status;

    private String otp;

    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", otp = " + otp + ", msg = " + msg + "]";
    }
}