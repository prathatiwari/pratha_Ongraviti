package com.exceptionaire.ongraviti.model;

import java.util.List;

public class ResponseBlockUser {
    private String status;

    private List<BlockList> blockList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BlockList> getBlockList() {
        return blockList;
    }

    public void setBlockList(List<BlockList> blockList) {
        this.blockList = blockList;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", blockList = " + blockList + "]";
    }
}

			