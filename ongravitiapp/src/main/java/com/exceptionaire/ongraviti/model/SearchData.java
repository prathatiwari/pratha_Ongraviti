package com.exceptionaire.ongraviti.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SearchData implements Parcelable {
    private String contact_number;
    private String profile_picture;
    private String user_id;
    private String thumb;
    private String full_contact_number;
    private String email_address;
    private String c_name;
    private String profile_cat_id;
    private String orbit_status;

    public SearchData() {
    }

    protected SearchData(Parcel in) {
        contact_number = in.readString();
        profile_picture = in.readString();
        user_id = in.readString();
        thumb = in.readString();
        full_contact_number = in.readString();
        email_address = in.readString();
        c_name = in.readString();
        profile_cat_id = in.readString();
        orbit_status = in.readString();
    }

    public static final Creator<SearchData> CREATOR = new Creator<SearchData>() {
        @Override
        public SearchData createFromParcel(Parcel in) {
            return new SearchData(in);
        }

        @Override
        public SearchData[] newArray(int size) {
            return new SearchData[size];
        }
    };

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getFull_contact_number() {
        return full_contact_number;
    }

    public void setFull_contact_number(String full_contact_number) {
        this.full_contact_number = full_contact_number;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getProfile_cat_id() {
        return profile_cat_id;
    }

    public void setProfile_cat_id(String profile_cat_id) {
        this.profile_cat_id = c_name;
    }


    public String getorbit_status() {
        return orbit_status;
    }

    public void setorbit_status(String orbit_status) {
        this.orbit_status = orbit_status;
    }
    @Override
    public String toString() {
        return "ClassPojo [contact_number = " + contact_number + ", profile_picture = " + profile_picture + ", user_id = " + user_id + ", thumb = " + thumb + ", full_contact_number = " + full_contact_number + ", email_address = " + email_address + ", c_name = " + c_name + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(contact_number);
        parcel.writeString(profile_picture);
        parcel.writeString(user_id);
        parcel.writeString(thumb);
        parcel.writeString(full_contact_number);
        parcel.writeString(email_address);
        parcel.writeString(c_name);
        parcel.writeString(profile_cat_id);
        parcel.writeString(orbit_status);
    }
}