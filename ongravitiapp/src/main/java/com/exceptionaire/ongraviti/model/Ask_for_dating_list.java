package com.exceptionaire.ongraviti.model;

public class Ask_for_dating_list {
    private String user_name;

    private String profile_picture;

    private String profile_cat_id;

    private String user_id;

    private String profile_picture_thumb;

    private String ask_for_date_id;
    private String ask_for_date_message;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getProfile_cat_id() {
        return profile_cat_id;
    }

    public void setProfile_cat_id(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProfile_picture_thumb() {
        return profile_picture_thumb;
    }

    public void setProfile_picture_thumb(String profile_picture_thumb) {
        this.profile_picture_thumb = profile_picture_thumb;
    }

    public String getAsk_for_date_id() {
        return ask_for_date_id;
    }

    public void setAsk_for_date_id(String ask_for_date_id) {
        this.ask_for_date_id = ask_for_date_id;
    }


    public String getask_for_date_message() {
        return ask_for_date_message;
    }

    public void setask_for_date_message(String ask_for_date_message) {
        this.ask_for_date_message = ask_for_date_message;
    }

    @Override
    public String toString() {
        return "ClassPojo [user_name = " + user_name + ", profile_picture = " + profile_picture + ", profile_cat_id = " + profile_cat_id + ", user_id = " + user_id + ", profile_picture_thumb = " + profile_picture_thumb + ", ask_for_date_id = " + ask_for_date_id + "]";
    }
}
