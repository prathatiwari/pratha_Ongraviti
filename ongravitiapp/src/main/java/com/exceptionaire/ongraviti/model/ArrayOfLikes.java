package com.exceptionaire.ongraviti.model;

/**
 * Created by Laxmikant on 27/3/17.
 */

public class ArrayOfLikes {
    private String postlike_id;
    private String status;
    private String user_id;
    private String post_id;
    private String c_name;

    public ArrayOfLikes() {
    }

    public ArrayOfLikes(String c_name, String post_id, String postlike_id, String status, String user_id) {
        this.c_name = c_name;
        this.post_id = post_id;
        this.postlike_id = postlike_id;
        this.status = status;
        this.user_id = user_id;
    }

    public String getPostlike_id() {
        return postlike_id;
    }

    public void setPostlike_id(String postlike_id) {
        this.postlike_id = postlike_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    @Override
    public String toString() {
        return "ClassPojo [postlike_id = " + postlike_id + ", status = " + status + ", user_id = " + user_id + ", post_id = " + post_id + ", c_name = " + c_name + "]";
    }
}

