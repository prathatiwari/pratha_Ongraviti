package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

public class ResponseGetInterestSubcategory {
    private String status;

    private ArrayList<Categories> categories;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Categories> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Categories> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", categories = " + categories + "]";
    }
}