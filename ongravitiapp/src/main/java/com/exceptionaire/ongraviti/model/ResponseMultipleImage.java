package com.exceptionaire.ongraviti.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bhakti Gade on 15/9/17.
 */

public class ResponseMultipleImage {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("picData")
    @Expose
    private ArrayList<PicDatum> picData = null;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<PicDatum> getPicData() {
        return picData;
    }

    public void setPicData(ArrayList<PicDatum> picData) {
        this.picData = picData;
    }

    public class PicDatum {

        @SerializedName("set_profile_pic_id")
        @Expose
        private String setProfilePicId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("set_profile_picture")
        @Expose
        private String setProfilePicture;
        @SerializedName("set_thumb")
        @Expose
        private String setThumb;

        public String getSetProfilePicId() {
            return setProfilePicId;
        }

        public void setSetProfilePicId(String setProfilePicId) {
            this.setProfilePicId = setProfilePicId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getSetProfilePicture() {
            return setProfilePicture;
        }

        public void setSetProfilePicture(String setProfilePicture) {
            this.setProfilePicture = setProfilePicture;
        }

        public String getSetThumb() {
            return setThumb;
        }

        public void setSetThumb(String setThumb) {
            this.setThumb = setThumb;
        }
    }
}
