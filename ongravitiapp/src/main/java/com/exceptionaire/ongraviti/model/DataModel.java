package com.exceptionaire.ongraviti.model;

public class DataModel {

    public String name;
    public boolean checked;

    public DataModel(String name, boolean checked) {
        this.name = name;
        this.checked = checked;

    }
}