package com.exceptionaire.ongraviti.model;

public class ResponseNotificationsList {
    private String status;
    private String msg;
    private String totalCount;
    private NotificationDetails[] notification_details;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NotificationDetails[] getNotification_details() {
        return notification_details;
    }

    public void setNotification_details(NotificationDetails[] notification_details) {
        this.notification_details = notification_details;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", msg = " + msg + ", remainingCount = " + totalCount + ", notification_details = " + notification_details + "]";
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNotificationCount() {
        return totalCount;
    }

    public void setNotificationCount(String notificationCount) {
        this.totalCount = notificationCount;
    }
}
