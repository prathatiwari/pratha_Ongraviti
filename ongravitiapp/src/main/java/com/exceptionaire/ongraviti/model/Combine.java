package com.exceptionaire.ongraviti.model;

import com.exceptionaire.ongraviti.activities.base.RoundedImageView;

/**
 * Created by Laxmikant on 29/9/17.
 */

public class Combine {

    private RoundedImageView roundedImageView;
    private Boolean isSelected;
    private String selectedID;


    public Combine(RoundedImageView roundedImageView, Boolean isSeleted) {
        this.roundedImageView = roundedImageView;
        this.isSelected = isSeleted;
    }

    public RoundedImageView getRoundedImageView() {
        return roundedImageView;
    }

    public void setRoundedImageView(RoundedImageView roundedImageView) {
        this.roundedImageView = roundedImageView;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public String getSelectedID() {
        return selectedID;
    }

    public void setSelectedID(String selectedID) {
        this.selectedID = selectedID;
    }
}
