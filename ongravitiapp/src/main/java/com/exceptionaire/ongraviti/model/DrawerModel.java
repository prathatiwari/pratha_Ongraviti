package com.exceptionaire.ongraviti.model;

/**
 * Created by bhakti gade on 10/12/15.
 */
public class DrawerModel {

    public int icon;
    public int comming_soon;
    public String name;

    // Constructor.
    public DrawerModel(int icon, String name, int comming_soon) {

        this.icon = icon;
        this.name = name;
        this.comming_soon = comming_soon;
    }
}
