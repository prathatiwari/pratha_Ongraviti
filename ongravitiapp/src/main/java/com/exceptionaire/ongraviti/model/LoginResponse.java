package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

/**
 * Created by root on 23/3/17.
 */

public class LoginResponse {
    private String user_token;
    private String status;
    private String profile_status;
    private User user;
    private String msg;
    private String flag;
    private int count_profile_cat_dating;
    private int count_profile_cat_business;
    private int count_profile_cat_social;
    private ArrayList<PicData> multiPicData;
    private String registration_status;


    public String getregistration_status() {
        return registration_status;
    }

    public void setregistration_status(String registration_status) {
        this.registration_status = registration_status;
    }

    public int getCount_profile_cat_dating() {
        return count_profile_cat_dating;
    }

    public void setCount_profile_cat_dating(int count_profile_cat_dating) {
        this.count_profile_cat_dating = count_profile_cat_dating;
    }

    public int getCount_profile_cat_business() {
        return count_profile_cat_business;
    }

    public void setCount_profile_cat_business(int count_profile_cat_business) {
        this.count_profile_cat_business = count_profile_cat_business;
    }

    public int getCount_profile_cat_social() {
        return count_profile_cat_social;
    }

    public void setCount_profile_cat_social(int count_profile_cat_social) {
        this.count_profile_cat_social = count_profile_cat_social;
    }


    public ArrayList<PicData> getMultiPicData() {
        return multiPicData;
    }

    public void setMultiPicData(ArrayList<PicData> multiPicData) {
        this.multiPicData = multiPicData;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfile_status() {
        return profile_status;
    }

    public void setProfile_status(String profile_status) {
        this.profile_status = profile_status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ClassPojo [user_token = " + user_token + ", status = " + status + ", profile_status = " + profile_status + ", user = " + user + ", msg = " + msg + "]";
    }


    public class User {
        private String id;
        private String profile_cat_id;
        private String profile_picture;
        private String modified_date;
        private String status;
        private String user_type;
        private String quickblox_id;
        private String registered_date;
        private String password;
        private String full_contact_number;
        private String email_address;
        private String c_name;
        private String quickblox_password;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProfile_cat_id() {
            return profile_cat_id;
        }

        public void setProfile_cat_id(String profile_cat_id) {
            this.profile_cat_id = profile_cat_id;
        }

        public String getProfile_picture() {
            return profile_picture;
        }

        public void setProfile_picture(String profile_picture) {
            this.profile_picture = profile_picture;
        }

        public String getModified_date() {
            return modified_date;
        }

        public void setModified_date(String modified_date) {
            this.modified_date = modified_date;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUser_type() {
            return user_type;
        }

        public void setUser_type(String user_type) {
            this.user_type = user_type;
        }

        public String getQuickblox_id() {
            return quickblox_id;
        }

        public void setQuickblox_id(String quickblox_id) {
            this.quickblox_id = quickblox_id;
        }


        public String getQuickblox_password() {
            return quickblox_password;
        }

        public void setQuickblox_password(String quickblox_password) {
            this.quickblox_password = quickblox_password;
        }


        public String getRegistered_date() {
            return registered_date;
        }

        public void setRegistered_date(String registered_date) {
            this.registered_date = registered_date;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getFull_contact_number() {
            return full_contact_number;
        }

        public void setFull_contact_number(String full_contact_number) {
            this.full_contact_number = full_contact_number;
        }

        public String getEmail_address() {
            return email_address;
        }

        public void setEmail_address(String email_address) {
            this.email_address = email_address;
        }

        public String getC_name() {
            return c_name;
        }

        public void setC_name(String c_name) {
            this.c_name = c_name;
        }

        @Override
        public String toString() {
            return "ClassPojo [id = " + id + ", profile_cat_id = " + profile_cat_id + ", profile_picture = " + profile_picture + ", modified_date = " + modified_date + ", status = " + status + ", user_type = " + user_type + ", quickblox_id = " + quickblox_id + ", registered_date = " + registered_date + ", password = " + password + ", full_contact_number = " + full_contact_number + ", email_address = " + email_address + ", c_name = " + c_name + "]";
        }
    }
}