package com.exceptionaire.ongraviti.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileInfo {

    @SerializedName("profile_pic_id")
    @Expose
    private String profilePicId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("cover_picture")
    @Expose
    private String coverPicture;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("profile_bio")
    @Expose
    private String profileBio;
    @SerializedName("bio_thumb")
    @Expose
    private String bioThumb;
    @SerializedName("caption_text")
    @Expose
    private String captionText;
    @SerializedName("bio_desc")
    @Expose
    private String bioDesc;
    @SerializedName("profile_status")
    @Expose
    private String profileStatus;

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getProfilePicId() {
        return profilePicId;
    }

    public void setProfilePicId(String profilePicId) {
        this.profilePicId = profilePicId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategory_id() {
        return categoryId;
    }

    public void setCategory_id(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProfile_picture() {
        return profilePicture;
    }

    public void setProfile_picture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getCoverPicture() {
        return coverPicture;
    }

    public void setCoverPicture(String coverPicture) {
        this.coverPicture = coverPicture;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getProfile_bio() {
        return profileBio;
    }

    public void setProfile_bio(String profileBio) {
        this.profileBio = profileBio;
    }

    public String getBio_thumb() {
        return bioThumb;
    }

    public void setBio_thumb(String bioThumb) {
        this.bioThumb = bioThumb;
    }

    public String getCaptionText() {
        return captionText;
    }

    public void setCaptionText(String captionText) {
        this.captionText = captionText;
    }

    public String getBioDesc() {
        return bioDesc;
    }

    public void setBioDesc(String bioDesc) {
        this.bioDesc = bioDesc;
    }

}