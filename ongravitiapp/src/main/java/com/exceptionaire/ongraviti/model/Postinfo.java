package com.exceptionaire.ongraviti.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Postinfo implements Parcelable {
    private String posted_by_username;
    private String self_like_status;
    private String posts_pic;
    private String post_id;
    private String visibility_status;
    private String count_of_like;
    private String post_thumb;
    private String post_content;
    private String posts_video;
    private String post_user_id;
    private ArrayList<ArrayOfLikes> array_of_like;
    private String array_of_like_rss;
    private String count_of_res_comments;
    private ArrayList<Array_of_comment> array_of_comment;
    private String array_of_comment_rss;
    private String count_of_comments;
    private String res_type;
    private String posted_by_userpic_thumb;
    private String device_date;
    private String isLoadItem;
    private String profile_cat_id;
    private String posted_by_userpic_profile_picture;

    public String getCategory_type() {
        return profile_cat_id;
    }

    public void setCategory_type(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }


    public Postinfo() {

    }

    public Postinfo(String isLoadItem) {
        this.isLoadItem = isLoadItem;
    }

    protected Postinfo(Parcel in) {
        posted_by_username = in.readString();
        self_like_status = in.readString();
        posts_pic = in.readString();
        post_id = in.readString();
        visibility_status = in.readString();
        count_of_like = in.readString();
        post_thumb = in.readString();
        post_content = in.readString();
        posts_video = in.readString();
        post_user_id = in.readString();
        array_of_like = in.readArrayList(ArrayOfLikes.class.getClassLoader());
        count_of_res_comments = in.readString();
        array_of_comment = in.readArrayList(Array_of_comment.class.getClassLoader());
        count_of_comments = in.readString();
        res_type = in.readString();
        posted_by_userpic_thumb = in.readString();
        device_date = in.readString();
        isLoadItem = in.readString();
        posted_by_userpic_profile_picture = in.readString();
    }

    public static final Creator<Postinfo> CREATOR = new Creator<Postinfo>() {
        @Override
        public Postinfo createFromParcel(Parcel in) {
            return new Postinfo(in);
        }

        @Override
        public Postinfo[] newArray(int size) {
            return new Postinfo[size];
        }
    };

    public String getPosted_by_username() {
        return posted_by_username;
    }

    public void setPosted_by_username(String posted_by_username) {
        this.posted_by_username = posted_by_username;
    }

    public String getposted_by_userpic_profile_picture() {
        return posted_by_userpic_profile_picture;
    }

    public void setposted_by_userpic_profile_picture(String posted_by_userpic_profile_picture) {
        this.posted_by_userpic_profile_picture = posted_by_userpic_profile_picture;
    }

    public String getSelf_like_status() {
        return self_like_status;
    }

    public void setSelf_like_status(String self_like_status) {
        this.self_like_status = self_like_status;
    }

    public String getPosts_pic() {
        return posts_pic;
    }

    public void setPosts_pic(String posts_pic) {
        this.posts_pic = posts_pic;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getVisiblity_status() {
        return visibility_status;
    }

    public void setVisiblity_status(String visibility_status) {
        this.visibility_status = visibility_status;
    }

    public String getCount_of_like() {
        return count_of_like;
    }

    public void setCount_of_like(String count_of_like) {
        this.count_of_like = count_of_like;
    }

    public String getPost_thumb() {
        return post_thumb;
    }

    public void setPost_thumb(String post_thumb) {
        this.post_thumb = post_thumb;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPosts_video() {
        return posts_video;
    }

    public void setPosts_video(String posts_video) {
        this.posts_video = posts_video;
    }

    public String getPost_user_id() {
        return post_user_id;
    }

    public void setPost_user_id(String post_user_id) {
        this.post_user_id = post_user_id;
    }

    public ArrayList<ArrayOfLikes> getArray_of_like() {
        return array_of_like;
    }

    public void setArray_of_like(ArrayList<ArrayOfLikes> array_of_like) {
        this.array_of_like = array_of_like;
    }

    public String getCount_of_res_comments() {
        return count_of_res_comments;
    }

    public void setCount_of_res_comments(String count_of_res_comments) {
        this.count_of_res_comments = count_of_res_comments;
    }

    public ArrayList<Array_of_comment> getArray_of_comment() {
        return array_of_comment;
    }

    public void setArray_of_comment(ArrayList<Array_of_comment> array_of_comment) {
        this.array_of_comment = array_of_comment;
    }

    public String getCount_of_comments() {
        return count_of_comments;
    }

    public void setCount_of_comments(String count_of_comments) {
        this.count_of_comments = count_of_comments;
    }

    public String getRes_type() {
        return res_type;
    }

    public void setRes_type(String res_type) {
        this.res_type = res_type;
    }

    public String getPosted_by_userpic_thumb() {
        return posted_by_userpic_thumb;
    }

    public void setPosted_by_userpic_thumb(String posted_by_userpic_thumb) {
        this.posted_by_userpic_thumb = posted_by_userpic_thumb;
    }


    public String getPost_date() {
        return device_date;
    }

    public void setPost_date(String device_date) {
        this.device_date = device_date;
    }

    @Override
    public String toString() {
        return "ClassPojo [posted_by_username = " + posted_by_username + ", self_like_status = " + self_like_status + ", posts_pic = " + posts_pic + ", post_id = " + post_id + ", count_of_like = " + count_of_like + ", post_thumb = " + post_thumb + ", post_content = " + post_content + ", posts_video = " + posts_video + ", post_user_id = " + post_user_id + ", array_of_like = " + array_of_like + ", count_of_res_comments = " + count_of_res_comments + ", array_of_comment = " + array_of_comment + ", count_of_comments = " + count_of_comments + ", res_type = " + res_type + ", posted_by_userpic_thumb = " + posted_by_userpic_thumb + ", device_date = " + device_date + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(posted_by_username);
        parcel.writeString(self_like_status);
        parcel.writeString(posts_pic);
        parcel.writeString(post_id);
        parcel.writeString(count_of_like);
        parcel.writeString(post_thumb);
        parcel.writeString(post_content);
        parcel.writeString(posts_video);
        parcel.writeString(post_user_id);
        parcel.writeList(array_of_like);
        parcel.writeString(count_of_res_comments);
        parcel.writeList(array_of_comment);
        parcel.writeString(count_of_comments);
        parcel.writeString(res_type);
        parcel.writeString(posted_by_userpic_thumb);
        parcel.writeString(device_date);
        parcel.writeString(isLoadItem);
    }

    public String getArray_of_like_rss() {
        return array_of_like_rss;
    }

    public void setArray_of_like_rss(String array_of_like_rss) {
        this.array_of_like_rss = array_of_like_rss;
    }

    public String getArray_of_comment_rss() {
        return array_of_comment_rss;
    }

    public void setArray_of_comment_rss(String array_of_comment_rss) {
        this.array_of_comment_rss = array_of_comment_rss;
    }

    public String getIsLoadItem() {
        return isLoadItem;
    }

    public void setIsLoadItem(String isLoadItem) {
        this.isLoadItem = isLoadItem;
    }
}