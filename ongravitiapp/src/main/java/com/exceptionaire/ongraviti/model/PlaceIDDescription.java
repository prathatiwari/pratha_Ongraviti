package com.exceptionaire.ongraviti.model;

/**
 * Created by Laxmikant on 4/4/16.
 */
public class PlaceIDDescription {
    private String placeID;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    public PlaceIDDescription() {

    }

    public PlaceIDDescription(String placeID, String description) {
        this.placeID = placeID;
        this.description = description;
    }
}
