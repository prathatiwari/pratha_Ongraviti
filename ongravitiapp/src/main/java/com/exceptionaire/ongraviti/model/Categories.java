package com.exceptionaire.ongraviti.model;

public class Categories {
    private String id;
    private String status;
    private String main_img;
    private String sub_cat_info;
    private String i_cat_id;
    private String sub_category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMain_img() {
        return main_img;
    }

    public void setMain_img(String main_img) {
        this.main_img = main_img;
    }

    public String getSub_cat_info() {
        return sub_cat_info;
    }

    public void setSub_cat_info(String sub_cat_info) {
        this.sub_cat_info = sub_cat_info;
    }

    public String getI_cat_id() {
        return i_cat_id;
    }

    public void setI_cat_id(String i_cat_id) {
        this.i_cat_id = i_cat_id;
    }

    public String getSub_category() {
        return sub_category;
    }

    public void setSub_category(String sub_category) {
        this.sub_category = sub_category;
    }

    @Override
    public String toString() {
        return this.sub_category;//"ClassPojo [id = " + id + ", status = " + status + ", main_img = " + main_img + ", sub_cat_info = " + sub_cat_info + ", i_cat_id = " + i_cat_id + ", sub_category = " + sub_category + "]";
    }
}