package com.exceptionaire.ongraviti.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by root on 2/2/17.
 */

public class UserProfileCategory {

    private String profile_pic_id, user_id_category, category_id, profile_picture, thumb, profile_bio, bio_thumb, caption_text, bio_desc;
    String TAG="UserProfileCategory";

    public UserProfileCategory(String fromFragment,JSONObject categoryjson) {
        try {

            JSONArray jsonArrayCategory = categoryjson.getJSONArray("user_data");
            for (int i = 0; i < jsonArrayCategory.length(); i++) {
                JSONObject jObj = jsonArrayCategory.getJSONObject(i);

                this.profile_pic_id = jObj.getString("profile_pic_id");
                this.user_id_category = jObj.getString("user_id");
                this.category_id = jObj.getString("category_id");
                this.profile_picture = jObj.getString("profile_picture");
                this.thumb = jObj.getString("thumb");
                this.profile_bio = jObj.getString("profile_bio");
                this.bio_thumb = jObj.getString("bio_thumb");
                this.caption_text = jObj.getString("caption_text");
                this.bio_desc = jObj.getString("bio_desc");
            }

        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }

    }

    public UserProfileCategory(JSONObject categoryjson) {
        try {
            this.profile_pic_id = categoryjson.getString("profile_pic_id");
            this.user_id_category = categoryjson.getString("user_id");
            this.category_id = categoryjson.getString("category_id");
            this.profile_picture = categoryjson.getString("profile_picture");
            this.thumb = categoryjson.getString("thumb");
            this.profile_bio = categoryjson.getString("profile_bio");
            this.bio_thumb = categoryjson.getString("bio_thumb");
            this.caption_text = categoryjson.getString("caption_text");
            this.bio_desc = categoryjson.getString("bio_desc");

        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public String getProfile_pic_id() {
        return profile_pic_id;
    }

    public void setProfile_pic_id(String profile_pic_id) {
        this.profile_pic_id = profile_pic_id;
    }

    public String getUser_id_category() {
        return user_id_category;
    }

    public void setUser_id_category(String user_id_category) {
        this.user_id_category = user_id_category;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getProfile_bio() {
        return profile_bio;
    }

    public void setProfile_bio(String profile_bio) {
        this.profile_bio = profile_bio;
    }

    public String getBio_thumb() {
        return bio_thumb;
    }

    public void setBio_thumb(String bio_thumb) {
        this.bio_thumb = bio_thumb;
    }

    public String getCaption_text() {
        return caption_text;
    }

    public void setCaption_text(String caption_text) {
        this.caption_text = caption_text;
    }

    public String getBio_desc() {
        return bio_desc;
    }

    public void setBio_desc(String bio_desc) {
        this.bio_desc = bio_desc;
    }
}
