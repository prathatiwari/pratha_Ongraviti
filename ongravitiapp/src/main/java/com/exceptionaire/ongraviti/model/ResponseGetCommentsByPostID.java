package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

public class ResponseGetCommentsByPostID {
    private ArrayList<Array_of_comment> comment_list;

    private String status;

    public ArrayList<Array_of_comment> getComment_list() {
        return comment_list;
    }

    public void setComment_list(ArrayList<Array_of_comment> comment_list) {
        this.comment_list = comment_list;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [comment_list = " + comment_list + ", status = " + status + "]";
    }
}