package com.exceptionaire.ongraviti.model;

/**
 * Created by root on 9/9/16.
 */
public class Gcontacts {
    private int id;

    public String getMobile_nos() {
        return Mobile_nos;
    }

    public void setMobile_nos(String mobile_nos) {
        Mobile_nos = mobile_nos;
    }

    private String Mobile_nos;
    private String name;
    private String email;

    public Gcontacts(){}

    public Gcontacts(String name, String email) {
        super();

        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
//return "Contact [id=" + id + ", name=" + name + ", email=" + email + "]";
        return name;
    }
}
