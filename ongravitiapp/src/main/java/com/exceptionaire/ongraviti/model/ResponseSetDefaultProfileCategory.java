package com.exceptionaire.ongraviti.model;

public class ResponseSetDefaultProfileCategory {

    private String profile_picture;
    private String profile_cat_id;
    private String status;
    private String msg;

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getProfile_cat_id() {
        return profile_cat_id;
    }

    public void setProfile_cat_id(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ClassPojo [profile_picture = " + profile_picture + ", profile_cat_id = " + profile_cat_id + ", status = " + status + ", msg = " + msg + "]";
    }
}