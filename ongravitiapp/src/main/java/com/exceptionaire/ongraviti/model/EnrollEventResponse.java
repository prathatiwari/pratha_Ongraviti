package com.exceptionaire.ongraviti.model;

/**
 * Created by root on 29/3/17.
 */

public class EnrollEventResponse {

    private String status;
    private String event_purchase_status;
    private String upload_video_status;
    private String current_credit;
    private String event_amount;
    private String msg;

    public String getEvent_purchase_status() {
        return event_purchase_status;
    }

    public void setEvent_purchase_status(String event_purchase_status) {
        this.event_purchase_status = event_purchase_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpload_video_status() {
        return upload_video_status;
    }

    public void setUpload_video_status(String upload_video_status) {
        this.upload_video_status = upload_video_status;
    }

    public String getCurrent_credit() {
        return current_credit;
    }

    public void setCurrent_credit(String current_credit) {
        this.current_credit = current_credit;
    }

    public String getEvent_amount() {
        return event_amount;
    }

    public void setEvent_amount(String event_amount) {
        this.event_amount = event_amount;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
