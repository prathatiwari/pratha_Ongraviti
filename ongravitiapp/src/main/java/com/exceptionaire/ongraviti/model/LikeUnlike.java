package com.exceptionaire.ongraviti.model;

/**
 * Created by root on 29/3/17.
 */
public class LikeUnlike {

    private String status;

    private String like_count;

    private String msg;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getLike_count ()
    {
        return like_count;
    }

    public void setLike_count (String like_count)
    {
        this.like_count = like_count;
    }

    public String getMsg ()
    {
        return msg;
    }

    public void setMsg (String msg)
    {
        this.msg = msg;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", like_count = "+like_count+", msg = "+msg+"]";
    }
}
