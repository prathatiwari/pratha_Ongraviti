package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

/**
 * Created by root on 25/5/17.
 */


public class ProfilePictureResponse {
    private ArrayList<User_data> user_data;
    private String status;
    private String msg;
    private String default_profile_pic_thumb;
    private String default_profile_pic;
    private ArrayList<PicData> multi_profile_info_cat;
    private String default_profile_cat_id;
    private int countMultiple_profile;

    public int getCountMultiple_profile() {
        return countMultiple_profile;
    }

    public void setCountMultiple_profile(int countMultiple_profile) {
        this.countMultiple_profile = countMultiple_profile;
    }


    public String getDefault_profile_pic_thumb() {
        return default_profile_pic_thumb;
    }

    public void setDefault_profile_pic_thumb(String default_profile_pic_thumb) {
        this.default_profile_pic_thumb = default_profile_pic_thumb;
    }

    public String getDefault_profile_pic() {
        return default_profile_pic;
    }

    public void setDefault_profile_pic(String default_profile_pic) {
        this.default_profile_pic = default_profile_pic;
    }

    public ArrayList<PicData> getMulti_profile_info_cat() {
        return multi_profile_info_cat;
    }

    public void setMulti_profile_info_cat(ArrayList<PicData> multi_profile_info_cat) {
        this.multi_profile_info_cat = multi_profile_info_cat;
    }

    public String getDefault_profile_cat_id() {
        return default_profile_cat_id;
    }

    public void setDefault_profile_cat_id(String default_profile_cat_id) {
        this.default_profile_cat_id = default_profile_cat_id;
    }

    public ArrayList<User_data> getUser_data() {
        return user_data;
    }

    public void setUser_data(ArrayList<User_data> user_data) {
        this.user_data = user_data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ClassPojo [user_data = " + user_data + ", status = " + status + ", msg = " + msg + "]";
    }


    /*****************************************************************************/


    public class User_data {
        private String profile_pic_id;
        private String user_id;
        private String category_id;
        private String profile_picture;
        private String thumb;
        private String profile_bio;
        private String bio_thumb;
        private String caption_text;
        private String bio_desc;

        public String getProfile_picture() {
            return profile_picture;
        }

        public void setProfile_picture(String profile_picture) {
            this.profile_picture = profile_picture;
        }

        public String getBio_desc() {
            return bio_desc;
        }

        public void setBio_desc(String bio_desc) {
            this.bio_desc = bio_desc;
        }

        public String getBio_thumb() {
            return bio_thumb;
        }

        public void setBio_thumb(String bio_thumb) {
            this.bio_thumb = bio_thumb;
        }

        public String getCaption_text() {
            return caption_text;
        }

        public void setCaption_text(String caption_text) {
            this.caption_text = caption_text;
        }

        public String getProfile_bio() {
            return profile_bio;
        }

        public void setProfile_bio(String profile_bio) {
            this.profile_bio = profile_bio;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getProfile_pic_id() {
            return profile_pic_id;
        }

        public void setProfile_pic_id(String profile_pic_id) {
            this.profile_pic_id = profile_pic_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        @Override
        public String toString() {
            return "ClassPojo [profile_picture = " + profile_picture + ", bio_desc = " + bio_desc + ", bio_thumb = " + bio_thumb + ", caption_text = " + caption_text + ", profile_bio = " + profile_bio + ", category_id = " + category_id + ", profile_pic_id = " + profile_pic_id + ", user_id = " + user_id + ", thumb = " + thumb + "]";
        }
    }
}

