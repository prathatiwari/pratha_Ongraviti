package com.exceptionaire.ongraviti.model;

public class UserBasicInfo {
    private String political_beliefs;
    private String future_goal;
    private String profile_cat_id;
    private String weight;
    private String beliefs_religious_beliefs;
    private String drinking;
    private String favorite_shows;
    private String degree_name;
    private String events_attended_i_want;
    private String relationship_status;
    private String area_of_interest;
    private String industries_i_want_to_target;
    private String height;
    private String events_attended;
    private String smoking;
    private String name;
    private String books_i_like;
    private String gender;
    private String interested_in;
    private String id;
    private String gym;
    private String profile_info_id;
    private String highest_degree;
    private String favorite_book;
    private String employment_status;
    private String body_type;
    private String date_of_birth;
    private String eye_color;
    private String favorite_movies;
    private String countries_travelled;
    private String hobbies;
    private String college_university_name;
    private String events_m_looking;
    private String interesting_ventures_to_look;
    private String hair_color;
    private String enterpreneurs_i_like;
    private String what_i_read;
    private String my_interest_cat;
    private String city;
    private String traits;

    public String get_interest_category() {
        return my_interest_cat;
    }

    public void set_interest_category(String my_interest_cat) {
        this.my_interest_cat = my_interest_cat;
    }

    public String getPolitical_beliefs() {
        return political_beliefs;
    }

    public void setPolitical_beliefs(String political_beliefs) {
        this.political_beliefs = political_beliefs;
    }

    public String getFuture_goal() {
        return future_goal;
    }

    public void setFuture_goal(String future_goal) {
        this.future_goal = future_goal;
    }

    public String getProfile_cat_id() {
        return profile_cat_id;
    }

    public void setProfile_cat_id(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBeliefs_religious_beliefs() {
        return beliefs_religious_beliefs;
    }

    public void setBeliefs_religious_beliefs(String beliefs_religious_beliefs) {
        this.beliefs_religious_beliefs = beliefs_religious_beliefs;
    }

    public String getDrinking() {
        return drinking;
    }

    public void setDrinking(String drinking) {
        this.drinking = drinking;
    }

    public String getFavorite_shows() {
        return favorite_shows;
    }

    public void setFavorite_shows(String favorite_shows) {
        this.favorite_shows = favorite_shows;
    }

    public String getDegree_name() {
        return degree_name;
    }

    public void setDegree_name(String degree_name) {
        this.degree_name = degree_name;
    }

    public String getEvents_attended_i_want() {
        return events_attended_i_want;
    }

    public void setEvents_attended_i_want(String events_attended_i_want) {
        this.events_attended_i_want = events_attended_i_want;
    }

    public String getRelationship_status() {
        return relationship_status;
    }

    public void setRelationship_status(String relationship_status) {
        this.relationship_status = relationship_status;
    }

    public String getArea_of_interest() {
        return area_of_interest;
    }

    public void setArea_of_interest(String area_of_interest) {
        this.area_of_interest = area_of_interest;
    }

    public String getIndustries_i_want_to_target() {
        return industries_i_want_to_target;
    }

    public void setIndustries_i_want_to_target(String industries_i_want_to_target) {
        this.industries_i_want_to_target = industries_i_want_to_target;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getEvents_attended() {
        return events_attended;
    }

    public void setEvents_attended(String events_attended) {
        this.events_attended = events_attended;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBooks_i_like() {
        return books_i_like;
    }

    public void setBooks_i_like(String books_i_like) {
        this.books_i_like = books_i_like;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInterested_in() {
        return interested_in;
    }

    public void setInterested_in(String interested_in) {
        this.interested_in = interested_in;
    }

    public String getUser_id() {
        return id;
    }

    public void setUser_id(String id) {
        this.id = id;
    }

    public String getGym() {
        return gym;
    }

    public void setGym(String gym) {
        this.gym = gym;
    }

    public String getProfile_info_id() {
        return profile_info_id;
    }

    public void setProfile_info_id(String profile_info_id) {
        this.profile_info_id = profile_info_id;
    }

    public String getHighest_degree() {
        return highest_degree;
    }

    public void setHighest_degree(String highest_degree) {
        this.highest_degree = highest_degree;
    }

    public String getFavorite_book() {
        return favorite_book;
    }

    public void setFavorite_book(String favorite_book) {
        this.favorite_book = favorite_book;
    }

    public String getEmployment_status() {
        return employment_status;
    }

    public void setEmployment_status(String employment_status) {
        this.employment_status = employment_status;
    }

    public String getBody_type() {
        return body_type;
    }

    public void setBody_type(String body_type) {
        this.body_type = body_type;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getEye_color() {
        return eye_color;
    }

    public void setEye_color(String eye_color) {
        this.eye_color = eye_color;
    }

    public String getFavorite_movies() {
        return favorite_movies;
    }

    public void setFavorite_movies(String favorite_movies) {
        this.favorite_movies = favorite_movies;
    }

    public String getCountries_travelled() {
        return countries_travelled;
    }

    public void setCountries_travelled(String countries_travelled) {
        this.countries_travelled = countries_travelled;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getCollege_university_name() {
        return college_university_name;
    }

    public void setCollege_university_name(String college_university_name) {
        this.college_university_name = college_university_name;
    }

    public String getEvents_m_looking() {
        return events_m_looking;
    }

    public void setEvents_m_looking(String events_m_looking) {
        this.events_m_looking = events_m_looking;
    }

    public String getInteresting_ventures_to_look() {
        return interesting_ventures_to_look;
    }

    public void setInteresting_ventures_to_look(String interesting_ventures_to_look) {
        this.interesting_ventures_to_look = interesting_ventures_to_look;
    }

    public String getHair_color() {
        return hair_color;
    }

    public void setHair_color(String hair_color) {
        this.hair_color = hair_color;
    }

    public String getEnterpreneurs_i_like() {
        return enterpreneurs_i_like;
    }

    public void setEnterpreneurs_i_like(String enterpreneurs_i_like) {
        this.enterpreneurs_i_like = enterpreneurs_i_like;
    }

    public String getwhat_i_read() {
        return what_i_read;
    }

    public void setwhat_i_read(String what_i_read) {
        this.what_i_read = what_i_read;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTraits() {
        return traits;
    }

    public void setTraits(String traits) {
        this.traits = traits;
    }

    @Override
    public String toString() {
        return "ClassPojo [political_beliefs = " + political_beliefs + ", future_goal = " + future_goal + ", profile_cat_id = " + profile_cat_id + ", weight = " + weight + ", beliefs_religious_beliefs = " + beliefs_religious_beliefs + ", drinking = " + drinking + ", favorite_shows = " + favorite_shows + ", degree_name = " + degree_name + ", events_attended_i_want = " + events_attended_i_want + ", relationship_status = " + relationship_status + ", area_of_interest = " + area_of_interest + ", industries_i_want_to_target = " + industries_i_want_to_target + ", height = " + height + ", events_attended = " + events_attended + ", smoking = " + smoking + ", name = " + name + ", books_i_like = " + books_i_like + ", gender = " + gender + ", interested_in = " + interested_in + ", id = " + id + ", gym = " + gym + ", profile_info_id = " + profile_info_id + ", highest_degree = " + highest_degree + ", favorite_book = " + favorite_book + ", employment_status = " + employment_status + ", body_type = " + body_type + ", date_of_birth = " + date_of_birth + ", eye_color = " + eye_color + ", favorite_movies = " + favorite_movies + ", countries_travelled = " + countries_travelled + ", hobbies = " + hobbies + ", college_university_name = " + college_university_name + ", events_m_looking = " + events_m_looking + ", interesting_ventures_to_look = " + interesting_ventures_to_look + ", hair_color = " + hair_color + ", enterpreneurs_i_like = " + enterpreneurs_i_like + "]";
    }
}