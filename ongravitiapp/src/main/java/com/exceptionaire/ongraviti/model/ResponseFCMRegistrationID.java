package com.exceptionaire.ongraviti.model;

public class ResponseFCMRegistrationID {
    private String result;

    private String msg;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ClassPojo [result = " + result + ", msg = " + msg + "]";
    }
}