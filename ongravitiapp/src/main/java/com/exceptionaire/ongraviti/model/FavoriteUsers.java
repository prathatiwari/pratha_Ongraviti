package com.exceptionaire.ongraviti.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Bhakti Gade on 26/12/16.
 */

public class FavoriteUsers {
    String TAG = "FavoriteUsers";
    public String favorite_id, favorite_user_id, c_name, profile_picture, thumb, profile_cat_id;
    public ArrayList<FavoriteUsers> list_fav_users;

    public FavoriteUsers(JSONObject jsonObject) {
        try {
            list_fav_users = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray("FavouriteUsers");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject secondjson = jsonArray.getJSONObject(i);

                this.favorite_id = secondjson.getString("favorite_id");
                this.favorite_user_id = secondjson.getString("favorite_user_id");
                this.c_name = secondjson.getString("c_name");
                this.profile_picture = secondjson.getString("profile_picture");
                this.thumb = secondjson.getString("thumb");
                this.profile_cat_id = secondjson.getString("profile_cat_id");
                FavoriteUsers fav_users = new FavoriteUsers(favorite_id, favorite_user_id, c_name, profile_picture, thumb, profile_cat_id);
                list_fav_users.add(fav_users);
            }

        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public FavoriteUsers(String favorite_id, String favorite_user_id, String c_name, String profile_picture, String thumb, String profile_cat_id) {
        this.favorite_id = favorite_id;
        this.favorite_user_id = favorite_user_id;
        this.c_name = c_name;
        this.profile_picture = profile_picture;
        this.thumb = thumb;
        this.profile_cat_id = profile_cat_id;
    }


    public ArrayList<FavoriteUsers> getList_fav_users() {
        return list_fav_users;
    }

    public void setList_fav_users(ArrayList<FavoriteUsers> list_fav_users) {
        this.list_fav_users = list_fav_users;
    }

    public String getFavorite_id() {
        return favorite_id;
    }

    public void setFavorite_id(String favorite_id) {
        this.favorite_id = favorite_id;
    }

    public String getFavorite_user_id() {
        return favorite_user_id;
    }

    public void setFavorite_user_id(String favorite_user_id) {
        this.favorite_user_id = favorite_user_id;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }


    public String getCategory_type() {
        return profile_cat_id;
    }

    public void setCategory_type(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }

}
