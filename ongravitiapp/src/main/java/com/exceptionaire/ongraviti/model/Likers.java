package com.exceptionaire.ongraviti.model;

import java.io.Serializable;

/**
 * Created by root on 18/11/16.
 */

public class Likers implements Serializable
{



    private String id;
    private String feed_id;
    private String first_name;
    private String last_name;
    private String picture;
    private String feed_like_status;

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    public boolean clicked;

    public Likers() {
    }



    public Likers(String id, String feed_id, String first_name, String last_name,
                           String picture, String feed_like_status) {

        this.id = id;
        this.feed_id = feed_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.picture = picture;
        this.feed_like_status = feed_like_status;


    }
    public String getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(String feed_id) {
        this.feed_id = feed_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getFeed_like_status() {
        return feed_like_status;
    }

    public void setFeed_like_status(String feed_like_status) {
        this.feed_like_status = feed_like_status;
    }
}