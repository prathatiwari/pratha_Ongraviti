package com.exceptionaire.ongraviti.model;

/**
 * Created by root on 12/9/16.
 */
public class VideoPOJO {
    String question, answer, question_type, answer_type_one, answer_type_two, answer_type_three, answer_type_four;
    int question_flag;

    public VideoPOJO(String question, String question_type, String answer_type_one, String answer_type_two, String answer_type_three, String answer_type_four, String answer, int flag) {
        this.question = question;
        this.answer = answer;
        this.question_type = question_type;
        this.answer_type_one = answer_type_one;
        this.answer_type_two = answer_type_two;
        this.answer_type_three = answer_type_three;
        this.answer_type_four = answer_type_four;
        this.question_flag = flag;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public String getAnswer_type_one() {
        return answer_type_one;
    }

    public void setAnswer_type_one(String answer_type_one) {
        this.answer_type_one = answer_type_one;
    }

    public String getAnswer_type_two() {
        return answer_type_two;
    }

    public void setAnswer_type_two(String answer_type_two) {
        this.answer_type_two = answer_type_two;
    }

    public String getAnswer_type_three() {
        return answer_type_three;
    }

    public void setAnswer_type_three(String answer_type_three) {
        this.answer_type_three = answer_type_three;
    }

    public String getAnswer_type_four() {
        return answer_type_four;
    }

    public int getQuestion_flag() {
        return question_flag;
    }

    public void setQuestion_flag(int question_flag) {
        this.question_flag = question_flag;
    }

    public void setAnswer_type_four(String answer_type_four) {
        this.answer_type_four = answer_type_four;


    }
}
