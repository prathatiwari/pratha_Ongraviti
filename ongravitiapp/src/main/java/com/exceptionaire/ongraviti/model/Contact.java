package com.exceptionaire.ongraviti.model;

/**
 * Created by root on 7/6/16.
 */
public class Contact {
    String contact_name;
    String contact_number;
    String original_contact_number;
    String contact_email;
    boolean selected = false;

    public Contact() {
    }

    public Contact(String contact_number, String contact_name, String original_contact_number, String contact_email) {
        this.contact_number = contact_number;
        this.contact_name = contact_name;
        this.contact_email = contact_email;
        this.original_contact_number = original_contact_number;
    }

    public Contact(String contact_name, boolean selected) {
        super();
        this.contact_name = contact_name;
        this.selected = selected;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String name) {
        this.contact_name = name;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String number) {
        this.contact_number = number;
    }

    public String getContact_email() {
        return contact_email;
    }

    public void setContact_email(String contact_email) {
        this.contact_email = contact_email;

    }

    public String getOriginal_contact_number() {
        return original_contact_number;
    }

    public void setOriginal_contact_number(String original_contact_number) {
        this.original_contact_number = original_contact_number;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
