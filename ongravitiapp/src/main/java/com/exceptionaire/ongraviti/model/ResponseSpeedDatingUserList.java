package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

/**
 * Created by root on 30/3/17.
 */

public class ResponseSpeedDatingUserList {
    private String status;

    private ArrayList<User_list> user_list;

    private String nexthas;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<User_list> getUser_list() {
        return user_list;
    }

    public void setUser_list(ArrayList<User_list> user_list) {
        this.user_list = user_list;
    }


    public String getNexthas() {
        return nexthas;
    }

    public void setNexthas(String nexthas) {
        this.nexthas = nexthas;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", user_list = " + user_list + ", nexthas = " + nexthas + "]";
    }

    public class User_list {
        private String profile_cat_id;

        private String profile_picture;

        private String quickblox_array;

        private String sender_user_id;

        private String event_id;

        private String receiver_user_id;

        private String c_name;

        private String full_contact_number;

        private String contact_number;

        private String event_video_name;

        private String event_video_thumb;

        private String[] quickblox_details;

        private String purchase_id;

        private String user_id;

        private String quickblox_id;

        private String eventrequest_id;

        private String thumb;

        private String event_flag;

        public String getProfile_cat_id() {
            return profile_cat_id;
        }

        public void setProfile_cat_id(String profile_cat_id) {
            this.profile_cat_id = profile_cat_id;
        }

        public String getProfile_picture() {
            return profile_picture;
        }

        public void setProfile_picture(String profile_picture) {
            this.profile_picture = profile_picture;
        }

        public String getQuickblox_array() {
            return quickblox_array;
        }

        public void setQuickblox_array(String quickblox_array) {
            this.quickblox_array = quickblox_array;
        }

        public String getSender_user_id() {
            return sender_user_id;
        }

        public void setSender_user_id(String sender_user_id) {
            this.sender_user_id = sender_user_id;
        }

        public String getEvent_id() {
            return event_id;
        }

        public void setEvent_id(String event_id) {
            this.event_id = event_id;
        }

        public String getReceiver_user_id() {
            return receiver_user_id;
        }

        public void setReceiver_user_id(String receiver_user_id) {
            this.receiver_user_id = receiver_user_id;
        }

        public String getC_name() {
            return c_name;
        }

        public void setC_name(String c_name) {
            this.c_name = c_name;
        }

        public String getFull_contact_number() {
            return full_contact_number;
        }

        public void setFull_contact_number(String full_contact_number) {
            this.full_contact_number = full_contact_number;
        }

        public String getContact_number() {
            return contact_number;
        }

        public void setContact_number(String contact_number) {
            this.contact_number = contact_number;
        }

        public String getEvent_video_name() {
            return event_video_name;
        }

        public void setEvent_video_name(String event_video_name) {
            this.event_video_name = event_video_name;
        }

        public String getEvent_video_thumb() {
            return event_video_thumb;
        }

        public void setEvent_video_thumb(String event_video_thumb) {
            this.event_video_thumb = event_video_thumb;
        }

        public String[] getQuickblox_details() {
            return quickblox_details;
        }

        public void setQuickblox_details(String[] quickblox_details) {
            this.quickblox_details = quickblox_details;
        }

        public String getPurchase_id() {
            return purchase_id;
        }

        public void setPurchase_id(String purchase_id) {
            this.purchase_id = purchase_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getQuickblox_id() {
            return quickblox_id;
        }

        public void setQuickblox_id(String quickblox_id) {
            this.quickblox_id = quickblox_id;
        }

        public String getEventrequest_id() {
            return eventrequest_id;
        }

        public void setEventrequest_id(String eventrequest_id) {
            this.eventrequest_id = eventrequest_id;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getEvent_flag() {
            return event_flag;
        }

        public void setEvent_flag(String event_flag) {
            this.event_flag = event_flag;
        }

        @Override
        public String toString() {
            return "ClassPojo [profile_cat_id = " + profile_cat_id + ", profile_picture = " + profile_picture + ", quickblox_array = " + quickblox_array + ", sender_user_id = " + sender_user_id + ", event_id = " + event_id + ", receiver_user_id = " + receiver_user_id + ", c_name = " + c_name + ", full_contact_number = " + full_contact_number + ", contact_number = " + contact_number + ", event_video_name = " + event_video_name + ", event_video_thumb = " + event_video_thumb + ", quickblox_details = " + quickblox_details + ", purchase_id = " + purchase_id + ", user_id = " + user_id + ", quickblox_id = " + quickblox_id + ", eventrequest_id = " + eventrequest_id + ", thumb = " + thumb + ", event_flag = " + event_flag + "]";
        }
    }
}