package com.exceptionaire.ongraviti.model;

/**
 * Created by bhakti on 4/25/16.
 */
public class DatingCard {
    public String name;
    public int imageId;

    public DatingCard() {
    }

    public DatingCard(String name, int imageId)
    {
        this.name = name;
        this.imageId = imageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
