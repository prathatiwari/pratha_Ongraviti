package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

public class ResponseGetUserProfileData {

    private String status;
    private String orbitStatus;
    private String msg;
    private UserBasicInfo[] user_basic_info;
    private ProfileInfo[] profile_info;
    private PurchaseEventDetail[] purchaseEventDetails;
    private ArrayList<MultiProfileInfo> multi_profile_info_cat_social;
    private ArrayList<MultiProfileInfo> multi_profile_info_cat_dating;
    private ArrayList<MultiProfileInfo> multi_profile_info_cat_business;
//    private ArrayList<String> hobbies;


//    public ArrayList<String> gethobbies() {
//        return hobbies;
//    }
//
//    public void sethobbies(ArrayList<String> hobbies) {
//        this.hobbies = hobbies;
//    }


    public ArrayList<MultiProfileInfo> getUser_multi_profile_info_cat_dating() {
        return multi_profile_info_cat_dating;
    }

    public void set_multi_profile_info_cat_dating(ArrayList<MultiProfileInfo> multi_profile_info_cat_dating) {
        this.multi_profile_info_cat_dating = multi_profile_info_cat_dating;
    }

    public ArrayList<MultiProfileInfo> getUser_multi_profile_info_cat_business() {
        return multi_profile_info_cat_business;
    }

    public void set_multi_profile_info_cat_business(ArrayList<MultiProfileInfo> multi_profile_info_cat_business) {
        this.multi_profile_info_cat_business = multi_profile_info_cat_business;
    }


    public UserBasicInfo[] getUser_basic_info() {
        return user_basic_info;
    }

    public void setUser_basic_info(UserBasicInfo[] user_basic_info) {
        this.user_basic_info = user_basic_info;
    }

    public ArrayList<MultiProfileInfo> getUser_multi_profile_info() {
        return multi_profile_info_cat_social;
    }

    public void set_multi_profile_info(ArrayList<MultiProfileInfo> multi_profile_info_cat_social) {
        this.multi_profile_info_cat_social = multi_profile_info_cat_social;
    }

    public PurchaseEventDetail[] getPurchaseEventDetails() {
        return purchaseEventDetails;
    }

    public void setPurchaseEventDetails(PurchaseEventDetail[] purchaseEventDetails) {
        this.purchaseEventDetails = purchaseEventDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProfileInfo[] getProfile_info() {
        return profile_info;
    }

    public void setProfile_info(ProfileInfo[] profile_info) {
        this.profile_info = profile_info;
    }

    @Override
    public String toString() {
        return "ClassPojo [user_basic_info = " + user_basic_info + ", status = " + status + ", profile_info = " + profile_info + "]";
    }

    public String getorbitStatus() {
        return orbitStatus;
    }

    public void setorbitStatus(String orbitStatus) {
        this.orbitStatus = orbitStatus;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}