package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

public class MyOrbitRequest {
    private String user_id;
    private String user_name;
    private String profile_picture;
    private String profile_picture_thumb;
    private ArrayList<MutualOrbitDetail> mutualOrbitDetails;
    private String profile_cat_id;

    public ArrayList<MutualOrbitDetail> getMutualOrbitDetails() {
        return this.mutualOrbitDetails;
    }

    public void setMutualOrbitDetails(ArrayList<MutualOrbitDetail> mutualOrbitDetails) {
        this.mutualOrbitDetails = mutualOrbitDetails;
    }


    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_name() {
        return this.user_name;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getProfile_picture() {
        return this.profile_picture;
    }

    public void setProfile_picture_thumb(String profile_picture_thumb) {
        this.profile_picture_thumb = profile_picture_thumb;
    }

    public String getProfile_picture_thumb() {
        return this.profile_picture_thumb;
    }

    public void setCategory_type(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }

    public String getCategory_type() {
        return profile_cat_id;
    }
}