package com.exceptionaire.ongraviti.model;

/**
 * Created by root on 20/6/16.
 */
public class CategoryInterest
{
    String cat_image,cat_type,cat_info,cat_status,cat_id;

    public CategoryInterest() {
    }

    public CategoryInterest(String cat_image, String cat_type, String cat_info, String cat_status, String cat_id) {
        this.cat_image = cat_image;
        this.cat_type = cat_type;
        this.cat_info = cat_info;
        this.cat_status = cat_status;
        this.cat_id = cat_id;
    }

    public String getCat_image() {
        return cat_image;
    }

    public void setCat_image(String cat_image) {
        this.cat_image = cat_image;
    }

    public String getCat_type() {
        return cat_type;
    }

    public void setCat_type(String cat_type) {
        this.cat_type = cat_type;
    }

    public String getCat_info() {
        return cat_info;
    }

    public void setCat_info(String cat_info) {
        this.cat_info = cat_info;
    }

    public String getCat_status() {
        return cat_status;
    }

    public void setCat_status(String cat_status) {
        this.cat_status = cat_status;
    }

    public String getCat_id()
    {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    @Override
    public String toString() {
        return getCat_type();
    }
}
