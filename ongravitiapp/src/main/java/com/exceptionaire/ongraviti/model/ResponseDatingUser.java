package com.exceptionaire.ongraviti.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResponseDatingUser {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("c_name")
    @Expose
    private String cName;
    @SerializedName("profile_picture")
    @Expose
    private Object profilePicture;
    @SerializedName("thumb")
    @Expose
    private Object thumb;
    @SerializedName("profile_cat_id")
    @Expose
    private String profileCatId;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("relationship_status")
    @Expose
    private String relationshipStatus;
    @SerializedName("weight")
    @Expose
    private String weight;

    @SerializedName("height")
    @Expose
    private String height;

    @SerializedName("traits")
    @Expose
    private String traits;

    @SerializedName("body_type")
    @Expose
    private String bodyType;
    @SerializedName("smoking")
    @Expose
    private String smoking;
    @SerializedName("drinking")
    @Expose
    private String drinking;
    @SerializedName("hair_color")
    @Expose
    private String hairColor;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("highest_degree")
    @Expose
    private String highestDegree;
    @SerializedName("college_university_name")
    @Expose
    private String collegeUniversityName;
    @SerializedName("total_orbit")
    @Expose
    private Integer totalOrbit;
    @SerializedName("total_event")
    @Expose
    private Integer totalEvent;
    @SerializedName("total_askforevent")
    @Expose
    private Integer totalAskforevent;
    @SerializedName("multipleProfileSet")
    @Expose
    private ArrayList<MultiProfileInfo> multipleProfileSet = null;
    @SerializedName("favorite")
    @Expose
    private Integer favorite;

    private String profile_bio;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCName() {
        return cName;
    }

    public void setCName(String cName) {
        this.cName = cName;
    }

    public Object getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Object profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Object getThumb() {
        return thumb;
    }

    public void setThumb(Object thumb) {
        this.thumb = thumb;
    }

    public String getProfileCatId() {
        return profileCatId;
    }

    public void setProfileCatId(String profileCatId) {
        this.profileCatId = profileCatId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRelationshipStatus() {
        return relationshipStatus;
    }

    public void setRelationshipStatus(String relationshipStatus) {
        this.relationshipStatus = relationshipStatus;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String weight) {
        this.height = height;
    }


    public String gettraits() {
        return traits;
    }

    public void settraits(String weight) {
        this.traits = traits;
    }


    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getDrinking() {
        return drinking;
    }

    public void setDrinking(String drinking) {
        this.drinking = drinking;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getHighestDegree() {
        return highestDegree;
    }

    public void setHighestDegree(String highestDegree) {
        this.highestDegree = highestDegree;
    }

    public String getCollegeUniversityName() {
        return collegeUniversityName;
    }

    public void setCollegeUniversityName(String collegeUniversityName) {
        this.collegeUniversityName = collegeUniversityName;
    }

    public Integer getTotalOrbit() {
        return totalOrbit;
    }

    public void setTotalOrbit(Integer totalOrbit) {
        this.totalOrbit = totalOrbit;
    }

    public Integer getTotalEvent() {
        return totalEvent;
    }

    public void setTotalEvent(Integer totalEvent) {
        this.totalEvent = totalEvent;
    }

    public Integer getTotalAskforevent() {
        return totalAskforevent;
    }

    public void setTotalAskforevent(Integer totalAskforevent) {
        this.totalAskforevent = totalAskforevent;
    }

    public ArrayList<MultiProfileInfo> getMultipleProfileSet() {
        return multipleProfileSet;
    }

    public void setMultipleProfileSet(ArrayList<MultiProfileInfo> multipleProfileSet) {
        this.multipleProfileSet = multipleProfileSet;
    }

    public Integer getFavorite() {
        return favorite;
    }

    public void setFavorite(Integer favorite) {
        this.favorite = favorite;
    }

    public String getProfile_bio() {
        return profile_bio;
    }

    public void setProfile_bio(String profile_bio) {
        this.profile_bio = profile_bio;
    }
}