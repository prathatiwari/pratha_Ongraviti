package com.exceptionaire.ongraviti.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponseDatingUsersList {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("user_list")
    @Expose
    private ArrayList<ResponseDatingUser> userList = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ResponseDatingUser> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<ResponseDatingUser> userList) {
        this.userList = userList;
    }
}

