package com.exceptionaire.ongraviti.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by root on 14/2/17.
 */

public class Questionaire {

    String TAG = "Questionaire";
    ArrayList<Questionaire> list_questionaire;

    public String ques_id, question_type, question, question_option, answer; //, ans1, ans2, ans3, ans4;

    public Questionaire(JSONObject jsonObject) {
        try{
            list_questionaire=new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray("question");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject secondjson = jsonArray.getJSONObject(i);

                this.ques_id = secondjson.getString("ques_id");
                this.question = secondjson.getString("question");
                this.question_type = secondjson.getString("question_type");
                this.answer = secondjson.getString("answer");
                this.question_option = secondjson.getString("question_option");

                Questionaire questionaire=new Questionaire(this.ques_id,this.question,this.question_type,this.answer,this.question_option);
                list_questionaire.add(questionaire);
            }
            Log.d(TAG,"## list_questionaire : "+list_questionaire.size());

        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public Questionaire(String ques_id,String question, String question_type, String answer,String question_option) {
        this.ques_id=ques_id;
        this.question_type = question_type;
        this.question = question;
        this.answer = answer;
        this.question_option=question_option;
    }

   /* public Questionaire(String question, String question_type, String answer,String question_option) {
        this.question_type = question_type;
        this.question = question;
        this.answer = answer;
        this.question_option=question_option;
    }*/

    public ArrayList<Questionaire> getList_questionaire() {
        return list_questionaire;
    }

    public void setList_questionaire(ArrayList<Questionaire> list_questionaire) {
        this.list_questionaire = list_questionaire;
    }

    public String getQues_id() {
        return ques_id;
    }

    public void setQues_id(String ques_id) {
        this.ques_id = ques_id;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestion_option() {
        return question_option;
    }

    public void setQuestion_option(String question_option) {
        this.question_option = question_option;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
