package com.exceptionaire.ongraviti.model;

import java.util.List;

public class ResponseMyOrbitRequests {
    private String status;
    private String msg;
    private List<MyOrbitRequest> orbit_list;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setOrbit_list(List<MyOrbitRequest> orbit_list) {
        this.orbit_list = orbit_list;
    }

    public List<MyOrbitRequest> getOrbit_list() {
        return this.orbit_list;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}