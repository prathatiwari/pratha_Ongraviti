package com.exceptionaire.ongraviti.model;

public class User {
    private String contact_number;
    private String profile_picture;
    private String purchase_id;
    private String user_id;
    private String full_contact_number;
    private String email_address;
    private String c_name;

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getPurchase_id() {
        return purchase_id;
    }

    public void setPurchase_id(String purchase_id) {
        this.purchase_id = purchase_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFull_contact_number() {
        return full_contact_number;
    }

    public void setFull_contact_number(String full_contact_number) {
        this.full_contact_number = full_contact_number;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    @Override
    public String toString() {
        return "ClassPojo [contact_number = " + contact_number + ", profile_picture = " + profile_picture + ", purchase_id = " + purchase_id + ", user_id = " + user_id + ", full_contact_number = " + full_contact_number + ", email_address = " + email_address + ", c_name = " + c_name + "]";
    }
}