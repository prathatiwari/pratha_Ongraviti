package com.exceptionaire.ongraviti.model;

public class MutualOrbitDetail {
    private String user_id;
    private String user_name;
    private String profile_picture;
    private String profile_picture_thumb;

    public String getUserId() {
        return this.user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getUserName() {
        return this.user_name;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getProfilePicture() {
        return this.profile_picture;
    }

    public void setProfilePicture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getProfilePictureThumb() {
        return this.profile_picture_thumb;
    }

    public void setProfilePictureThumb(String profile_picture_thumb) {
        this.profile_picture_thumb = profile_picture_thumb;
    }
}