package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 16/11/16.
 */

public class OrbitRequest {
    private String userID;
    private String userName;
    private String  profilePic;
    private List<OrbitFriend> orbitFriendList = new ArrayList<>();

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    private String Flag;

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public List<OrbitFriend> getOrbitFriendList() {
        return orbitFriendList;
    }

    public void setOrbitFriendList(List<OrbitFriend> orbitFriendList) {
        this.orbitFriendList = orbitFriendList;
    }
}
