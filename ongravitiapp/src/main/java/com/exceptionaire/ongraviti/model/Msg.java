package com.exceptionaire.ongraviti.model;

public class Msg {
    private String message;

    private String action_by_profile_pic;

    private String action;

    private String action_by;

    private String action_time;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAction_by_profile_pic() {
        return action_by_profile_pic;
    }

    public void setAction_by_profile_pic(String action_by_profile_pic) {
        this.action_by_profile_pic = action_by_profile_pic;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction_by() {
        return action_by;
    }

    public void setAction_by(String action_by) {
        this.action_by = action_by;
    }

    public String getAction_time() {
        return action_time;
    }

    public void setAction_time(String action_time) {
        this.action_time = action_time;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", action_by_profile_pic = " + action_by_profile_pic + ", action = " + action + ", action_by = " + action_by + ", action_time = " + action_time + "]";
    }
}
