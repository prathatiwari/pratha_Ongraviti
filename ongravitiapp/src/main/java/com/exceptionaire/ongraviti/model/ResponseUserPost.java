package com.exceptionaire.ongraviti.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bhakti Gade on 14/9/17.
 */


public class ResponseUserPost {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("postinfo")
    @Expose
    private ArrayList<Postinfo> postinfo = null;
    @SerializedName("totalpost")
    @Expose
    private Integer totalpost;
    @SerializedName("resultpostcount")
    @Expose
    private Integer resultpostcount;
    @SerializedName("nextpoststart")
    @Expose
    private Object nextpoststart;
    @SerializedName("lastPostId")
    @Expose
    private String lastPostId;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Postinfo> getPostinfo() {
        return postinfo;
    }

    public void setPostinfo(ArrayList<Postinfo> postinfo) {
        this.postinfo = postinfo;
    }

    public Integer getTotalpost() {
        return totalpost;
    }

    public void setTotalpost(Integer totalpost) {
        this.totalpost = totalpost;
    }

    public Integer getResultpostcount() {
        return resultpostcount;
    }

    public void setResultpostcount(Integer resultpostcount) {
        this.resultpostcount = resultpostcount;
    }

    public Object getNextpoststart() {
        return nextpoststart;
    }

    public void setNextpoststart(Object nextpoststart) {
        this.nextpoststart = nextpoststart;
    }

    public String getLastPostId() {
        return lastPostId;
    }

    public void setLastPostId(String lastPostId) {
        this.lastPostId = lastPostId;
    }


    public class Postinfo {

        @SerializedName("post_id")
        @Expose
        private String postId;
        @SerializedName("post_user_id")
        @Expose
        private String postUserId;
        @SerializedName("post_content")
        @Expose
        private String postContent;
        @SerializedName("posts_pic")
        @Expose
        private String postsPic;
        @SerializedName("post_thumb")
        @Expose
        private String postThumb;
        @SerializedName("posts_video")
        @Expose
        private String postsVideo;
        @SerializedName("profile_picture")
        @Expose
        private String profilePicture;
        @SerializedName("thumb")
        @Expose
        private String thumb;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("post_date")
        @Expose
        private String postDate;
        @SerializedName("count_of_like")
        @Expose
        private String countOfLike;
        @SerializedName("count_of_res_comments")
        @Expose
        private Integer countOfResComments;
        @SerializedName("count_of_comments")
        @Expose
        private Integer countOfComments;
        @SerializedName("self_like_status")
        @Expose
        private String selfLikeStatus;
        @SerializedName("array_of_like")
        @Expose
        private ArrayList<Object> arrayOfLike = null;
        @SerializedName("array_of_comment")
        @Expose
        private ArrayList<Object> arrayOfComment = null;
        @SerializedName("visibility_status")
        @Expose
        private String visibility_status;
        @SerializedName("profile_cat_id")
        @Expose
        private String profile_cat_id;

        public String getvisibility_status() {
            return visibility_status;
        }

        public void setvisibility_status(String visibility_status) {
            this.visibility_status = visibility_status;
        }

        public String getprofile_cat_id() {
            return profile_cat_id;
        }

        public void setprofile_cat_id(String profile_cat_id) {
            this.profile_cat_id = profile_cat_id;
        }

        public String getPostId() {
            return postId;
        }

        public void setPostId(String postId) {
            this.postId = postId;
        }

        public String getPostUserId() {
            return postUserId;
        }

        public void setPostUserId(String postUserId) {
            this.postUserId = postUserId;
        }

        public String getPostContent() {
            return postContent;
        }

        public void setPostContent(String postContent) {
            this.postContent = postContent;
        }

        public String getPostsPic() {
            return postsPic;
        }

        public void setPostsPic(String postsPic) {
            this.postsPic = postsPic;
        }

        public String getPostThumb() {
            return postThumb;
        }

        public void setPostThumb(String postThumb) {
            this.postThumb = postThumb;
        }

        public String getPostsVideo() {
            return postsVideo;
        }

        public void setPostsVideo(String postsVideo) {
            this.postsVideo = postsVideo;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPostDate() {
            return postDate;
        }

        public void setPostDate(String postDate) {
            this.postDate = postDate;
        }

        public String getCountOfLike() {
            return countOfLike;
        }

        public void setCountOfLike(String countOfLike) {
            this.countOfLike = countOfLike;
        }

        public Integer getCountOfResComments() {
            return countOfResComments;
        }

        public void setCountOfResComments(Integer countOfResComments) {
            this.countOfResComments = countOfResComments;
        }

        public Integer getCountOfComments() {
            return countOfComments;
        }

        public void setCountOfComments(Integer countOfComments) {
            this.countOfComments = countOfComments;
        }

        public String getSelfLikeStatus() {
            return selfLikeStatus;
        }

        public void setSelfLikeStatus(String selfLikeStatus) {
            this.selfLikeStatus = selfLikeStatus;
        }

        public ArrayList<Object> getArrayOfLike() {
            return arrayOfLike;
        }

        public void setArrayOfLike(ArrayList<Object> arrayOfLike) {
            this.arrayOfLike = arrayOfLike;
        }

        public ArrayList<Object> getArrayOfComment() {
            return arrayOfComment;
        }

        public void setArrayOfComment(ArrayList<Object> arrayOfComment) {
            this.arrayOfComment = arrayOfComment;
        }

    }

}
