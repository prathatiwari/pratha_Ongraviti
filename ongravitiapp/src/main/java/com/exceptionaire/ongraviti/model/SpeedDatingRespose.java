package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

/**
 * Created by root on 27/3/17.
 */


public class SpeedDatingRespose
{
    private ArrayList<Event_details> event_details;

    private String status;

    public ArrayList<Event_details> getEvent_details() {
        return event_details;
    }

    public void setEvent_details(ArrayList<Event_details> event_details) {
        this.event_details = event_details;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [event_details = "+event_details+", status = "+status+"]";
    }



    public class Event_details
    {
        private String amount;

        private String title;

        private String event_end_time;

        private String event_date;

        private String event_desc;

        private String event_st_time;

        private String event_id;

        public String getAmount ()
        {
            return amount;
        }

        public void setAmount (String amount)
        {
            this.amount = amount;
        }

        public String getTitle ()
        {
            return title;
        }

        public void setTitle (String title)
        {
            this.title = title;
        }

        public String getEvent_end_time ()
        {
            return event_end_time;
        }

        public void setEvent_end_time (String event_end_time)
        {
            this.event_end_time = event_end_time;
        }

        public String getEvent_date ()
        {
            return event_date;
        }

        public void setEvent_date (String event_date)
        {
            this.event_date = event_date;
        }

        public String getEvent_desc ()
        {
            return event_desc;
        }

        public void setEvent_desc (String event_desc)
        {
            this.event_desc = event_desc;
        }

        public String getEvent_st_time ()
        {
            return event_st_time;
        }

        public void setEvent_st_time (String event_st_time)
        {
            this.event_st_time = event_st_time;
        }

        public String getEvent_id ()
        {
            return event_id;
        }

        public void setEvent_id (String event_id)
        {
            this.event_id = event_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [amount = "+amount+", title = "+title+", event_end_time = "+event_end_time+", event_date = "+event_date+", event_desc = "+event_desc+", event_st_time = "+event_st_time+", event_id = "+event_id+"]";
        }
    }




}

