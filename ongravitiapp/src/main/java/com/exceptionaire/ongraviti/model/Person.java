package com.exceptionaire.ongraviti.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by root on 10/2/17.
 */

public class Person{

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public LatLng getmPosition() {
        return mPosition;
    }

    public void setmPosition(LatLng mPosition) {
        this.mPosition = mPosition;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    private String name;
    private String profilePhoto;
    private LatLng mPosition;
    private String profileType;
    private String user_id;

    public Person(String user_id, LatLng position, String name, String picture,String profileType) {
        this.user_id=user_id;
        this.name = name;
        this.profilePhoto = picture;
        this.mPosition = position;
        this.profileType = profileType;
    }

}
