package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

public class ResponseQuestionnaireList {
    private String status;

    private String user_id;

    private ArrayList<Question> question;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public ArrayList<Question> getQuestion() {
        return question;
    }

    public void setQuestion(ArrayList<Question> question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", user_id = " + user_id + ", question = " + question + "]";
    }
}
