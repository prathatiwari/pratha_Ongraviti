package com.exceptionaire.ongraviti.model;

public class Question
{
    private String ques_id;

    private String question_type;

    private String answer;

    private String question_option;

    private String question;

    public String getQues_id ()
    {
        return ques_id;
    }

    public void setQues_id (String ques_id)
    {
        this.ques_id = ques_id;
    }

    public String getQuestion_type ()
    {
        return question_type;
    }

    public void setQuestion_type (String question_type)
    {
        this.question_type = question_type;
    }

    public String getAnswer ()
    {
        return answer;
    }

    public void setAnswer (String answer)
    {
        this.answer = answer;
    }

    public String getQuestion_option ()
    {
        return question_option;
    }

    public void setQuestion_option (String question_option)
    {
        this.question_option = question_option;
    }

    public String getQuestion ()
    {
        return question;
    }

    public void setQuestion (String question)
    {
        this.question = question;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ques_id = "+ques_id+", question_type = "+question_type+", answer = "+answer+", question_option = "+question_option+", question = "+question+"]";
    }
}
