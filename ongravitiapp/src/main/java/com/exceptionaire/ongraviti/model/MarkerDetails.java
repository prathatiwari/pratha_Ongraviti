package com.exceptionaire.ongraviti.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 26/12/16.
 */

public class MarkerDetails implements Parcelable {

    public double longitude;
    public double latitude;
    public String address;

    public MarkerDetails(double longitude, double latitude, String address) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.address = address;
    }

    protected MarkerDetails(Parcel in) {
        longitude = in.readDouble();
        latitude = in.readDouble();
        address = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
        dest.writeString(address);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MarkerDetails> CREATOR = new Parcelable.Creator<MarkerDetails>() {
        @Override
        public MarkerDetails createFromParcel(Parcel in) {
            return new MarkerDetails(in);
        }

        @Override
        public MarkerDetails[] newArray(int size) {
            return new MarkerDetails[size];
        }
    };
}
