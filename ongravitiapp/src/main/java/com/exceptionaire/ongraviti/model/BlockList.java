package com.exceptionaire.ongraviti.model;

public class BlockList {
    private String profile_cat_id;

    private String profile_picture;

    private String orbit_id;

    private String sender_user_id;

    private String orbit_date;

    private String c_name;

    private String receiver_user_id;

    public String getProfile_cat_id() {
        return profile_cat_id;
    }

    public void setProfile_cat_id(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getOrbit_id() {
        return orbit_id;
    }

    public void setOrbit_id(String orbit_id) {
        this.orbit_id = orbit_id;
    }

    public String getSender_user_id() {
        return sender_user_id;
    }

    public void setSender_user_id(String sender_user_id) {
        this.sender_user_id = sender_user_id;
    }

    public String getOrbit_date() {
        return orbit_date;
    }

    public void setOrbit_date(String orbit_date) {
        this.orbit_date = orbit_date;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getReceiver_user_id() {
        return receiver_user_id;
    }

    public void setReceiver_user_id(String receiver_user_id) {
        this.receiver_user_id = receiver_user_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [profile_cat_id = " + profile_cat_id + ", profile_picture = " + profile_picture + ", orbit_id = " + orbit_id + ", sender_user_id = " + sender_user_id + ", orbit_date = " + orbit_date + ", c_name = " + c_name + ", receiver_user_id = " + receiver_user_id + "]";
    }
}
