package com.exceptionaire.ongraviti.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhakti Gade on 18/9/17.
 */

public class MultiProfileInfo {
    @SerializedName("set_profile_pic_id")
    @Expose
    private String setProfilePicId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("set_profile_picture")
    @Expose
    private String setProfilePicture;
    @SerializedName("set_thumb")
    @Expose
    private String setThumb;

    public String getSetProfilePicId() {
        return setProfilePicId;
    }

    public void setSetProfilePicId(String setProfilePicId) {
        this.setProfilePicId = setProfilePicId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSetProfilePicture() {
        return setProfilePicture;
    }

    public void setSetProfilePicture(String setProfilePicture) {
        this.setProfilePicture = setProfilePicture;
    }

    public String getSetThumb() {
        return setThumb;
    }

    public void setSetThumb(String setThumb) {
        this.setThumb = setThumb;
    }

}
