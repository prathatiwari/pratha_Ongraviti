package com.exceptionaire.ongraviti.model;

public class OrbitContact {
    private String profile_picture;
    private String orbit_id;
    private String quickblox_id;
    private String thumb;
    private String orbit_name;
    private Boolean isSelected;


    public OrbitContact(String profile_picture, String orbit_id, String quickblox_id, String thumb, String orbit_name) {

    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getOrbit_id() {
        return orbit_id;
    }

    public void setOrbit_id(String orbit_id) {
        this.orbit_id = orbit_id;
    }

    public String getQuickblox_id() {
        return quickblox_id;
    }

    public void setQuickblox_id(String quickblox_id) {
        this.quickblox_id = quickblox_id;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getOrbit_name() {
        return orbit_name;
    }

    public void setOrbit_name(String orbit_name) {
        this.orbit_name = orbit_name;
    }

    @Override
    public String toString() {
        return "ClassPojo [profile_picture = " + profile_picture + ", orbit_id = " + orbit_id + ", quickblox_id = " + quickblox_id + ", thumb = " + thumb + ", orbit_name = " + orbit_name + "]";
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }
}