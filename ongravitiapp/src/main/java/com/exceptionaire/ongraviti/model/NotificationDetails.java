package com.exceptionaire.ongraviti.model;

public class NotificationDetails {
    private String read_status;
    private String device_date;
    private String sender_user_id;
    private String notification;
    private String notification_id;
    private String profile_pic;
    private String sender_name;
    private String profile_cat_id;
    private String action;

    public String getaction() {
        return action;
    }

    public void setaction(String action) {
        this.action = action;
    }

    public String getCategory_type() {
        return profile_cat_id;
    }

    public void setCategory_type(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }

    public String getRead_status() {
        return read_status;
    }

    public void setRead_status(String read_status) {
        this.read_status = read_status;
    }

    public String getUpdated_date() {
        return device_date;
    }

    public void setUpdated_date(String device_date) {
        this.device_date = device_date;
    }

    public String getSender_user_id() {
        return sender_user_id;
    }

    public void setSender_user_id(String sender_user_id) {
        this.sender_user_id = sender_user_id;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    @Override
    public String toString() {
        return "ClassPojo [read_status = " + read_status + ", device_date = " + device_date + ", sender_user_id = " + sender_user_id + ", notification = " + notification + ", notification_id = " + notification_id + ", profile_pic = " + profile_pic + ", sender_name = " + sender_name + "]";
    }
}