package com.exceptionaire.ongraviti.model;

/**
 * Created by root on 18/11/16.
 */

public class  Comment
{

    private String picture;

    private String id;

    private String first_name;

    private String feed_comment_date;

    private String feed_comment_id;

    private String feed_comment_text;

    private String comment_by_user_id;


    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    private String last_name;

    private String feed_id;

    public String getPicture ()
    {
        return picture;
    }

    public void setPicture (String picture)
    {
        this.picture = picture;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getFeed_comment_date ()
    {
        return feed_comment_date;
    }

    public void setFeed_comment_date (String feed_comment_date)
    {
        this.feed_comment_date = feed_comment_date;
    }

    public String getFeed_comment_id ()
    {
        return feed_comment_id;
    }

    public void setFeed_comment_id (String feed_comment_id)
    {
        this.feed_comment_id = feed_comment_id;
    }

    public String getFeed_comment_text ()
    {
        return feed_comment_text;
    }

    public void setFeed_comment_text (String feed_comment_text)
    {
        this.feed_comment_text = feed_comment_text;
    }

    public String getComment_by_user_id ()
    {
        return comment_by_user_id;
    }

    public void setComment_by_user_id (String comment_by_user_id)
    {
        this.comment_by_user_id = comment_by_user_id;
    }

    public String getFeed_id ()
    {
        return feed_id;
    }

    public void setFeed_id (String feed_id)
    {
        this.feed_id = feed_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [picture = "+picture+", id = "+id+", first_name = "+first_name+", feed_comment_date = "+feed_comment_date+", feed_comment_id = "+feed_comment_id+", feed_comment_text = "+feed_comment_text+", comment_by_user_id = "+comment_by_user_id+", feed_id = "+feed_id+"]";
    }
}

