package com.exceptionaire.ongraviti.model;

/**
 * Created by root on 27/3/17.
 */
public class Array_of_comment {
    private String profile_picture;
    private String device_date;
    private String status;
    private String profile_cat_id;
    private String comment_ID;
    private String comment_content;
    private String comment_by_user_id;
    private String post_id;
    private String thumb;
    private String c_name;


    public Array_of_comment() {
    }

    public Array_of_comment(String c_name, String comment_by_user_id, String comment_content, String comment_date, String comment_ID, String post_id, String profile_picture, String status, String thumb) {
        this.c_name = c_name;
        this.comment_by_user_id = comment_by_user_id;
        this.comment_content = comment_content;
        this.device_date = device_date;
        this.comment_ID = comment_ID;
        this.post_id = post_id;
        this.profile_picture = profile_picture;
        this.status = status;
        this.thumb = thumb;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getComment_date() {
        return device_date;
    }

    public void setComment_date(String device_date) {
        this.device_date = device_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment_ID() {
        return comment_ID;
    }

    public void setComment_ID(String comment_ID) {
        this.comment_ID = comment_ID;
    }

    public String getComment_content() {
        return comment_content;
    }

    public void setComment_content(String comment_content) {
        this.comment_content = comment_content;
    }

    public String getComment_by_user_id() {
        return comment_by_user_id;
    }

    public void setComment_by_user_id(String comment_by_user_id) {
        this.comment_by_user_id = comment_by_user_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    @Override
    public String toString() {
        return "ClassPojo [profile_picture = " + profile_picture + ", device_date = " + device_date + ", status = " + status + ", comment_ID = " + comment_ID + ", comment_content = " + comment_content + ", comment_by_user_id = " + comment_by_user_id + ", post_id = " + post_id + ", thumb = " + thumb + ", c_name = " + c_name + "]";
    }

    public String getProfile_cat_id() {
        return profile_cat_id;
    }

    public void setProfile_cat_id(String profile_cat_id) {
        this.profile_cat_id = profile_cat_id;
    }
}