package com.exceptionaire.ongraviti.model;

public class ResponseForSearchQuery {
    private SearchData[] result;
    private String status;

    public SearchData[] getResult() {
        return result;
    }

    public void setResult(SearchData[] result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [result = " + result + ", status = " + status + "]";
    }
}
