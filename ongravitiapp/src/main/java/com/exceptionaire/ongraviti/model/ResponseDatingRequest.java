package com.exceptionaire.ongraviti.model;

import java.util.List;

public class ResponseDatingRequest {
    private String status;

    private List<Ask_for_dating_list> ask_for_dating_list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Ask_for_dating_list> getAsk_for_dating_list() {
        return ask_for_dating_list;
    }

    public void setAsk_for_dating_list(List<Ask_for_dating_list> ask_for_dating_list) {
        this.ask_for_dating_list = ask_for_dating_list;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", ask_for_dating_list = " + ask_for_dating_list + "]";
    }
}