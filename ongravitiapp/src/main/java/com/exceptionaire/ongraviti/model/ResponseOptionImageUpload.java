package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

public class ResponseOptionImageUpload {
    private ArrayList<PicData> picData;
    private String status;
    private String msg;
    private int countProfilePicSet;

    public int getCountProfilePicSet() {
        return countProfilePicSet;
    }

    public void setCountProfilePicSet(int countProfilePicSet) {
        this.countProfilePicSet = countProfilePicSet;
    }

    public ArrayList<PicData> getPicData() {
        return picData;
    }

    public void setPicData(ArrayList<PicData> picData) {
        this.picData = picData;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ClassPojo [picData = " + picData + ", status = " + status + ", msg = " + msg + "]";
    }
}