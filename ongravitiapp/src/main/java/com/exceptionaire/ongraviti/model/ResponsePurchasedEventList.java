package com.exceptionaire.ongraviti.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePurchasedEventList {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("purchaseEventDetails")
    @Expose
    private List<PurchaseEventDetail> purchaseEventDetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<PurchaseEventDetail> getPurchaseEventDetails() {
        return purchaseEventDetails;
    }

    public void setPurchaseEventDetails(List<PurchaseEventDetail> purchaseEventDetails) {
        this.purchaseEventDetails = purchaseEventDetails;
    }
}