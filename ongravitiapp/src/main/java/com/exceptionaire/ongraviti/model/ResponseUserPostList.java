package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

public class ResponseUserPostList {
    private String lastPostId;
    private String status;
    private String totalpost;
    private String resultpostcount;
    private String nextpoststart;
    private ArrayList<Postinfo> postinfo;

    public String getLastPostId() {
        return lastPostId;
    }

    public void setLastPostId(String lastPostId) {
        this.lastPostId = lastPostId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotalpost() {
        return totalpost;
    }

    public void setTotalpost(String totalpost) {
        this.totalpost = totalpost;
    }

    public String getResultpostcount() {
        return resultpostcount;
    }

    public void setResultpostcount(String resultpostcount) {
        this.resultpostcount = resultpostcount;
    }

    public String getNextpoststart() {
        return nextpoststart;
    }

    public void setNextpoststart(String nextpoststart) {
        this.nextpoststart = nextpoststart;
    }

    public ArrayList<Postinfo> getPostinfo() {
        return postinfo;
    }

    public void setPostinfo(ArrayList<Postinfo> postinfo) {
        this.postinfo = postinfo;
    }

    @Override
    public String toString() {
        return "ClassPojo [lastPostId = " + lastPostId + ", status = " + status + ", totalpost = " + totalpost + ", resultpostcount = " + resultpostcount + ", nextpoststart = " + nextpoststart + ", postinfo = " + postinfo + "]";
    }
}