package com.exceptionaire.ongraviti.model;

import java.util.ArrayList;

public class ResponseSwitchProfileImage {
    private String default_profile_pic_thumb;

    private String default_profile_pic;

    private String status;

    private ArrayList<PicData> multi_profile_info_cat;

    private String default_profile_cat_id;

    private String msg;

    private int countMultiple_profile;

    public String getDefault_profile_pic_thumb() {
        return default_profile_pic_thumb;
    }

    public void setDefault_profile_pic_thumb(String default_profile_pic_thumb) {
        this.default_profile_pic_thumb = default_profile_pic_thumb;
    }

    public String getDefault_profile_pic() {
        return default_profile_pic;
    }

    public void setDefault_profile_pic(String default_profile_pic) {
        this.default_profile_pic = default_profile_pic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<PicData> getMulti_profile_info_cat() {
        return multi_profile_info_cat;
    }

    public void setMulti_profile_info_cat(ArrayList<PicData> multi_profile_info_cat) {
        this.multi_profile_info_cat = multi_profile_info_cat;
    }

    public String getDefault_profile_cat_id() {
        return default_profile_cat_id;
    }

    public void setDefault_profile_cat_id(String default_profile_cat_id) {
        this.default_profile_cat_id = default_profile_cat_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCountMultiple_profile() {
        return countMultiple_profile;
    }

    public void setCountMultiple_profile(int countMultiple_profile) {
        this.countMultiple_profile = countMultiple_profile;
    }

    @Override
    public String toString() {
        return "ClassPojo [default_profile_pic_thumb = " + default_profile_pic_thumb + ", default_profile_pic = " + default_profile_pic + ", status = " + status + ", multi_profile_info_cat = " + multi_profile_info_cat + ", default_profile_cat_id = " + default_profile_cat_id + ", msg = " + msg + ", countMultiple_profile = " + countMultiple_profile + "]";
    }
}