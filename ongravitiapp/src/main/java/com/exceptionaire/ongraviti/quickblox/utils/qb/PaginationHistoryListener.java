package com.exceptionaire.ongraviti.quickblox.utils.qb;

public interface PaginationHistoryListener {
    void downloadMore();
}
