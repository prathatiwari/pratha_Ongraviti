package com.exceptionaire.ongraviti.quickblox.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.exceptionaire.ongraviti.R;
import com.exceptionaire.ongraviti.activities.AskForBusiness;
import com.exceptionaire.ongraviti.activities.AskForDating;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.LoginActivity;
import com.exceptionaire.ongraviti.activities.MyFavoritesActivity;
import com.exceptionaire.ongraviti.activities.NotificationsActivity;
import com.exceptionaire.ongraviti.activities.PeopleNearByMeActivity;
import com.exceptionaire.ongraviti.activities.ServicesActivity;
import com.exceptionaire.ongraviti.activities.Setting;
import com.exceptionaire.ongraviti.activities.adapters.DrawerItemCustomAdapter;
import com.exceptionaire.ongraviti.activities.base.ArcMenu;
import com.exceptionaire.ongraviti.activities.base.CommonDialogs;
import com.exceptionaire.ongraviti.activities.base.CustomProgressDialog;
import com.exceptionaire.ongraviti.activities.base.RoundedImageView;
import com.exceptionaire.ongraviti.activities.chats.ChatsMainActivity;
import com.exceptionaire.ongraviti.activities.myorbit.ContactsFromPhoneActivity;
import com.exceptionaire.ongraviti.activities.myorbit.ImportGmailContactsActivity;
import com.exceptionaire.ongraviti.activities.myorbit.MyOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.core.OnGravitiApp;
import com.exceptionaire.ongraviti.dialog.NormalDialog;
import com.exceptionaire.ongraviti.dialog.animation.BaseAnimatorSet;
import com.exceptionaire.ongraviti.dialog.animation.BounceEnter.BounceTopEnter;
import com.exceptionaire.ongraviti.dialog.animation.BounceEnter.SlideBottomExit;
import com.exceptionaire.ongraviti.dialog.listener.OnBtnClickL;
import com.exceptionaire.ongraviti.model.DrawerModel;
import com.exceptionaire.ongraviti.model.ResponseSetDefaultProfileCategory;
import com.exceptionaire.ongraviti.model.UserProfileCategory;
import com.exceptionaire.ongraviti.quickblox.QBData;
import com.exceptionaire.ongraviti.quickblox.core.gcm.GooglePlayServicesHelper;
import com.exceptionaire.ongraviti.quickblox.core.ui.dialog.ProgressDialogFragment;
import com.exceptionaire.ongraviti.quickblox.core.utils.SharedPrefsHelper;
import com.exceptionaire.ongraviti.quickblox.core.utils.Toaster;
import com.exceptionaire.ongraviti.quickblox.utils.QBResRequestExecutor;
import com.exceptionaire.ongraviti.quickblox.utils.chat.ChatHelper;
import com.exceptionaire.ongraviti.quickblox.utils.qb.QbAuthUtils;
import com.exceptionaire.ongraviti.quickblox.utils.qb.QbSessionStateCallback;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.exceptionaire.ongraviti.utilities.LogUtil;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.exceptionaire.ongraviti.core.AppDetails.getActivity;

public abstract class BaseActivity extends CoreBaseActivity implements QbSessionStateCallback, Response.Listener, Response.ErrorListener {
    public static final String TAG = BaseActivity.class.getSimpleName();
    private static final Handler mainThreadHandler = new Handler(Looper.getMainLooper());
    protected boolean isAppSessionActive;

    protected SharedPrefsHelper sharedPrefsHelper;
    protected GooglePlayServicesHelper googlePlayServicesHelper;
    protected QBResRequestExecutor requestExecutor;


    /*OnGraviti BaseDrawerActivity Code start*/
    private String[] mNavigationDrawerItemTitles;
    public DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private CharSequence mTitle;
    ActionBarDrawerToggle mDrawerToggle;
    public FrameLayout frameLayout;
    Toolbar toolbar;
    int pos;
    private TextView username;
    ImageView imgSearch, imgDating, imgBusiness, imgNotification;
    //    private RoundedImageView profile_picture;
    private RoundedImageView profile_picture;
    static Boolean flagDrawerOpened = false;

    //volly request variables
    private int WESERVICE_FLAG;
    private RequestQueue requestQueue1;
    private ProgressDialog progressDialog;
    public static ArrayList<UserProfileCategory> list_profile_category;
    String status, message;
    boolean doubleBackToExitPressedOnce = false;
    private static int HeaderIconSelection = 0; // 1=Dating, 2=Business , 3= Notification
    private ImageView imgSocial, imgDating_icon, imgBusiness_icon;
    TextView text_view_socials, text_view_datings, text_view_businesss;
    public BaseAnimatorSet mBasIn;
    public BaseAnimatorSet mBasOut;

    public ArcMenu arcMenu;


    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LogUtil.writeDebugLog(TAG, "onCreate", "1");
        boolean wasAppRestored = savedInstanceState != null;
        boolean isQbSessionActive = QbAuthUtils.isSessionActive();
        final boolean needToRestoreSession = wasAppRestored || !isQbSessionActive;
        Log.v(TAG, "wasAppRestored = " + wasAppRestored);
        Log.v(TAG, "isQbSessionActive = " + isQbSessionActive);

        // Triggering callback via Handler#post() method
        // to let child's code in onCreate() to execute first
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if (needToRestoreSession) {
                    recreateChatSession();
                    isAppSessionActive = false;
                } else {
                    onSessionCreated(true);
                    isAppSessionActive = true;
                }
            }
        });

        requestExecutor = OnGravitiApp.getInstance().getQbResRequestExecutor();
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        googlePlayServicesHelper = new GooglePlayServicesHelper();


        /*OnGraviti BaseDrawerActivity code start*/
        try {
//        mNavigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (ListView) findViewById(R.id.left_drawer);
            frameLayout = (FrameLayout) findViewById(R.id.content_frame);
            imgSearch = (ImageView) findViewById(R.id.imgSearch);
            imgDating = (ImageView) findViewById(R.id.imgDating);
            imgBusiness = (ImageView) findViewById(R.id.imgBusiness);
            imgNotification = (ImageView) findViewById(R.id.imgNotification);
            arcMenu = (ArcMenu) findViewById(R.id.arcMenu);
            arcMenu.setRadius(getResources().getDimension(R.dimen.radius));

            imgSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(BaseActivity.this, "Search", Toast.LENGTH_SHORT).show();
                }
            });

            Log.d(TAG, "## onCreate HeaderIconSelection : " + HeaderIconSelection);
            if (HeaderIconSelection == 0) {
                imgBusiness.setImageResource(R.drawable.ic_business);
                imgNotification.setImageResource(R.drawable.ic_notification);
                imgDating.setImageResource(R.drawable.ic_dating);

            } else if (HeaderIconSelection == 1) {
                imgDating.setImageResource(R.drawable.ic_dating);
                imgBusiness.setImageResource(R.drawable.ic_business);
                imgNotification.setImageResource(R.drawable.ic_notification);

            } else if (HeaderIconSelection == 2) {
                imgBusiness.setImageResource(R.drawable.ic_business);
                imgNotification.setImageResource(R.drawable.ic_notification);
                imgDating.setImageResource(R.drawable.ic_dating);

            } else if (HeaderIconSelection == 3) {
                imgNotification.setImageResource(R.drawable.ic_notification);
                imgBusiness.setImageResource(R.drawable.ic_business);
                imgDating.setImageResource(R.drawable.ic_dating);
            }

            imgDating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HeaderIconSelection = 1;

                    Intent intentDating = new Intent(BaseActivity.this, AskForDating.class);
                    startActivity(intentDating);
                    //  finish();
                }
            });
            imgBusiness.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HeaderIconSelection = 2;

                    Intent intentBusiness = new Intent(BaseActivity.this, AskForBusiness.class);
                    startActivity(intentBusiness);
                    // finish();
                }
            });
            imgNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HeaderIconSelection = 3;

                    startActivity(new Intent(BaseActivity.this, NotificationsActivity.class));
                    finish();
                }
            });

           /* imgSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(BaseActivity.this, "Search", Toast.LENGTH_SHORT).show();
                }
            });*/

            LayoutInflater inflater = getLayoutInflater();
            View listHeaderView = inflater.inflate(R.layout.drawer_header, null, false);
            View listFotterView = inflater.inflate(R.layout.drawer_bottom, null, false);

            username = (TextView) listHeaderView.findViewById(R.id.drawer_header_username);
            profile_picture = (RoundedImageView) listHeaderView.findViewById(R.id.drawer_header_profile_picture); // set profile picture
            // check if default category set, set default category picture
            /*if(AppSettings.getProfilePicturePath()!=null)
                profile_picture.setImageURI(android.net.Uri.parse(AppSettings.getProfilePicturePath()));*/
            //setProfiePicturePath
            if (AppSettings.getProfilePictureName() != null)
                username.setText(AppSettings.getProfilePictureName());
            Log.d("Baseadpt", "## AppSettings.getProfilePictureName()" + AppSettings.getProfilePictureName());
            Log.d("Baseadpt", "## AppSettings.getProfileDefaultCategory() : " + String.valueOf(AppSettings.getProfileDefaultCategory()));

            mDrawerList.addHeaderView(listHeaderView);
            mDrawerList.addFooterView(listFotterView);

            imgSocial = (ImageView) listHeaderView.findViewById(R.id.imgSocial_);
            imgDating_icon = (ImageView) listHeaderView.findViewById(R.id.imgDating_);
            imgBusiness_icon = (ImageView) listHeaderView.findViewById(R.id.imgBusiness_);
            text_view_socials = (TextView) listHeaderView.findViewById(R.id.text_view_socials);
            text_view_businesss = (TextView) listHeaderView.findViewById(R.id.text_view_businesss);
            text_view_datings = (TextView) listHeaderView.findViewById(R.id.text_view_datings);
            //find which default profile is active now, and change its bg
            if (String.valueOf(AppSettings.getProfileDefaultCategory()).equals("1")) {
                Log.d("Baseadpt", "## if 1 ");
                imgSocial.setImageResource(R.drawable.ic_social_checked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
//                text_view_socials.setTextColor(R.color.selected);
//                text_view_datings.setTextColor(R.color.white);
//                text_view_businesss.setTextColor(R.color.white);
            } else if (String.valueOf(AppSettings.getProfileDefaultCategory()).equals("2")) {
                Log.d("Baseadpt", "## if 2 ");
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating_checked);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
//                text_view_socials.setTextColor(R.color.white);
//                text_view_datings.setTextColor(R.color.selected);
//                text_view_businesss.setTextColor(R.color.white);
            } else if (String.valueOf(AppSettings.getProfileDefaultCategory()).equals("3")) {
                Log.d("Baseadpt", "## if 3 ");
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business_checked);
//                text_view_socials.setTextColor(R.color.white);
//                text_view_datings.setTextColor(R.color.white);
//                text_view_businesss.setTextColor(R.color.selected);
            }

           /* username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(BaseActivity.this, EditProfileActivity.class));
                }
            });
*/
            DrawerModel[] drawerItem = new DrawerModel[8];
            drawerItem[0] = new DrawerModel(R.drawable.home_selector, getResources().getString(R.string.home), R.drawable.no_comming_soon);
            drawerItem[1] = new DrawerModel(R.drawable.peoplenearby_selector, getResources().getString(R.string.people_nearby), R.drawable.no_comming_soon);
            drawerItem[2] = new DrawerModel(R.drawable.myorbit_selector, getResources().getString(R.string.myorbit), R.drawable.no_comming_soon);
//            drawerItem[3] = new DrawerModel(R.drawable.favorite_selector, getResources().getString(R.string.txtfavorites), R.drawable.no_comming_soon);
            drawerItem[3] = new DrawerModel(R.drawable.speak_selector, getResources().getString(R.string.speak_out), R.drawable.comming_soon);
            drawerItem[4] = new DrawerModel(R.drawable.chat_selector, getResources().getString(R.string.chat), R.drawable.no_comming_soon);
            drawerItem[5] = new DrawerModel(R.drawable.speed_dating_selector, getResources().getString(R.string.speed_dating), R.drawable.comming_soon);
            drawerItem[6] = new DrawerModel(R.drawable.services_selector, getResources().getString(R.string.service), R.drawable.comming_soon);
            drawerItem[7] = new DrawerModel(R.drawable.ic_settings, getResources().getString(R.string.settings), R.drawable.no_comming_soon);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(true);
            DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_row, drawerItem);
            mDrawerList.setAdapter(adapter);
            mDrawerList.setOnItemClickListener(new BaseActivity.DrawerItemClickListener());
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerLayout.setDrawerListener(mDrawerToggle);
//        mTitle = mNavigationDrawerItemTitles[12];
            System.out.println("Title :" + mTitle);
            setupDrawerToggle();

//            showProgressBar();
//            getUserProfileData();
        } catch (Exception ee) {
            Log.e(TAG, "## e : " + ee.getMessage());
            ee.printStackTrace();
        }


        setDefaultProfileLocally();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putInt("dummy_value", 0);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    protected abstract View getSnackbarAnchorView();

    private void recreateChatSession() {
        Log.d(TAG, "Need to recreate chat session");
        LogUtil.writeDebugLog(TAG, "recreateChatSession", "Need to recreate chat session");
        QBUser user = QBData.curQBUser;
        if (user == null) {
            LogUtil.writeDebugLog(TAG, "recreateChatSession", "User is null, can't restore session");
            Toaster.longToast("User is null, can't restore session");
            throw new RuntimeException("User is null, can't restore session");
        }

        reloginToChat(user);
    }

    private void reloginToChat(final QBUser user) {
        ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.v(TAG, "Chat login onSuccess()");
                LogUtil.writeDebugLog(TAG, "reloginToChat", "onSuccess");
                isAppSessionActive = true;
                onSessionCreated(true);

                ProgressDialogFragment.hide(getSupportFragmentManager());
            }

            @Override
            public void onError(QBResponseException e) {
                isAppSessionActive = false;
                LogUtil.writeDebugLog(TAG, "reloginToChat", "onError");
                ProgressDialogFragment.hide(getSupportFragmentManager());
                Log.w(TAG, "Chat login onError(): " + e);
                onSessionCreated(false);
            }
        });
    }

    protected void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            // Disable the back button
            DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    return keyCode == KeyEvent.KEYCODE_BACK;
                }
            };
            progressDialog.setOnKeyListener(keyListener);
        }
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    protected void hideProgressDialog() {
        LogUtil.writeDebugLog(TAG, "hideProgressDialog", "1");
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @SuppressLint("ResourceAsColor")
    private void setDefaultProfileLocally() {
        try {
            if (AppSettings.getProfileDefaultCategory().equals(AppConstants.PROFILE_CATEGORY_ONE_SOCIAL)) {
                if (AppSettings.getProfileCatPicPathOne() != null) {
                    AppSettings.setProfiePicturePath(AppSettings.getProfileCatPicPathOne());
                    Log.d(TAG, "## AppSettings.getProfileCatPicPathOne() :" + AppSettings.getProfileCatPicPathOne());
//                profile_picture.setImageUrl(AppSettings.getProfilePicturePath(), imageLoader);
                    Picasso.with(this)
                            .load(AppSettings.getProfileCatPicPathOne())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(profile_picture);
                } else {
                    profile_picture.setImageResource(R.drawable.profile);
                }
                //set selected profile to drawer
                imgSocial.setImageResource(R.drawable.ic_social_checked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
//                text_view_socials.setTextColor(R.color.selected);
//                text_view_datings.setTextColor(R.color.white);
//                text_view_businesss.setTextColor(R.color.white);
            } else if (AppSettings.getProfileDefaultCategory().equals(AppConstants.PROFILE_CATEGORY_TWO_DATING)) {
                if (AppSettings.getProfileCatPicPathTWO() != null) {
                    AppSettings.setProfiePicturePath(AppSettings.getProfileCatPicPathTWO());
                    Log.d(TAG, "## AppSettings.getProfileCatPicPathTWO() :" + AppSettings.getProfileCatPicPathTWO());
//                profile_picture.setImageUrl(AppSettings.getProfilePicturePath(), imageLoader);

                    Picasso.with(this)
                            .load(AppSettings.getProfileCatPicPathTWO())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(profile_picture);
                } else {
                    profile_picture.setImageResource(R.drawable.profile);
                }
                //set selected profile to drawer
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating_checked);
                imgBusiness_icon.setImageResource(R.drawable.ic_business);
//                text_view_socials.setTextColor(R.color.white);
//                text_view_datings.setTextColor(R.color.selected);
//                text_view_businesss.setTextColor(R.color.white);
            } else if (AppSettings.getProfileDefaultCategory().equals(AppConstants.PROFILE_CATEGORY_THREE_BUSINESS)) {
                if (AppSettings.getProfileCatPicPathThree() != null) {
                    AppSettings.setProfiePicturePath(AppSettings.getProfileCatPicPathThree());
                    Log.d(TAG, "## AppSettings.getProfileCatPicPathThree() :" + AppSettings.getProfileCatPicPathThree());
//                profile_picture.setImageUrl(AppSettings.getProfilePicturePath(), imageLoader);

                    Picasso.with(this)
                            .load(AppSettings.getProfileCatPicPathThree())
                            .placeholder(R.drawable.profile) //this is optional the image to display while the url image is downloading
                            .error(R.drawable.profile)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                            .into(profile_picture);
                } else {
                    profile_picture.setImageResource(R.drawable.profile);
                }
                //set selected profile to drawer
                imgSocial.setImageResource(R.drawable.ic_social_unchecked);
                imgDating_icon.setImageResource(R.drawable.ic_dating);
                imgBusiness_icon.setImageResource(R.drawable.ic_business_checked);
//                text_view_socials.setTextColor(R.color.white);
//                text_view_datings.setTextColor(R.color.white);
//                text_view_businesss.setTextColor(R.color.selected);
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }


    /*OnGraviti BaseDrawerActivity code start*/
    public void onClickSocialCategory(View view) {
        try {
            AppSettings.setProfileDefaultCategory("1");
            CallSetDefaultProfileApi();
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void onClickDatingCategory(View view) {
        try {
            AppSettings.setProfileDefaultCategory("2");
            CallSetDefaultProfileApi();
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void onClickBusinessCategory(View view) {
        try {
            AppSettings.setProfileDefaultCategory("3");
            CallSetDefaultProfileApi();
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    public void CallSetDefaultProfileApi() {
        showProgressBar();
        Retrofit retrofit = ApiClient.getClient();
        final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<ResponseSetDefaultProfileCategory> accessTokenCall = requestInterface.setUserDefaultProfile(AppSettings.getLoginUserId(), String.valueOf(AppSettings.getProfileDefaultCategory()));
        accessTokenCall.enqueue(new Callback<ResponseSetDefaultProfileCategory>() {
            @Override
            public void onResponse(Call<ResponseSetDefaultProfileCategory> call, retrofit2.Response<ResponseSetDefaultProfileCategory> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                if (response.body() != null && response.body().getStatus().equals(AppConstants.RESPONSE_SUCCESS)) {
                    Log.d(TAG, "## response : " + response.body().toString() + " ");


                    if (response.body().getProfile_cat_id() != null && !response.body().getProfile_cat_id().isEmpty() && !response.body().getProfile_cat_id().equals("")) {
                        AppSettings.setProfileDefaultCategory(response.body().getProfile_cat_id());
                        if (response.body().getProfile_cat_id().equals(AppConstants.PROFILE_CATEGORY_ONE_SOCIAL)) {

                            AppSettings.setProfileCatPicPathOne(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getProfile_picture());

                        } else if (response.body().getProfile_cat_id().equals(AppConstants.PROFILE_CATEGORY_TWO_DATING)) {

                            AppSettings.setProfileCatPicPathTwo(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getProfile_picture());

                        } else if (response.body().getProfile_cat_id().equals(AppConstants.PROFILE_CATEGORY_THREE_BUSINESS)) {

                            AppSettings.setProfileCatPicPathThree(AppConstants.PROFILE_PICTURE_PATH + File.separator + response.body().getProfile_picture());
                        }
                    }
                    setDefaultProfileLocally();

                    CommonDialogs.dialog_with_one_btn_without_title(BaseActivity.this, message);

                } else {
                    CommonDialogs.dialog_with_one_btn_without_title(BaseActivity.this, message);
                }
            }

            @Override
            public void onFailure(Call<ResponseSetDefaultProfileCategory> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(getActivity(), "Something went wrong, please try after some time.", Toast.LENGTH_SHORT).show();
                Log.e("Error: ", t.toString());
            }
        });
    }

    private void showProgressBar() {
        progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.show();
    }


    public static boolean isDrawerOpen() {
        return flagDrawerOpened;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mDrawerLayout.closeDrawers();

            /*if (AppSettings.getProfilePicturePath() != "") {
                profile_picture.setImageUrl(AppSettings.getProfilePicturePath(), imageLoader);
            }*/
        } catch (Exception e) {
            Log.e(TAG, "## onClick error : " + e.getMessage());
            e.printStackTrace();
        }
    }
/*

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.item_search:
                Toast.makeText(this, "Refresh selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            // action with ID action_settings was selected
            case R.id.item_dating:
                Intent intentDating = new Intent(BaseActivity.this, AskForDating.class);
                startActivity(intentDating);
                break;
            case R.id.item_business:
                Intent intentBusiness = new Intent(BaseActivity.this, AskForBusiness.class);
                startActivity(intentBusiness);
                break;
            case R.id.item_notification:
                startActivity(new Intent(this, NotificationsActivity.class));
                finish();
                break;
        }

        return true;


//        return super.onOptionsItemSelected(item);
    }
*/

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d(TAG, "## onErrorResponse : " + error.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            error.printStackTrace();
            String errorMessage = getString(R.string.error_connection_generic);
            if (error instanceof NoConnectionError)
                errorMessage = getString(R.string.error_no_connection);
            else if (error instanceof AuthFailureError)
                errorMessage = getString(R.string.error_authentication);
            else if (error instanceof NetworkError)
                errorMessage = getString(R.string.error_network_error);
            else if (error instanceof ParseError)
                errorMessage = getString(R.string.error_parse_error);
            else if (error instanceof ServerError)
                errorMessage = getString(R.string.error_server_error);
            else if (error instanceof TimeoutError)
                errorMessage = getString(R.string.error_connection_timeout);
            Toast.makeText(BaseActivity.this, errorMessage, Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object response) {
        Log.d(TAG, "## onResponse : " + response.toString());
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            JSONObject jObject;
            if (WESERVICE_FLAG == 2) {  // get profile data
                try {
//                    Log.d(TAG, "## response : " + response + " ");
                    jObject = new JSONObject(String.valueOf(response));

                    status = jObject.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        new CommonDialogs().showOKFinishDialog(BaseActivity.this, getResources().getString(R.string.ongravity), message).show();

                        /*userAllProfileData = new UserAllProfileData(jObject);

                        AppSettings.setProfiePictureName(userAllProfileData.getName());

                        //set name in edittext basedrawer
                        username.setText(userAllProfileData.getName());

                        if (userAllProfileData.getList_profileData().size() > 0) {
                            //set profile pic according to default category, and profile name in appSettings
                            Log.d(TAG, "## userAllProfileData.getUser_id() :: " + userAllProfileData.getUser_id());
                            if (userAllProfileData.getProfile_cat_id().equalsIgnoreCase("1")) {
                                AppSettings.setProfileDefaultCategory("1");
                                AppSettings.setProfiePicturePath(userAllProfileData.getList_profileData().get(0).getProfile_picture());
                            } else if (userAllProfileData.getProfile_cat_id().equalsIgnoreCase("2")) {
                                AppSettings.setProfileDefaultCategory("2");
                                AppSettings.setProfiePicturePath(userAllProfileData.getList_profileData().get(1).getProfile_picture());
                            } else if (userAllProfileData.getProfile_cat_id().equalsIgnoreCase("3")) {
                                AppSettings.setProfileDefaultCategory("3");
                                AppSettings.setProfiePicturePath(userAllProfileData.getList_profileData().get(2).getProfile_picture());
                            }
                        }

                        profile_picture.setImageUrl(AppSettings.getProfilePicturePath(), imageLoader);
*/
                        Log.d(TAG, "## AppSettings.getProfileDefaultCategory() : " + String.valueOf(AppSettings.getProfileDefaultCategory()));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "## e : " + e.getMessage());
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
//            mTitle=mNavigationDrawerItemTitles[position];
        }

    }

    private void selectItem(int position) {
        try {
            Log.d(TAG, "## position : " + position);
            Fragment fragment = null;
            pos = position;
            switch (position) {
                case 1:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseActivity.this, HomeUserPostsActivity.class));
                    overridePendingTransition(R.anim.slide_in_right,
                            R.anim.slide_out_left);
                    finish();
                    break;
                case 2:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseActivity.this, PeopleNearByMeActivity.class));
                    overridePendingTransition(R.anim.slide_in_right,
                            R.anim.slide_out_left);
                    finish();
                    break;
                case 3:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseActivity.this, MyOrbitActivity.class));
                    overridePendingTransition(R.anim.slide_in_right,
                            R.anim.slide_out_left);
                    finish();
                    break;
//                case 4:
//                    HeaderIconSelection = 0;
//                    startActivity(new Intent(BaseActivity.this, MyFavoritesActivity.class));
//                    overridePendingTransition(R.anim.slide_in_right,
//                            R.anim.slide_out_left);
//                    finish();
//                    break;
                case 4:
                    HeaderIconSelection = 0;
//                    startActivity(new Intent(BaseActivity.this, SpeakOutYourMindActivity.class));
//                    overridePendingTransition(R.anim.slide_in_right,
//                            R.anim.slide_out_left);
//                    finish();
                    break;
                case 5:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseActivity.this, ChatsMainActivity.class));
                    finish();
                    break;

                case 7:
                    HeaderIconSelection = 0;
//                    startActivity(new Intent(BaseActivity.this, ServicesActivity.class));
//                    finish();
                    break;
                case 6:
                    HeaderIconSelection = 0;
//                    startActivity(new Intent(BaseActivity.this, SpeedDatingActivity.class));
//                    finish();
                    break;
                case 8:
                    HeaderIconSelection = 0;
                    startActivity(new Intent(BaseActivity.this, Setting.class));
                    overridePendingTransition(R.anim.slide_in_right,
                            R.anim.slide_out_left);
                    finish();
                    break;
                case 11:
                    HeaderIconSelection = 0;
                    final Dialog dialogs1 = CommonDialogs.showContactDialog(BaseActivity.this, "Select Contact Option");
                    TextView gmailContacts = (TextView) dialogs1.findViewById(R.id.dialogContactOptionGmailContact);
                    gmailContacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent in = new Intent(BaseActivity.this, ImportGmailContactsActivity.class);
                            startActivity(in);

                        }
                    });
                    TextView facebookContacts = (TextView) dialogs1.findViewById(R.id.dialogContactOptionFbContact);
                    facebookContacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Toast.makeText(BaseActivity.this, "No Permission Access Denied", Toast.LENGTH_LONG).show();

                        }
                    });
                    TextView linkedInContacts = (TextView) dialogs1.findViewById(R.id.dialogContactOptionLinkedInContact);
                    linkedInContacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(BaseActivity.this, "No Permission Access Denied", Toast.LENGTH_LONG).show();
                        }
                    });
                    TextView PhoneContacts = (TextView) dialogs1.findViewById(R.id.dialogContactOptionContactFromPhone);
                    PhoneContacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(BaseActivity.this, ContactsFromPhoneActivity.class));
                            dialogs1.dismiss();
                        }
                    });

                    break;
                case 10:
                    Log.d(TAG, "## case 10");
                    HeaderIconSelection = 0;
                    mBasIn = new BounceTopEnter();
                    mBasOut = new SlideBottomExit();

                    mDrawerLayout.closeDrawers();
                    final NormalDialog dialog = new NormalDialog(BaseActivity.this);
                    dialog.isTitleShow(false);

                    dialog.content("Are you sure you want to logout")
                            .showAnim(mBasIn)
                            .dismissAnim(mBasOut)
                            .contentGravity(Gravity.CENTER)
                            .show();

                    dialog.setOnBtnClickL(
                            new OnBtnClickL() {
                                @Override
                                public void onBtnClick() {
                                    Log.d("dialog", "## Left");
                                    dialog.dismiss();
                                    AppSettings.setLogin(false);
                                    AppSettings.setProfiePicturePath("");
                                    AppSettings.setUserProfileName("");
                                    AppSettings.setProfileDefaultCategory("");
                                    AppSettings.setProfiePictureName("");
                                    AppSettings.setProfileStepStatus1(0);
                                    AppSettings.setProfileStepStatus2(0);
                                    AppSettings.setProfileStepStatus3(0);
                                    AppSettings.setProfileStepStatus4(0);
                                    AppSettings.setProfileStepStatus5(0);
                                    AppSettings.setProfileCatPicPathFB("");
                                    Intent loginIntent = new Intent(BaseActivity.this, LoginActivity.class);
                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(loginIntent);
                                    finish();
                                }
                            },
                            new OnBtnClickL() {
                                @Override
                                public void onBtnClick() {
                                    Log.d("dialog", "## Right");
                                    dialog.dismiss();

                                }
                            });

               /* final Dialog dialog1 = CommonDialogs.showAlertDialog(BaseActivity.this, "Logout", "Are you sure you want to logout");
                Button yes = (Button) dialog1.findViewById(R.id.button_ok);
                yes.setText("Yes");
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();
                        AppSettings.setLogin(false);
                        AppSettings.setProfiePicturePath("");
                        AppSettings.setUserProfileName("");
                        AppSettings.setProfileDefaultCategory("");
                        AppSettings.setProfiePictureName("");
                        EditProfileActivity.list_profile_category = null;
                        Intent loginIntent = new Intent(BaseActivity.this, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(loginIntent);
                        finish();
                    }
                });
                Button cancle = (Button) dialog1.findViewById(R.id.button_cancel);
                cancle.setText("Cancel");
                cancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();
                    }
                });
                dialog1.show();*/
//                startActivity(new Intent(BaseActivity.this, Setting.class));
                    break;
                case 14:
                    HeaderIconSelection = 0;
                    Log.d(TAG, "## case 14");
                    break;

                default:
                    break;
            }

            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                mDrawerList.setItemChecked(position, true);
                mDrawerList.setSelection(position);
//            setTitle(mNavigationDrawerItemTitles[position]);
                System.out.println("Title :" + mNavigationDrawerItemTitles[position]);
                mDrawerLayout.closeDrawer(mDrawerList);

            } else {
                Log.e("MainActivity", "Error in creating fragment");
            }
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
//        getSupportActionBar().setTitle(mTitle);
    }

    void setupDrawerToggle() {
        try {
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_open) {

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    flagDrawerOpened = true;
                    Log.d(TAG, "## onDrawerOpened");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                    if (imm.isAcceptingText()) {
                        Log.d(TAG, "## if imm.isAcceptingText()");
                        hideSoftKeyboard(BaseActivity.this);
                    }
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    flagDrawerOpened = false;
                }
            };

        /*getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);*/

            ;
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            toolbar.setNavigationIcon(R.drawable.menu);
//        toolbar.setLogo(R.mipmap.my_world);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            });
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
//        super.onBackPressed();
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawers();
            } else {
//            finish();
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Press once again to exit the app", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        } catch (Exception ee) {
            Log.e(TAG, "## ee : " + ee.getMessage());
            ee.printStackTrace();
        }
    }
}