package com.exceptionaire.ongraviti.quickblox.utils.qb;

public interface QbSessionStateCallback {

    void onSessionCreated(boolean success);
}
