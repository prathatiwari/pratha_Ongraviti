package com.exceptionaire.ongraviti.fcm;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.exceptionaire.ongraviti.activities.AcceptDatingRequest;
import com.exceptionaire.ongraviti.activities.FavoriteUserInfoActivity;
import com.exceptionaire.ongraviti.activities.HomeUserPostsActivity;
import com.exceptionaire.ongraviti.activities.chats.ChatsMainActivity;
import com.exceptionaire.ongraviti.activities.myorbit.MyOrbitActivity;
import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService implements AppConstants {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage == null)
            return;

        if (AppSettings.isLogin()) {

            Log.e(TAG, "From: " + remoteMessage.getFrom());
            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {
                Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
                handleNotification(remoteMessage.getNotification().getBody());
            }

            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

                try {
                    JSONObject json = new JSONObject(remoteMessage.getData());
                    handleDataMessage(json);
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {

            String action = json.getString("action");
            String message = json.getString("message");
            final String action_by = json.getString("action_by");
            String action_by_profile_pic = "";
            if (json.has("action_by_profile_pic"))
                action_by_profile_pic = json.getString("action_by_profile_pic");
            String action_time = json.getString("action_time");

            Log.e(TAG, "action: " + action);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "action_by: " + action_by);
            Log.e(TAG, "action_by_profile_pic: " + action_by_profile_pic);
            Log.e(TAG, "action_time: " + action_time);


//            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
//
//                Log.e(LOG_TAG, " !isAppIsInBackground ------------------ > true");
//                // app is in foreground, broadcast the push message
//                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
//                pushNotification.putExtra("message", message);
//                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//
//                // play notification sound
//                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//                if (action.equals(NOTIFICATION_ACTION_FAVOURITE)) {
//                    notificationUtils.playNotificationSound();
//                    Dialog dialog = new CommonDialogs().showOKFinishDialog(getApplicationContext(), "Added as favourite", message);
//                    ((Button) dialog.findViewById(R.id.button_ok)).setText("Show Profile");
//                    dialog.findViewById(R.id.button_ok).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent resultIntent = new Intent(getApplicationContext(), FavoriteUserInfoActivity.class);
//                            resultIntent.putExtra("ActivityName", "OrbitProfile");
//                            resultIntent.putExtra("OrbitUserID", "" + action_by);
//                            getApplication().startActivity(resultIntent);
//                        }
//                    });
//
//                    dialog.show();
//
//                }
//
//            } else {
            Log.e(TAG, " !isAppIsInBackground ------------------ > false");

            // app is in background, show the notification in notification tray
            Intent resultIntent = new Intent(getApplicationContext(), HomeUserPostsActivity.class);
            resultIntent.putExtra("message", message);

            // check for image attachment
            if (TextUtils.isEmpty(action_by_profile_pic) || action_by_profile_pic.equals("")) {
                Log.e(TAG, " !isAppIsInBackground ------------------ > False--  Small");
                if (action.equals(NOTIFICATION_ACTION_FAVOURITE)) {
                    resultIntent = new Intent(getApplicationContext(), FavoriteUserInfoActivity.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessage(getApplicationContext(), "Added as favourite", message, action_time, resultIntent);
                } else if (action.equals(NOTIFICATION_ACTION_ORBIT_REQUEST)) {
                    resultIntent = new Intent(getApplicationContext(), MyOrbitActivity.class);
                    resultIntent.putExtra("page_index", "1");
                    showNotificationMessage(getApplicationContext(), "Orbit Request", message, action_time, resultIntent);
                } else if (action.equals(NOTIFICATION_ACTION_ORBIT_REQUEST_ACCEPTED)) {
                    resultIntent = new Intent(getApplicationContext(), FavoriteUserInfoActivity.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    Log.d("OrbitUserIDOrbitUserID",action_by);
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessage(getApplicationContext(), "Request Accepted", message, action_time, resultIntent);
                } else if (action.equals(NOTIFICATION_ACTION_ASK_DATING)) {
                    resultIntent = new Intent(getApplicationContext(), AcceptDatingRequest.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessage(getApplicationContext(), "Dating Invitation", message, action_time, resultIntent);
                } else if (action.equals(NOTIFICATION_ACTION_DATING_REQUEST_ACCEPTED)) {
                    resultIntent = new Intent(getApplicationContext(), ChatsMainActivity.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessageWithBigImage(getApplicationContext(), "Dating Invitation", message, action_time, resultIntent, AppConstants.PROFILE_PICTURE_PATH + java.io.File.separator + action_by_profile_pic);
                } else if (action.equals(NOTIFICATION_ACTION_DATING_REQUEST)) {
                    resultIntent = new Intent(getApplicationContext(), AcceptDatingRequest.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessage(getApplicationContext(), "Dating Invitation", message, action_time, resultIntent);
                } else if (action.equals(NOTIFICATION_ACTION_DATING_REQUEST_REJECTED)) {
                    resultIntent = new Intent(getApplicationContext(), AcceptDatingRequest.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessage(getApplicationContext(), "Dating Invitation", message, action_time, resultIntent);
                }
            } else {
                // image is present, show notification with image
                Log.e(TAG, " !isAppIsInBackground ------------------ > False--  Big");
                if (action.equals(NOTIFICATION_ACTION_FAVOURITE)) {
                    resultIntent = new Intent(getApplicationContext(), FavoriteUserInfoActivity.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessageWithBigImage(getApplicationContext(), "Added as favourite", message, action_time, resultIntent, AppConstants.PROFILE_PICTURE_PATH + java.io.File.separator + action_by_profile_pic);
                } else if (action.equals(NOTIFICATION_ACTION_ORBIT_REQUEST)) {
                    resultIntent = new Intent(getApplicationContext(), MyOrbitActivity.class);
                    resultIntent.putExtra("page_index", "1");
                    showNotificationMessageWithBigImage(getApplicationContext(), "Orbit Request", message, action_time, resultIntent, AppConstants.PROFILE_PICTURE_PATH + java.io.File.separator + action_by_profile_pic);
                } else if (action.equals(NOTIFICATION_ACTION_ORBIT_REQUEST_ACCEPTED)) {
                    resultIntent = new Intent(getApplicationContext(), FavoriteUserInfoActivity.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    Log.d("OrbitUserIDOrbitUserID",action_by);
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessageWithBigImage(getApplicationContext(), "Request Accepted", message, action_time, resultIntent, AppConstants.PROFILE_PICTURE_PATH + java.io.File.separator + action_by_profile_pic);
                } else if (action.equals(NOTIFICATION_ACTION_ASK_DATING)) {
                    resultIntent = new Intent(getApplicationContext(), AcceptDatingRequest.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessageWithBigImage(getApplicationContext(), "Dating Invitation", message, action_time, resultIntent, AppConstants.PROFILE_PICTURE_PATH + java.io.File.separator + action_by_profile_pic);
                } else if (action.equals(NOTIFICATION_ACTION_DATING_REQUEST_ACCEPTED)) {
                    resultIntent = new Intent(getApplicationContext(), ChatsMainActivity.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessageWithBigImage(getApplicationContext(), "Dating Invitation", message, action_time, resultIntent, AppConstants.PROFILE_PICTURE_PATH + java.io.File.separator + action_by_profile_pic);
                } else if (action.equals(NOTIFICATION_ACTION_DATING_REQUEST)) {
                    resultIntent = new Intent(getApplicationContext(), AcceptDatingRequest.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessage(getApplicationContext(), "Dating Invitation", message, action_time, resultIntent);
                } else if (action.equals(NOTIFICATION_ACTION_DATING_REQUEST_REJECTED)) {
                    resultIntent = new Intent(getApplicationContext(), AcceptDatingRequest.class);
                    resultIntent.putExtra("ActivityName", "OrbitProfile");
                    resultIntent.putExtra("OrbitUserID", "" + action_by);
                    showNotificationMessage(getApplicationContext(), "Dating Invitation", message, action_time, resultIntent);
                }
            }
//            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
