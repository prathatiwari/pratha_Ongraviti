
package com.exceptionaire.ongraviti.fcm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.exceptionaire.ongraviti.core.AppConstants;
import com.exceptionaire.ongraviti.core.AppSettings;
import com.exceptionaire.ongraviti.model.ResponseFCMRegistrationID;
import com.exceptionaire.ongraviti.rest.ApiClient;
import com.exceptionaire.ongraviti.rest.ApiInterface;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;


/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);

        /*if (progressDialog == null) {
            progressDialog = new CustomProgressDialog(this, getResources().getString(R.string.loading));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
        }*/

        if (AppSettings.isLogin() && AppSettings.getLoginUserId() != null) {

            String deviceIMEI = getDeviceID();
            if (deviceIMEI == null)
                deviceIMEI = "";

            Retrofit retrofit = ApiClient.getClient();
            ApiInterface loginRequest = retrofit.create(ApiInterface.class);
            Call<ResponseFCMRegistrationID> loginCall = loginRequest.setFCMDeviceRegistrationID(AppSettings.getLoginUserId(), token, "android");
            loginCall.enqueue(new Callback<ResponseFCMRegistrationID>() {
                @Override
                public void onResponse(Call<ResponseFCMRegistrationID> call, retrofit2.Response<ResponseFCMRegistrationID> response) {
                /*if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();*/
                    if (response != null && response.body() != null && response.body().getResult().equals("1"))
                        if (response.body().getMsg() != null)
                            Log.e(TAG, "ResponseFCMRegistrationID : " + response.body().getMsg());
                }

                @Override
                public void onFailure(Call<ResponseFCMRegistrationID> call, Throwable t) {
                /*if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();*/
                }
            });
        }
    }

    // get IMEI number of device
    private String getDeviceID() {
        String deviceId = null;
        try {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();
            Log.e(TAG, countryCodeValue + " ");
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = telephonyManager.getDeviceId();
            Log.e(TAG, deviceId + " ");
        } catch (Exception e) {
            Log.e(TAG, "## error : " + e.getMessage());
            e.printStackTrace();
        }
        return deviceId;
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }
}